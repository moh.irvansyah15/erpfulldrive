﻿using System;
using System.Web;
using System.Data.OleDb;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Xpo;
using DevExpress.Xpo.DB;
using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using FullDrive.Module.BusinessObjects;
using DevExpress.Data.Filtering;
using System.Web.Hosting;
using System.Reflection;
using System.Data;
using Excel;
using DevExpress.ExpressApp.Security.Strategy;
using System.Net.Mail;
using System.Net;

namespace FullDrive.Module.CustomProcess
{
    class GlobalFunction
    {
        #region Numbering

        public string GetNumberingUnlockOptimisticRecord(IDataLayer _currIDataLayer, ObjectList _currObject)
        {
            string _result = null;
            try
            {
                Session _generatorSession = new Session(_currIDataLayer);

                ApplicationSetup _appSetup = _generatorSession.FindObject<ApplicationSetup>(new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Active", true), new BinaryOperator("DefaultSystem", true)));
                if (_appSetup != null)
                {
                    NumberingHeader _numberingHeader = _generatorSession.FindObject<NumberingHeader>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("NumberingType", NumberingType.Objects),
                                                         new BinaryOperator("ApplicationSetup", _appSetup, BinaryOperatorType.Equal),
                                                         new BinaryOperator("Active", true)));
                    if (_numberingHeader != null)
                    {
                        NumberingLine _numberingLine = _generatorSession.FindObject<NumberingLine>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("ObjectList", _currObject),
                                                     new BinaryOperator("Default", true),
                                                     new BinaryOperator("Active", true),
                                                     new BinaryOperator("NumberingHeader", _numberingHeader)));
                        if (_numberingLine != null)
                        {
                            _numberingLine.LastValue = _numberingLine.LastValue + _numberingLine.IncrementValue;
                            _numberingLine.Save();
                            _result = _numberingLine.FormatedValue;
                            _numberingLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public string GetDocumentNumberingUnlockOptimisticRecord(IDataLayer _currIDataLayer, DocumentType _currDocType)
        {
            string _result = null;
            try
            {
                Session _generatorSession = new Session(_currIDataLayer);

                ApplicationSetup _appSetup = _generatorSession.FindObject<ApplicationSetup>(new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Active", true), new BinaryOperator("DefaultSystem", true)));
                if (_appSetup != null)
                {
                    NumberingHeader _numberingHeader = _generatorSession.FindObject<NumberingHeader>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("NumberingType", NumberingType.Documents),
                                                         new BinaryOperator("ApplicationSetup", _appSetup, BinaryOperatorType.Equal),
                                                         new BinaryOperator("Active", true)));

                    if (_numberingHeader != null)
                    {
                        NumberingLine _numberingLine = _generatorSession.FindObject<NumberingLine>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("DocumentType.Oid", _currDocType.Oid),
                                                     new BinaryOperator("Active", true),
                                                     new BinaryOperator("NumberingHeader", _numberingHeader)));
                        if (_numberingLine != null)
                        {
                            _numberingLine.LastValue = _numberingLine.LastValue + _numberingLine.IncrementValue;
                            _numberingLine.Save();
                            _result = _numberingLine.FormatedValue;
                            _numberingLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public string GetNumberingSelectionUnlockOptimisticRecord(IDataLayer _currIDataLayer, ObjectList _currObject, NumberingLine _currNumberingLine)
        {
            string _result = null;
            try
            {
                Session _generatorSession = new Session(_currIDataLayer);

                ApplicationSetup _appSetup = _generatorSession.FindObject<ApplicationSetup>(new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Active", true), new BinaryOperator("DefaultSystem", true)));
                if (_appSetup != null)
                {
                    NumberingHeader _numberingHeader = _generatorSession.FindObject<NumberingHeader>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("NumberingType", NumberingType.Objects),
                                                         new BinaryOperator("ApplicationSetup", _appSetup, BinaryOperatorType.Equal),
                                                         new BinaryOperator("Active", true)));
                    if (_numberingHeader != null)
                    {
                        NumberingLine _numberingLine = _generatorSession.FindObject<NumberingLine>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("Oid", _currNumberingLine.Oid),
                                                     new BinaryOperator("ObjectList", _currObject),
                                                     new BinaryOperator("Selection", true),
                                                     new BinaryOperator("Active", true),
                                                     new BinaryOperator("NumberingHeader", _numberingHeader)));
                        if (_numberingLine != null)
                        {
                            _numberingLine.LastValue = _numberingLine.LastValue + _numberingLine.IncrementValue;
                            _numberingLine.Save();
                            _result = _numberingLine.FormatedValue;
                            _numberingLine.Session.CommitTransaction();
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public string GetNumberingSignUnlockOptimisticRecord(IDataLayer _currIDataLayer, ObjectList _currObject)
        {
            string _result = null;
            try
            {
                Session _generatorSession = new Session(_currIDataLayer);

                ApplicationSetup _appSetup = _generatorSession.FindObject<ApplicationSetup>(new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Active", true), new BinaryOperator("DefaultSystem", true)));
                if (_appSetup != null)
                {
                    NumberingHeader _numberingHeader = _generatorSession.FindObject<NumberingHeader>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("NumberingType", NumberingType.Objects),
                                                         new BinaryOperator("ApplicationSetup", _appSetup, BinaryOperatorType.Equal),
                                                         new BinaryOperator("Active", true)));
                    if (_numberingHeader != null)
                    {
                        NumberingLine _numberingLine = _generatorSession.FindObject<NumberingLine>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("ObjectList", _currObject),
                                                     new BinaryOperator("Sign", true),
                                                     new BinaryOperator("Active", true),
                                                     new BinaryOperator("NumberingHeader", _numberingHeader)));
                        if (_numberingLine != null)
                        {
                            _numberingLine.LastValue = _numberingLine.LastValue + _numberingLine.IncrementValue;
                            _numberingLine.Save();
                            _result = _numberingLine.FormatedValue;
                            _numberingLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public string GetNumberingPICUnlockOptimisticRecord(IDataLayer _currIDataLayer, ObjectList _currObject, string _locPICCode, string _locPriceGroupCode)
        {
            string _result = null;
            Employee _locPIC = null;
            PriceGroup _locPriceGroup = null;
            try
            {
               
                Session _generatorSession = new Session(_currIDataLayer);
              
                if (_generatorSession != null)
                {

                    ApplicationSetup _appSetup = _generatorSession.FindObject<ApplicationSetup>(new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Active", true), new BinaryOperator("DefaultSystem", true)));

                    if (_appSetup != null)
                    {
                        if(_locPICCode != null)
                        {
                            _locPIC = _generatorSession.FindObject<Employee>(new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("Code", _locPICCode)));
                        }

                        if(_locPriceGroupCode != null)
                        {
                            _locPriceGroup = _generatorSession.FindObject<PriceGroup>(new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("Code", _locPriceGroupCode)));
                        }

                        if(_locPIC != null && _locPriceGroup != null)
                        {
                            NumberingHeader _numberingHeader = _generatorSession.FindObject<NumberingHeader>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                             new BinaryOperator("NumberingType", NumberingType.Employee),
                                                             new BinaryOperator("ApplicationSetup", _appSetup, BinaryOperatorType.Equal),
                                                             new BinaryOperator("Active", true)));
                            if (_numberingHeader != null)
                            {
                                NumberingLine _numberingLine = _generatorSession.FindObject<NumberingLine>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                             new BinaryOperator("ObjectList", _currObject),
                                                             new BinaryOperator("PIC", _locPIC),
                                                             new BinaryOperator("PriceGroup", _locPriceGroup),
                                                             new BinaryOperator("Active", true),
                                                             new BinaryOperator("NumberingHeader", _numberingHeader)));
                                if (_numberingLine != null)
                                {
                                    _numberingLine.LastValue = _numberingLine.LastValue + _numberingLine.IncrementValue;
                                    _numberingLine.Save();
                                    _result = _numberingLine.FormatedValue;
                                    _numberingLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        #endregion Numbering

        #region GlobalFunction
        public static string Right(string value, int lenght)
        {
            return value.Substring(value.Length - lenght);
        }

        public static string Mid(string value, int val1, int val2)
        {
            return value.Substring(val1, val2);
        }

        public static bool IsDate(Object obj)
        {
            string strDate = obj.ToString();
            try
            {
                DateTime dt = DateTime.Parse(strDate);
                if ((dt.Month != System.DateTime.Now.Month) || (dt.Day < 1 && dt.Day > 31) || dt.Year != System.DateTime.Now.Year)
                    return false;
                else
                    return true;
            }
            catch
            {
                return false;
            }
        }

        protected bool CheckDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion GlobalFunction


        #region Import Data

        public void ImportDataExcel(Session _currSession, string _fullPath, string _ext, ObjectList _objList)
        {
            PropertyInfo[] _propInfo = null;
            object _newObject = null;
            IExcelDataReader excelReader;
            DataSet ds = new DataSet();
            Boolean _dataBool;
            try
            {
                FileStream stream = File.Open(_fullPath, FileMode.Open, FileAccess.Read);
                if (_ext == ".xlsx")
                {
                    //1. Reading from a binary Excel file ('97-2003 format; *.xls)
                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                }
                else
                {
                    //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
                    excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                }

                excelReader.IsFirstRowAsColumnNames = true;
                ds = excelReader.AsDataSet();
                DataTable dt = ds.Tables[0];
                _propInfo = GetPropInfo(_objList);
                excelReader.Close();

                if (_propInfo != null)
                {
                    foreach (DataRow dRow in dt.Rows)
                    {
                        _newObject = GetBusinessObject(_currSession, _objList);
                        for (int i = 0; i <= dRow.ItemArray.Length - 1; i++)
                        {
                            for (int j = 0; j <= _propInfo.Length - 1; j++)
                            {
                                if (_propInfo[j].Name.ToLower().Replace(" ", "") == dt.Columns[i].ToString().Trim().ToLower().Replace(" ", ""))
                                {
                                    #region DefaultProperty

                                    if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(string)))
                                    {
                                        _propInfo[j].SetValue(_newObject, dRow[i].ToString(), null);
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(bool)))
                                    {
                                        if (dRow[i].ToString().Trim().ToLower() == "Checked".ToLower())
                                        {
                                            _dataBool = true;
                                        }
                                        else if (dRow[i].ToString().Trim().ToLower() == "TRUE".ToLower())
                                        {
                                            _dataBool = true;
                                        }
                                        else
                                        {
                                            _dataBool = false;
                                        }

                                        _propInfo[j].SetValue(_newObject, _dataBool, null);
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(decimal)))
                                    {
                                        _propInfo[j].SetValue(_newObject, Convert.ToDecimal(dRow[i].ToString()), null);
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(int)))
                                    {
                                        _propInfo[j].SetValue(_newObject, Convert.ToInt32(dRow[i].ToString()), null);
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(double)))
                                    {
                                        _propInfo[j].SetValue(_newObject, Convert.ToDouble(dRow[i].ToString()), null);
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(System.DateTime)))
                                    {
                                        if (CheckDate(dRow[i].ToString()))
                                        {
                                            _propInfo[j].SetValue(_newObject, Convert.ToDateTime(dRow[i].ToString()), null);
                                        }
                                    }

                                    #endregion DefaultProperty

                                    #region Import Excel For Enum
                                    //Import Excel For Enum
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ObjectList)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetObjectList(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(AccountCharge)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetAccountCharge(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(AccountMode)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetAccountMode(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(AccountType)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetAccountType(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ApprovalLevel)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetApprovalLevel(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(BusinessPartnerType)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetBusinessPartnerType(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(DirectionRule)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetDirectionRule(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(DirectionType)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetDirectionType(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(DiscountMode)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetDiscountMode(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(DocumentRule)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetDocumentRule(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(FieldName)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetFieldName(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(FinancialStatment)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetFinancialStatment(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(FunctionList)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetFunctionList(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(InventoryMovingType)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetInventoryMovingType(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(OrderType)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetOrderType(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(PageType)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetPageType(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(PostingMethod)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetPostingMethod(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(PostingMethodType)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetPostingMethodType(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(PostingType)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetPostingType(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(PriorityType)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetPriorityType(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ReturnType)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetReturnType(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Status)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetStatus(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(StockType)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetStockType(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(TaxMode)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetTaxMode(dRow[i].ToString()), null);
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(TaxNature)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetTaxNature(dRow[i].ToString()), null);
                                        }
                                    }
                                    //New 010320
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(LocationType)))
                                    {
                                        if (dRow[i].ToString() != String.Empty)
                                        {
                                            _propInfo[j].SetValue(_newObject, GetLocationType(dRow[i].ToString()), null);
                                        }
                                    }
                                    #endregion Import Excel For Enum

                                    #region Import Excel For Object
                                    //Import Excel For Object
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(AccountMap)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            AccountMap _lineData = _currSession.FindObject<AccountMap>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(AccountMapLine)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            AccountMapLine _lineData = _currSession.FindObject<AccountMapLine>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(AccountReclassJournal)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            AccountReclassJournal _lineData = _currSession.FindObject<AccountReclassJournal>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(AccountReclassJournalLine)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            AccountReclassJournalLine _lineData = _currSession.FindObject<AccountReclassJournalLine>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ApplicationSetup)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            ApplicationSetup _lineData = _currSession.FindObject<ApplicationSetup>
                                                (new BinaryOperator("SetupName", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ApplicationSetupDetail)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            ApplicationSetupDetail _lineData = _currSession.FindObject<ApplicationSetupDetail>
                                                (new BinaryOperator("Code", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Armour)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Armour _lineData = _currSession.FindObject<Armour>
                                                (new BinaryOperator("Code", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(BankAccount)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            BankAccount _lineData = _currSession.FindObject<BankAccount>
                                                (new BinaryOperator("FullName", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(BankAccountGroup)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            BankAccountGroup _lineData = _currSession.FindObject<BankAccountGroup>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(BeginingInventory)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            BeginingInventory _lineData = _currSession.FindObject<BeginingInventory>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(BeginingInventoryLine)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            BeginingInventoryLine _lineData = _currSession.FindObject<BeginingInventoryLine>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(BillOfMaterial)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            BillOfMaterial _lineData = _currSession.FindObject<BillOfMaterial>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(BillOfMaterialLine)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            BillOfMaterialLine _lineData = _currSession.FindObject<BillOfMaterialLine>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(BillOfMaterialVersion)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            BillOfMaterialVersion _lineData = _currSession.FindObject<BillOfMaterialVersion>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(BinLocation)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            BinLocation _lineData = _currSession.FindObject<BinLocation>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Branch)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            Branch _lineData = _currSession.FindObject<Branch>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Brand)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Brand _lineData = _currSession.FindObject<Brand>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(BusinessPartner)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            BusinessPartner _lineData = _currSession.FindObject<BusinessPartner>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(BusinessPartnerAccountGroup)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            BusinessPartnerAccountGroup _lineData = _currSession.FindObject<BusinessPartnerAccountGroup>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(BusinessPartnerGroup)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            BusinessPartnerGroup _lineData = _currSession.FindObject<BusinessPartnerGroup>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(CashAdvance)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            CashAdvance _lineData = _currSession.FindObject<CashAdvance>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(CashAdvanceLine)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            CashAdvanceLine _lineData = _currSession.FindObject<CashAdvanceLine>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ChartOfAccount)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            ChartOfAccount _lineData = _currSession.FindObject<ChartOfAccount>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(City)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            City _lineData = _currSession.FindObject<City>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Color)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Color _lineData = _currSession.FindObject<Color>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Company)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Company _lineData = _currSession.FindObject<Company>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(CompanyAccountGroup)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            CompanyAccountGroup _lineData = _currSession.FindObject<CompanyAccountGroup>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Conductor)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Conductor _lineData = _currSession.FindObject<Conductor>
                                                (new BinaryOperator("Code", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Consumption)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            Consumption _lineData = _currSession.FindObject<Consumption>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ConsumptionJournal)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            ConsumptionJournal _lineData = _currSession.FindObject<ConsumptionJournal>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ConsumptionLine)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            ConsumptionLine _lineData = _currSession.FindObject<ConsumptionLine>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(CoreNumber)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            CoreNumber _lineData = _currSession.FindObject<CoreNumber>
                                                (new BinaryOperator("FullName", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Country)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Country _lineData = _currSession.FindObject<Country>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(CreditMemoCollection)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            CreditMemoCollection _lineData = _currSession.FindObject<CreditMemoCollection>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(CrossSectionArea)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            CrossSectionArea _lineData = _currSession.FindObject<CrossSectionArea>
                                                (new BinaryOperator("Code", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Currency)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Currency _lineData = _currSession.FindObject<Currency>
                                                (new BinaryOperator("ShortName", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(CurrencyRate)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            CurrencyRate _lineData = _currSession.FindObject<CurrencyRate>
                                                (new BinaryOperator("Code", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(CuttingProcess)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            CuttingProcess _lineData = _currSession.FindObject<CuttingProcess>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(CuttingProcessLine)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            CuttingProcessLine _lineData = _currSession.FindObject<CuttingProcessLine>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(DebitMemo)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            DebitMemo _lineData = _currSession.FindObject<DebitMemo>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(DebitMemoCollection)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            DebitMemoCollection _lineData = _currSession.FindObject<DebitMemoCollection>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(DebitMemoLine)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            DebitMemoLine _lineData = _currSession.FindObject<DebitMemoLine>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Department)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Department _lineData = _currSession.FindObject<Department>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(DepartmentAccountGroup)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            DepartmentAccountGroup _lineData = _currSession.FindObject<DepartmentAccountGroup>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(DesignNumber)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            DesignNumber _lineData = _currSession.FindObject<DesignNumber>
                                                (new BinaryOperator("Code", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(DesignProperty)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            DesignProperty _lineData = _currSession.FindObject<DesignProperty>
                                                (new BinaryOperator("Code", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Discount)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            Discount _lineData = _currSession.FindObject<Discount>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(DiscountAccountGroup)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            DiscountAccountGroup _lineData = _currSession.FindObject<DiscountAccountGroup>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Division)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Division _lineData = _currSession.FindObject<Division>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(DocumentType)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            DocumentType _lineData = _currSession.FindObject<DocumentType>
                                                (new BinaryOperator("Code", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Education)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            Education _lineData = _currSession.FindObject<Education>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(EducationDegree)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            EducationDegree _lineData = _currSession.FindObject<EducationDegree>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(EducationDepartment)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            EducationDepartment _lineData = _currSession.FindObject<EducationDepartment>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Employee)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Employee _lineData = _currSession.FindObject<Employee>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Family)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            Family _lineData = _currSession.FindObject<Family>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(FamilyType)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            FamilyType _lineData = _currSession.FindObject<FamilyType>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Filler)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Filler _lineData = _currSession.FindObject<Filler>
                                                (new BinaryOperator("Code", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(FinishProduction)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            FinishProduction _lineData = _currSession.FindObject<FinishProduction>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(FinishProductionJournal)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            FinishProductionJournal _lineData = _currSession.FindObject<FinishProductionJournal>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(FinishProductionLine)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            FinishProductionLine _lineData = _currSession.FindObject<FinishProductionLine>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(GenderType)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            GenderType _lineData = _currSession.FindObject<GenderType>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Grade)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            Grade _lineData = _currSession.FindObject<Grade>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(IdentityType)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            IdentityType _lineData = _currSession.FindObject<IdentityType>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Identity)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            Identity _lineData = _currSession.FindObject<Identity>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(IndustrialType)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            IndustrialType _lineData = _currSession.FindObject<IndustrialType>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Insulation)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Insulation _lineData = _currSession.FindObject<Insulation>
                                                (new BinaryOperator("Code", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Insurance)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            Insurance _lineData = _currSession.FindObject<Insurance>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(InsuranceLine)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            InsuranceLine _lineData = _currSession.FindObject<InsuranceLine>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(InventoryProduction)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            InventoryProduction _lineData = _currSession.FindObject<InventoryProduction>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(InventoryProductionLine)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            InventoryProductionLine _lineData = _currSession.FindObject<InventoryProductionLine>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Item)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Item _lineData = _currSession.FindObject<Item>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ItemAccountGroup)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            ItemAccountGroup _lineData = _currSession.FindObject<ItemAccountGroup>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ItemBillOfMaterial)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            ItemBillOfMaterial _lineData = _currSession.FindObject<ItemBillOfMaterial>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ItemGroup)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            ItemGroup _lineData = _currSession.FindObject<ItemGroup>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ItemReclassJournal)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            ItemReclassJournal _lineData = _currSession.FindObject<ItemReclassJournal>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ItemReclassJournalLine)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            ItemReclassJournalLine _lineData = _currSession.FindObject<ItemReclassJournalLine>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ItemType)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            ItemType _lineData = _currSession.FindObject<ItemType>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ItemUnitOfMeasure)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            ItemUnitOfMeasure _lineData = _currSession.FindObject<ItemUnitOfMeasure>
                                                (new BinaryOperator("Code", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(JournalMap)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            JournalMap _lineData = _currSession.FindObject<JournalMap>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(LoanApproval)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            LoanApproval _lineData = _currSession.FindObject<LoanApproval>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(LoanProcess)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            LoanProcess _lineData = _currSession.FindObject<LoanProcess>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(LoanProposal)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            LoanProposal _lineData = _currSession.FindObject<LoanProposal>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(LoanType)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            LoanType _lineData = _currSession.FindObject<LoanType>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(LoanStatus)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            LoanStatus _lineData = _currSession.FindObject<LoanStatus>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Location)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Location _lineData = _currSession.FindObject<Location>
                                                (new BinaryOperator("FullName", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(MachineMapLine)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            MachineMapLine _lineData = _currSession.FindObject<MachineMapLine>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(MachineMapVersion)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            MachineMapVersion _lineData = _currSession.FindObject<MachineMapVersion>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(MailSetupDetail)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            MailSetupDetail _lineData = _currSession.FindObject<MailSetupDetail>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(MaterialRequisition)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            MaterialRequisition _lineData = _currSession.FindObject<MaterialRequisition>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(MaterialRequisitionLine)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            MaterialRequisitionLine _lineData = _currSession.FindObject<MaterialRequisitionLine>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(NonTaxableIncomeType)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            NonTaxableIncomeType _lineData = _currSession.FindObject<NonTaxableIncomeType>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(NumberingHeader)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            NumberingHeader _lineData = _currSession.FindObject<NumberingHeader>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(OrganizationSetupDetail)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            OrganizationSetupDetail _lineData = _currSession.FindObject<OrganizationSetupDetail>
                                                (new BinaryOperator("Code", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(OuterSheath)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            OuterSheath _lineData = _currSession.FindObject<OuterSheath>
                                                (new BinaryOperator("Code", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(OutputProduction)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            OutputProduction _lineData = _currSession.FindObject<OutputProduction>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(OutputProductionJournal)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            OutputProductionJournal _lineData = _currSession.FindObject<OutputProductionJournal>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(OutputProductionLine)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            OutputProductionLine _lineData = _currSession.FindObject<OutputProductionLine>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(PaymentMethod)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            PaymentMethod _lineData = _currSession.FindObject<PaymentMethod>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(PaymentRealization)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            PaymentRealization _lineData = _currSession.FindObject<PaymentRealization>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(PaymentRealizationLine)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            PaymentRealizationLine _lineData = _currSession.FindObject<PaymentRealizationLine>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(PayrollProcess)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            PayrollProcess _lineData = _currSession.FindObject<PayrollProcess>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(PayrollProcessDetail)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            PayrollProcessDetail _lineData = _currSession.FindObject<PayrollProcessDetail>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Period)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            Period _lineData = _currSession.FindObject<Period>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(PhoneNumber)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            PhoneNumber _lineData = _currSession.FindObject<PhoneNumber>
                                                (new BinaryOperator("Number", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(PhoneType)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            PhoneType _lineData = _currSession.FindObject<PhoneType>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Position)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Position _lineData = _currSession.FindObject<Position>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Price)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Price _lineData = _currSession.FindObject<Price>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(PriceGroup)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            PriceGroup _lineData = _currSession.FindObject<PriceGroup>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(PriceLine)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            PriceLine _lineData = _currSession.FindObject<PriceLine>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ProductCode)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            ProductCode _lineData = _currSession.FindObject<ProductCode>
                                                (new BinaryOperator("Code", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ProductGroup)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            ProductGroup _lineData = _currSession.FindObject<ProductGroup>
                                                (new BinaryOperator("FullName", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Production)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            Production _lineData = _currSession.FindObject<Production>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ProductionLine)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            ProductionLine _lineData = _currSession.FindObject<ProductionLine>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ProductionMaterial)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            ProductionMaterial _lineData = _currSession.FindObject<ProductionMaterial>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(PurchaseInvoiceCollection)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            PurchaseInvoiceCollection _lineData = _currSession.FindObject<PurchaseInvoiceCollection>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(PurchasePrePaymentInvoice)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            PurchasePrePaymentInvoice _lineData = _currSession.FindObject<PurchasePrePaymentInvoice>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(PurchaseRequisition)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            PurchaseRequisition _lineData = _currSession.FindObject<PurchaseRequisition>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(PurchaseRequisitionLine)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            PurchaseRequisitionLine _lineData = _currSession.FindObject<PurchaseRequisitionLine>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(PurchaseSetupDetail)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            PurchaseSetupDetail _lineData = _currSession.FindObject<PurchaseSetupDetail>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(RatedVoltage)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            RatedVoltage _lineData = _currSession.FindObject<RatedVoltage>
                                                (new BinaryOperator("Code", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Religion)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            Religion _lineData = _currSession.FindObject<Religion>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(RoundingSetupDetail)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            RoundingSetupDetail _lineData = _currSession.FindObject<RoundingSetupDetail>
                                                (new BinaryOperator("Code", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(SalaryBasic)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            SalaryBasic _lineData = _currSession.FindObject<SalaryBasic>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(SalaryComponent)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            SalaryComponent _lineData = _currSession.FindObject<SalaryComponent>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(SalaryGroup)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            SalaryGroup _lineData = _currSession.FindObject<SalaryGroup>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(SalaryGroupDetail)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            SalaryGroupDetail _lineData = _currSession.FindObject<SalaryGroupDetail>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(SalesInvoiceCollection)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            SalesInvoiceCollection _lineData = _currSession.FindObject<SalesInvoiceCollection>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(SalesInvoiceCollectionLine)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            SalesInvoiceCollectionLine _lineData = _currSession.FindObject<SalesInvoiceCollectionLine>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(SalesPrePaymentInvoice)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            SalesPrePaymentInvoice _lineData = _currSession.FindObject<SalesPrePaymentInvoice>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(SalesReturn)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            SalesReturn _lineData = _currSession.FindObject<SalesReturn>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(SalesReturnLine)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            SalesReturnLine _lineData = _currSession.FindObject<SalesReturnLine>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(School)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            School _lineData = _currSession.FindObject<School>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Screening)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Screening _lineData = _currSession.FindObject<Screening>
                                                (new BinaryOperator("Code", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Section)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Section _lineData = _currSession.FindObject<Section>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Separator)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Separator _lineData = _currSession.FindObject<Separator>
                                                (new BinaryOperator("Code", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(ShapeOfConductor)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            ShapeOfConductor _lineData = _currSession.FindObject<ShapeOfConductor>
                                                (new BinaryOperator("Code", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Speciality)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            Speciality _lineData = _currSession.FindObject<Speciality>
                                                (new BinaryOperator("SpecialityMaster", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(SpecialityMaster)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            SpecialityMaster _lineData = _currSession.FindObject<SpecialityMaster>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Standart)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Standart _lineData = _currSession.FindObject<Standart>
                                                (new BinaryOperator("Code", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(SubBrand)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            SubBrand _lineData = _currSession.FindObject<SubBrand>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(SubProductGroup)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            SubProductGroup _lineData = _currSession.FindObject<SubProductGroup>
                                                (new BinaryOperator("FullName", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Tax)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            Tax _lineData = _currSession.FindObject<Tax>
                                                (new BinaryOperator("Code", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(TaxAccountGroup)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            TaxAccountGroup _lineData = _currSession.FindObject<TaxAccountGroup>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(TaxGroup)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            TaxGroup _lineData = _currSession.FindObject<TaxGroup>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(TaxProcess)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            TaxProcess _lineData = _currSession.FindObject<TaxProcess>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(TaxRateNPWP)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            TaxRateNPWP _lineData = _currSession.FindObject<TaxRateNPWP>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(TaxRateNonNPWP)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            TaxRateNonNPWP _lineData = _currSession.FindObject<TaxRateNonNPWP>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(TaxSetup)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            TaxSetup _lineData = _currSession.FindObject<TaxSetup>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(TermOfPayment)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            TermOfPayment _lineData = _currSession.FindObject<TermOfPayment>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(Training)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            Training _lineData = _currSession.FindObject<Training>
                                                (new BinaryOperator("TrainingMaster", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(TrainingMaster)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            TrainingMaster _lineData = _currSession.FindObject<TrainingMaster>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(UnitOfMeasure)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            UnitOfMeasure _lineData = _currSession.FindObject<UnitOfMeasure>
                                                (new BinaryOperator("FullName", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(UnitOfMeasureType)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            UnitOfMeasureType _lineData = _currSession.FindObject<UnitOfMeasureType>
                                                (new BinaryOperator("Name", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(UserAccess)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            UserAccess _lineData = _currSession.FindObject<UserAccess>
                                                (new BinaryOperator("UserName", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(WarehouseSetupDetail)))
                                    {
                                        if (dRow[i].ToString() != null)
                                        {
                                            WarehouseSetupDetail _lineData = _currSession.FindObject<WarehouseSetupDetail>
                                                (new BinaryOperator("Code", dRow[i].ToString()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(WireNumber)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            WireNumber _lineData = _currSession.FindObject<WireNumber>
                                                (new BinaryOperator("FullName", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(WorkingContract)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            WorkingContract _lineData = _currSession.FindObject<WorkingContract>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(WorkingExperience)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            WorkingExperience _lineData = _currSession.FindObject<WorkingExperience>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(WorkingStatus)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            WorkingStatus _lineData = _currSession.FindObject<WorkingStatus>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(WorkOrder)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            WorkOrder _lineData = _currSession.FindObject<WorkOrder>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(WorkOrderLine)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            WorkOrderLine _lineData = _currSession.FindObject<WorkOrderLine>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(WorkOrderMaterial)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            WorkOrderMaterial _lineData = _currSession.FindObject<WorkOrderMaterial>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(WorkRequisition)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            WorkRequisition _lineData = _currSession.FindObject<WorkRequisition>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(WorkRequisitionCollection)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            WorkRequisitionCollection _lineData = _currSession.FindObject<WorkRequisitionCollection>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(WorkRequisitionCollectionLine)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            WorkRequisitionCollectionLine _lineData = _currSession.FindObject<WorkRequisitionCollectionLine>
                                                (new BinaryOperator("Name", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    else if (object.ReferenceEquals(_propInfo[j].PropertyType, typeof(WorkRequisitionLine)))
                                    {
                                        if (dRow[i].ToString() != null || !string.IsNullOrEmpty(dRow[i].ToString()))
                                        {
                                            WorkRequisitionLine _lineData = _currSession.FindObject<WorkRequisitionLine>
                                                (new BinaryOperator("Code", dRow[i].ToString().Trim()));
                                            if (_lineData != null)
                                            {
                                                _propInfo[j].SetValue(_newObject, _lineData, null);
                                            }
                                        }
                                    }
                                    
                                    #endregion Import Excel For Object
                                }
                            }
                        }
                        _currSession.Save(_newObject);
                        _currSession.CommitTransaction();
                    }
                    //_currSession.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
        }

        private PropertyInfo[] GetPropInfo(ObjectList _objList)
        {
            PropertyInfo[] _result = null;
            try
            {
                if (_objList == ObjectList.AccountGroup)
                {
                    _result = typeof(AccountGroup).GetProperties();
                }
                else if (_objList == ObjectList.AccountMap)
                {
                    _result = typeof(AccountMap).GetProperties();
                }
                else if (_objList == ObjectList.AccountMapLine)
                {
                    _result = typeof(AccountMapLine).GetProperties();
                }
                else if (_objList == ObjectList.AccountReclassJournal)
                {
                    _result = typeof(AccountReclassJournal).GetProperties();
                }
                else if (_objList == ObjectList.AccountReclassJournalLine)
                {
                    _result = typeof(AccountReclassJournalLine).GetProperties();
                }
                else if (_objList == ObjectList.AdditionalAnalyze)
                {
                    _result = typeof(AdditionalAnalyze).GetProperties();
                }
                else if (_objList == ObjectList.AdditionalDashboard)
                {
                    _result = typeof(AdditionalDashboard).GetProperties();
                }
                else if (_objList == ObjectList.AdditionalReport)
                {
                    _result = typeof(AdditionalReport).GetProperties();
                }
                else if (_objList == ObjectList.Address)
                {
                    _result = typeof(Address).GetProperties();
                }
                else if (_objList == ObjectList.ApplicationImport)
                {
                    _result = typeof(ApplicationImport).GetProperties();
                }
                else if (_objList == ObjectList.ApplicationSetup)
                {
                    _result = typeof(ApplicationSetup).GetProperties();
                }
                else if (_objList == ObjectList.ApplicationSetupDetail)
                {
                    _result = typeof(ApplicationSetupDetail).GetProperties();
                }
                else if (_objList == ObjectList.ApprovalLine)
                {
                    _result = typeof(ApprovalLine).GetProperties();
                }
                else if (_objList == ObjectList.Armour)
                {
                    _result = typeof(Armour).GetProperties();
                }
                else if (_objList == ObjectList.BankAccount)
                {
                    _result = typeof(BankAccount).GetProperties();
                }
                else if (_objList == ObjectList.BankAccountGroup)
                {
                    _result = typeof(BankAccountGroup).GetProperties();
                }
                else if (_objList == ObjectList.BeginingInventory)
                {
                    _result = typeof(BeginingInventory).GetProperties();
                }
                else if (_objList == ObjectList.BeginingInventoryLine)
                {
                    _result = typeof(BeginingInventoryLine).GetProperties();
                }
                else if (_objList == ObjectList.BillOfMaterial)
                {
                    _result = typeof(BillOfMaterial).GetProperties();
                }
                else if (_objList == ObjectList.BillOfMaterialLine)
                {
                    _result = typeof(BillOfMaterialLine).GetProperties();
                }
                else if (_objList == ObjectList.BillOfMaterialVersion)
                {
                    _result = typeof(BillOfMaterialVersion).GetProperties();
                }
                else if (_objList == ObjectList.BinLocation)
                {
                    _result = typeof(BinLocation).GetProperties();
                }
                else if (_objList == ObjectList.Branch)
                {
                    _result = typeof(Branch).GetProperties();
                }
                else if (_objList == ObjectList.Brand)
                {
                    _result = typeof(Brand).GetProperties();
                }
                else if (_objList == ObjectList.BusinessPartner)
                {
                    _result = typeof(BusinessPartner).GetProperties();
                }
                else if (_objList == ObjectList.BusinessPartnerAccountGroup)
                {
                    _result = typeof(BusinessPartnerAccountGroup).GetProperties();
                }
                else if (_objList == ObjectList.BusinessPartnerGroup)
                {
                    _result = typeof(BusinessPartnerGroup).GetProperties();
                }
                else if (_objList == ObjectList.CashAdvance)
                {
                    _result = typeof(CashAdvance).GetProperties();
                }
                else if (_objList == ObjectList.CashAdvanceLine)
                {
                    _result = typeof(CashAdvanceLine).GetProperties();
                }
                else if (_objList == ObjectList.ChartOfAccount)
                {
                    _result = typeof(ChartOfAccount).GetProperties();
                }
                else if (_objList == ObjectList.City)
                {
                    _result = typeof(City).GetProperties();
                }
                else if (_objList == ObjectList.Color)
                {
                    _result = typeof(Color).GetProperties();
                }
                else if (_objList == ObjectList.Company)
                {
                    _result = typeof(Company).GetProperties();
                }
                else if (_objList == ObjectList.CompanyAccountGroup)
                {
                    _result = typeof(CompanyAccountGroup).GetProperties();
                }
                else if (_objList == ObjectList.Conductor)
                {
                    _result = typeof(Conductor).GetProperties();
                }
                else if (_objList == ObjectList.Consumption)
                {
                    _result = typeof(Consumption).GetProperties();
                }
                else if (_objList == ObjectList.ConsumptionJournal)
                {
                    _result = typeof(ConsumptionJournal).GetProperties();
                }
                else if (_objList == ObjectList.ConsumptionLine)
                {
                    _result = typeof(ConsumptionLine).GetProperties();
                }
                else if (_objList == ObjectList.CoreNumber)
                {
                    _result = typeof(CoreNumber).GetProperties();
                }
                else if (_objList == ObjectList.CostOfGoodsSold)
                {
                    _result = typeof(CostOfGoodsSold).GetProperties();
                }
                else if (_objList == ObjectList.Country)
                {
                    _result = typeof(Country).GetProperties();
                }
                else if (_objList == ObjectList.CreditMemo)
                {
                    _result = typeof(CreditMemo).GetProperties();
                }
                else if (_objList == ObjectList.CreditMemoCollection)
                {
                    _result = typeof(CreditMemoCollection).GetProperties();
                }
                else if (_objList == ObjectList.CreditMemoLine)
                {
                    _result = typeof(CreditMemoLine).GetProperties();
                }
                else if (_objList == ObjectList.CrossSectionArea)
                {
                    _result = typeof(CrossSectionArea).GetProperties();
                }
                else if (_objList == ObjectList.Currency)
                {
                    _result = typeof(Currency).GetProperties();
                }
                else if (_objList == ObjectList.CurrencyRate)
                {
                    _result = typeof(CurrencyRate).GetProperties();
                }
                else if (_objList == ObjectList.CuttingProcess)
                {
                    _result = typeof(CuttingProcess).GetProperties();
                }
                else if (_objList == ObjectList.CuttingProcessLine)
                {
                    _result = typeof(CuttingProcessLine).GetProperties();
                }
                else if (_objList == ObjectList.CuttingProcessLot)
                {
                    _result = typeof(CuttingProcessLot).GetProperties();
                }
                else if (_objList == ObjectList.DebitMemo)
                {
                    _result = typeof(DebitMemo).GetProperties();
                }
                else if (_objList == ObjectList.DebitMemoCollection)
                {
                    _result = typeof(DebitMemoCollection).GetProperties();
                }
                else if (_objList == ObjectList.DebitMemoLine)
                {
                    _result = typeof(DebitMemoLine).GetProperties();
                }
                else if (_objList == ObjectList.Department)
                {
                    _result = typeof(Department).GetProperties();
                }
                else if (_objList == ObjectList.DepartmentAccountGroup)
                {
                    _result = typeof(DepartmentAccountGroup).GetProperties();
                }
                else if (_objList == ObjectList.DesignNumber)
                {
                    _result = typeof(DesignNumber).GetProperties();
                }
                else if (_objList == ObjectList.DesignProperty)
                {
                    _result = typeof(DesignProperty).GetProperties();
                }
                else if (_objList == ObjectList.DesignSheet)
                {
                    _result = typeof(DesignSheet).GetProperties();
                }
                else if (_objList == ObjectList.DesignSheetComponentHeader)
                {
                    _result = typeof(DesignSheetComponentHeader).GetProperties();
                }
                else if (_objList == ObjectList.DesignSheetComponentLine)
                {
                    _result = typeof(DesignSheetComponentLine).GetProperties();
                }
                else if (_objList == ObjectList.DesignSheetLine)
                {
                    _result = typeof(DesignSheetLine).GetProperties();
                }
                else if (_objList == ObjectList.Discount)
                {
                    _result = typeof(Discount).GetProperties();
                }
                else if (_objList == ObjectList.DiscountAccountGroup)
                {
                    _result = typeof(DiscountAccountGroup).GetProperties();
                }
                else if (_objList == ObjectList.DiscountLine)
                {
                    _result = typeof(DiscountLine).GetProperties();
                }
                else if (_objList == ObjectList.Division)
                {
                    _result = typeof(Division).GetProperties();
                }
                else if (_objList == ObjectList.DocumentType)
                {
                    _result = typeof(DocumentType).GetProperties();
                }
                else if (_objList == ObjectList.EDocument)
                {
                    _result = typeof(EDocument).GetProperties();
                }
                else if (_objList == ObjectList.Education)
                {
                    _result = typeof(Education).GetProperties();
                }
                else if (_objList == ObjectList.EducationDegree)
                {
                    _result = typeof(EducationDegree).GetProperties();
                }
                else if (_objList == ObjectList.EducationDepartment)
                {
                    _result = typeof(EducationDepartment).GetProperties();
                }
                else if (_objList == ObjectList.Employee)
                {
                    _result = typeof(Employee).GetProperties();
                }
                else if (_objList == ObjectList.ExchangeRate)
                {
                    _result = typeof(ExchangeRate).GetProperties();
                }
                else if (_objList == ObjectList.Family)
                {
                    _result = typeof(Family).GetProperties();
                }
                else if (_objList == ObjectList.FamilyType)
                {
                    _result = typeof(FamilyType).GetProperties();
                }
                else if (_objList == ObjectList.Filler)
                {
                    _result = typeof(Filler).GetProperties();
                }
                else if (_objList == ObjectList.FinishProduction)
                {
                    _result = typeof(FinishProduction).GetProperties();
                }
                else if (_objList == ObjectList.FinishProductionJournal)
                {
                    _result = typeof(FinishProductionJournal).GetProperties();
                }
                else if (_objList == ObjectList.FinishProductionLine)
                {
                    _result = typeof(FinishProductionLine).GetProperties();
                }
                else if (_objList == ObjectList.GenderType)
                {
                    _result = typeof(GenderType).GetProperties();
                }
                else if (_objList == ObjectList.GeneralJournal)
                {
                    _result = typeof(GeneralJournal).GetProperties();
                }
                else if (_objList == ObjectList.Grade)
                {
                    _result = typeof(Grade).GetProperties();
                }
                else if (_objList == ObjectList.Grease)
                {
                    _result = typeof(Grease).GetProperties();
                }
                else if (_objList == ObjectList.Identity)
                {
                    _result = typeof(Identity).GetProperties();
                }
                else if (_objList == ObjectList.IdentityType)
                {
                    _result = typeof(IdentityType).GetProperties();
                }
                else if (_objList == ObjectList.IndustrialType)
                {
                    _result = typeof(IndustrialType).GetProperties();
                }
                else if (_objList == ObjectList.Insulation)
                {
                    _result = typeof(Insulation).GetProperties();
                }
                else if (_objList == ObjectList.Insurance)
                {
                    _result = typeof(Insurance).GetProperties();
                }
                else if (_objList == ObjectList.InsuranceLine)
                {
                    _result = typeof(InsuranceLine).GetProperties();
                }
                else if (_objList == ObjectList.InventoryJournal)
                {
                    _result = typeof(InventoryJournal).GetProperties();
                }
                else if (_objList == ObjectList.InventoryProduction)
                {
                    _result = typeof(InventoryProduction).GetProperties();
                }
                else if (_objList == ObjectList.InventoryProductionLine)
                {
                    _result = typeof(InventoryProductionLine).GetProperties();
                }
                else if (_objList == ObjectList.InventorySalesCollection)
                {
                    _result = typeof(InventorySalesCollection).GetProperties();
                }
                else if (_objList == ObjectList.InventoryTransferIn)
                {
                    _result = typeof(InventoryTransferIn).GetProperties();
                }
                else if (_objList == ObjectList.InventoryTransferInLine)
                {
                    _result = typeof(InventoryTransferInLine).GetProperties();
                }
                else if (_objList == ObjectList.InventoryTransferInLot)
                {
                    _result = typeof(InventoryTransferInLot).GetProperties();
                }
                else if (_objList == ObjectList.InventoryTransferOrder)
                {
                    _result = typeof(InventoryTransferOrder).GetProperties();
                }
                else if (_objList == ObjectList.InventoryTransferOrderLine)
                {
                    _result = typeof(InventoryTransferOrderLine).GetProperties();
                }
                else if (_objList == ObjectList.InventoryTransferOrderLot)
                {
                    _result = typeof(InventoryTransferOrderLot).GetProperties();
                }
                else if (_objList == ObjectList.InventoryTransferOrderMonitoring)
                {
                    _result = typeof(InventoryTransferOrderMonitoring).GetProperties();
                }
                else if (_objList == ObjectList.InventoryTransferOut)
                {
                    _result = typeof(InventoryTransferOut).GetProperties();
                }
                else if (_objList == ObjectList.InventoryTransferOutLine)
                {
                    _result = typeof(InventoryTransferOutLine).GetProperties();
                }
                else if (_objList == ObjectList.InventoryTransferOutLot)
                {
                    _result = typeof(InventoryTransferOutLot).GetProperties();
                }
                else if (_objList == ObjectList.InventoryTransferOutMonitoring)
                {
                    _result = typeof(InventoryTransferOutMonitoring).GetProperties();
                }
                else if (_objList == ObjectList.Item)
                {
                    _result = typeof(Item).GetProperties();
                }
                else if (_objList == ObjectList.ItemAccountGroup)
                {
                    _result = typeof(ItemAccountGroup).GetProperties();
                }
                else if (_objList == ObjectList.ItemBillOfMaterial)
                {
                    _result = typeof(ItemBillOfMaterial).GetProperties();
                }
                else if (_objList == ObjectList.ItemComponent)
                {
                    _result = typeof(ItemComponent).GetProperties();
                }
                else if (_objList == ObjectList.ItemConsumption)
                {
                    _result = typeof(ItemConsumption).GetProperties();
                }
                else if (_objList == ObjectList.ItemConsumptionLine)
                {
                    _result = typeof(ItemConsumptionLine).GetProperties();
                }
                else if (_objList == ObjectList.ItemGroup)
                {
                    _result = typeof(ItemGroup).GetProperties();
                }
                else if (_objList == ObjectList.ItemReclassJournal)
                {
                    _result = typeof(ItemReclassJournal).GetProperties();
                }
                else if (_objList == ObjectList.ItemReclassJournalLine)
                {
                    _result = typeof(ItemReclassJournalLine).GetProperties();
                }
                else if (_objList == ObjectList.ItemType)
                {
                    _result = typeof(ItemType).GetProperties();
                }
                else if (_objList == ObjectList.ItemUnitOfMeasure)
                {
                    _result = typeof(ItemUnitOfMeasure).GetProperties();
                }
                else if (_objList == ObjectList.JournalMap)
                {
                    _result = typeof(JournalMap).GetProperties();
                }
                else if (_objList == ObjectList.JournalMapLine)
                {
                    _result = typeof(JournalMapLine).GetProperties();
                }
                else if (_objList == ObjectList.ListImport)
                {
                    _result = typeof(ListImport).GetProperties();
                }
                else if (_objList == ObjectList.LoanApproval)
                {
                    _result = typeof(LoanApproval).GetProperties();
                }
                else if (_objList == ObjectList.LoanProcess)
                {
                    _result = typeof(LoanProcess).GetProperties();
                }
                else if (_objList == ObjectList.LoanProposal)
                {
                    _result = typeof(LoanProposal).GetProperties();
                }
                else if (_objList == ObjectList.LoanStatus)
                {
                    _result = typeof(LoanStatus).GetProperties();
                }
                else if (_objList == ObjectList.LoanType)
                {
                    _result = typeof(LoanType).GetProperties();
                }
                else if (_objList == ObjectList.Location)
                {
                    _result = typeof(Location).GetProperties();
                }
                else if (_objList == ObjectList.Machine)
                {
                    _result = typeof(Machine).GetProperties();
                }
                else if (_objList == ObjectList.MachineMapLine)
                {
                    _result = typeof(MachineMapLine).GetProperties();
                }
                else if (_objList == ObjectList.MachineMapVersion)
                {
                    _result = typeof(MachineMapVersion).GetProperties();
                }
                else if (_objList == ObjectList.MailSetupDetail)
                {
                    _result = typeof(MailSetupDetail).GetProperties();
                }
                else if (_objList == ObjectList.MaterialPrice)
                {
                    _result = typeof(MaterialPrice).GetProperties();
                }
                else if (_objList == ObjectList.MaterialRequisition)
                {
                    _result = typeof(MaterialRequisition).GetProperties();
                }
                else if (_objList == ObjectList.MaterialRequisitionLine)
                {
                    _result = typeof(MaterialRequisitionLine).GetProperties();
                }
                else if (_objList == ObjectList.NonTaxableIncome)
                {
                    _result = typeof(NonTaxableIncomeType).GetProperties();
                }
                else if (_objList == ObjectList.NumberingHeader)
                {
                    _result = typeof(NumberingHeader).GetProperties();
                }
                else if (_objList == ObjectList.NumberingLine)
                {
                    _result = typeof(NumberingLine).GetProperties();
                }
                else if (_objList == ObjectList.OrganizationSetupDetail)
                {
                    _result = typeof(OrganizationSetupDetail).GetProperties();
                }
                else if (_objList == ObjectList.OuterSheath)
                {
                    _result = typeof(OuterSheath).GetProperties();
                }
                else if (_objList == ObjectList.OutputProduction)
                {
                    _result = typeof(OutputProduction).GetProperties();
                }
                else if (_objList == ObjectList.OutputProductionJournal)
                {
                    _result = typeof(OutputProductionJournal).GetProperties();
                }
                else if (_objList == ObjectList.OutputProductionLine)
                {
                    _result = typeof(OutputProductionLine).GetProperties();
                }
                else if (_objList == ObjectList.OverheadCost)
                {
                    _result = typeof(OverheadCost).GetProperties();
                }
                else if (_objList == ObjectList.OverheadCostDepreciation)
                {
                    _result = typeof(OverheadCostDepreciation).GetProperties();
                }
                else if (_objList == ObjectList.OverheadCostElectric)
                {
                    _result = typeof(OverheadCostElectric).GetProperties();
                }
                else if (_objList == ObjectList.OverheadCostEmployee)
                {
                    _result = typeof(OverheadCostEmployee).GetProperties();
                }
                else if (_objList == ObjectList.PayableTransaction)
                {
                    _result = typeof(PayableTransaction).GetProperties();
                }
                else if (_objList == ObjectList.PayableTransactionLine)
                {
                    _result = typeof(PayableTransactionLine).GetProperties();
                }
                else if (_objList == ObjectList.PaymentInPlan)
                {
                    _result = typeof(PaymentInPlan).GetProperties();
                }
                else if (_objList == ObjectList.PaymentMethod)
                {
                    _result = typeof(PaymentMethod).GetProperties();
                }
                else if (_objList == ObjectList.PaymentOutPlan)
                {
                    _result = typeof(PaymentOutPlan).GetProperties();
                }
                else if (_objList == ObjectList.PaymentRealization)
                {
                    _result = typeof(PaymentRealization).GetProperties();
                }
                else if (_objList == ObjectList.PaymentRealizationLine)
                {
                    _result = typeof(PaymentRealizationLine).GetProperties();
                }
                else if (_objList == ObjectList.PayrollProcess)
                {
                    _result = typeof(PayrollProcess).GetProperties();
                }
                else if (_objList == ObjectList.PayrollProcessDetail)
                {
                    _result = typeof(PayrollProcessDetail).GetProperties();
                }
                else if (_objList == ObjectList.Period)
                {
                    _result = typeof(Period).GetProperties();
                }
                else if (_objList == ObjectList.PhoneNumber)
                {
                    _result = typeof(PhoneNumber).GetProperties();
                }
                else if (_objList == ObjectList.PhoneType)
                {
                    _result = typeof(PhoneType).GetProperties();
                }
                else if (_objList == ObjectList.Position)
                {
                    _result = typeof(Position).GetProperties();
                }
                else if (_objList == ObjectList.PrePurchaseOrder)
                {
                    _result = typeof(PrePurchaseOrder).GetProperties();
                }
                else if (_objList == ObjectList.PrePurchaseOrder2)
                {
                    _result = typeof(PrePurchaseOrder2).GetProperties();
                }
                else if (_objList == ObjectList.PreSalesOrder)
                {
                    _result = typeof(PreSalesOrder).GetProperties();
                }
                else if (_objList == ObjectList.PreSalesOrder2)
                {
                    _result = typeof(PreSalesOrder2).GetProperties();
                }
                else if (_objList == ObjectList.Price)
                {
                    _result = typeof(Price).GetProperties();
                }
                else if (_objList == ObjectList.PriceGroup)
                {
                    _result = typeof(PriceGroup).GetProperties();
                }
                else if (_objList == ObjectList.PriceLine)
                {
                    _result = typeof(PriceLine).GetProperties();
                }
                else if (_objList == ObjectList.ProductCode)
                {
                    _result = typeof(ProductCode).GetProperties();
                }
                else if (_objList == ObjectList.ProductGroup)
                {
                    _result = typeof(ProductGroup).GetProperties();
                }
                else if (_objList == ObjectList.Production)
                {
                    _result = typeof(Production).GetProperties();
                }
                else if (_objList == ObjectList.ProductionLine)
                {
                    _result = typeof(ProductionLine).GetProperties();
                }
                else if (_objList == ObjectList.ProductionMaterial)
                {
                    _result = typeof(ProductionMaterial).GetProperties();
                }
                else if (_objList == ObjectList.ProductType)
                {
                    _result = typeof(ProductType).GetProperties();
                }
                else if (_objList == ObjectList.ProjectHeader)
                {
                    _result = typeof(ProjectHeader).GetProperties();
                }
                else if (_objList == ObjectList.ProjectLine)
                {
                    _result = typeof(ProjectLine).GetProperties();
                }
                else if (_objList == ObjectList.ProjectLine2)
                {
                    _result = typeof(ProjectLine2).GetProperties();
                }
                else if (_objList == ObjectList.ProjectLine2Item)
                {
                    _result = typeof(ProjectLine2Item).GetProperties();
                }
                else if (_objList == ObjectList.ProjectLine2Item2)
                {
                    _result = typeof(ProjectLine2Item2).GetProperties();
                }
                else if (_objList == ObjectList.ProjectLine2Service)
                {
                    _result = typeof(ProjectLine2Service).GetProperties();
                }
                else if (_objList == ObjectList.ProjectLineItem)
                {
                    _result = typeof(ProjectLineItem).GetProperties();
                }
                else if (_objList == ObjectList.ProjectLineItem2)
                {
                    _result = typeof(ProjectLineItem2).GetProperties();
                }
                else if (_objList == ObjectList.ProjectLineService)
                {
                    _result = typeof(ProjectLineService).GetProperties();
                }
                else if (_objList == ObjectList.PurchaseInvoice)
                {
                    _result = typeof(PurchaseInvoice).GetProperties();
                }
                else if (_objList == ObjectList.PurchaseInvoiceCollection)
                {
                    _result = typeof(PurchaseInvoiceCollection).GetProperties();
                }
                else if (_objList == ObjectList.PurchaseInvoiceCollectionLine)
                {
                    _result = typeof(PurchaseInvoiceCollectionLine).GetProperties();
                }
                else if (_objList == ObjectList.PurchaseInvoiceLine)
                {
                    _result = typeof(PurchaseInvoiceLine).GetProperties();
                }
                else if (_objList == ObjectList.PurchaseOrder)
                {
                    _result = typeof(PurchaseOrder).GetProperties();
                }
                else if (_objList == ObjectList.PurchaseOrderLine)
                {
                    _result = typeof(PurchaseOrderLine).GetProperties();
                }
                else if (_objList == ObjectList.PurchaseRequisition)
                {
                    _result = typeof(PurchaseRequisition).GetProperties();
                }
                else if (_objList == ObjectList.PurchaseRequisitionCollection)
                {
                    _result = typeof(PurchaseRequisitionCollection).GetProperties();
                }
                else if (_objList == ObjectList.PurchaseRequisitionLine)
                {
                    _result = typeof(PurchaseRequisitionLine).GetProperties();
                }
                else if (_objList == ObjectList.PurchaseReturn)
                {
                    _result = typeof(PurchaseReturn).GetProperties();
                }
                else if (_objList == ObjectList.PurchaseReturnLine)
                {
                    _result = typeof(PurchaseReturnLine).GetProperties();
                }
                else if (_objList == ObjectList.PurchaseSetupDetail)
                {
                    _result = typeof(PurchaseSetupDetail).GetProperties();
                }
                else if (_objList == ObjectList.RatedVoltage)
                {
                    _result = typeof(RatedVoltage).GetProperties();
                }
                else if (_objList == ObjectList.ReceivableTransaction)
                {
                    _result = typeof(ReceivableTransaction).GetProperties();
                }
                else if (_objList == ObjectList.ReceivableTransactionLine)
                {
                    _result = typeof(ReceivableTransactionLine).GetProperties();
                }
                else if (_objList == ObjectList.Religion)
                {
                    _result = typeof(Religion).GetProperties();
                }
                else if (_objList == ObjectList.RoundingSetupDetail)
                {
                    _result = typeof(RoundingSetupDetail).GetProperties();
                }
                else if (_objList == ObjectList.SalaryBasic)
                {
                    _result = typeof(SalaryBasic).GetProperties();
                }
                else if (_objList == ObjectList.SalaryComponent)
                {
                    _result = typeof(SalaryComponent).GetProperties();
                }
                else if (_objList == ObjectList.SalaryGroup)
                {
                    _result = typeof(SalaryGroup).GetProperties();
                }
                else if (_objList == ObjectList.SalaryGroupDetail)
                {
                    _result = typeof(SalaryGroupDetail).GetProperties();
                }
                else if (_objList == ObjectList.SalesInvoice)
                {
                    _result = typeof(SalesInvoice).GetProperties();
                }
                else if (_objList == ObjectList.SalesInvoiceCollection)
                {
                    _result = typeof(SalesInvoiceCollection).GetProperties();
                }
                else if (_objList == ObjectList.SalesInvoiceCollectionLine)
                {
                    _result = typeof(SalesInvoiceCollectionLine).GetProperties();
                }
                else if (_objList == ObjectList.SalesInvoiceLine)
                {
                    _result = typeof(SalesInvoiceLine).GetProperties();
                }
                else if (_objList == ObjectList.SalesOrder)
                {
                    _result = typeof(SalesOrder).GetProperties();
                }
                else if (_objList == ObjectList.SalesOrderLine)
                {
                    _result = typeof(SalesOrderLine).GetProperties();
                }
                else if (_objList == ObjectList.SalesOrderMonitoring)
                {
                    _result = typeof(SalesOrderMonitoring).GetProperties();
                }
                else if (_objList == ObjectList.SalesQuotation)
                {
                    _result = typeof(SalesQuotation).GetProperties();
                }
                else if (_objList == ObjectList.SalesQuotationCollection)
                {
                    _result = typeof(SalesQuotationCollection).GetProperties();
                }
                else if (_objList == ObjectList.SalesQuotationLine)
                {
                    _result = typeof(SalesQuotationLine).GetProperties();
                }
                else if (_objList == ObjectList.SalesQuotationMonitoring)
                {
                    _result = typeof(SalesQuotationMonitoring).GetProperties();
                }
                else if (_objList == ObjectList.SalesReturn)
                {
                    _result = typeof(SalesReturn).GetProperties();
                }
                else if (_objList == ObjectList.SalesReturnLine)
                {
                    _result = typeof(SalesReturnLine).GetProperties();
                }
                else if (_objList == ObjectList.SalesSetupDetail)
                {
                    _result = typeof(SalesSetupDetail).GetProperties();
                }
                else if (_objList == ObjectList.School)
                {
                    _result = typeof(School).GetProperties();
                }
                else if (_objList == ObjectList.Screening)
                {
                    _result = typeof(Screening).GetProperties();
                }
                else if (_objList == ObjectList.Section)
                {
                    _result = typeof(Section).GetProperties();
                }
                else if (_objList == ObjectList.Separator)
                {
                    _result = typeof(Separator).GetProperties();
                }
                else if (_objList == ObjectList.ShapeOfConductor)
                {
                    _result = typeof(ShapeOfConductor).GetProperties();
                }
                else if (_objList == ObjectList.Speciality)
                {
                    _result = typeof(Speciality).GetProperties();
                }
                else if (_objList == ObjectList.SpecialityMaster)
                {
                    _result = typeof(SpecialityMaster).GetProperties();
                }
                else if (_objList == ObjectList.Standart)
                {
                    _result = typeof(Standart).GetProperties();
                }
                else if (_objList == ObjectList.SubBrand)
                {
                    _result = typeof(SubBrand).GetProperties();
                }
                else if (_objList == ObjectList.SubProductGroup)
                {
                    _result = typeof(SubProductGroup).GetProperties();
                }
                else if (_objList == ObjectList.Tax)
                {
                    _result = typeof(Tax).GetProperties();
                }
                else if (_objList == ObjectList.TaxAccountGroup)
                {
                    _result = typeof(TaxAccountGroup).GetProperties();
                }
                else if (_objList == ObjectList.TaxGroup)
                {
                    _result = typeof(TaxGroup).GetProperties();
                }
                else if (_objList == ObjectList.TaxLine)
                {
                    _result = typeof(TaxLine).GetProperties();
                }
                else if (_objList == ObjectList.TaxProcess)
                {
                    _result = typeof(TaxProcess).GetProperties();
                }
                else if (_objList == ObjectList.TaxRateNonNPWP)
                {
                    _result = typeof(TaxRateNonNPWP).GetProperties();
                }
                else if (_objList == ObjectList.TaxRateNPWP)
                {
                    _result = typeof(TaxRateNPWP).GetProperties();
                }
                else if (_objList == ObjectList.TaxSetup)
                {
                    _result = typeof(TaxSetup).GetProperties();
                }
                else if (_objList == ObjectList.TaxType)
                {
                    _result = typeof(TaxType).GetProperties();
                }
                else if (_objList == ObjectList.TempBeginingInventoryLine)
                {
                    _result = typeof(TempBeginingInventoryLine).GetProperties();
                }
                else if (_objList == ObjectList.TermOfPayment)
                {
                    _result = typeof(TermOfPayment).GetProperties();
                }
                else if (_objList == ObjectList.Training)
                {
                    _result = typeof(Training).GetProperties();
                }
                else if (_objList == ObjectList.TrainingMaster)
                {
                    _result = typeof(TrainingMaster).GetProperties();
                }
                else if (_objList == ObjectList.TransferIn)
                {
                    _result = typeof(TransferIn).GetProperties();
                }
                else if (_objList == ObjectList.TransferInLine)
                {
                    _result = typeof(TransferInLine).GetProperties();
                }
                else if (_objList == ObjectList.TransferInLot)
                {
                    _result = typeof(TransferInLot).GetProperties();
                }
                else if (_objList == ObjectList.TransferOrder)
                {
                    _result = typeof(TransferOrder).GetProperties();
                }
                else if (_objList == ObjectList.TransferOrderLine)
                {
                    _result = typeof(TransferOrderLine).GetProperties();
                }
                else if (_objList == ObjectList.TransferOrderLot)
                {
                    _result = typeof(TransferOrderLot).GetProperties();
                }
                else if (_objList == ObjectList.TransferOut)
                {
                    _result = typeof(TransferOut).GetProperties();
                }
                else if (_objList == ObjectList.TransferOutLine)
                {
                    _result = typeof(TransferOutLine).GetProperties();
                }
                else if (_objList == ObjectList.TransferOutLot)
                {
                    _result = typeof(TransferOutLot).GetProperties();
                }
                else if (_objList == ObjectList.UnitOfMeasure)
                {
                    _result = typeof(UnitOfMeasure).GetProperties();
                }
                else if (_objList == ObjectList.UnitOfMeasureType)
                {
                    _result = typeof(UnitOfMeasureType).GetProperties();
                }
                else if (_objList == ObjectList.UserAccess)
                {
                    _result = typeof(UserAccess).GetProperties();
                }
                else if (_objList == ObjectList.UserAccessRole)
                {
                    _result = typeof(UserAccessRole).GetProperties();
                }
                else if (_objList == ObjectList.WarehouseSetupDetail)
                {
                    _result = typeof(WarehouseSetupDetail).GetProperties();
                }
                else if (_objList == ObjectList.WireNumber)
                {
                    _result = typeof(WireNumber).GetProperties();
                }
                else if (_objList == ObjectList.WorkingContract)
                {
                    _result = typeof(WorkingContract).GetProperties();
                }
                else if (_objList == ObjectList.WorkingExperience)
                {
                    _result = typeof(WorkingExperience).GetProperties();
                }
                else if (_objList == ObjectList.WorkingStatus)
                {
                    _result = typeof(WorkingStatus).GetProperties();
                }
                else if (_objList == ObjectList.WorkOrder)
                {
                    _result = typeof(WorkOrder).GetProperties();
                }
                else if (_objList == ObjectList.WorkOrderLine)
                {
                    _result = typeof(WorkOrderLine).GetProperties();
                }
                else if (_objList == ObjectList.WorkOrderMaterial)
                {
                    _result = typeof(WorkOrderMaterial).GetProperties();
                }
                else if (_objList == ObjectList.WorkRequisition)
                {
                    _result = typeof(WorkRequisition).GetProperties();
                }
                else if (_objList == ObjectList.WorkRequisitionCollection)
                {
                    _result = typeof(WorkRequisitionCollection).GetProperties();
                }
                else if (_objList == ObjectList.WorkRequisitionCollectionLine)
                {
                    _result = typeof(WorkRequisitionCollectionLine).GetProperties();
                }
                else if (_objList == ObjectList.WorkRequisitionLine)
                {
                    _result = typeof(WorkRequisitionLine).GetProperties();
                }
                else if (_objList == ObjectList.Wrapping)
                {
                    _result = typeof(Wrapping).GetProperties();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        private object GetBusinessObject(Session _currSession, ObjectList _objList)
        {
            object _result = null;
            try
            {
                if (_objList == ObjectList.AccountGroup)
                {
                    _result = (AccountGroup)Activator.CreateInstance(typeof(AccountGroup), _currSession);
                }
                else if (_objList == ObjectList.AccountMap)
                {
                    _result = (AccountMap)Activator.CreateInstance(typeof(AccountMap), _currSession);
                }
                else if (_objList == ObjectList.AccountMapLine)
                {
                    _result = (AccountMapLine)Activator.CreateInstance(typeof(AccountMapLine), _currSession);
                }
                else if (_objList == ObjectList.AccountReclassJournal)
                {
                    _result = (AccountReclassJournal)Activator.CreateInstance(typeof(AccountReclassJournal), _currSession);
                }
                else if (_objList == ObjectList.AccountReclassJournalLine)
                {
                    _result = (AccountReclassJournalLine)Activator.CreateInstance(typeof(AccountReclassJournalLine), _currSession);
                }
                else if (_objList == ObjectList.AdditionalAnalyze)
                {
                    _result = (AdditionalAnalyze)Activator.CreateInstance(typeof(AdditionalAnalyze), _currSession);
                }
                else if (_objList == ObjectList.AdditionalDashboard)
                {
                    _result = (AdditionalDashboard)Activator.CreateInstance(typeof(AdditionalDashboard), _currSession);
                }
                else if (_objList == ObjectList.AdditionalReport)
                {
                    _result = (AdditionalReport)Activator.CreateInstance(typeof(AdditionalReport), _currSession);
                }
                else if (_objList == ObjectList.Address)
                {
                    _result = (Address)Activator.CreateInstance(typeof(Address), _currSession);
                }
                else if (_objList == ObjectList.ApplicationSetup)
                {
                    _result = (ApplicationSetup)Activator.CreateInstance(typeof(ApplicationSetup), _currSession);
                }
                else if (_objList == ObjectList.ApplicationSetupDetail)
                {
                    _result = (ApplicationSetupDetail)Activator.CreateInstance(typeof(ApplicationSetupDetail), _currSession);
                }
                else if (_objList == ObjectList.ApprovalLine)
                {
                    _result = (ApprovalLine)Activator.CreateInstance(typeof(ApprovalLine), _currSession);
                }
                else if (_objList == ObjectList.Armour)
                {
                    _result = (Armour)Activator.CreateInstance(typeof(Armour), _currSession);
                }
                else if (_objList == ObjectList.BankAccount)
                {
                    _result = (BankAccount)Activator.CreateInstance(typeof(BankAccount), _currSession);
                }
                else if (_objList == ObjectList.BankAccountGroup)
                {
                    _result = (BankAccountGroup)Activator.CreateInstance(typeof(BankAccountGroup), _currSession);
                }
                else if (_objList == ObjectList.BeginingInventory)
                {
                    _result = (BeginingInventory)Activator.CreateInstance(typeof(BeginingInventory), _currSession);
                }
                else if (_objList == ObjectList.BeginingInventoryLine)
                {
                    _result = (BeginingInventoryLine)Activator.CreateInstance(typeof(BeginingInventoryLine), _currSession);
                }
                else if (_objList == ObjectList.BillOfMaterial)
                {
                    _result = (BillOfMaterial)Activator.CreateInstance(typeof(BillOfMaterial), _currSession);
                }
                else if (_objList == ObjectList.BillOfMaterialLine)
                {
                    _result = (BillOfMaterialLine)Activator.CreateInstance(typeof(BillOfMaterialLine), _currSession);
                }
                else if (_objList == ObjectList.BillOfMaterialVersion)
                {
                    _result = (BillOfMaterialVersion)Activator.CreateInstance(typeof(BillOfMaterialVersion), _currSession);
                }
                else if (_objList == ObjectList.BinLocation)
                {
                    _result = (BinLocation)Activator.CreateInstance(typeof(BinLocation), _currSession);
                }
                else if (_objList == ObjectList.Branch)
                {
                    _result = (Branch)Activator.CreateInstance(typeof(Branch), _currSession);
                }
                else if (_objList == ObjectList.Brand)
                {
                    _result = (Brand)Activator.CreateInstance(typeof(Brand), _currSession);
                }
                else if (_objList == ObjectList.BusinessPartner)
                {
                    _result = (BusinessPartner)Activator.CreateInstance(typeof(BusinessPartner), _currSession);
                }
                else if (_objList == ObjectList.BusinessPartnerAccountGroup)
                {
                    _result = (BusinessPartnerAccountGroup)Activator.CreateInstance(typeof(BusinessPartnerAccountGroup), _currSession);
                }
                else if (_objList == ObjectList.BusinessPartnerGroup)
                {
                    _result = (BusinessPartnerGroup)Activator.CreateInstance(typeof(BusinessPartnerGroup), _currSession);
                }
                else if (_objList == ObjectList.CashAdvance)
                {
                    _result = (CashAdvance)Activator.CreateInstance(typeof(CashAdvance), _currSession);
                }
                else if (_objList == ObjectList.CashAdvanceLine)
                {
                    _result = (CashAdvanceLine)Activator.CreateInstance(typeof(CashAdvanceLine), _currSession);
                }
                else if (_objList == ObjectList.ChartOfAccount)
                {
                    _result = (ChartOfAccount)Activator.CreateInstance(typeof(ChartOfAccount), _currSession);
                }
                else if (_objList == ObjectList.City)
                {
                    _result = (City)Activator.CreateInstance(typeof(City), _currSession);
                }
                else if (_objList == ObjectList.Color)
                {
                    _result = (Color)Activator.CreateInstance(typeof(Color), _currSession);
                }
                else if (_objList == ObjectList.Company)
                {
                    _result = (Company)Activator.CreateInstance(typeof(Company), _currSession);
                }
                else if (_objList == ObjectList.CompanyAccountGroup)
                {
                    _result = (CompanyAccountGroup)Activator.CreateInstance(typeof(CompanyAccountGroup), _currSession);
                }
                else if (_objList == ObjectList.Conductor)
                {
                    _result = (Conductor)Activator.CreateInstance(typeof(Conductor), _currSession);
                }
                else if (_objList == ObjectList.Consumption)
                {
                    _result = (Consumption)Activator.CreateInstance(typeof(Consumption), _currSession);
                }
                else if (_objList == ObjectList.ConsumptionJournal)
                {
                    _result = (ConsumptionJournal)Activator.CreateInstance(typeof(ConsumptionJournal), _currSession);
                }
                else if (_objList == ObjectList.ConsumptionLine)
                {
                    _result = (ConsumptionLine)Activator.CreateInstance(typeof(ConsumptionLine), _currSession);
                }
                else if (_objList == ObjectList.CoreNumber)
                {
                    _result = (CoreNumber)Activator.CreateInstance(typeof(CoreNumber), _currSession);
                }
                else if (_objList == ObjectList.CostOfGoodsSold)
                {
                    _result = (CostOfGoodsSold)Activator.CreateInstance(typeof(CostOfGoodsSold), _currSession);
                }
                else if (_objList == ObjectList.Country)
                {
                    _result = (Country)Activator.CreateInstance(typeof(Country), _currSession);
                }
                else if (_objList == ObjectList.CreditMemo)
                {
                    _result = (CreditMemo)Activator.CreateInstance(typeof(CreditMemo), _currSession);
                }
                else if (_objList == ObjectList.CreditMemoCollection)
                {
                    _result = (CreditMemoCollection)Activator.CreateInstance(typeof(CreditMemoCollection), _currSession);
                }
                else if (_objList == ObjectList.CreditMemoLine)
                {
                    _result = (CreditMemoLine)Activator.CreateInstance(typeof(CreditMemoLine), _currSession);
                }
                else if (_objList == ObjectList.CrossSectionArea)
                {
                    _result = (CrossSectionArea)Activator.CreateInstance(typeof(CrossSectionArea), _currSession);
                }
                else if (_objList == ObjectList.Currency)
                {
                    _result = (Currency)Activator.CreateInstance(typeof(Currency), _currSession);
                }
                else if (_objList == ObjectList.CurrencyRate)
                {
                    _result = (CurrencyRate)Activator.CreateInstance(typeof(CurrencyRate), _currSession);
                }
                else if (_objList == ObjectList.CuttingProcess)
                {
                    _result = (CuttingProcess)Activator.CreateInstance(typeof(CuttingProcess), _currSession);
                }
                else if (_objList == ObjectList.CuttingProcessLine)
                {
                    _result = (CuttingProcessLine)Activator.CreateInstance(typeof(CuttingProcessLine), _currSession);
                }
                else if (_objList == ObjectList.CuttingProcessLot)
                {
                    _result = (CuttingProcessLot)Activator.CreateInstance(typeof(CuttingProcessLot), _currSession);
                }
                else if (_objList == ObjectList.DebitMemo)
                {
                    _result = (DebitMemo)Activator.CreateInstance(typeof(DebitMemo), _currSession);
                }
                else if (_objList == ObjectList.DebitMemoCollection)
                {
                    _result = (DebitMemoCollection)Activator.CreateInstance(typeof(DebitMemoCollection), _currSession);
                }
                else if (_objList == ObjectList.DebitMemoLine)
                {
                    _result = (DebitMemoLine)Activator.CreateInstance(typeof(DebitMemoLine), _currSession);
                }
                else if (_objList == ObjectList.Department)
                {
                    _result = (Department)Activator.CreateInstance(typeof(Department), _currSession);
                }
                else if (_objList == ObjectList.DepartmentAccountGroup)
                {
                    _result = (DepartmentAccountGroup)Activator.CreateInstance(typeof(DepartmentAccountGroup), _currSession);
                }
                else if (_objList == ObjectList.DesignNumber)
                {
                    _result = (DesignNumber)Activator.CreateInstance(typeof(DesignNumber), _currSession);
                }
                else if (_objList == ObjectList.DesignProperty)
                {
                    _result = (DesignProperty)Activator.CreateInstance(typeof(DesignProperty), _currSession);
                }
                else if (_objList == ObjectList.DesignSheet)
                {
                    _result = (DesignSheet)Activator.CreateInstance(typeof(DesignSheet), _currSession);
                }
                else if (_objList == ObjectList.DesignSheetComponentHeader)
                {
                    _result = (DesignSheetComponentHeader)Activator.CreateInstance(typeof(DesignSheetComponentHeader), _currSession);
                }
                else if (_objList == ObjectList.DesignSheetComponentLine)
                {
                    _result = (DesignSheetComponentLine)Activator.CreateInstance(typeof(DesignSheetComponentLine), _currSession);
                }
                else if (_objList == ObjectList.DesignSheetLine)
                {
                    _result = (DesignSheetLine)Activator.CreateInstance(typeof(DesignSheetLine), _currSession);
                }
                else if (_objList == ObjectList.Discount)
                {
                    _result = (Discount)Activator.CreateInstance(typeof(Discount), _currSession);
                }
                else if (_objList == ObjectList.DiscountAccountGroup)
                {
                    _result = (DiscountAccountGroup)Activator.CreateInstance(typeof(DiscountAccountGroup), _currSession);
                }
                else if (_objList == ObjectList.DiscountLine)
                {
                    _result = (DiscountLine)Activator.CreateInstance(typeof(DiscountLine), _currSession);
                }
                else if (_objList == ObjectList.Division)
                {
                    _result = (Division)Activator.CreateInstance(typeof(Division), _currSession);
                }
                else if (_objList == ObjectList.DocumentType)
                {
                    _result = (DocumentType)Activator.CreateInstance(typeof(DocumentType), _currSession);
                }
                else if (_objList == ObjectList.EDocument)
                {
                    _result = (EDocument)Activator.CreateInstance(typeof(EDocument), _currSession);
                }
                else if (_objList == ObjectList.Education)
                {
                    _result = (Education)Activator.CreateInstance(typeof(Education), _currSession);
                }
                else if (_objList == ObjectList.EducationDegree)
                {
                    _result = (EducationDegree)Activator.CreateInstance(typeof(EducationDegree), _currSession);
                }
                else if (_objList == ObjectList.EducationDepartment)
                {
                    _result = (EducationDepartment)Activator.CreateInstance(typeof(EducationDepartment), _currSession);
                }
                else if (_objList == ObjectList.Employee)
                {
                    _result = (Employee)Activator.CreateInstance(typeof(Employee), _currSession);
                }
                else if (_objList == ObjectList.ExchangeRate)
                {
                    _result = (ExchangeRate)Activator.CreateInstance(typeof(ExchangeRate), _currSession);
                }
                else if (_objList == ObjectList.Filler)
                {
                    _result = (Filler)Activator.CreateInstance(typeof(Filler), _currSession);
                }
                else if (_objList == ObjectList.FinishProduction)
                {
                    _result = (FinishProduction)Activator.CreateInstance(typeof(FinishProduction), _currSession);
                }
                else if (_objList == ObjectList.FinishProductionJournal)
                {
                    _result = (FinishProductionJournal)Activator.CreateInstance(typeof(FinishProductionJournal), _currSession);
                }
                else if (_objList == ObjectList.FinishProductionLine)
                {
                    _result = (FinishProductionLine)Activator.CreateInstance(typeof(FinishProductionLine), _currSession);
                }
                else if (_objList == ObjectList.GenderType)
                {
                    _result = (GenderType)Activator.CreateInstance(typeof(GenderType), _currSession);
                }
                else if (_objList == ObjectList.GeneralJournal)
                {
                    _result = (GeneralJournal)Activator.CreateInstance(typeof(GeneralJournal), _currSession);
                }
                else if (_objList == ObjectList.Grade)
                {
                    _result = (Grade)Activator.CreateInstance(typeof(Grade), _currSession);
                }
                else if (_objList == ObjectList.Grease)
                {
                    _result = (Grease)Activator.CreateInstance(typeof(Grease), _currSession);
                }
                else if (_objList == ObjectList.Identity)
                {
                    _result = (Identity)Activator.CreateInstance(typeof(Identity), _currSession);
                }
                else if (_objList == ObjectList.IdentityType)
                {
                    _result = (IdentityType)Activator.CreateInstance(typeof(IdentityType), _currSession);
                }
                else if (_objList == ObjectList.IndustrialType)
                {
                    _result = (IndustrialType)Activator.CreateInstance(typeof(IndustrialType), _currSession);
                }
                else if (_objList == ObjectList.Insulation)
                {
                    _result = (Insulation)Activator.CreateInstance(typeof(Insulation), _currSession);
                }
                else if (_objList == ObjectList.Insurance)
                {
                    _result = (Insurance)Activator.CreateInstance(typeof(Insurance), _currSession);
                }
                else if (_objList == ObjectList.InsuranceLine)
                {
                    _result = (InsuranceLine)Activator.CreateInstance(typeof(InsuranceLine), _currSession);
                }
                else if (_objList == ObjectList.InventoryJournal)
                {
                    _result = (InventoryJournal)Activator.CreateInstance(typeof(InventoryJournal), _currSession);
                }
                else if (_objList == ObjectList.InventoryProduction)
                {
                    _result = (InventoryProduction)Activator.CreateInstance(typeof(InventoryProduction), _currSession);
                }
                else if (_objList == ObjectList.InventoryProductionLine)
                {
                    _result = (InventoryProductionLine)Activator.CreateInstance(typeof(InventoryProductionLine), _currSession);
                }
                else if (_objList == ObjectList.InventorySalesCollection)
                {
                    _result = (InventorySalesCollection)Activator.CreateInstance(typeof(InventorySalesCollection), _currSession);
                }
                else if (_objList == ObjectList.InventoryTransferIn)
                {
                    _result = (InventoryTransferIn)Activator.CreateInstance(typeof(InventoryTransferIn), _currSession);
                }
                else if (_objList == ObjectList.InventoryTransferInLine)
                {
                    _result = (InventoryTransferInLine)Activator.CreateInstance(typeof(InventoryTransferInLine), _currSession);
                }
                else if (_objList == ObjectList.InventoryTransferInLot)
                {
                    _result = (InventoryTransferInLot)Activator.CreateInstance(typeof(InventoryTransferInLot), _currSession);
                }
                else if (_objList == ObjectList.InventoryTransferOrder)
                {
                    _result = (InventoryTransferOrder)Activator.CreateInstance(typeof(InventoryTransferOrder), _currSession);
                }
                else if (_objList == ObjectList.InventoryTransferOrderLine)
                {
                    _result = (InventoryTransferOrderLine)Activator.CreateInstance(typeof(InventoryTransferOrderLine), _currSession);
                }
                else if (_objList == ObjectList.InventoryTransferOrderLot)
                {
                    _result = (InventoryTransferOrderLot)Activator.CreateInstance(typeof(InventoryTransferOrderLot), _currSession);
                }
                else if (_objList == ObjectList.InventoryTransferOrderMonitoring)
                {
                    _result = (InventoryTransferOrderMonitoring)Activator.CreateInstance(typeof(InventoryTransferOrderMonitoring), _currSession);
                }
                else if (_objList == ObjectList.InventoryTransferOut)
                {
                    _result = (InventoryTransferOut)Activator.CreateInstance(typeof(InventoryTransferOut), _currSession);
                }
                else if (_objList == ObjectList.InventoryTransferOutLine)
                {
                    _result = (InventoryTransferOutLine)Activator.CreateInstance(typeof(InventoryTransferOutLine), _currSession);
                }
                else if (_objList == ObjectList.InventoryTransferOutLot)
                {
                    _result = (InventoryTransferOutLot)Activator.CreateInstance(typeof(InventoryTransferOutLot), _currSession);
                }
                else if (_objList == ObjectList.InventoryTransferOutMonitoring)
                {
                    _result = (InventoryTransferOutMonitoring)Activator.CreateInstance(typeof(InventoryTransferOutMonitoring), _currSession);
                }
                else if (_objList == ObjectList.Item)
                {
                    _result = (Item)Activator.CreateInstance(typeof(Item), _currSession);
                }
                else if (_objList == ObjectList.ItemAccountGroup)
                {
                    _result = (ItemAccountGroup)Activator.CreateInstance(typeof(ItemAccountGroup), _currSession);
                }
                else if (_objList == ObjectList.ItemBillOfMaterial)
                {
                    _result = (ItemBillOfMaterial)Activator.CreateInstance(typeof(ItemBillOfMaterial), _currSession);
                }
                else if (_objList == ObjectList.ItemComponent)
                {
                    _result = (ItemComponent)Activator.CreateInstance(typeof(ItemComponent), _currSession);
                }
                else if (_objList == ObjectList.ItemConsumption)
                {
                    _result = (ItemConsumption)Activator.CreateInstance(typeof(ApplicationSetup), _currSession);
                }
                else if (_objList == ObjectList.ItemConsumptionLine)
                {
                    _result = (ItemConsumptionLine)Activator.CreateInstance(typeof(ApplicationSetup), _currSession);
                }
                else if (_objList == ObjectList.ItemGroup)
                {
                    _result = (ItemGroup)Activator.CreateInstance(typeof(ItemGroup), _currSession);
                }
                else if (_objList == ObjectList.ItemReclassJournal)
                {
                    _result = (ItemReclassJournal)Activator.CreateInstance(typeof(ItemReclassJournal), _currSession);
                }
                else if (_objList == ObjectList.ItemReclassJournalLine)
                {
                    _result = (ItemReclassJournalLine)Activator.CreateInstance(typeof(ItemReclassJournalLine), _currSession);
                }
                else if (_objList == ObjectList.ItemType)
                {
                    _result = (ItemType)Activator.CreateInstance(typeof(ItemType), _currSession);
                }
                else if (_objList == ObjectList.ItemUnitOfMeasure)
                {
                    _result = (ItemUnitOfMeasure)Activator.CreateInstance(typeof(ItemUnitOfMeasure), _currSession);
                }
                else if (_objList == ObjectList.JournalMap)
                {
                    _result = (JournalMap)Activator.CreateInstance(typeof(JournalMap), _currSession);
                }
                else if (_objList == ObjectList.JournalMapLine)
                {
                    _result = (JournalMapLine)Activator.CreateInstance(typeof(JournalMapLine), _currSession);
                }
                else if (_objList == ObjectList.ListImport)
                {
                    _result = (ListImport)Activator.CreateInstance(typeof(ListImport), _currSession);
                }
                else if (_objList == ObjectList.LoanApproval)
                {
                    _result = (LoanApproval)Activator.CreateInstance(typeof(LoanApproval), _currSession);
                }
                else if (_objList == ObjectList.LoanProcess)
                {
                    _result = (LoanProcess)Activator.CreateInstance(typeof(LoanProcess), _currSession);
                }
                else if (_objList == ObjectList.LoanProposal)
                {
                    _result = (LoanProposal)Activator.CreateInstance(typeof(LoanProposal), _currSession);
                }
                else if (_objList == ObjectList.LoanStatus)
                {
                    _result = (LoanStatus)Activator.CreateInstance(typeof(LoanStatus), _currSession);
                }
                else if (_objList == ObjectList.LoanType)
                {
                    _result = (LoanType)Activator.CreateInstance(typeof(LoanType), _currSession);
                }
                else if (_objList == ObjectList.Location)
                {
                    _result = (Location)Activator.CreateInstance(typeof(Location), _currSession);
                }
                else if (_objList == ObjectList.Machine)
                {
                    _result = (Machine)Activator.CreateInstance(typeof(Machine), _currSession);
                }
                else if (_objList == ObjectList.MachineMapLine)
                {
                    _result = (MachineMapLine)Activator.CreateInstance(typeof(MachineMapLine), _currSession);
                }
                else if (_objList == ObjectList.MachineMapVersion)
                {
                    _result = (MachineMapVersion)Activator.CreateInstance(typeof(MachineMapVersion), _currSession);
                }
                else if (_objList == ObjectList.MailSetupDetail)
                {
                    _result = (MailSetupDetail)Activator.CreateInstance(typeof(MailSetupDetail), _currSession);
                }
                else if (_objList == ObjectList.Material)
                {
                    _result = (Material)Activator.CreateInstance(typeof(Material), _currSession);
                }
                else if (_objList == ObjectList.MaterialPrice)
                {
                    _result = (MaterialPrice)Activator.CreateInstance(typeof(MaterialPrice), _currSession);
                }
                else if (_objList == ObjectList.MaterialRequisition)
                {
                    _result = (MaterialRequisition)Activator.CreateInstance(typeof(MaterialRequisition), _currSession);
                }
                else if (_objList == ObjectList.MaterialRequisitionLine)
                {
                    _result = (MaterialRequisitionLine)Activator.CreateInstance(typeof(MaterialRequisitionLine), _currSession);
                }
                else if (_objList == ObjectList.NonTaxableIncome)
                {
                    _result = (NonTaxableIncomeType)Activator.CreateInstance(typeof(NonTaxableIncomeType), _currSession);
                }
                else if (_objList == ObjectList.NumberingHeader)
                {
                    _result = (NumberingHeader)Activator.CreateInstance(typeof(NumberingHeader), _currSession);
                }
                else if (_objList == ObjectList.NumberingLine)
                {
                    _result = (NumberingLine)Activator.CreateInstance(typeof(NumberingLine), _currSession);
                }
                else if (_objList == ObjectList.OrganizationSetupDetail)
                {
                    _result = (OrganizationSetupDetail)Activator.CreateInstance(typeof(OrganizationSetupDetail), _currSession);
                }
                else if (_objList == ObjectList.OuterSheath)
                {
                    _result = (OuterSheath)Activator.CreateInstance(typeof(OuterSheath), _currSession);
                }
                else if (_objList == ObjectList.OutputProduction)
                {
                    _result = (OutputProduction)Activator.CreateInstance(typeof(OutputProduction), _currSession);
                }
                else if (_objList == ObjectList.OutputProductionJournal)
                {
                    _result = (OutputProductionJournal)Activator.CreateInstance(typeof(OutputProductionJournal), _currSession);
                }
                else if (_objList == ObjectList.OutputProductionLine)
                {
                    _result = (OutputProductionLine)Activator.CreateInstance(typeof(OutputProductionLine), _currSession);
                }
                else if (_objList == ObjectList.OverheadCost)
                {
                    _result = (OverheadCost)Activator.CreateInstance(typeof(OverheadCost), _currSession);
                }
                else if (_objList == ObjectList.OverheadCostDepreciation)
                {
                    _result = (OverheadCostDepreciation)Activator.CreateInstance(typeof(OverheadCostDepreciation), _currSession);
                }
                else if (_objList == ObjectList.OverheadCostElectric)
                {
                    _result = (OverheadCostElectric)Activator.CreateInstance(typeof(OverheadCostElectric), _currSession);
                }
                else if (_objList == ObjectList.OverheadCostEmployee)
                {
                    _result = (OverheadCostEmployee)Activator.CreateInstance(typeof(OverheadCostEmployee), _currSession);
                }
                else if (_objList == ObjectList.PayableTransaction)
                {
                    _result = (PayableTransaction)Activator.CreateInstance(typeof(PayableTransaction), _currSession);
                }
                else if (_objList == ObjectList.PayableTransactionLine)
                {
                    _result = (PayableTransactionLine)Activator.CreateInstance(typeof(PayableTransactionLine), _currSession);
                }
                else if (_objList == ObjectList.PaymentInPlan)
                {
                    _result = (PaymentInPlan)Activator.CreateInstance(typeof(PaymentInPlan), _currSession);
                }
                else if (_objList == ObjectList.PaymentMethod)
                {
                    _result = (PaymentMethod)Activator.CreateInstance(typeof(PaymentMethod), _currSession);
                }
                else if (_objList == ObjectList.PaymentOutPlan)
                {
                    _result = (PaymentOutPlan)Activator.CreateInstance(typeof(PaymentOutPlan), _currSession);
                }
                else if (_objList == ObjectList.PaymentRealization)
                {
                    _result = (PaymentRealization)Activator.CreateInstance(typeof(PaymentRealization), _currSession);
                }
                else if (_objList == ObjectList.PaymentRealizationLine)
                {
                    _result = (PaymentRealizationLine)Activator.CreateInstance(typeof(PaymentRealizationLine), _currSession);
                }
                else if (_objList == ObjectList.PayrollProcess)
                {
                    _result = (PayrollProcess)Activator.CreateInstance(typeof(PayrollProcess), _currSession);
                }
                else if (_objList == ObjectList.PayrollProcessDetail)
                {
                    _result = (PayrollProcessDetail)Activator.CreateInstance(typeof(PayrollProcessDetail), _currSession);
                }
                else if (_objList == ObjectList.Period)
                {
                    _result = (Period)Activator.CreateInstance(typeof(Period), _currSession);
                }
                else if (_objList == ObjectList.PhoneNumber)
                {
                    _result = (PhoneNumber)Activator.CreateInstance(typeof(PhoneNumber), _currSession);
                }
                else if (_objList == ObjectList.PhoneType)
                {
                    _result = (PhoneType)Activator.CreateInstance(typeof(PhoneType), _currSession);
                }
                else if (_objList == ObjectList.Position)
                {
                    _result = (Position)Activator.CreateInstance(typeof(Position), _currSession);
                }
                else if (_objList == ObjectList.PrePurchaseOrder)
                {
                    _result = (PrePurchaseOrder)Activator.CreateInstance(typeof(PrePurchaseOrder), _currSession);
                }
                else if (_objList == ObjectList.PrePurchaseOrder2)
                {
                    _result = (PrePurchaseOrder2)Activator.CreateInstance(typeof(PrePurchaseOrder2), _currSession);
                }
                else if (_objList == ObjectList.PreSalesOrder)
                {
                    _result = (PreSalesOrder)Activator.CreateInstance(typeof(PreSalesOrder), _currSession);
                }
                else if (_objList == ObjectList.PreSalesOrder2)
                {
                    _result = (PreSalesOrder2)Activator.CreateInstance(typeof(PreSalesOrder2), _currSession);
                }
                else if (_objList == ObjectList.Price)
                {
                    _result = (Price)Activator.CreateInstance(typeof(Price), _currSession);
                }
                else if (_objList == ObjectList.PriceGroup)
                {
                    _result = (PriceGroup)Activator.CreateInstance(typeof(PriceGroup), _currSession);
                }
                else if (_objList == ObjectList.PriceLine)
                {
                    _result = (PriceLine)Activator.CreateInstance(typeof(PriceLine), _currSession);
                }
                else if (_objList == ObjectList.ProductCode)
                {
                    _result = (ProductCode)Activator.CreateInstance(typeof(ProductCode), _currSession);
                }
                else if (_objList == ObjectList.ProductGroup)
                {
                    _result = (ProductGroup)Activator.CreateInstance(typeof(ProductGroup), _currSession);
                }
                else if (_objList == ObjectList.Production)
                {
                    _result = (Production)Activator.CreateInstance(typeof(Production), _currSession);
                }
                else if (_objList == ObjectList.ProductionLine)
                {
                    _result = (ProductionLine)Activator.CreateInstance(typeof(ProductionLine), _currSession);
                }
                else if (_objList == ObjectList.ProductionMaterial)
                {
                    _result = (ProductionMaterial)Activator.CreateInstance(typeof(ProductionMaterial), _currSession);
                }
                else if (_objList == ObjectList.ProductType)
                {
                    _result = (ProductType)Activator.CreateInstance(typeof(ProductType), _currSession);
                }
                else if (_objList == ObjectList.ProjectHeader)
                {
                    _result = (ProjectHeader)Activator.CreateInstance(typeof(ProjectHeader), _currSession);
                }
                else if (_objList == ObjectList.ProjectLine)
                {
                    _result = (ProjectLine)Activator.CreateInstance(typeof(ProjectLine), _currSession);
                }
                else if (_objList == ObjectList.ProjectLine2)
                {
                    _result = (ProjectLine2)Activator.CreateInstance(typeof(ProjectLine2), _currSession);
                }
                else if (_objList == ObjectList.ProjectLine2Item)
                {
                    _result = (ProjectLine2Item)Activator.CreateInstance(typeof(ProjectLine2Item), _currSession);
                }
                else if (_objList == ObjectList.ProjectLine2Item2)
                {
                    _result = (ProjectLine2Item2)Activator.CreateInstance(typeof(ProjectLine2Item2), _currSession);
                }
                else if (_objList == ObjectList.ProjectLine2Service)
                {
                    _result = (ProjectLine2Service)Activator.CreateInstance(typeof(ProjectLine2Service), _currSession);
                }
                else if (_objList == ObjectList.ProjectLineItem)
                {
                    _result = (ProjectLineItem)Activator.CreateInstance(typeof(ProjectLineItem), _currSession);
                }
                else if (_objList == ObjectList.ProjectLineItem2)
                {
                    _result = (ProjectLineItem2)Activator.CreateInstance(typeof(ProjectLineItem2), _currSession);
                }
                else if (_objList == ObjectList.ProjectLineService)
                {
                    _result = (ProjectLineService)Activator.CreateInstance(typeof(ProjectLineService), _currSession);
                }
                else if (_objList == ObjectList.PurchaseInvoice)
                {
                    _result = (PurchaseInvoice)Activator.CreateInstance(typeof(PurchaseInvoice), _currSession);
                }
                else if (_objList == ObjectList.PurchaseInvoiceCollection)
                {
                    _result = (PurchaseInvoiceCollection)Activator.CreateInstance(typeof(PurchaseInvoiceCollection), _currSession);
                }
                else if (_objList == ObjectList.PurchaseInvoiceCollectionLine)
                {
                    _result = (PurchaseInvoiceCollectionLine)Activator.CreateInstance(typeof(PurchaseInvoiceCollectionLine), _currSession);
                }
                else if (_objList == ObjectList.PurchaseInvoiceLine)
                {
                    _result = (PurchaseInvoiceLine)Activator.CreateInstance(typeof(PurchaseInvoiceLine), _currSession);
                }
                else if (_objList == ObjectList.PurchaseOrder)
                {
                    _result = (PurchaseOrder)Activator.CreateInstance(typeof(PurchaseOrder), _currSession);
                }
                else if (_objList == ObjectList.PurchaseOrderLine)
                {
                    _result = (PurchaseOrderLine)Activator.CreateInstance(typeof(PurchaseOrderLine), _currSession);
                }
                else if (_objList == ObjectList.PurchaseRequisition)
                {
                    _result = (PurchaseRequisition)Activator.CreateInstance(typeof(PurchaseRequisition), _currSession);
                }
                else if (_objList == ObjectList.PurchaseRequisitionCollection)
                {
                    _result = (PurchaseRequisitionCollection)Activator.CreateInstance(typeof(PurchaseRequisitionCollection), _currSession);
                }
                else if (_objList == ObjectList.PurchaseRequisitionLine)
                {
                    _result = (PurchaseRequisitionLine)Activator.CreateInstance(typeof(PurchaseRequisitionLine), _currSession);
                }
                else if (_objList == ObjectList.PurchaseReturn)
                {
                    _result = (PurchaseReturn)Activator.CreateInstance(typeof(PurchaseReturn), _currSession);
                }
                else if (_objList == ObjectList.PurchaseReturnLine)
                {
                    _result = (PurchaseReturnLine)Activator.CreateInstance(typeof(PurchaseReturnLine), _currSession);
                }
                else if (_objList == ObjectList.PurchaseSetupDetail)
                {
                    _result = (PurchaseSetupDetail)Activator.CreateInstance(typeof(PurchaseSetupDetail), _currSession);
                }
                else if (_objList == ObjectList.RatedVoltage)
                {
                    _result = (RatedVoltage)Activator.CreateInstance(typeof(RatedVoltage), _currSession);
                }
                else if (_objList == ObjectList.ReceivableTransaction)
                {
                    _result = (ReceivableTransaction)Activator.CreateInstance(typeof(ReceivableTransaction), _currSession);
                }
                else if (_objList == ObjectList.ReceivableTransactionLine)
                {
                    _result = (ReceivableTransactionLine)Activator.CreateInstance(typeof(ReceivableTransactionLine), _currSession);
                }
                else if (_objList == ObjectList.Religion)
                {
                    _result = (Religion)Activator.CreateInstance(typeof(Religion), _currSession);
                }
                else if (_objList == ObjectList.RoundingSetupDetail)
                {
                    _result = (RoundingSetupDetail)Activator.CreateInstance(typeof(RoundingSetupDetail), _currSession);
                }
                else if (_objList == ObjectList.SalaryBasic)
                {
                    _result = (SalaryBasic)Activator.CreateInstance(typeof(SalaryBasic), _currSession);
                }
                else if (_objList == ObjectList.SalaryComponent)
                {
                    _result = (SalaryComponent)Activator.CreateInstance(typeof(SalaryComponent), _currSession);
                }
                else if (_objList == ObjectList.SalaryGroup)
                {
                    _result = (SalaryGroup)Activator.CreateInstance(typeof(SalaryGroup), _currSession);
                }
                else if (_objList == ObjectList.SalaryGroupDetail)
                {
                    _result = (SalaryGroupDetail)Activator.CreateInstance(typeof(SalaryGroupDetail), _currSession);
                }
                else if (_objList == ObjectList.SalesInvoice)
                {
                    _result = (SalesInvoice)Activator.CreateInstance(typeof(SalesInvoice), _currSession);
                }
                else if (_objList == ObjectList.SalesInvoiceCollection)
                {
                    _result = (SalesInvoiceCollection)Activator.CreateInstance(typeof(SalesInvoiceCollection), _currSession);
                }
                else if (_objList == ObjectList.SalesInvoiceCollectionLine)
                {
                    _result = (SalesInvoiceCollectionLine)Activator.CreateInstance(typeof(SalesInvoiceCollectionLine), _currSession);
                }
                else if (_objList == ObjectList.SalesInvoiceLine)
                {
                    _result = (SalesInvoiceLine)Activator.CreateInstance(typeof(SalesInvoiceLine), _currSession);
                }
                else if (_objList == ObjectList.SalesOrder)
                {
                    _result = (SalesOrder)Activator.CreateInstance(typeof(SalesOrder), _currSession);
                }
                else if (_objList == ObjectList.SalesOrderLine)
                {
                    _result = (SalesOrderLine)Activator.CreateInstance(typeof(SalesOrderLine), _currSession);
                }
                else if (_objList == ObjectList.SalesOrderMonitoring)
                {
                    _result = (SalesOrderMonitoring)Activator.CreateInstance(typeof(SalesOrderMonitoring), _currSession);
                }
                else if (_objList == ObjectList.SalesQuotation)
                {
                    _result = (SalesQuotation)Activator.CreateInstance(typeof(SalesQuotation), _currSession);
                }
                else if (_objList == ObjectList.SalesQuotationCollection)
                {
                    _result = (SalesQuotationCollection)Activator.CreateInstance(typeof(SalesQuotationCollection), _currSession);
                }
                else if (_objList == ObjectList.SalesQuotationLine)
                {
                    _result = (SalesQuotationLine)Activator.CreateInstance(typeof(SalesQuotationLine), _currSession);
                }
                else if (_objList == ObjectList.SalesQuotationMonitoring)
                {
                    _result = (SalesQuotationMonitoring)Activator.CreateInstance(typeof(SalesQuotationMonitoring), _currSession);
                }
                else if (_objList == ObjectList.SalesReturn)
                {
                    _result = (SalesReturn)Activator.CreateInstance(typeof(SalesReturn), _currSession);
                }
                else if (_objList == ObjectList.SalesReturnLine)
                {
                    _result = (SalesReturnLine)Activator.CreateInstance(typeof(SalesReturnLine), _currSession);
                }
                else if (_objList == ObjectList.SalesSetupDetail)
                {
                    _result = (SalesSetupDetail)Activator.CreateInstance(typeof(SalesSetupDetail), _currSession);
                }
                else if (_objList == ObjectList.School)
                {
                    _result = (School)Activator.CreateInstance(typeof(School), _currSession);
                }
                else if (_objList == ObjectList.Screening)
                {
                    _result = (Screening)Activator.CreateInstance(typeof(Screening), _currSession);
                }
                else if (_objList == ObjectList.Section)
                {
                    _result = (Section)Activator.CreateInstance(typeof(Section), _currSession);
                }
                else if (_objList == ObjectList.Separator)
                {
                    _result = (Separator)Activator.CreateInstance(typeof(Separator), _currSession);
                }
                else if (_objList == ObjectList.ShapeOfConductor)
                {
                    _result = (ShapeOfConductor)Activator.CreateInstance(typeof(ShapeOfConductor), _currSession);
                }
                else if (_objList == ObjectList.Speciality)
                {
                    _result = (Speciality)Activator.CreateInstance(typeof(Speciality), _currSession);
                }
                else if (_objList == ObjectList.SpecialityMaster)
                {
                    _result = (SpecialityMaster)Activator.CreateInstance(typeof(SpecialityMaster), _currSession);
                }
                else if (_objList == ObjectList.Standart)
                {
                    _result = (Standart)Activator.CreateInstance(typeof(Standart), _currSession);
                }
                else if (_objList == ObjectList.StockTransfer)
                {
                    _result = (StockTransfer)Activator.CreateInstance(typeof(StockTransfer), _currSession);
                }
                else if (_objList == ObjectList.StockTransferLine)
                {
                    _result = (StockTransferLine)Activator.CreateInstance(typeof(StockTransferLine), _currSession);
                }         
                else if (_objList == ObjectList.SubBrand)
                {
                    _result = (SubBrand)Activator.CreateInstance(typeof(SubBrand), _currSession);
                }
                else if (_objList == ObjectList.SubProductGroup)
                {
                    _result = (SubProductGroup)Activator.CreateInstance(typeof(SubProductGroup), _currSession);
                }
                else if (_objList == ObjectList.Tax)
                {
                    _result = (Tax)Activator.CreateInstance(typeof(Tax), _currSession);
                }
                else if (_objList == ObjectList.TaxAccountGroup)
                {
                    _result = (TaxAccountGroup)Activator.CreateInstance(typeof(TaxAccountGroup), _currSession);
                }
                else if (_objList == ObjectList.TaxGroup)
                {
                    _result = (TaxGroup)Activator.CreateInstance(typeof(TaxGroup), _currSession);
                }
                else if (_objList == ObjectList.TaxLine)
                {
                    _result = (TaxLine)Activator.CreateInstance(typeof(TaxLine), _currSession);
                }
                else if (_objList == ObjectList.TaxProcess)
                {
                    _result = (TaxProcess)Activator.CreateInstance(typeof(TaxProcess), _currSession);
                }
                else if (_objList == ObjectList.TaxRateNonNPWP)
                {
                    _result = (TaxRateNonNPWP)Activator.CreateInstance(typeof(TaxRateNonNPWP), _currSession);
                }
                else if (_objList == ObjectList.TaxRateNPWP)
                {
                    _result = (TaxRateNPWP)Activator.CreateInstance(typeof(TaxRateNPWP), _currSession);
                }
                else if (_objList == ObjectList.TaxSetup)
                {
                    _result = (TaxSetup)Activator.CreateInstance(typeof(TaxSetup), _currSession);
                }
                else if (_objList == ObjectList.TaxType)
                {
                    _result = (TaxType)Activator.CreateInstance(typeof(TaxType), _currSession);
                }
                else if (_objList == ObjectList.TempBeginingInventoryLine)
                {
                    _result = (TempBeginingInventoryLine)Activator.CreateInstance(typeof(TempBeginingInventoryLine), _currSession);
                }
                else if (_objList == ObjectList.TermOfPayment)
                {
                    _result = (TermOfPayment)Activator.CreateInstance(typeof(TermOfPayment), _currSession);
                }
                else if (_objList == ObjectList.Training)
                {
                    _result = (Training)Activator.CreateInstance(typeof(Training), _currSession);
                }
                else if (_objList == ObjectList.TrainingMaster)
                {
                    _result = (TrainingMaster)Activator.CreateInstance(typeof(TrainingMaster), _currSession);
                }
                else if (_objList == ObjectList.TransferIn)
                {
                    _result = (TransferIn)Activator.CreateInstance(typeof(TransferIn), _currSession);
                }
                else if (_objList == ObjectList.TransferInLine)
                {
                    _result = (TransferInLine)Activator.CreateInstance(typeof(TransferInLine), _currSession);
                }
                else if (_objList == ObjectList.TransferInLot)
                {
                    _result = (TransferInLot)Activator.CreateInstance(typeof(TransferInLot), _currSession);
                }
                else if (_objList == ObjectList.TransferOrder)
                {
                    _result = (TransferOrder)Activator.CreateInstance(typeof(TransferOrder), _currSession);
                }
                else if (_objList == ObjectList.TransferOrderLine)
                {
                    _result = (TransferOrderLine)Activator.CreateInstance(typeof(TransferOrderLine), _currSession);
                }
                else if (_objList == ObjectList.TransferOrderLot)
                {
                    _result = (TransferOrderLot)Activator.CreateInstance(typeof(TransferOrderLot), _currSession);
                }
                else if (_objList == ObjectList.TransferOut)
                {
                    _result = (TransferOut)Activator.CreateInstance(typeof(TransferOut), _currSession);
                }
                else if (_objList == ObjectList.TransferOutLine)
                {
                    _result = (TransferOutLine)Activator.CreateInstance(typeof(TransferOutLine), _currSession);
                }
                else if (_objList == ObjectList.TransferOutLot)
                {
                    _result = (TransferOutLot)Activator.CreateInstance(typeof(TransferOutLot), _currSession);
                }
                else if (_objList == ObjectList.UnitOfMeasure)
                {
                    _result = (UnitOfMeasure)Activator.CreateInstance(typeof(UnitOfMeasure), _currSession);
                }
                else if (_objList == ObjectList.UnitOfMeasureType)
                {
                    _result = (UnitOfMeasureType)Activator.CreateInstance(typeof(UnitOfMeasureType), _currSession);
                }
                else if (_objList == ObjectList.UserAccess)
                {
                    _result = (UserAccess)Activator.CreateInstance(typeof(UserAccess), _currSession);
                }
                else if (_objList == ObjectList.UserAccessRole)
                {
                    _result = (UserAccessRole)Activator.CreateInstance(typeof(UserAccessRole), _currSession);
                }
                else if (_objList == ObjectList.WarehouseSetupDetail)
                {
                    _result = (WarehouseSetupDetail)Activator.CreateInstance(typeof(WarehouseSetupDetail), _currSession);
                }
                else if (_objList == ObjectList.WireNumber)
                {
                    _result = (WireNumber)Activator.CreateInstance(typeof(WireNumber), _currSession);
                }
                else if (_objList == ObjectList.WorkingContract)
                {
                    _result = (WorkingContract)Activator.CreateInstance(typeof(WorkingContract), _currSession);
                }
                else if (_objList == ObjectList.WorkingExperience)
                {
                    _result = (WorkingExperience)Activator.CreateInstance(typeof(WorkingExperience), _currSession);
                }
                else if (_objList == ObjectList.WorkingStatus)
                {
                    _result = (WorkingStatus)Activator.CreateInstance(typeof(WorkingStatus), _currSession);
                }
                else if (_objList == ObjectList.WorkOrder)
                {
                    _result = (WorkOrder)Activator.CreateInstance(typeof(WorkOrder), _currSession);
                }
                else if (_objList == ObjectList.WorkOrderLine)
                {
                    _result = (WorkOrderLine)Activator.CreateInstance(typeof(WorkOrderLine), _currSession);
                }
                else if (_objList == ObjectList.WorkOrderMaterial)
                {
                    _result = (WorkOrderMaterial)Activator.CreateInstance(typeof(WorkOrderMaterial), _currSession);
                }
                else if (_objList == ObjectList.WorkRequisition)
                {
                    _result = (WorkRequisition)Activator.CreateInstance(typeof(WorkRequisition), _currSession);
                }
                else if (_objList == ObjectList.WorkRequisitionCollection)
                {
                    _result = (WorkRequisitionCollection)Activator.CreateInstance(typeof(WorkRequisitionCollection), _currSession);
                }
                else if (_objList == ObjectList.WorkRequisitionCollectionLine)
                {
                    _result = (WorkRequisitionCollectionLine)Activator.CreateInstance(typeof(WorkRequisitionCollectionLine), _currSession);
                }
                else if (_objList == ObjectList.WorkRequisitionLine)
                {
                    _result = (WorkRequisitionLine)Activator.CreateInstance(typeof(WorkRequisitionLine), _currSession);
                }
                else if (_objList == ObjectList.Wrapping)
                {
                    _result = (Wrapping)Activator.CreateInstance(typeof(Wrapping), _currSession);
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        #region Function For Enum

        public int GetObjectList(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "AccountGroup".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "AccountMap".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "AccountMapLine".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "AccountReclassJournal".ToLower())
                    {
                        _result = 4;
                    }
                    else if (_locObjectName == "AccountReclassJournalLine".ToLower())
                    {
                        _result = 5;
                    }
                    else if (_locObjectName == "AdditionalAnalyze".ToLower())
                    {
                        _result = 6;
                    }
                    else if (_locObjectName == "AdditionalDashboard".ToLower())
                    {
                        _result = 7;
                    }
                    else if (_locObjectName == "AdditionalReport".ToLower())
                    {
                        _result = 8;
                    }
                    else if (_locObjectName == "Address".ToLower())
                    {
                        _result = 9;
                    }
                    else if (_locObjectName == "ApplicationImport".ToLower())
                    {
                        _result = 10;
                    }
                    else if (_locObjectName == "ApplicationSetup".ToLower())
                    {
                        _result = 11;
                    }
                    else if (_locObjectName == "ApplicationSetupDetail".ToLower())
                    {
                        _result = 12;
                    }
                    else if (_locObjectName == "ApprovalLine".ToLower())
                    {
                        _result = 13;
                    }
                    else if (_locObjectName == "Armour".ToLower())
                    {
                        _result = 14;
                    }
                    else if (_locObjectName == "BankAccount".ToLower())
                    {
                        _result = 15;
                    }
                    else if (_locObjectName == "BankAccountGroup".ToLower())
                    {
                        _result = 16;
                    }
                    else if (_locObjectName == "BeginingInventory".ToLower())
                    {
                        _result = 17;
                    }
                    else if (_locObjectName == "BeginingInventoryLine".ToLower())
                    {
                        _result = 18;
                    }
                    else if (_locObjectName == "BillOfMaterial".ToLower())
                    {
                        _result = 19;
                    }
                    else if (_locObjectName == "BillOfMaterialLine".ToLower())
                    {
                        _result = 20;
                    }
                    else if (_locObjectName == "BillOfMaterialVersion".ToLower())
                    {
                        _result = 21;
                    }   
                    else if (_locObjectName == "BinLocation".ToLower())
                    {
                        _result = 22;
                    }
                    else if (_locObjectName == "Branch".ToLower())
                    {
                        _result = 23;
                    }
                    else if (_locObjectName == "Brand".ToLower())
                    {
                        _result = 24;
                    }
                    else if (_locObjectName == "BusinessPartner".ToLower())
                    {
                        _result = 25;
                    }
                    else if (_locObjectName == "BusinessPartnerAccountGroup".ToLower())
                    {
                        _result = 26;
                    }
                    else if (_locObjectName == "BusinessPartnerGroup".ToLower())
                    {
                        _result = 27;
                    }
                    else if (_locObjectName == "CashAdvance".ToLower())
                    {
                        _result = 28;
                    }
                    else if (_locObjectName == "CashAdvanceLine".ToLower())
                    {
                        _result = 29;
                    }                       
                    else if (_locObjectName == "ChartOfAccount".ToLower())
                    {
                        _result = 30;
                    }
                    else if (_locObjectName == "City".ToLower())
                    {
                        _result = 31;
                    }
                    else if (_locObjectName == "Color".ToLower())
                    {
                        _result = 32;
                    }
                    else if (_locObjectName == "Company".ToLower())
                    {
                        _result = 33;
                    }
                    else if (_locObjectName == "CompanyAccountGroup".ToLower())
                    {
                        _result = 34;
                    }
                    else if (_locObjectName == "Conductor".ToLower())
                    {
                        _result = 35;
                    }
                    else if (_locObjectName == "Consumption".ToLower())
                    {
                        _result = 36;
                    }
                    else if (_locObjectName == "ConsumptionJournal".ToLower())
                    {
                        _result = 37;
                    }
                    else if (_locObjectName == "ConsumptionLine".ToLower())
                    {
                        _result = 38;
                    }             
                    else if (_locObjectName == "CoreNumber".ToLower())
                    {
                        _result = 39;
                    }
                    else if (_locObjectName == "CostOfGoodsSold".ToLower())
                    {
                        _result = 40;
                    }
                    else if (_locObjectName == "Country".ToLower())
                    {
                        _result = 41;
                    }
                    else if (_locObjectName == "CreditMemo".ToLower())
                    {
                        _result = 42;
                    }
                    else if (_locObjectName == "CreditMemoCollection".ToLower())
                    {
                        _result = 43;
                    }
                    else if (_locObjectName == "CreditMemoLine".ToLower())
                    {
                        _result = 44;
                    }
                    else if (_locObjectName == "CrossSectionArea".ToLower())
                    {
                        _result = 45;
                    }
                    else if (_locObjectName == "Currency".ToLower())
                    {
                        _result = 46;
                    }
                    else if (_locObjectName == "CurrencyRate".ToLower())
                    {
                        _result = 47;
                    }
                    else if (_locObjectName == "CuttingProcess".ToLower())
                    {
                        _result = 48;
                    }
                    else if (_locObjectName == "CuttingProcessLine".ToLower())
                    {
                        _result = 49;
                    }
                    else if (_locObjectName == "CuttingProcessLot".ToLower())
                    {
                        _result = 50;
                    }
                    else if (_locObjectName == "DebitMemo".ToLower())
                    {
                        _result = 51;
                    }
                    else if (_locObjectName == "DebitMemoCollection".ToLower())
                    {
                        _result = 52;
                    }
                    else if (_locObjectName == "DebitMemoLine".ToLower())
                    {
                        _result = 53;
                    }      
                    else if (_locObjectName == "Department".ToLower())
                    {
                        _result = 54;
                    }
                    else if (_locObjectName == "DepartmentAccountGroup".ToLower())
                    {
                        _result = 55;
                    }
                    else if (_locObjectName == "DesignNumber".ToLower())
                    {
                        _result = 56;
                    }
                    else if (_locObjectName == "DesignProperty".ToLower())
                    {
                        _result = 57;
                    }
                    else if (_locObjectName == "DesignSheet".ToLower())
                    {
                        _result = 58;
                    }
                    else if (_locObjectName == "DesignSheetComponentHeader".ToLower())
                    {
                        _result = 59;
                    }
                    else if (_locObjectName == "DesignSheetComponentLine".ToLower())
                    {
                        _result = 60;
                    }
                    else if (_locObjectName == "DesignSheetLine".ToLower())
                    {
                        _result = 61;
                    }
                    else if (_locObjectName == "Discount".ToLower())
                    {
                        _result = 62;
                    }
                    else if (_locObjectName == "DiscountAccountGroup".ToLower())
                    {
                        _result = 63;
                    }
                    else if (_locObjectName == "DiscountLine".ToLower())
                    {
                        _result = 64;
                    }
                    else if (_locObjectName == "Division".ToLower())
                    {
                        _result = 65;
                    }
                    else if (_locObjectName == "DocumentType".ToLower())
                    {
                        _result = 66;
                    }
                    else if (_locObjectName == "EDocument".ToLower())
                    {
                        _result = 67;
                    }
                    else if (_locObjectName == "Education".ToLower())
                    {
                        _result = 68;
                    }
                    else if (_locObjectName == "EducationDegree".ToLower())
                    {
                        _result = 69;
                    }
                    else if (_locObjectName == "EducationDepartment".ToLower())
                    {
                        _result = 70;
                    }     
                    else if (_locObjectName == "Employee".ToLower())
                    {
                        _result = 71;
                    }
                    else if (_locObjectName == "ExchangeRate".ToLower())
                    {
                        _result = 72;
                    }
                    else if (_locObjectName == "ExtraData".ToLower())
                    {
                        _result = 73;
                    }
                    else if (_locObjectName == "Family".ToLower())
                    {
                        _result = 74;
                    }
                    else if (_locObjectName == "FamilyType".ToLower())
                    {
                        _result = 75;
                    }       
                    else if (_locObjectName == "Filler".ToLower())
                    {
                        _result = 76;
                    }
                    else if (_locObjectName == "FinishProduction".ToLower())
                    {
                        _result = 77;
                    }
                    else if (_locObjectName == "FinishProductionJournal".ToLower())
                    {
                        _result = 78;
                    }
                    else if (_locObjectName == "FinishProductionLine".ToLower())
                    {
                        _result = 79;
                    }   
                    else if (_locObjectName == "FormulaDesignSheetLine".ToLower())
                    {
                        _result = 80;
                    }
                    else if (_locObjectName == "GenderType".ToLower())
                    {
                        _result = 81;
                    }
                    else if (_locObjectName == "GeneralJournal".ToLower())
                    {
                        _result = 82;
                    }
                    else if (_locObjectName == "Grade".ToLower())
                    {
                        _result = 83;
                    }
                    else if (_locObjectName == "Grease".ToLower())
                    {
                        _result = 84;
                    }
                    else if (_locObjectName == "Identity".ToLower())
                    {
                        _result = 85;
                    }
                    else if (_locObjectName == "IdentityType".ToLower())
                    {
                        _result = 86;
                    }
                    else if (_locObjectName == "IndustrialType".ToLower())
                    {
                        _result = 87;
                    }
                    else if (_locObjectName == "Insulation".ToLower())
                    {
                        _result = 88;
                    }
                    else if (_locObjectName == "Insurance".ToLower())
                    {
                        _result = 89;
                    }
                    else if (_locObjectName == "InsuranceLine".ToLower())
                    {
                        _result = 90;
                    }   
                    else if (_locObjectName == "InventoryJournal".ToLower())
                    {
                        _result = 91;
                    }
                    else if (_locObjectName == "InventoryProduction".ToLower())
                    {
                        _result = 92;
                    }
                    else if (_locObjectName == "InventoryProductionLine".ToLower())
                    {
                        _result = 93;
                    }
                    else if (_locObjectName == "InventorySalesCollection".ToLower())
                    {
                        _result = 94;
                    }  
                    else if (_locObjectName == "InventoryTransferIn".ToLower())
                    {
                        _result = 95;
                    }
                    else if (_locObjectName == "InventoryTransferInLine".ToLower())
                    {
                        _result = 96;
                    }
                    else if (_locObjectName == "InventoryTransferInLot".ToLower())
                    {
                        _result = 97;
                    }
                    else if (_locObjectName == "InventoryTransferOrder".ToLower())
                    {
                        _result = 98;
                    }
                    else if (_locObjectName == "InventoryTransferOrderLine".ToLower())
                    {
                        _result = 99;
                    }
                    else if (_locObjectName == "InventoryTransferOrderLot".ToLower())
                    {
                        _result = 100;
                    }
                    else if (_locObjectName == "InventoryTransferOrderMonitoring".ToLower())
                    {
                        _result = 101;
                    }     
                    else if (_locObjectName == "InventoryTransferOut".ToLower())
                    {
                        _result = 102;
                    }
                    else if (_locObjectName == "InventoryTransferOutLine".ToLower())
                    {
                        _result = 103;
                    }
                    else if (_locObjectName == "InventoryTransferOutLot".ToLower())
                    {
                        _result = 104;
                    }
                    else if (_locObjectName == "InventoryTransferOutMonitoring".ToLower())
                    {
                        _result = 105;
                    }
                    else if (_locObjectName == "Item".ToLower())
                    {
                        _result = 106;
                    }
                    else if (_locObjectName == "ItemAccountGroup".ToLower())
                    {
                        _result = 107;
                    }
                    else if (_locObjectName == "ItemBillOfMaterial".ToLower())
                    {
                        _result = 108;
                    }
                    else if (_locObjectName == "ItemComponent".ToLower())
                    {
                        _result = 109;
                    }
                    else if (_locObjectName == "ItemConsumption".ToLower())
                    {
                        _result = 110;
                    }
                    else if (_locObjectName == "ItemConsumptionLine".ToLower())
                    {
                        _result = 111;
                    }
                    else if (_locObjectName == "ItemGroup".ToLower())
                    {
                        _result = 112;
                    }
                    else if (_locObjectName == "ItemReclassJournal".ToLower())
                    {
                        _result = 113;
                    }
                    else if (_locObjectName == "ItemReclassJournalLine".ToLower())
                    {
                        _result = 114;
                    }   
                    else if (_locObjectName == "ItemType".ToLower())
                    {
                        _result = 115;
                    }
                    else if (_locObjectName == "ItemUnitOfMeasure".ToLower())
                    {
                        _result = 116;
                    }
                    else if (_locObjectName == "JournalMap".ToLower())
                    {
                        _result = 117;
                    }
                    else if (_locObjectName == "JournalMapLine".ToLower())
                    {
                        _result = 118;
                    }
                    else if (_locObjectName == "ListImport".ToLower())
                    {
                        _result = 119;
                    }
                    else if (_locObjectName == "LoanApproval".ToLower())
                    {
                        _result = 120;
                    }
                    else if (_locObjectName == "LoanProcess".ToLower())
                    {
                        _result = 121;
                    }
                    else if (_locObjectName == "LoanProposal".ToLower())
                    {
                        _result = 122;
                    }
                    else if (_locObjectName == "LoanStatus".ToLower())
                    {
                        _result = 123;
                    }
                    else if (_locObjectName == "LoanType".ToLower())
                    {
                        _result = 124;
                    }    
                    else if (_locObjectName == "Location".ToLower())
                    {
                        _result = 125;
                    }
                    else if (_locObjectName == "Machine".ToLower())
                    {
                        _result = 126;
                    }
                    else if (_locObjectName == "MachineMapLine".ToLower())
                    {
                        _result = 127;
                    }
                    else if (_locObjectName == "MachineMapVersion".ToLower())
                    {
                        _result = 128;
                    }
                    else if (_locObjectName == "MailSetupDetail".ToLower())
                    {
                        _result = 129;
                    }   
                    else if (_locObjectName == "Material".ToLower())
                    {
                        _result = 130;
                    }
                    else if (_locObjectName == "MaterialPrice".ToLower())
                    {
                        _result = 131;
                    }
                    else if (_locObjectName == "MaterialRequisition".ToLower())
                    {
                        _result = 132;
                    }
                    else if (_locObjectName == "MaterialRequisitionLine".ToLower())
                    {
                        _result = 133;
                    }
                    else if (_locObjectName == "NonTaxableIncome".ToLower())
                    {
                        _result = 134;
                    }   
                    else if (_locObjectName == "NumberingHeader".ToLower())
                    {
                        _result = 135;
                    }
                    else if (_locObjectName == "NumberingLine".ToLower())
                    {
                        _result = 136;
                    }
                    else if (_locObjectName == "OrganizationSetupDetail".ToLower())
                    {
                        _result = 137;
                    }
                    else if (_locObjectName == "OuterSheath".ToLower())
                    {
                        _result = 138;
                    }
                    else if (_locObjectName == "OutputProduction".ToLower())
                    {
                        _result = 139;
                    }
                    else if (_locObjectName == "OutputProductionJournal".ToLower())
                    {
                        _result = 140;
                    }
                    else if (_locObjectName == "OutputProductionLine".ToLower())
                    {
                        _result = 141;
                    }    
                    else if (_locObjectName == "OverheadCost".ToLower())
                    {
                        _result = 142;
                    }
                    else if (_locObjectName == "OverheadCostDepreciation".ToLower())
                    {
                        _result = 143;
                    }
                    else if (_locObjectName == "OverheadCostElectric".ToLower())
                    {
                        _result = 144;
                    }
                    else if (_locObjectName == "OverheadCostEmployee".ToLower())
                    {
                        _result = 145;
                    }
                    else if (_locObjectName == "PayableTransaction".ToLower())
                    {
                        _result = 146;
                    }
                    else if (_locObjectName == "PayableTransactionLine".ToLower())
                    {
                        _result = 147;
                    }
                    else if (_locObjectName == "PaymentInPlan".ToLower())
                    {
                        _result = 148;
                    }
                    else if (_locObjectName == "PaymentMethod".ToLower())
                    {
                        _result = 149;
                    }
                    else if (_locObjectName == "PaymentOutPlan".ToLower())
                    {
                        _result = 150;
                    }
                    else if (_locObjectName == "PaymentRealization".ToLower())
                    {
                        _result = 151;
                    }
                    else if (_locObjectName == "PaymentRealizationLine".ToLower())
                    {
                        _result = 152;
                    }
                    else if (_locObjectName == "PayrollProcess".ToLower())
                    {
                        _result = 153;
                    }
                    else if (_locObjectName == "PayrollProcessDetail".ToLower())
                    {
                        _result = 154;
                    }
                    else if (_locObjectName == "Period".ToLower())
                    {
                        _result = 155;
                    }    
                    else if (_locObjectName == "PhoneNumber".ToLower())
                    {
                        _result = 156;
                    }
                    else if (_locObjectName == "PhoneType".ToLower())
                    {
                        _result = 157;
                    }
                    else if (_locObjectName == "Position".ToLower())
                    {
                        _result = 158;
                    }
                    else if (_locObjectName == "PrePurchaseOrder".ToLower())
                    {
                        _result = 159;
                    }
                    else if (_locObjectName == "PrePurchaseOrder2".ToLower())
                    {
                        _result = 160;
                    }
                    else if (_locObjectName == "PreSalesOrder".ToLower())
                    {
                        _result = 161;
                    }
                    else if (_locObjectName == "PreSalesOrder2".ToLower())
                    {
                        _result = 162;
                    }
                    else if (_locObjectName == "Price".ToLower())
                    {
                        _result = 163;
                    }
                    else if (_locObjectName == "PriceGroup".ToLower())
                    {
                        _result = 164;
                    }
                    else if (_locObjectName == "PriceLine".ToLower())
                    {
                        _result = 165;
                    }
                    else if (_locObjectName == "ProductCode".ToLower())
                    {
                        _result = 166;
                    }
                    else if (_locObjectName == "ProductGroup".ToLower())
                    {
                        _result = 167;
                    }
                    else if (_locObjectName == "Production".ToLower())
                    {
                        _result = 168;
                    }
                    else if (_locObjectName == "ProductionLine".ToLower())
                    {
                        _result = 169;
                    }
                    else if (_locObjectName == "ProductionMaterial".ToLower())
                    {
                        _result = 170;
                    }   
                    else if (_locObjectName == "ProductType".ToLower())
                    {
                        _result = 171;
                    }
                    else if (_locObjectName == "ProjectHeader".ToLower())
                    {
                        _result = 172;
                    }
                    else if (_locObjectName == "ProjectLine".ToLower())
                    {
                        _result = 173;
                    }
                    else if (_locObjectName == "ProjectLine2".ToLower())
                    {
                        _result = 174;
                    }
                    else if (_locObjectName == "ProjectLine2Item".ToLower())
                    {
                        _result = 175;
                    }
                    else if (_locObjectName == "ProjectLine2Item2".ToLower())
                    {
                        _result = 176;
                    }
                    else if (_locObjectName == "ProjectLine2Service".ToLower())
                    {
                        _result = 177;
                    }
                    else if (_locObjectName == "ProjectLineItem".ToLower())
                    {
                        _result = 178;
                    }
                    else if (_locObjectName == "ProjectLineItem2".ToLower())
                    {
                        _result = 179;
                    }
                    else if (_locObjectName == "ProjectLineService".ToLower())
                    {
                        _result = 180;
                    }
                    else if (_locObjectName == "PurchaseInvoice".ToLower())
                    {
                        _result = 181;
                    }
                    else if (_locObjectName == "PurchaseInvoiceCollection".ToLower())
                    {
                        _result = 182;
                    }
                    else if (_locObjectName == "PurchaseInvoiceCollectionLine".ToLower())
                    {
                        _result = 183;
                    }    
                    else if (_locObjectName == "PurchaseInvoiceLine".ToLower())
                    {
                        _result = 184;
                    }
                    else if (_locObjectName == "PurchaseOrder".ToLower())
                    {
                        _result = 185;
                    }
                    else if (_locObjectName == "PurchaseOrderLine".ToLower())
                    {
                        _result = 186;
                    }
                    else if (_locObjectName == "PurchasePrePaymentInvoice".ToLower())
                    {
                        _result = 187;
                    }
                    else if (_locObjectName == "PurchaseRequisition".ToLower())
                    {
                        _result = 188;
                    }
                    else if (_locObjectName == "PurchaseRequisitionCollection".ToLower())
                    {
                        _result = 189;
                    }
                    else if (_locObjectName == "PurchaseRequisitionLine".ToLower())
                    {
                        _result = 190;
                    }
                    else if (_locObjectName == "PurchaseReturn".ToLower())
                    {
                        _result = 191;
                    }
                    else if (_locObjectName == "PurchaseReturnLine".ToLower())
                    {
                        _result = 192;
                    }
                    else if (_locObjectName == "PurchaseSetupDetail".ToLower())
                    {
                        _result = 193;
                    }
                    else if (_locObjectName == "RatedVoltage".ToLower())
                    {
                        _result = 194;
                    }
                    else if (_locObjectName == "ReceivableTransaction".ToLower())
                    {
                        _result = 195;
                    }
                    else if (_locObjectName == "ReceivableTransactionLine".ToLower())
                    {
                        _result = 196;
                    }
                    else if (_locObjectName == "Religion".ToLower())
                    {
                        _result = 197;
                    }
                    else if (_locObjectName == "RoundingSetupDetail".ToLower())
                    {
                        _result = 198;
                    }
                    else if (_locObjectName == "SalaryBasic".ToLower())
                    {
                        _result = 199;
                    }
                    else if (_locObjectName == "SalaryComponent".ToLower())
                    {
                        _result = 200;
                    }
                    else if (_locObjectName == "SalaryGroup".ToLower())
                    {
                        _result = 201;
                    }
                    else if (_locObjectName == "SalaryGroupDetail".ToLower())
                    {
                        _result = 202;
                    }    
                    else if (_locObjectName == "SalesInvoice".ToLower())
                    {
                        _result = 203;
                    }
                    else if (_locObjectName == "SalesInvoiceCollection".ToLower())
                    {
                        _result = 204;
                    }
                    else if (_locObjectName == "SalesInvoiceCollectionLine".ToLower())
                    {
                        _result = 205;
                    }     
                    else if (_locObjectName == "SalesInvoiceLine".ToLower())
                    {
                        _result = 206;
                    }
                    else if (_locObjectName == "SalesOrder".ToLower())
                    {
                        _result = 207;
                    }
                    else if (_locObjectName == "SalesOrderLine".ToLower())
                    {
                        _result = 208;
                    }
                    else if (_locObjectName == "SalesOrderMonitoring".ToLower())
                    {
                        _result = 209;
                    }
                    else if (_locObjectName == "SalesPrePaymentInvoice".ToLower())
                    {
                        _result = 210;
                    }
                    else if (_locObjectName == "SalesQuotation".ToLower())
                    {
                        _result = 211;
                    }
                    else if (_locObjectName == "SalesQuotationCollection".ToLower())
                    {
                        _result = 212;
                    }
                    else if (_locObjectName == "SalesQuotationLine".ToLower())
                    {
                        _result = 213;
                    }
                    else if (_locObjectName == "SalesQuotationMonitoring".ToLower())
                    {
                        _result = 214;
                    }
                    else if (_locObjectName == "SalesReturn".ToLower())
                    {
                        _result = 215;
                    }
                    else if (_locObjectName == "SalesReturnLine".ToLower())
                    {
                        _result = 216;
                    }
                    else if (_locObjectName == "SalesSetupDetail".ToLower())
                    {
                        _result = 217;
                    }
                    else if (_locObjectName == "School".ToLower())
                    {
                        _result = 218;
                    }   
                    else if (_locObjectName == "Screening".ToLower())
                    {
                        _result = 219;
                    }
                    else if (_locObjectName == "Section".ToLower())
                    {
                        _result = 220;
                    }
                    else if (_locObjectName == "Separator".ToLower())
                    {
                        _result = 221;
                    }
                    else if (_locObjectName == "ShapeOfConductor".ToLower())
                    {
                        _result = 222;
                    }
                    else if (_locObjectName == "Speciality".ToLower())
                    {
                        _result = 223;
                    }
                    else if (_locObjectName == "SpecialityMaster".ToLower())
                    {
                        _result = 224;
                    }   
                    else if (_locObjectName == "Standart".ToLower())
                    {
                        _result = 225;
                    }
                    else if (_locObjectName == "StockTransfer".ToLower())
                    {
                        _result = 226;
                    }
                    else if (_locObjectName == "StockTransferLine".ToLower())
                    {
                        _result = 227;
                    }
                    else if (_locObjectName == "SubBrand".ToLower())
                    {
                        _result = 228;
                    }
                    else if (_locObjectName == "SubProductGroup".ToLower())
                    {
                        _result = 229;
                    }
                    else if (_locObjectName == "Tax".ToLower())
                    {
                        _result = 230;
                    }
                    else if (_locObjectName == "TaxAccountGroup".ToLower())
                    {
                        _result = 231;
                    }
                    else if (_locObjectName == "TaxGroup".ToLower())
                    {
                        _result = 232;
                    }
                    else if (_locObjectName == "TaxLine".ToLower())
                    {
                        _result = 233;
                    }
                    else if (_locObjectName == "TaxProcess".ToLower())
                    {
                        _result = 234;
                    }
                    else if (_locObjectName == "TaxRateNonNPWP".ToLower())
                    {
                        _result = 235;
                    }
                    else if (_locObjectName == "TaxRateNPWP".ToLower())
                    {
                        _result = 236;
                    }
                    else if (_locObjectName == "TaxSetup".ToLower())
                    {
                        _result = 237;
                    }    
                    else if (_locObjectName == "TaxType".ToLower())
                    {
                        _result = 238;
                    }
                    else if (_locObjectName == "TempBeginingInventoryLine".ToLower())
                    {
                        _result = 239;
                    }
                    else if (_locObjectName == "TermOfPayment".ToLower())
                    {
                        _result = 240;
                    }
                    else if (_locObjectName == "Training".ToLower())
                    {
                        _result = 241;
                    }
                    else if (_locObjectName == "TrainingMaster".ToLower())
                    {
                        _result = 242;
                    }  
                    else if (_locObjectName == "TransferIn".ToLower())
                    {
                        _result = 243;
                    }
                    else if (_locObjectName == "TransferInLine".ToLower())
                    {
                        _result = 244;
                    }
                    else if (_locObjectName == "TransferInLot".ToLower())
                    {
                        _result = 245;
                    }
                    else if (_locObjectName == "TransferOrder".ToLower())
                    {
                        _result = 246;
                    }
                    else if (_locObjectName == "TransferOrderLine".ToLower())
                    {
                        _result = 247;
                    }
                    else if (_locObjectName == "TransferOrderLot".ToLower())
                    {
                        _result = 248;
                    }
                    else if (_locObjectName == "TransferOut".ToLower())
                    {
                        _result = 249;
                    }
                    else if (_locObjectName == "TransferOutLine".ToLower())
                    {
                        _result = 250;
                    }
                    else if (_locObjectName == "TransferOutLot".ToLower())
                    {
                        _result = 251;
                    }
                    else if (_locObjectName == "UnitOfMeasure".ToLower())
                    {
                        _result = 252;
                    }
                    else if (_locObjectName == "UnitOfMeasureType".ToLower())
                    {
                        _result = 253;
                    }
                    else if (_locObjectName == "UserAccess".ToLower())
                    {
                        _result = 254;
                    }
                    else if (_locObjectName == "UserAccessRole".ToLower())
                    {
                        _result = 255;
                    }
                    else if (_locObjectName == "WarehouseSetupDetail".ToLower())
                    {
                        _result = 256;
                    }
                    else if (_locObjectName == "WireNumber".ToLower())
                    {
                        _result = 257;
                    }
                    else if (_locObjectName == "WorkingContract".ToLower())
                    {
                        _result = 258;
                    }
                    else if (_locObjectName == "WorkingExperience".ToLower())
                    {
                        _result = 259;
                    }
                    else if (_locObjectName == "WorkingStatus".ToLower())
                    {
                        _result = 260;
                    }   
                    else if (_locObjectName == "WorkOrder".ToLower())
                    {
                        _result = 261;
                    }
                    else if (_locObjectName == "WorkOrderLine".ToLower())
                    {
                        _result = 262;
                    }
                    else if (_locObjectName == "WorkOrderMaterial".ToLower())
                    {
                        _result = 263;
                    }
                    else if (_locObjectName == "WorkRequisition".ToLower())
                    {
                        _result = 264;
                    }
                    else if (_locObjectName == "WorkRequisitionCollection".ToLower())
                    {
                        _result = 265;
                    }
                    else if (_locObjectName == "WorkRequisitionCollectionLine".ToLower())
                    {
                        _result = 266;
                    }
                    else if (_locObjectName == "WorkRequisitionLine".ToLower())
                    {
                        _result = 267;
                    }
                    else if (_locObjectName == "Wrapping".ToLower())
                    {
                        _result = 268;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetAccountCharge(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Debit".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Credit".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Both".ToLower())
                    {
                        _result = 3;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetAccountMode(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Customer".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Vendor".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "BankAccount".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "Others".ToLower())
                    {
                        _result = 4;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetAccountType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Posting".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Heading".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Total".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "BeginTotal".ToLower())
                    {
                        _result = 4;
                    }
                    else if (_locObjectName == "EndTotal".ToLower())
                    {
                        _result = 5;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetApprovalLevel(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Level1".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Level2".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Level3".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "Level4".ToLower())
                    {
                        _result = 4;
                    }
                    else if (_locObjectName == "Level5".ToLower())
                    {
                        _result = 5;
                    }
                    else if (_locObjectName == "Level6".ToLower())
                    {
                        _result = 6;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetBusinessPartnerType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "All".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Customer".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Vendor".ToLower())
                    {
                        _result = 3;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetDirectionRule(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Direct".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Indirect".ToLower())
                    {
                        _result = 2;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetDirectionType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Internal".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "External".ToLower())
                    {
                        _result = 2;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetDiscountMode(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Purchase".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Sales".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Both".ToLower())
                    {
                        _result = 3;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetDocumentRule(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Customer".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Vendor".ToLower())
                    {
                        _result = 2;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetFieldName(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "MxUAmount".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "MxTUAmount".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "UAmount".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "TUAmount".ToLower())
                    {
                        _result = 4;
                    }
                    else if (_locObjectName == "TxAmount".ToLower())
                    {
                        _result = 5;
                    }
                    else if (_locObjectName == "DiscAmount".ToLower())
                    {
                        _result = 6;
                    }
                    else if (_locObjectName == "TAmount".ToLower())
                    {
                        _result = 7;
                    }
                    else if (_locObjectName == "TotalUnitAmount".ToLower())
                    {
                        _result = 8;
                    }
                    else if (_locObjectName == "UnitAmount".ToLower())
                    {
                        _result = 9;
                    }
                    else if (_locObjectName == "Amount1".ToLower())
                    {
                        _result = 10;
                    }
                    else if (_locObjectName == "Amount2".ToLower())
                    {
                        _result = 11;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetFinancialStatment(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "BalanceSheet".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "IncomeStatment".ToLower())
                    {
                        _result = 2;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetFunctionList(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Approval".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Posting".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Progress".ToLower())
                    {
                        _result = 3;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetInventoryMovingType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Receive".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Deliver".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "TransferIn".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "TransferOut".ToLower())
                    {
                        _result = 4;
                    }
                    else if (_locObjectName == "Adjust".ToLower())
                    {
                        _result = 5;
                    }
                    else if (_locObjectName == "Return".ToLower())
                    {
                        _result = 6;
                    }
                    else if (_locObjectName == "Consumpt".ToLower())
                    {
                        _result = 7;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetOrderType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Item".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "FixedAsset".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Account".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "Service".ToLower())
                    {
                        _result = 4;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetPageType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "DetailView".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "ListView".ToLower())
                    {
                        _result = 2;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetPostingMethod(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Receipt".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Deliver".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "InvoiceAR".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "InvoiceAP".ToLower())
                    {
                        _result = 4;
                    }
                    else if (_locObjectName == "Payment".ToLower())
                    {
                        _result = 5;
                    }
                    else if (_locObjectName == "Bill".ToLower())
                    {
                        _result = 6;
                    }
                    else if (_locObjectName == "Return".ToLower())
                    {
                        _result = 7;
                    }
                    else if (_locObjectName == "CreditMemo".ToLower())
                    {
                        _result = 8;
                    }
                    else if (_locObjectName == "DebitMemo".ToLower())
                    {
                        _result = 9;
                    }
                    else if (_locObjectName == "ReclassJournal".ToLower())
                    {
                        _result = 10;
                    }
                    else if (_locObjectName == "PrePayment".ToLower())
                    {
                        _result = 11;
                    }
                    else if (_locObjectName == "CashAdvance".ToLower())
                    {
                        _result = 12;
                    }
                    else if (_locObjectName == "CashRealization".ToLower())
                    {
                        _result = 13;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetPostingMethodType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Normal".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Tax".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Discount".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "Direct".ToLower())
                    {
                        _result = 4;
                    }
                    else if (_locObjectName == "Domestic".ToLower())
                    {
                        _result = 5;
                    }
                    else if (_locObjectName == "Advances".ToLower())
                    {
                        _result = 6;
                    }
                    else if (_locObjectName == "DownPayment".ToLower())
                    {
                        _result = 7;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetPostingType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Purchase".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Sales".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Routing".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "Packing".ToLower())
                    {
                        _result = 4;
                    }
                    else if (_locObjectName == "Operation".ToLower())
                    {
                        _result = 5;
                    }
                    else if (_locObjectName == "Production".ToLower())
                    {
                        _result = 6;
                    }
                    else if (_locObjectName == "Account".ToLower())
                    {
                        _result = 7;
                    }
                    else if (_locObjectName == "Advances".ToLower())
                    {
                        _result = 8;
                    }
                    else if (_locObjectName == "Others".ToLower())
                    {
                        _result = 9;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetPriorityType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Low".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Middle".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "High".ToLower())
                    {
                        _result = 3;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetReturnType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "ReplacedFull".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "CreditMemo".ToLower())
                    {
                        _result = 2;
                    }
                    //clm (060120-13:51)
                    else if (_locObjectName == "DebitMemo".ToLower())
                    {
                        _result = 3;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetStatus(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Open".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Progress".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Posted".ToLower())
                    {
                        _result = 3;
                    }
                    else if (_locObjectName == "Close".ToLower())
                    {
                        _result = 4;
                    }
                    else if (_locObjectName == "Lock".ToLower())
                    {
                        _result = 5;
                    }
                    else if (_locObjectName == "Approved".ToLower())
                    {
                        _result = 6;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetStockType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Good".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Bad".ToLower())
                    {
                        _result = 2;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetTaxMode(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Purchase".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Sales".ToLower())
                    {
                        _result = 2;
                    }
                    else if (_locObjectName == "Both".ToLower())
                    {
                        _result = 3;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetTaxNature(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Increase".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Decrease".ToLower())
                    {
                        _result = 2;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public int GetLocationType(string _objectName)
        {
            int _result = 0;
            string _locObjectName = null;
            try
            {
                if (_objectName != null)
                {
                    _locObjectName = _objectName.Trim().Replace(" ", "").ToLower();
                    if (_locObjectName == "Main".ToLower())
                    {
                        _result = 1;
                    }
                    else if (_locObjectName == "Transit".ToLower())
                    {
                        _result = 2;
                    }
                    else
                    {
                        _result = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        #endregion Function For Enum

        #endregion Import Data

        #region Normal Function

        public bool GetAdministrationAccessing(Session _locSession, string _locUserName)
        {
            bool _result = false;
            try
            {

                UserAccess _numLineUserAccess = _locSession.FindObject<UserAccess>
                                                (new BinaryOperator("UserName", _locUserName));
                if (_numLineUserAccess != null)
                {
                    XPCollection<UserAccessRole> _locUserAccessRoles = new XPCollection<UserAccessRole>(_locSession, new ContainsOperator("UserAccesses", new BinaryOperator("This", _numLineUserAccess)));
                    if (_locUserAccessRoles != null)
                    {
                        foreach (UserAccessRole _locUserAccessRole in _locUserAccessRoles)
                        {
                            if (_locUserAccessRole.IsAdministrative == true)
                            {
                                _result = true;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public Country GetDefaultCountry(Session _currSession)
        {
            Country _result = null;
            try
            {
                _result = _currSession.FindObject<Country>(new GroupOperator(GroupOperatorType.And,
                                                           new BinaryOperator("Default", true),
                                                           new BinaryOperator("Active", true)));
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public Separator GetDefaultSeparator(Session _currSession)
        {
            Separator _result = null;
            try
            {
                _result = _currSession.FindObject<Separator>(new GroupOperator(GroupOperatorType.And,
                                                           new BinaryOperator("Default", true),
                                                           new BinaryOperator("Active", true)));
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public Currency GetDefaultCurrency(Session _currSession)
        {
            Currency _result = null;
            try
            {
                _result = _currSession.FindObject<Currency>(new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Default", true),
                                                            new BinaryOperator("Active", true)));
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public Tax GetDefaultTax(Session _currSession, TaxMode _locTaxMode)
        {
            Tax _result = null;
            try
            {
                if (_locTaxMode != TaxMode.None)
                {
                    Tax _result2 = _currSession.FindObject<Tax>(new GroupOperator
                                (GroupOperatorType.And,
                                new BinaryOperator("Default", true),
                                new BinaryOperator("Active", true),
                                new BinaryOperator("TaxMode", _locTaxMode)));
                    if (_result2 != null)
                    {
                        _result = _result2;
                    }
                    else
                    {
                        Tax _result3 = _currSession.FindObject<Tax>(new GroupOperator
                                    (GroupOperatorType.And,
                                    new BinaryOperator("Default", true),
                                    new BinaryOperator("Active", true),
                                    new BinaryOperator("TaxMode", TaxMode.Both)));
                        if (_result3 != null)
                        {
                            _result = _result3;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public Discount GetDefaultDiscount(Session _currSession, DiscountMode _locDiscountMode)
        {
            Discount _result = null;
            try
            {
                if (_locDiscountMode != DiscountMode.None)
                {
                    Discount _result2 = _currSession.FindObject<Discount>(new GroupOperator
                                    (GroupOperatorType.And,
                                    new BinaryOperator("Default", true),
                                    new BinaryOperator("Active", true),
                                    new BinaryOperator("DiscountMode", _locDiscountMode)));
                    if (_result2 != null)
                    {
                        _result = _result2;
                    }
                    else
                    {
                        Discount _result3 = _currSession.FindObject<Discount>(new GroupOperator
                                        (GroupOperatorType.And,
                                        new BinaryOperator("Default", true),
                                        new BinaryOperator("Active", true),
                                        new BinaryOperator("DiscountMode", DiscountMode.Both)));
                        if (_result3 != null)
                        {
                            _result = _result3;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        #endregion Normal Function

        #region Rounding

        public bool GetRoundingList(Session _currSession, ObjectList _currObjectList, FieldName _currFieldName)
        {

            bool _result = false;
            RoundingSetupDetail _roundingSetupDetail = null;
            RoundingSetupDetail _roundingSetupDetail2 = null;

            try
            {

                ApplicationSetup _appSetup = _currSession.FindObject<ApplicationSetup>(new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Active", true), new BinaryOperator("DefaultSystem", true),
                                            new BinaryOperator("Code", "APS0001")));
                if (_appSetup != null)
                {
                    if (_currFieldName != FieldName.None)
                    {
                        _roundingSetupDetail = _currSession.FindObject<RoundingSetupDetail>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("ObjectList", _currObjectList),
                                                            new BinaryOperator("FieldName", _currFieldName),
                                                            new BinaryOperator("Active", true)));
                        if (_roundingSetupDetail != null)
                        {
                            _result = true;
                        }
                        else
                        {
                            _roundingSetupDetail2 = _currSession.FindObject<RoundingSetupDetail>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("All", true),
                                                            new BinaryOperator("Active", true)));

                            if (_roundingSetupDetail2 != null)
                            {
                                _result = true;
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        public Double GetRoundUp(Session _currSession, Double _currValue, ObjectList _currObjectList, FieldName _currFieldName)
        {
            Double result = 0;

            try
            {
                int _roundUpNumber = 0;
                RoundingSetupDetail _roundingSetupDetail = null;
                RoundingSetupDetail _roundingSetupDetail2 = null;

                ApplicationSetup _appSetup = _currSession.FindObject<ApplicationSetup>(new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Active", true),
                                            new BinaryOperator("DefaultSystem", true),
                                            new BinaryOperator("SetupCode", "APS0001")));
                if (_appSetup != null)
                {
                    if (_currFieldName != FieldName.None)
                    {
                        _roundingSetupDetail = _currSession.FindObject<RoundingSetupDetail>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("ObjectList", _currObjectList),
                                                            new BinaryOperator("FieldName", _currFieldName),
                                                            new BinaryOperator("Active", true),
                                                            new BinaryOperator("ApplicationSetup", _appSetup)));
                        if (_roundingSetupDetail != null)
                        {
                            _roundUpNumber = _roundingSetupDetail.RoundValue;
                        }
                        else
                        {
                            _roundingSetupDetail2 = _currSession.FindObject<RoundingSetupDetail>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("All", true),
                                                            new BinaryOperator("Active", true),
                                                            new BinaryOperator("ApplicationSetup", _appSetup)));
                            if (_roundingSetupDetail2 != null)
                            {
                                _roundUpNumber = _roundingSetupDetail2.RoundValue;
                            }
                        }
                    }
                    else
                    {
                        _roundingSetupDetail = _currSession.FindObject<RoundingSetupDetail>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("All", true),
                                                            new BinaryOperator("Active", true),
                                                            new BinaryOperator("ApplicationSetup", _appSetup)));
                        if (_roundingSetupDetail != null)
                        {
                            _roundUpNumber = _roundingSetupDetail.RoundValue;
                        }
                    }

                }

                //result = System.Math.Round(_currValue, _roundUpNumber, MidpointRounding.ToEven);
                result = System.Math.Round(_currValue, _roundUpNumber);

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return result;
        }

        #endregion Rounding

        #region Approval

        public ApplicationSetupDetail GetApplicationSetupDetailByApprovalLevel(Session currentSession, UserAccess _locUserAccess, ApprovalLevel _locApprovalLevel, ObjectList _locObjectList)
        {
            ApplicationSetupDetail _result = null;
            try
            {
                ApplicationSetupDetail _locAppSetupDetail = currentSession.FindObject<ApplicationSetupDetail>
                                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("UserAccess", _locUserAccess),
                                                                                  new BinaryOperator("ObjectList", _locObjectList),
                                                                                  new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                  new BinaryOperator("ApprovalLevel", _locApprovalLevel),
                                                                                  new BinaryOperator("Active", true)));
                if (_locAppSetupDetail != null)
                {
                    _result = _locAppSetupDetail;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject =  GlobalFunction " + ex.ToString());
            }
            return _result;
        }

        #endregion Approval      

        #region Email

        public void SetAndSendMail(string _locSmtpHost, int _locSmtpPort, string _locMailFrom, string _locMailFromPassword, string _locMailTo, string _locSubjectMail, string _locMessageBody)
        {
            try
            {
                if (_locMailTo != null)
                {
                    foreach (string _locRowMailTo in _locMailTo.Replace(" ", "").Split(';'))
                    {
                        if (!string.IsNullOrEmpty(_locRowMailTo))
                        {
                            SmtpClient client = new SmtpClient(_locSmtpHost, _locSmtpPort);
                            client.TargetName = "None";
                            client.UseDefaultCredentials = false;
                            client.Credentials = new System.Net.NetworkCredential(_locMailFrom, _locMailFromPassword);
                            MailAddress from = new MailAddress(_locMailFrom, String.Empty, System.Text.Encoding.UTF8);
                            MailAddress to = new MailAddress(_locRowMailTo);
                            MailMessage message = new MailMessage(from, to);
                            message.Body = _locMessageBody;
                            message.IsBodyHtml = true;
                            message.BodyEncoding = System.Text.Encoding.UTF8;
                            message.Subject = _locSubjectMail;
                            message.SubjectEncoding = System.Text.Encoding.UTF8;
                            client.Send(message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = GlobalFunction " + ex.ToString());
            }
        }

        #endregion Email

    }
}