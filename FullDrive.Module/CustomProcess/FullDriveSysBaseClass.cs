﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FullDrive.Module.CustomProcess
{
    class FullDriveSysBaseClass
    {
    }

    public enum NumberingType
    {
        Documents = 0,
        Objects = 1,
        Employee = 2,
        Other = 3
    }

    public enum ObjectList
    {
        None = 0,
        AccountGroup = 1,
        AccountMap = 2,
        AccountMapLine = 3,
        AccountReclassJournal = 4,
        AccountReclassJournalLine = 5,
        AdditionalAnalyze = 6,
        AdditionalDashboard = 7,
        AdditionalReport = 8,
        Address = 9,
        ApplicationImport = 10,
        ApplicationSetup = 11,
        ApplicationSetupDetail = 12,
        ApprovalLine = 13,
        Armour = 14,
        BankAccount = 15,
        BankAccountGroup = 16,
        BeginingInventory = 17,
        BeginingInventoryLine = 18,
        BillOfMaterial = 19,
        BillOfMaterialLine = 20,
        BillOfMaterialVersion = 21,
        BinLocation = 22,
        Branch = 23,
        Brand = 24,
        BusinessPartner = 25,
        BusinessPartnerAccountGroup = 26,
        BusinessPartnerGroup = 27,
        CashAdvance = 28,
        CashAdvanceLine = 29,
        ChartOfAccount = 30,
        City = 31,
        Color = 32,
        Company = 33,
        CompanyAccountGroup = 34,
        Conductor = 35,
        Consumption = 36,
        ConsumptionJournal = 37,
        ConsumptionLine = 38,
        CoreNumber = 39,
        CostOfGoodsSold = 40,
        Country = 41,
        CreditMemo = 42,
        CreditMemoCollection = 43,
        CreditMemoLine = 44,
        CrossSectionArea = 45,
        Currency = 46,
        CurrencyRate = 47,
        CuttingProcess = 48,
        CuttingProcessLine = 49,
        CuttingProcessLot = 50,
        DebitMemo = 51,
        DebitMemoCollection = 52,
        DebitMemoLine = 53,
        Department = 54,
        DepartmentAccountGroup = 55,
        DesignNumber = 56,
        DesignProperty = 57,
        DesignSheet = 58,
        DesignSheetComponentHeader = 59,
        DesignSheetComponentLine = 60,
        DesignSheetLine = 61,
        Discount = 62,
        DiscountAccountGroup = 63,
        DiscountLine = 64,
        Division = 65,
        DocumentType = 66,
        EDocument = 67,
        Education = 68,
        EducationDegree = 69,
        EducationDepartment = 70,
        Employee = 71,
        ExchangeRate = 72,
        ExtraData = 73,
        Family = 74,
        FamilyType = 75,
        Filler = 76,
        FinishProduction = 77,
        FinishProductionJournal = 78,
        FinishProductionLine = 79,
        FormulaDesignSheetLine = 80,
        GenderType = 81,
        GeneralJournal = 82,
        Grade = 83,
        Grease = 84,
        Identity = 85,
        IdentityType = 86,
        IndustrialType = 87,
        Insulation = 88,
        Insurance = 89,
        InsuranceLine = 90,
        InventoryJournal = 91,
        InventoryProduction = 92,
        InventoryProductionLine = 93,
        InventorySalesCollection = 94,
        InventoryTransferIn = 95,
        InventoryTransferInLine = 96,
        InventoryTransferInLot = 97,
        InventoryTransferOrder = 98,
        InventoryTransferOrderLine = 99,
        InventoryTransferOrderLot = 100,
        InventoryTransferOrderMonitoring = 101,
        InventoryTransferOut = 102,
        InventoryTransferOutLine = 103,
        InventoryTransferOutLot = 104,
        InventoryTransferOutMonitoring = 105,
        Item = 106,
        ItemAccountGroup = 107,
        ItemBillOfMaterial = 108,
        ItemComponent = 109,
        ItemConsumption = 110,
        ItemConsumptionLine = 111,
        ItemGroup = 112,
        ItemReclassJournal = 113,
        ItemReclassJournalLine = 114,
        ItemType = 115,
        ItemUnitOfMeasure = 116,
        JournalMap = 117,
        JournalMapLine = 118,
        ListImport = 119,
        LoanApproval = 120,
        LoanProcess = 121,
        LoanProposal = 122,
        LoanStatus = 123,
        LoanType = 124,
        Location = 125,
        Machine = 126,
        MachineMapLine = 127,
        MachineMapVersion = 128,
        MailSetupDetail = 129,
        Material = 130,
        MaterialPrice = 131,
        MaterialRequisition = 132,
        MaterialRequisitionLine = 133,
        NonTaxableIncome = 134,
        NumberingHeader = 135,
        NumberingLine = 136,
        OrganizationSetupDetail = 137,
        OuterSheath = 138,
        OutputProduction = 139,
        OutputProductionJournal = 140,
        OutputProductionLine = 141,
        OverheadCost = 142,
        OverheadCostDepreciation = 143,
        OverheadCostElectric = 144,
        OverheadCostEmployee = 145,
        PayableTransaction = 146,
        PayableTransactionLine = 147,
        PaymentInPlan = 148,
        PaymentMethod = 149,
        PaymentOutPlan = 150,
        PaymentRealization = 151,
        PaymentRealizationLine = 152,
        PayrollProcess = 153,
        PayrollProcessDetail = 154,
        Period = 155,
        PhoneNumber = 156,
        PhoneType = 157,
        Position = 158,
        PrePurchaseOrder = 159,
        PrePurchaseOrder2 = 160,
        PreSalesOrder = 161,
        PreSalesOrder2 = 162,
        Price = 163,
        PriceGroup = 164,
        PriceLine = 165,
        ProductCode = 166,
        ProductGroup = 167,
        Production = 168,
        ProductionLine = 169,
        ProductionMaterial = 170,
        ProductType = 171,
        ProjectHeader = 172,
        ProjectLine = 173,
        ProjectLine2 = 174,
        ProjectLine2Item = 175,
        ProjectLine2Item2 = 176,
        ProjectLine2Service = 177,
        ProjectLineItem = 178,
        ProjectLineItem2 = 179,
        ProjectLineService = 180,
        PurchaseInvoice = 181,
        PurchaseInvoiceCollection = 182,
        PurchaseInvoiceCollectionLine = 183,
        PurchaseInvoiceLine = 184,
        PurchaseOrder = 185,
        PurchaseOrderLine = 186,
        PurchasePrePaymentInvoice = 187,
        PurchaseRequisition = 188,
        PurchaseRequisitionCollection = 189,
        PurchaseRequisitionLine = 190,
        PurchaseReturn = 191,
        PurchaseReturnLine = 192,
        PurchaseSetupDetail = 193,
        RatedVoltage = 194,
        ReceivableTransaction = 195,
        ReceivableTransactionLine = 196,
        Religion = 197,
        RoundingSetupDetail = 198,
        SalaryBasic = 199,
        SalaryComponent = 200,
        SalaryGroup = 201,
        SalaryGroupDetail = 202,
        SalesInvoice = 203,
        SalesInvoiceCollection = 204,
        SalesInvoiceCollectionLine = 205,
        SalesInvoiceLine = 206,
        SalesOrder = 207,
        SalesOrderLine = 208,
        SalesOrderMonitoring = 209,
        SalesPrePaymentInvoice = 210,
        SalesQuotation = 211,
        SalesQuotationCollection = 212,
        SalesQuotationLine = 213,
        SalesQuotationMonitoring = 214,
        SalesReturn = 215,
        SalesReturnLine = 216,
        SalesSetupDetail = 217,
        School = 218,
        Screening = 219,
        Section = 220,
        Separator = 221,
        ShapeOfConductor = 222,
        Speciality = 223,
        SpecialityMaster = 224,
        Standart = 225,
        StockTransfer = 226,
        StockTransferLine = 227,
        SubBrand = 228,
        SubProductGroup = 229,
        Tax = 230,
        TaxAccountGroup = 231,
        TaxGroup = 232,
        TaxLine = 233,
        TaxProcess = 234,
        TaxRateNonNPWP = 235,
        TaxRateNPWP = 236,
        TaxSetup = 237,
        TaxType = 238,
        TempBeginingInventoryLine = 239,
        TermOfPayment = 240,
        Training = 241,
        TrainingMaster = 242,       
        TransferIn = 243,
        TransferInLine = 244,
        TransferInLot = 245,
        TransferOrder = 246,
        TransferOrderLine = 247,
        TransferOrderLot = 248,
        TransferOut = 249,
        TransferOutLine = 250,
        TransferOutLot = 251,
        UnitOfMeasure = 252,
        UnitOfMeasureType = 253,
        UserAccess = 254,
        UserAccessRole = 255,
        WarehouseSetupDetail = 256,
        WireNumber = 257,
        WorkingContract = 258,
        WorkingExperience = 259,
        WorkingStatus = 260,
        WorkOrder = 261,
        WorkOrderLine = 262,
        WorkOrderMaterial = 263,
        WorkRequisition = 264,
        WorkRequisitionCollection = 265,
        WorkRequisitionCollectionLine = 266,
        WorkRequisitionLine = 267,
        Wrapping = 268,

        //Tambahan
        InvoiceSalesCollection = 269,
        SalesInvoiceMonitoring = 270,
        ReceivableSalesCollection = 271,
        ReceivableTransactionMonitoring = 272,
        ReceivableTransactionDetailLine = 273,
        SalesPrePaymentInvoiceLine = 274,

        //Wahab 03-06-2020
        PurchaseRequisitionMonitoring = 275,
        PurchaseOrderMonitoring = 276,
    }

    public enum AccountCharge
    {
        None = 0,
        Debit = 1,
        Credit = 2,
        Both = 3
    }

    public enum AccountMode
    {
        None = 0,
        Customer = 1,
        Vendor = 2,
        BankAccount = 3,
        Others = 4
    }

    public enum AccountType
    {
        None = 0,
        Posting = 1,
        Heading = 2,
        Total = 3,
        BeginTotal = 4,
        EndTotal = 5
    }

    public enum ApprovalFilter
    {
        AllData = 0,
        Approval = 1,
        EndApproval = 2,
    }

    public enum ApprovalLevel
    {
        None = 0,
        Level1 = 1,
        Level2 = 2,
        Level3 = 3,
        Level4 = 4,
        Level5 = 5,
        Level6 = 6,
    }

    public enum BalanceType
    {
        None = 0,
        Change = 1,
        Fix = 2,
        Other = 3,
    }

    public enum BusinessPartnerType
    {
        None = 0,
        All = 1,
        Customer = 2,
        Vendor = 3
    }

    public enum BloodType
    {
        None = 0,
        O = 1,
        A = 2,
        B = 3,
        AB = 4,
    }

    public enum CashAdvanceType
    {
        None = 0,
        BusinessTrip = 1,
        MaintenanceFee = 2,
        MonthlyFee = 3,
        Transportation = 4,
        Accomodation = 5,
        Other = 6,
    }

    public enum DirectionRule
    {
        None = 0,
        Direct = 1,
        Indirect = 2
    }

    public enum DirectionType
    {
        None = 0,
        Internal = 1,
        External = 2,
    }

    public enum DiscountMode
    {
        None = 0,
        Purchase = 1,
        Sales = 2,
        Both = 3,
    }

    public enum DocumentRule
    {
        None = 0,
        Customer = 1,
        Vendor = 2
    }

    public enum EmployeeList
    {
        Education = 0,
        Employee = 1,
        Family = 2,
        Identity = 3,
        Speciality = 4,
        Training = 5,
        WorkingContract = 6,
        WorkingExperience = 7,
    }

    public enum FieldName
    {
        None = 0,
        MxUAmount = 1,
        MxTUAmount = 2,
        UAmount = 3,
        TUAmount = 4,
        TxAmount = 5,
        DiscAmount = 6,
        TAmount = 7,
        TotalUnitAmount = 8,
        UnitAmount = 9,
        Amount1 = 10,
        Amount2 = 11,
    }

    public enum FinancialStatment
    {
        None = 0,
        BalanceSheet = 1,
        IncomeStatment = 2
    }

    public enum FunctionList
    {
        None = 0,
        Approval = 1,
        Posting = 2,
        Progress = 3,
        QcPassed = 4,
    }

    public enum InventoryMovingType
    {
        None = 0,
        Receive = 1,
        Deliver = 2,
        TransferIn = 3,
        TransferOut = 4,
        Adjust = 5,
        Return = 6,
        Consumpt = 7,
        Deformed = 8,
        Cutting = 9,
    }

    public enum LoanLlist
    {
        LoanApproval = 0,
        LoanProcess = 1,
        LoanProposal = 2,
        LoanStatus = 3,
        LoanType = 4,
    }

    public enum LocationType
    {
        None = 0,
        Main = 1,
        Transit = 2,
    }

    public enum LocationType2
    {
        None = 0,
        RM = 1,
        QC = 2,
        FG = 3,
        Prod = 4,
    }

    public enum MachineCost
    {
        None = 0,
        NonAssembling = 1,
        Assembling = 2,
    }

    public enum OrderType
    {
        None = 0,
        Item = 1,
        FixedAsset = 2,
        Account = 3,
        Service = 4,
    }

    public enum PageType
    {
        None = 0,
        DetailView = 1,
        ListView = 2,
    }

    public enum PaymentMethodType
    {
        None = 0,
        CashBeforeDelivery = 1,
        DownPayment = 2,
        Normal = 3,
    }

    public enum PaymentType
    {
        None = 0,
        Cash = 1,
        Cheque = 2,
        Transfer = 3, 
    }

    public enum PostingMethod
    {
        None = 0,
        Receipt = 1,
        Deliver = 2,
        InvoiceAR = 3,
        InvoiceAP = 4,
        Payment = 5,
        Bill = 6,
        Return = 7,
        CreditMemo = 8,
        DebitMemo = 9,
        ReclassJournal = 10,
        PrePayment = 11,
        CashAdvance = 12,
        CashRealization = 13,
        AdvancePayment = 14,
    }

    public enum PostingMethodType
    {
        None = 0,
        Normal = 1,
        Tax = 2,
        Discount = 3,
        Direct = 4,
        Domestic = 5,
        Advances = 6,
        DownPayment = 7,
    }

    public enum PostingType
    {
        None = 0,
        Purchase = 1,
        Sales = 2,
        Routing = 3,
        Packing = 4,
        Operation = 5,
        Production = 6,
        Account = 7,
        Advances = 8,
        Others = 9,
    }

    public enum PPH21Coloumn
    {
        None = 0,
        Kolom1 = 1,
        Kolom2 = 2,
        Kolom3 = 3,
        Kolom4 = 4,
        Kolom5 = 5,
        Kolom6 = 6,
        Kolom7 = 7,
        Kolom8 = 8,
        Kolom9 = 9,
        Kolom10 = 10,
        Kolom11 = 11,
        Kolom12 = 12,
        Kolom13 = 13,
        Kolom14 = 14,
        Kolom15 = 15,
        Kolom16 = 16,
        Kolom17 = 17,
        Kolom18 = 18,
        Kolom19 = 19,
        Kolom20 = 20,
        Kolom21 = 21,
        Kolom22 = 22,
        Kolom23 = 23,
    }

    public enum PriceGroupType
    {
        None = 0,
        Cutting = 1,
        NonCutting = 2,
    }

    public enum PriorityType
    {
        None = 0,
        Low = 1,
        Middle = 2,
        High = 3
    }

    public enum PTKPStatus
    {

        None = 0,
        TK0 = 1,
        TK1 = 2,
        TK2 = 3,
        TK3 = 4,
        K0 = 5,
        K1 = 6,
        K2 = 7,
        K3 = 8,
        KI0 = 9,
        KI1 = 10,
        KI2 = 11,
        KI3 = 12,
    }

    public enum QCPassed
    {
        None = 0,
        Good = 1,
        Bad = 2,
    }

    public enum ReturnType
    {
        None = 0,
        ReplacedFull = 1,
        CreditMemo = 2,
        DebitMemo = 3,
    }

    public enum SalaryList
    {
        PayrollProcess = 0,
        PayrollProcessDetail = 1,
        SalaryBasic = 2,
        SalaryComponent = 3,
        SalaryGroup = 4,
        SalaryGroupDetail = 5,
    }

    public enum SalaryType
    {
        BasicSalary = 0,
        Allowance = 1,
        Deduction = 2,
    }

    public enum SetupList
    {
        ApplicationSetup = 0,
        Currency = 1,
        CurrencyRate = 2,
        Numbering = 3,
        Period = 4,
    }

    public enum Status
    {
        None = 0,
        Open = 1,
        Progress = 2,
        Posted = 3,
        Close = 4,
        Lock = 5,
        Approved = 6,
    }

    public enum StockType
    {
        None = 0,
        Good = 1,
        Bad = 2
    }

    public enum TaxCalculationMethod
    {
        None = 0,
        Net = 1,
        Gross = 2,
        Mixed = 3,
    }

    public enum TaxGroupType
    {
        None = 0,
        PPN = 1,
        PPH = 2,
        NPWP = 3,
        NonNPWP = 4,
        Materai = 5,
    }

    public enum TaxList
    {
        NonTaxAbleIncomeType = 0,
        TaxRateNonNPWP = 1,
        TaxRateNPWP = 2,
        TaxSetup = 3,
    }

    public enum TaxMode
    {
        None = 0,
        Purchase = 1,
        Sales = 2,
        Both = 3,
    }

    public enum TaxNature
    {
        None = 0,
        Increase = 1,
        Decrease = 2,
    }

    public enum MaterialType
    {
        Normal = 0,
        ConductorPhasa = 1,
        ConductorNetral = 2,
        InsulationPhasa = 3,
        InsulationNetral = 4,
    }

    public enum MaterialComponent
    {
        None = 0,
        SusunanKawat = 1,
        JumlahKawat = 2,
        DiameterKawat = 3,
        DiameterKonduktor = 4,
        PemuluranSebelumPilin = 5,
        PemuluranSetelahPilin = 6,
        TebalRataRataIsolasi = 7,
        MinimalTebalPadaSatuTitikIsolasi = 8,
        DiameterIsolasi = 9,
        WarnaIsolasi = 10,
        DiameterCabling = 11,
        TebalRataRataFiller = 12,
        MinimalTebalPadaSatuTitikFiller = 13,
        DiameterFiller = 14,
        WarnaFiller = 15,
        TebalRataRataSelubung = 16,
        MinimalTebalPadaSatuTitikSelubung = 17,
        DiameterSelubung = 18,
        JumlahTape = 19,
        Tebal = 20,
        Lebar = 21,
        MetodeProses = 22,
        JarakOverlap = 23,
        LebarOverlap = 24,
        DiameterBinder = 25
    }
    
}