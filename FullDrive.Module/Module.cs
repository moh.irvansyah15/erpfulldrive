﻿#region Default

using System;
using System.Text;
using System.Linq;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using System.Collections.Generic;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.BaseImpl.PermissionPolicy;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Updating;
using DevExpress.ExpressApp.Model.Core;
using DevExpress.ExpressApp.Model.DomainLogics;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.Xpo;

#endregion Default

using DevExpress.ExpressApp.ReportsV2;
using FullDrive.Module.BusinessObjects;
using FullDrive.Module.Reports;

namespace FullDrive.Module {
    // For more typical usage scenarios, be sure to check out https://docs.devexpress.com/eXpressAppFramework/DevExpress.ExpressApp.ModuleBase.
    public sealed partial class FullDriveModule : ModuleBase {
        public FullDriveModule() {
            InitializeComponent();
			BaseObject.OidInitializationMode = OidInitializationMode.AfterConstruction;
        }
        public override IEnumerable<ModuleUpdater> GetModuleUpdaters(IObjectSpace objectSpace, Version versionFromDB) {
            ModuleUpdater updater = new DatabaseUpdate.Updater(objectSpace, versionFromDB);
            PredefinedReportsUpdater predefinedReportsUpdater = new PredefinedReportsUpdater(Application, objectSpace, versionFromDB);
            predefinedReportsUpdater.AddPredefinedReport<ReportPO>("Report PO", typeof(PurchaseOrder), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportPOInternal>("Report PO Internal", typeof(PurchaseOrder), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportPOEksternal>("Report PO Eksternal", typeof(PurchaseOrder), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportPI>("Report PI", typeof(PurchaseInvoice), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportPIEksternal>("Report PI Eksternal", typeof(PurchaseInvoice), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportPIInternal>("Report PI Internal", typeof(PurchaseInvoice), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportPIAfterPosting>("Report PI After Posting", typeof(PurchaseInvoice), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportPIAfterPostingEksternal>("Report PI After Posting Eksternal", typeof(PurchaseInvoice), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportPIAfterPostingInternal>("Report PI After Posting Internal", typeof(PurchaseInvoice), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportBPU>("Report Payable", typeof(PurchaseInvoice), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportBPUInternal>("Report Payable Internal", typeof(PurchaseInvoice), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportBPUForCBD>("Report Payable For CBD", typeof(PurchaseInvoice), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportBPUForCBDAfterPosting>("Report Payable For CBD After Posting", typeof(PurchaseInvoice), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportBPUForCBDInternal>("Report Payable Internal For CBD", typeof(PurchaseInvoice), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportBPUAfterPosting>("Report Payable After Posting", typeof(PurchaseInvoice), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportBPUAfterPostingInternal>("Report Payable After Posting Internal", typeof(PurchaseInvoice), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportPR>("Report PR", typeof(PurchaseRequisition), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportPRAfterPosting>("Report PR After Posting", typeof(PurchaseRequisition), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportSO>("Report SO", typeof(SalesOrder), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportSOInternal>("Report SO Internal", typeof(SalesOrder), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportSI>("Report SI", typeof(SalesInvoice), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportSIInternal>("Report SI Internal", typeof(SalesInvoice), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportSIAfterPosting>("Report SI After Posting", typeof(SalesInvoice), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportSIAfterPostingInternal>("Report SI After Posting Internal", typeof(SalesInvoice), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportReceivable>("Report Receivable", typeof(SalesInvoice), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportReceivableInternal>("Report Receivable Internal", typeof(SalesInvoice), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportReceivableAfterPosting>("Report Receivable After Posting", typeof(SalesInvoice), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportReceivableAfterPostingInternal>("Report Receivable After Posting Internal", typeof(SalesInvoice), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportSQ>("Report SQ", typeof(SalesQuotation), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportSQInternal>("Report SQ Internal", typeof(SalesQuotation), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportSQAfterPosting>("Report SQ After Posting", typeof(SalesQuotation), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportSQAfterPostingInternal>("Report SQ After Posting Internal", typeof(SalesQuotation), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportRAB>("Budget Plan", typeof(ProjectHeader), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportContract>("Contract", typeof(ProjectHeader), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportITIn>("Report BPB", typeof(InventoryTransferIn), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportITInAfterPosting>("Report BPB After Posting", typeof(InventoryTransferIn), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportDO>("Report DO", typeof(InventoryTransferOut), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportDOAfterPosting>("Report DO After Posting", typeof(InventoryTransferOut), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportDOPasar_Peminjaman>("Report DO Pasar & Peminjaman", typeof(InventoryTransferOut), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportDOCanvasing_NonPPN>("Report DO Non PPN & Canvasing", typeof(InventoryTransferOut), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportDOPLN_Freemarket_Return>("Report DO PLN, Freemarket & Return", typeof(InventoryTransferOut), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportDOPasar_Peminjaman_AP>("Report DO After Posting Pasar & Peminjaman", typeof(InventoryTransferOut), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportDOCanvasing_NonPPN_AP>("Report DO After Posting Non PPN & Canvasing", typeof(InventoryTransferOut), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportDOPLN_Freemarket_Return_AP>("Report DO After Posting PLN, Freemarket & Return", typeof(InventoryTransferOut), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportTO>("Report TO", typeof(TransferOrder), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportTOInternal>("Report TO Internal", typeof(TransferOrder), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportTOAfterPosting>("Report TO After Posting", typeof(TransferOrder), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportTOAfterPostingInternal>("Report TO After Posting Internal", typeof(TransferOrder), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportSalesPrePayment>("Report Sales Pre Payment Invoice", typeof(SalesPrePaymentInvoice), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportPurchasePrePayment>("Report Purchase Pre Payment Invoice", typeof(PurchasePrePaymentInvoice), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportTransferIn>("Report Transfer In", typeof(TransferIn), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportTransferOut>("Report Transfer Out", typeof(TransferOut), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportIP>("Report Production", typeof(InventoryProduction), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportIPCheck>("Report Production Check", typeof(InventoryProduction), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportLN>("Report Loading Note", typeof(InventoryTransferOutLine), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportKPGood>("Report Production Good", typeof(InventoryProduction), isInplaceReport: true);
            predefinedReportsUpdater.AddPredefinedReport<ReportKPBad>("Report Production Bad", typeof(InventoryProduction), isInplaceReport: true);
            return new ModuleUpdater[] { updater, predefinedReportsUpdater };
        }
        public override void Setup(XafApplication application) {
            base.Setup(application);
            // Manage various aspects of the application UI and behavior at the module level.
        }
        public override void CustomizeTypesInfo(ITypesInfo typesInfo) {
            base.CustomizeTypesInfo(typesInfo);
            CalculatedPersistentAliasHelper.CustomizeTypesInfo(typesInfo);
        }
    }
}
