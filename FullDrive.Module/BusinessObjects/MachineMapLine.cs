﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [NavigationItem("Production")]
    [DefaultProperty("Code")]
    [RuleCombinationOfPropertiesIsUnique("MachineMapLineRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class MachineMapLine : BaseObject
    {
        #region Default

        private bool _activationPosting;
        //private bool _select;
        private int _no;
        private string _code;
        private string _name;
        private MachineMapVersion _machineMapVersion;
        private Item _item;
        private Machine _machine;
        private bool _active;
        private string _description;       
        private Status _status;
        private DateTime _statusDate;
        private ProductionLine _productionLine;
        private Company _company;
        private GlobalFunction _globFunc;

        public MachineMapLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.MachineMapLine);
                DateTime now = DateTime.Now;
                this.Status = CustomProcess.Status.Open;
                this.StatusDate = now;
            }
        }

        //No
        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }
        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        #endregion Default

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        //[Appearance("MachineMapLineSelectEnabled", Criteria = "ActivationPosting = true", Enabled = false)]
        //public bool Select
        //{
        //    get { return _select; }
        //    set { SetPropertyValue("Select", ref _select, value); }
        //}

        [Appearance("MachineMapLineNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("MachineMapLineCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("MachineMapLineNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Appearance("MachineMapLineDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Association("MachineMapVersion-MachineMapLines")]
        public MachineMapVersion MachineMapVersion
        {
            get { return _machineMapVersion; }
            set { SetPropertyValue("MachineMapVersion", ref _machineMapVersion, value); }
        }

        [Appearance("MachineMapLineItemClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Item Item
        {
            get { return _item; }
            set { SetPropertyValue("Item", ref _item, value); }
        }

        [Appearance("MachineMapLineMachineClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Machine Machine
        {
            get { return _machine; }
            set { SetPropertyValue("Machine", ref _machine, value); }
        }

        [ImmediatePostData()]
        [Association("ProductionLine-MachineMapLines")]
        [Appearance("MachineMapLineProductionLineEnabled", Enabled = false)]
        public ProductionLine ProductionLine
        {
            get { return _productionLine; }
            set { SetPropertyValue("ProductionLine", ref _productionLine, value); }
        }

        //[Appearance("MachineMapLineStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("MachineMapLineStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Browsable(false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        #endregion Field

        //==== Code Only ====

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.ProductionLine != null)
                    {
                        object _makRecord = Session.Evaluate<MachineMapLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("ProductionLine=?", this.ProductionLine));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = MachineMapLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.ProductionLine != null)
                {
                    ProductionLine _numHeader = Session.FindObject<ProductionLine>
                                                (new BinaryOperator("Code", this.ProductionLine.Code));

                    XPCollection<MachineMapLine> _numLines = new XPCollection<MachineMapLine>
                                                             (Session, new BinaryOperator("ProductionLine", _numHeader),
                                                              new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (MachineMapLine _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = MachineMapLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.ProductionLine != null)
                {
                    ProductionLine _numHeader = Session.FindObject<ProductionLine>
                                                (new BinaryOperator("Code", this.ProductionLine.Code));

                    XPCollection<MachineMapLine> _numLines = new XPCollection<MachineMapLine>
                                                             (Session, new GroupOperator(GroupOperatorType.And,
                                                              new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                              new BinaryOperator("ProductionLine", _numHeader)),
                                                              new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (MachineMapLine _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = MachineMapLine " + ex.ToString());
            }
        }

        #endregion No

    }
}