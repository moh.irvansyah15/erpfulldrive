﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("GeneralJournalRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class GeneralJournal : FullDriveSysBaseObject
    {
        #region Default

        private string _code;
        private string _name;
        private DateTime _postingDate;
        private PostingType _postingType;
        private OrderType _orderType;
        private PostingMethod _postingMethod;
        private PostingMethodType _postingMethodType;
        private ChartOfAccount _account;
        private string _description;
        private double _debit;
        private double _credit;
        private PurchaseOrder _purchaseOrder;
        private PurchaseReturn _purchaseReturn;
        private InventoryTransferOut _inventoryTransferOut;
        private InventoryTransferOutLine _inventoryTransferOutLine;
        private InventoryTransferIn _inventoryTransferIn;
        private PurchaseInvoice _purchaseInvoice;
        private PurchasePrePaymentInvoice _purchasePrePaymentInvoice;
        private SalesOrder _salesOrder;
        private SalesOrderLine _salesOrderLine;
        private SalesInvoice _salesInvoice;
        private SalesPrePaymentInvoice _salesPrePaymentInvoice;
        private PayableTransaction _payableTransaction;
        private ReceivableTransaction _receivableTransaction;
        private CreditMemo _creditMemo;
        private AccountReclassJournal _accountReclassJournal;
        private string _userAccess;
        private Company _company;
        private GlobalFunction _globFunc;
        //clm
        private SalesReturn _salesReturn;
        private DebitMemo _debitMemo;
        private CashAdvance _cashAdvance;
        private PaymentRealization _paymentRealization;

        public GeneralJournal(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.GeneralJournal);

                _userAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                if (Session.IsNewObject(this))
                {
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null)
                            {
                                Company = _locUserAccess.Employee.Company;
                            }
                        }
                    }
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #endregion Default

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("GeneralJournalCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        public DateTime PostingDate
        {
            get { return _postingDate; }
            set { SetPropertyValue("PostingDate", ref _postingDate, value); }
        }

        public PostingType PostingType
        {
            get { return _postingType; }
            set { SetPropertyValue("PostingType", ref _postingType, value); }
        }

        public OrderType OrderType
        {
            get { return _orderType; }
            set { SetPropertyValue("OrderType", ref _orderType, value); }
        }

        public PostingMethod PostingMethod
        {
            get { return _postingMethod; }
            set { SetPropertyValue("PostingMethod", ref _postingMethod, value); }
        }

        public PostingMethodType PostingMethodType
        {
            get { return _postingMethodType; }
            set { SetPropertyValue("PostingMethodType", ref _postingMethodType, value); }
        }

        public ChartOfAccount Account
        {
            get { return _account; }
            set { SetPropertyValue("Account", ref _account, value); }
        }

        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        public double Debit
        {
            get { return _debit; }
            set { SetPropertyValue("Debit", ref _debit, value); }
        }

        public double Credit
        {
            get { return _credit; }
            set { SetPropertyValue("Credit", ref _credit, value); }
        }

        [Appearance("GeneralJournalPurchaseOrderClose", Enabled = false)]
        public PurchaseOrder PurchaseOrder
        {
            get { return _purchaseOrder; }
            set { SetPropertyValue("PurchaseOrder", ref _purchaseOrder, value); }
        }

        [Appearance("GeneralJournalPurchaseReturnClose", Enabled = false)]
        public PurchaseReturn PurchaseReturn
        {
            get { return _purchaseReturn; }
            set { SetPropertyValue("PurchaseReturn", ref _purchaseReturn, value); }
        }

        [Appearance("GeneralJournalInventoryTransferOutClose", Enabled = false)]
        public InventoryTransferOut InventoryTransferOut
        {
            get { return _inventoryTransferOut; }
            set { SetPropertyValue("InventoryTransferOut", ref _inventoryTransferOut, value); }
        }

        [Appearance("GeneralJournalInventoryTransferOutLineClose", Enabled = false)]
        public InventoryTransferOutLine InventoryTransferOutLine
        {
            get { return _inventoryTransferOutLine; }
            set { SetPropertyValue("InventoryTransferOutLine", ref _inventoryTransferOutLine, value); }
        }

        [Appearance("GeneralJournalInventoryTransferInClose", Enabled = false)]
        public InventoryTransferIn InventoryTransferIn
        {
            get { return _inventoryTransferIn; }
            set { SetPropertyValue("InventoryTransferIn", ref _inventoryTransferIn, value); }
        }

        [Appearance("GeneralJournalPurchaseInvoiceClose", Enabled = false)]
        public PurchaseInvoice PurchaseInvoice
        {
            get { return _purchaseInvoice; }
            set { SetPropertyValue("PurchaseInvoice", ref _purchaseInvoice, value); }
        }

        [Appearance("GeneralJournalPurchasePrePaymentInvoiceClose", Enabled = false)]
        public PurchasePrePaymentInvoice PurchasePrePaymentInvoice
        {
            get { return _purchasePrePaymentInvoice; }
            set { SetPropertyValue("PurchasePrePaymentInvoice", ref _purchasePrePaymentInvoice, value); }
        }

        [Appearance("GeneralJournalSalesOrderClose", Enabled = false)]
        public SalesOrder SalesOrder
        {
            get { return _salesOrder; }
            set { SetPropertyValue("SalesOrder", ref _salesOrder, value); }
        }

        [Appearance("GeneralJournalSalesOrderLineClose", Enabled = false)]
        public SalesOrderLine SalesOrderLine
        {
            get { return _salesOrderLine; }
            set { SetPropertyValue("SalesOrderLine", ref _salesOrderLine, value); }
        }

        [Appearance("GeneralJournalSalesInvoiceClose", Enabled = false)]
        public SalesInvoice SalesInvoice
        {
            get { return _salesInvoice; }
            set { SetPropertyValue("SalesInvoice", ref _salesInvoice, value); }
        }

        [Appearance("GeneralJournalSalesPrePaymentInvoiceClose", Enabled = false)]
        public SalesPrePaymentInvoice SalesPrePaymentInvoice
        {
            get { return _salesPrePaymentInvoice; }
            set { SetPropertyValue("SalesPrePaymentInvoice", ref _salesPrePaymentInvoice, value); }
        }

        [Appearance("GeneralJournalPayableTransactionClose", Enabled = false)]
        public PayableTransaction PayableTransaction
        {
            get { return _payableTransaction; }
            set { SetPropertyValue("PayableTransaction", ref _payableTransaction, value); }
        }

        [Appearance("GeneralJournalReceivableTransactionClose", Enabled = false)]
        public ReceivableTransaction ReceivableTransaction
        {
            get { return _receivableTransaction; }
            set { SetPropertyValue("ReceivableTransaction", ref _receivableTransaction, value); }
        }

        [Appearance("GeneralJournalCreditMemoClose", Enabled = false)]
        public CreditMemo CreditMemo
        {
            get { return _creditMemo; }
            set { SetPropertyValue("CreditMemo", ref _creditMemo, value); }
        }

        [Appearance("GeneralJournalAccountReclassJournalClose", Enabled = false)]
        public AccountReclassJournal AccountReclassJournal
        {
            get { return _accountReclassJournal; }
            set { SetPropertyValue("AccountReclassJournal", ref _accountReclassJournal, value); }
        }

        [DataSourceCriteria("Active = true")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        //clm
        [Appearance("GeneralJournalSalesReturnClose", Enabled = false)]
        public SalesReturn SalesReturn
        {
            get { return _salesReturn; }
            set { SetPropertyValue("SalesReturn", ref _salesReturn, value); }
        }

        [Appearance("GeneralJournalDebitMemoClose", Enabled = false)]
        public DebitMemo DebitMemo
        {
            get { return _debitMemo; }
            set { SetPropertyValue("DebitMemo", ref _debitMemo, value); }
        }

        [Appearance("GeneralJournalCashAdvanceClose", Enabled = false)]
        public CashAdvance CashAdvance
        {
            get { return _cashAdvance; }
            set { SetPropertyValue("CashAdvance", ref _cashAdvance, value); }
        }

        [Appearance("GeneralJournalPaymentRealizationClose", Enabled = false)]
        public PaymentRealization PaymentRealization
        {
            get { return _paymentRealization; }
            set { SetPropertyValue("PaymentRealization", ref _paymentRealization, value); }
        }

        #endregion Field

    }
}