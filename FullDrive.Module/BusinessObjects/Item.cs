﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Inventory")]
    [RuleCombinationOfPropertiesIsUnique("ItemRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class Item : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private ObjectList _objectList;
        XPCollection<NumberingLine> _availableNumberingLine;
        private NumberingLine _numbering;
        private string _name;
        private string _shortName;
        private UnitOfMeasure _basedUOM;
        private DateTime _startDate;
        private DateTime _endDate;
        private ItemGroup _itemGroup;
        private ItemType _itemType;
        private ItemAccountGroup _itemAccountGroup;
        private BillOfMaterialVersion _billOfMaterialVersion;
        private Brand _brand;
        private OrderType _orderType;
        private bool _pis;
        private Item _partItemService;
        private string _description;
        private string _description2;
        private ProductCode _productCode;
        private Company _company;
        private bool _active;
        private GlobalFunction _globFunc;

        public Item(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.Item);
                this.ObjectList = CustomProcess.ObjectList.Item;
                this.OrderType = OrderType.Item;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Browsable(false)]
        public ObjectList ObjectList
        {
            get { return _objectList; }
            set { SetPropertyValue("ObjectList", ref _objectList, value); }
        }

        [Browsable(false)]
        public XPCollection<NumberingLine> AvailableNumberingLine
        {
            get
            {
                if (ObjectList == ObjectList.None)
                {
                    _availableNumberingLine = new XPCollection<NumberingLine>(Session);
                }
                else
                {
                    _availableNumberingLine = new XPCollection<NumberingLine>(Session,
                                              new GroupOperator(GroupOperatorType.And,
                                              new BinaryOperator("ObjectList", this.ObjectList),
                                              new BinaryOperator("Selection", true),
                                              new BinaryOperator("Active", true)));
                }
                return _availableNumberingLine;

            }
        }

        [NonPersistent()]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableNumberingLine", DataSourcePropertyIsNullMode.SelectAll)]
        public NumberingLine Numbering
        {
            get { return _numbering; }
            set
            {
                NumberingLine _oldNumbering = _numbering;
                SetPropertyValue("Numbering", ref _numbering, value);
                if (!IsLoading && _numbering != null && _numbering != _oldNumbering)
                {
                    this.Code = _globFunc.GetNumberingSelectionUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.Item, _numbering);
                }
            }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        public string ShortName
        {
            get { return _shortName; }
            set { SetPropertyValue("ShortName", ref _shortName, value); }
        }

        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure BasedUOM
        {
            get { return _basedUOM; }
            set { SetPropertyValue("BasedUOM", ref _basedUOM, value); }
        }

        public DateTime StartDate
        {
            get { return _startDate; }
            set { SetPropertyValue("StartDate", ref _startDate, value); }
        }

        public DateTime EndDate
        {
            get { return _endDate; }
            set { SetPropertyValue("EndDate", ref _endDate, value); }
        }

        [Association("ItemGroup-Items")]
        [DataSourceCriteria("Active = true")]
        public ItemGroup ItemGroup
        {
            get { return _itemGroup; }
            set { SetPropertyValue("ItemGroup", ref _itemGroup, value); }
        }

        [DataSourceCriteria("Active = true")]
        public ItemType ItemType
        {
            get { return _itemType; }
            set { SetPropertyValue("ItemType", ref _itemType, value); }
        }

        [Association("ItemAccountGroup-Items")]
        public ItemAccountGroup ItemAccountGroup
        {
            get { return _itemAccountGroup; }
            set { SetPropertyValue("ItemAccountGroup", ref _itemAccountGroup, value); }
        }

        public BillOfMaterialVersion BillOfMaterialVersion
        {
            get { return _billOfMaterialVersion; }
            set { SetPropertyValue("BillOfMaterialVersion", ref _billOfMaterialVersion, value); }
        }

        public Brand Brand
        {
            get { return _brand; }
            set { SetPropertyValue("Brand", ref _brand, value); }
        }

        [ImmediatePostData()]
        public OrderType OrderType
        {
            get { return _orderType; }
            set
            {
                SetPropertyValue("OrderType", ref _orderType, value);
                if (!IsLoading)
                {
                    if (this._orderType == OrderType.Service)
                    {
                        this.PIS = true;
                    }
                    else
                    {
                        this.PIS = false;
                        this.PartItemService = null;
                    }
                }
            }
        }

        [Browsable(false)]
        public bool PIS
        {
            get { return _pis; }
            set { SetPropertyValue("PIS", ref _pis, value); }
        }

        [Appearance("PurchaseRequisitionLineSelectClose", Criteria = "PIS = false", Enabled = false)]
        public Item PartItemService
        {
            get { return _partItemService; }
            set { SetPropertyValue("PartItemService", ref _partItemService, value); }
        }

        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Size(512)]
        public string Description2
        {
            get { return _description2; }
            set { SetPropertyValue("Description2", ref _description2, value); }
        }

        [Association("ProductCode-Items")]
        public ProductCode ProductCode
        {
            get { return _productCode; }
            set
            {
                SetPropertyValue("ProductCode", ref _productCode, value);
                if(!IsLoading)
                {
                    if(this._productCode != null)
                    {
                        if(this._productCode.FullName != null)
                        {
                            this.Code = this._productCode.FullName;
                        }
                    }
                }
            }
        }

        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        [Association("Item-MaterialPrices")]
        public XPCollection<MaterialPrice> MaterialPrices
        {
            get { return GetCollection<MaterialPrice>("MaterialPrices"); }
        }

        [Association("Item-OverheadCosts")]
        public XPCollection<OverheadCost> OverheadCosts
        {
            get { return GetCollection<OverheadCost>("OverheadCosts"); }
        }

        [Association("Item-CostOfGoodsSolds")]
        public XPCollection<CostOfGoodsSold> CostOfGoodsSolds
        {
            get { return GetCollection<CostOfGoodsSold>("CostOfGoodsSolds"); }
        }

        [Association("Item-ItemUnitOfMeasures")]
        public XPCollection<ItemUnitOfMeasure> ItemUnitOfMeasures
        {
            get { return GetCollection<ItemUnitOfMeasure>("ItemUnitOfMeasures"); }
        }

        [Association("Item-ItemComponents")]
        public XPCollection<ItemComponent> ItemComponents
        {
            get { return GetCollection<ItemComponent>("ItemComponents"); }
        }

        [Association("Item-ItemBillOfMaterials")]
        public XPCollection<ItemBillOfMaterial> ItemBillOfMaterials
        {
            get { return GetCollection<ItemBillOfMaterial>("ItemBillOfMaterials"); }
        }

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}

        //================================================== Code Only ==================================================

    }
}