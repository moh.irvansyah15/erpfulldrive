﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Inventory")]
    [RuleCombinationOfPropertiesIsUnique("TransferOrderRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class TransferOrder : FullDriveSysBaseObject
    {
        #region Default

        private bool _activationPosting;
        private string _code;
        private DirectionType _transferType;
        private InventoryMovingType _inventoryMovingType;
        private ObjectList _objectList;
        XPCollection<DocumentType> _availableDocumentType;
        private DocumentType _documentType;
        private string _docNo;
        private XPCollection<Location> _availableLocationFrom;
        private Location _locationFrom;
        private XPCollection<Location> _availableLocationTo;
        private Location _locationTo;
        private DateTime _estimatedDate;
        private Status _status;
        private DateTime _statusDate;
        private Company _company;
        private ProjectHeader _projectHeader;
        private string _userAccess;
        private FinishProduction _finishProduction;
        private TransferOrder _internalTransferOrder;
        private InventoryTransferOrder _inventoryTransferOrder;
        private GlobalFunction _globFunc;
        //clm
        private bool _activeApproved1;
        private bool _activeApproved2;
        private bool _activeApproved3;

        public TransferOrder(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.TransferOrder);
                this.Status = Status.Open;
                this.TransferType = DirectionType.Internal;
                this.StatusDate = now;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #endregion Default

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [ImmediatePostData()]
        [Appearance("TransferOrderCodeClose", Enabled = false)]
        [Appearance("TransferOrderRedColor", Criteria = "ActiveApproved1 = true", BackColor = "#fd79a8")]
        [Appearance("TransferOrderYellowColor", Criteria = "ActiveApproved2 = true", BackColor = "#ffeaa7")]
        [Appearance("TransferOrderGreenOColor", Criteria = "ActiveApproved3 = true", BackColor = "#81ecec")]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("TransferOrderCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("TransferOrderTransferTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public DirectionType TransferType
        {
            get { return _transferType; }
            set
            {
                SetPropertyValue("TransferType", ref _transferType, value);
                if (!IsLoading)
                {

                }
            }
        }

        [Appearance("TransferOrderInventoryMovingTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public InventoryMovingType InventoryMovingType
        {
            get { return _inventoryMovingType; }
            set
            {
                SetPropertyValue("InventoryMovingType", ref _inventoryMovingType, value);
                if(!IsLoading)
                {
                    if(this._inventoryMovingType == InventoryMovingType.TransferOut)
                    {
                        this.ObjectList = ObjectList.TransferOut;
                    }else if(this._inventoryMovingType == InventoryMovingType.TransferIn)
                    {
                        this.ObjectList = ObjectList.TransferIn;
                    }
                }
            }
        }

        [Browsable(false)]
        public ObjectList ObjectList
        {
            get { return _objectList; }
            set { SetPropertyValue("ObjectList", ref _objectList, value); }
        }

        [Browsable(false)]
        public XPCollection<DocumentType> AvailableDocumentType
        {
            get
            {
                if (ObjectList == ObjectList.None)
                {
                    _availableDocumentType = new XPCollection<DocumentType>(Session);
                }
                else
                {
                    _availableDocumentType = new XPCollection<DocumentType>(Session,
                                              new GroupOperator(GroupOperatorType.And,
                                              new BinaryOperator("ObjectList", this.ObjectList),
                                              new BinaryOperator("InventoryMovingType", this.InventoryMovingType),
                                              new BinaryOperator("TransferType", this.TransferType),
                                              new BinaryOperator("Active", true)));
                }
                return _availableDocumentType;

            }
        }

        [Appearance("TransferOrderDocumentTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableDocumentType", DataSourcePropertyIsNullMode.SelectAll)]
        public DocumentType DocumentType
        {
            get { return _documentType; }
            set
            {
                SetPropertyValue("DocumentType", ref _documentType, value);
                if (!IsLoading)
                {
                    if (_documentType != null)
                    {
                        _globFunc = new GlobalFunction();
                        this.DocNo = _globFunc.GetDocumentNumberingUnlockOptimisticRecord(this.Session.DataLayer, this._documentType);
                    }
                }
            }
        }

        [Appearance("TransferOrderDocNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string DocNo
        {
            get { return _docNo; }
            set { SetPropertyValue("DocNo", ref _docNo, value); }
        }

        #region LocationFrom

        [Browsable(false)]
        public XPCollection<Location> AvailableLocationFrom
        {
            get
            {
                _userAccess = SecuritySystem.CurrentUserName;
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                string _locLocationCode = null;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                List<string> _stringLocation = new List<string>();
                if (_locUserAccess != null)
                {
                    #region TransferOut
                    if ((this.TransferType == DirectionType.Internal) && this.InventoryMovingType == InventoryMovingType.TransferOut)
                    {
                        
                        XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                                                  (Session, new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("UserAccess", _locUserAccess),
                                                                                   new BinaryOperator("Owner", true),
                                                                                   new BinaryOperator("Active", true)));

                        if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                        {
                            foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                            {
                                if(_locWhsSetupDetail.Location != null)
                                {
                                    if(_locWhsSetupDetail.Location.Code != null)
                                    {
                                        _locLocationCode = _locWhsSetupDetail.Location.Code;
                                        _stringLocation.Add(_locLocationCode);
                                    }
                                }    
                            }
                        }

                        IEnumerable<string> _stringArrayLocationFromDistinct = _stringLocation.Distinct();
                        string[] _stringArrayLocationFromList = _stringArrayLocationFromDistinct.ToArray();
                        if (_stringArrayLocationFromList.Length == 1)
                        {
                            for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                            {
                                Location _locLocationFrom = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                if (_locLocationFrom != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locLocationFrom.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArrayLocationFromList.Length > 1)
                        {
                            for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                            {
                                Location _locLocationFrom = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                if (_locLocationFrom != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locLocationFrom.Code + "'";
                                    }
                                    else
                                    {
                                        _endString = " OR [Code]=='" + _locLocationFrom.Code + "'";
                                    }
                                }
                            }
                        }
                        _fullString = _beginString + _endString;

                        if (_fullString != null)
                        {
                            _availableLocationFrom = new XPCollection<Location>(Session, CriteriaOperator.Parse(_fullString));
                        }
                    }
                    #endregion TransferOut
                    #region TransferIn
                    else if ((this.TransferType == DirectionType.Internal) && this.InventoryMovingType == InventoryMovingType.TransferIn)
                    {
                        XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                                                  (Session, new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("UserAccess", _locUserAccess),
                                                                                   new BinaryOperator("Active", true)));

                        if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                        {
                            foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                            {
                                if (_locWhsSetupDetail.Location != null)
                                {
                                    if (_locWhsSetupDetail.Location.Code != null)
                                    {
                                        _locLocationCode = _locWhsSetupDetail.Location.Code;
                                        _stringLocation.Add(_locLocationCode);
                                    }
                                }
                            }
                        }

                        IEnumerable<string> _stringArrayLocationFromDistinct = _stringLocation.Distinct();
                        string[] _stringArrayLocationFromList = _stringArrayLocationFromDistinct.ToArray();
                        if (_stringArrayLocationFromList.Length == 1)
                        {
                            for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                            {
                                Location _locLocationFrom = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                if (_locLocationFrom != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locLocationFrom.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArrayLocationFromList.Length > 1)
                        {
                            for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                            {
                                Location _locLocationFrom = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                if (_locLocationFrom != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locLocationFrom.Code + "'";
                                    }
                                    else
                                    {
                                        _endString = " OR [Code]=='" + _locLocationFrom.Code + "'";
                                    }
                                }
                            }
                        }
                        _fullString = _beginString + _endString;

                        if (_fullString != null)
                        {
                            _availableLocationFrom = new XPCollection<Location>(Session, CriteriaOperator.Parse(_fullString));
                        }
                    }
                    #endregion TransferIn
                }
                else
                {
                    _availableLocationFrom = new XPCollection<Location>(Session);
                }
                return _availableLocationFrom;

            }
        }

        [Appearance("TransferOrderLocationFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableLocationFrom", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        [ImmediatePostData()]
        public Location LocationFrom
        {
            get { return _locationFrom; }
            set { SetPropertyValue("LocationFrom", ref _locationFrom, value); }
        }

        #endregion LocationFrom

        #region LocationTo

        [Browsable(false)]
        public XPCollection<Location> AvailableLocationTo
        {
            get
            {
                _userAccess = SecuritySystem.CurrentUserName;
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                string _locLocationCode = null;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                List<string> _stringLocation = new List<string>();
                if (_locUserAccess != null)
                {
                    #region TransferOut
                    if (this.TransferType == DirectionType.Internal && this.InventoryMovingType == InventoryMovingType.TransferOut)
                    {
                        XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                                                  (Session, new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("UserAccess", _locUserAccess),
                                                                                   new BinaryOperator("Active", true)));

                        if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                        {
                            foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                            {
                                if (_locWhsSetupDetail.Location != null)
                                {
                                    if (_locWhsSetupDetail.Location.Code != null)
                                    {
                                        _locLocationCode = _locWhsSetupDetail.Location.Code;
                                        _stringLocation.Add(_locLocationCode);
                                    }
                                }       
                            }
                        }

                        IEnumerable<string> _stringArrayLocationToDistinct = _stringLocation.Distinct();
                        string[] _stringArrayLocationToList = _stringArrayLocationToDistinct.ToArray();
                        if (_stringArrayLocationToList.Length == 1)
                        {
                            for (int i = 0; i < _stringArrayLocationToList.Length; i++)
                            {
                                Location _locLocationTo = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationToList[i]));
                                if (_locLocationTo != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locLocationTo.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArrayLocationToList.Length > 1)
                        {
                            for (int i = 0; i < _stringArrayLocationToList.Length; i++)
                            {
                                Location _locLocationTo = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationToList[i]));
                                if (_locLocationTo != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locLocationTo.Code + "'";
                                    }
                                    else
                                    {
                                        _endString = " OR [Code]=='" + _locLocationTo.Code + "'";
                                    }
                                }
                            }
                        }
                        _fullString = _beginString + _endString;

                        if (_fullString != null)
                        {
                            _availableLocationTo = new XPCollection<Location>(Session, CriteriaOperator.Parse(_fullString));
                        }
                    }

                    #endregion TransferOut
                    #region TransferIn
                    else if (this.TransferType == DirectionType.Internal && this.InventoryMovingType == InventoryMovingType.TransferIn)
                    {
                        XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                                                  (Session, new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("UserAccess", _locUserAccess),
                                                                                   new BinaryOperator("Owner", true),
                                                                                   new BinaryOperator("Active", true)));

                        if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                        {
                            foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                            {
                                if (_locWhsSetupDetail.Location != null)
                                {
                                    if (_locWhsSetupDetail.Location.Code != null)
                                    {
                                        _locLocationCode = _locWhsSetupDetail.Location.Code;
                                        _stringLocation.Add(_locLocationCode);
                                    }
                                }    
                            }
                        }

                        IEnumerable<string> _stringArrayLocationToDistinct = _stringLocation.Distinct();
                        string[] _stringArrayLocationToList = _stringArrayLocationToDistinct.ToArray();
                        if (_stringArrayLocationToList.Length == 1)
                        {
                            for (int i = 0; i < _stringArrayLocationToList.Length; i++)
                            {
                                Location _locLocationTo = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationToList[i]));
                                if (_locLocationTo != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locLocationTo.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArrayLocationToList.Length > 1)
                        {
                            for (int i = 0; i < _stringArrayLocationToList.Length; i++)
                            {
                                Location _locLocationTo = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationToList[i]));
                                if (_locLocationTo != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locLocationTo.Code + "'";
                                    }
                                    else
                                    {
                                        _endString = " OR [Code]=='" + _locLocationTo.Code + "'";
                                    }
                                }
                            }
                        }
                        _fullString = _beginString + _endString;

                        if (_fullString != null)
                        {
                            _availableLocationTo = new XPCollection<Location>(Session, CriteriaOperator.Parse(_fullString));
                        }
                    }
                    #endregion TransferIn
                }
                else
                {
                    _availableLocationTo = new XPCollection<Location>(Session);
                }

                return _availableLocationTo;

            }
        }

        [Appearance("TransferOrderLineLocationToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableLocationTo", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        [ImmediatePostData]
        public Location LocationTo
        {
            get { return _locationTo; }
            set { SetPropertyValue("LocationTo", ref _locationTo, value); }
        }

        #endregion LocationTo

        [Appearance("TransferOrderEstimatedDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime EstimatedDate
        {
            get { return _estimatedDate; }
            set { SetPropertyValue("EstimatedDate", ref _estimatedDate, value); }
        }

        [Appearance("TransferOrderStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("TransferOrderStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("TransferOrderCompanyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("TransferOrderProjectHeaderClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Association("ProjectHeader-TransferOrders")]
        public ProjectHeader ProjectHeader
        {
            get { return _projectHeader; }
            set { SetPropertyValue("ProjectHeader", ref _projectHeader, value); }
        }

        //Wahab 27-02-2020
        //[Association("InventoryTransferOrder-TransferOrders")]
        [Appearance("TransferOrderInventoryTransferOrderClose", Criteria = "ActivationPosting = true", Enabled = false)]
        
        public InventoryTransferOrder InventoryTransferOrder
        {
            get { return _inventoryTransferOrder; }
            set { SetPropertyValue("InventoryTransferOrder", ref _inventoryTransferOrder, value); }
        }

        [Appearance("TransferOrderFinishProductionEnabled", Enabled = false)]
        public FinishProduction FinishProduction
        {
            get { return _finishProduction; }
            set { SetPropertyValue("FinishProduction", ref _finishProduction, value); }
        }

        [Browsable(false)]
        public TransferOrder InternalTransferOrder
        {
            get { return _internalTransferOrder; }
            set { SetPropertyValue("InternalTransferOrder", ref _internalTransferOrder, value); }
        }

        [Association("TransferOrder-TransferOrderLines")]
        public XPCollection<TransferOrderLine> TransferOrderLines
        {
            get { return GetCollection<TransferOrderLine>("TransferOrderLines"); }
        }

        [Association("TransferOrder-ApprovalLines")]
        public XPCollection<ApprovalLine> ApprovalLines
        {
            get { return GetCollection<ApprovalLine>("ApprovalLines"); }
        }

        //Wahab 27-02-2020

        //[Association("TransferOrder-TransferOuts")]
        //public XPCollection<TransferOut> TransferOuts
        //{
        //    get { return GetCollection<TransferOut>("TransferOuts"); }
        //}

        //[Association("TransferOrder-TransferIns")]
        //public XPCollection<TransferIn> TransferIns
        //{
        //    get { return GetCollection<TransferIn>("TransferIns"); }
        //}

        //clm
        [Browsable(false)]
        [Appearance("TransferOrderActiveApproved1Close", Enabled = false)]
        public bool ActiveApproved1
        {
            get { return _activeApproved1; }
            set { SetPropertyValue("ActiveApproved1", ref _activeApproved1, value); }
        }

        [Browsable(false)]
        [Appearance("TransferOrderActiveApproved2Close", Enabled = false)]
        public bool ActiveApproved2
        {
            get { return _activeApproved2; }
            set { SetPropertyValue("ActiveApproved2", ref _activeApproved2, value); }
        }

        [Browsable(false)]
        [Appearance("TransferOrderActiveApproved3Close", Enabled = false)]
        public bool ActiveApproved3
        {
            get { return _activeApproved3; }
            set { SetPropertyValue("ActiveApproved3", ref _activeApproved3, value); }
        }

        #endregion Field

        //==== Code Only ====

        private void SetObjectList()
        {
            try
            {
                if (this._transferType != DirectionType.None && this._inventoryMovingType != InventoryMovingType.None)
                {
                    if (this._transferType == DirectionType.Internal && this._inventoryMovingType == InventoryMovingType.TransferOut)
                    {
                        this.ObjectList = ObjectList.TransferOut;
                    }
                    if (this._transferType == DirectionType.Internal && this._inventoryMovingType == InventoryMovingType.TransferIn)
                    {
                        this.ObjectList = ObjectList.TransferIn;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOrder " + ex.ToString());
            }
        }

    }
}