﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#endregion Default

using DevExpress.Persistent.BaseImpl;
using DevExpress.ExpressApp.DC;
using DevExpress.Xpo;

namespace FullDrive.Module.BusinessObjects
{
    public class ExtraData
    {
        public Company Company { get; set; }
        public Division Division { get; set; }
        public Department Department { get; set; }
        public Section Section { get; set; }
    }

}
