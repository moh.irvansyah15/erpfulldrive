﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("PurchaseInvoiceRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class PurchaseInvoice : FullDriveSysBaseObject
    {
        #region Default

        private bool _activationPosting;
        private string _code;
        private ObjectList _objectList;
        XPCollection<NumberingLine> _availableNumberingLine;
        private NumberingLine _numbering;
        private BusinessPartner _buyFromVendor;
        private string _buyFromContact;
        private Country _buyFromCountry;
        private City _buyFromCity;
        private string _buyFromAddress;
        private BusinessPartner _payToVendor;
        private string _payToContact;
        private Country _payToCountry;
        private City _payToCity;
        private string _payToAddress;
        private PaymentMethod _paymentMethod;
        private TermOfPayment _top;
        private Currency _currency;
        private XPCollection<BankAccount> _availableBankAccountCompany;
        private BankAccount _bankAccountCompany;
        private string _companyAccountNo;
        private string _companyAccountName;
        private XPCollection<BankAccount> _availableBankAccounts;
        private BankAccount _bankAccount;
        private string _accountNo;
        private string _accountName;
        private string _taxNo;       
        private double _maxPay;
        private double _pay;
        private double _plan;
        private double _outstanding;
        private double _advFee;
        private double _postAdvFee;
        private string _description;
        private DateTime _estimatedDate;       
        private string _notification;
        private int _postedCount;
        private Status _status;
        private DateTime _statusDate;
        private string _signCode;
        private InventoryTransferIn _inventoryTransferIn;
        private PurchaseOrder _purchaseOrder;
        private ProjectHeader _projectHeader;
        private Company _company;
        private string _userAccess;
        private GlobalFunction _globFunc;
        //clm
        private double _cm_amount;

        public PurchaseInvoice(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PurchaseInvoice);
                DateTime now = DateTime.Now;
                this.Status = CustomProcess.Status.Open;
                this.StatusDate = now;
                this.ObjectList = CustomProcess.ObjectList.PurchaseInvoice;

                _userAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                if (Session.IsNewObject(this))
                {
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null)
                            {
                                Company = _locUserAccess.Employee.Company;
                            }
                        }
                    }
                }
                this.Currency = _globFunc.GetDefaultCurrency(this.Session);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #endregion Default

        #region Field

        [ImmediatePostData()]
        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("PurchaseInvoiceCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Browsable(false)]
        public ObjectList ObjectList
        {
            get { return _objectList; }
            set { SetPropertyValue("ObjectList", ref _objectList, value); }
        }

        [Browsable(false)]
        public XPCollection<NumberingLine> AvailableNumberingLine
        {
            get
            {
                if (ObjectList == ObjectList.None)
                {
                    _availableNumberingLine = new XPCollection<NumberingLine>(Session);
                }
                else
                {
                    _availableNumberingLine = new XPCollection<NumberingLine>(Session,
                                              new GroupOperator(GroupOperatorType.And,
                                              new BinaryOperator("ObjectList", this.ObjectList),
                                              new BinaryOperator("Selection", true),
                                              new BinaryOperator("Active", true)));
                }
                return _availableNumberingLine;

            }
        }

        [NonPersistent()]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableNumberingLine", DataSourcePropertyIsNullMode.SelectAll)]
        public NumberingLine Numbering
        {
            get { return _numbering; }
            set
            {
                NumberingLine _oldNumbering = _numbering;
                SetPropertyValue("Numbering", ref _numbering, value);
                if (!IsLoading)
                {
                    if (_numbering != null)
                    {
                        if (this.Session != null)
                        {
                            if (this.Session.DataLayer != null)
                            {
                                this.Code = LocGetNumberingSelectionUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PurchaseInvoice, _numbering);
                            }
                        }

                    }
                }
            }
        }

        #region BuyFromVendor

        [DataSourceCriteria("Active = true")]
        [Appearance("PurchaseInvoiceBuyFromVendorClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner BuyFromVendor
        {
            get { return _buyFromVendor; }
            set { SetPropertyValue("BuyFromVendor", ref _buyFromVendor, value); }
        }

        [Appearance("PurchaseInvoiceBuyFromContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string BuyFromContact
        {
            get { return _buyFromContact; }
            set { SetPropertyValue("BuyFromContact", ref _buyFromContact, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("PurchaseInvoiceBuyFromCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country BuyFromCountry
        {
            get { return _buyFromCountry; }
            set { SetPropertyValue("BuyFromCountry", ref _buyFromCountry, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("PurchaseInvoiceBuyFromCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City BuyFromCity
        {
            get { return _buyFromCity; }
            set { SetPropertyValue("BuyFromCity", ref _buyFromCity, value); }
        }

        [Appearance("PurchaseInvoiceBuyFromAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string BuyFromAddress
        {
            get { return _buyFromAddress; }
            set { SetPropertyValue("BuyFromAddress", ref _buyFromAddress, value); }
        }

        #endregion BuyFromVendor

        #region PayToVendor

        [DataSourceCriteria("Active = true")]
        [Appearance("PurchaseInvoicePayToVendorClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner PayToVendor
        {
            get { return _payToVendor; }
            set { SetPropertyValue("PayToVendor", ref _payToVendor, value); }
        }

        [Appearance("PurchaseInvoicePayToContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string PayToContact
        {
            get { return _payToContact; }
            set { SetPropertyValue("PayToContact", ref _payToContact, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("PurchaseInvoicePayToCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country PayToCountry
        {
            get { return _payToCountry; }
            set { SetPropertyValue("PayToCountry", ref _payToCountry, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("PurchaseInvoicePayToCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City PayToCity
        {
            get { return _payToCity; }
            set { SetPropertyValue("PayToCity", ref _payToCity, value); }
        }

        [Appearance("PurchaseInvoicePayToAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string PayToAddress
        {
            get { return _payToAddress; }
            set { SetPropertyValue("PayToAddress", ref _payToAddress, value); }
        }

        #endregion PayToVendor

        [Appearance("PurchaseInvoicePaymentMethodClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentMethod PaymentMethod
        {
            get { return _paymentMethod; }
            set { SetPropertyValue("PaymentMethod", ref _paymentMethod, value); }
        }

        [Appearance("PurchaseInvoiceTermOfPaymentClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public TermOfPayment TOP
        {
            get { return _top; }
            set { SetPropertyValue("TOP", ref _top, value); }
        }

        [Appearance("PurchaseInvoiceCurrencyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        [Browsable(false)]
        public XPCollection<BankAccount> AvailableBankAccountCompany
        {
            get
            {
                if (this.Company == null)
                {
                    _availableBankAccountCompany = new XPCollection<BankAccount>(Session);
                }
                else
                {
                    _availableBankAccountCompany = new XPCollection<BankAccount>(Session, new GroupOperator
                                                (GroupOperatorType.And,
                                                new BinaryOperator("Company", this.Company)));
                }
                return _availableBankAccountCompany;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableBankAccountCompany", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("PurchaseInvoiceBankAccountCompanyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BankAccount BankAccountCompany
        {
            get { return _bankAccountCompany; }
            set
            {
                SetPropertyValue("BankAccountCompany", ref _bankAccountCompany, value);
                if (!IsLoading)
                {
                    if(this._bankAccountCompany != null)
                    {
                        this.CompanyAccountNo = this._bankAccountCompany.AccountNo;
                        this.CompanyAccountName = this._bankAccountCompany.AccountName;
                    }
                }
            }
        }

        [Appearance("PurchaseInvoiceCompanyAccountNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string CompanyAccountNo
        {
            get { return _companyAccountNo; }
            set { SetPropertyValue("CompanyAccountNo", ref _companyAccountNo, value); }
        }

        [Appearance("PurchaseInvoiceCompanyAccountNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string CompanyAccountName
        {
            get { return _companyAccountName; }
            set { SetPropertyValue("CompanyAccountName", ref _companyAccountName, value); }
        }

        [Browsable(false)]
        public XPCollection<BankAccount> AvailableBankAccounts
        {
            get
            {
                if (this.PayToVendor == null)
                {
                    _availableBankAccounts = new XPCollection<BankAccount>(Session);
                }
                else
                {
                    _availableBankAccounts = new XPCollection<BankAccount>(Session, new GroupOperator
                                                (GroupOperatorType.And,
                                                new BinaryOperator("BusinessPartner", this.PayToVendor)));
                }
                return _availableBankAccounts;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableBankAccounts", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("PurchaseInvoiceBankAccountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BankAccount BankAccount
        {
            get { return _bankAccount; }
            set
            {
                SetPropertyValue("BankAccount", ref _bankAccount, value);
                if (!IsLoading)
                {
                    if (this._bankAccount != null)
                    {
                        this.AccountNo = this._bankAccount.AccountNo;
                        this.AccountName = this._bankAccount.AccountName;
                    }
                }
            }
        }

        [Appearance("PurchaseInvoiceAccountNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string AccountNo
        {
            get { return _accountNo; }
            set { SetPropertyValue("AccountNo", ref _accountNo, value); }
        }

        [Appearance("PurchaseInvoiceAccountNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string AccountName
        {
            get { return _accountName; }
            set { SetPropertyValue("AccountName", ref _accountName, value); }
        }

        [Appearance("PurchaseInvoiceTaxNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string TaxNo
        {
            get { return _taxNo; }
            set { SetPropertyValue("TaxNo", ref _taxNo, value); }
        }

        #region Amount

        [Appearance("PurchaseInvoiceMaxPayClose", Enabled = false)]
        public double MaxPay
        {
            get { return _maxPay; }
            set { SetPropertyValue("MaxPay", ref _maxPay, value); }
        }

        [ImmediatePostData()]
        [Appearance("PurchaseInvoicePayClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Pay
        {
            get { return _pay; }
            set
            {
                SetPropertyValue("Pay", ref _pay, value);
                if (!IsLoading)
                {
                    if (this.PostedCount > 0)
                    {
                        if (this.Outstanding > 0)
                        {
                            if (this._pay > 0 && this._pay > this.Outstanding)
                            {
                                this._pay = this.Outstanding;
                            }
                        }
                    }

                    if (this.PostedCount == 0)
                    {
                        if (this._pay > 0 && this._pay > this.MaxPay)
                        {
                            this._pay = this.MaxPay;
                        }
                    }
                }
            }
        }

        //[Browsable(false)]
        [ImmediatePostData()]
        [Appearance("PurchaseInvoiceCM_AmountClose", Enabled = false)]
        public double CM_Amount
        {
            get { return _cm_amount; }
            set { SetPropertyValue("CM_Amount", ref _cm_amount, value); }
        }

        [Appearance("PurchaseInvoicePlanClose", Enabled = false)]
        public double Plan
        {
            get { return _plan; }
            set { SetPropertyValue("Plan", ref _plan, value); }
        }

        [Appearance("PurchaseInvoiceOutstandingClose", Enabled = false)]
        public double Outstanding
        {
            get { return _outstanding; }
            set { SetPropertyValue("Outstanding", ref _outstanding, value); }
        }

        [Appearance("PurchaseInvoiceAdvFeeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double AdvFee
        {
            get { return _advFee; }
            set { SetPropertyValue("AdvFee", ref _advFee, value); }
        }

        public double PostAdvFee
        {
            get { return _postAdvFee; }
            set { SetPropertyValue("PostAdvFee", ref _postAdvFee, value); }
        }

        #endregion Amount

        [Appearance("PurchaseInvoiceEstimatedDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime EstimatedDate
        {
            get { return _estimatedDate; }
            set { SetPropertyValue("EstimatedDate", ref _estimatedDate, value); }
        }

        [Appearance("PurchaseInvoiceDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Appearance("PurchaseInvoiceNotificationClose", Enabled = false)]
        public string Notification
        {
            get { return _notification; }
            set { SetPropertyValue("Notification", ref _notification, value); }
        }

        [Appearance("PurchaseInvoicePostedCountClose", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        [Appearance("PurchaseInvoiceStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("PurchaseInvoiceStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("PurchaseInvoiceSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        //Wahab 27-02-2020
        //[Association("InventoryTransferIn-PurchaseInvoices")]
        [Appearance("PurchaseInvoiceInventoryTransferInClose", Criteria = "ActivationPosting = true", Enabled = false)]       
        public InventoryTransferIn InventoryTransferIn
        {
            get { return _inventoryTransferIn; }
            set { SetPropertyValue("InventoryTransferIn", ref _inventoryTransferIn, value); }
        }

        //Wahab 27-02-2020
        //[Association("PurchaseOrder-PurchaseInvoices")]
        [Appearance("PurchaseInvoicePurchaseOrderClose", Criteria = "ActivationPosting = true", Enabled = false)]      
        public PurchaseOrder PurchaseOrder
        {
            get { return _purchaseOrder; }
            set { SetPropertyValue("PurchaseOrder", ref _purchaseOrder, value); }
        }

        [Appearance("PurchaseInvoiceProjectHeaderClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Association("ProjectHeader-PurchaseInvoices")]
        public ProjectHeader ProjectHeader
        {
            get { return _projectHeader; }
            set { SetPropertyValue("ProjectHeader", ref _projectHeader, value); }
        }

        [Browsable(false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Association("PurchaseInvoice-PurchaseInvoiceLines")]
        public XPCollection<PurchaseInvoiceLine> PurchaseInvoiceLines
        {
            get { return GetCollection<PurchaseInvoiceLine>("PurchaseInvoiceLines"); }
        }

        [Association("PurchaseInvoice-EDocuments")]
        public XPCollection<EDocument> EDocuments
        {
            get { return GetCollection<EDocument>("EDocuments"); }
        }

        [Association("PurchaseInvoice-PaymentOutPlans")]
        public XPCollection<PaymentOutPlan> PaymentOutPlans
        {
            get { return GetCollection<PaymentOutPlan>("PaymentOutPlans"); }
        }

        #region CLM

        //[Association("PurchaseInvoice-CreditMemoCollections")]
        //public XPCollection<CreditMemoCollection> CreditMemoCollections
        //{
        //    get { return GetCollection<CreditMemoCollection>("CreditMemoCollections"); }
        //}

        [Persistent("TotalPI")]
        public double TotalPIL
        {
            get
            {
                double _result = 0;
                int _totalLine = 0;
                if (!IsLoading)
                {
                    _totalLine = Convert.ToInt32(Session.Evaluate<PurchaseInvoice>(CriteriaOperator.Parse("PurchaseInvoiceLines.Count"), CriteriaOperator.Parse("Oid=?", Oid)));
                    if (_totalLine >= 0)
                    {
                        _result = UpdateGrandTotalPIL(true);
                    }
                }
                return _result;
            }
        }

        //[Appearance("PurchaseInvoiceCreditMemoAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        //public double CreditMemoAmount
        //{
        //    get
        //    {
        //        double _result = 0;
        //        int _totalLine = 0;
        //        if (!IsLoading)
        //        {
        //            _totalLine = Convert.ToInt32(Session.Evaluate<PurchaseInvoice>(CriteriaOperator.Parse("PurchaseInvoiceLines.Count"), CriteriaOperator.Parse("Oid=?", Oid)));
        //            if (_totalLine > 0)
        //            {
        //                _result = UpdateGrandTotalCMCL(true);
        //            }
        //        }
        //        return _result;
        //    }
        //}

        //[Persistent("GrandTotal")]
        //public double GrandTotal
        //{
        //    get
        //    {
        //        double _result = 0;
        //        if (!IsLoading)
        //        {
        //            _result = TotalPIL - CreditMemoAmount;
        //        }
        //        return _result;
        //    }
        //}

        //[Persistent("GrandTotalCm")]
        //public double GrandTotalCm
        //{
        //    get
        //    {
        //        double _result = 0;
        //        int _totalLine = 0;
        //        if (!IsLoading)
        //        {
        //            _totalLine = Convert.ToInt32(Session.Evaluate<PurchaseInvoice>(CriteriaOperator.Parse("PurchaseInvoiceLines.Count"), CriteriaOperator.Parse("Oid=?", Oid)));
        //            if (_totalLine > 0)
        //            {
        //                _result = UpdateGrandTotalCm(true);
        //            }
        //        }
        //        return _result;
        //    }
        //}

        #endregion CLM

        #endregion Field

        #region CLM

        public double UpdateGrandTotalPIL(bool forceChangeEvents)
        {
            double _result = 0;
            try
            {
                XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>(Session,
                                                                             new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("PurchaseInvoice", this)));

                if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count > 0)
                {
                    if (_locPurchaseInvoiceLines.Count > 0)
                    {
                        foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                        {
                            if (forceChangeEvents)
                            {
                                if (_locPurchaseInvoiceLine.TAmountPIL >= 0)
                                {
                                    _result = _locPurchaseInvoiceLine.TAmountPIL;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseInvoice", ex.ToString());
            }
            return _result;
        }

        //public double UpdateGrandTotalCMCL(bool forceChangeEvents)
        //{
        //    double _result = 0;
        //    try
        //    {
        //        XPCollection<CreditMemoCollectionLine> _locCMCLines = new XPCollection<CreditMemoCollectionLine>(Session,
        //                                                              new GroupOperator(GroupOperatorType.And,
        //                                                              new BinaryOperator("CreditMemoCollection", this)));

        //        if (_locCMCLines != null && _locCMCLines.Count > 0)
        //        {
        //            if (_locCMCLines.Count > 0)
        //            {
        //                foreach (CreditMemoCollectionLine _locCMCLine in _locCMCLines)
        //                {
        //                    if (forceChangeEvents)
        //                    {
        //                        if (_locCMCLine.TotalCMCL >= 0)
        //                        {
        //                            _result = _locCMCLine.TotalCMCL;
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Tracing.Tracer.LogError("Business Object = PurchaseInvoice", ex.ToString());
        //    }
        //    return _result;
        //}

        //public double UpdateGrandTotalAll(bool forceChangeEvents)
        //{
        //    double _result = 0;
        //    try
        //    {
        //        XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>(Session,
        //                                                                     new GroupOperator(GroupOperatorType.And,
        //                                                                     new BinaryOperator("PurchaseInvoice", this)));

        //        if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count > 0)
        //        {
        //            if (_locPurchaseInvoiceLines.Count > 0)
        //            {
        //                foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
        //                {
        //                    if (forceChangeEvents)
        //                    {
        //                        if (_locPurchaseInvoiceLine.TotalCMCL >= 0 && _locPurchaseInvoiceLine.TAmountPIL >= 0)
        //                        {
        //                            _result = _locPurchaseInvoiceLine.TAmountPIL - _locPurchaseInvoiceLine.TotalCMCL;
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Tracing.Tracer.LogError("Business Object = PurchaseInvoice", ex.ToString());
        //    }
        //    return _result;
        //}

        //public double UpdateGrandTotalCm(bool forceChangeEvents)
        //{
        //    double _result = 0;
        //    try
        //    {
        //        XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>(Session,
        //                                                                     new GroupOperator(GroupOperatorType.And,
        //                                                                     new BinaryOperator("PurchaseInvoice", this)));

        //        if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count > 0)
        //        {
        //            if (_locPurchaseInvoiceLines.Count > 0)
        //            {
        //                foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
        //                {
        //                    if (forceChangeEvents)
        //                    {
        //                        if (_locPurchaseInvoiceLine.TotalCm >= 0)
        //                        {
        //                            _result = _locPurchaseInvoiceLine.TotalCm;
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Tracing.Tracer.LogError("Business Object = PurchaseInvoice", ex.ToString());
        //    }
        //    return _result;
        //}

        #endregion CLm

        #region LocalNumbering
        public string LocGetNumberingSelectionUnlockOptimisticRecord(IDataLayer _currIDataLayer, ObjectList _currObject, NumberingLine _currNumberingLine)
        {
            string _result = null;
            Session _generatorSession = null;
            try
            {
                if (_currIDataLayer != null)
                {
                    _generatorSession = new Session(_currIDataLayer);
                }

                if (_generatorSession != null)
                {
                    ApplicationSetup _appSetup = _generatorSession.FindObject<ApplicationSetup>(new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Active", true), new BinaryOperator("DefaultSystem", true)));
                    if (_appSetup != null)
                    {
                        NumberingHeader _numberingHeader = _generatorSession.FindObject<NumberingHeader>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                             new BinaryOperator("NumberingType", NumberingType.Objects),
                                                             new BinaryOperator("ApplicationSetup", _appSetup, BinaryOperatorType.Equal),
                                                             new BinaryOperator("Active", true)));
                        if (_numberingHeader != null)
                        {
                            NumberingLine _numberingLine = _generatorSession.FindObject<NumberingLine>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Oid", _currNumberingLine.Oid),
                                                         new BinaryOperator("ObjectList", _currObject),
                                                         new BinaryOperator("Selection", true),
                                                         new BinaryOperator("Active", true),
                                                         new BinaryOperator("NumberingHeader", _numberingHeader)));
                            if (_numberingLine != null)
                            {
                                _numberingLine.LastValue = _numberingLine.LastValue + _numberingLine.IncrementValue;
                                _numberingLine.Save();
                                _result = _numberingLine.FormatedValue;
                                _numberingLine.Session.CommitTransaction();
                            }

                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }
        #endregion LocalNumbering

    }
}