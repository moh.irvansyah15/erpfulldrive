﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("DebitMemoCollectionRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class DebitMemoCollection : FullDriveSysBaseObject
    {
        #region Default

        private bool _activationPosting;
        private string _code;
        private BusinessPartner _billToCustomer;
        private string _billToContact;
        private Country _billToCountry;
        private City _billToCity;
        private string _billToAddress;
        private TermOfPayment _top;
        private Status _status;
        private DebitMemo _debitMemo;
        private double _dm_amount;
        private DateTime _statusDate;
        private string _signCode;
        private SalesReturn _salesReturn;
        private SalesOrder _salesOrder;
        private InventoryTransferIn _inventoryTransferIn;
        private GlobalFunction _globFunc;
        private SalesInvoice _salesInvoice;
        private SalesInvoiceCollection _salesInvoiceCollection;

        public DebitMemoCollection(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.DebitMemoCollection);
                this.Status = Status.Open;
                this.StatusDate = now;
            }
        }

        #endregion Default

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("DebitMemoCollectionCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("DebitMemoCollectionBillToCustomerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner BillToCustomer
        {
            get { return _billToCustomer; }
            set { SetPropertyValue("BillToCustomer", ref _billToCustomer, value); }
        }

        [Appearance("DebitMemoCollectionBillToContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string BillToContact
        {
            get { return _billToContact; }
            set { SetPropertyValue("BillToContact", ref _billToContact, value); }
        }

        [Appearance("DebitMemoCollectionBillToCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country BillToCountry
        {
            get { return _billToCountry; }
            set { SetPropertyValue("BillToCountry", ref _billToCountry, value); }
        }

        [Appearance("DebitMemoCollectionBillToCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City BillToCity
        {
            get { return _billToCity; }
            set { SetPropertyValue("BillToCity", ref _billToCity, value); }
        }

        [Size(512)]
        [Appearance("DebitMemoCollectionBillToAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string BillToAddress
        {
            get { return _billToAddress; }
            set { SetPropertyValue("BillToAddress", ref _billToAddress, value); }
        }

        [Appearance("DebitMemoCollectionBuyFromTOPClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public TermOfPayment TOP
        {
            get { return _top; }
            set { SetPropertyValue("TOP", ref _top, value); }
        }

        [Appearance("DebitMemoCollectionStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("DebitMemoCollectionStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("DebitMemoCollectionSignCodeEnabled", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        //clm
        [Appearance("DebitMemoCollectionDebitMemoEnabled", Enabled = false)]
        public DebitMemo DebitMemo
        {
            get { return _debitMemo; }
            set { SetPropertyValue("DebitMemo", ref _debitMemo, value); }
        }

        [Appearance("DebitMemoCollectionDMAmountEnabled", Enabled = false)]
        public double DM_Amount
        {
            get { return _dm_amount; }
            set { SetPropertyValue("DMAmount", ref _dm_amount, value); }
        }

        [Appearance("DebitMemoCollectionSalesReturnClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public SalesReturn SalesReturn
        {
            get { return _salesReturn; }
            set
            {
                SetPropertyValue("SalesReturn", ref _salesReturn, value);
                if (!IsLoading)
                {
                    if (this._salesReturn != null)
                    {
                        if (this._salesReturn.SalesOrder != null)
                        {
                            this.SalesOrder = _salesReturn.SalesOrder;
                        }
                    }
                }
            }
        }

        [Appearance("DebitMemoCollectionSalesOrderClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public SalesOrder SalesOrder
        {
            get { return _salesOrder; }
            set { SetPropertyValue("SalesOrder", ref _salesOrder, value); }
        }

        [Browsable(false)]
        [Appearance("DebitMemoCollectionInventoryTransferInClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public InventoryTransferIn InventoryTransferIn
        {
            get { return _inventoryTransferIn; }
            set { SetPropertyValue("InventoryTransferIn", ref _inventoryTransferIn, value); }
        }

        //Wahab 27-02-2020
        //[Association("SalesInvoice-DebitMemoCollections")]
        public SalesInvoice SalesInvoice
        {
            get { return _salesInvoice; }
            set { SetPropertyValue("SalesInvoice", ref _salesInvoice, value); }
        }

        [Association("SalesInvoiceCollection-DebitMemoCollections")]
        public SalesInvoiceCollection SalesInvoiceCollection
        {
            get { return _salesInvoiceCollection; }
            set { SetPropertyValue("SalesInvoiceCollection", ref _salesInvoiceCollection, value); }
        }

        #endregion Field

    }
}