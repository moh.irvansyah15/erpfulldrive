﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("FullName")]
    [NavigationItem("Quality Assurance")]
    [RuleCombinationOfPropertiesIsUnique("ProductCodeRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class ProductCode : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private SubProductGroup _subProductGroup;
        private Conductor _conductor;
        private ShapeOfConductor _shapeOfConductor;
        private CrossSectionArea _crossSectionArea;
        private CoreNumber _coreNumber;
        private WireNumber _wireNumber;
        private Insulation _insulation;
        private Color _color;
        private Wrapping _wrapping;
        private Filler _filler;
        private Screening _screening;
        private Armour _armour;
        private OuterSheath _outerSheath;
        private RatedVoltage _ratedVoltage;
        private Standart _standart;
        private Brand _brand;
        #region Additional1
        private bool _additional1;
        private Conductor _conductorAdd1;
        private ShapeOfConductor _shapeOfConductorAdd1;
        private CrossSectionArea _crossSectionAreaAdd1;
        private CoreNumber _coreNumberAdd1;
        private WireNumber _wireNumberAdd1;
        private Color _colorAdd1;
        #endregion Additional1
        #region Additional2
        private bool _additional2;
        private Conductor _conductorAdd2;
        private ShapeOfConductor _shapeOfConductorAdd2;
        private CrossSectionArea _crossSectionAreaAdd2;
        private CoreNumber _coreNumberAdd2;
        private WireNumber _wireNumberAdd2;
        private Color _colorAdd2;
        #endregion Additional2
        private string _fullName;
        //private ProductType _productType;
        private bool _active;
        private GlobalFunction _globFunc;

        public ProductCode(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ProductCode);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        [Appearance("ProductCodeCodeEnabled", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        public SubProductGroup SubProductGroup
        {
            get { return _subProductGroup; }
            set
            {
                SetPropertyValue("SubProductGroup", ref _subProductGroup, value);
                if (!IsLoading)
                {
                    if (this._subProductGroup != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        public Conductor Conductor
        {
            get { return _conductor; }
            set
            {
                SetPropertyValue("Conductor", ref _conductor, value);
                if (!IsLoading)
                {
                    if (this._conductor != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        public ShapeOfConductor ShapeOfConductor
        {
            get { return _shapeOfConductor; }
            set
            {
                SetPropertyValue("ShapeOfConductor", ref _shapeOfConductor, value);
                if (!IsLoading)
                {
                    if (this._shapeOfConductor != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        public CrossSectionArea CrossSectionArea
        {
            get { return _crossSectionArea; }
            set
            {
                SetPropertyValue("CrossSectionArea", ref _crossSectionArea, value);
                if (!IsLoading)
                {
                    if (this._crossSectionArea != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        public CoreNumber CoreNumber
        {
            get { return _coreNumber; }
            set
            {
                SetPropertyValue("CoreNumber", ref _coreNumber, value);
                if (!IsLoading)
                {
                    if (this._coreNumber != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        public WireNumber WireNumber
        {
            get { return _wireNumber; }
            set
            {
                SetPropertyValue("WireNumber", ref _wireNumber, value);
                if (!IsLoading)
                {
                    if (this._wireNumber != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        public Insulation Insulation
        {
            get { return _insulation; }
            set
            {
                SetPropertyValue("Insulation", ref _insulation, value);
                if (!IsLoading)
                {
                    if (this._insulation != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        [DataSourceCriteria("Active = true And CoreNumber = '@This.CoreNumber'"), ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        public Color Color
        {
            get { return _color; }
            set
            {
                SetPropertyValue("Color", ref _color, value);
                if (!IsLoading)
                {
                    if (this._color != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        public Wrapping Wrapping
        {
            get { return _wrapping; }
            set
            {
                SetPropertyValue("Wrapping", ref _wrapping, value);
                if (!IsLoading)
                {
                    if (this._wrapping != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        public Filler Filler
        {
            get { return _filler; }
            set
            {
                SetPropertyValue("Filler", ref _filler, value);
                if (!IsLoading)
                {
                    if (this._filler != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        public Screening Screening
        {
            get { return _screening; }
            set
            {
                SetPropertyValue("Screening", ref _screening, value);
                if (!IsLoading)
                {
                    if (this._screening != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        public Armour Armour
        {
            get { return _armour; }
            set
            {
                SetPropertyValue("Armour", ref _armour, value);
                if (!IsLoading)
                {
                    if (this._armour != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        public OuterSheath OuterSheath
        {
            get { return _outerSheath; }
            set
            {
                SetPropertyValue("OuterSheath", ref _outerSheath, value);
                if (!IsLoading)
                {
                    if (this._outerSheath != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        public RatedVoltage RatedVoltage
        {
            get { return _ratedVoltage; }
            set
            {
                SetPropertyValue("RatedVoltage", ref _ratedVoltage, value);
                if (!IsLoading)
                {
                    if (this._ratedVoltage != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        public Standart Standart
        {
            get { return _standart; }
            set
            {
                SetPropertyValue("Standart", ref _standart, value);
                if (!IsLoading)
                {
                    if (this._standart != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        public Brand Brand
        {
            get { return _brand; }
            set
            {
                SetPropertyValue("Brand", ref _brand, value);
                if (!IsLoading)
                {
                    if (this._brand != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        #region Additional1
        [ImmediatePostData()]
        public bool Additional1
        {
            get { return _additional1; }
            set { SetPropertyValue("Additional1", ref _additional1, value); }
        }

        [Appearance("ProductCodeConductorAdd1Hide", Criteria = "Additional1 = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        public Conductor ConductorAdd1
        {
            get { return _conductorAdd1; }
            set
            {
                SetPropertyValue("ConductorAdd1", ref _conductorAdd1, value);
                if(!IsLoading)
                {
                    if (this._conductorAdd1 != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        [Appearance("ProductCodeShapeOfConductorAdd1Hide", Criteria = "Additional1 = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        public ShapeOfConductor ShapeOfConductorAdd1
        {
            get { return _shapeOfConductorAdd1; }
            set
            {
                SetPropertyValue("ShapeOfConductorAdd1", ref _shapeOfConductorAdd1, value);
                if (!IsLoading)
                {
                    if (this._shapeOfConductorAdd1 != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        [Appearance("ProductCodeCrossSectionAreaAdd1Hide", Criteria = "Additional1 = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        public CrossSectionArea CrossSectionAreaAdd1
        {
            get { return _crossSectionAreaAdd1; }
            set
            {
                SetPropertyValue("CrossSectionAreaAdd1", ref _crossSectionAreaAdd1, value);
                if (!IsLoading)
                {
                    if (this._crossSectionAreaAdd1 != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        [Appearance("ProductCodeCoreNumberAdd1Hide", Criteria = "Additional1 = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        public CoreNumber CoreNumberAdd1
        {
            get { return _coreNumberAdd1; }
            set
            {
                SetPropertyValue("CoreNumberAdd1", ref _coreNumberAdd1, value);
                if (!IsLoading)
                {
                    if (this._coreNumberAdd1 != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        [Appearance("ProductCodeWireNumberAdd1Hide", Criteria = "Additional1 = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        public WireNumber WireNumberAdd1
        {
            get { return _wireNumberAdd1; }
            set
            {
                SetPropertyValue("WireNumberAdd1", ref _wireNumberAdd1, value);
                if (!IsLoading)
                {
                    if (this._wireNumberAdd1 != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        //[DataSourceCriteria("Active = true And CoreNumberAdd1 = '@This.CoreNumberAdd1'"), ImmediatePostData()]
        [ImmediatePostData()]
        [Appearance("ProductCodeColorAdd1Hide", Criteria = "Additional1 = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        public Color ColorAdd1
        {
            get { return _colorAdd1; }
            set
            {
                SetPropertyValue("ColorAdd1", ref _colorAdd1, value);
                if (!IsLoading)
                {
                    if (this._colorAdd1 != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        //[Appearance("ProductCodeStandartAdd1Hide", Criteria = "Additional1 = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        //[DataSourceCriteria("Active = true"), ImmediatePostData()]
        //public Standart StandartAdd1
        //{
        //    get { return _standartAdd1; }
        //    set
        //    {
        //        SetPropertyValue("StandartAdd1", ref _standartAdd1, value);
        //        if (!IsLoading)
        //        {
        //            if (this._standartAdd1 != null)
        //            {
        //                SetFullName();
        //            }
        //        }
        //    }
        //}
        #endregion Additional1

        #region Additional2
        [ImmediatePostData()]
        public bool Additional2
        {
            get { return _additional2; }
            set { SetPropertyValue("Additional2", ref _additional2, value); }
        }

        [Appearance("ProductCodeConductorAdd2Hide", Criteria = "Additional2 = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        public Conductor ConductorAdd2
        {
            get { return _conductorAdd2; }
            set
            {
                SetPropertyValue("ConductorAdd2", ref _conductorAdd2, value);
                if (!IsLoading)
                {
                    if (this._conductorAdd2 != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        [Appearance("ProductCodeShapeOfConductorAdd2Hide", Criteria = "Additional2 = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        public ShapeOfConductor ShapeOfConductorAdd2
        {
            get { return _shapeOfConductorAdd2; }
            set
            {
                SetPropertyValue("ShapeOfConductorAdd2", ref _shapeOfConductorAdd2, value);
                if (!IsLoading)
                {
                    if (this._shapeOfConductorAdd2 != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        [Appearance("ProductCodeCrossSectionAreaAdd2Hide", Criteria = "Additional2 = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        public CrossSectionArea CrossSectionAreaAdd2
        {
            get { return _crossSectionAreaAdd2; }
            set
            {
                SetPropertyValue("CrossSectionAreaAdd2", ref _crossSectionAreaAdd2, value);
                if (!IsLoading)
                {
                    if (this._crossSectionAreaAdd2 != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        [Appearance("ProductCodeCoreNumberAdd2Hide", Criteria = "Additional2 = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        public CoreNumber CoreNumberAdd2
        {
            get { return _coreNumberAdd2; }
            set
            {
                SetPropertyValue("CoreNumberAdd2", ref _coreNumberAdd2, value);
                if (!IsLoading)
                {
                    if (this._coreNumberAdd2 != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        [Appearance("ProductCodeWireNumberAdd2Hide", Criteria = "Additional2 = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        public WireNumber WireNumberAdd2
        {
            get { return _wireNumberAdd2; }
            set
            {
                SetPropertyValue("WireNumberAdd2", ref _wireNumberAdd2, value);
                if (!IsLoading)
                {
                    if (this._wireNumberAdd2 != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        //[DataSourceCriteria("Active = true And CoreNumberAdd2 = '@This.CoreNumberAdd2'"), ImmediatePostData()]
        [Appearance("ProductCodeColorAdd2Hide", Criteria = "Additional2 = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        public Color ColorAdd2
        {
            get { return _colorAdd2; }
            set
            {
                SetPropertyValue("ColorAdd2", ref _colorAdd2, value);
                if (!IsLoading)
                {
                    if (this._colorAdd2 != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        //[Appearance("ProductCodeStandartAdd2Hide", Criteria = "Additional2 = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        //[DataSourceCriteria("Active = true"), ImmediatePostData()]
        //public Standart StandartAdd2
        //{
        //    get { return _standartAdd2; }
        //    set
        //    {
        //        SetPropertyValue("StandartAdd2", ref _standartAdd2, value);
        //        if (!IsLoading)
        //        {
        //            if (this._standartAdd2 != null)
        //            {
        //                SetFullName();
        //            }
        //        }
        //    }
        //}
        #endregion Additional2

        [Appearance("ProductCodeFullNameEnabled", Enabled = false)]
        public string FullName
        {
            get { return _fullName; }
            set { SetPropertyValue("FullName", ref _fullName, value); }
        }

        //[Association("ProductType-ProductCodes")]
        //[DataSourceCriteria("Active = true")]
        //public ProductType ProductType
        //{
        //    get { return _productType; }
        //    set { SetPropertyValue("ProductType", ref _productType, value); }
        //}

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        [Association("ProductCode-Items")]
        public XPCollection<Item>Items
        {
            get { return GetCollection<Item>("Items"); }
        }

        //[Association("ProductCode-ProductTypes")]
        //public XPCollection<ProductType>ProductTypes
        //{
        //    get { return GetCollection<ProductType>("ProductTypes"); }
        //}

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}

        //=============================================== Code In Here ===============================================

        private void SetFullName()
        {
            try
            {
                string _locSubProductGroup = "";
                string _locConductor = "";
                string _locShapeOfConductor = "";
                string _locCrossSectionArea = "";
                string _locCoreNumber = "";
                string _locWireNumber = "";
                string _locInsulation = "";
                string _locColor = "";
                string _locWrapping = "";
                string _locFiller = "";
                string _locScreening = "";
                string _locArmour = "";
                string _locOuterSheath = "";
                string _locRatedVoltage = "";
                string _locStandart = "";
                string _locBrand = "";
                //adding
                string _locConductorAdd1 = "";
                string _locShapeOfConductorAdd1 = "";
                string _locCrossSectionAreaAdd1 = "";
                string _locCoreNumberAdd1 = "";
                string _locWireNumberAdd1 = "";
                string _locColorAdd1 = "";
                string _locConductorAdd2 = "";
                string _locShapeOfConductorAdd2 = "";
                string _locCrossSectionAreaAdd2 = "";
                string _locCoreNumberAdd2 = "";
                string _locWireNumberAdd2 = "";
                string _locColorAdd2 = "";

                if (this.SubProductGroup != null) { _locSubProductGroup = this.SubProductGroup.Code; }
                if (this.Conductor != null) { _locConductor = this.Conductor.Code; }
                if (this.ShapeOfConductor != null) { _locShapeOfConductor = this.ShapeOfConductor.Code; }
                if (this.CrossSectionArea != null) { _locCrossSectionArea = this.CrossSectionArea.Code; }
                if (this.CoreNumber != null) { _locCoreNumber = this.CoreNumber.Code; }
                if (this.WireNumber != null) { _locWireNumber = this.WireNumber.Code; }
                if (this.Insulation != null) { _locInsulation = this.Insulation.Code; }
                if (this.Color != null) { _locColor = this.Color.Code; }
                if (this.Wrapping != null) { _locWrapping = this.Wrapping.Code; }
                if (this.Filler != null) { _locFiller = this.Filler.Code; }
                if (this.Screening != null) { _locScreening = this.Screening.Code; }
                if (this.Armour != null) { _locArmour = this.Armour.Code; }
                if (this.OuterSheath != null) { _locOuterSheath = this.OuterSheath.Code; }
                if (this.RatedVoltage != null) { _locRatedVoltage = this.RatedVoltage.Code; }
                if (this.Standart != null) { _locStandart = this.Standart.Code; }
                if (this.Brand != null) { _locBrand = this.Brand.Code; }
                //Adding
                if (this.ConductorAdd1 != null) { _locConductorAdd1 = this.ConductorAdd1.Code; }
                if (this.ShapeOfConductorAdd1 != null) { _locShapeOfConductorAdd1 = this.ShapeOfConductorAdd1.Code; }
                if (this.CrossSectionAreaAdd1 != null) { _locCrossSectionAreaAdd1 = this.CrossSectionAreaAdd1.Code; }
                if (this.CoreNumberAdd1 != null) { _locCoreNumberAdd1 = this.CoreNumberAdd1.Code; }
                if (this.WireNumberAdd1 != null) { _locWireNumberAdd1 = this.WireNumberAdd1.Code; }
                if (this.ColorAdd1 != null) { _locColorAdd1 = this.ColorAdd1.Code; }
                if (this.ConductorAdd2 != null) { _locConductorAdd2 = this.ConductorAdd2.Code; }
                if (this.ShapeOfConductorAdd2 != null) { _locShapeOfConductorAdd2 = this.ShapeOfConductorAdd2.Code; }
                if (this.CrossSectionAreaAdd2 != null) { _locCrossSectionAreaAdd2 = this.CrossSectionAreaAdd2.Code; }
                if (this.CoreNumberAdd2 != null) { _locCoreNumberAdd2 = this.CoreNumberAdd2.Code; }
                if (this.WireNumberAdd2 != null) { _locWireNumberAdd2 = this.WireNumberAdd2.Code; }
                if (this.ColorAdd2 != null) { _locColorAdd2 = this.ColorAdd2.Code; }

                this.FullName = String.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}{11}{12}{13}{14}{15} {16}{17}{18}{19}{20}{21} {22}{23}{24}{25}{26}{27}", _locSubProductGroup, _locConductor, _locShapeOfConductor,
                                _locCrossSectionArea, _locCoreNumber, _locWireNumber, _locInsulation, _locColor, _locWrapping, _locFiller, _locScreening, _locArmour,
                                _locOuterSheath, _locRatedVoltage, _locStandart, _locBrand, _locConductorAdd1, _locShapeOfConductorAdd1, _locCrossSectionAreaAdd1,
                                _locCoreNumberAdd1, _locWireNumberAdd1, _locColorAdd1, _locConductorAdd2, _locShapeOfConductorAdd2, _locCrossSectionAreaAdd2,
                                _locCoreNumberAdd2, _locWireNumberAdd2, _locColorAdd2).Trim().Replace(" ", "");
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ProductCode ", ex.ToString());
            }
        }

    }
}