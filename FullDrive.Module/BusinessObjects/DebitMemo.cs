﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("DebitMemoRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class DebitMemo : FullDriveSysBaseObject
    {
        #region Default

        private bool _activationPosting;
        private string _code;
        private BusinessPartner _billToCustomer;
        private string _billToContact;
        private Country _billToCountry;
        private City _billToCity;
        private string _billToAddress;
        private TermOfPayment _top;
        private Status _status;
        private DateTime _statusDate;
        private string _signCode;
        private SalesReturn _salesReturn;
        private SalesOrder _salesOrder;
        private InventoryTransferIn _inventoryTransferIn;
        private GlobalFunction _globFunc;

        public DebitMemo(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.DebitMemo);
                this.Status = Status.Open;
                this.StatusDate = now;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #endregion Default

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("DebitMemoCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("DebitMemoBillToCustomerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner BillToCustomer
        {
            get { return _billToCustomer; }
            set { SetPropertyValue("BillToCustomer", ref _billToCustomer, value); }
        }

        [Appearance("DebitMemoBillToContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string BillToContact
        {
            get { return _billToContact; }
            set { SetPropertyValue("BillToContact", ref _billToContact, value); }
        }

        [Appearance("DebitMemoBillToCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country BillToCountry
        {
            get { return _billToCountry; }
            set { SetPropertyValue("BillToCountry", ref _billToCountry, value); }
        }

        [Appearance("DebitMemoBillToBillToCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City BillToCity
        {
            get { return _billToCity; }
            set { SetPropertyValue("BillToCity", ref _billToCity, value); }
        }

        [Size(512)]
        [Appearance("DebitMemoBillToAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string BillToAddress
        {
            get { return _billToAddress; }
            set { SetPropertyValue("BillToAddress", ref _billToAddress, value); }
        }

        [Appearance("DebitMemoBillToTOPClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public TermOfPayment TOP
        {
            get { return _top; }
            set { SetPropertyValue("TOP", ref _top, value); }
        }

        [Appearance("DebitMemoStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("DebitMemoStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("DebitMemoSignCodeEnabled", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [Appearance("DebitMemoSalesReturnClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public SalesReturn SalesReturn
        {
            get { return _salesReturn; }
            set
            {
                SetPropertyValue("SalesReturn", ref _salesReturn, value);
                if (!IsLoading)
                {
                    if (this._salesReturn != null)
                    {
                        if (this._salesReturn.SalesOrder != null)
                        {
                            this.SalesOrder = _salesReturn.SalesOrder;
                        }
                    }
                }
            }
        }

        [Appearance("DebitMemoSalesOrderClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public SalesOrder SalesOrder
        {
            get { return _salesOrder; }
            set { SetPropertyValue("SalesOrder", ref _salesOrder, value); }
        }

        [Appearance("DebitMemoInventoryTransferClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public InventoryTransferIn InventoryTransferIn
        {
            get { return _inventoryTransferIn; }
            set { SetPropertyValue("InventoryTransferIn", ref _inventoryTransferIn, value); }
        }

        [Association("DebitMemo-DebitMemoLines")]
        public XPCollection<DebitMemoLine> DebitMemoLines
        {
            get { return GetCollection<DebitMemoLine>("DebitMemoLines"); }
        }

        #region CLM

        [Persistent("GrandTotalCM")]
        public double GrandTotalCm
        {
            get
            {
                double _result = 0;
                int _totalLine = 0;
                if (!IsLoading)
                {
                    _totalLine = Convert.ToInt32(Session.Evaluate<DebitMemo>(CriteriaOperator.Parse("DebitMemoLines.Count"), CriteriaOperator.Parse("Oid=?", Oid)));
                    if (_totalLine > 0)
                    {
                        _result = UpdateGrandTotalDM(true);
                    }
                }
                return _result;
            }
        }

        #endregion CLM

        #endregion Field

        //==== Code Only ====

        #region CLM

        public double UpdateGrandTotalDM(bool forceChangeEvents)
        {
            double _result = 0;
            try
            {
                XPCollection<DebitMemoLine> _locDebitMemoLines = new XPCollection<DebitMemoLine>(Session,
                                                                 new GroupOperator(GroupOperatorType.And,
                                                                 new BinaryOperator("DebitMemo", this)));

                if (_locDebitMemoLines != null && _locDebitMemoLines.Count > 0)
                {
                    if (_locDebitMemoLines.Count > 0)
                    {
                        foreach (DebitMemoLine _locDebitMemoLine in _locDebitMemoLines)
                        {
                            if (forceChangeEvents)
                            {
                                if (_locDebitMemoLine.TGrandAmountDML >= 0)
                                {
                                    _result = _locDebitMemoLine.TGrandAmountDML;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = DebitMemo", ex.ToString());
            }
            return _result;
        }

        #endregion CLm

    }
}