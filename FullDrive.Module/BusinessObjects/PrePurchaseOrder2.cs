﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Project Transaction")]
    [RuleCombinationOfPropertiesIsUnique("PrePurchaseOrder2RuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class PrePurchaseOrder2 : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private string _code;
        private bool _select;
        private string _name;
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasure;
        private Item _item;
        private double _maxQty;
        private UnitOfMeasure _maxUom;
        private double _maxUnitAmount;
        private double _maxTotalUnitAmount;
        private double _qty;
        private UnitOfMeasure _uom;
        private double _unitAmount;
        private double _totalUnitAmount;
        private int _postedCount;
        private Status _status;
        private DateTime _statusDate;
        private Company _company;
        private ProjectHeader _projectHeader;
        private GlobalFunction _globFunc;

        public PrePurchaseOrder2(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PrePurchaseOrder2);
                DateTime now = DateTime.Now;
                this.Status = Status.Open;
                this.StatusDate = now;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("PrePurchaseOrder2CodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("PrePurchaseOrder2SelectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        [Appearance("PrePurchaseOrder2NameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("PrePurchaseOrder2ItemClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Item Item
        {
            get { return _item; }
            set { SetPropertyValue("Item", ref _item, value); }
        }

        [Browsable(false)]
        public XPCollection<UnitOfMeasure> AvailableUnitOfMeasure
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                if (Item == null)
                {
                    _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session);
                }
                else
                {
                    List<string> _stringUOM = new List<string>();

                    XPCollection<ItemUnitOfMeasure> _locItemUnitOfMeasures = new XPCollection<ItemUnitOfMeasure>
                                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Item", this.Item),
                                                                               new BinaryOperator("Active", true)));

                    if (_locItemUnitOfMeasures != null && _locItemUnitOfMeasures.Count() > 0)
                    {
                        foreach (ItemUnitOfMeasure _locItemUnitOfMeasure in _locItemUnitOfMeasures)
                        {
                            _stringUOM.Add(_locItemUnitOfMeasure.UOM.Code);
                        }
                    }

                    IEnumerable<string> _stringArrayUOMDistinct = _stringUOM.Distinct();
                    string[] _stringArrayUOMList = _stringArrayUOMDistinct.ToArray();
                    if (_stringArrayUOMList.Length == 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArrayUOMList.Length > 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                                else
                                {
                                    _endString = " OR [Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session, CriteriaOperator.Parse(_fullString));
                    }

                }
                return _availableUnitOfMeasure;

            }
        }

        [Appearance("PrePurchaseOrder2MaxQtyClose", Enabled = false)]
        public double MaxQty
        {
            get { return _maxQty; }
            set { SetPropertyValue("MaxQty", ref _maxQty, value); }
        }

        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        [Appearance("PrePurchaseOrder2MaxUOMClose", Enabled = false)]
        public UnitOfMeasure MaxUOM
        {
            get { return _maxUom; }
            set { SetPropertyValue("MaxUOM", ref _maxUom, value); }
        }

        [Appearance("PrePurchaseOrder2MaxUnitAmountClose", Enabled = false)]
        public double MaxUnitAmount
        {
            get { return _maxUnitAmount; }
            set { SetPropertyValue("MaxUnitAmount", ref _maxUnitAmount, value); }
        }

        [Appearance("PrePurchaseOrder2MaxTotalUnitAmountClose", Enabled = false)]
        public double MaxTotalUnitAmount
        {
            get { return _maxTotalUnitAmount; }
            set { SetPropertyValue("MaxTotalUnitAmount", ref _maxTotalUnitAmount, value); }
        }

        [Appearance("PrePurchaseOrder2QtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double Qty
        {
            get { return _qty; }
            set
            {
                SetPropertyValue("Qty", ref _qty, value);
                if (!IsLoading)
                {
                    SetTotalAmount();
                }
            }
        }

        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        [Appearance("PrePurchaseOrder2UOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set
            {
                SetPropertyValue("UOM", ref _uom, value);
                if (!IsLoading)
                {
                    SetTotalAmount();
                }
            }
        }

        [Appearance("PrePurchaseOrder2UnitAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double UnitAmount
        {
            get { return _unitAmount; }
            set
            {
                SetPropertyValue("UnitAmount", ref _unitAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();

                    if (this._unitAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.PrePurchaseOrder2, FieldName.UnitAmount) == true)
                        {
                            this._unitAmount = _globFunc.GetRoundUp(Session, this._unitAmount, ObjectList.PrePurchaseOrder2, FieldName.UnitAmount);
                        }
                    }
                    SetTotalAmount();
                }
            }
        }

        [Appearance("PrePurchaseOrder2TotalUnitAmountClose", Enabled = false)]
        public double TotalUnitAmount
        {
            get { return _totalUnitAmount; }
            set { SetPropertyValue("TotalUnitAmount", ref _totalUnitAmount, value); }
        }

        [Appearance("PrePurchaseOrder2PostedCountClose", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        [Appearance("PrePurchaseOrder2StatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("PrePurchaseOrder2StatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("PrePurchaseOrder2CompanyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Association("ProjectHeader-PrePurchaseOrder2s")]
        public ProjectHeader ProjectHeader
        {
            get { return _projectHeader; }
            set { SetPropertyValue("ProjectHeader", ref _projectHeader, value); }
        }

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}

        //================================================== Code Only ==================================================

        #region AmountMethod

        private void SetTotalAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_qty >= 0 && _unitAmount >= 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.PrePurchaseOrder2, FieldName.TotalUnitAmount) == true)
                    {
                        this.TotalUnitAmount = _globFunc.GetRoundUp(Session, (this.Qty * this.UnitAmount), ObjectList.PrePurchaseOrder2, FieldName.TotalUnitAmount);
                    }
                    else
                    {
                        this.TotalUnitAmount = this.Qty * this.UnitAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PrePurchaseOrder2 " + ex.ToString());
            }
        }

        #endregion AmountMethod

    }
}