﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.SystemModule;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Editors;


namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Sales")]
    [RuleCombinationOfPropertiesIsUnique("SalesQuotationMonitoringRuleUnique", DefaultContexts.Save, "Code")]
    //[DeferredDeletion]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class SalesQuotationMonitoring : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private bool _select;
        private SalesQuotation _salesQuotation;
        private SalesQuotationLine _salesQuotationLine;
        private SalesOrder _salesOrder;
        private SalesOrderLine _salesOrderLine;
        private OrderType _salesType;
        private XPCollection<Item> _availableItem;
        private Item _item;
        #region InisialisasiDefaultQty
        private double _dQty;
        private UnitOfMeasure _dUom;
        private double _qty;
        private UnitOfMeasure _uom;
        private double _tQty;
        private GlobalFunction _globFunc;
        #endregion InisialisasiDefaultQty 
        #region InisialisasiAmount
        private Currency _currency;
        private PriceGroup _priceGroup;
        private XPCollection<Price> _availablePrice;
        private Price _price;
        private XPCollection<PriceLine> _availablePriceLine;
        private PriceLine _priceLine;
        private double _uAmount;
        private double _tUAmount;
        private bool _multiTax;
        private Tax _tax;
        private double _txValue;
        private double _txAmount;
        private bool _multiDiscount;
        private Discount _discount;
        private double _disc;
        private double _discAmount;
        private double _tAmount;
        #endregion InisialisasiAmount
        private Status _status;
        private DateTime _statusDate;
        private int _postedCount;
        public SalesQuotationMonitoring(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            _globFunc = new GlobalFunction();
            DateTime now = DateTime.Now;
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.SalesQuotationMonitoring);
            this.Select = true;
            this.Status = Status.Open;
            this.StatusDate = now;
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field
        
        [Appearance("SalesQuotationMonitoringCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        [Appearance("SalesQuotationMonitoringSalesQuotationClose", Enabled = false)]
        public SalesQuotation SalesQuotation
        {
            get { return _salesQuotation; }
            set { SetPropertyValue("SalesQuotation", ref _salesQuotation, value); }
        }

        [Appearance("SalesQuotationMonitoringSalesQuotationLineClose", Enabled = false)]
        public SalesQuotationLine SalesQuotationLine
        {
            get { return _salesQuotationLine; }
            set { SetPropertyValue("SalesQuotationLine", ref _salesQuotationLine, value); }
        }

        [Appearance("SalesQuotationMonitoringSalesOrderClose", Enabled = false)]
        public SalesOrder SalesOrder
        {
            get { return _salesOrder; }
            set { SetPropertyValue("SalesOrder", ref _salesOrder, value); }
        }

        [Appearance("SalesQuotationMonitoringSalesOrderLineClose", Enabled = false)]
        public SalesOrderLine SalesOrderLine
        {
            get { return _salesOrderLine; }
            set { SetPropertyValue("SalesOrderLine", ref _salesOrderLine, value); }
        }

        [ImmediatePostData()]
        [Appearance("SalesQuotationMonitoringSalesTypeClose", Enabled = false)]
        public OrderType SalesType
        {
            get { return _salesType; }
            set { SetPropertyValue("SalesType", ref _salesType, value); }
        }

        [Browsable(false)]
        public XPCollection<Item> AvailableItem
        {
            get
            {
                if (SalesType == OrderType.Item)
                {
                    _availableItem = new XPCollection<Item>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("OrderType", OrderType.Item)));

                }
                else if (SalesType == OrderType.Service)
                {
                    _availableItem = new XPCollection<Item>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("OrderType", OrderType.Service)));
                }
                else
                {
                    _availableItem = new XPCollection<Item>(Session);
                }

                return _availableItem;

            }
        }

        [DataSourceProperty("AvailableItem", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("SalesQuotationMonitoringItemClose", Enabled = false)]
        public Item Item
        {
            get { return _item; }
            set { SetPropertyValue("Item", ref _item, value); }
        }

        #region DefaultQty

        [Appearance("SalesQuotationMonitoringDQtyClose", Enabled = false)]
        [ImmediatePostData()]
        public double DQty
        {
            get { return _dQty; }
            set { SetPropertyValue("DQty", ref _dQty, value); }
        }

        [Appearance("SalesQuotationMonitoringDUOMClose", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM
        {
            get { return _dUom; }
            set { SetPropertyValue("DUOM", ref _dUom, value); }
        }

        [Appearance("SalesQuotationMonitoringQtyClose", Enabled = false)]
        [ImmediatePostData()]
        public double Qty
        {
            get { return _qty; }
            set { SetPropertyValue("Qty", ref _qty, value); }
        }

        [Appearance("SalesQuotationMonitoringUOMClose", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set { SetPropertyValue("UOM", ref _uom, value); }
        }

        [Appearance("SalesQuotationMonitoringTotalQtyEnabled", Enabled = false)]
        public double TQty
        {
            get { return _tQty; }
            set { SetPropertyValue("TotalQty", ref _tQty, value); }
        }
        #endregion DefaultQty

        #region Amount

        [Appearance("SalesQuotationMonitoringCurrencyClose", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        [ImmediatePostData()]
        [Appearance("SalesQuotationMonitoringPriceGroupClose", Enabled = false)]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set { SetPropertyValue("PriceGroup", ref _priceGroup, value); }
        }

        [Browsable(false)]
        public XPCollection<Price> AvailablePrice
        {
            get
            {
                if (this.Item != null && this.PriceGroup != null)
                {
                    _availablePrice = new XPCollection<Price>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Item", this.Item),
                                    new BinaryOperator("PriceGroup", this.PriceGroup)));

                }

                else
                {
                    _availablePrice = new XPCollection<Price>(Session);
                }

                return _availablePrice;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailablePrice", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("SalesQuotationMonitoringPriceClose", Enabled = false)]
        public Price Price
        {
            get { return _price; }
            set { SetPropertyValue("Price", ref _price, value); }
        }

        [Browsable(false)]
        public XPCollection<PriceLine> AvailablePriceLine
        {
            get
            {
                if (this.Price != null)
                {
                    _availablePriceLine = new XPCollection<PriceLine>(Session,
                                          new GroupOperator(GroupOperatorType.And,
                                          new BinaryOperator("Price", this.Price)));

                }

                else
                {
                    _availablePriceLine = new XPCollection<PriceLine>(Session);
                }

                return _availablePriceLine;

            }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [DataSourceProperty("AvailablePriceLine", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("SalesQuotationMonitoringPriceLineClose", Enabled = false)]
        public PriceLine PriceLine
        {
            get { return _priceLine; }
            set
            {
                SetPropertyValue("PriceLine", ref _priceLine, value);
                if (!IsLoading)
                {
                    if (this._priceLine != null)
                    {
                        this.UAmount = this._priceLine.Amount2;
                    }
                }
            }
        }

        [Appearance("SalesQuotationMonitoringUAmountClose", Enabled = false)]
        [ImmediatePostData()]
        public double UAmount
        {
            get { return _uAmount; }
            set { SetPropertyValue("UAmount", ref _uAmount, value); }
        }

        [Appearance("SalesQuotationMonitoringTUAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TUAmount
        {
            get { return _tUAmount; }
            set { SetPropertyValue("TUAmount", ref _tUAmount, value); }
        }

        [Appearance("SalesQuotationMonitoringMultiTaxClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public bool MultiTax
        {
            get { return _multiTax; }
            set { SetPropertyValue("ActivationPosting", ref _multiTax, value); }
        }

        [Appearance("SalesQuotationMonitoringTaxClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("SalesQuotationMonitoringTaxEnabled", Criteria = "MultiTax = true", Enabled = false)]
        [ImmediatePostData]
        [DataSourceCriteria("Active = true")]
        public Tax Tax
        {
            get { return _tax; }
            set { SetPropertyValue("Tax", ref _tax, value); }
        }

        [Appearance("SalesQuotationMonitoringTxValueClose", Enabled = false)]
        [ImmediatePostData()]
        public double TxValue
        {
            get { return _txValue; }
            set { SetPropertyValue("TxValue", ref _txValue, value); }
        }

        [Appearance("SalesQuotationMonitoringTxAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TxAmount
        {
            get { return _txAmount; }
            set { SetPropertyValue("TxAmount", ref _txAmount, value); }
        }

        [Appearance("SalesQuotationMonitoringMultiDiscountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public bool MultiDiscount
        {
            get { return _multiDiscount; }
            set { SetPropertyValue("MultiDiscount", ref _multiDiscount, value); }
        }

        [Appearance("SalesQuotationMonitoringDiscountClose", Enabled = false)]
        [Appearance("SalesQuotationMonitoringDiscountEnabled", Criteria = "MultiTax = true", Enabled = false)]
        [ImmediatePostData()]
        public Discount Discount
        {
            get { return _discount; }
            set { SetPropertyValue("Discount", ref _discount, value); }
        }

        [Appearance("SalesQuotationMonitoringDiscClose", Enabled = false)]
        [Appearance("SalesQuotationMonitoringDiscEnabled", Criteria = "MultiDiscount = true", Enabled = false)]
        [ImmediatePostData()]
        public double Disc
        {
            get { return _disc; }
            set { SetPropertyValue("Disc", ref _disc, value); }
        }

        [Appearance("SalesQuotationMonitoringDiscAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double DiscAmount
        {
            get { return _discAmount; }
            set { SetPropertyValue("DiscAmount", ref _discAmount, value); }
        }

        [Appearance("SalesQuotationMonitoringTAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TAmount
        {
            get { return _tAmount; }
            set { SetPropertyValue("TAmount", ref _tAmount, value); }
        }
        #endregion Amount

        [Appearance("SalesQuotationMonitoringStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("SalesQuotationMonitoringStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("SalesQuotationMonitoringPostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }
        #endregion Field

    }
}