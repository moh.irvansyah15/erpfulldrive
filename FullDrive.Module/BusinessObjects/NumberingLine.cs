﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Setup")]
    [OptimisticLocking(false)]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class NumberingLine : FullDriveSysBaseObject
    {
        #region Default

        private int _no;
        private string _name;
        private ObjectList _objectList;
        private string _prefix;
        private string _formatNumber;
        private string _suffix;
        private int _lastValue;
        private int _incrementValue;
        private int _minValue;
        private string _formatedValue;
        private DocumentType _documentType;
        private Employee _pic;
        private PriceGroup _priceGroup;
        private bool _active;
        private bool _default;
        private bool _selection;
        private bool _sign;
        private NumberingHeader _numberingHeader;

        public NumberingLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        //No
        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading)
            {
                if (this.Default == true)
                {
                    CheckDefaults();
                }
                UpdateNo();
            }
        }
        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        #endregion Default

        #region Field

        [Appearance("NumberingLineNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        public ObjectList ObjectList
        {
            get { return _objectList; }
            set { SetPropertyValue("ObjectList", ref _objectList, value); }
        }

        public string Prefix
        {
            get { return _prefix; }
            set { SetPropertyValue("Prefix", ref _prefix, value); }
        }

        public string FormatNumber
        {
            get { return _formatNumber; }
            set { SetPropertyValue("FormatNumber", ref _formatNumber, value); }
        }

        public string Suffix
        {
            get { return _suffix; }
            set { SetPropertyValue("Suffix", ref _suffix, value); }
        }

        public int LastValue
        {
            get { return _lastValue; }
            set { SetPropertyValue("LastValue", ref _lastValue, value); }
        }

        public int IncrementValue
        {
            get { return _incrementValue; }
            set { SetPropertyValue("IncrementValue", ref _incrementValue, value); }
        }

        public int MinValue
        {
            get { return _minValue; }
            set { SetPropertyValue("MinValue", ref _minValue, value); }
        }

        public string FormatedValue
        {
            get
            {
                string strFormat;
                if (this.FormatNumber == null)
                {
                    strFormat = "{0:0}";
                }
                else
                {
                    strFormat = String.Format("{{0:{0}}}", this.FormatNumber);
                }
                _formatedValue = this.Prefix + string.Format(strFormat, this.LastValue) + this.Suffix;
                return _formatedValue;
            }
        }

        public DocumentType DocumentType
        {
            get { return _documentType; }
            set { SetPropertyValue("DocumentType", ref _documentType, value); }
        }

        public Employee PIC
        {
            get { return _pic; }
            set { SetPropertyValue("PIC", ref _pic, value); }
        }

        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set { SetPropertyValue("PriceGroup", ref _priceGroup, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        public bool Default
        {
            get { return _default; }
            set { SetPropertyValue("Default", ref _default, value); }
        }

        public bool Selection
        {
            get { return _selection; }
            set { SetPropertyValue("Selection", ref _selection, value); }
        }

        public bool Sign
        {
            get { return _sign; }
            set { SetPropertyValue("Sign", ref _sign, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Association("NumberingHeader-NumberingLines")]
        public NumberingHeader NumberingHeader
        {
            get { return _numberingHeader; }
            set
            {
                NumberingHeader oldNumberingHeader = _numberingHeader;
                SetPropertyValue("NumberingHeader", ref _numberingHeader, value);
                if (!IsLoading && !IsSaving && !object.ReferenceEquals(oldNumberingHeader, _numberingHeader))
                {
                    oldNumberingHeader = oldNumberingHeader ?? _numberingHeader;
                    oldNumberingHeader.UpdateTotalLine(true);
                }
            }
        }

        #endregion Field

        //===== Code Only =====

        #region Numbering

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.NumberingHeader != null)
                    {
                        object _makRecord = Session.Evaluate<NumberingLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("NumberingHeader=?", this.NumberingHeader));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = NumberingLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.NumberingHeader != null)
                {
                    NumberingHeader _numHeader = Session.FindObject<NumberingHeader>
                                                (new BinaryOperator("Code", this.NumberingHeader.Code));

                    XPCollection<NumberingLine> _numLines = new XPCollection<NumberingLine>
                                                (Session, new BinaryOperator("NumberingHeader", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (NumberingLine _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                        Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = NumberingLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.NumberingHeader != null)
                {
                    NumberingHeader _numHeader = Session.FindObject<NumberingHeader>
                                                (new BinaryOperator("Code", this.NumberingHeader.Code));

                    XPCollection<NumberingLine> _numLines = new XPCollection<NumberingLine>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("NumberingHeader", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (NumberingLine _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                        Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = NumberingLine " + ex.ToString());
            }
        }

        #endregion Numbering

        private void CheckDefaults()
        {
            try
            {
                XPCollection<NumberingLine> _numSetups = new XPCollection<NumberingLine>(Session,
                                                            new BinaryOperator("This", this, BinaryOperatorType.NotEqual));
                if (_numSetups == null)
                {
                    return;
                }
                else
                {
                    foreach (NumberingLine _numSetup in _numSetups)
                    {
                        if (_numSetup.ObjectList == this.ObjectList)
                        {
                            _numSetup.Default = false;
                            _numSetup.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = NumberingLine ", ex.ToString());
            }
        }
    }
}