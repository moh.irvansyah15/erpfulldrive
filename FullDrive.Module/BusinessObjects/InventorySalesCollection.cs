﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Inventory")]
    [RuleCombinationOfPropertiesIsUnique("InventorySalesCollectionRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class InventorySalesCollection : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private bool _activationPosting;
        private string _code;
        private XPCollection<SalesOrder> _availableSalesOrder;
        private SalesOrder _salesOrder;
        private XPCollection<InventoryTransferOrder> _availableInventoryTransferOrder;
        private InventoryTransferOrder _inventoryTransferOrder;
        private InventoryTransferOut _inventoryTransferOut;
        private Status _status;
        private DateTime _statusDate;
        private int _postedCount;
        private GlobalFunction _globFunc;

        public InventorySalesCollection(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.InventorySalesCollection);
                DateTime now = DateTime.Now;
                this.Status = Status.Open;
                this.StatusDate = now;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("InventorySalesCollectionCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Browsable(false)]
        public XPCollection<SalesOrder> AvailableSalesOrder
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (!IsLoading)
                {
                    if (this.InventoryTransferOut != null && this.InventoryTransferOut.PriceGroup != null)
                    {
                        if (this.InventoryTransferOut.BusinessPartner != null)
                        {
                            XPQuery<SalesOrderMonitoring> _salesOrderMonitoringsQuery = new XPQuery<SalesOrderMonitoring>(Session);

                            var _salesOrderMonitorings = from som in _salesOrderMonitoringsQuery
                                                         where (som.Status == Status.Open && som.PostedCount == 0
                                                         && som.PriceGroup == this.InventoryTransferOut.PriceGroup
                                                         && som.SalesOrder.SalesToCostumer == this.InventoryTransferOut.BusinessPartner
                                                         && som.InventoryTransferOut == null)
                                                         group som by som.SalesOrder into g
                                                         select new { SalesOrder = g.Key };

                            if (_salesOrderMonitorings != null && _salesOrderMonitorings.Count() > 0)
                            {
                                List<string> _stringSOM = new List<string>();

                                foreach (var _salesOrderMonitoring in _salesOrderMonitorings)
                                {
                                    if (_salesOrderMonitoring != null)
                                    {
                                        _stringSOM.Add(_salesOrderMonitoring.SalesOrder.Code);
                                    }
                                }

                                IEnumerable<string> _stringArraySOMDistinct = _stringSOM.Distinct();
                                string[] _stringArraySOMList = _stringArraySOMDistinct.ToArray();
                                if (_stringArraySOMList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArraySOMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArraySOMList[i] + "'";
                                        }
                                    }
                                }
                                else if (_stringArraySOMList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArraySOMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArraySOMList[i] + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _stringArraySOMList[i] + "'";
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availableSalesOrder = new XPCollection<SalesOrder>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                        }
                    }
                }

                return _availableSalesOrder;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableSalesOrder", DataSourcePropertyIsNullMode.SelectNothing)]
        public SalesOrder SalesOrder
        {
            get { return _salesOrder; }
            set { SetPropertyValue("SalesOrder", ref _salesOrder, value); }
        }

        public XPCollection<InventoryTransferOrder> AvailableInventoryTransferOrder
        {
            get
            {
                if (!IsLoading)
                {
                    if (this.InventoryTransferOut != null)
                    {
                        if (InventoryTransferOut.BusinessPartner != null)
                        {
                            XPCollection<InventoryTransferOrder> _locCollectionITOs = new XPCollection<InventoryTransferOrder>
                                                                            (Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("ActiveApproved3", true),
                                                                            new BinaryOperator("BusinessPartner", InventoryTransferOut.BusinessPartner),
                                                                            new GroupOperator(GroupOperatorType.Or,
                                                                            new BinaryOperator("Status", Status.Progress),
                                                                            new BinaryOperator("Status", Status.Posted)))
                                                                            );

                            if (_locCollectionITOs != null && _locCollectionITOs.Count() > 0)
                            {
                                _availableInventoryTransferOrder = _locCollectionITOs;
                            }
                        }             
                    }
                }
                return _availableInventoryTransferOrder;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableInventoryTransferOrder", DataSourcePropertyIsNullMode.SelectNothing)]
        public InventoryTransferOrder InventoryTransferOrder
        {
            get { return _inventoryTransferOrder; }
            set { SetPropertyValue("InventoryTransferOrder", ref _inventoryTransferOrder, value); }
        }

        [ImmediatePostData()]
        [Appearance("InventorySalesCollectionPurchaseOrderClose", Enabled = false)]
        [Association("InventoryTransferOut-InventorySalesCollections")]
        public InventoryTransferOut InventoryTransferOut
        {
            get { return _inventoryTransferOut; }
            set { SetPropertyValue("InventoryTransferOut", ref _inventoryTransferOut, value); }
        }

        [Appearance("InventorySalesCollectionStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("InventorySalesCollectionStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("InventorySalesCollectionPostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }
        #endregion Field
    }
}