﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.SystemModule;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Editors;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Sales")]
    [RuleCombinationOfPropertiesIsUnique("SalesOrderLineRuleUnique", DefaultContexts.Save, "Code")]
    //[DeferredDeletion]
    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class SalesOrderLine : FullDriveSysBaseObject
    {
        #region Default

        private bool _activationQuantity;
        private bool _activationPosting;
        private bool _selectEnabled;
        private int _no;
        private string _code;
        private bool _select;
        private OrderType _salesType;
        private XPCollection<Item> _availableItem;
        private Item _item;
        private string _name;
        private string _description;
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasure;
        #region InisialisasiMaxQty
        private double _mxDQTY;
        private UnitOfMeasure _mxDUom;
        private double _mxQty;
        private UnitOfMeasure _mxUom;
        private double _mxTQty;
        private double _mxUAmount;
        private double _mxTUAmount;
        #endregion InisialisasiMaxQty
        #region InisialisasiDefaultQty
        private double _dQty;
        private UnitOfMeasure _dUom;
        private double _qty;
        private UnitOfMeasure _uom;
        private double _tQty;
        #endregion InisialisasiDefaultQty
        #region InisialisasiRemainQty
        private double _rmDQty;
        private double _rmQty;
        private double _rmTQty;
        #endregion InisialisasiRemainQty
        #region InitialPostingQuantityTo
        private double _pDQty;
        private UnitOfMeasure _pDUom;
        private double _pQty;
        private UnitOfMeasure _pUom;
        private double _pTQty;
        #endregion InitialPostingQuantityTo
        #region InisialisasiAmount
        private Currency _currency;
        private PriceGroup _priceGroup;
        private XPCollection<Price> _availablePrice;
        private Price _price;
        private XPCollection<PriceLine> _availablePriceLine;
        private PriceLine _priceLine;
        private double _uAmount;
        private double _tUAmount;
        private bool _multiTax;
        private Tax _tax;
        private double _txValue;
        private double _txAmount;
        private bool _multiDiscount;
        private Discount _discount;
        private double _disc;
        private double _discAmount;
        private double _tAmount;
        #endregion InisialisasiAmount
        private DateTime _estimatedDate;
        private Status _status;
        private DateTime _statusDate;
        private int _postedCount;
        private string _signCode;
        private Company _company;
        private SalesQuotationMonitoring _salesQuotationMonitoring;
        private SalesOrder _salesOrder;
        private GlobalFunction _globFunc;
        //clm
        private bool _hideSumTotalItem;
        private int _postedCountWR;
        private Status _statusWR;
        private DateTime _statusDateWR;
        private double _totalPPN;
        private double _totalPPH;
        private double _totalNPWP;
        private double _totalNonNPWP;
        private double _totalMaterai;

        public SalesOrderLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).

            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.SalesOrderLine);
                DateTime now = DateTime.Now;
                this.SalesType = CustomProcess.OrderType.Item;
                this.Status = CustomProcess.Status.Open;
                this.StatusWR = CustomProcess.Status.Open;
                this.StatusDate = now;
                this.Select = true;
            }
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
                SetActivationPostingBasedActivationQuantity();
            }
        }
        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        #endregion Default

        #region Field

        [Browsable(false)]
        public bool ActivationQuantity
        {
            get { return _activationQuantity; }
            set { SetPropertyValue("ActivationQuantity", ref _activationQuantity, value); }
        }

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public bool SelectEnabled
        {
            get { return _selectEnabled; }
            set { SetPropertyValue("SelectEnabled", ref _selectEnabled, value); }
        }

        [Appearance("SalesOrderLineNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("SalesOrderLineCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("SalesOrderLineSelectClose", Criteria = "SelectEnabled = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        [ImmediatePostData()]
        [Appearance("SalesOrderLineSalesTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("SalesOrderLineSalesTypeClose2", Criteria = "ActivationQuantity = true", Enabled = false)]
        public OrderType SalesType
        {
            get { return _salesType; }
            set { SetPropertyValue("SalesType", ref _salesType, value); }
        }

        [Browsable(false)]
        public XPCollection<Item> AvailableItem
        {
            get
            {
                if (SalesType == OrderType.Item)
                {
                    _availableItem = new XPCollection<Item>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("OrderType", OrderType.Item)));

                }
                else if (SalesType == OrderType.Service)
                {
                    _availableItem = new XPCollection<Item>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("OrderType", OrderType.Service)));
                }
                else
                {
                    _availableItem = new XPCollection<Item>(Session);
                }

                return _availableItem;

            }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [DataSourceProperty("AvailableItem", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("SalesOrderLineItemClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("SalesOrderLineItemClose2", Criteria = "ActivationQuantity = true", Enabled = false)]
        public Item Item
        {
            get { return _item; }
            set
            {
                SetPropertyValue("Item", ref _item, value);

                if (!IsLoading)
                {
                    if (this._item != null)
                    {
                        this.DUOM = this._item.BasedUOM;
                        this.Description = GetDescriptions();
                    }
                }
            }
        }

        [Appearance("SalesOrderLineNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("SalesOrderLineNameClose2", Criteria = "ActivationQuantity = true", Enabled = false)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Appearance("SalesOrderLineDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("SalesOrderLineDescriptionClose2", Criteria = "ActivationQuantity = true", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Browsable(false)]
        public XPCollection<UnitOfMeasure> AvailableUnitOfMeasure
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                if (Item == null)
                {
                    _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session);
                }
                else
                {
                    List<string> _stringUOM = new List<string>();

                    XPCollection<ItemUnitOfMeasure> _locItemUnitOfMeasures = new XPCollection<ItemUnitOfMeasure>
                                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Item", this.Item),
                                                                               new BinaryOperator("Active", true)));

                    if (_locItemUnitOfMeasures != null && _locItemUnitOfMeasures.Count() > 0)
                    {
                        foreach (ItemUnitOfMeasure _locItemUnitOfMeasure in _locItemUnitOfMeasures)
                        {
                            _stringUOM.Add(_locItemUnitOfMeasure.UOM.Code);
                        }
                    }

                    IEnumerable<string> _stringArrayUOMDistinct = _stringUOM.Distinct();
                    string[] _stringArrayUOMList = _stringArrayUOMDistinct.ToArray();
                    if (_stringArrayUOMList.Length == 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArrayUOMList.Length > 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                                else
                                {
                                    _endString = _endString + " OR [Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session, CriteriaOperator.Parse(_fullString));
                    }

                }
                return _availableUnitOfMeasure;

            }
        }

        #region MaxDefaultQty
        [Appearance("SalesOrderLineMxDQtyClose", Enabled = false)]
        [ImmediatePostData()]
        public double MxDQty
        {
            get { return _mxDQTY; }
            set
            {
                SetPropertyValue("MxDQty", ref _mxDQTY, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        [Appearance("SalesOrderLineMxDUOMClose", Enabled = false)]
        public UnitOfMeasure MxDUOM
        {
            get { return _mxDUom; }
            set
            {
                SetPropertyValue("MxDUOM", ref _mxDUom, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("SalesOrderLineMxQtyClose", Enabled = false)]
        public double MxQty
        {
            get { return _mxQty; }
            set
            {
                SetPropertyValue("MxQty", ref _mxQty, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        [Appearance("SalesOrderLineMxUOMClose", Enabled = false)]
        public UnitOfMeasure MxUOM
        {
            get { return _mxUom; }
            set
            {
                SetPropertyValue("MxUOM", ref _mxUom, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [Appearance("SalesOrderLineMxTQtyClose", Enabled = false)]
        public double MxTQty
        {
            get { return _mxTQty; }
            set { SetPropertyValue("MxTQty", ref _mxTQty, value); }
        }

        [Appearance("SalesOrderLineMxUAmountClose", Enabled = false)]
        public double MxUAmount
        {
            get { return _mxUAmount; }
            set { SetPropertyValue("MxUAmount", ref _mxUAmount, value); }
        }

        [Appearance("SalesOrderLineMxTUAmountClose", Enabled = false)]
        public double MxTUAmount
        {
            get { return _mxTUAmount; }
            set { SetPropertyValue("MxTUAmount", ref _mxTUAmount, value); }
        }
        #endregion MaxDefaultQty

        #region DefaultQty
        [Appearance("SalesOrderLineDQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double DQty
        {
            get { return _dQty; }
            set
            {
                SetPropertyValue("DQty", ref _dQty, value);
                if (!IsLoading)
                {
                    SetMxDQty();
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTaxAmount();
                    SetDiscAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("SalesOrderLineDUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM
        {
            get { return _dUom; }
            set
            {
                SetPropertyValue("DUOM", ref _dUom, value);
                if (!IsLoading)
                {
                    SetMxDUOM();
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTaxAmount();
                    SetDiscAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("SalesOrderLineQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double Qty
        {
            get { return _qty; }
            set
            {
                SetPropertyValue("Qty", ref _qty, value);
                if (!IsLoading)
                {
                    SetMxQty();
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTaxAmount();
                    SetDiscAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("SalesOrderLineUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set
            {
                SetPropertyValue("UOM", ref _uom, value);
                if (!IsLoading)
                {
                    SetMxUOM();
                    SetTotalQty();
                    SetTotalUnitAmount();
                    SetTaxAmount();
                    SetDiscAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("SalesOrderLineTotalQtyEnabled", Enabled = false)]
        public double TQty
        {
            get { return _tQty; }
            set { SetPropertyValue("TotalQty", ref _tQty, value); }
        }
        #endregion DefaultQty

        #region RemainQty

        [Appearance("SalesOrderLineRmDQtyClose", Enabled = false)]
        public double RmDQty
        {
            get { return _rmDQty; }
            set { SetPropertyValue("RmDQty", ref _rmDQty, value); }
        }

        [Appearance("SalesOrderLineRmQtyClose", Enabled = false)]
        public double RmQty
        {
            get { return _rmQty; }
            set { SetPropertyValue("RmQty", ref _rmQty, value); }
        }

        [Appearance("SalesOrderLineRmTQtyClose", Enabled = false)]
        public double RmTQty
        {
            get { return _rmTQty; }
            set { SetPropertyValue("RmTQty", ref _rmTQty, value); }
        }

        #endregion RemainQty

        #region PostingQty

        [Appearance("SalesOrderLinePDQtyClose", Enabled = false)]
        public double PDQty
        {
            get { return _pDQty; }
            set
            {
                SetPropertyValue("PDQty", ref _pDQty, value);
                if (!IsLoading)
                {
                    SetTotalPQty();
                }
            }
        }

        [Appearance("SalesOrderLinePDUOMClose", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure PDUOM
        {
            get { return _pDUom; }
            set
            {
                SetPropertyValue("PDUOM", ref _pDUom, value);
                if (!IsLoading)
                {
                    SetTotalPQty();
                }
            }
        }

        [Appearance("SalesOrderLinePQtyToClose", Enabled = false)]
        public double PQty
        {
            get { return _pQty; }
            set
            {
                SetPropertyValue("PQty", ref _pQty, value);
                if (!IsLoading)
                {
                    SetTotalPQty();
                }
            }
        }

        [Appearance("SalesOrderLinePUOMClose", Enabled = false)]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure PUOM
        {
            get { return _pUom; }
            set
            {
                SetPropertyValue("PUOM", ref _pUom, value);
                if (!IsLoading)
                {
                    SetTotalPQty();
                }
            }
        }

        [Appearance("SalesOrderLinePTQtyEnabled", Enabled = false)]
        public double PTQty
        {
            get { return _pTQty; }
            set { SetPropertyValue("PTQty", ref _pTQty, value); }
        }

        #endregion PostingQty

        #region Amount
        [Appearance("SalesOrderLineCurrencyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("SalesOrderLineCurrencyClose2", Criteria = "ActivationQuantity = true", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        [ImmediatePostData()]
        [Appearance("SalesOrderLinePriceGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("SalesOrderLinePriceGroupClose2", Criteria = "ActivationQuantity = true", Enabled = false)]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set { SetPropertyValue("PriceGroup", ref _priceGroup, value); }
        }

        [Browsable(false)]
        public XPCollection<Price> AvailablePrice
        {
            get
            {
                if (this.Item != null && this.PriceGroup != null)
                {
                    _availablePrice = new XPCollection<Price>(Session,
                                    new GroupOperator(GroupOperatorType.And,
                                    new BinaryOperator("Item", this.Item),
                                    new BinaryOperator("PriceGroup", this.PriceGroup)));

                }

                else
                {
                    _availablePrice = new XPCollection<Price>(Session);
                }

                return _availablePrice;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailablePrice", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("SalesOrderLinePriceClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("SalesOrderLinePriceClose2", Criteria = "ActivationQuantity = true", Enabled = false)]
        public Price Price
        {
            get { return _price; }
            set { SetPropertyValue("Price", ref _price, value); }
        }

        [Browsable(false)]
        public XPCollection<PriceLine> AvailablePriceLine
        {
            get
            {
                if (this.Price != null)
                {
                    _availablePriceLine = new XPCollection<PriceLine>(Session,
                                          new GroupOperator(GroupOperatorType.And,
                                          new BinaryOperator("Price", this.Price)));

                }

                else
                {
                    _availablePriceLine = new XPCollection<PriceLine>(Session);
                }

                return _availablePriceLine;

            }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [DataSourceProperty("AvailablePriceLine", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("SalesOrderLinePriceLineClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("SalesOrderLinePriceLineClose2", Criteria = "ActivationQuantity = true", Enabled = false)]
        public PriceLine PriceLine
        {
            get { return _priceLine; }
            set
            {
                SetPropertyValue("PriceLine", ref _priceLine, value);
                if (!IsLoading)
                {
                    if (this._priceLine != null)
                    {
                        this.UAmount = this._priceLine.Amount2;
                    }
                }
            }
        }

        [Appearance("SalesOrderLineUAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("SalesOrderLineUAmountClose2", Criteria = "ActivationQuantity = true", Enabled = false)]
        [ImmediatePostData()]
        public double UAmount
        {
            get { return _uAmount; }
            set
            {
                SetPropertyValue("UAmount", ref _uAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._uAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.SalesOrderLine, FieldName.UAmount) == true)
                        {
                            this._uAmount = _globFunc.GetRoundUp(Session, this._uAmount, ObjectList.SalesOrderLine, FieldName.UAmount);
                        }
                    }
                    SetTotalUnitAmount();
                    SetTaxAmount();
                    SetDiscAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("SalesOrderLineTUAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TUAmount
        {
            get { return _tUAmount; }
            set { SetPropertyValue("TUAmount", ref _tUAmount, value); }
        }

        [Appearance("SalesOrderLineMultiTaxClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("SalesOrderLineMultiTaxClose2", Criteria = "ActivationQuantity = true", Enabled = false)]
        [ImmediatePostData()]
        public bool MultiTax
        {
            get { return _multiTax; }
            set
            {
                SetPropertyValue("ActivationPosting", ref _multiTax, value);
                if (!IsLoading)
                {
                    SetNormalTax();
                }
            }
        }

        [Appearance("SalesOrderLineTaxClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("SalesOrderLineTaxClose2", Criteria = "ActivationQuantity = true", Enabled = false)]
        [Appearance("SalesOrderLineTaxEnabled", Criteria = "MultiTax = true", Enabled = false)]
        [ImmediatePostData]
        [DataSourceCriteria("Active = true")]
        public Tax Tax
        {
            get { return _tax; }
            set
            {
                SetPropertyValue("Tax", ref _tax, value);
                if (!IsLoading)
                {
                    if (this._tax != null)
                    {
                        this.TxValue = this._tax.Value;
                        SetTaxAmount();
                        SetTotalAmount();
                    }
                    else
                    {
                        SetNormalTax();
                    }
                }
            }
        }

        [Appearance("SalesOrderLineTxValueClose", Enabled = false)]
        [ImmediatePostData()]
        public double TxValue
        {
            get { return _txValue; }
            set
            {
                SetPropertyValue("TxValue", ref _txValue, value);
                if (!IsLoading)
                {
                    SetTaxAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("SalesOrderLineTxAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TxAmount
        {
            get { return _txAmount; }
            set
            {
                SetPropertyValue("TxAmount", ref _txAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._txAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.SalesOrderLine, FieldName.TxAmount) == true)
                        {
                            this._txAmount = _globFunc.GetRoundUp(Session, this._txAmount, ObjectList.SalesOrderLine, FieldName.TxAmount);
                        }
                    }

                    SetTotalAmount();
                }
            }
        }

        [Appearance("SalesOrderLineMultiDiscountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("SalesOrderLineMultiDiscountClose2", Criteria = "ActivationQuantity = true", Enabled = false)]
        [ImmediatePostData()]
        public bool MultiDiscount
        {
            get { return _multiDiscount; }
            set
            {
                SetPropertyValue("MultiDiscount", ref _multiDiscount, value);
                if (!IsLoading)
                {
                    SetNormalDiscount();
                }
            }
        }

        [Appearance("SalesOrderLineDiscountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("SalesOrderLineDiscountClose2", Criteria = "ActivationQuantity = true", Enabled = false)]
        [Appearance("SalesOrderLineDiscountEnabled", Criteria = "MultiDiscount = true", Enabled = false)]
        [ImmediatePostData()]
        public Discount Discount
        {
            get { return _discount; }
            set
            {
                SetPropertyValue("Discount", ref _discount, value);
                if(!IsLoading)
                {
                    if(this._discount != null)
                    {
                        this.Disc = this._discount.Value;
                    }
                    else
                    {
                        SetNormalDiscount();
                    }
                }
            }
        }

        [Appearance("SalesOrderLineDiscClose", Enabled = false)]
        [Appearance("SalesOrderLineDiscEnabled", Criteria = "MultiDiscount = true", Enabled = false)]
        [ImmediatePostData()]
        public double Disc
        {
            get { return _disc; }
            set
            {
                SetPropertyValue("Disc", ref _disc, value);
                if (!IsLoading)
                {
                    SetDiscAmount();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("SalesOrderLineDiscAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double DiscAmount
        {
            get { return _discAmount; }
            set
            {
                SetPropertyValue("DiscAmount", ref _discAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._discAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.SalesOrderLine, FieldName.DiscAmount) == true)
                        {
                            this._discAmount = _globFunc.GetRoundUp(Session, this._discAmount, ObjectList.SalesOrderLine, FieldName.DiscAmount);
                        }
                    }
                }
            }
        }

        [Appearance("SalesOrderLineTPriceEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TAmount
        {
            get { return _tAmount; }
            set
            {
                SetPropertyValue("TAmount", ref _tAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._tAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.SalesOrderLine, FieldName.TAmount) == true)
                        {
                            this._tAmount = _globFunc.GetRoundUp(Session, this._tAmount, ObjectList.SalesOrderLine, FieldName.TAmount);
                        }
                    }
                }
            }
        }
        #endregion Amount

        [Appearance("SalesOrderLineEstimationDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("SalesOrderLineEstimationDateClose2", Criteria = "ActivationQuantity = true", Enabled = false)]
        public DateTime EstimatedDate
        {
            get { return _estimatedDate; }
            set { SetPropertyValue("EstimatedDate", ref _estimatedDate, value); }
        }

        [Appearance("SalesOrderLineStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("SalesOrderLineStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("SalesOrderLinePostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        [Browsable(false)]
        [Appearance("SalesOrderStatusWRClose", Enabled = false)]
        public Status StatusWR
        {
            get { return _statusWR; }
            set { SetPropertyValue("StatusWR", ref _statusWR, value); }
        }

        [Browsable(false)]
        [Appearance("SalesOrderStatusDateWRClose", Enabled = false)]
        public DateTime StatusDateWR
        {
            get { return _statusDateWR; }
            set { SetPropertyValue("StatusDateWR", ref _statusDateWR, value); }
        }

        [Appearance("SalesOrderLineSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [Browsable(false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Browsable(false)]
        public SalesQuotationMonitoring SalesQuotationMonitoring
        {
            get { return _salesQuotationMonitoring; }
            set { SetPropertyValue("SalesQuotationMonitoring", ref _salesQuotationMonitoring, value); }
        }

        [ImmediatePostData()]
        [Association("SalesOrder-SalesOrderLines")]
        [Appearance("SalesOrderLineSalesOrderEnabled", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public SalesOrder SalesOrder
        {
            get { return _salesOrder; }
            set
            {
                SetPropertyValue("SalesOrder", ref _salesOrder, value);
                if (!IsLoading)
                {
                    if (this._salesOrder != null)
                    {
                        if (this._salesOrder.EstimatedDate != null)
                        {
                            this.EstimatedDate = this._salesOrder.EstimatedDate;
                        }
                        if (this._salesOrder.Company != null)
                        {
                            this.Company = this._salesOrder.Company;
                        }
                        if (this._salesOrder.PriceGroup != null)
                        {
                            this.PriceGroup = this._salesOrder.PriceGroup;
                        }
                        if (this._salesOrder.Currency != null)
                        {
                            this.Currency = this._salesOrder.Currency;
                        }
                    }
                }
            }
        }

        #region Clm

        [Appearance("SalesOrderLinePostedCountWREnabled", Enabled = false)]
        public int PostedCountWR
        {
            get { return _postedCountWR; }
            set { SetPropertyValue("PostedCountWR", ref _postedCountWR, value); }
        }

        //grand
        [Browsable(false)]
        public bool HideSumTotalItem
        {
            get { return _hideSumTotalItem; }
            set { SetPropertyValue("HideSumTotalItem", ref _hideSumTotalItem, value); }
        }

        [Appearance("SalesOrderLineTotalSOLEnabled", Criteria = "HideSumTotalItem = false", Visibility = ViewItemVisibility.Hide)]
        [Persistent("TotalSOL")]
        public double TotalSOL
        {
            get
            {
                double _result = 0;
                if (!IsLoading)
                {
                    if (GetTotalSOL() >= 0)
                    {
                        _result = UpdateTotalSOL(true);
                    }
                }
                return _result;
            }
        }

        [Appearance("SalesOrderLineTAmountSOLEnabled", Enabled = false, Criteria = "HideSumTotalItem = false", Visibility = ViewItemVisibility.Hide)]
        [ImmediatePostData()]
        public double TAmountSOL
        {
            get
            {
                if (this._tAmount >= 0)
                {
                    return GetTotalAmountSOL();
                }
                else
                {
                    return 0;
                }
            }
        }

        #region Report
        [ImmediatePostData()]
        [Appearance("SalesOrderLineTotalPPNEnabled", Enabled = false, Visibility = ViewItemVisibility.Hide)]
        public double TotalPPN
        {
            get { return _totalPPN; }
            set { SetPropertyValue("TotalPPN", ref _totalPPN, value); }
        }

        [ImmediatePostData()]
        [Appearance("SalesOrderLineTotalPPHEnabled", Enabled = false, Visibility = ViewItemVisibility.Hide)]
        public double TotalPPH
        {
            get { return _totalPPH; }
            set { SetPropertyValue("TotalPPH", ref _totalPPH, value); }
        }

        [ImmediatePostData()]
        [Appearance("SalesOrderLineTotalNPWPEnabled", Enabled = false, Visibility = ViewItemVisibility.Hide)]
        public double TotalNPWP
        {
            get { return _totalNPWP; }
            set { SetPropertyValue("TotalNPWP", ref _totalNPWP, value); }
        }

        [ImmediatePostData()]
        [Appearance("SalesOrderLineTotalNonNPWPEnabled", Enabled = false, Visibility = ViewItemVisibility.Hide)]
        public double TotalNonNPWP
        {
            get { return _totalNonNPWP; }
            set { SetPropertyValue("TotaNonNPWP", ref _totalNonNPWP, value); }
        }

        [ImmediatePostData()]
        [Appearance("SalesOrderLineTotalMateraiEnabled", Enabled = false, Visibility = ViewItemVisibility.Hide)]
        public double TotalMaterai
        {
            get { return _totalMaterai; }
            set { SetPropertyValue("TotaMaterai", ref _totalMaterai, value); }
        }

        [Appearance("SalesOrderLineReportSOLEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double ReportTSOL
        {
            get
            {
                if (this._tAmount >= 0)
                {
                    return ReportTotalSOL();
                }
                else
                {
                    return 0;
                }
            }
        }

        [Appearance("SalesOrderLineReportDiscSOLEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double ReportDisc
        {
            get
            {
                if (this._disc >= 0)
                {
                    return ReportDiscSOL();
                }
                else
                {
                    return 0;
                }
            }
        }

        [Appearance("SalesOrderLineReportTaxSOLEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double ReportTax
        {
            get
            {
                if (this._txValue >= 0)
                {
                    return ReportTaxSOL();
                }
                else
                {
                    return 0;
                }
            }
        }
        #endregion Report

        #endregion Clm

        #endregion Field

        //==== Code Only ====

        public string GetDescriptions()
        {
            string _result = "";
            try
            {
                if (this._item != null)
                {
                    string _result1 = null;
                    string _result2 = null;
                    if (this._item.Name != null)
                    {
                        _result1 = this._item.Name;
                    }
                    if (this._item.Description != null)
                    {
                        _result2 = this._item.Description;
                    }
                    _result = _result1 + " " + _result2;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderLine " + ex.ToString());
            }

            return _result;
        }

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.SalesOrder != null)
                    {
                        object _makRecord = Session.Evaluate<SalesOrderLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("SalesOrder=?", this.SalesOrder));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.SalesOrder != null)
                {
                    SalesOrder _numHeader = Session.FindObject<SalesOrder>
                                            (new BinaryOperator("Code", this.SalesOrder.Code));

                    XPCollection<SalesOrderLine> _numLines = new XPCollection<SalesOrderLine>
                                                             (Session, new BinaryOperator("SalesOrder", _numHeader),
                                                              new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (SalesOrderLine _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.SalesOrder != null)
                {
                    SalesOrder _numHeader = Session.FindObject<SalesOrder>
                                            (new BinaryOperator("Code", this.SalesOrder.Code));

                    XPCollection<SalesOrderLine> _numLines = new XPCollection<SalesOrderLine>
                                                             (Session, new GroupOperator(GroupOperatorType.And,
                                                              new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                              new BinaryOperator("SalesOrder", _numHeader)),
                                                              new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (SalesOrderLine _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderLine " + ex.ToString());
            }
        }

        #endregion No

        #region Set

        private void SetTotalUnitAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_tQty >= 0 & _uAmount >= 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.SalesOrderLine, FieldName.TUAmount) == true)
                    {
                        this.TUAmount = _globFunc.GetRoundUp(Session, (this.TQty * this.UAmount), ObjectList.SalesOrderLine, FieldName.TUAmount);
                    }
                    else
                    {
                        this.TUAmount = this.TQty * this.UAmount;
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderLine " + ex.ToString());
            }
        }

        private void SetTotalAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_uAmount >= 0 && _txAmount >= 0 && _discAmount >= 0)
                {
                    if (!IsLoading)
                    {
                        if (this.Tax != null)
                        {
                            if (this.Tax.TaxNature == TaxNature.Increase)
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.SalesOrderLine, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount + this.TxAmount - this.DiscAmount), ObjectList.SalesOrderLine, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount + this.TxAmount - this.DiscAmount;
                                }
                            }
                            else if (this.Tax.TaxNature == TaxNature.Decrease)
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.SalesOrderLine, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount - this.TxAmount - this.DiscAmount), ObjectList.SalesOrderLine, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount - this.TxAmount - this.DiscAmount;
                                }
                            }
                            else if (this.Tax.TaxNature == TaxNature.None)
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.SalesOrderLine, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount - this.DiscAmount), ObjectList.SalesOrderLine, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount - this.DiscAmount;
                                }
                            }
                            else
                            {
                                if (_globFunc.GetRoundingList(Session, ObjectList.SalesOrderLine, FieldName.TAmount) == true)
                                {
                                    this.TAmount = _globFunc.GetRoundUp(Session, (this.TUAmount + this.TxAmount - this.DiscAmount), ObjectList.SalesOrderLine, FieldName.TAmount);
                                }
                                else
                                {
                                    this.TAmount = this.TUAmount + this.TxAmount - this.DiscAmount;
                                }
                            }
                        }
                        //clm
                        else
                        {
                            this.TAmount = this.TUAmount + this.TxAmount - this.DiscAmount;
                        }
                    }
                }
                //clm
                else
                {
                    this.TAmount = this.TUAmount + this.TxAmount - this.DiscAmount;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderLine " + ex.ToString());
            }
        }

        private void SetTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.SalesOrder != null)
                {
                    if (this.Item != null && this.UOM != null && this.DUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty * _locItemUOM.DefaultConversion + this.DQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty / _locItemUOM.Conversion + this.DQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty + this.DQty;
                            }

                            this.TQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.Qty + this.DQty;
                        this.TQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderLine " + ex.ToString());
            }
        }

        private void SetDiscAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_uAmount >= 0 && _discAmount >= 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.SalesOrderLine, FieldName.DiscAmount) == true)
                    {
                        this.DiscAmount = _globFunc.GetRoundUp(Session, (this.TUAmount * this.Disc / 100), ObjectList.SalesOrderLine, FieldName.DiscAmount);
                    }
                    else
                    {
                        this.DiscAmount = this.TUAmount * this.Disc / 100;
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderLine " + ex.ToString());
            }
        }

        private void SetTaxAmount()
        {
            try
            {
                _globFunc = new GlobalFunction();

                if (_uAmount >= 0 && Tax.Value >= 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.SalesOrderLine, FieldName.TxAmount) == true)
                    {
                        this.TxAmount = _globFunc.GetRoundUp(Session, (this.TUAmount * this.TxValue / 100), ObjectList.SalesOrderLine, FieldName.TxAmount);
                    }
                    else
                    {
                        this.TxAmount = this.TUAmount * this.TxValue / 100;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderLine " + ex.ToString());
            }
        }

        private void SetMaxTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.SalesOrder != null)
                {
                    if (this.Item != null && this.MxUOM != null && this.MxDUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.MxUOM),
                                                         new BinaryOperator("DefaultUOM", this.MxDUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.MxQty * _locItemUOM.DefaultConversion + this.MxDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.MxQty / _locItemUOM.Conversion + this.MxDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.MxQty + this.MxDQty;
                            }

                            this.MxTQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.MxQty + this.MxDQty;
                        this.MxTQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderLine " + ex.ToString());
            }
        }

        private void SetMaxTotalUnitAmount()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.SalesOrder != null)
                {
                    if (this._mxTQty >= 0 && this._mxUAmount >= 0)
                    {
                        this.MxTUAmount = this.MxTQty * this.MxUAmount;
                    }
                    else
                    {
                        _locInvLineTotal = this.MxQty + this.MxDQty;
                        this.MxTQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderLine " + ex.ToString());
            }
        }

        private void SetTotalPQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.SalesOrder != null)
                {
                    if (this.Item != null && this.UOM != null && this.DUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.PQty * _locItemUOM.DefaultConversion + this.PDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.PQty / _locItemUOM.Conversion + this.PDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.PQty + this.PDQty;
                            }

                            this.PTQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.PQty + this.PDQty;
                        this.PTQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderLine " + ex.ToString());
            }
        }

        #region SetMaxInQty
        private void SetMxDQty()
        {
            try
            {
                if (this.SalesOrder != null)
                {

                    XPCollection<ApprovalLine> _locApprovalLineXPOs = new XPCollection<ApprovalLine>
                                                                   (Session, new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("SalesOrder", this.SalesOrder)));
                    if (_locApprovalLineXPOs != null && _locApprovalLineXPOs.Count() > 0)
                    {
                        if(this.Status == Status.Posted || this.PostedCount > 0)
                        {
                            if(this.RmDQty > 0)
                            {
                                if(this._dQty > this.RmDQty)
                                {
                                    this._dQty = this.RmDQty;
                                }
                            }
                        }
                        else if(this.Status == Status.Close || this.PostedCount > 0)
                        {
                            this._dQty = 0;
                        }
                    }
                    else
                    {
                        if ((this.Status == Status.Open || this.Status == Status.Progress) && this.PostedCount <= 0)
                        {
                            XPCollection<SalesQuotationMonitoring> _locSalesQuotationMonitorings = new XPCollection<SalesQuotationMonitoring>(Session,
                                                                                                    new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("SalesOrder", this.SalesOrder),
                                                                                                    new BinaryOperator("SalesQuotationLine", this)));
                            if (_locSalesQuotationMonitorings != null && _locSalesQuotationMonitorings.Count() > 0)
                            {
                                if (this.MxDQty > 0)
                                {
                                    if (this._dQty > this.MxDQty)
                                    {
                                        this._dQty = this.MxDQty;
                                    }
                                }
                            }
                            else
                            {
                                if (this._dQty > 0)
                                {
                                    this.MxDQty = this._dQty;
                                }
                            }
                        }
                        
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderLine " + ex.ToString());
            }
        }

        private void SetMxDUOM()
        {
            try
            {
                if (this.SalesOrder != null)
                {
                    ApprovalLine _locApprovalLineXPO = Session.FindObject<ApprovalLine>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("SalesOrder", this.SalesOrder)));
                    if (_locApprovalLineXPO == null)
                    {
                        if ((this.Status == Status.Open || this.Status == Status.Progress)
                            && this.PostedCount <= 0)
                        {
                            if (this.SalesQuotationMonitoring == null)
                            {
                                this.MxDUOM = this.DUOM;
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderLine " + ex.ToString());
            }
        }

        private void SetMxQty()
        {
            try
            {
                if (this.SalesOrder != null)
                {
                    XPCollection<ApprovalLine> _locApprovalLineXPOs = new XPCollection<ApprovalLine>
                                                                   (Session, new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("SalesOrder", this.SalesOrder)));
                    if (_locApprovalLineXPOs != null && _locApprovalLineXPOs.Count() > 0)
                    {
                        if(this.Status == Status.Posted || this.PostedCount > 0)
                        {
                            if(this.RmQty > 0)
                            {
                                if(this._qty > this.RmQty)
                                {
                                    this._qty = this.RmQty;
                                }
                            }
                        }
                        else if(this.Status == Status.Close || this.PostedCount > 0)
                        {
                            this._qty = 0;
                        }
                    }
                    else
                    {
                        if ((this.Status == Status.Open || this.Status == Status.Progress) && this.PostedCount <= 0)
                        {
                            XPCollection<SalesQuotationMonitoring> _locSalesQuotationMonitorings = new XPCollection<SalesQuotationMonitoring>(Session,
                                                                                                   new GroupOperator(GroupOperatorType.And,
                                                                                                   new BinaryOperator("SalesOrder", this.SalesOrder),
                                                                                                   new BinaryOperator("SalesQuotationLine", this)));
                            if (_locSalesQuotationMonitorings != null && _locSalesQuotationMonitorings.Count() > 0)
                            {
                                if (this.MxQty > 0)
                                {
                                    if (this._qty > this.MxQty)
                                    {
                                        this._qty = this.MxQty;
                                    }
                                }
                            }
                            else
                            {
                                if (this._qty > 0)
                                {
                                    this.MxQty = this._qty;
                                }
                            }
                        }
                        
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderLine " + ex.ToString());
            }
        }

        private void SetMxUOM()
        {
            try
            {
                if (this.SalesOrder != null)
                {
                    ApprovalLine _locApprovalLineXPO = Session.FindObject<ApprovalLine>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("SalesOrder", this.SalesOrder)));
                    if (_locApprovalLineXPO == null)
                    {
                        if ((this.Status == Status.Open || this.Status == Status.Progress)
                            && this.PostedCount <= 0)
                        {
                            if (this.SalesQuotationMonitoring == null)
                            {
                                this.MxUOM = this.UOM;
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderLine " + ex.ToString());
            }
        }

        public void SetActivationPostingBasedActivationQuantity()
        {
            try
            {
                if (this.SalesOrder != null)
                {
                    if (this.ActivationQuantity == true)
                    {
                        this.ActivationPosting = true;
                        this.Save();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderLine " + ex.ToString());
            }
        }

        #endregion SetMaxInQty

        #endregion Set

        #region Tax

        [Association("SalesOrderLine-TaxLines")]
        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [Appearance("SalesOrderLineTaxLineClose", Criteria = "MultiTax = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [Appearance("SalesOrderLineTaxLineEnabled", Criteria = "MultiTax = true", Visibility = ViewItemVisibility.Show, Context = "DetailView")]
        public XPCollection<TaxLine> TaxLines
        {
            get { return GetCollection<TaxLine>("TaxLines"); }
        }

        private void SetNormalTax()
        {
            try
            {
                if (_tax != null && _txValue > 0 && _txAmount > 0)
                {
                    this.Tax = this.Tax;
                    this.TxValue = this.TxValue;
                    this.TxAmount = this.TxAmount;
                }
                else
                {
                    this.TxValue = 0;
                    this.TxAmount = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderLine " + ex.ToString());
            }
        }

        public Tax UpdateTaxLine(bool forceChangeEvents)
        {
            Tax _result = null;
            try
            {
                Tax _locTax = null;
                XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(Session,
                                                     new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("SalesOrderLine", this)),
                                                     new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));

                if (_locTaxLines != null && _locTaxLines.Count > 0)
                {
                    if (_locTaxLines.Count == 1)
                    {
                        foreach (TaxLine _locTaxLinerLine in _locTaxLines)
                        {
                            if (forceChangeEvents)
                            {
                                //OnChanged("CurrencyLine", oldCurrencyLine, _locSalesOrderLine.Currency);
                                _result = _locTaxLinerLine.Tax;
                            }
                        }
                    }
                    else
                    {
                        foreach (TaxLine _locTaxLine in _locTaxLines)
                        {
                            if (_locTaxLine.No == 1)
                            {
                                _locTax = _locTaxLine.Tax;
                            }
                            else
                            {
                                if (_locTaxLine.Tax != _locTax)
                                {
                                    _globFunc = new GlobalFunction();
                                    //OnChanged("CurrencyLine", oldCurrencyLine, _globFunc.GetDefaultCurrency(Session));
                                    _result = _globFunc.GetDefaultTax(Session, TaxMode.Sales);
                                    break;
                                }
                                else
                                {
                                    _result = _locTaxLine.Tax;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesOrderLine ", ex.ToString());
            }
            return _result;
        }

        #endregion Tax

        #region Discount

        [Association("SalesOrderLine-DiscountLines")]
        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [Appearance("SalesOrderLineDiscountLineClose", Criteria = "MultiDiscount = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [Appearance("SalesOrderLineDiscountLineEnabled", Criteria = "MultiDiscount = true", Visibility = ViewItemVisibility.Show, Context = "DetailView")]
        public XPCollection<DiscountLine> DiscountLines
        {
            get { return GetCollection<DiscountLine>("DiscountLines"); }
        }

        private void SetNormalDiscount()
        {
            try
            {
                if (_discount != null)
                {
                    this.Disc = this.Disc;
                    this.DiscAmount = this.DiscAmount;
                }
                else
                {
                    this.Disc = 0;
                    this.DiscAmount = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderLine " + ex.ToString());
            }
        }

        public Discount UpdateDiscountLine(bool forceChangeEvents)
        {
            Discount _result = null;
            try
            {
                Discount _locDiscount = null;
                XPCollection<DiscountLine> _locDiscountLines = new XPCollection<DiscountLine>(Session,
                                                               new GroupOperator(GroupOperatorType.And,
                                                               new BinaryOperator("SalesOrderLine", this)),
                                                               new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));

                if (_locDiscountLines != null && _locDiscountLines.Count > 0)
                {
                    if (_locDiscountLines.Count == 1)
                    {
                        foreach (DiscountLine _locDiscountLinerLine in _locDiscountLines)
                        {
                            if (forceChangeEvents)
                            {
                                //OnChanged("CurrencyLine", oldCurrencyLine, _locSalesOrderLine.Currency);
                                _result = _locDiscountLinerLine.Discount;
                            }
                        }
                    }
                    else
                    {
                        foreach (DiscountLine _locDiscountLine in _locDiscountLines)
                        {
                            if (_locDiscountLine.No == 1)
                            {
                                _locDiscount = _locDiscountLine.Discount;
                            }
                            else
                            {
                                if (_locDiscountLine.Discount != _locDiscount)
                                {
                                    _globFunc = new GlobalFunction();
                                    //OnChanged("CurrencyLine", oldCurrencyLine, _globFunc.GetDefaultCurrency(Session));
                                    _result = _globFunc.GetDefaultDiscount(Session, DiscountMode.Sales);
                                    break;
                                }
                                else
                                {
                                    _result = _locDiscountLine.Discount;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesOrderLine ", ex.ToString());
            }
            return _result;
        }

        #endregion Discount

        #region Clm

        public int GetTotalSOL()
        {
            int _result = 0;
            try
            {
                XPCollection<SalesOrderLine> _locSalesOrderLines = new XPCollection<SalesOrderLine>(Session, new GroupOperator(GroupOperatorType.And,
                                                                   new BinaryOperator("SalesOrderLine", this)));

                if (_locSalesOrderLines != null && _locSalesOrderLines.Count > 0)
                {
                    _result = _locSalesOrderLines.Count();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesOrderLine", ex.ToString());
            }
            return _result;
        }

        private double GetTotalAmountSOL()
        {
            double _result = 0;
            try
            {
                double _locTotalAmount = 0;
                if (!IsLoading)
                {
                    XPCollection<SalesOrderLine> _locSalesOrderLines = new XPCollection<SalesOrderLine>(Session,
                                                                       new BinaryOperator("SalesOrder", this.SalesOrder));

                    if (_locSalesOrderLines.Count() >= 0)
                    {
                        foreach (SalesOrderLine _locSalesOrderLine in _locSalesOrderLines)
                        {
                            _locTotalAmount = _locTotalAmount + _locSalesOrderLine.TAmount;
                        }
                        _result = _locTotalAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesOrderLine" + ex.ToString());
            }

            return _result;
        }

        public double UpdateTotalSOL(bool forceChangeEvents)
        {
            double _result = 0;
            try
            {
                double _locTotalAmount = 0;
                if (!IsLoading)
                {
                    XPCollection<SalesOrderLine> _locSalesOrderLines = new XPCollection<SalesOrderLine>(Session, new GroupOperator(GroupOperatorType.And,
                                                                       new BinaryOperator("SalesOrderLine", this)));

                    if (_locSalesOrderLines != null && _locSalesOrderLines.Count() > 0)
                    {
                        foreach (SalesOrderLine _locSalesOrderLine in _locSalesOrderLines)
                        {
                            _locTotalAmount = _locTotalAmount + _locSalesOrderLine.TAmountSOL;
                        }
                        _result = _locTotalAmount;
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrderLine " + ex.ToString());
            }
            return _result;
        }

        //For Report
        private double ReportTotalSOL()
        {
            double _result = 0;
            try
            {
                double _locTotalSol = 0;
                if (!IsLoading)
                {
                    XPCollection<SalesOrderLine> _locSalesOrderLines = new XPCollection<SalesOrderLine>(Session,
                                                                       new BinaryOperator("SalesOrder", this.SalesOrder));

                    if (_locSalesOrderLines.Count() >= 0)
                    {
                        foreach (SalesOrderLine _locSalesOrderLine in _locSalesOrderLines)
                        {
                            _locTotalSol = _locTotalSol + _locSalesOrderLine.TUAmount;
                        }
                        _result = _locTotalSol;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesOrderLine" + ex.ToString());
            }

            return _result;
        }

        private double ReportDiscSOL()
        {
            double _result = 0;
            try
            {
                double _locTotalDiscSol = 0;
                if (!IsLoading)
                {
                    XPCollection<SalesOrderLine> _locSalesOrderLines = new XPCollection<SalesOrderLine>(Session,
                                                                             new BinaryOperator("SalesOrder", this.SalesOrder));

                    if (_locSalesOrderLines.Count() >= 0)
                    {
                        foreach (SalesOrderLine _locSalesOrderLine in _locSalesOrderLines)
                        {
                            _locTotalDiscSol = _locTotalDiscSol + _locSalesOrderLine.DiscAmount;
                        }
                        _result = _locTotalDiscSol;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesOrderLine" + ex.ToString());
            }

            return _result;
        }

        private double ReportTaxSOL()
        {
            double _result = 0;
            try
            {
                double _locTotalTaxSol = 0;
                if (!IsLoading)
                {
                    XPCollection<SalesOrderLine> _locSalesOrderLines = new XPCollection<SalesOrderLine>(Session,
                                                                       new BinaryOperator("SalesOrder", this.SalesOrder));

                    if (_locSalesOrderLines.Count() >= 0)
                    {
                        foreach (SalesOrderLine _locSalesOrderLine in _locSalesOrderLines)
                        {
                            _locTotalTaxSol = _locTotalTaxSol + _locSalesOrderLine.TxAmount;
                        }
                        _result = _locTotalTaxSol;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesOrderLine" + ex.ToString());
            }

            return _result;
        }

        #endregion Clm

    }
}