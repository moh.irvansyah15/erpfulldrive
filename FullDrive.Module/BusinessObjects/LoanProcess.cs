﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.ExpressApp.ConditionalAppearance;
using FullDrive.Module.CustomProcess;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Loan")]
    [RuleCombinationOfPropertiesIsUnique("LoanProcessRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class LoanProcess : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private string _name;
        private SalaryComponent _salaryComponent;
        private DateTime _maxPaymentTime;
        private string _processValue;
        private LoanProposal _loanCode;
        private double _sequenceNo;
        private Period _salaryPeriod;
        private GlobalFunction _globFunc;
        private Employee _employee;
        private Currency _currencyCode;
        private CurrencyRate _currencyRate;
        public LoanProcess(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this._globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.LoanProcess);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Association("Period-LoanProcesses")]
        [DataSourceCriteria("Active = True")]
        public Period Period
        {
            get { return _salaryPeriod; }
            set { SetPropertyValue("Period", ref _salaryPeriod, value); }
        }

        [Association("Employee-LoanProcesses")]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        public LoanProposal LoanCode
        {
            get { return _loanCode; }
            set { SetPropertyValue("LoanCode", ref _loanCode, value); }
        }

        public string ProcessValue
        {
            get { return _processValue; }
            set { SetPropertyValue("ProcessValue", ref _processValue, value); }
        }

        public DateTime MaxPaymentTime
        {
            get { return _maxPaymentTime; }
            set { SetPropertyValue("MaxPaymentTime", ref _maxPaymentTime, value); }
        }

        public double SequenceNo
        {
            get { return _sequenceNo; }
            set { SetPropertyValue("SequenceNo", ref _sequenceNo, value); }
        }

        public SalaryComponent SalaryComponent
        {
            get { return _salaryComponent; }
            set { SetPropertyValue("SalaryComponent", ref _salaryComponent, value); }
        }

        [DataSourceCriteria("Active = True")]
        public Currency CurrencyCode
        {
            get { return _currencyCode; }
            set { SetPropertyValue("CurrencyCode", ref _currencyCode, value); }
        }

        [DataSourceCriteria("Active = True")]
        public CurrencyRate CurrencyRate
        {
            get { return _currencyRate; }
            set { SetPropertyValue("CurrencRate", ref _currencyRate, value); }
        }

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}

    }
}