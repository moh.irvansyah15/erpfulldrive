﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class AdditionalDashboard : DashboardData
    {
        #region Default

        private Company _company;
        private Division _division;
        private Department _department;
        private Section _section;

        public AdditionalDashboard(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #endregion Default

        #region Field

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [DataSourceCriteria("Active = true And Company = '@This.Company'"), ImmediatePostData()]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [DataSourceCriteria("Active = true And Division = '@This.Division'"), ImmediatePostData()]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Departement", ref _department, value); }
        }

        [DataSourceCriteria("Active = true And Department = '@This.Department'"), ImmediatePostData()]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        #endregion Field
        
    }
}