﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Title")]
    [NavigationItem("Project Transaction")]
    [RuleCombinationOfPropertiesIsUnique("ProjectLineRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class ProjectLine : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private int _no;
        private string _code;
        private string _title;
        private string _title2;
        private string _title3;
        private Status _status;
        private DateTime _statusDate;
        private string _description;
        private Company _company;
        private ProjectHeader _projectHeader;
        private GlobalFunction _globFunc;
        //clm
        private bool _hideSumTotalItem;
        private bool _hideSumTotalService;

        public ProjectLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ProjectLine);
                DateTime now = DateTime.Now;
                this.Status = CustomProcess.Status.Open;
                this.StatusDate = now;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            RecoveryDeleteNo();
        }

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("ProjectLineNoClose", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ProjectLineCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("ProjectLineTitleLock", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Title
        {
            get { return _title; }
            set { SetPropertyValue("Title", ref _title, value); }
        }

        [Appearance("ProjectLineTitle2Lock", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Title2
        {
            get { return _title2; }
            set { SetPropertyValue("Title2", ref _title2, value); }
        }

        [Appearance("ProjectLineTitle3Lock", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Title3
        {
            get { return _title3; }
            set { SetPropertyValue("Title3", ref _title3, value); }
        }

        [Appearance("ProjectLineStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("ProjectLineStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("ProjectLineDescriptionLock", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Appearance("ProjectLineCompanyLock", Criteria = "ActivationPosting = true", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ProjectLineProjectHeaderClose", Enabled = false)]
        [Association("ProjectHeader-ProjectLines")]
        public ProjectHeader ProjectHeader
        {
            get { return _projectHeader; }
            set
            {
                SetPropertyValue("ProjectHeader", ref _projectHeader, value);
                if (!IsLoading)
                {
                    if (this._projectHeader != null)
                    {
                        this.Company = this._projectHeader.Company;
                    }
                }
            }
        }

        [Association("ProjectLine-ProjectLineItems")]
        public XPCollection<ProjectLineItem> ProjectLineItems
        {
            get { return GetCollection<ProjectLineItem>("ProjectLineItems"); }
        }

        [Association("ProjectLine-ProjectLineItem2s")]
        public XPCollection<ProjectLineItem2> ProjectLineItem2s
        {
            get { return GetCollection<ProjectLineItem2>("ProjectLineItem2s"); }
        }

        [Association("ProjectLine-ProjectLineServices")]
        public XPCollection<ProjectLineService> ProjectLineServices
        {
            get { return GetCollection<ProjectLineService>("ProjectLineServices"); }
        }

        #region CLM
        [Browsable(false)]
        public bool HideSumTotalItem
        {
            get { return _hideSumTotalItem; }
            set { SetPropertyValue("HideSumTotalItem", ref _hideSumTotalItem, value); }
        }

        [Appearance("ProjectLineTotalLI2Enabled", Criteria = "HideSumTotalItem = false", Visibility = ViewItemVisibility.Hide)]
        [Persistent("TotalLI2")]
        public double TotalPLI2
        {
            get
            {
                double _result = 0;
                if (!IsLoading)
                {
                    if (GetTotalPLI2() > 0)
                    {
                        _result = UpdateTotalPLI2(true);
                    }
                }
                return _result;
            }
        }

        [Browsable(false)]
        public bool HideSumTotalService
        {
            get { return _hideSumTotalService; }
            set { SetPropertyValue("HideSumTotalService", ref _hideSumTotalService, value); }
        }

        [Appearance("ProjectLineTotalPLSEnabled", Criteria = "HideSumTotalService = false", Visibility = ViewItemVisibility.Hide)]
        [Persistent("TotalPLS")]
        public double TotalPLS
        {
            get
            {
                double _result = 0;
                if (!IsLoading)
                {
                    if (GetTotalPLS() > 0)
                    {
                        _result = UpdateTotalPLS(true);
                    }
                }
                return _result;
            }
        }

        [Appearance("ProjectLineSumTotalPLI2Enabled", Criteria = "HideSumTotalItem = false", Visibility = ViewItemVisibility.Hide)]
        public double SumTotalPLI2
        {
            get
            {
                if (this.TotalPLI2 > 0)
                {
                    return GetSumTotalPLI2();
                }
                else
                {
                    return 0;
                }
            }
        }

        [Appearance("ProjectLineSumTotalPLSEnabled", Criteria = "HideSumTotalItem = false", Visibility = ViewItemVisibility.Hide)]
        public double SumTotalPLS
        {
            get
            {
                if (this.TotalPLS > 0)
                {
                    return GetSumTotalPLS();
                }
                else
                {
                    return 0;
                }
            }
        }
        #endregion CLM

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}

        //=============================================== Code In Here ===============================================

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.ProjectHeader != null)
                    {
                        object _makRecord = Session.Evaluate<ProjectLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("ProjectHeader=?", this.ProjectHeader));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.ProjectHeader != null)
                {
                    ProjectHeader _numHeader = Session.FindObject<ProjectHeader>
                                                (new BinaryOperator("Code", this.ProjectHeader.Code));

                    XPCollection<ProjectLine> _numLines = new XPCollection<ProjectLine>
                                                (Session, new BinaryOperator("ProjectHeader", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (ProjectLine _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.ProjectHeader != null)
                {
                    ProjectHeader _numHeader = Session.FindObject<ProjectHeader>
                                                (new BinaryOperator("Code", this.ProjectHeader.Code));

                    XPCollection<ProjectLine> _numLines = new XPCollection<ProjectLine>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("ProjectHeader", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (ProjectLine _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectLine " + ex.ToString());
            }
        }

        #endregion No

        #region CLM

        public int GetTotalPLI2()
        {
            int _result = 0;
            try
            {
                XPCollection<ProjectLineItem2> _locProjectLineItem2s = new XPCollection<ProjectLineItem2>(Session, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ProjectLine", this)));

                if (_locProjectLineItem2s != null && _locProjectLineItem2s.Count > 0)
                {
                    _result = _locProjectLineItem2s.Count();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ProjectLine", ex.ToString());
            }
            return _result;
        }

        public double UpdateTotalPLI2(bool forceChangeEvents)
        {
            double _result = 0;
            try
            {
                double _locTotalAmount = 0;
                if (!IsLoading)
                {
                    XPCollection<ProjectLineItem2> _locProjectLineItem2s = new XPCollection<ProjectLineItem2>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("ProjectLine", this)));

                    if (_locProjectLineItem2s != null && _locProjectLineItem2s.Count() > 0)
                    {
                        foreach (ProjectLineItem2 _locProjectLineItem2 in _locProjectLineItem2s)
                        {
                            _locTotalAmount = _locTotalAmount + _locProjectLineItem2.TotalUnitAmount;
                        }
                        _result = _locTotalAmount;
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectLine " + ex.ToString());
            }
            return _result;
        }

        public double GetSumTotalPLI2()
        {
            double _result = 0;
            try
            {
                double _locSumTotalPLI2 = 0;

                if (!IsLoading)
                {
                    if (_projectHeader != null)
                    {
                        XPCollection<ProjectLine> _locProjectLines = new XPCollection<ProjectLine>(Session, new GroupOperator(GroupOperatorType.And,
                                                                 new BinaryOperator("ProjectHeader", this.ProjectHeader)));

                        if (_locProjectLines != null && _locProjectLines.Count > 0)
                        {
                            foreach (ProjectLine _locProjectLine in _locProjectLines)
                            {
                                _locSumTotalPLI2 = _locSumTotalPLI2 + _locProjectLine.TotalPLI2;
                            }
                        }
                        _result = _locSumTotalPLI2;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ProjectLine", ex.ToString());
            }
            return _result;
        }

        //====

        public int GetTotalPLS()
        {
            int _result = 0;
            try
            {
                XPCollection<ProjectLineService> _locProjectLineServices = new XPCollection<ProjectLineService>(Session, new GroupOperator(GroupOperatorType.And,
                                                                           new BinaryOperator("ProjectLine", this)));

                if (_locProjectLineServices != null && _locProjectLineServices.Count > 0)
                {
                    _result = _locProjectLineServices.Count();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ProjectLine", ex.ToString());
            }
            return _result;
        }

        public double UpdateTotalPLS(bool forceChangeEvents)
        {
            double _result = 0;
            try
            {
                double _locTotalAmount = 0;
                if (!IsLoading)
                {
                    XPCollection<ProjectLineService> _locProjectLineServices = new XPCollection<ProjectLineService>(Session, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("ProjectLine", this)));

                    if (_locProjectLineServices != null && _locProjectLineServices.Count() > 0)
                    {
                        foreach (ProjectLineService _locProjectLineService in _locProjectLineServices)
                        {
                            _locTotalAmount = _locTotalAmount + _locProjectLineService.TotalUnitAmount;
                        }
                        _result = _locTotalAmount;
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectLine " + ex.ToString());
            }
            return _result;
        }

        public double GetSumTotalPLS()
        {
            double _result = 0;
            try
            {
                double _locSumTotalPLS = 0;

                if (!IsLoading)
                {
                    if (_projectHeader != null)
                    {
                        XPCollection<ProjectLine> _locProjectLines = new XPCollection<ProjectLine>(Session, new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("ProjectHeader", this.ProjectHeader)));

                        if (_locProjectLines != null && _locProjectLines.Count > 0)
                        {
                            foreach (ProjectLine _locProjectLine in _locProjectLines)
                            {
                                _locSumTotalPLS = _locSumTotalPLS + _locProjectLine.TotalPLS;
                            }
                        }
                        _result = _locSumTotalPLS;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ProjectLine", ex.ToString());
            }
            return _result;
        }

        #endregion CLM

    }
}