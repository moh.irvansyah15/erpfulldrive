﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("CreditMemoLineRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class CreditMemoLine : FullDriveSysBaseObject
    {
        #region Default

        private bool _activationPosting;
        private int _no;
        private string _code;
        private StockType _stockType;
        private Location _location;
        private BinLocation _binLocation;
        private Item _item;
        private string _description;
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasure;
        #region InisialisasiMaxQty
        private double _mxDQTY;
        private UnitOfMeasure _mxDUom;
        private double _mxQty;
        private UnitOfMeasure _mxUom;
        private double _mxTQty;
        #endregion InisialisasiMaxQty
        private double _dQty;
        private UnitOfMeasure _dUom;
        private double _qty;
        private UnitOfMeasure _uom;
        private double _tQty;
        private double _uAmount;
        private double _tUAmount;
        private Status _status;
        private DateTime _statusDate;
        private CreditMemo _creditMemo;
        private InventoryTransferOutLine _inventoryTransferOutLine;
        private GlobalFunction _globFunc;
        //clm
        private bool _hideSumTotalItem;

        public CreditMemoLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.CreditMemoLine);
                this.Status = Status.Open;
                this.StatusDate = now;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #endregion Default

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("CreditMemoLineNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("CreditMemoLineCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("CreditMemoLineStockTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockType StockType
        {
            get { return _stockType; }
            set { SetPropertyValue("StockType", ref _stockType, value); }
        }

        [Appearance("CreditMemoLineLocationClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Location Location
        {
            get { return _location; }
            set { SetPropertyValue("Location", ref _location, value); }
        }

        [Appearance("CreditMemoLineBinLocationClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BinLocation BinLocation
        {
            get { return _binLocation; }
            set { SetPropertyValue("BinLocation", ref _binLocation, value); }
        }

        [ImmediatePostData()]
        [Appearance("CreditMemoLineItemClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Item Item
        {
            get { return _item; }
            set
            {
                SetPropertyValue("Item", ref _item, value);
                if (!IsLoading)
                {
                    if (this._item != null)
                    {
                        this.DUOM = this._item.BasedUOM;
                    }
                }
            }
        }

        [Appearance("CreditMemoLineDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Browsable(false)]
        public XPCollection<UnitOfMeasure> AvailableUnitOfMeasure
        {
            get
            {
                if (Item == null)
                {
                    _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session);
                }
                else
                {
                    XPQuery<UnitOfMeasure> _locQueryUnitOfMeasures = new XPQuery<UnitOfMeasure>(Session);
                    XPQuery<ItemUnitOfMeasure> _locQueryItemUnitOfMeasures = new XPQuery<ItemUnitOfMeasure>(Session);
                    var _results = from _locUOM in _locQueryUnitOfMeasures
                                   join _locIUOM in _locQueryItemUnitOfMeasures on _locUOM.Oid equals _locIUOM.UOM.Oid
                                   where (_locIUOM.Item.Oid == this.Item.Oid)
                                   select _locUOM;

                    _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session, _results.ToList());

                }
                return _availableUnitOfMeasure;

            }
        }

        #region MaxDefaultQty

        [Appearance("CreditMemoLineMxDQtyClose", Enabled = false)]
        [ImmediatePostData()]
        public double MxDQty
        {
            get { return _mxDQTY; }
            set
            {
                SetPropertyValue("MxDQty", ref _mxDQTY, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        [Appearance("CreditMemoLineMxDUOMClose", Enabled = false)]
        public UnitOfMeasure MxDUOM
        {
            get { return _mxDUom; }
            set
            {
                SetPropertyValue("MxDUOM", ref _mxDUom, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("CreditMemoLineMxQtyClose", Enabled = false)]
        public double MxQty
        {
            get { return _mxQty; }
            set
            {
                SetPropertyValue("MxQty", ref _mxQty, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        [Appearance("CreditMemoLineMxUOMClose", Enabled = false)]
        public UnitOfMeasure MxUOM
        {
            get { return _mxUom; }
            set
            {
                SetPropertyValue("MxUOM", ref _mxUom, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [Appearance("CreditMemoLineMxTQtyClose", Enabled = false)]
        public double MxTQty
        {
            get { return _mxTQty; }
            set { SetPropertyValue("MxTQty", ref _mxTQty, value); }
        }

        #endregion MaxDefaultQty

        #region DefaultQty

        [Appearance("CreditMemoLineDQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double DQty
        {
            get { return _dQty; }
            set
            {
                SetPropertyValue("DQty", ref _dQty, value);
                if (!IsLoading)
                {
                    if (this.MxDQty > 0)
                    {
                        if (this._dQty > this.MxDQty)
                        {
                            this._dQty = this.MxDQty;
                        }
                    }
                    SetTotalQty();
                    SetTotalUnitPrice();
                }
            }
        }

        [Appearance("CreditMemoLineDUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM
        {
            get { return _dUom; }
            set
            {
                SetPropertyValue("DUOM", ref _dUom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    SetTotalUnitPrice();
                }
            }
        }

        [Appearance("CreditMemoLineQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double Qty
        {
            get { return _qty; }
            set
            {
                SetPropertyValue("Qty", ref _qty, value);
                if (!IsLoading)
                {
                    if (this._mxQty > 0)
                    {
                        if (this._qty > this.MxQty)
                        {
                            this._qty = this.MxQty;
                        }
                    }
                    SetTotalQty();
                    SetTotalUnitPrice();
                }
            }
        }

        [Appearance("CreditMemoLineUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set
            {
                SetPropertyValue("UOM", ref _uom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    SetTotalUnitPrice();
                }
            }
        }

        [Appearance("CreditMemoLineTotalQtyEnabled", Enabled = false)]
        public double TQty
        {
            get { return _tQty; }
            set { SetPropertyValue("TQty", ref _tQty, value); }
        }

        #endregion DefaultQty

        #region Amount

        [Appearance("CreditMemoLineUAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double UAmount
        {
            get { return _uAmount; }
            set
            {
                SetPropertyValue("UAmount", ref _uAmount, value);
                if (!IsLoading)
                {
                    SetTotalUnitPrice();
                }
            }
        }

        [Appearance("CreditMemoLineTUAmountEnabled", Enabled = false)]
        public double TUAmount
        {
            get { return _tUAmount; }
            set { SetPropertyValue("TUAmount", ref _tUAmount, value); }
        }

        #endregion Price

        [Appearance("CreditMemoLineStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("CreditMemoLineStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("CreditMemoLineCreditMemoEnabled", Enabled = false)]
        [Association("CreditMemo-CreditMemoLines")]
        public CreditMemo CreditMemo
        {
            get { return _creditMemo; }
            set { SetPropertyValue("CreditMemo", ref _creditMemo, value); }
        }

        [Browsable(false)]
        public InventoryTransferOutLine InventoryTransferOutLine
        {
            get { return _inventoryTransferOutLine; }
            set { SetPropertyValue("InventoryTransferOutLine", ref _inventoryTransferOutLine, value); }
        }

        #region Clm

        [Browsable(false)]
        public bool HideSumTotalItem
        {
            get { return _hideSumTotalItem; }
            set { SetPropertyValue("HideSumTotalItem", ref _hideSumTotalItem, value); }
        }

        [Appearance("CreditMemoLineEnabled", Criteria = "HideSumTotalItem = false", Visibility = ViewItemVisibility.Hide)]
        [Persistent("TotalCML")]
        public double TotalCML
        {
            get
            {
                double _result = 0;
                if (!IsLoading)
                {
                    if (GetTotalCML() > 0)
                    {
                        _result = UpdateTotalCML(true);
                    }
                }
                return _result;
            }
        }

        [Browsable(false)]
        [Appearance("CreditMemoLineTAmountCMLEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TAmountCML
        {
            get
            {
                if (this._tUAmount > 0)
                {
                    return GetTotalAmountCML();
                }
                else
                {
                    return 0;
                }
            }
        }

        [Browsable(false)]
        [Appearance("CreditMemoLineTGrandAmountCMLEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TGrandAmountCML
        {
            get
            {
                if (this.TAmountCML > 0)
                {
                    return GetTotalGrandAmountCML();
                }
                else
                {
                    return 0;
                }
            }
        }

        #endregion Clm

        #endregion Field

        //===== Code Only =====

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.CreditMemo != null)
                    {
                        object _makRecord = Session.Evaluate<CreditMemoLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("CreditMemo=?", this.CreditMemo));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemoLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.CreditMemo != null)
                {
                    CreditMemo _numHeader = Session.FindObject<CreditMemo>
                                                (new BinaryOperator("Code", this.CreditMemo.Code));

                    XPCollection<CreditMemoLine> _numLines = new XPCollection<CreditMemoLine>
                                                (Session, new BinaryOperator("CreditMemo", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (CreditMemoLine _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemoLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.CreditMemo != null)
                {
                    CreditMemo _numHeader = Session.FindObject<CreditMemo>
                                                (new BinaryOperator("Code", this.CreditMemo.Code));

                    XPCollection<CreditMemoLine> _numLines = new XPCollection<CreditMemoLine>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("CreditMemo", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (CreditMemoLine _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemoLine " + ex.ToString());
            }
        }

        #endregion No

        #region Set

        private void SetTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.CreditMemo != null)
                {
                    ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                    if (_locItemUOM != null)
                    {
                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.Qty * _locItemUOM.DefaultConversion + this.DQty;
                        }
                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.Qty / _locItemUOM.Conversion + this.DQty;
                        }
                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.Qty + this.DQty;
                        }

                        this.TQty = _locInvLineTotal;
                    }
                    else
                    {
                        _locInvLineTotal = this.Qty + this.DQty;
                        this.TQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemoLine " + ex.ToString());
            }
        }

        private void SetMaxTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.CreditMemo != null)
                {
                    ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.MxUOM),
                                                         new BinaryOperator("DefaultUOM", this.MxDUOM),
                                                         new BinaryOperator("Active", true)));
                    if (_locItemUOM != null)
                    {
                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.MxQty * _locItemUOM.DefaultConversion + this.MxDQty;
                        }
                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.MxQty / _locItemUOM.Conversion + this.MxDQty;
                        }
                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.MxQty + this.MxDQty;
                        }

                        this.MxTQty = _locInvLineTotal;
                    }
                    else
                    {
                        _locInvLineTotal = this.MxQty + this.MxDQty;
                        this.MxTQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemoLine " + ex.ToString());
            }
        }

        private void SetTotalUnitPrice()
        {
            try
            {
                if (_tQty >= 0 & _uAmount >= 0)
                {
                    this.TUAmount = this.TQty * this.UAmount;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemoLine " + ex.ToString());
            }
        }

        #endregion Set

        #region Clm

        private double GetTotalAmountCML()
        {
            double _result = 0;
            try
            {
                double _locTotalTaxCml = 0;
                if (!IsLoading)
                {
                    XPCollection<CreditMemoLine> _locCreditMemoLines = new XPCollection<CreditMemoLine>(Session,
                                                                       new BinaryOperator("CreditMemo", this.CreditMemo));

                    if (_locCreditMemoLines.Count() >= 0)
                    {
                        foreach (CreditMemoLine _locCreditMemoLine in _locCreditMemoLines)
                        {
                            _locTotalTaxCml = _locTotalTaxCml + _locCreditMemoLine.TUAmount;
                        }
                        _result = _locTotalTaxCml;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CreditMemoLine" + ex.ToString());
            }

            return _result;
        }

        //grand
        public int GetTotalCML()
        {
            int _result = 0;
            try
            {
                XPCollection<CreditMemoLine> _locCreditMemoLines = new XPCollection<CreditMemoLine>(Session, new GroupOperator(GroupOperatorType.And,
                                                                   new BinaryOperator("CreditMemoLine", this)));

                if (_locCreditMemoLines != null && _locCreditMemoLines.Count > 0)
                {
                    _result = _locCreditMemoLines.Count();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CreditMemoLine", ex.ToString());
            }
            return _result;
        }

        public double UpdateTotalCML(bool forceChangeEvents)
        {
            double _result = 0;
            try
            {
                double _locTotalAmount = 0;
                if (!IsLoading)
                {
                    XPCollection<CreditMemoLine> _locCreditMemoLines = new XPCollection<CreditMemoLine>(Session, new GroupOperator(GroupOperatorType.And,
                                                                       new BinaryOperator("CreditMemoLine", this)));

                    if (_locCreditMemoLines != null && _locCreditMemoLines.Count() > 0)
                    {
                        foreach (CreditMemoLine _locCreditMemoLine in _locCreditMemoLines)
                        {
                            _locTotalAmount = _locTotalAmount + _locCreditMemoLine.TAmountCML;
                        }
                        _result = _locTotalAmount;
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectLine " + ex.ToString());
            }
            return _result;
        }

        private double GetTotalGrandAmountCML()
        {
            double _result = 0;
            try
            {
                double _locTGrandAmountCML = 0;

                if (!IsLoading)
                {
                    if (_creditMemo != null)
                    {
                        XPCollection<CreditMemoLine> _locCreditMemoLines = new XPCollection<CreditMemoLine>(Session, new GroupOperator(GroupOperatorType.And,
                                                                           new BinaryOperator("CreditMemo", this.CreditMemo)));

                        if (_locCreditMemoLines != null && _locCreditMemoLines.Count > 0)
                        {
                            foreach (CreditMemoLine _locCreditMemoLine in _locCreditMemoLines)
                            {
                                _locTGrandAmountCML = _locCreditMemoLine.TAmountCML;
                            }
                        }
                        _result = _locTGrandAmountCML;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CreditMemo", ex.ToString());
            }
            return _result;

        }

        #endregion Clm
    }
}