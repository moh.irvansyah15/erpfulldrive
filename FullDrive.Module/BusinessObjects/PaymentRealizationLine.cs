﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.ConditionalAppearance;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using System.IO;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("PaymentRealizationLineRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class PaymentRealizationLine : BaseObject
    {
        #region Default

        private bool _activationPosting;
        private string _code;
        private string _name;
        private PaymentRealization _paymentRealization;
        private CashAdvance _cashAdvance;
        private Employee _employee;
        private FileData _attachmentRealization;
        private double _realizationAmount;
        private DateTime _uploadDate;
        private string _description;
        private Status _status;
        private DateTime _statusDate;
        private int _processCount;
        private GlobalFunction _globFunc;
        //private bool _openCompany;
        private Company _company;
        //private bool _openDepartment;
        private Department _department;

        public PaymentRealizationLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).

            this._globFunc = new GlobalFunction();
            this._code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PaymentRealizationLine);
            this._uploadDate = DateTime.Now;
            this._status = Status.Open;
            this._statusDate = DateTime.Now;
        }

        #endregion Default

        #region Field 

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("PaymentRealizationLineCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("PaymentRealizationLineNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Association("PaymentRealization-PaymentRealizationLines")]
        [Appearance("PaymentRealizationLinePaymentRealizationClose", Enabled = false)]
        public PaymentRealization PaymentRealization
        {
            get { return _paymentRealization; }
            set
            {
                SetPropertyValue("PaymentRealization", ref _paymentRealization, value);
                if (this.PaymentRealization != null)
                {
                    this._employee = this._paymentRealization.Employee;
                    this._department = this._paymentRealization.Department;
                    this._company = this._paymentRealization.Company;
                    this._cashAdvance = this._paymentRealization.CashAdvance;
                }
            }
        }

        [Appearance("PaymentRealizationLineCashAdvanceClose", Enabled = false)]
        public CashAdvance CashAdvance
        {
            get { return _cashAdvance; }
            set { SetPropertyValue("CashAdvance", ref _cashAdvance, value); }
        }

        [Appearance("PaymentRealizationLineEmployeeClose", Enabled = false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        [Appearance("PaymentRealizationLineAttachmentRealizationClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public FileData AttachmentRealization
        {
            get { return _attachmentRealization; }
            set { SetPropertyValue("AttachmentRealization", ref _attachmentRealization, value); }
        }

        [ImmediatePostData()]
        [Appearance("PaymentRealizationLineRealizationAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double RealizationAmount
        {
            get { return _realizationAmount; }
            set
            {
                SetPropertyValue("RealizationAmount", ref _realizationAmount, value);

                //clm
                if (!IsLoading)
                {
                    double _result = this._realizationAmount;

                    if (this._paymentRealization.TotalRemainingAmount <= 0 && this._paymentRealization.TotalAmount > 0)
                    {
                        if (this._realizationAmount > this._paymentRealization.TotalAmount)
                        {
                            this._realizationAmount = 0;
                        }
                        else if (this._realizationAmount <= this._paymentRealization.TotalAmount)
                        {
                            this._realizationAmount = _result;
                        }
                        else
                        {
                            this._realizationAmount = 0;
                        }
                    }
                    else if (this._paymentRealization.TotalRemainingAmount >= 0)
                    {
                        if (this._realizationAmount > this._paymentRealization.TotalRemainingAmount)
                        {
                            this._realizationAmount = 0;
                        }
                        else if (this._realizationAmount <= this._paymentRealization.TotalRemainingAmount)
                        {
                            this._realizationAmount = _result;
                        }
                        else
                        {
                            this._realizationAmount = 0;
                        }
                    }
                    else
                    {
                        this._realizationAmount = 0;
                    }
                }
            }
        }

        [Appearance("PaymentRealizationLineUploadDateClose", Enabled = false)]
        public DateTime UploadDate
        {
            get { return _uploadDate; }
            set { SetPropertyValue("UploadDate", ref _uploadDate, value); }
        }

        [Size(512)]
        [Appearance("PaymentRealizationLineNotesClose", Criteria = "ActivationPosting = True", Enabled = false)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Appearance("PaymentRealizationLineStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("PaymentRealizationLineStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("PaymentRealizationLineProcessCountEnabled", Enabled = false)]
        public int ProcessCount
        {
            get { return _processCount; }
            set { SetPropertyValue("ProcessCount", ref _processCount, value); }
        }

        //clm
        //[ImmediatePostData]
        //[Browsable(false)]
        //public bool OpenCompany
        //{
        //    get { return _openCompany; }
        //    set { SetPropertyValue("OpenCompany", ref _openCompany, value); }
        //}

        //[Appearance("CashAdvanceLineCompanyClose", Criteria = "OpenCompany = false", Enabled = false)]
        //[ImmediatePostData()]
        //[DataSourceCriteria("Active = true")]

        [Browsable(false)]
        [Appearance("PaymentRealizationLineCompanyEnabled", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        //[ImmediatePostData]
        //[Browsable(false)]
        //public bool OpenDepartment
        //{
        //    get { return _openDepartment; }
        //    set { SetPropertyValue("OpenDepartment", ref _openDepartment, value); }
        //}

        //[Appearance("CashAdvanceLineDepartmentClose", Criteria = "OpenDepartment = false", Enabled = false)]
        //[ImmediatePostData()]
        //[DataSourceCriteria("Active = true")]

        [Browsable(false)]
        [Appearance("PaymentRealizationLineDepartmentEnabled", Enabled = false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        #endregion Field

        //==== Code Only ====

        #region UpdateName

        private void UpdateName()
        {
            try
            {
                this._name = string.Format("PaymentRealizationLine [ {0} ]", Employee.Name);
            }

            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PaymentRealizationLine" + ex.ToString());
            }
        }

        #endregion UpdateName

    }
}