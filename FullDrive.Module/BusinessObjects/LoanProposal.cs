﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.ExpressApp.ConditionalAppearance;
using FullDrive.Module.CustomProcess;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Loan")]
    [RuleCombinationOfPropertiesIsUnique("LoanProposalRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class LoanProposal : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private string _name;
        private Employee _employee;
        private LoanType _loantype;
        private double _loanValue;
        private LoanStatus _loanStatus;
        private DateTime _paymentTime;
        private double _monthlyDeduction;
        private DateTime _documentDate;
        private Currency _currencyCode;
        private CurrencyRate _currencyRate;
        private GlobalFunction _globFunc;
        
        public LoanProposal(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this._globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.LoanProposal);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Association("LoanType-LoanProposals")]
        [DataSourceCriteria("Active = True")]
        public LoanType LoanType
        {
            get { return _loantype; }
            set { SetPropertyValue("LoanType", ref _loantype, value); }
        }

        public DateTime DocumentDate
        {
            get { return _documentDate; }
            set { SetPropertyValue("DocumentDate", ref _documentDate, value); }
        }

        public double LoanValue
        {
            get { return _loanValue; }
            set { SetPropertyValue("LoanValue", ref _loanValue, value); }
        }

        public double MonthlyDeduction
        {
            get { return _monthlyDeduction; }
            set { SetPropertyValue("MonthlyDeduction", ref _monthlyDeduction, value); }
        }

        public DateTime PaymentTime
        {
            get { return _paymentTime; }
            set { SetPropertyValue("PaymentTime", ref _paymentTime, value); }
        }

        [Association("LoanStatus-LoanProposals")]
        [DataSourceCriteria("Active = True")]
        public LoanStatus LoanStatus
        {
            get { return _loanStatus; }
            set { SetPropertyValue("LoanStatus", ref _loanStatus, value); }
        }

        [Association("Employee-LoanProposals")]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        public Currency CurrencyCode
        {
            get { return _currencyCode; }
            set { SetPropertyValue("CurrencyCode", ref _currencyCode, value); }
        }

        public CurrencyRate CurrencyRate
        {
            get { return _currencyRate; }
            set { SetPropertyValue("CurrencyRate", ref _currencyRate, value); }
        }

 
        
        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}
    }
}