﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Cost Of Goods Sold")]
    [RuleCombinationOfPropertiesIsUnique("MaterialPriceRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class MaterialPrice : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private int _no;
        private string _code;
        private Item _item;
        private double _qty;
        private UnitOfMeasure _uom;
        private double _diameter;
        private string _fasa;
        private string _netral;
        private Conductor _conductor;
        private double _conductorWeight;
        private double _conductorPrice;
        private Insulation _insulation;
        private double _insulationWeight;
        private double _insulationPrice;
        private Filler _filler;
        private double _fillerWeight;
        private double _fillerPrice;
        private Screening _screening;
        private double _screeningWeight;
        private double _screeningPrice;
        private Armour _armour;
        private double _armourWeight;
        private double _armourPrice;
        private OuterSheath _outerSheath;
        private double _outerSheathWeight;
        private double _outerSheathPrice;
        private double _totalWeight;
        private double _totalMaterialPrice;
        private GlobalFunction _globFunc;

        public MaterialPrice(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if(!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.MaterialPrice);
                this.Qty = 1000;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            RecoveryDeleteNo();
        }

        [Appearance("MaterialPriceNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Association("Item-MaterialPrices")]
        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        public Item Item
        {
            get { return _item; }
            set
            {
                SetPropertyValue("Item", ref _item, value);
                if (!IsLoading)
                {
                    if(this._item != null)
                    {
                        this.UOM = _item.BasedUOM;
                    }
                }
            }
        }

        public double Qty
        {
            get { return _qty; }
            set { SetPropertyValue("Qty", ref _qty, value); }
        }

        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set { SetPropertyValue("UOM", ref _uom, value); }
        }

        public double Diameter
        {
            get { return _diameter; }
            set { SetPropertyValue("Diameter", ref _diameter, value); }
        }

        public string Fasa
        {
            get { return _fasa; }
            set { SetPropertyValue("Fasa", ref _fasa, value); }
        }

        public string Netral
        {
            get { return _netral; }
            set { SetPropertyValue("Netral", ref _netral, value); }
        }

        [DataSourceCriteria("Active = true")]
        public Conductor Conductor
        {
            get { return _conductor; }
            set { SetPropertyValue("Conductor", ref _conductor, value); }
        }

        [ImmediatePostData()]
        public double ConductorWeight
        {
            get { return _conductorWeight; }
            set
            {
                SetPropertyValue("ConductorWeight", ref _conductorWeight, value);
                if (!IsLoading)
                {
                    if (this._conductorWeight > 0)
                    {
                        SetTotalWeight();                        
                    }
                }
            }
        }

        [ImmediatePostData()]
        public double ConductorPrice
        {
            get { return _conductorPrice; }
            set
            {
                SetPropertyValue("ConductorPrice", ref _conductorPrice, value);
                if (!IsLoading)
                {
                    if (this._conductorPrice > 0)
                    {
                        SetTotalMaterialPrice();
                    }
                }
            }
        }

        [DataSourceCriteria("Active = true")]
        public Insulation Insulation
        {
            get { return _insulation; }
            set { SetPropertyValue("Insulation", ref _insulation, value); }
        }

        [ImmediatePostData()]
        public double InsulationWeight
        {
            get { return _insulationWeight; }
            set
            {
                SetPropertyValue("InsulationWeight", ref _insulationWeight, value);
                if (!IsLoading)
                {
                    if (this._insulationWeight > 0)
                    {
                        SetTotalWeight();
                    }
                }
            }
        }

        [ImmediatePostData()]
        public double InsulationPrice
        {
            get { return _insulationPrice; }
            set
            {
                SetPropertyValue("InsulationPrice", ref _insulationPrice, value);
                if (!IsLoading)
                {
                    if (this._insulationPrice > 0)
                    {
                        SetTotalMaterialPrice();
                    }
                }
            }
        }

        [DataSourceCriteria("Active = true")]
        public Filler Filler
        {
            get { return _filler; }
            set { SetPropertyValue("Filler", ref _filler, value); }
        }

        [ImmediatePostData()]
        public double FillerWeight
        {
            get { return _fillerWeight; }
            set
            {
                SetPropertyValue("FillerWeight", ref _fillerWeight, value);
                if (!IsLoading)
                {
                    if (this._fillerWeight > 0)
                    {
                        SetTotalWeight();
                    }
                }
            }
        }

        [ImmediatePostData()]
        public double FillerPrice
        {
            get { return _fillerPrice; }
            set
            {
                SetPropertyValue("FillerPrice", ref _fillerPrice, value);
                if (!IsLoading)
                {
                    if (this._fillerPrice > 0)
                    {
                        SetTotalMaterialPrice();
                    }
                }
            }
        }

        [DataSourceCriteria("Active = true")]
        public Screening Screening
        {
            get { return _screening; }
            set { SetPropertyValue("Screening", ref _screening, value); }
        }

        [ImmediatePostData()]
        public double ScreeningWeight
        {
            get { return _screeningWeight; }
            set
            {
                SetPropertyValue("ScreeningWeight", ref _screeningWeight, value);
                if (!IsLoading)
                {
                    if (this._screeningWeight > 0)
                    {
                        SetTotalWeight();
                    }
                }
            }
        }

        [ImmediatePostData()]
        public double ScreeningPrice
        {
            get { return _screeningPrice; }
            set
            {
                SetPropertyValue("ScreeningPrice", ref _screeningPrice, value);
                if (!IsLoading)
                {
                    if (this._screeningPrice > 0)
                    {
                        SetTotalMaterialPrice();
                    }
                }
            }
        }

        [DataSourceCriteria("Active = true")]
        public Armour Armour
        {
            get { return _armour; }
            set { SetPropertyValue("Armour", ref _armour, value); }
        }

        [ImmediatePostData()]
        public double ArmourWeight
        {
            get { return _armourWeight; }
            set
            {
                SetPropertyValue("ArmourWeight", ref _armourWeight, value);
                if (!IsLoading)
                {
                    if (this._armourWeight > 0)
                    {
                        SetTotalWeight();
                        SetTotalMaterialPrice();
                    }
                }
            }
        }

        [ImmediatePostData()]
        public double ArmourPrice
        {
            get { return _armourPrice; }
            set
            {
                SetPropertyValue("ArmourPrice", ref _armourPrice, value);
                if (!IsLoading)
                {
                    if (this._armourPrice > 0)
                    {
                        SetTotalMaterialPrice();
                    }
                }
            }
        }

        [DataSourceCriteria("Active = true")]
        public OuterSheath OuterSheath
        {
            get { return _outerSheath; }
            set { SetPropertyValue("OuterSheath", ref _outerSheath, value); }
        }

        [ImmediatePostData()]
        public double OuterSheathWeight
        {
            get { return _outerSheathWeight; }
            set
            {
                SetPropertyValue("OuterSheathWeight", ref _outerSheathWeight, value);
                if (!IsLoading)
                {
                    if (this._outerSheathWeight > 0)
                    {
                        SetTotalWeight();
                        SetTotalMaterialPrice();
                    }
                }
            }
        }

        [ImmediatePostData()]
        public double OuterSheathPrice
        {
            get { return _outerSheathPrice; }
            set
            {
                SetPropertyValue("OuterSheathPrice", ref _outerSheathPrice, value);
                if (!IsLoading)
                {
                    if (this._outerSheathPrice > 0)
                    {
                        SetTotalMaterialPrice();
                    }
                }
            }
        }

        public double TotalWeight
        {
            get { return _totalWeight; }
            set { SetPropertyValue("TotalWeight", ref _totalWeight, value); }
        }

        public double TotalMaterialPrice
        {
            get { return _totalMaterialPrice; }
            set { SetPropertyValue("TotalMaterialPrice", ref _totalMaterialPrice, value); }
        }

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}

        //=============================================== Code In Here ===============================================

        #region Numbering
        public void UpdateNo()
        {
            try
            {
                if (!IsLoading)
                {
                    if (!(Session is NestedUnitOfWork) && Session.IsNewObject(this))
                    {
                        if (this.Item != null)
                        {
                            object _makRecord = Session.Evaluate<MaterialPrice>(CriteriaOperator.Parse("Max(No)"),
                                                CriteriaOperator.Parse("Item=?", this.Item));
                            this.No = Convert.ToInt32(_makRecord) + 1;
                            this.Save();
                            RecoveryUpdateNo();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = MaterialPrice " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.Item != null)
                {
                    Item _numHeader = Session.FindObject<Item>
                                                (new BinaryOperator("Code", this.Item.Code));

                    XPCollection<MaterialPrice> _numLines = new XPCollection<MaterialPrice>
                                                (Session, new BinaryOperator("Item", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (MaterialPrice _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                        //Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = MaterialPrice " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.Item != null)
                {
                    Item _numHeader = Session.FindObject<Item>
                                                (new BinaryOperator("Code", this.Item.Code));

                    XPCollection<MaterialPrice> _numLines = new XPCollection<MaterialPrice>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("Item", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (MaterialPrice _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                        //Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = MaterialPrice " + ex.ToString());
            }
        }
        #endregion Numbering

        private void SetTotalWeight()
        {
            try
            {
                this._totalWeight = (this._conductorWeight + this._insulationWeight + this._fillerWeight + this._screeningWeight + this._armourWeight + this.OuterSheathWeight);
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = MaterialPrice " + ex.ToString());
            }
        }

        private void SetTotalMaterialPrice()
        {
            try
            {
                this._totalMaterialPrice = (this._conductorPrice * this._conductorWeight) + (this._insulationPrice * this._insulationWeight) +
                                           (this._fillerPrice * this._fillerWeight) + (this._screeningPrice * this._screeningWeight) +
                                           (this._armourPrice * this._armourWeight) + (this._outerSheathPrice * this._outerSheathWeight);
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = MaterialPrice " + ex.ToString());
            }
        }


    }
}