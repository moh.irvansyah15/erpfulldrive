﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("FullName")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("BankAccountRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class BankAccount : FullDriveSysBaseObject
    {
        #region Default

        private string _code;
        private string _name;
        private string _fullName;
        private string _address;
        private Country _country;
        private City _city;
        private string _accountNo;
        private BankAccountGroup _bankAccountGroup;
        private BusinessPartner _businessPartner;
        private string _accountName;
        private Company _company;
        private bool _default;
        private bool _active;
        private GlobalFunction _globFunc;

        public BankAccount(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.BankAccount);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #endregion Default

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [ImmediatePostData]
        public string Name
        {
            get { return _name; }
            set
            {
                SetPropertyValue("Name", ref _name, value);
                if (!IsLoading)
                {
                    if (this._name != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        [Appearance("BankAccountFullNameEnabled", Enabled = false)]
        public string FullName
        {
            get { return _fullName; }
            set { SetPropertyValue("FullName", ref _fullName, value); }
        }

        [Size(512)]
        public string Address
        {
            get { return _address; }
            set { SetPropertyValue("Address", ref _address, value); }
        }

        [DataSourceCriteria("Active = true")]
        public Country Country
        {
            get { return _country; }
            set { SetPropertyValue("Country", ref _country, value); }
        }

        [DataSourceCriteria("Active = true")]
        public City City
        {
            get { return _city; }
            set { SetPropertyValue("City", ref _city, value); }
        }

        [ImmediatePostData()]
        public string AccountNo
        {
            get { return _accountNo; }
            set
            {
                SetPropertyValue("AccountNo", ref _accountNo, value);
                if (!IsLoading)
                {
                    if (this._accountNo != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        [Association("BankAccountGroup-BankAccounts")]
        [DataSourceCriteria("Active = true")]
        public BankAccountGroup BankAccountGroup
        {
            get { return _bankAccountGroup; }
            set { SetPropertyValue("BankAccountGroup", ref _bankAccountGroup, value); }
        }

        [Association("BusinessPartner-BankAccounts")]
        [DataSourceCriteria("Active = true")]
        public BusinessPartner BusinessPartner
        {
            get { return _businessPartner; }
            set { SetPropertyValue("BusinessPartner", ref _businessPartner, value); }
        }

        [Association("Company-BankAccounts")]
        [DataSourceCriteria("Active = true")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        public string AccountName
        {
            get { return _accountName; }
            set { SetPropertyValue("AccountName", ref _accountName, value); }
        }

        public bool Default
        {
            get { return _default; }
            set { SetPropertyValue("Default", ref _default, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        #endregion Field

        //=== Coce Only ===

        private void SetFullName()
        {
            try
            {
                if (this._name != null && this._accountNo != null)
                {
                    this.FullName = String.Format("{0} ({1})", this.Name, this.AccountNo);
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = BankAccount ", ex.ToString());
            }
        }


    }
}