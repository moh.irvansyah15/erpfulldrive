﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Master")]
    [RuleCombinationOfPropertiesIsUnique("FormulaDesignSheetLineRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class FormulaDesignSheetLine : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private string _name;
        //private CoreNumber _coreNumber;
        //private CrossSectionArea _crossSectionArea;
        private ShapeOfConductor _shapeOfConductor;
        private WireNumber _wireNumber;
        private string _wireLine;
        private double _wireFactor;
        private bool _active;
        private GlobalFunction _globFunc;

        public FormulaDesignSheetLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if(!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.FormulaDesignSheetLine);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        [Appearance("FormulaDesignSheetLineCodeEnabled", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        //[DataSourceCriteria("Active = true")]
        //public CoreNumber CoreNumber
        //{
        //    get { return _coreNumber; }
        //    set { SetPropertyValue("CoreNumber", ref _coreNumber, value); }
        //}

        //[DataSourceCriteria("Active = true")]
        //public CrossSectionArea CrossSectionArea
        //{
        //    get { return _crossSectionArea; }
        //    set { SetPropertyValue("CrossSectionArea", ref _crossSectionArea, value); }
        //}

        [DataSourceCriteria("Active = true")]
        public ShapeOfConductor ShapeOfConductor
        {
            get { return _shapeOfConductor; }
            set { SetPropertyValue("ShapeOfConductor", ref _shapeOfConductor, value); }
        }

        [DataSourceCriteria("Active = true")]
        public WireNumber WireNumber
        {
            get { return _wireNumber; }
            set { SetPropertyValue("WireNumber", ref _wireNumber, value); }
        }

        public string WireLine
        {
            get { return _wireLine; }
            set { SetPropertyValue("WireLine", ref _wireLine, value); }
        }

        public double WireFactor
        {
            get { return _wireFactor; }
            set { SetPropertyValue("WireFactor", ref _wireFactor, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}
    }
}