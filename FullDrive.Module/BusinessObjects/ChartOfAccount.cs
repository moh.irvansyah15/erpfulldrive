﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("ChartOfAccountRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class ChartOfAccount : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private string _name;
        private AccountGroup _accountGroup;
        private FinancialStatment _financialStatment;
        private AccountType _accountType;
        private ChartOfAccount _totalingStart;
        private ChartOfAccount _totalingEnd;
        private BalanceType _balanceType;
        private double _balance;
        private AccountCharge _accountCharge;
        private bool _debitEnabled;
        private double _debit;
        private bool _creditEnabled;
        private double _credit;
        private Company _company;
        private bool _active;
        private GlobalFunction _globFunc;

        public ChartOfAccount(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ChartOfAccount);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        public AccountGroup AccountGroup
        {
            get { return _accountGroup; }
            set { SetPropertyValue("AccountGroup", ref _accountGroup, value); }
        }

        public FinancialStatment FinancialStatment
        {
            get { return _financialStatment; }
            set { SetPropertyValue("FinancialStatment", ref _financialStatment, value); }
        }

        public AccountType AccountType
        {
            get { return _accountType; }
            set { SetPropertyValue("AccountType", ref _accountType, value); }
        }

        [DataSourceCriteria("Active = true")]
        public ChartOfAccount TotallingStart
        {
            get { return _totalingStart; }
            set { SetPropertyValue("TotallingStart", ref _totalingStart, value); }
        }

        [DataSourceCriteria("Active = true")]
        public ChartOfAccount TotallingEnd
        {
            get { return _totalingEnd; }
            set { SetPropertyValue("TotallingEnd", ref _totalingEnd, value); }
        }

        public BalanceType BalanceType
        {
            get { return _balanceType; }
            set { SetPropertyValue("BalanceType", ref _balanceType, value); }
        }

        public double Balance
        {
            get { return _balance; }
            set { SetPropertyValue("Balance", ref _balance, value); }
        }

        [ImmediatePostData()]
        public AccountCharge AccountCharge
        {
            get { return _accountCharge; }
            set
            {
                SetPropertyValue("AccountCharge", ref _accountCharge, value);
                if (!IsLoading)
                {
                    if (this._accountCharge == AccountCharge.Debit)
                    {
                        this.DebitEnabled = true;
                        this.CreditEnabled = false;
                    }
                    else if (this._accountCharge == AccountCharge.Credit)
                    {
                        this.DebitEnabled = false;
                        this.CreditEnabled = true;
                    }
                    else if (this._accountCharge == AccountCharge.Both)
                    {
                        this.DebitEnabled = true;
                        this.CreditEnabled = true;
                    }
                    else
                    {
                        this.DebitEnabled = false;
                        this.CreditEnabled = false;
                    }
                }
            }
        }

        [Browsable(false)]
        public bool DebitEnabled
        {
            get { return _debitEnabled; }
            set { SetPropertyValue("DebitEnabled", ref _debitEnabled, value); }
        }

        [Appearance("ChartOfAccountDebitEnabled", Criteria = "DebitEnabled = false", Enabled = false)]
        public double Debit
        {
            get { return _debit; }
            set { SetPropertyValue("Debit", ref _debit, value); }
        }

        [Browsable(false)]
        public bool CreditEnabled
        {
            get { return _creditEnabled; }
            set { SetPropertyValue("CreditEnabled", ref _creditEnabled, value); }
        }

        [Appearance("ChartOfAccountCreditEnabled", Criteria = "CreditEnabled = false", Enabled = false)]
        public double Credit
        {
            get { return _credit; }
            set { SetPropertyValue("Credit", ref _credit, value); }
        }

        [DataSourceCriteria("Active = true")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}

        //=============================================== Code In Here ===============================================

    }
}