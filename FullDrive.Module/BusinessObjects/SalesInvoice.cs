﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("SalesInvoiceRuleUnique", DefaultContexts.Save, "Code")]
    //[DeferredDeletion]
    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class SalesInvoice : FullDriveSysBaseObject
    {
        #region Default

        private bool _activationPosting;
        private string _code;
        private ObjectList _objectList;
        XPCollection<NumberingLine> _availableNumberingLine;
        private NumberingLine _numbering;
        #region Sales
        private BusinessPartner _salesToCostumer;
        private string _salesToContact;
        private Country _salesToCountry;
        private City _salesToCity;
        private string _salesToAddress;
        #endregion Sales
        #region Bill
        private BusinessPartner _billToCostumer;
        private string _billToContact;
        private Country _billToCountry;
        private City _billToCity;
        private string _billToAddress;
        #endregion Bill
        private PriceGroup _priceGroup;
        private PaymentMethod _paymentMethod;
        private PaymentMethodType _paymentMethodType;
        private PaymentType _paymentType;
        private TermOfPayment _top;
        private Currency _currency;
        #region Bank
        private XPCollection<BankAccount> _availableBankAccountCompany;
        private BankAccount _bankAccountCompany;
        private string _companyAccountNo;
        private string _companyAccountName;
        private XPCollection<BankAccount> _availableBankAccounts;
        private BankAccount _bankAccount;
        private string _accountNo;
        private string _accountName;
        #endregion Bank
        private string _taxNo;
        private double _maxBill;
        private double _bill;
        private double _plan;
        private double _outstanding;
        private string _description;
        private int _postedCount;
        private Status _status;
        private Company _company;
        private DateTime _statusDate;
        private DateTime _estimatedDate;
        private string _signCode;
        private string _userAccess;
        private ProjectHeader _projectHeader;
        private GlobalFunction _globFunc;
        //clm
        private double _dm_amount;
        private bool _activeApproved1;
        private bool _activeApproved2;
        private bool _activeApproved3;

        public SalesInvoice(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.SalesInvoice);
                DateTime now = DateTime.Now;
                this.Status = CustomProcess.Status.Open;
                this.StatusDate = now;
                this.ObjectList = CustomProcess.ObjectList.SalesInvoice;

                _userAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                if (Session.IsNewObject(this))
                {
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null)
                            {
                                Company = _locUserAccess.Employee.Company;
                            }
                        }
                    }
                }

                this.Currency = _globFunc.GetDefaultCurrency(this.Session);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #endregion Default

        #region Field

        //[Browsable(false)]
        [ImmediatePostData()]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("SalesOrderRedColor", Criteria = "ActiveApproved1 = true", BackColor = "#fd79a8")]
        [Appearance("SalesOrderYellowColor", Criteria = "ActiveApproved2 = true", BackColor = "#ffeaa7")]
        [Appearance("SalesOrderGreenOColor", Criteria = "ActiveApproved3 = true", BackColor = "#81ecec")]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("SalesInvoiceCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Browsable(false)]
        public ObjectList ObjectList
        {
            get { return _objectList; }
            set { SetPropertyValue("ObjectList", ref _objectList, value); }
        }

        [Browsable(false)]
        public XPCollection<NumberingLine> AvailableNumberingLine
        {
            get
            {
                if (ObjectList == ObjectList.None)
                {
                    _availableNumberingLine = new XPCollection<NumberingLine>(Session);
                }
                else
                {
                    _availableNumberingLine = new XPCollection<NumberingLine>(Session,
                                              new GroupOperator(GroupOperatorType.And,
                                              new BinaryOperator("ObjectList", this.ObjectList),
                                              new BinaryOperator("Selection", true),
                                              new BinaryOperator("Active", true)));
                }
                return _availableNumberingLine;

            }
        }

        [NonPersistent()]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableNumberingLine", DataSourcePropertyIsNullMode.SelectAll)]
        public NumberingLine Numbering
        {
            get { return _numbering; }
            set
            {
                NumberingLine _oldNumbering = _numbering;
                SetPropertyValue("Numbering", ref _numbering, value);
                if (!IsLoading)
                {
                    if (_numbering != null)
                    {
                        if (this.Session != null)
                        {
                            if (this.Session.DataLayer != null)
                            {
                                this.Code = LocGetNumberingSelectionUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.SalesInvoice, _numbering);
                            }
                        }

                    }
                }
            }
        }

        #region SalesToCostumer

        [ImmediatePostData]
        [DataSourceCriteria("Active = true")]
        [Appearance("SalesInvoiceSalesToCostumerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner SalesToCostumer
        {
            get { return _salesToCostumer; }
            set {
                SetPropertyValue("SalesToCostumer", ref _salesToCostumer, value);
                if(!IsLoading)
                {
                    if(_salesToCostumer != null)
                    {
                        this.SalesToContact = _salesToCostumer.Contact;
                        this.SalesToCountry = _salesToCostumer.Country;
                        this.SalesToCity = _salesToCostumer.City;
                        this.SalesToAddress = this.SalesToAddress;
                        this.BillToCostumer = _salesToCostumer;
                    }
                }
            }
        }

        [Appearance("SalesInvoiceSalesToContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string SalesToContact
        {
            get { return _salesToContact; }
            set { SetPropertyValue("SalesToContact", ref _salesToContact, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("SalesInvoiceSalesCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country SalesToCountry
        {
            get { return _salesToCountry; }
            set { SetPropertyValue("SalesToCountry", ref _salesToCountry, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("SalesInvoiceSalesToCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City SalesToCity
        {
            get { return _salesToCity; }
            set { SetPropertyValue("SalesToCity", ref _salesToCity, value); }
        }

        [Appearance("SalesInvoiceSalesToAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string SalesToAddress
        {
            get { return _salesToAddress; }
            set { SetPropertyValue("SalesToAddress", ref _salesToAddress, value); }
        }

        #endregion SalesToCostumer

        #region BillToCostumer

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        [Appearance("SellInvoiceBillToCostumerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner BillToCostumer
        {
            get { return _billToCostumer; }
            set
            {
                SetPropertyValue("BillToCostumer", ref _billToCostumer, value);
                if (!IsLoading)
                {
                    if (this._billToCostumer != null)
                    {
                        this.BillToContact = this._billToCostumer.Contact;
                        this.BillToAddress = this._billToCostumer.Address;
                        this.BillToCountry = this._billToCostumer.Country;
                        this.BillToCity = this._billToCostumer.City;
                    }
                }
            }
        }

        [Appearance("SellInvoiceBillToContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string BillToContact
        {
            get { return _billToContact; }
            set { SetPropertyValue("BillToContact", ref _billToContact, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("SellInvoiceBillCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country BillToCountry
        {
            get { return _billToCountry; }
            set { SetPropertyValue("BillToCountry", ref _billToCountry, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("SellInvoiceBillToCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City BillToCity
        {
            get { return _billToCity; }
            set { SetPropertyValue("BillToCity", ref _billToCity, value); }
        }

        [Appearance("SellInvoiceBillToAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string BillToAddress
        {
            get { return _billToAddress; }
            set { SetPropertyValue("BillToAddress", ref _billToAddress, value); }
        }

        #endregion BillToCostumer

        [ImmediatePostData()]
        [Appearance("SalesInvoicePriceGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set { SetPropertyValue("PriceGroup", ref _priceGroup, value); }
        }

        [Appearance("SalesInvoicePaymentMethodClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentMethod PaymentMethod
        {
            get { return _paymentMethod; }
            set { SetPropertyValue("PaymentMethod", ref _paymentMethod, value); }
        }

        [Appearance("SalesInvoicePaymentMethodTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentMethodType PaymentMethodType
        {
            get { return _paymentMethodType; }
            set { SetPropertyValue("PaymentMethodType", ref _paymentMethodType, value); }
        }

        [Appearance("SalesInvoicePaymentTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentType PaymentType
        {
            get { return _paymentType; }
            set { SetPropertyValue("PaymentType", ref _paymentType, value); }
        }

        [Appearance("SalesInvoiceTermOfPaymentClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public TermOfPayment TOP
        {
            get { return _top; }
            set { SetPropertyValue("TOP", ref _top, value); }
        }

        [Appearance("SalesInvoiceCurrencyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        #region Bank

        [Browsable(false)]
        public XPCollection<BankAccount> AvailableBankAccountCompany
        {
            get
            {
                if (this.Company == null)
                {
                    _availableBankAccountCompany = new XPCollection<BankAccount>(Session);
                }
                else
                {
                    _availableBankAccountCompany = new XPCollection<BankAccount>(Session, new GroupOperator
                                                (GroupOperatorType.And,
                                                new BinaryOperator("Company", this.Company)));
                }
                return _availableBankAccountCompany;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableBankAccountCompany", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("SalesInvoiceBankAccountCompanyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BankAccount BankAccountCompany
        {
            get { return _bankAccountCompany; }
            set
            {
                SetPropertyValue("BankAccountCompany", ref _bankAccountCompany, value);
                if (!IsLoading)
                {
                    if (this._bankAccountCompany != null)
                    {
                        this.CompanyAccountNo = this._bankAccountCompany.AccountNo;
                        this.CompanyAccountName = this._bankAccountCompany.AccountName;
                    }
                }
            }
        }

        [Appearance("SalesInvoiceCompanyAccountNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string CompanyAccountNo
        {
            get { return _companyAccountNo; }
            set { SetPropertyValue("CompanyAccountNo", ref _companyAccountNo, value); }
        }

        [Appearance("SalesInvoiceCompanyAccountNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string CompanyAccountName
        {
            get { return _companyAccountName; }
            set { SetPropertyValue("CompanyAccountName", ref _companyAccountName, value); }
        }

        [Browsable(false)]
        public XPCollection<BankAccount> AvailableBankAccounts
        {
            get
            {
                if (this.BillToCostumer == null)
                {
                    _availableBankAccounts = new XPCollection<BankAccount>(Session);
                }
                else
                {
                    _availableBankAccounts = new XPCollection<BankAccount>(Session,
                                             new GroupOperator(GroupOperatorType.And,
                                             new BinaryOperator("BusinessPartner", this.BillToCostumer)));
                }

                return _availableBankAccounts;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableBankAccounts", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("SalesInvoiceBankAccountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BankAccount BankAccount
        {
            get { return _bankAccount; }
            set
            {
                SetPropertyValue("BankAccount", ref _bankAccount, value);
                if (!IsLoading)
                {
                    if (this._bankAccount != null)
                    {
                        this.AccountNo = this._bankAccount.AccountNo;
                        this.AccountName = this._bankAccount.AccountName;
                    }
                }
            }
        }

        [Appearance("SalesInvoiceAccountNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string AccountNo
        {
            get { return _accountNo; }
            set { SetPropertyValue("AccountNo", ref _accountNo, value); }
        }

        [Appearance("SalesInvoiceAccountNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string AccountName
        {
            get { return _accountName; }
            set { SetPropertyValue("AccountName", ref _accountName, value); }
        }

        #endregion Bank

        [Appearance("SalesInvoiceTaxNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string TaxNo
        {
            get { return _taxNo; }
            set { SetPropertyValue("TaxNo", ref _taxNo, value); }
        }

        [Appearance("SalesInvoiceMaxPayClose", Enabled = false)]
        public double MaxBill
        {
            get { return _maxBill; }
            set { SetPropertyValue("MaxBill", ref _maxBill, value); }
        }

        [ImmediatePostData()]
        [Appearance("SalesInvoicePayClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Bill
        {
            get { return _bill; }
            set
            {
                SetPropertyValue("Bill", ref _bill, value);
                if (!IsLoading)
                {
                    if (this.PostedCount > 0)
                    {
                        if (this.Outstanding > 0)
                        {
                            if (this._bill > 0 && this._bill > this.Outstanding)
                            {
                                this._bill = this.Outstanding;
                            }
                        }
                    }

                    if (this.PostedCount == 0)
                    {
                        if (this._bill > 0 && this._bill > this.MaxBill)
                        {
                            this._bill = this.MaxBill;
                        }
                    }
                }
            }
        }

        //[Browsable(false)]
        [ImmediatePostData()]
        [Appearance("SalesInvoiceDM_AmountClose", Enabled = false)]
        public double DM_Amount
        {
            get { return _dm_amount; }
            set { SetPropertyValue("DM_Amount", ref _dm_amount, value); }
        }

        [Appearance("SalesInvoiceOutstandingClose", Enabled = false)]
        public double Outstanding
        {
            get { return _outstanding; }
            set { SetPropertyValue("Outstanding", ref _outstanding, value); }
        }

        [Appearance("SalesInvoicePlanClose", Enabled = false)]
        public double Plan
        {
            get { return _plan; }
            set { SetPropertyValue("Plan", ref _plan, value); }
        }

        [Appearance("SalesInvoiceDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Appearance("SalesInvoicePostedCountClose", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        [Appearance("SalesInvoiceStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Browsable(false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("SalesInvoiceStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("SalesInvoiceEstimatedDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime EstimatedDate
        {
            get { return _estimatedDate; }
            set { SetPropertyValue("EstimatedDate", ref _estimatedDate, value); }
        }

        [Appearance("SalesInvoiceSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [Appearance("SalesInvoiceProjectHeaderClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Association("ProjectHeader-SalesInvoices")]
        public ProjectHeader ProjectHeader
        {
            get { return _projectHeader; }
            set { SetPropertyValue("ProjectHeader", ref _projectHeader, value); }
        }

        [Association("SalesInvoice-SalesInvoiceLines")]
        public XPCollection<SalesInvoiceLine> SalesInvoiceLines
        {
            get { return GetCollection<SalesInvoiceLine>("SalesInvoiceLines"); }
        }

        [Association("SalesInvoice-ApprovalLines")]
        public XPCollection<ApprovalLine> ApprovalLines
        {
            get { return GetCollection<ApprovalLine>("ApprovalLines"); }
        }

        [Association("SalesInvoice-PaymentInPlans")]
        public XPCollection<PaymentInPlan> PaymentInPlans
        {
            get { return GetCollection<PaymentInPlan>("PaymentInPlans"); }
        }

        [Association("SalesInvoice-InvoiceSalesCollections")]
        public XPCollection<InvoiceSalesCollection> InvoiceSalesCollections
        {
            get { return GetCollection<InvoiceSalesCollection>("InvoiceSalesCollections"); }
        }

        #region CLM

        //[Association("SalesInvoice-DebitMemoCollections")]
        //public XPCollection<DebitMemoCollection> DebitMemoCollections
        //{
        //    get { return GetCollection<DebitMemoCollection>("DebitMemoCollections"); }
        //}

        [Browsable(false)]
        [Appearance("SalesInvoiceActiveApproved1Close", Enabled = false)]
        public bool ActiveApproved1
        {
            get { return _activeApproved1; }
            set { SetPropertyValue("ActiveApproved1", ref _activeApproved1, value); }
        }

        [Browsable(false)]
        [Appearance("SalesInvoiceActiveApproved2Close", Enabled = false)]
        public bool ActiveApproved2
        {
            get { return _activeApproved2; }
            set { SetPropertyValue("ActiveApproved2", ref _activeApproved2, value); }
        }

        [Browsable(false)]
        [Appearance("SalesInvoiceActiveApproved3Close", Enabled = false)]
        public bool ActiveApproved3
        {
            get { return _activeApproved3; }
            set { SetPropertyValue("ActiveApproved3", ref _activeApproved3, value); }
        }

        [Persistent("GrandTotalSIL")]
        public double GrandTotalSIL
        {
            get
            {
                double _result = 0;
                int _totalLine = 0;
                if (!IsLoading)
                {
                    _totalLine = Convert.ToInt32(Session.Evaluate<SalesInvoice>(CriteriaOperator.Parse("SalesInvoiceLines.Count"), CriteriaOperator.Parse("Oid=?", Oid)));
                    if (_totalLine >= 0)
                    {
                        _result = UpdateGrandTotalSIL(true);
                    }
                }
                return _result;
            }
        }

        #endregion CLM

        #endregion Field

        #region CLM

        public double UpdateGrandTotalSIL(bool forceChangeEvents)
        {
            double _result = 0;
            try
            {
                XPCollection<SalesInvoiceLine> _locSalesInvoiceLines = new XPCollection<SalesInvoiceLine>(Session,
                                                                       new GroupOperator(GroupOperatorType.And,
                                                                       new BinaryOperator("SalesInvoice", this)));

                if (_locSalesInvoiceLines != null && _locSalesInvoiceLines.Count > 0)
                {
                    if (_locSalesInvoiceLines.Count > 0)
                    {
                        foreach (SalesInvoiceLine _locSalesInvoiceLine in _locSalesInvoiceLines)
                        {
                            if (forceChangeEvents)
                            {
                                if (_locSalesInvoiceLine.TAmountSIL >= 0)
                                {
                                    _result = _locSalesInvoiceLine.TAmountSIL;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesInvoice", ex.ToString());
            }
            return _result;
        }

        #endregion CLm

        #region LocalNumbering
        public string LocGetNumberingSelectionUnlockOptimisticRecord(IDataLayer _currIDataLayer, ObjectList _currObject, NumberingLine _currNumberingLine)
        {
            string _result = null;
            Session _generatorSession = null;
            try
            {
                if (_currIDataLayer != null)
                {
                    _generatorSession = new Session(_currIDataLayer);
                }

                if (_generatorSession != null)
                {
                    ApplicationSetup _appSetup = _generatorSession.FindObject<ApplicationSetup>(new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Active", true), new BinaryOperator("DefaultSystem", true)));
                    if (_appSetup != null)
                    {
                        NumberingHeader _numberingHeader = _generatorSession.FindObject<NumberingHeader>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                             new BinaryOperator("NumberingType", NumberingType.Objects),
                                                             new BinaryOperator("ApplicationSetup", _appSetup, BinaryOperatorType.Equal),
                                                             new BinaryOperator("Active", true)));
                        if (_numberingHeader != null)
                        {
                            NumberingLine _numberingLine = _generatorSession.FindObject<NumberingLine>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Oid", _currNumberingLine.Oid),
                                                         new BinaryOperator("ObjectList", _currObject),
                                                         new BinaryOperator("Selection", true),
                                                         new BinaryOperator("Active", true),
                                                         new BinaryOperator("NumberingHeader", _numberingHeader)));
                            if (_numberingLine != null)
                            {
                                _numberingLine.LastValue = _numberingLine.LastValue + _numberingLine.IncrementValue;
                                _numberingLine.Save();
                                _result = _numberingLine.FormatedValue;
                                _numberingLine.Session.CommitTransaction();
                            }

                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = GlobalFunction " + ex.ToString());
            }
            return _result;
        }
#endregion LocalNumbering

    }
}