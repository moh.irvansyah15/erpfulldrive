﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Sales")]
    [RuleCombinationOfPropertiesIsUnique("SalesQuotationCollectionRuleUnique", DefaultContexts.Save, "Code")]
    //[DeferredDeletion]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class SalesQuotationCollection : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private string _code;
        private XPCollection<SalesQuotation> _availableSalesQuotation;
        private SalesQuotation _salesQuotation;
        private SalesOrder _salesOrder;
        private Status _status;
        private DateTime _statusDate;
        private GlobalFunction _globFunc;

        public SalesQuotationCollection(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.SalesQuotationCollection);
                DateTime now = DateTime.Now;
                this.Status = Status.Open;
                this.StatusDate = now;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("SalesQuotationCollectionCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Browsable(false)]
        public XPCollection<SalesQuotation> AvailableSalesQuotation
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (!IsLoading)
                {
                    if (this.SalesOrder != null)
                    {
                        if(this.SalesOrder.PriceGroup != null && this.SalesOrder.SalesToCostumer != null)
                        {
                            XPQuery<SalesQuotationMonitoring> _salesQuotationMonitoringsQuery = new XPQuery<SalesQuotationMonitoring>(Session);

                            var _salesQuotationMonitorings = from sqm in _salesQuotationMonitoringsQuery
                                                             where (sqm.Status == Status.Open && sqm.PostedCount == 0
                                                             && sqm.PriceGroup == this.SalesOrder.PriceGroup 
                                                             && sqm.SalesQuotation.Customer == this.SalesOrder.SalesToCostumer
                                                             && sqm.SalesOrder == null)
                                                             group sqm by sqm.SalesQuotation into g
                                                             select new { SalesQuotation = g.Key };

                            if (_salesQuotationMonitorings != null && _salesQuotationMonitorings.Count() > 0)
                            {
                                List<string> _stringSQM = new List<string>();

                                foreach (var _salesQuotationMonitoring in _salesQuotationMonitorings)
                                {
                                    if (_salesQuotationMonitoring != null)
                                    {
                                        _stringSQM.Add(_salesQuotationMonitoring.SalesQuotation.Code);
                                    }
                                }

                                IEnumerable<string> _stringArraySQMDistinct = _stringSQM.Distinct();
                                string[] _stringArraySQMList = _stringArraySQMDistinct.ToArray();
                                if (_stringArraySQMList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArraySQMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArraySQMList[i] + "'";
                                        }
                                    }
                                }
                                else if (_stringArraySQMList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArraySQMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArraySQMList[i] + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _stringArraySQMList[i] + "'";
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availableSalesQuotation = new XPCollection<SalesQuotation>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                        }    
                    }
                }
                return _availableSalesQuotation;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableSalesQuotation", DataSourcePropertyIsNullMode.SelectNothing)]
        public SalesQuotation SalesQuotation
        {
            get { return _salesQuotation; }
            set { SetPropertyValue("SalesQuotation", ref _salesQuotation, value); }
        }

        [ImmediatePostData()]
        [Appearance("SalesQuotationCollectionSalesOrderClose", Enabled = false)]
        [Association("SalesOrder-SalesQuotationCollections")]
        public SalesOrder SalesOrder
        {
            get { return _salesOrder; }
            set { SetPropertyValue("SalesOrder", ref _salesOrder, value); }
        }

        [Appearance("SalesQuotationCollectionStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("SalesQuotationCollectionStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        #endregion Field
    }
}