﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [NavigationItem("PPIC")]
    [DefaultProperty("Code")]
    [RuleCombinationOfPropertiesIsUnique("BillOfMaterialRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class BillOfMaterial : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private string _name;
        private string _description;
        private BillOfMaterialVersion _billOfMaterialVersion;
        private Item _item;
        private double _dQty;
        private UnitOfMeasure _dUom;
        private bool _active;
        private GlobalFunction _globFunc;

        public BillOfMaterial(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.BillOfMaterial);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Association("BillOfMaterialVersion-BillOfMaterials")]
        public BillOfMaterialVersion BillOfMaterialVersion
        {
            get { return _billOfMaterialVersion; }
            set { SetPropertyValue("BillOfMaterialVersion", ref _billOfMaterialVersion, value); }
        }

        [ImmediatePostData()]
        public Item Item
        {
            get { return _item; }
            set {
                SetPropertyValue("Item", ref _item, value);
                if(!IsLoading)
                {
                    if(this._item != null)
                    {
                        if(this._item.BasedUOM != null)
                        {
                            this.DUOM = this._item.BasedUOM;
                        }
                    }
                }
            }
        }

        [ImmediatePostData()]
        public double DQty
        {
            get { return _dQty; }
            set { SetPropertyValue("DQty", ref _dQty, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM
        {
            get { return _dUom; }
            set
            {
                SetPropertyValue("DUOM", ref _dUom, value);
            }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        [Association("BillOfMaterial-BillOfMaterialLines")]
        public XPCollection<BillOfMaterialLine> BillOfMaterialLines
        {
            get { return GetCollection<BillOfMaterialLine>("BillOfMaterialLines"); }
        }

        #endregion Field
    }
}