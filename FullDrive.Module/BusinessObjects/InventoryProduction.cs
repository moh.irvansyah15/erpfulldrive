﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Production")]
    [RuleCombinationOfPropertiesIsUnique("InventoryProductionRuleUnique", DefaultContexts.Save, "Code")]

    //Disable Button
    [Appearance("DisableQcPassedtIP", Criteria = "SelectHide = false", AppearanceItemType = "Action", TargetItems = "InventoryProductionQcPassedActionId", Enabled = false)]
    [Appearance("DisableQcRejectIP", Criteria = "SelectHide = false", AppearanceItemType = "Action", TargetItems = "InventoryProductionQcRejectActionId", Enabled = false)]
    [Appearance("DisableActualWeightIP", Criteria = "SelectHide = false", AppearanceItemType = "Action", TargetItems = "InventoryProductionActualweightActionId", Enabled = false)]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class InventoryProduction : FullDriveSysBaseObject
    {
        #region Default

        private bool _activationPosting;
        private string _code;

        private DirectionType _transferType;
        private InventoryMovingType _inventoryMovingType;
        private DocumentRule _documentRule;
        private ObjectList _objectList;

        XPCollection<DocumentType> _availableDocumentType;
        private DocumentType _documentType;
        private string _docNo;

        private XPCollection<Location> _availableLocation;
        private Location _location;
        private BinLocation _binLocation;

        private int _productionYear;
        private string _orderNumber;
        private Item _item;
        private string _description;
        private Country _country;
        private City _city;
        private double _qtyBegining;
        private double _qtyMinimal;
        private double _qtyAvailable;
        private double _qtySale;

        private double _bruto;
        private double _netto;
        private double _tara;
        private bool _selectHide;

        private UnitOfMeasure _defaultUOM;
        private double _count;
        private double _qtyPackage;
        private UnitOfMeasure _unitPackage;
        private int _fromCount;

        private double _brutoActual;
        private double _nettoActual;
        private double _taraActual;
        private int _postCountWeight;

        private int _postCountLine;
        private WorkOrder _workOrder;
        private Status _status;
        private DateTime _statusDate;
        private string _userAccess;
        private Company _company;
        private bool _activeApproved1;
        private bool _activeApproved2;
        private bool _activeApproved3;
        private GlobalFunction _globFunc;

        public InventoryProduction(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).

            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.InventoryProduction);
                this.Status = Status.Open;
                this.StatusDate = now;

                this.ObjectList = ObjectList.InventoryProduction;
                this.TransferType = DirectionType.External;
                this.InventoryMovingType = InventoryMovingType.Receive;
                this.DocumentRule = DocumentRule.None;

                #region UserAccess
                _userAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                if (Session.IsNewObject(this))
                {
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null)
                            {
                                Company = _locUserAccess.Employee.Company;
                            }
                        }
                    }
                }
                #endregion UserAccess
            }
        }

        #endregion Default

        #region Field

        [Browsable(false)]
        [ImmediatePostData()]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [ImmediatePostData()]
        [Appearance("InventoryProductionCodeClose", Enabled = false)]
        [Appearance("InventoryProductionRedColor", Criteria = "ActiveApproved1 = true", BackColor = "#fd79a8")]
        [Appearance("InventoryProductionYellowColor", Criteria = "ActiveApproved2 = true", BackColor = "#ffeaa7")]
        [Appearance("InventoryProductionGreenOColor", Criteria = "ActiveApproved3 = true", BackColor = "#81ecec")]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        #region Document
        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("InventoryTransferInTransferTypeClose", Enabled = false)]
        public DirectionType TransferType
        {
            get { return _transferType; }
            set { SetPropertyValue("TransferType", ref _transferType, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("InventoryTransferInInventoryMovingTypeClose", Enabled = false)]
        public InventoryMovingType InventoryMovingType
        {
            get { return _inventoryMovingType; }
            set { SetPropertyValue("InventoryMovingType", ref _inventoryMovingType, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        [Appearance("InventoryTransferInDocumentRuleClose", Enabled = false)]
        public DocumentRule DocumentRule
        {
            get { return _documentRule; }
            set { SetPropertyValue("DocumentRule", ref _documentRule, value); }
        }

        [Browsable(false)]
        [Appearance("InventoryTransferInObjectListClose", Enabled = false)]
        public ObjectList ObjectList
        {
            get { return _objectList; }
            set { SetPropertyValue("ObjectList", ref _objectList, value); }
        }

        [Browsable(false)]
        public XPCollection<DocumentType> AvailableDocumentType
        {
            get
            {
                if (ObjectList == ObjectList.None)
                {
                    _availableDocumentType = new XPCollection<DocumentType>(Session);
                }
                else
                {
                    _availableDocumentType = new XPCollection<DocumentType>(Session,
                                             new GroupOperator(GroupOperatorType.And,
                                             new BinaryOperator("ObjectList", this.ObjectList),
                                             new BinaryOperator("InventoryMovingType", this.InventoryMovingType),
                                             new BinaryOperator("TransferType", this._transferType),
                                             new BinaryOperator("Active", true)));
                }
                return _availableDocumentType;

            }
        }

        [Appearance("InventoryProductionDocumentTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableDocumentType", DataSourcePropertyIsNullMode.SelectAll)]
        public DocumentType DocumentType
        {
            get { return _documentType; }
            set
            {
                SetPropertyValue("DocumentType", ref _documentType, value);
                if (!IsLoading)
                {
                    if (_documentType != null)
                    {
                        this.InventoryMovingType = this._documentType.InventoryMovingType;

                        _globFunc = new GlobalFunction();
                        this.DocNo = _globFunc.GetDocumentNumberingUnlockOptimisticRecord(this.Session.DataLayer, this._documentType);
                    }
                }
            }
        }

        [Appearance("InventoryProductionDocNoClose", Enabled = false)]
        public string DocNo
        {
            get { return _docNo; }
            set { SetPropertyValue("DocNo", ref _docNo, value); }
        }
        #endregion Document

        #region Location
        [Browsable(false)]
        public XPCollection<Location> AvailableLocation
        {
            get
            {
                _userAccess = SecuritySystem.CurrentUserName;
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                string _locLocationCode = null;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));

                if (_locUserAccess != null)
                {
                    if ((this.TransferType == DirectionType.External || this.TransferType == DirectionType.Internal)
                        && this.InventoryMovingType == InventoryMovingType.Receive)
                    {
                        List<string> _stringLocation = new List<string>();

                        XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                                                 (Session, new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("UserAccess", _locUserAccess),
                                                                                  new BinaryOperator("ObjectList", ObjectList.InventoryProduction),
                                                                                  new BinaryOperator("Owner", true),
                                                                                  new BinaryOperator("Default", true),
                                                                                  new BinaryOperator("TransferType", this.TransferType),
                                                                                  new BinaryOperator("InventoryMovingType", this.InventoryMovingType),
                                                                                  //new BinaryOperator("LocationType2", LocationType2.Prod),
                                                                                  new BinaryOperator("Active", true)));

                        #region 
                        if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                        {
                            foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                            {
                                if (_locWhsSetupDetail.Location != null)
                                {
                                    if (_locWhsSetupDetail.Location.Code != null)
                                    {
                                        _locLocationCode = _locWhsSetupDetail.Location.Code;
                                        _stringLocation.Add(_locLocationCode);
                                    }
                                }

                            }
                        }

                        IEnumerable<string> _stringArrayLocationFromDistinct = _stringLocation.Distinct();
                        string[] _stringArrayLocationFromList = _stringArrayLocationFromDistinct.ToArray();
                        if (_stringArrayLocationFromList.Length == 1)
                        {
                            for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                            {
                                Location _locLocationFrom = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                if (_locLocationFrom != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locLocationFrom.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArrayLocationFromList.Length > 1)
                        {
                            for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                            {
                                Location _locLocationFrom = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                if (_locLocationFrom != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locLocationFrom.Code + "'";
                                    }
                                    else
                                    {
                                        _endString = " OR [Code]=='" + _locLocationFrom.Code + "'";
                                    }
                                }
                            }
                        }
                        _fullString = _beginString + _endString;

                        if (_fullString != null)
                        {
                            _availableLocation = new XPCollection<Location>(Session, CriteriaOperator.Parse(_fullString));
                        }
                        #endregion
                    }
                }
                else
                {
                    _availableLocation = new XPCollection<Location>(Session);
                }

                return _availableLocation;

            }
        }

        [Appearance("InventoryProductionLineLocationClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableLocation", DataSourcePropertyIsNullMode.SelectAll)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public Location Location
        {
            get { return _location; }
            set { SetPropertyValue("Location", ref _location, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true And Location = '@This.Location'")]
        [Appearance("InventoryProductionLineBinLocationClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BinLocation BinLocation
        {
            get { return _binLocation; }
            set { SetPropertyValue("BinLocation", ref _binLocation, value); }
        }
        #endregion Location

        [Appearance("InventoryProductionProductionYearEnabled", Criteria = "ActivationPosting = true", Enabled = false)]
        public int ProductionYear
        {
            get { return _productionYear; }
            set { SetPropertyValue("ProductionYear", ref _productionYear, value); }
        }

        [Appearance("InventoryProductionOrderNumberEnabled", Criteria = "ActivationPosting = true", Enabled = false)]
        public string OrderNumber
        {
            get { return _orderNumber; }
            set { SetPropertyValue("OrderNumber", ref _orderNumber, value); }
        }

        [ImmediatePostData()]
        [Appearance("InventoryProductionItemClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Item Item
        {
            get { return _item; }
            set
            {
                SetPropertyValue("Item", ref _item, value);
                if (!IsLoading)
                {
                    if (this._item != null)
                    {
                        this.DefaultUOM = this._item.BasedUOM;
                    }
                }
            }
        }

        [Size(512)]
        [Appearance("InventoryProductionDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Appearance("InventoryProductionDefaultUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public UnitOfMeasure DefaultUOM
        {
            get { return _defaultUOM; }
            set { SetPropertyValue("DefaultUOM", ref _defaultUOM, value); }
        }

        [Appearance("InventoryProductionCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country Country
        {
            get { return _country; }
            set { SetPropertyValue("Country", ref _country, value); }
        }

        [Appearance("InventoryProductionCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City City
        {
            get { return _city; }
            set { SetPropertyValue("City", ref _city, value); }
        }

        [Browsable(false)]
        [Appearance("InventoryProductionQtyBeginingClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double QtyBegining
        {
            get { return _qtyBegining; }
            set { SetPropertyValue("QtyBegining", ref _qtyBegining, value); }
        }

        [Browsable(false)]
        [Appearance("InventoryProductionQtyMinimalClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double QtyMinimal
        {
            get { return _qtyMinimal; }
            set { SetPropertyValue("QtyMinimal", ref _qtyMinimal, value); }
        }

        [Appearance("InventoryProductionQtyAvailableClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double QtyAvailable
        {
            get { return _qtyAvailable; }
            set { SetPropertyValue("QtyAvailable", ref _qtyAvailable, value); }
        }

        [ImmediatePostData]
        [Appearance("InventoryProductionQtyPackageClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double QtyPackage
        {
            get { return _qtyPackage; }
            set { SetPropertyValue("QtyPackage", ref _qtyPackage, value); }
        }

        [Appearance("InventoryProductionCountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Count
        {
            get
            {
                if (QtyAvailable != 0)
                {
                    this._count = this._qtyAvailable / this._qtyPackage;
                }

                return _count;
            }
            set { SetPropertyValue("Count", ref _count, value); }
        }

        [Appearance("InventoryProductionUnitPackageClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public UnitOfMeasure UnitPackage
        {
            get { return _unitPackage; }
            set { SetPropertyValue("DefaultUOM", ref _unitPackage, value); }
        }

        [Browsable(false)]
        [Appearance("InventoryProductionQtySaleClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double QtySale
        {
            get { return _qtySale; }
            set { SetPropertyValue("QtySale", ref _qtySale, value); }
        }

        [Appearance("InventoryProductionBrutoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Bruto
        {
            get { return _bruto; }
            set { SetPropertyValue("Bruto", ref _bruto, value); }
        }

        [Appearance("InventoryProductionNettoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Netto
        {
            get { return _netto; }
            set { SetPropertyValue("Netto", ref _netto, value); }
        }

        [Appearance("InventoryProductionTaraClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Tara
        {
            get { return _tara; }
            set { SetPropertyValue("Tara", ref _tara, value); }
        }

        [Appearance("InventoryProductionFormCountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public int FromCount
        {
            get { return _fromCount; }
            set { SetPropertyValue("FormCount", ref _fromCount, value); }
        }

        [Appearance("InventoryProductionBrutoActualClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double BrutoActual
        {
            get { return _brutoActual; }
            set { SetPropertyValue("BrutoActual", ref _brutoActual, value); }
        }

        [Appearance("InventoryProductionNettoActualClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double NettoActual
        {
            get { return _nettoActual; }
            set { SetPropertyValue("NettoActual", ref _nettoActual, value); }
        }

        [Appearance("InventoryProductionTaraActualClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double TaraActual
        {
            get { return _taraActual; }
            set { SetPropertyValue("Tara", ref _taraActual, value); }
        }

        [Browsable(false)]
        [Appearance("InventoryProductionPostCountWeightClose", Enabled = false)]
        public int PostCountWeight
        {
            get { return _postCountWeight; }
            set { SetPropertyValue("PostCountWeight", ref _postCountWeight, value); }
        }

        [Browsable(false)]
        [Appearance("InventoryProductionPostCountLineClose", Enabled = false)]
        public int PostCountLine
        {
            get { return _postCountLine; }
            set { SetPropertyValue("PostCountLine", ref _postCountLine, value); }
        }

        [Appearance("InventoryProductionCompanyClose", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("InventoryProductionWorkOrderClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public WorkOrder WorkOrder
        {
            get { return _workOrder; }
            set { SetPropertyValue("WorkOrder", ref _workOrder, value); }
        }

        [Appearance("InventoryProductionStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("InventoryProductionStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Association("InventoryProduction-InventoryProductionLines")]
        public XPCollection<InventoryProductionLine> InventoryProductionLines
        {
            get { return GetCollection<InventoryProductionLine>("InventoryProductionLines"); }
        }

        [Browsable(false)]
        [Appearance("InventoryProductionActiveApproved1Close", Enabled = false)]
        public bool ActiveApproved1
        {
            get { return _activeApproved1; }
            set { SetPropertyValue("ActiveApproved1", ref _activeApproved1, value); }
        }

        [Browsable(false)]
        [Appearance("InventoryProductionActiveApproved2Close", Enabled = false)]
        public bool ActiveApproved2
        {
            get { return _activeApproved2; }
            set { SetPropertyValue("ActiveApproved2", ref _activeApproved2, value); }
        }

        [Browsable(false)]
        [Appearance("InventoryProductionActiveApproved3Close", Enabled = false)]
        public bool ActiveApproved3
        {
            get { return _activeApproved3; }
            set { SetPropertyValue("ActiveApproved3", ref _activeApproved3, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public bool SelectHide
        {
            get { return _selectHide; }
            set { SetPropertyValue("SelectHide", ref _selectHide, value); }
        }

        #endregion Field

    }
}