﻿        #region Default
using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using FullDrive.Module.CustomProcess;
using System.IO;
using System.Text;
using DevExpress.XtraEditors;
using FullDrive.Module.Controllers;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Payroll")]
    [RuleCombinationOfPropertiesIsUnique("PayrollProcessRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class PayrollProcess : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private string _name;
        private Employee _employee;
        private Period _period;
        private double _salarybasic;
        private string _salaryBasicText;
        private double _salaryBasicLCY;
        private double _salaryProcessValue;
        private string _salaryProcessValueText;
        private double _salaryProcessValueLCY;
        private double _totalAllowanceValue;
        private string _totalAllowanceValueText;
        private double _totalDeductionValue;
        private string _totalDeductionValueText;
        private bool _useActiveCurrencyForCurrencyRate;
        private Currency _currencyCode;
        private double _currencyRate;
        private bool _active;
        private string _employeeDepartmentName;
        private string _employeeName;
        private string _employeePeriodName;
        private GlobalFunction _globFunc;
        
        public PayrollProcess(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PayrollProcess);
                this.UseActiveCurrencyForCurrencyRate = true;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }
        #endregion

        #region OnChanged
        protected override void OnChanged(string propertyName, object oldValue, object newValue)
        {
            base.OnChanged(propertyName, oldValue, newValue);
            try
            {
                if (!IsLoading)
                {
                    switch (propertyName)
                    {
                        case "Employee":
                            {
                                UpdateName();
                                //UpdateCurrencyRate();
                                //SetCurrencyRate();
                                break;
                            }

                        case "Period":
                            {
                                UpdateName();
                                //UpdateCurrencyRate();
                                //SetCurrencyRate();
                                break;
                            }

                        case "SalaryFormula":
                            {
                                //Calculate();
                                break;
                            }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PayrollProcess" + ex.ToString());
            }
        }
        #endregion OnChanged

        #region OnSaving
        protected override void OnSaving()
        {
            base.OnSaving();
            try
            {
                if (this.CurrencyCode != null)
                {
                    SetCurrencyRate();
                    SetSalaryBasicLCY();
                }
                Calculate();
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PayrollProcess" + ex.ToString());
            }
        }
        #endregion OnSaving

        #region Field
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("PayrollProcessCode", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("PayrollProcessName", Enabled =false)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [ImmediatePostData()]
        [Association("Employee-PayrollProcesses")]
        public Employee Employee
        {
            get { return _employee; }
            set {
                    SetPropertyValue("Employee", ref _employee, value);
                    SetCurrencyRate();
                    SetSalaryBasicLCY();
            }
            
        }

        [ImmediatePostData()]
        [Association("Period-PayrollProcesses")]
        public Period Period
        {
            get { return _period; }
            set {
                    SetPropertyValue("Period", ref _period, value);
                    SetCurrencyRate();
                    SetSalaryBasicLCY();
                }
            
        }

        [ImmediatePostData()]
        [Appearance("PayrollProcessSalaryBasic", Enabled = false)]
        public double SalaryBasic
        {
            //get
            //{
            //    if (Period != null)
            //    {
            //        return RijndaelSimple.DecryptString(this, SalaryBasicText);
            //    }
            //    return 0;
            //}
            //set
            //{
            //    SalaryBasicText = RijndaelSimple.EncryptString(this, value);
            //    //UseCurrencyRate();
            //}
            get { return _salarybasic; }
            set {
                    SetPropertyValue("SalaryBasic", ref _salarybasic, value);
                    if (SalaryBasic > 0)
                {
                    this._salaryBasicText = this._salarybasic.ToString();
                    this._salaryProcessValue = this._salarybasic;
                    this._salaryProcessValueText = this._salarybasic.ToString();
                    //UpdateCurrencyRate();

                }
                }
        }

        [ImmediatePostData()]
        public string SalaryBasicText
        {
            get { return _salaryBasicText; }
            set { SetPropertyValue("SalaryBasicText", ref _salaryBasicText, value); }
        }

        [ImmediatePostData()]
        [Appearance("PayrollProcessSalaryProcessValue", Enabled = false)]
        public double SalaryProcessValue
        {
            //get
            //{
            //    if (Period != null)
            //    {
            //        return RijndaelSimple.DecryptString(this, SalaryBasicValueText);
            //    }
            //    return 0;
            //}
            //set
            //{
            //    SalaryBasicValueText = RijndaelSimple.EncryptString(this, value);
            //}
            get {   if (this.SalaryBasic > 0 )
                    {
                        this._salaryProcessValue = _salarybasic;
                    }
                    return _salaryProcessValue; }
            set {
                    SetPropertyValue("SalaryProcessValue", ref _salaryProcessValue, value);
                    if (SalaryProcessValue > 0)
                    {
                        this.SalaryProcessValueText = this.SalaryProcessValue.ToString();
                    }
                }
        }

        [ImmediatePostData()]
        public string SalaryProcessValueText
        {
            get { return _salaryProcessValueText; }
            set { SetPropertyValue("SalaryProcessValueText", ref _salaryProcessValueText, value); }
        }

        [ImmediatePostData()]
        [Appearance("PayrollProcessSalaryBasicLCY", Enabled = false)]
        public double SalaryBasicLCY
        {
            get { return _salaryBasicLCY = SalaryBasic * this.CurrencyRate; }
            set { SetPropertyValue("SalaryBasicLCY", ref _salaryBasicLCY, value); }
            // return SalaryBasic * CurrencyRate
        }

        [ImmediatePostData()]
        [Appearance("PayrollProcessSalaryProcessValueLCY", Enabled = false)]
        public double SalaryProcessValueLCY
        {
            get { return _salaryProcessValueLCY = SalaryProcessValue * this.CurrencyRate; }
            set { SetPropertyValue("SalaryProcessValueLCY", ref _salaryProcessValueLCY, value); }
            // return SalaryProcessValue * CurrencyRate
        }

        [Appearance("PayrollProcessCurrencyCode", Enabled = false)]
        public Currency CurrencyCode
        {
            get {
                    if (Employee != null)
                        {
                        return this.Employee.Currency;
                }
                return null;
                }
            set { SetPropertyValue("CurrencyCode", ref _currencyCode, value); }
            // Employee.Currency
        }

        [ImmediatePostData()]
        public double CurrencyRate
        {
            get
            {
                if (Employee != null)
                {
                    //SetCurrencyRate();
                }
                return _currencyRate;
            }

            set { SetPropertyValue("CurrencyRate", ref _currencyRate, value); }
        }

        [ImmediatePostData()]
        public bool UseActiveCurrencyForCurrencyRate
        {
            get { return _useActiveCurrencyForCurrencyRate; }
            set {
                    SetPropertyValue("UseActiveCurrencyForCurrencyRate", ref _useActiveCurrencyForCurrencyRate, value);
                    SetCurrencyRate();
                    SetSalaryBasicLCY();
                }
        }

        [ImmediatePostData()]
        public double TotalAllowanceValue
        {
            get { return _totalAllowanceValue; }
            set {
                    SetPropertyValue("TotalAllowanceValue", ref _totalAllowanceValue, value);
                    //SetProcessValue();
                }
        }

        public string TotalAllowanceValueText
        {
            get { return _totalAllowanceValueText; }
            set { SetPropertyValue("TotalAllowanceValueText", ref _totalAllowanceValueText, value); }
        }

        [ImmediatePostData()]
        public double TotalDeductionValue
        {
            get { return _totalDeductionValue; }
            set {
                    SetPropertyValue("TotalDecutionValue", ref _totalDeductionValue, value);
                    //SetProcessValue();
                }
        }

        public string TotalDeductionValueText
        {
            get { return _totalDeductionValueText; }
            set
            {
                SetPropertyValue("TotalDecutionValueText", ref _totalDeductionValueText, value);
                //SetProcessValue();
            }
        }

        [ImmediatePostData()]
        [Appearance("PayrollProcessEmployeeDepartmentName", Enabled =false)]
        public string EmployeeDepartmentName
        {

            //get {
            //    string val1 = null;
            //    if (this.Employee != null)
            //    {
            //        val1 = this.Employee.Department.ToString();
            //        EmployeeDepartmentName = val1;
            //    }
            //    else
            //        {
            //            return val1;
            //        }
            //        return null;
            //    }
            get { if(Employee != null)
                    {
                    this._employeeDepartmentName = this.Employee.Department.ToString();
                    }
                return _employeeDepartmentName;
                }
            set { SetPropertyValue("EmployeeDepartmentName", ref _employeeDepartmentName, value); }

        }

        [ImmediatePostData()]
        [Appearance("PayrollProcessEmployeeName", Enabled =false)]
        public string EmployeeName
        {
            //get {
            //    string val2 = null;
            //    if (this.Employee != null)
            //    {
            //        val2 = this.Employee.Name;
            //        EmployeeName = val2;
            //    }
            //    else
            //        {
            //            return val2;
            //        }
            //        return null;
            //    }
            get {
                    if (Employee != null)
                {
                    this._employeeName = this.Employee.Name;
                }
                return _employeeName;
                    }
            set { SetPropertyValue("EmployeeName", ref _employeeName, value); }
        }

        [ImmediatePostData()]
        [Appearance("PayrollProcessEmployeePeriodName", Enabled =false)]
        public string EmployeePeriodName
        {
            //get
            //{
            //    string val3 = null;
            //    if (this.Period != null)
            //    {
            //        val3 = this.Period.Name;
            //        EmployeePeriodName = val3;
            //    }
            //    else
            //    {
            //        return val3;
            //    }
            //    return null;
            //}
            get
            {
                   
                
                    if (_period !=  null)
                    {
                        return _employeePeriodName = this.Period.Name.ToString();
                    }
                
                return null;
            }
            set { SetPropertyValue("EmployeePeriodName", ref _employeePeriodName, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        [Association("PayrollProcess-PayrollProcessDetails")]
        public XPCollection<PayrollProcessDetail> PayrollProcessDetails
        {
            get { return GetCollection<PayrollProcessDetail>("PayrollProcessDetails"); }
        }
        #endregion Field

        #region UpdateName
        private void UpdateName()
        {
            try
            {
                Name = string.Format("Payroll Process - {0} [ {1} ] {2:0000} - {3:00}", Employee.Name, Employee.Code, Period.PeriodYear, Period.PeriodMonth );
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PayrollProcess" + ex.ToString());
            }
        }
        #endregion UpdateName

        #region GetSalaryBasic
        //[Action(ToolTip = "Get Salary Basic", Caption = "Get Salary Basic")]
        public double GetSalaryBasic()
        {
            double Result = 0;
            SalaryBasic SalBasicRec = Session.FindObject<SalaryBasic>
                                      (new GroupOperator(GroupOperatorType.And,
                                      new BinaryOperator("Period", this.Period),
                                      new BinaryOperator("Employee", this.Employee)));
            if (SalBasicRec != null)
                Result = SalBasicRec.SalaryBasicValue;
            this.SalaryBasic = Result;
            return Result;
        }
        #endregion GetSalaryBasic

        #region SetCurrencyRate
        public void SetCurrencyRate()
        {
            try
            {
                double _result = 0;
                if (Employee != null)
                {
                    if (Employee.Currency != null)
                    {
                        Currency _locCurrency = Session.FindObject<Currency>
                                                   (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Active", true),
                                                    new BinaryOperator("Default", true)));
                        if (_locCurrency != null)
                        {
                            //if (_locCurrency != this._employee.Currency)
                            //{
                            CurrencyRate _loccurrencyRate = Session.FindObject<CurrencyRate>
                                         (new GroupOperator(GroupOperatorType.And,
                                          new BinaryOperator("Currency1", _locCurrency),
                                          new BinaryOperator("Currency2", this._employee.Currency)));

                            if (_loccurrencyRate != null)
                            {
                                _result = _loccurrencyRate.Rate1;
                                this._currencyRate = _result;


                            }
                            //this._currencyRate = 1;
                            //}
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalaryBasic" + ex.ToString());
            }
        }
        #endregion

        #region SetSalaryBasicLCY
        public void SetSalaryBasicLCY()
        {
            if (Employee != null)
            {
                if (Employee.Currency != null)
                {
                    if (SalaryBasic > 0)
                    {
                        if (CurrencyRate > 0)
                        {
                            this._salarybasic = this._salarybasic * this._currencyRate;
                        }
                    }
                }
            }
        }
        #endregion

        #region CalculateWithTax
        //[Action(ToolTip = "Process Payroll with Tax Process", Caption = "Process With Tax")]
        public void CalculateWithTax() 
        {
            try
            {
                TaxProcess TaxProc = Session.FindObject<TaxProcess>(new GroupOperator(GroupOperatorType.And, new BinaryOperator("Period", this.Period), new BinaryOperator("Employee", this.Employee)));
                if (TaxProc == null)
                {
                    //MsgBox(string.Format("Tax process doesnt exist for Employee {0} and Period {1}, system will create it for you", this.Employee, this.Period), Constants.vbExclamation);
                    TaxProc = new TaxProcess(Session);
                    TaxProc.Employee = this.Employee;
                    TaxProc.Period = this.Period;
                    TaxProc.Save();
                }
                Calculate();
                //TaxProc.CalculatesPPh21();
                //TaxProc.Save();
                Calculate();
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PayrollProcess " + ex.ToString());
            }
        }
        #endregion CalculateWithTax

        #region Calculate
        //[Action(ToolTip = "Process", Caption = "Process")]
        public void Calculate()
        {
            //ProgressBarForm pBarForm = new ProgressBarForm();
            try
            {
                SalaryBasic = GetSalaryBasic();

                SalaryProcessValue = 0;

                //pBarForm.Text = "Payroll Process";
                //pBarForm.pBar.Minimum = 0;
                //pBarForm.pBar.Maximum = this.PayrollProcessDetails.Count + 1;
                //pBarForm.Show();
                //pBarForm.UpdatePbar(true, "Initialize....", null/* Conversion error: Set to default value for this argument */, null/* Conversion error: Set to default value for this argument */);

                foreach (PayrollProcessDetail PayProcDetRec in this.PayrollProcessDetails)
                {
                    {
                        var withBlock = PayProcDetRec;
                        //pBarForm.UpdatePbar(true, "Calculating data...", null/* Conversion error: Set to default value for this argument */, null/* Conversion error: Set to default value for this argument */);
                        // Debug.Print(String.Format("{0} {1} {2}", "PayrollProcess before", .Component.Name, .ComponentValue))
                        withBlock.Calculate();
                        if (withBlock.Component.SalaryType == SalaryType.Deduction)
                        {
                            SalaryProcessValue -= withBlock.ComponentValue;
                            //pBarForm.UpdatePbar(false, "Deduction...", withBlock.ComponentValue.ToString, SalaryProcessValue.ToString);
                        }
                        else
                        {
                            SalaryProcessValue += withBlock.ComponentValue;
                            //pBarForm.UpdatePbar(false, "Addition...", withBlock.ComponentValue.ToString, SalaryProcessValue.ToString);
                        }
                    }
                }
                this.Save();
                CalculateTotal();
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PayrollProcess" + ex.ToString());
            }
            finally
            {
                //pBarForm.Close();
                //pBarForm.Dispose();
            }
        }
        #endregion Calculate

        #region CalculateTotal
        public void CalculateTotal()
        {
            try
            {
                this.TotalDeductionValue = 0;
                this.TotalAllowanceValue = 0;
                foreach (PayrollProcessDetail PayProDetTot in this.PayrollProcessDetails)
                {
                    {
                        var withBlock = PayProDetTot;
                        if (withBlock.Component.SalaryType == SalaryType.Deduction)
                            this.TotalDeductionValue += withBlock.ComponentValue;
                        else if (withBlock.Component.SalaryType == SalaryType.Allowance)
                            this.TotalAllowanceValue += withBlock.ComponentValue;
                    }
                }
                // MsgBox("Deduction = " & Me.TotalDeductionValue.ToString)
                // MsgBox("Allowance = " & Me.TotalAllowanceValue.ToString)
                this.Save();
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusineesObject = PyarollProcess" + ex.ToString());
            }
        }
        #endregion CalculateTotal

        //
    }
}