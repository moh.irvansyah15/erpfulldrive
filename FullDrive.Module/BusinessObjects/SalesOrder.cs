﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.SystemModule;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Editors;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Sales")]
    [RuleCombinationOfPropertiesIsUnique("SalesOrderRuleUnique", DefaultContexts.Save, "Code")]
    //[DeferredDeletion]
    //[ListViewFilter("AllDataSO", "", "All Data", "All Data In Sales Order", 1, true)]
    //[ListViewFilter("OpenSO", "Status = 'Open'", "Open", "Open Data Status In Sales Order", 2, true)]
    //[ListViewFilter("ProgressSO", "Status = 'Progress'", "Progress", "Progress Data Status In Sales Order", 3, true)]
    //[ListViewFilter("CloseSO", "Status = 'Close'", "Close", "Close Data Status In Sales Order", 4, true)]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class SalesOrder : FullDriveSysBaseObject
    {
        #region Default

        private bool _activationPosting;
        private string _code;
        private ObjectList _objectList;
        XPCollection<NumberingLine> _availableNumberingLine;
        private NumberingLine _numbering;
        private BusinessPartner _salesToCostumer;
        private string _salesToContact;
        private Country _salesToCountry;
        private City _salesToCity;
        private string _salesToAddress;
        private DateTime _orderDate;
        private DateTime _documentDate;
        private string _costumerOrderNo;
        private string _costumerShipmentNo;
        private string _costumerInvoiceNo;
        private TermOfPayment _top;
        private PaymentMethod _paymentMethod;
        private string _taxNo;
        private string _remarks;
        private DirectionType _transferType;
        private Employee _internalPIC;
        private bool _codeInternalPIC;
        private int _internalPICCodeCount;
        private string _remarkTOP;
        private UserAccess _userAccess;
        private Currency _currency;
        private XPCollection<PriceGroup> _availablePriceGroup;
        private PriceGroup _priceGroup;
        private double _dp_percentage;
        private Status _status;
        private DateTime _statusDate;
        private DateTime _estimatedDate;
        private ProjectHeader _projectHeader;
        private string _signCode;
        private Company _company;
        private double _maksBill;
        private string _localUserAccess;
        private GlobalFunction _globFunc;
        //clm
        private bool _activeApproved1;
        private bool _activeApproved2;
        private bool _activeApproved3;
        private Status _statusWR;
        private DateTime _statusDateWR;
        private Section _section;
        private Division _division;
        private Employee _employee;
        private Department _department;

        public SalesOrder(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).

            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.SalesOrder);
                DateTime now = DateTime.Now;
                this.Status = CustomProcess.Status.Open;
                this.StatusWR = CustomProcess.Status.Open;
                this.StatusDate = now;
                this.StatusDateWR = now;
                this.ObjectList = CustomProcess.ObjectList.SalesOrder;

                #region UserAccess
                _localUserAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                if (Session.IsNewObject(this))
                {
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null)
                            {
                                Company = _locUserAccess.Employee.Company;
                            }
                            if (_locUserAccess.Employee.Division != null)
                            {
                                Division = _locUserAccess.Employee.Division;
                            }
                            if (_locUserAccess.Employee.Department != null)
                            {
                                Department = _locUserAccess.Employee.Department;
                            }
                            if (_locUserAccess.Employee.Section != null)
                            {
                                Section = _locUserAccess.Employee.Section;
                            }
                        }
                        this.UserAccess = _locUserAccess;
                    }
                }
                #endregion UserAccess

                this.Currency = _globFunc.GetDefaultCurrency(this.Session);
            }
        }

        #endregion Default

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                if (this.InternalPICCodeCount > 0 && this.CodeInternalPIC == false)
                {
                    this.CodeInternalPIC = true;
                    this.Save();
                }
            }
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("SalesOrderRedColor", Criteria = "ActiveApproved1 = true", BackColor = "#fd79a8")]
        [Appearance("SalesOrderYellowColor", Criteria = "ActiveApproved2 = true", BackColor = "#ffeaa7")]
        [Appearance("SalesOrderGreenOColor", Criteria = "ActiveApproved3 = true", BackColor = "#81ecec")]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("SalesOrderCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Browsable(false)]
        public ObjectList ObjectList
        {
            get { return _objectList; }
            set { SetPropertyValue("ObjectList", ref _objectList, value); }
        }

        [Browsable(false)]
        public XPCollection<NumberingLine> AvailableNumberingLine
        {
            get
            {
                if (ObjectList == ObjectList.None)
                {
                    _availableNumberingLine = new XPCollection<NumberingLine>(Session);
                }
                else
                {
                    _availableNumberingLine = new XPCollection<NumberingLine>(Session,
                                              new GroupOperator(GroupOperatorType.And,
                                              new BinaryOperator("ObjectList", this.ObjectList),
                                              new BinaryOperator("Selection", true),
                                              new BinaryOperator("Active", true)));
                }
                return _availableNumberingLine;

            }
        }

        [NonPersistent()]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableNumberingLine", DataSourcePropertyIsNullMode.SelectAll)]
        public NumberingLine Numbering
        {
            get { return _numbering; }
            set
            {
                NumberingLine _oldNumbering = _numbering;
                SetPropertyValue("Numbering", ref _numbering, value);
                if (!IsLoading)
                {
                    
                    _globFunc = new GlobalFunction();
                    if (this._numbering != null)
                    {
                        this.Code = LocGetNumberingSelectionUnlockOptimisticRecord(ObjectList.SalesOrder, _numbering);
                    }
                    //clm (070420)
                    else
                    {
                        string _locSO = null;
                        _globFunc = new GlobalFunction();
                        _locSO = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.SalesOrder);

                        this.Code = _locSO;
                    }
                }
            }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        [Appearance("SalesOrderSalesToCostumerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner SalesToCostumer
        {
            get { return _salesToCostumer; }
            set
            {
                SetPropertyValue("SalesToCostumer", ref _salesToCostumer, value);
                if (!IsLoading)
                {
                    if (this._salesToCostumer != null)
                    {
                        this.SalesToContact = this._salesToCostumer.Contact;
                        this.SalesToCountry = this._salesToCostumer.Country;
                        this.SalesToCity = this._salesToCostumer.City;
                        this.TaxNo = this._salesToCostumer.TaxNo;
                        this.TOP = this._salesToCostumer.TOP;
                        this.SalesToAddress = this._salesToCostumer.Address;
                        if(this.InternalPIC == null)
                        {
                            this.InternalPIC = this._salesToCostumer.InternalPIC;
                        } 
                    }
                }
            }
        }

        [Appearance("SalesOrderSalesToContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string SalesToContact
        {
            get { return _salesToContact; }
            set { SetPropertyValue("SalesToContact", ref _salesToContact, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("SalesOrderSalesToCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country SalesToCountry
        {
            get { return _salesToCountry; }
            set { SetPropertyValue("SalesToCountry", ref _salesToCountry, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("SalesOrderSalesToCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City SalesToCity
        {
            get { return _salesToCity; }
            set { SetPropertyValue("SalesToCity", ref _salesToCity, value); }
        }

        [Size(512)]
        [Appearance("SalesOrderSalesToAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string SalesToAddress
        {
            get { return _salesToAddress; }
            set { SetPropertyValue("SalesToAddress", ref _salesToAddress, value); }
        }

        [Appearance("SalesOrderOrderDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime OrderDate
        {
            get { return _orderDate; }
            set { SetPropertyValue("OrderDate", ref _orderDate, value); }
        }

        [Appearance("SalesOrderDocumentDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime DocumentDate
        {
            get { return _documentDate; }
            set { SetPropertyValue("DocumentDate", ref _documentDate, value); }
        }

        [Appearance("SalesOrderCostumerOrderNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string CostumerOrderNo
        {
            get { return _costumerOrderNo; }
            set { SetPropertyValue("CostumerOrderNo", ref _costumerOrderNo, value); }
        }

        [Appearance("SalesOrderCostumerShipmentNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string CostumerShipmentNo
        {
            get { return _costumerShipmentNo; }
            set { SetPropertyValue("CostumerShipmentNo", ref _costumerShipmentNo, value); }
        }

        [Appearance("SalesOrderCostumerInvoiceNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string CostumerInvoiceNo
        {
            get { return _costumerInvoiceNo; }
            set { SetPropertyValue("CostumerInvoiceNo", ref _costumerInvoiceNo, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("SalesOrderTOPClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public TermOfPayment TOP
        {
            get { return _top; }
            set { SetPropertyValue("TOP", ref _top, value); }
        }

        [DataSourceCriteria("Active = true")]
        [ImmediatePostData()]
        [Appearance("SalesOrderPaymentMethodClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentMethod PaymentMethod
        {
            get { return _paymentMethod; }
            set
            {
                SetPropertyValue("PaymentMethod", ref _paymentMethod, value);
            }
        }

        [Appearance("SalesOrderTaxNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string TaxNo
        {
            get { return _taxNo; }
            set { SetPropertyValue("TaxNo", ref _taxNo, value); }
        }

        [Appearance("SalesOrderTransferTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DirectionType TransferType
        {
            get { return _transferType; }
            set { SetPropertyValue("TransferType", ref _transferType, value); }
        }

        [ImmediatePostData()]
        [Appearance("SalesOrderInternalPICClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Employee InternalPIC
        {
            get { return _internalPIC; }
            set {
                SetPropertyValue("InternalPIC", ref _internalPIC, value);
                if(!IsLoading)
                {
                    string _locPICCode = null;
                    string _locPIC_Code = null;
                    string _locPriceGroup_Code = null;
                    _globFunc = new GlobalFunction();
                    if (this._internalPIC != null)
                    {
                        if (this._priceGroup != null)
                        {
                            if (this._priceGroup.Code != null)
                            {
                                _locPriceGroup_Code = this._priceGroup.Code;
                            }
                        }

                        if (this.InternalPIC.Code != null)
                        {
                            _locPIC_Code = this.InternalPIC.Code;
                        }

                        if (_locPIC_Code != null && _locPriceGroup_Code != null)
                        {
                            _locPICCode = _globFunc.GetNumberingPICUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.SalesOrder, _locPIC_Code, _locPriceGroup_Code);
                            if (_locPICCode != null)
                            {
                                if (this.CodeInternalPIC == false)
                                {
                                    this.Code = _locPICCode;
                                    this.InternalPICCodeCount = this.InternalPICCodeCount + 1;
                                }
                            }
                        }
                    }
                    //clm (070420)
                    else
                    {
                        string _locSO = null;
                        _globFunc = new GlobalFunction();
                        _locSO = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.SalesOrder);

                        this.Code = _locSO;
                    }
                }
            }
        }

        [Browsable(false)]
        public bool CodeInternalPIC
        {
            get { return _codeInternalPIC; }
            set { SetPropertyValue("CodeInternalPIC", ref _codeInternalPIC, value); }
        }

        [Browsable(false)]
        public int InternalPICCodeCount
        {
            get { return _internalPICCodeCount; }
            set { SetPropertyValue("InternalPICCodeCount", ref _internalPICCodeCount, value); }
        }

        [Appearance("SalesOrderRemarkTOPClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string RemarkTOP
        {
            get { return _remarkTOP; }
            set { SetPropertyValue("RemarkTOP", ref _remarkTOP, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public UserAccess UserAccess
        {
            get { return _userAccess; }
            set { SetPropertyValue("UserAccess", ref _userAccess, value); }
        }

        [Appearance("SalesOrderCurrencyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        [Browsable(false)]
        public XPCollection<PriceGroup> AvailablePriceGroup
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                if (UserAccess == null)
                {
                    _availablePriceGroup = new XPCollection<PriceGroup>(Session);
                }
                else
                {
                    List<string> _stringPG = new List<string>();

                    XPCollection<SalesSetupDetail> _locSalesSetupDetails = new XPCollection<SalesSetupDetail>
                                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("UserAccess", this.UserAccess),
                                                                               new BinaryOperator("Active", true)));

                    if (_locSalesSetupDetails != null && _locSalesSetupDetails.Count() > 0)
                    {
                        foreach (SalesSetupDetail _locSalesSetupDetail in _locSalesSetupDetails)
                        {
                            if(_locSalesSetupDetail.PriceGroup != null)
                            {
                                if(_locSalesSetupDetail.PriceGroup.Code != null)
                                {
                                    _stringPG.Add(_locSalesSetupDetail.PriceGroup.Code);
                                }
                            }   
                        }
                    }

                    IEnumerable<string> _stringArrayPGDistinct = _stringPG.Distinct();
                    string[] _stringArrayPGList = _stringArrayPGDistinct.ToArray();
                    if (_stringArrayPGList.Length == 1)
                    {
                        for (int i = 0; i < _stringArrayPGList.Length; i++)
                        {
                            PriceGroup _locPG = Session.FindObject<PriceGroup>(new BinaryOperator("Code", _stringArrayPGList[i]));
                            if (_locPG != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locPG.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArrayPGList.Length > 1)
                    {
                        for (int i = 0; i < _stringArrayPGList.Length; i++)
                        {
                            PriceGroup _locPG = Session.FindObject<PriceGroup>(new BinaryOperator("Code", _stringArrayPGList[i]));
                            if (_locPG != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locPG.Code + "'";
                                }
                                else
                                {
                                    _endString = _endString + " OR [Code]=='" + _locPG.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availablePriceGroup = new XPCollection<PriceGroup>(Session, CriteriaOperator.Parse(_fullString));
                    }

                }
                return _availablePriceGroup;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailablePriceGroup", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        [Appearance("SalesOrderPriceGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set {
                SetPropertyValue("PriceGroup", ref _priceGroup, value);
                if (!IsLoading)
                {
                    string _locPICCode = null;
                    string _locPIC_Code = null;
                    string _locPriceGroup_Code = null;
                    _globFunc = new GlobalFunction();
                    if (this._priceGroup != null)
                    {
                        if (this._priceGroup.Code != null)
                        {
                            _locPriceGroup_Code = this._priceGroup.Code;
                            
                        }
                        if (this.InternalPIC != null)
                        {
                            if (this.InternalPIC.Code != null)
                            {
                                _locPIC_Code = this.InternalPIC.Code;
                            }
                        }
                        if (_locPIC_Code != null && _locPriceGroup_Code != null)
                        {
                            _locPICCode = _globFunc.GetNumberingPICUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.SalesOrder, _locPIC_Code, _locPriceGroup_Code);
                            if (_locPICCode != null)
                            {
                                if (this.CodeInternalPIC == false)
                                {
                                    this.Code = _locPICCode;
                                    this.InternalPICCodeCount = this.InternalPICCodeCount + 1;
                                }
                            }
                        }  
                    }
                }
            }
        }

        //menghitung total di line terus di kalikan 
        [ImmediatePostData()]
        [Appearance("SalesOrderDP_PercentageClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double DP_Percentage
        {
            get { return _dp_percentage; }
            set { SetPropertyValue("DP_Percentage", ref _dp_percentage, value); }
        }

        [Appearance("SalesOrderStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("SalesOrderStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("SalesOrderStatusWRClose", Enabled = false)]
        public Status StatusWR
        {
            get { return _statusWR; }
            set { SetPropertyValue("StatusWR", ref _statusWR, value); }
        }

        [Appearance("SalesOrderStatusDateWRClose", Enabled = false)]
        public DateTime StatusDateWR
        {
            get { return _statusDateWR; }
            set { SetPropertyValue("StatusDateWR", ref _statusDateWR, value); }
        }

        [Appearance("SalesOrderEstimatedDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime EstimatedDate
        {
            get { return _estimatedDate; }
            set { SetPropertyValue("EstimatedDate", ref _estimatedDate, value); }
        }

        [Appearance("SalesOrderRemarksClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Remarks
        {
            get { return _remarks; }
            set { SetPropertyValue("Remarks", ref _remarks, value); }
        }

        [Appearance("SalesOrderSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [Appearance("SalesOrderEmployeeClose", Enabled = false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        [Browsable(false)]
        [DataSourceCriteria("Active = true")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Browsable(false)]
        public double MaksBill
        {
            get { return _maksBill; }
            set { SetPropertyValue("MaksBill", ref _maksBill, value); }
        }

        [Appearance("SalesOrderProjectHeaderClose", Enabled = false)]
        [Association("ProjectHeader-SalesOrders")]
        public ProjectHeader ProjectHeader
        {
            get { return _projectHeader; }
            set { SetPropertyValue("ProjectHeader", ref _projectHeader, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Association("SalesOrder-SalesOrderLines")]
        public XPCollection<SalesOrderLine> SalesOrderLines
        {
            get { return GetCollection<SalesOrderLine>("SalesOrderLines"); }
        }

        [Association("SalesOrder-ApprovalLines")]
        public XPCollection<ApprovalLine> ApprovalLines
        {
            get { return GetCollection<ApprovalLine>("ApprovalLines"); }
        }

        [Association("SalesOrder-SalesQuotationCollections")]
        public XPCollection<SalesQuotationCollection> SalesQuotationCollections
        {
            get { return GetCollection<SalesQuotationCollection>("SalesQuotationCollections"); }
        }

        #region CLM

        [Browsable(false)]
        [Appearance("SalesOrderActiveApproved1Close", Enabled = false)]
        public bool ActiveApproved1
        {
            get { return _activeApproved1; }
            set { SetPropertyValue("ActiveApproved1", ref _activeApproved1, value); }
        }

        [Browsable(false)]
        [Appearance("SalesOrderActiveApproved2Close", Enabled = false)]
        public bool ActiveApproved2
        {
            get { return _activeApproved2; }
            set { SetPropertyValue("ActiveApproved2", ref _activeApproved2, value); }
        }

        [Browsable(false)]
        [Appearance("SalesOrderActiveApproved3Close", Enabled = false)]
        public bool ActiveApproved3
        {
            get { return _activeApproved3; }
            set { SetPropertyValue("ActiveApproved3", ref _activeApproved3, value); }
        }

        [Persistent("GrandTotalSOL")]
        public double GrandTotalSOL
        {
            get
            {
                double _result = 0;
                int _totalLine = 0;
                if (!IsLoading)
                {
                    _totalLine = Convert.ToInt32(Session.Evaluate<SalesOrder>(CriteriaOperator.Parse("SalesOrderLines.Count"), CriteriaOperator.Parse("Oid=?", Oid)));
                    if (_totalLine >= 0)
                    {
                        _result = UpdateGrandTotalSOL(true);
                    }
                }
                return _result;
            }
        }

        #endregion CLM

        #endregion Field

        //======================================= Code Only ================================================

        #region CLM

        public double UpdateGrandTotalSOL(bool forceChangeEvents)
        {
            double _result = 0;
            try
            {
                XPCollection<SalesOrderLine> _locSalesOrderLines = new XPCollection<SalesOrderLine>(Session,
                                                                         new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("SalesOrder", this)));

                if (_locSalesOrderLines != null && _locSalesOrderLines.Count > 0)
                {
                    if (_locSalesOrderLines.Count > 0)
                    {
                        foreach (SalesOrderLine _locSalesOrderLine in _locSalesOrderLines)
                        {
                            if (forceChangeEvents)
                            {
                                if (_locSalesOrderLine.TAmountSOL >= 0)
                                {
                                    _result = _locSalesOrderLine.TAmountSOL;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesOrder", ex.ToString());
            }
            return _result;
        }

        #endregion CLm

        #region LocalNumbering
        private string LocGetNumberingSelectionUnlockOptimisticRecord(ObjectList _currObject, NumberingLine _currNumberingLine)
        {
            string _result = null;
          
            try
            {
                ApplicationSetup _appSetup = this.Session.FindObject<ApplicationSetup>(new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Active", true), new BinaryOperator("DefaultSystem", true)));
                if (_appSetup != null)
                {
                    NumberingHeader _numberingHeader = this.Session.FindObject<NumberingHeader>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("NumberingType", NumberingType.Objects),
                                                         new BinaryOperator("ApplicationSetup", _appSetup, BinaryOperatorType.Equal),
                                                         new BinaryOperator("Active", true)));
                    if (_numberingHeader != null)
                    {
                        NumberingLine _numberingLine = this.Session.FindObject<NumberingLine>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("Oid", _currNumberingLine.Oid),
                                                     new BinaryOperator("ObjectList", _currObject),
                                                     new BinaryOperator("Selection", true),
                                                     new BinaryOperator("Active", true),
                                                     new BinaryOperator("NumberingHeader", _numberingHeader)));
                        if (_numberingLine != null)
                        {
                            _numberingLine.LastValue = _numberingLine.LastValue + _numberingLine.IncrementValue;
                            _numberingLine.Save();
                            _result = _numberingLine.FormatedValue;
                            _numberingLine.Session.CommitTransaction();
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = SalesOrder " + ex.ToString());
            }
            return _result;
        }

        //private string LocGetNumberingPICUnlockOptimisticRecord(ObjectList _currObject)
        //{
        //    string _result = null;
            
        //    try
        //    {
        //        ApplicationSetup _appSetup = this.Session.FindObject<ApplicationSetup>(new GroupOperator(GroupOperatorType.And,
        //                                    new BinaryOperator("Active", true), new BinaryOperator("DefaultSystem", true)));

        //        if (_appSetup != null)
        //        {
        //            NumberingHeader _numberingHeader = this.Session.FindObject<NumberingHeader>
        //                                                (new GroupOperator(GroupOperatorType.And,
        //                                                 new BinaryOperator("NumberingType", NumberingType.Employee),
        //                                                 new BinaryOperator("ApplicationSetup", _appSetup, BinaryOperatorType.Equal),
        //                                                 new BinaryOperator("Active", true)));
        //            if (_numberingHeader != null)
        //            {
        //                if (this.InternalPIC != null && this.PriceGroup != null)
        //                {
        //                    NumberingLine _numberingLine = this.Session.FindObject<NumberingLine>
        //                                            (new GroupOperator(GroupOperatorType.And,
        //                                             new BinaryOperator("ObjectList", _currObject),
        //                                             new BinaryOperator("PIC", this.InternalPIC),
        //                                             new BinaryOperator("PriceGroup", this.PriceGroup),
        //                                             new BinaryOperator("Active", true),
        //                                             new BinaryOperator("NumberingHeader", _numberingHeader)));
        //                    if (_numberingLine != null)
        //                    {
        //                        _numberingLine.LastValue = _numberingLine.LastValue + _numberingLine.IncrementValue;
        //                        _numberingLine.Save();
        //                        _result = _numberingLine.FormatedValue;
        //                        _numberingLine.Session.CommitTransaction();
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Tracing.Tracer.LogError(" Module = SalesOrder " + ex.ToString());
        //    }
        //    return _result;
        //}
        #endregion LocalNumbering

    }
}