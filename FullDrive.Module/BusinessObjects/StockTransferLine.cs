﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Inventory")]
    [RuleCombinationOfPropertiesIsUnique("StockTransferLineRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class StockTransferLine : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private int _no;
        private string _code;
        private Item _item;
        private bool _activationPostingFrom;
        private Location _locationFrom;
        private BinLocation _binLocationFrom;
        private StockType _stockTypeFrom;
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasure;
        #region InitialDefaultQuantityFrom
        private double _dQtyFrom;
        private UnitOfMeasure _dUomFrom;
        private double _qtyFrom;
        private UnitOfMeasure _uomFrom;
        private double _tQtyFrom;
        #endregion InitialDefaultQuantityFrom
        private Status _statusFrom;
        private DateTime _statusDateFrom;
        private bool _activationPostingTo;
        private Location _locationTo;
        private BinLocation _binLocationTo;
        private StockType _stockTypeTo;
        #region InitialDefaultQuantityTo
        private double _dQtyTo;
        private UnitOfMeasure _dUomTo;
        private double _qtyTo;
        private UnitOfMeasure _uomTo;
        private double _tQtyTo;
        #endregion InitialDefaultQuantityTo
        private Status _statusTo;
        private DateTime _statusDateTo;
        private StockTransfer _stockTransfer;
        private GlobalFunction _globFunc;

        public StockTransferLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.TempBeginingInventoryLine);
                this.StatusFrom = CustomProcess.Status.Open;
                this.StatusDateFrom = now;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("StockTransferLineNoClose", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [Appearance("StockTransferLineCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("StockTransferLineItemClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Item Item
        {
            get { return _item; }
            set
            {
                SetPropertyValue("Item", ref _item, value);
                if (!IsLoading)
                {
                    if (this._item != null)
                    {
                        this.DUOM_From = this._item.BasedUOM;
                        this.DUOM_To = this._item.BasedUOM;
                    }
                }
            }
        }

        [Browsable(false)]
        public bool ActivationPostingFrom
        {
            get { return _activationPostingFrom; }
            set { SetPropertyValue("ActivationPostingFrom", ref _activationPostingFrom, value); }
        }

        #region LocationFrom
        [Appearance("StockTransferLineLocationFromCloseFrom", Criteria = "ActivationPostingFrom = true", Enabled = false)]
        [Appearance("StockTransferLineLocationFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Location LocationFrom
        {
            get { return _locationFrom; }
            set { SetPropertyValue("LocationFrom", ref _locationFrom, value); }
        }

        [Appearance("StockTransferLineBinLocationFromCloseFrom", Criteria = "ActivationPostingFrom = true", Enabled = false)]
        [Appearance("StockTransferLineBinLocationFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BinLocation BinLocationFrom
        {
            get { return _binLocationFrom; }
            set { SetPropertyValue("BinLocationFrom", ref _binLocationFrom, value); }
        }

        [Appearance("StockTransferLineStockTypeFromCloseFrom", Criteria = "ActivationPostingFrom = true", Enabled = false)]
        [Appearance("StockTransferLineStockTypeFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockType StockTypeFrom
        {
            get { return _stockTypeFrom; }
            set { SetPropertyValue("StockTypeFrom", ref _stockTypeFrom, value); }
        }
        #endregion LocationFrom

        [Browsable(false)]
        public XPCollection<UnitOfMeasure> AvailableUnitOfMeasure
        {
            get
            {
                if (Item == null)
                {
                    _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session);
                }
                else
                {
                    XPQuery<UnitOfMeasure> _locQueryUnitOfMeasures = new XPQuery<UnitOfMeasure>(Session);
                    XPQuery<ItemUnitOfMeasure> _locQueryItemUnitOfMeasures = new XPQuery<ItemUnitOfMeasure>(Session);
                    var _results = from _locUOM in _locQueryUnitOfMeasures
                                   join _locIUOM in _locQueryItemUnitOfMeasures on _locUOM.Oid equals _locIUOM.UOM.Oid
                                   where (_locIUOM.Item.Oid == this.Item.Oid)
                                   select _locUOM;

                    _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session, _results.ToList());

                }
                return _availableUnitOfMeasure;

            }
        }

        #region DefaultQtyFrom

        [Appearance("StockTransferLineDQtyFromCloseFrom", Criteria = "ActivationPostingFrom = true", Enabled = false)]
        [Appearance("StockTransferLineDQtyFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double DQtyFrom
        {
            get { return _dQtyFrom; }
            set
            {
                SetPropertyValue("DQtyFrom", ref _dQtyFrom, value);
                if (!IsLoading)
                {
                    SetTotalQtyFrom();
                }
            }
        }

        [Appearance("StockTransferLineDUOM_FromCloseFrom", Criteria = "ActivationPostingFrom = true", Enabled = false)]
        [Appearance("StockTransferLineDUOM_FromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM_From
        {
            get { return _dUomFrom; }
            set
            {
                SetPropertyValue("DUOM_From", ref _dUomFrom, value);
                if (!IsLoading)
                {
                    SetTotalQtyFrom();
                }
            }
        }

        [Appearance("StockTransferLineQtyFromCloseFrom", Criteria = "ActivationPostingFrom = true", Enabled = false)]
        [Appearance("StockTransferLineQtyFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double QtyFrom
        {
            get { return _qtyFrom; }
            set
            {
                SetPropertyValue("QtyFrom", ref _qtyFrom, value);
                if (!IsLoading)
                {
                    SetTotalQtyFrom();
                }
            }
        }

        [Appearance("StockTransferLineUOM_FromCloseFrom", Criteria = "ActivationPostingFrom = true", Enabled = false)]
        [Appearance("StockTransferLineUOM_FromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure UOM_From
        {
            get { return _uomFrom; }
            set
            {
                SetPropertyValue("UOM_From", ref _uomFrom, value);
                if (!IsLoading)
                {
                    SetTotalQtyFrom();
                }
            }
        }

        [Appearance("StockTransferLineTQtyFromEnabled", Enabled = false)]
        public double TQtyFrom
        {
            get { return _tQtyFrom; }
            set { SetPropertyValue("TQtyFrom", ref _tQtyFrom, value); }
        }

        #endregion DefaultQtyFrom

        [Appearance("StockTransferLineStatusFromClose", Enabled = false)]
        public Status StatusFrom
        {
            get { return _statusFrom; }
            set { SetPropertyValue("StatusFrom", ref _statusFrom, value); }
        }

        [Appearance("StockTransferLineStatusDateFromClose", Enabled = false)]
        public DateTime StatusDateFrom
        {
            get { return _statusDateFrom; }
            set { SetPropertyValue("StatusDateFrom", ref _statusDateFrom, value); }
        }

        [Browsable(false)]
        public bool ActivationPostingTo
        {
            get { return _activationPostingTo; }
            set { SetPropertyValue("ActivationPostingTo", ref _activationPostingTo, value); }
        }

        #region LocationTo
        [Appearance("StockTransferLineLocationToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Location LocationTo
        {
            get { return _locationTo; }
            set { SetPropertyValue("LocationTo", ref _locationTo, value); }
        }

        [Appearance("StockTransferLineBinLocationToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BinLocation BinLocationTo
        {
            get { return _binLocationTo; }
            set { SetPropertyValue("BinLocationTo", ref _binLocationTo, value); }
        }

        [Appearance("StockTransferLineStockTypeToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockType StockTypeTo
        {
            get { return _stockTypeTo; }
            set { SetPropertyValue("StockTypeTo", ref _stockTypeTo, value); }
        }
        #endregion LocationTo

        #region DefaultQtyTo

        [Appearance("StockTransferLineDQtyToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double DQtyTo
        {
            get { return _dQtyTo; }
            set
            {
                SetPropertyValue("DQtyTo", ref _dQtyTo, value);
                if (!IsLoading)
                {
                    SetTotalQtyFrom();
                }
            }
        }

        [Appearance("StockTransferLineDUOM_ToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM_To
        {
            get { return _dUomTo; }
            set
            {
                SetPropertyValue("DUOM_To", ref _dUomTo, value);
                if (!IsLoading)
                {
                    SetTotalQtyFrom();
                }
            }
        }

        [Appearance("StockTransferLineQtyToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double QtyTo
        {
            get { return _qtyTo; }
            set
            {
                SetPropertyValue("QtyTo", ref _qtyTo, value);
                if (!IsLoading)
                {
                    SetTotalQtyFrom();
                }
            }
        }

        [Appearance("StockTransferLineUOM_ToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure UOM_To
        {
            get { return _uomTo; }
            set
            {
                SetPropertyValue("UOM_To", ref _uomTo, value);
                if (!IsLoading)
                {
                    SetTotalQtyFrom();
                }
            }
        }

        [Appearance("StockTransferLineTQtyToEnabled", Enabled = false)]
        public double TQtyTo
        {
            get { return _tQtyTo; }
            set { SetPropertyValue("TQtyTo", ref _tQtyTo, value); }
        }

        #endregion DefaultQtyTo

        [Appearance("StockTransferLineStatusToClose", Enabled = false)]
        public Status StatusTo
        {
            get { return _statusTo; }
            set { SetPropertyValue("StatusTo", ref _statusTo, value); }
        }

        [Appearance("StockTransferLineStatusDateToClose", Enabled = false)]
        public DateTime StatusDateTo
        {
            get { return _statusDateTo; }
            set { SetPropertyValue("StatusDateTo", ref _statusDateTo, value); }
        }

        [Association("StockTransfer-StockTransferLines")]
        public StockTransfer StockTransfer
        {
            get { return _stockTransfer; }
            set { SetPropertyValue("StockTransfer", ref _stockTransfer, value); }
        }

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}

        //================================================== Code Only ==================================================

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.StockTransfer != null)
                    {
                        object _makRecord = Session.Evaluate<StockTransferLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("StockTransfer=?", this.StockTransfer));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = StockTransferLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.StockTransfer != null)
                {
                    StockTransfer _numHeader = Session.FindObject<StockTransfer>
                                                (new BinaryOperator("Code", this.StockTransfer.Code));

                    XPCollection<StockTransferLine> _numLines = new XPCollection<StockTransferLine>
                                                (Session, new BinaryOperator("StockTransfer", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (StockTransferLine _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = StockTransferLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.StockTransfer != null)
                {
                    StockTransfer _numHeader = Session.FindObject<StockTransfer>
                                                (new BinaryOperator("Code", this.StockTransfer.Code));

                    XPCollection<StockTransferLine> _numLines = new XPCollection<StockTransferLine>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("StockTransfer", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (StockTransferLine _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = StockTransferLine " + ex.ToString());
            }
        }

        #endregion No

        private void SetTotalQtyFrom()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.StockTransfer != null)
                {
                    if (this.Item != null && this.UOM_From != null && this.DUOM_From != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM_From),
                                                         new BinaryOperator("DefaultUOM", this.DUOM_From),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.QtyFrom * _locItemUOM.DefaultConversion + this.DQtyFrom;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.QtyFrom / _locItemUOM.Conversion + this.DQtyFrom;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.QtyFrom + this.DQtyFrom;
                            }

                            this.TQtyFrom = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.QtyFrom + this.DQtyFrom;
                        this.TQtyFrom = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = StockTransferLine " + ex.ToString());
            }
        }

    }
}