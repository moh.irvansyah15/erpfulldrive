﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("FullName")]
    [NavigationItem("Quality Assurance")]
    [RuleCombinationOfPropertiesIsUnique("DesignNumberRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class DesignNumber : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private ProductGroup _productGroup;
        private DesignProperty _designProperty;
        private Standart _standart;
        private string _orderCreationDesign;
        private Separator _separator;
        private string _revisiNo;
        private string _fullName;
        private ProductType _productType;
        private bool _active;
        private GlobalFunction _globFunc;

        public DesignNumber(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.DesignNumber);
                this.Separator = _globFunc.GetDefaultSeparator(Session);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        [Appearance("DesignNumberCodeEnabled", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        public ProductGroup ProductGroup
        {
            get { return _productGroup; }
            set
            {
                SetPropertyValue("ProductGroup", ref _productGroup, value);
                if (!IsLoading)
                {
                    if (this._productGroup != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        public DesignProperty DesignProperty
        {
            get { return _designProperty; }
            set
            {
                SetPropertyValue("DesignProperty", ref _designProperty, value);
                if (!IsLoading)
                {
                    if (this._designProperty != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        public Standart Standart
        {
            get { return _standart; }
            set
            {
                SetPropertyValue("Standart", ref _standart, value);
                if (!IsLoading)
                {
                    if (this._standart != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        [RuleRequiredField(DefaultContexts.Save), ImmediatePostData()]
        public string OrderCreationDesign
        {
            get { return _orderCreationDesign; }
            set
            {
                SetPropertyValue("OrderCreationDesign", ref _orderCreationDesign, value);
                if (!IsLoading)
                {
                    if (this._orderCreationDesign != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        public Separator Separator
        {
            get { return _separator; }
            set
            {
                SetPropertyValue("Separator", ref _separator, value);
                if (!IsLoading)
                {
                    if (this._separator != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        [ImmediatePostData()]
        public string RevisiNo
        {
            get { return _revisiNo; }
            set
            {
                SetPropertyValue("RevisiNo", ref _revisiNo, value);
                if (!IsLoading)
                {
                    if (this._revisiNo != null)
                    {
                        SetFullName();
                    }
                }
            }
        }

        [Appearance("DesignNumberFullNameEnabled", Enabled = false)]
        public string FullName
        {
            get { return _fullName; }
            set { SetPropertyValue("FullName", ref _fullName, value); }
        }

        [Association("ProductType-DesignNumbers")]
        [DataSourceCriteria("Active = true")]
        public ProductType ProductType
        {
            get { return _productType; }
            set { SetPropertyValue("ProductType", ref _productType, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        //[Association("DesignNumber-ProductTypes")]
        //public XPCollection<ProductType>ProductTypes
        //{
        //    get { return GetCollection<ProductType>("ProductTypes"); }
        //}

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}

        //=============================================== Code In Here ===============================================

        private void SetFullName()
        {
            try
            {
                string _locProductGroup = "";
                string _locDesignProperty = "";
                string _locStandart = "";
                string _locOrderCreationDesign = "";
                string _locSeparator = "";
                string _locRevisiNo = "";
                if (this.ProductGroup != null) { _locProductGroup = this.ProductGroup.Code; }
                if (this.DesignProperty != null) { _locDesignProperty = this.DesignProperty.Code; }
                if (this.Standart != null) { _locStandart = this.Standart.Code; }
                if (this.OrderCreationDesign != null) { _locOrderCreationDesign = this.OrderCreationDesign; }
                if (this.Separator != null) { _locSeparator = this.Separator.Code; }
                if (this.RevisiNo != null) { _locRevisiNo = this.RevisiNo; }

                this.FullName = String.Format("{0}{1}{2}{3}{4}{5}", _locProductGroup, _locDesignProperty, _locStandart,
                                _locOrderCreationDesign, _locSeparator, _locRevisiNo).Trim().Replace(" ", "");
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = DesignNumber ", ex.ToString());
            }
        }

    }
}