﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("DiscountLineRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class DiscountLine : FullDriveSysBaseObject
    {
        #region Default

        private int _no;
        private string _code;
        private string _name;
        private DiscountMode _discountMode;
        private string _description;
        private PurchaseOrderLine _purchaseOrderLine;
        private PurchaseInvoiceLine _purchaseInvoiceLine;
        private SalesOrderLine _salesOrderLine;
        private SalesQuotationLine _salesQuotationLine;
        private SalesInvoiceLine _salesInvoiceLine;
        private Discount _discount;
        private double _disc;
        private double _discAmount;
        private double _uAmount;
        private double _tUAmount;
        private bool _active;
        private bool _default;
        private GlobalFunction _globFunc;

        public DiscountLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.DiscountLine);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        //No
        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }
        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        #endregion Default

        #region Field

        [Browsable(false)]
        [Appearance("DiscountLineNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [Appearance("DiscountLineCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        //[Appearance("DiscountLineTaxClose", Enabled = false)]
        [ImmediatePostData]
        public Discount Discount
        {
            get { return _discount; }
            set
            {
                SetPropertyValue("Discount", ref _discount, value);
                if (!IsLoading)
                {
                    if (this._discount != null)
                    {
                        this.Name = GetName();
                        this.DiscountMode = this._discount.DiscountMode;
                        this.Active = this._discount.Active;
                        this.Default = this._discount.Default;
                        this.Disc = this._discount.Value;
                    }
                }
            }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Appearance("DiscountLineDiscountModeClose", Enabled = false)]
        public DiscountMode DiscountMode
        {
            get { return _discountMode; }
            set { SetPropertyValue("DiscountMode", ref _discountMode, value); }
        }

        [Browsable(false)]
        [Appearance("DiscountLineUAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double UAmount
        {
            get { return _uAmount; }
            set
            {
                SetPropertyValue("UAmount", ref _uAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._uAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.DiscountLine, FieldName.UAmount) == true)
                        {
                            this._uAmount = _globFunc.GetRoundUp(Session, this._uAmount, ObjectList.DiscountLine, FieldName.UAmount);
                        }
                    }
                }
            }
        }

        [Appearance("DiscountLineTUAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TUAmount
        {
            get { return _tUAmount; }
            set
            {
                SetPropertyValue("TUAmount", ref _tUAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._tUAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.DiscountLine, FieldName.TUAmount) == true)
                        {
                            this._tUAmount = _globFunc.GetRoundUp(Session, this._tUAmount, ObjectList.DiscountLine, FieldName.TUAmount);
                        }
                    }
                }
            }
        }

        //[Appearance("DiscountLineDiscClose", Enabled = false)]
        [ImmediatePostData()]
        public double Disc
        {
            get { return _disc; }
            set
            {
                SetPropertyValue("Disc", ref _disc, value);
                if (!IsLoading)
                {
                    SetDiscAmount();
                }
            }
        }

        //[Browsable(false)]
        [Appearance("DiscountLineTDiscEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TDisc
        {
            get
            {
                if (this._disc > 0 && this._disc != null)
                {
                    return GetSumTotalDisc();
                }
                else
                {
                    return 0;
                }
            }
        }

        [Appearance("DiscountLineDiscAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double DiscAmount
        {
            get { return _discAmount; }
            set
            {
                SetPropertyValue("DiscAmount", ref _discAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._discAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.DiscountLine, FieldName.DiscAmount) == true)
                        {
                            this._discAmount = _globFunc.GetRoundUp(Session, this._discAmount, ObjectList.DiscountLine, FieldName.DiscAmount);
                        }
                    }
                }
            }
        }

        //[Browsable(false)]
        [Appearance("DiscountLineTDiscAmountEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TDiscAmount
        {
            get
            {
                if (this._disc > 0 && this._discAmount > 0)
                {
                    return GetSumTotalDiscAmount();
                }
                else
                {
                    return 0;
                }
            }
        }

        [Browsable(false)]
        [Appearance("DiscountLineActiveClose", Enabled = false)]
        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        [Browsable(false)]
        [Appearance("DiscountLineDefaultClose", Enabled = false)]
        public bool Default
        {
            get { return _default; }
            set { SetPropertyValue("Default", ref _default, value); }
        }

        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Appearance("DiscountLinePurchaseOrderLineEnabled", Enabled = false)]
        [Association("PurchaseOrderLine-DiscountLines")]
        public PurchaseOrderLine PurchaseOrderLine
        {
            get { return _purchaseOrderLine; }
            set
            {
                SetPropertyValue("PurchaseOrderLine", ref _purchaseOrderLine, value);
                if (!IsLoading)
                {
                    if (this._purchaseOrderLine != null)
                    {
                        this.UAmount = this._purchaseOrderLine.UAmount;
                        this.TUAmount = this._purchaseOrderLine.TUAmount;
                    }
                }
            }
        }

        [Appearance("DiscountLinePurchaseInvoiceLineEnabled", Enabled = false)]
        [Association("PurchaseInvoiceLine-DiscountLines")]
        public PurchaseInvoiceLine PurchaseInvoiceLine
        {
            get { return _purchaseInvoiceLine; }
            set { SetPropertyValue("PurchaseInvoiceLine", ref _purchaseInvoiceLine, value); }
        }

        [Appearance("DiscountLineSalesOrderLineEnabled", Enabled = false)]
        [Association("SalesOrderLine-DiscountLines")]
        public SalesOrderLine SalesOrderLine
        {
            get { return _salesOrderLine; }
            set
            {
                SetPropertyValue("SalesOrderLine", ref _salesOrderLine, value);
                if (!IsLoading)
                {
                    if (this._salesOrderLine != null)
                    {
                        this.UAmount = this._salesOrderLine.UAmount;
                        this.TUAmount = this._salesOrderLine.TUAmount;
                    }
                }
            }
        }

        [Appearance("DiscountLineSalesQuotationLineEnabled", Enabled = false)]
        [Association("SalesQuotationLine-DiscountLines")]
        public SalesQuotationLine SalesQuotationLine
        {
            get { return _salesQuotationLine; }
            set
            {
                SetPropertyValue("SalesQuotationLine", ref _salesQuotationLine, value);
                if (!IsLoading)
                {
                    if (this._salesQuotationLine != null)
                    {
                        this.UAmount = this._salesQuotationLine.UAmount;
                        this.TUAmount = this._salesQuotationLine.TUAmount;
                    }
                }
            }
        }

        [Appearance("DiscountLineSalesInvoiceLineEnabled", Enabled = false)]
        [Association("SalesInvoiceLine-DiscountLines")]
        public SalesInvoiceLine SalesInvoiceLine
        {
            get { return _salesInvoiceLine; }
            set { SetPropertyValue("SalesInvoiceLine", ref _salesInvoiceLine, value); }
        }

        #endregion Field

        //===== Code Only =====

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.Discount != null)
                    {
                        object _makRecord = Session.Evaluate<DiscountLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("Discount=?", this.Discount));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DiscountLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.Discount != null)
                {
                    Tax _numHeader = Session.FindObject<Tax>
                                                (new BinaryOperator("Code", this.Discount.Code));

                    XPCollection<DiscountLine> _numLines = new XPCollection<DiscountLine>
                                                (Session, new BinaryOperator("Discount", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (DiscountLine _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DiscountLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.Discount != null)
                {
                    Tax _numHeader = Session.FindObject<Tax>
                                                (new BinaryOperator("Code", this.Discount.Code));

                    XPCollection<TaxLine> _numLines = new XPCollection<TaxLine>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("Discount", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (TaxLine _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DiscountLine " + ex.ToString());
            }
        }

        #endregion No

        #region Get

        public string GetName()
        {
            string _result = "";
            try
            {
                if (this._discount != null)
                {
                    string _result1 = null;
                    string _result2 = null;
                    if (this._discount.Name != null)
                    {
                        _result1 = this._discount.Name;
                    }
                    if (this._discount.Description != null)
                    {
                        _result2 = this._discount.Description;
                    }
                    _result = _result1 + " " + _result2;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DiscountLine " + ex.ToString());
            }
            return _result;
        }

        private void SetDiscAmount()
        {
            try
            {
                if (_globFunc.GetRoundingList(Session, ObjectList.DiscountLine, FieldName.DiscAmount) == true)
                {
                    this.DiscAmount = _globFunc.GetRoundUp(Session, (this.TUAmount * this.Disc / 100), ObjectList.DiscountLine, FieldName.DiscAmount);
                }
                else
                {
                    this.DiscAmount = this.TUAmount * this.Disc / 100;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DiscountLine " + ex.ToString());
            }
        }

        private double GetSumTotalDisc()
        {
            double _result = 0;
            try
            {
                double _locTotalDisc = 0;
                double _locTotalDisc2 = 0;
                if (!IsLoading)
                {
                    XPCollection<DiscountLine> _locDiscountLines = new XPCollection<DiscountLine>(Session,
                                                                   new BinaryOperator("PurchaseOrderLine", this.PurchaseOrderLine));

                    XPCollection<DiscountLine> _locDiscountLines2 = new XPCollection<DiscountLine>(Session,
                                                                   new BinaryOperator("SalesOrderLine", this.SalesOrderLine));

                    if (_locDiscountLines.Count() > 0)
                    {
                        foreach (DiscountLine _locDiscountLine in _locDiscountLines)
                        {
                            _locTotalDisc = _locTotalDisc + _locDiscountLine.Disc;
                        }
                        _result = _locTotalDisc;
                    }

                    if (_locDiscountLines2.Count() > 0)
                    {
                        foreach (DiscountLine _locDiscountLine2 in _locDiscountLines2)
                        {
                            _locTotalDisc2 = _locTotalDisc2 + _locDiscountLine2.Disc;
                        }
                        _result = _locTotalDisc2;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = DiscountLine" + ex.ToString());
            }
            return _result;
        }

        private double GetSumTotalDiscAmount()
        {
            double _result = 0;
            try
            {
                double _locTotalDiscOfPrice = 0;
                double _locTotalDiscOfPrice2 = 0;
                if (!IsLoading)
                {
                    XPCollection<DiscountLine> _locDiscountLines = new XPCollection<DiscountLine>(Session,
                                                                   new BinaryOperator("PurchaseOrderLine", this.PurchaseOrderLine));

                    XPCollection<DiscountLine> _locDiscountLines2 = new XPCollection<DiscountLine>(Session,
                                                                   new BinaryOperator("SalesOrderLine", this.SalesOrderLine));

                    if (_locDiscountLines.Count() > 0)
                    {
                        foreach (DiscountLine _locDiscountLine in _locDiscountLines)
                        {
                            _locTotalDiscOfPrice = _locTotalDiscOfPrice + _locDiscountLine.DiscAmount;
                        }
                        _result = _locTotalDiscOfPrice;
                    }

                    if (_locDiscountLines2.Count() > 0)
                    {
                        foreach (DiscountLine _locDiscountLine2 in _locDiscountLines2)
                        {
                            _locTotalDiscOfPrice2 = _locTotalDiscOfPrice2 + _locDiscountLine2.DiscAmount;
                        }
                        _result = _locTotalDiscOfPrice2;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = DiscountLine" + ex.ToString());
            }
            return _result;
        }

        #endregion Get

    }
}