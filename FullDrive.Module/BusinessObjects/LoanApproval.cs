﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;
using DevExpress.ExpressApp.ConditionalAppearance;
using System.Collections;
using FullDrive.Module.CustomProcess;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Loan")]
    [RuleCombinationOfPropertiesIsUnique("LoanApprovalRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class LoanApproval : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
      //private 
        private string _code;
        private SalaryComponent _salaryComponent;
        private DateTime _maxPaymentTime;
        private double _monthlyDeduction;
        private double _approvedValue;
        private string _notes;
        private LoanStatus _loanStatus;
        private Employee _approvedBy;
        private LoanProposal _loanCode;
        private Employee _employee;
        private Period _salaryPeriod;
        private GlobalFunction _globFunc;
        public LoanApproval(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                this._globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.LoanApproval);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Association("Period-LoanApprovals")]
        public Period Period
        {
            get { return _salaryPeriod; }
            set { SetPropertyValue("Period", ref _salaryPeriod, value); }
        }

        [Association("Employee-LoanApprovals")]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        public LoanProposal LoanCode
        {
            get { return _loanCode; }
            set { SetPropertyValue("LoanCode", ref _loanCode, value); }
        }

        public Employee ApprovedBy
        {
            get { return _approvedBy; }
            set { SetPropertyValue("ApprovedBy", ref _approvedBy, value); }
        }

        public LoanStatus LoanStatus
        {
            get { return _loanStatus; }
            set { SetPropertyValue("LoanStatus", ref _loanStatus, value); }
        }

        public string Notes
        {
            get { return _notes; }
            set { SetPropertyValue("Notes", ref _notes, value); }
        }

        public double MonthlyDeduction
        {
            get { return _monthlyDeduction; }
            set { SetPropertyValue("MonthlyDeduction", ref _monthlyDeduction, value); }
        }

        public double ApprovedValue
        {
            get { return _approvedValue; }
            set { SetPropertyValue("ApprovedValue", ref _approvedValue, value); }
        }

        public DateTime MaxPaymentTime
        {
            get { return _maxPaymentTime; }
            set { SetPropertyValue("MaxPaymentTime", ref _maxPaymentTime, value); }
        }

        public SalaryComponent SalaryComponent
        {
            get { return _salaryComponent; }
            set { SetPropertyValue("SalaryComponent", ref _salaryComponent, value); }
        }
        
        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}
    }
}