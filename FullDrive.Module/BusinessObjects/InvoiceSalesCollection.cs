﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("InvoiceSalesCollectionRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class InvoiceSalesCollection : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        private bool _activationPosting;
        private string _code;
        private XPCollection<SalesOrder> _availableSalesOrder;
        private SalesOrder _salesOrder;
        private XPCollection<InventoryTransferOut> _availableInventoryTransferOut;
        private InventoryTransferOut _inventoryTransferOut;
        private SalesInvoice _salesInvoice;
        private Status _status;
        private DateTime _statusDate;
        private int _postedCount;
        private GlobalFunction _globFunc;
        public InvoiceSalesCollection(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.InvoiceSalesCollection);
                DateTime now = DateTime.Now;
                this.Status = Status.Open;
                this.StatusDate = now;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("InvoiceSalesCollectionCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Browsable(false)]
        public XPCollection<SalesOrder> AvailableSalesOrder
        {
            get
            {
                if (!IsLoading)
                {
                    PriceGroup _locPriceGroup = null;
                    if (this.SalesInvoice != null)
                    {
                        if (this.SalesInvoice.SalesToCostumer != null)
                        {
                            if(this.SalesInvoice.PriceGroup != null)
                            {
                                _locPriceGroup = this.SalesInvoice.PriceGroup;
                            }
                            XPCollection<SalesOrder> _locCollectionSOs = new XPCollection<SalesOrder>
                                                                            (Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("ActiveApproved3", true),
                                                                            new BinaryOperator("SalesToCostumer", SalesInvoice.SalesToCostumer),
                                                                            new BinaryOperator("PriceGroup", _locPriceGroup),
                                                                            new GroupOperator(GroupOperatorType.Or,
                                                                            new BinaryOperator("Status", Status.Progress),
                                                                            new BinaryOperator("Status", Status.Posted)))
                                                                            );

                            if (_locCollectionSOs != null && _locCollectionSOs.Count() > 0)
                            {
                                _availableSalesOrder = _locCollectionSOs;
                            }
                        }
                    }
                }
                return _availableSalesOrder;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableSalesOrder", DataSourcePropertyIsNullMode.SelectNothing)]
        public SalesOrder SalesOrder
        {
            get { return _salesOrder; }
            set { SetPropertyValue("SalesOrder", ref _salesOrder, value); }
        }

        public XPCollection<InventoryTransferOut> AvailableInventoryTransferOut
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (!IsLoading)
                {
                    if (this.SalesInvoice != null && this.SalesInvoice.PriceGroup != null)
                    {
                        if (this.SalesInvoice.SalesToCostumer != null)
                        {
                            XPQuery<InventoryTransferOutMonitoring> _inventoryTransferOutMonitoringsQuery = new XPQuery<InventoryTransferOutMonitoring>(Session);

                            var _inventoryTransferOutMonitorings = from itom in _inventoryTransferOutMonitoringsQuery
                                                         where (itom.Status == Status.Open && itom.PostedCount == 0
                                                         && itom.InventoryTransferOut.PriceGroup == this.SalesInvoice.PriceGroup
                                                         && itom.InventoryTransferOut.BusinessPartner == this.SalesInvoice.SalesToCostumer
                                                         && itom.SalesInvoice == null)
                                                         group itom by itom.InventoryTransferOut into g
                                                         select new { InventoryTransferOut = g.Key };

                            if (_inventoryTransferOutMonitorings != null && _inventoryTransferOutMonitorings.Count() > 0)
                            {
                                List<string> _stringITOM = new List<string>();

                                foreach (var _inventoryTransferOutMonitoring in _inventoryTransferOutMonitorings)
                                {
                                    if (_inventoryTransferOutMonitoring != null)
                                    {
                                        _stringITOM.Add(_inventoryTransferOutMonitoring.InventoryTransferOut.Code);
                                    }
                                }

                                IEnumerable<string> _stringArrayITODistinct = _stringITOM.Distinct();
                                string[] _stringArrayITOMList = _stringArrayITODistinct.ToArray();
                                if (_stringArrayITOMList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayITOMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayITOMList[i] + "'";
                                        }
                                    }
                                }
                                else if (_stringArrayITOMList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayITOMList.Length; i++)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _stringArrayITOMList[i] + "'";
                                        }
                                        else
                                        {
                                            _endString = _endString + " OR [Code]=='" + _stringArrayITOMList[i] + "'";
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availableInventoryTransferOut = new XPCollection<InventoryTransferOut>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                        }    
                    }
                }
                return _availableInventoryTransferOut;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableInventoryTransferOut", DataSourcePropertyIsNullMode.SelectNothing)]
        public InventoryTransferOut InventoryTransferOut
        {
            get { return _inventoryTransferOut; }
            set { SetPropertyValue("InventoryTransferOut", ref _inventoryTransferOut, value); }
        }

        [ImmediatePostData()]
        [Appearance("InvoiceSalesCollectionSalesInvoiceClose", Enabled = false)]
        [Association("SalesInvoice-InvoiceSalesCollections")]
        public SalesInvoice SalesInvoice
        {
            get { return _salesInvoice; }
            set { SetPropertyValue("SalesInvoice", ref _salesInvoice, value); }
        }

        [Appearance("InvoiceSalesCollectionStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("InvoiceSalesCollectionStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("InvoiceSalesCollectionPostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }
    }
}