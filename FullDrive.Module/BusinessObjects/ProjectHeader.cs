﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Project Transaction")]
    [RuleCombinationOfPropertiesIsUnique("ProjectHeaderRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class ProjectHeader : FullDriveSysBaseObject
    {
        #region Default

        private bool _activationPosting;
        private string _code;
        private string _name;
        private BusinessPartner _customer;
        private DateTime _startDate;
        private DateTime _endDate;
        private string _description;
        private FileData _attachment;
        //private double _approximateAmount;
        //private double _offerAmount;
        private Status _status;
        private DateTime _statusDate;
        private string _userAccess;
        private Company _company;
        private GlobalFunction _globFunc;

        public ProjectHeader(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ProjectHeader);
                DateTime now = DateTime.Now;
                this.Status = CustomProcess.Status.Open;
                this.StatusDate = now;

                _userAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                if (Session.IsNewObject(this))
                {
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null)
                            {
                                Company = _locUserAccess.Employee.Company;
                            }
                        }
                    }
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #endregion Default

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ProjectHeaderCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("ProjectHeaderNameLock", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [DataSourceCriteria("Active = true")]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ProjectHeaderCustomerLock", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner Customer
        {
            get { return _customer; }
            set { SetPropertyValue("Customer", ref _customer, value); }
        }

        [Appearance("ProjectHeaderStartDateLock", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime StartDate
        {
            get { return _startDate; }
            set { SetPropertyValue("StartDate", ref _startDate, value); }
        }

        [Appearance("ProjectHeaderEndDateLock", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime EndDate
        {
            get { return _endDate; }
            set { SetPropertyValue("EndDate", ref _endDate, value); }
        }

        [Appearance("ProjectHeaderDescriptionLock", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Appearance("ProjectHeaderAttachmentLock", Criteria = "ActivationPosting = true", Enabled = false)]
        public FileData Attachment
        {
            get { return _attachment; }
            set { SetPropertyValue("Attachment", ref _attachment, value); }
        }

        //[Appearance("ProjectHeaderApproximateAmountClose", Enabled = false)]
        //public double ApproximateAmount
        //{
        //    get { return _approximateAmount; }
        //    set { SetPropertyValue("ApproximateAmount", ref _approximateAmount, value); }
        //}

        //[Appearance("ProjectHeaderOfferAmountClose", Enabled = false)]
        //public double OfferAmount
        //{
        //    get { return _offerAmount; }
        //    set { SetPropertyValue("OfferAmount", ref _offerAmount, value); }
        //}

        [Appearance("ProjectHeaderStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("ProjectHeaderStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("ProjectHeaderCompanyLock", Criteria = "ActivationPosting = true", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Association("ProjectHeader-ProjectLines")]
        public XPCollection<ProjectLine> ProjectLines
        {
            get { return GetCollection<ProjectLine>("ProjectLines"); }
        }

        [Association("ProjectHeader-ProjectLine2s")]
        public XPCollection<ProjectLine2> ProjectLine2s
        {
            get { return GetCollection<ProjectLine2>("ProjectLine2s"); }
        }

        [Association("ProjectHeader-PrePurchaseOrders")]
        public XPCollection<PrePurchaseOrder> PrePurchaseOrders
        {
            get { return GetCollection<PrePurchaseOrder>("PrePurchaseOrders"); }
        }

        [Association("ProjectHeader-PrePurchaseOrder2s")]
        public XPCollection<PrePurchaseOrder2> PrePurchaseOrder2s
        {
            get { return GetCollection<PrePurchaseOrder2>("PrePurchaseOrder2s"); }
        }

        [Association("ProjectHeader-PurchaseOrders")]
        public XPCollection<PurchaseOrder> PurchaseOrders
        {
            get { return GetCollection<PurchaseOrder>("PurchaseOrders"); }
        }

        [Association("ProjectHeader-InventoryTransferIns")]
        public XPCollection<InventoryTransferIn> InventoryTransferIns
        {
            get { return GetCollection<InventoryTransferIn>("InventoryTransferIns"); }
        }

        [Association("ProjectHeader-InventoryTransferOuts")]
        public XPCollection<InventoryTransferOut> InventoryTransferOuts
        {
            get { return GetCollection<InventoryTransferOut>("InventoryTransferOuts"); }
        }

        [Association("ProjectHeader-PurchaseRequisitions")]
        public XPCollection<PurchaseRequisition> PurchaseRequisitions
        {
            get { return GetCollection<PurchaseRequisition>("PurchaseRequisitions"); }
        }

        [Association("ProjectHeader-PurchaseInvoices")]
        public XPCollection<PurchaseInvoice> PurchaseInvoices
        {
            get { return GetCollection<PurchaseInvoice>("PurchaseInvoices"); }
        }

        [Association("ProjectHeader-TransferOrders")]
        public XPCollection<TransferOrder> TransferOrders
        {
            get { return GetCollection<TransferOrder>("TransferOrders"); }
        }

        [Association("ProjectHeader-InventoryTransferOrders")]
        public XPCollection<InventoryTransferOrder> InventoryTransferOrders
        {
            get { return GetCollection<InventoryTransferOrder>("InventoryTransferOrders"); }
        }

        [Association("ProjectHeader-TransferOuts")]
        public XPCollection<TransferOut> TransferOuts
        {
            get { return GetCollection<TransferOut>("TransferOuts"); }
        }

        [Association("ProjectHeader-TransferIns")]
        public XPCollection<TransferIn> TransferIns
        {
            get { return GetCollection<TransferIn>("TransferIns"); }
        }

        [Association("ProjectHeader-StockTransfers")]
        public XPCollection<StockTransfer> StockTransfers
        {
            get { return GetCollection<StockTransfer>("StockTransfers"); }
        }

        [Association("ProjectHeader-PurchaseReturns")]
        public XPCollection<PurchaseReturn> PurchaseReturns
        {
            get { return GetCollection<PurchaseReturn>("PurchaseReturns"); }
        }

        [Association("ProjectHeader-PreSalesOrders")]
        public XPCollection<PreSalesOrder> PreSalesOrders
        {
            get { return GetCollection<PreSalesOrder>("PreSalesOrders"); }
        }

        [Association("ProjectHeader-PreSalesOrder2s")]
        public XPCollection<PreSalesOrder2> PreSalesOrder2s
        {
            get { return GetCollection<PreSalesOrder2>("PreSalesOrder2s"); }
        }

        [Association("ProjectHeader-SalesOrders")]
        public XPCollection<SalesOrder> SalesOrders
        {
            get { return GetCollection<SalesOrder>("SalesOrders"); }
        }

        [Association("ProjectHeader-SalesInvoices")]
        public XPCollection<SalesInvoice> SalesInvoices
        {
            get { return GetCollection<SalesInvoice>("SalesInvoices"); }
        }
         
        [Association("ProjectHeader-SalesQuotations")]
        public XPCollection<SalesQuotation> SalesQuotations
        {
            get { return GetCollection<SalesQuotation>("SalesQuotations"); }
        }

        #region CLM

        [Association("ProjectHeader-SalesReturns")]
        public XPCollection<SalesReturn> SalesReturns
        {
            get { return GetCollection<SalesReturn>("SalesReturns"); }
        }

        [Persistent("GrandTotalRAB")]
        public double GrandTotalRAB
        {
            get
            {
                double _result = 0;
                int _totalLineProjectLine = 0;
                if (!IsLoading)
                {
                    _totalLineProjectLine = Convert.ToInt32(Session.Evaluate<ProjectHeader>(CriteriaOperator.Parse("ProjectLines.Count"), CriteriaOperator.Parse("Oid=?", Oid)));
                    if (_totalLineProjectLine > 0)
                    {
                        _result = UpdateGrandTotalRAB(true);
                    }
                }
                return _result;
            }
        }

        [Persistent("GrandTotalContract")]
        public double GrandTotalContract
        {
            get
            {
                double _result = 0;
                int _totalLineProjectLine2 = 0;
                if (!IsLoading)
                {
                    _totalLineProjectLine2 = Convert.ToInt32(Session.Evaluate<ProjectHeader>(CriteriaOperator.Parse("ProjectLines.Count"), CriteriaOperator.Parse("Oid=?", Oid)));
                    if (_totalLineProjectLine2 > 0)
                    {
                        _result = UpdateGrandTotalContract(true);
                    }
                }
                return _result;
            }
        }

        #endregion CLM

        #endregion Field

        #region CLM

        public double UpdateGrandTotalRAB(bool forceChangeEvents)
        {
            double _result = 0;
            try
            {
                XPCollection<ProjectLine> _locProjectLines = new XPCollection<ProjectLine>(Session,
                                                             new GroupOperator(GroupOperatorType.And,
                                                             new BinaryOperator("ProjectHeader", this)));

                if (_locProjectLines != null && _locProjectLines.Count > 0)
                {
                    if (_locProjectLines.Count > 0)
                    {
                        foreach (ProjectLine _locProjectLine in _locProjectLines)
                        {
                            if (forceChangeEvents)
                            {
                                if (_locProjectLine.SumTotalPLI2 >= 0 && _locProjectLine.SumTotalPLS >= 0)
                                {
                                    _result = _locProjectLine.SumTotalPLI2 + _locProjectLine.SumTotalPLS;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ProjectHeader ", ex.ToString());
            }
            return _result;
        }

        public double UpdateGrandTotalContract(bool forceChangeEvents)
        {
            double _result = 0;
            try
            {
                XPCollection<ProjectLine2> _locProjectLines2 = new XPCollection<ProjectLine2>(Session,
                                                               new GroupOperator(GroupOperatorType.And,
                                                               new BinaryOperator("ProjectHeader", this)));

                if (_locProjectLines2 != null && _locProjectLines2.Count > 0)
                {
                    if (_locProjectLines2.Count > 0)
                    {
                        foreach (ProjectLine2 _locProjectLine2 in _locProjectLines2)
                        {
                            if (forceChangeEvents)
                            {
                                if (_locProjectLine2.SumTotalPL2I2 >= 0 && _locProjectLine2.SumTotalPL2S >= 0)
                                {
                                    _result = _locProjectLine2.SumTotalPL2I2 + _locProjectLine2.SumTotalPL2S;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ProjectHeader ", ex.ToString());
            }
            return _result;
        }

        #endregion CLM

    }
}