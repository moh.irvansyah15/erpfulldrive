﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("CreditMemoRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class CreditMemo : FullDriveSysBaseObject
    {
        #region Default

        private bool _activationPosting;
        private string _code;
        private BusinessPartner _buyFromVendor;
        private string _buyFromContact;
        private Country _buyFromCountry;
        private City _buyFromCity;
        private string _buyFromAddress;
        private TermOfPayment _top;
        private Status _status;
        private DateTime _statusDate;
        private string _signCode;
        private PurchaseReturn _purchaseReturn;
        private PurchaseOrder _purchaseOrder;
        private InventoryTransferOut _inventoryTransferOut;
        private GlobalFunction _globFunc;

        public CreditMemo(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.CreditMemo);
                this.Status = Status.Open;
                this.StatusDate = now;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #endregion Default

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("CreditMemoCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("CreditMemoBuyFromVendorClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner BuyFromVendor
        {
            get { return _buyFromVendor; }
            set { SetPropertyValue("BuyFromVendor", ref _buyFromVendor, value); }
        }

        [Appearance("CreditMemoBuyFromContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string BuyFromContact
        {
            get { return _buyFromContact; }
            set { SetPropertyValue("BuyFromContact", ref _buyFromContact, value); }
        }

        [Appearance("CreditMemoBuyFromCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country BuyFromCountry
        {
            get { return _buyFromCountry; }
            set { SetPropertyValue("BuyFromCountry", ref _buyFromCountry, value); }
        }

        [Appearance("CreditMemoBuyFromBuyFromCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City BuyFromCity
        {
            get { return _buyFromCity; }
            set { SetPropertyValue("BuyFromCity", ref _buyFromCity, value); }
        }

        [Size(512)]
        [Appearance("CreditMemoBuyFromAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string BuyFromAddress
        {
            get { return _buyFromAddress; }
            set { SetPropertyValue("BuyFromAddress", ref _buyFromAddress, value); }
        }

        [Appearance("CreditMemoBuyFromTOPClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public TermOfPayment TOP
        {
            get { return _top; }
            set { SetPropertyValue("TOP", ref _top, value); }
        }

        [Appearance("CreditMemoStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("CreditMemoStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("CreditMemoSignCodeEnabled", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [Appearance("CreditMemoPurchaseReturnClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public PurchaseReturn PurchaseReturn
        {
            get { return _purchaseReturn; }
            set
            {
                SetPropertyValue("PurchaseReturn", ref _purchaseReturn, value);
                if (!IsLoading)
                {
                    if (this._purchaseReturn != null)
                    {
                        if (this._purchaseReturn.PurchaseOrder != null)
                        {
                            this.PurchaseOrder = _purchaseReturn.PurchaseOrder;
                        }
                    }
                }
            }
        }

        [Appearance("CreditMemoPurchaseOrderClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PurchaseOrder PurchaseOrder
        {
            get { return _purchaseOrder; }
            set { SetPropertyValue("PurchaseOrder", ref _purchaseOrder, value); }
        }

        [Appearance("CreditMemoInventoryTransferClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public InventoryTransferOut InventoryTransferOut
        {
            get { return _inventoryTransferOut; }
            set { SetPropertyValue("InventoryTransferOut", ref _inventoryTransferOut, value); }
        }

        [Association("CreditMemo-CreditMemoLines")]
        public XPCollection<CreditMemoLine> CreditMemoLines
        {
            get { return GetCollection<CreditMemoLine>("CreditMemoLines"); }
        }

        #region CLM

        [Persistent("GrandTotalCM")]
        public double GrandTotalCm
        {
            get
            {
                double _result = 0;
                int _totalLine = 0;
                if (!IsLoading)
                {
                    _totalLine = Convert.ToInt32(Session.Evaluate<CreditMemo>(CriteriaOperator.Parse("CreditMemoLines.Count"), CriteriaOperator.Parse("Oid=?", Oid)));
                    if (_totalLine > 0)
                    {
                        _result = UpdateGrandTotalCM(true);
                    }
                }
                return _result;
            }
        }

        #endregion CLM

        #endregion Field

        //==== Code Only =====

        #region CLM

        public double UpdateGrandTotalCM(bool forceChangeEvents)
        {
            double _result = 0;
            try
            {
                XPCollection<CreditMemoLine> _locCreditMemoLines = new XPCollection<CreditMemoLine>(Session,
                                                                         new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("CreditMemo", this)));

                if (_locCreditMemoLines != null && _locCreditMemoLines.Count > 0)
                {
                    if (_locCreditMemoLines.Count > 0)
                    {
                        foreach (CreditMemoLine _locCreditMemoLine in _locCreditMemoLines)
                        {
                            if (forceChangeEvents)
                            {
                                if (_locCreditMemoLine.TGrandAmountCML >= 0)
                                {
                                    _result = _locCreditMemoLine.TGrandAmountCML;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CreditMemo", ex.ToString());
            }
            return _result;
        }

        #endregion CLm

    }
}