﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Quality Assurance")]
    [RuleCombinationOfPropertiesIsUnique("DesignSheetLineRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class DesignSheetLine : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private int _no;
        private string _code;
        private CoreNumber _coreNumber;
        private CrossSectionArea _crossSectionArea;
        private ShapeOfConductor _shapeOfConductor;
        #region Wire
        private WireNumber _wireNumber;
        private string _wireLine;
        private double _wireFactor;
        private double _wireValue;
        private double _wireDiameterMin;
        private double _wireDiameterMax;
        #endregion Wire
        #region Conductor        
        private double _conductorDiameterMin;
        private double _conductroDiameterMax;
        #endregion Conductor
        #region Insulation
        private double _thickInsulationMin;
        private double _thickInsulationMax;
        private double _insulationDiameterMin;
        private double _insulationDiameterMax;
        #endregion Insulation
        #region Weight
        private double _qtyConductor;
        private double _conductorPercore;
        private double _qtyInsulation;
        private double _insulationPercore;
        private double _conductorWeight;
        private double _insulationWeight;
        private double _totalWeight;
        #endregion Weight
        private DesignSheet _designSheet;
        private GlobalFunction _globFunc;

        public DesignSheetLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if(!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.DesignSheetLine);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            RecoveryDeleteNo();
        }

        [Appearance("DesignSheetLineNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [Appearance("DesignSheetLineCodeEnabled", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        public CoreNumber CoreNumber
        {
            get { return _coreNumber; }
            set { SetPropertyValue("CoreNumber", ref _coreNumber, value); }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        public CrossSectionArea CrossSectionArea
        {
            get { return _crossSectionArea; }
            set { SetPropertyValue("CrossSectionArea", ref _crossSectionArea, value); }
        }

        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        public ShapeOfConductor ShapeOfConductor
        {
            get { return _shapeOfConductor; }
            set
            {
                SetPropertyValue("ShapeOfConductor", ref _shapeOfConductor, value);
                if (!IsLoading)
                {
                    if (this._shapeOfConductor != null)
                    {
                        SetWireFactor();
                    }
                }
            }
        }

        #region Deklarasi Wire
        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        public WireNumber WireNumber
        {
            get { return _wireNumber; }
            set
            {
                SetPropertyValue("WireNumber", ref _wireNumber, value);
                if(!IsLoading)
                {
                    if(this._wireNumber != null)
                    {
                        SetWireFactor();
                    }
                    if(this._wireNumber.Value != null)
                    {
                        this.WireValue = this._wireNumber.Value;
                    }
                }
            }
        }

        [Appearance("DesignSheetLineWireLineEnabled", Enabled = false)]
        public string WireLine
        {
            get { return _wireLine; }
            set { SetPropertyValue("WireLine", ref _wireLine, value); }
        }

        [Browsable(false)]
        public double WireFactor
        {
            get { return _wireFactor; }
            set { SetPropertyValue("WireFactor", ref _wireFactor, value); }
        }

        [Browsable(false)]
        public double WireValue
        {
            get { return _wireValue; }
            set { SetPropertyValue("WireValue", ref _wireValue, value); }
        }

        [ImmediatePostData]
        public double WireDiameterMin
        {
            get { return _wireDiameterMin; }
            set
            {
                SetPropertyValue("WireDiameterMin", ref _wireDiameterMin, value);
                if(!IsLoading)
                {
                    SetConductorDiameterMin();
                }
            }
        }

        [ImmediatePostData]
        public double WireDiameterMax
        {
            get { return _wireDiameterMax; }
            set
            {
                SetPropertyValue("WireDiameterMax", ref _wireDiameterMax, value);
                if (!IsLoading)
                {
                    SetConductorDiameterMax();
                    SetConductorPercore();
                }
            }
        }
        #endregion Deklarasi Wire

        #region Deklarasi Conductor
        [Appearance("DesignSheetLineConductorDiameterMinEnabled", Enabled = false)]
        public double ConductorDiameterMin
        {
            get { return _conductorDiameterMin; }
            set { SetPropertyValue("ConductorDiameterMin", ref _conductorDiameterMin, value); }
        }

        [Appearance("DesignSheetLineConductorDiameterMaxEnabled", Enabled = false)]
        public double ConductorDiameterMax
        {
            get { return _conductroDiameterMax; }
            set { SetPropertyValue("ConductorDiameterMax", ref _conductroDiameterMax, value); }
        }
        #endregion Deklarasi Conductor

        #region Deklarasi Insulation
        [ImmediatePostData()]
        public double ThickInsulationMin
        {
            get { return _thickInsulationMin; }
            set
            {
                SetPropertyValue("ThickInsulationMin", ref _thickInsulationMin, value);
                if (!IsLoading)
                {
                    SetThickInsulationMax();
                    SetInsulationDiameterMin();
                    SetInsulationDiameterMax();
                    SetInsulationPercore();
                }
            }
        }

        [Appearance("DesignSheetLineThickInsulationMaxEnabled", Enabled = false)]
        public double ThickInsulationMax
        {
            get { return _thickInsulationMax; }
            set { SetPropertyValue("ThickInsulationMax", ref _thickInsulationMax, value); }
        }

        [Appearance("DesignSheetLineInsulationDiameterMinEnabled", Enabled = false)]
        public double InsulationDiameterMin
        {
            get { return _insulationDiameterMin; }
            set { SetPropertyValue("InsulationDiameterMin", ref _insulationDiameterMin, value); }
        }

        [Appearance("DesignSheetLineInsulationDiameterMaxEnabled", Enabled = false)]
        public double InsulationDiameterMax
        {
            get { return _insulationDiameterMax; }
            set { SetPropertyValue("InsulationDiameterMax", ref _insulationDiameterMax, value); }
        }
        #endregion Deklarasi Insulation

        #region Deklarasi Weight
        [ImmediatePostData()]
        public double QtyConductor
        {
            get { return _qtyConductor; }
            set
            {
                SetPropertyValue("QtyConductor", ref _qtyConductor, value);
                if(!IsLoading)
                {
                    SetConductorWeight();
                    SetTotalWeight();
                }
            }
        }

        [Appearance("DesignSheetLineConductorPercoreEnabled", Enabled = false)]
        public double ConductorPercore
        {
            get { return _conductorPercore; }
            set { SetPropertyValue("ConductorPercore", ref _conductorPercore, value); }
        }

        [Appearance("DesignSheetLineConductorWeightEnabled", Enabled = false)]
        public double ConductorWeight
        {
            get { return _conductorWeight; }
            set { SetPropertyValue("ConductorWeight", ref _conductorWeight, value); }
        }

        [ImmediatePostData()]
        public double QtyInsulation
        {
            get { return _qtyInsulation; }
            set
            {
                SetPropertyValue("QtyInsulation", ref _qtyInsulation, value);
                if(!IsLoading)
                {
                    SetInsulationWeight();
                    SetTotalWeight();
                }
            }
        }

        [Appearance("DesignSheetLineInsulationPercoreEnabled", Enabled = false)]
        public double InsulationPercore
        {
            get { return _insulationPercore; }
            set { SetPropertyValue("InsulationPercore", ref _insulationPercore, value); }
        }       

        [Appearance("DesignSheetLineInsulationWeightEnabled", Enabled = false)]
        public double InsulationWeight
        {
            get { return _insulationWeight; }
            set { SetPropertyValue("InsulationWeight", ref _insulationWeight, value); }
        }

        [Appearance("DesignSheetLineTotalWeightEnabled", Enabled = false)]
        public double TotalWeight
        {
            get { return _totalWeight; }
            set { SetPropertyValue("TotalWeight", ref _totalWeight, value); }
        }
        #endregion Deklarasi Weight

        [Association("DesignSheet-DesignSheetLines")]
        [Appearance("DesignSheetLineDesignSheetEnabled", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public DesignSheet DesignSheet
        {
            get { return _designSheet; }
            set
            {
                SetPropertyValue("DesignSheet", ref _designSheet, value);
                if(!IsLoading)
                {
                    if(this._designSheet != null)
                    {
                        this.CoreNumber = _designSheet.CoreNumber;
                        this.CrossSectionArea = _designSheet.CrossSectionArea;
                        this.ShapeOfConductor = _designSheet.ShapeOfConductor;
                    }
                }
            }
        }

        //[Association("DesignSheetLine-DesignSheetComponents")]
        //public XPCollection<DesignSheetComponent> DesignSheetComponents
        //{
        //    get { return GetCollection<DesignSheetComponent>("DesignSheetComponents"); }
        //}

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}

        //================================================== Code Only ==================================================

        #region Numbering
        public void UpdateNo()
        {
            try
            {
                if (!IsLoading)
                {
                    if (!(Session is NestedUnitOfWork) && Session.IsNewObject(this))
                    {
                        if (this.DesignSheet != null)
                        {
                            object _makRecord = Session.Evaluate<DesignSheetLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("DesignSheet=?", this.DesignSheet));
                            this.No = Convert.ToInt32(_makRecord) + 1;
                            this.Save();
                            RecoveryUpdateNo();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DesignSheetLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.DesignSheet != null)
                {
                    DesignSheet _numHeader = Session.FindObject<DesignSheet>
                                                (new BinaryOperator("Code", this.DesignSheet.Code));

                    XPCollection<DesignSheetLine> _numLines = new XPCollection<DesignSheetLine>
                                                (Session, new BinaryOperator("DesignSheet", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (DesignSheetLine _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                        //Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DesignSheetLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.DesignSheet != null)
                {
                    DesignSheet _numHeader = Session.FindObject<DesignSheet>
                                                (new BinaryOperator("Code", this.DesignSheet.Code));

                    XPCollection<DesignSheetLine> _numLines = new XPCollection<DesignSheetLine>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("DesignSheet", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (DesignSheetLine _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                        //Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DesignSheetLine " + ex.ToString());
            }
        }
        #endregion Numbering

        #region Wire
        private void SetWireFactor()
        {
            try
            {
                XPCollection<FormulaDesignSheetLine> _numLines = new XPCollection<FormulaDesignSheetLine>(Session,
                                                                 new GroupOperator(GroupOperatorType.And,
                                                                 new BinaryOperator("ShapeOfConductor", this.ShapeOfConductor),
                                                                 new BinaryOperator("WireNumber", this.WireNumber),
                                                                 new BinaryOperator("Active", true)));

                if (_numLines != null && _numLines.Count > 0)
                {
                    foreach (FormulaDesignSheetLine _nunLine in _numLines)
                    {
                        this.WireFactor = _nunLine.WireFactor;
                        this.WireLine = _nunLine.WireLine;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DesignSheetLine " + ex.ToString());
            }
        }
        #endregion Wire

        #region Conductor
        private void SetConductorDiameterMin()
        {
            try
            {
                this.ConductorDiameterMin = this._wireDiameterMin * this._wireFactor;
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DesignSheetLine " + ex.ToString());
            }
        }

        private void SetConductorDiameterMax()
        {
            try
            {
                this.ConductorDiameterMax = this._wireDiameterMax * this._wireFactor;
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DesignSheetLine " + ex.ToString());
            }
        }
        #endregion Conductor

        #region Insulation
        private void SetThickInsulationMax()
        {
            try
            {
                this.ThickInsulationMax = this.ThickInsulationMin + 0.05;
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DesignSheetLine " + ex.ToString());
            }
        }

        private void SetInsulationDiameterMin()
        {
            try
            {
                this.InsulationDiameterMin = (this.ConductorDiameterMin) + (2 * this.ThickInsulationMin);
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DesignSheetLine " + ex.ToString());
            }
        }

        private void SetInsulationDiameterMax()
        {
            try
            {
                this.InsulationDiameterMax = (this.ConductorDiameterMax) + (2 * (this.ThickInsulationMin + 0.05));
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DesignSheetLine " + ex.ToString());
            }
        }
        #endregion Insulation

        #region Weight
        private void SetConductorPercore()
        {
            try
            {
                this.ConductorPercore = 0.25 * 3.14 * ((this._wireDiameterMax * this._wireFactor) * (this._wireDiameterMax * this._wireFactor)) * 8.89 * 1.01;
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DesignSheetLine " + ex.ToString());
            }
        }

        private void SetInsulationPercore()
        {
            try
            {
                this.InsulationPercore = ((this._wireDiameterMax * this._wireFactor) + (this.ThickInsulationMin + 0.05)) * (this.ThickInsulationMin + 0.05) * 3.14 * 1.5 * 1.01;
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DesignSheetLine " + ex.ToString());
            }
        }

        private void SetConductorWeight()
        {
            try
            {
                this.ConductorWeight = this._qtyConductor * (0.25 * 3.14 * ((this._wireDiameterMax * this._wireFactor) * (this._wireDiameterMax * this._wireFactor)) * 8.89 * 1.01);
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DesignSheetLine " + ex.ToString());
            }
        }

        private void SetInsulationWeight()
        {
            try
            {
                this.InsulationWeight = this._qtyInsulation * (((this._wireDiameterMax * this._wireFactor) + (this.ThickInsulationMin + 0.05)) * (this.ThickInsulationMin + 0.05) * 3.14 * 1.5 * 1.01);
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DesignSheetLine " + ex.ToString());
            }
        }

        private void SetTotalWeight()
        {
            try
            {
                this.TotalWeight = (this._qtyConductor * (0.25 * 3.14 * ((this._wireDiameterMax * this._wireFactor) * (this._wireDiameterMax * this._wireFactor)) * 8.89 * 1.01))
                                   + (this._qtyInsulation * (((this._wireDiameterMax * this._wireFactor) + (this.ThickInsulationMin + 0.05)) * (this.ThickInsulationMin + 0.05) * 3.14 * 1.5 * 1.01));
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DesignSheetLine " + ex.ToString());
            }
        }
        #endregion Weight

    }
}