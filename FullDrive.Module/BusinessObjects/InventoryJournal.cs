﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#region Addon
//call folder
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;
#endregion Addon

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]

    #region Addon
    [DefaultProperty("Code")]
    [NavigationItem("Inventory")]
    [RuleCombinationOfPropertiesIsUnique("InventoryJournalRuleUnique", DefaultContexts.Save, "Code")]
    #endregion Addon

    public class InventoryJournal : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).

        #region Listfield
        private string _code;
        private DocumentType _documentType;
        private string _docNo;
        private Location _location;
        private BinLocation _binLocation;
        private StockType _stockType;
        private Item _item;
        private double _qtyPos;
        private double _qtyNeg;
        private UnitOfMeasure _dUom;
        private DateTime _journalDate;
        private string _lotNumber;
        private Company _company;
        private ProjectHeader _projectHeader;
        private InventoryTransferOut _inventoryTransferOut;
        private InventoryTransferIn _inventoryTransferIn;
        private TransferOut _transferOut;
        private TransferIn _transferIn;
        private CuttingProcessLot _cuttingProcessLot;
        private GlobalFunction _globFunc;
        #endregion Listfield

        public InventoryJournal(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();

            #region Numbering
            //auto number
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                //jangan lupa diubah dibelakang objectlist
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.InventoryJournal);
            }
            #endregion Numbering
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field
        //Validation enabled field
        [Appearance("InventoryJournalCodeEnabled", Enabled = false)]
        //Validation required
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        public DocumentType DocumentType
        {
            get { return _documentType; }
            set { SetPropertyValue("DocumentType", ref _documentType, value); }
        }     

        public string DocNo
        {
            get { return _docNo; }
            set { SetPropertyValue("DocNo", ref _docNo, value); }
        }

        [DataSourceCriteria("Active = true")]
        public Location Location
        {
            get { return _location; }
            set { SetPropertyValue("Location", ref _location, value); }
        }

        [DataSourceCriteria("Active = true")]
        public BinLocation BinLocation
        {
            get { return _binLocation; }
            set { SetPropertyValue("binLocation", ref _binLocation, value); }
        }

        public StockType StockType
        {
            get { return _stockType; }
            set { SetPropertyValue("StockType", ref _stockType, value); }
        }

        [DataSourceCriteria("Active = true")]
        public Item Item
        {
            get { return _item; }
            set { SetPropertyValue("Item", ref _item, value); }
        }

        public double QtyPos
        {
            get { return _qtyPos; }
            set { SetPropertyValue("QtyPos", ref _qtyPos, value); }
        }

        public double QtyNeg
        {
            get { return _qtyNeg; }
            set { SetPropertyValue("QtyNeg", ref _qtyNeg, value); }
        }

        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM
        {
            get { return _dUom; }
            set { SetPropertyValue("DUOM", ref _dUom, value); }
        }

        public DateTime JournalDate
        {
            get { return _journalDate; }
            set { SetPropertyValue("JournalDate", ref _journalDate, value); }
        }

        public string LotNumber
        {
            get { return _lotNumber; }
            set { SetPropertyValue("LotNumber", ref _lotNumber, value); }
        }

        //Validation active or nonactive
        [DataSourceCriteria("Active = true")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        public ProjectHeader ProjectHeader
        {
            get { return _projectHeader; }
            set { SetPropertyValue("ProjectHeader", ref _projectHeader, value); }
        }

        public InventoryTransferOut InventoryTransferOut
        {
            get { return _inventoryTransferOut; }
            set { SetPropertyValue("InventoryTransferOut", ref _inventoryTransferOut, value); }
        }

        public InventoryTransferIn InventoryTransferIn
        {
            get { return _inventoryTransferIn; }
            set { SetPropertyValue("InventoryTransferIn", ref _inventoryTransferIn, value); }
        }

        public TransferOut TransferOut
        {
            get { return _transferOut; }
            set { SetPropertyValue("TransferOut", ref _transferOut, value); }
        }

        public TransferIn TransferIn
        {
            get { return _transferIn; }
            set { SetPropertyValue("TransferIn", ref _transferIn, value); }
        }

        public CuttingProcessLot CuttingProcessLot
        {
            get { return _cuttingProcessLot; }
            set { SetPropertyValue("CuttingProcessLot", ref _cuttingProcessLot, value); }
        }
        #endregion Field
    }
}