﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.SystemModule;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Editors;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("ReceivableTransactionMonitoringRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class ReceivableTransactionMonitoring : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private ReceivableTransaction _receivableTransaction;
        private SalesInvoice _salesInvoice;
        private SalesOrder _salesOrder;
        private Currency _currency;
        private double _totAmountDebit;
        private double _totAmountCredit;
        private Status _status;
        private DateTime _statusDate;
        private int _postedCount;
        private string _signCode;
        private SalesInvoiceMonitoring _salesInvoiceMonitoring;
        private GlobalFunction _globFunc;

        public ReceivableTransactionMonitoring(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            _globFunc = new GlobalFunction();
            DateTime now = DateTime.Now;
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.SalesInvoiceMonitoring);
            this.Status = Status.Open;
            this.StatusDate = now;
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }
        #region Field
        [Appearance("ReceivableTransactionMonitoringCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("ReceivableTransactionMonitoringReceivableTransactionClose", Enabled = false)]
        public ReceivableTransaction ReceivableTransaction
        {
            get { return _receivableTransaction; }
            set { SetPropertyValue("ReceivableTransaction", ref _receivableTransaction, value); }
        }

        [Appearance("ReceivableTransactionMonitoringSalesInvoiceClose", Enabled = false)]
        public SalesInvoice SalesInvoice
        {
            get { return _salesInvoice; }
            set { SetPropertyValue("SalesInvoice", ref _salesInvoice, value); }
        }

        [Appearance("ReceivableTransactionMonitoringSalesOrderClose", Enabled = false)]
        public SalesOrder SalesOrder
        {
            get { return _salesOrder; }
            set { SetPropertyValue("SalesOrder", ref _salesOrder, value); }
        }

        [Appearance("ReceivableTransactionMonitoringCurrencyClose", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        [Appearance("ReceivableTransactionMonitoringTotAmountDebitClose", Enabled = false)]
        public double TotAmountDebit
        {
            get { return _totAmountDebit; }
            set { SetPropertyValue("TotAmountDebit", ref _totAmountDebit, value); }
        }

        [Appearance("ReceivableTransactionMonitoringTotAmountCreditClose", Enabled = false)]
        public double TotAmountCredit
        {
            get { return _totAmountCredit; }
            set { SetPropertyValue("TotAmountCredit", ref _totAmountCredit, value); }
        }

        [Appearance("ReceivableTransactionMonitoringStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("ReceivableTransactionMonitoringStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("ReceivableTransactionMonitoringPostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        [Appearance("ReceivableTransactionMonitoringSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [Appearance("ReceivableTransactionMonitoringSalesInvoiceMonitoringEnabled", Enabled = false)]
        public SalesInvoiceMonitoring SalesInvoiceMonitoring
        {
            get { return _salesInvoiceMonitoring; }
            set { SetPropertyValue("SalesInvoiceMonitoring", ref _salesInvoiceMonitoring, value); }
        }

        #endregion Field
    }
}