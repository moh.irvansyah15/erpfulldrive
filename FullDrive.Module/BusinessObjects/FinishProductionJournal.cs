﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Production")]
    [RuleCombinationOfPropertiesIsUnique("FinishProductionJournalRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class FinishProductionJournal : BaseObject
    {
        #region Default

        private string _code;
        private Location _location;
        private BinLocation _binLocation;
        private Item _item;
        private Company _company;
        private double _qtyFinish;
        private double _qtyActual;
        private double _qtyReturn;
        private UnitOfMeasure _dUOM;
        private FinishProduction _finishProduction;
        private DateTime _journalDate;
        private GlobalFunction _globFunc;

        public FinishProductionJournal(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.FinishProductionJournal);
            }
        }

        #endregion Default

        #region Field

        [Appearance("FinishProductionJournalCodeEnabled", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("FinishProductionJournalLocationEnabled", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Location Location
        {
            get { return _location; }
            set { SetPropertyValue("Location", ref _location, value); }
        }

        [Appearance("FinishProductionJournalBinLocationEnabled", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public BinLocation BinLocation
        {
            get { return _binLocation; }
            set { SetPropertyValue("binLocation", ref _binLocation, value); }
        }

        [Appearance("FinishProductionJournalItemEnabled", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Item Item
        {
            get { return _item; }
            set { SetPropertyValue("Item", ref _item, value); }
        }

        [Appearance("FinishProductionJournalQtyFinishEnabled", Enabled = false)]
        public double QtyFinish
        {
            get { return _qtyFinish; }
            set { SetPropertyValue("QtyFinish", ref _qtyFinish, value); }
        }

        [Appearance("FinishProductionJournalQtyActualEnabled", Enabled = false)]
        public double QtyActual
        {
            get { return _qtyActual; }
            set { SetPropertyValue("QtyActual", ref _qtyActual, value); }
        }

        [Appearance("FinishProductionJournalQtyReturnEnabled", Enabled = false)]
        public double QtyReturn
        {
            get { return _qtyReturn; }
            set { SetPropertyValue("QtyReturn", ref _qtyReturn, value); }
        }

        [Appearance("FinishProductionJournalDUOMEnabled", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM
        {
            get { return _dUOM; }
            set { SetPropertyValue("DUOM", ref _dUOM, value); }
        }

        [Appearance("FinishProductionJournalJournalDateEnabled", Enabled = false)]
        public DateTime JournalDate
        {
            get { return _journalDate; }
            set { SetPropertyValue("JournalDate", ref _journalDate, value); }
        }

        [Appearance("FinishProductionJournalCompanyEnabled", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("FinishProductionJournalFinishProductionEnabled", Enabled = false)]
        public FinishProduction FinishProduction
        {
            get { return _finishProduction; }
            set { SetPropertyValue("FinishProduction", ref _finishProduction, value); }
        }

        #endregion Field

    }
}