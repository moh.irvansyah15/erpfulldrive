﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;


namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("FullName")]
    [NavigationItem("Organization")]
    [RuleCombinationOfPropertiesIsUnique("EmployeeRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class Employee : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private string _name;
        private string _address;
        private string _email;
        private string _contact;
        private DateTime _birthday;
        private City _placeOfBirth;
        private GenderType _genderType;
        private Religion _religion;
        private BloodType _bloodType;
        private byte[] _photo;
        private Company _company;
        private City _city;
        private Country _country;
        private Division _division;
        private Department _department;
        private Section _section;
        private Position _position;
        private Branch _branch;
        private Location _location;
        private WorkingStatus _workingStatus;
        private DateTime _startDate;
        private DateTime _endDate;
        private string _npwp;
        private bool _isExPatriate;
        private Currency _currency;
        private TaxCalculationMethod _taxCalculationMethod;
        private NonTaxableIncomeType _nonTaxableIncomeStatus;
        private string _insuranceNumber;
        private string _BPJSTK;
        private DateTime _BPJSTKStartDate;
        private double _taxDeductionFromPreviousCompany;
        private Grade _grade;
        private SalaryGroup _salaryGroup;
        private bool _isImported;
        private string _taxStreet;
        private string _taxCity;
        private string _taxCountry;
        private string _remark;
        private bool _active;
        private GlobalFunction _globFunc;

        public Employee(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if(!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.Employee);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Size(512)]
        public string Address
        {
            get { return _address; }
            set { SetPropertyValue("Address", ref _address, value); }
        }

        public string Email
        {
            get { return _email; }
            set { SetPropertyValue("Email", ref _email, value); }
        }

        public string Contact
        {
            get { return _contact; }
            set { SetPropertyValue("Contact", ref _contact, value); }
        }

        public DateTime Birthday
        {
            get { return _birthday; }
            set { SetPropertyValue("Birthday", ref _birthday, value); }
        }

        [DataSourceCriteria("Active = true")]
        public City PlaceOfBirth
        {
            get { return _placeOfBirth; }
            set { SetPropertyValue("PlaceOfBirth", ref _placeOfBirth, value); }
        }


        [Association("GenderType-Employees")]
        public GenderType GenderType
        {
            get { return _genderType; }
            set { SetPropertyValue("GenderType", ref _genderType, value); }
        }

        [Association("Religion-Employees")]
        public Religion Religion
        {
            get { return _religion; }
            set { SetPropertyValue("Religion", ref _religion, value); }
        }

        public BloodType BloodType
        {
            get { return _bloodType; }
            set { SetPropertyValue("BloodType", ref _bloodType, value); }
        }

        [DevExpress.Xpo.Size(SizeAttribute.DefaultStringMappingFieldSize)]
        [VisibleInListViewAttribute(true)]
        [ImageEditor( // ListViewImageEditorMode = ImageEditorMode.PopupPictureEdit, 
                      DetailViewImageEditorFixedHeight = 275, DetailViewImageEditorFixedWidth = 240, ListViewImageEditorCustomHeight = 115)]
        public byte[] Photo
        {
            get { return GetPropertyValue<byte[]>("Photo"); }
            set { SetPropertyValue<byte[]>("Photo", value); }
        }

        [Association("City-Employees")]
        [DataSourceCriteria("Active = true")]
        public City City
        {
            get { return _city; }
            set { SetPropertyValue("City", ref _city, value); }
        }

        [Association("Country-Employees")]
        [DataSourceCriteria("Active = true")]
        public Country Country
        {
            get { return _country; }
            set { SetPropertyValue("Country", ref _country, value); }
        }

        [Association("Company-Employees")]
        [DataSourceCriteria("Active = true")]
        [ImmediatePostData()]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }
        
        [Association("Division-Employees")]
        [DataSourceCriteria("Active = true And Company = '@This.Company'"), ImmediatePostData()]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Association("Department-Employees")]
        [DataSourceCriteria("Active = true And Division = '@This.Division'"), ImmediatePostData()]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Association("Section-Employees")]
        [DataSourceCriteria("Active = true And Department = '@This.Department'"), ImmediatePostData()]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Association("Position-Employees")]
        [DataSourceCriteria("Active = true")]
        public Position Position
        {
            get { return _position; }
            set { SetPropertyValue("Position", ref _position, value); }
        }

        [Association("Branch-Employees")]
        [DataSourceCriteria("Active = true")]
        [ImmediatePostData()]
        public Branch Branch
        {
            get { return _branch; }
            set { SetPropertyValue("Branch", ref _branch, value); }
        }

        [Association("Location-Employees")]
        public Location Location
        {
            get { return _location; }
            set { SetPropertyValue("Location", ref _location, value); }
        }

        [Association("WorkingStatus-Employees")]
        public WorkingStatus WorkingStatus
        {
            get { return _workingStatus; }
            set { SetPropertyValue("WorkingStatus", ref _workingStatus, value); }
        }

        public DateTime StartDate
        {
            get { return _startDate; }
            set { SetPropertyValue("StartDate", ref _startDate, value); }
        }

        public DateTime EndDate
        {
            get { return _endDate; }
            set { SetPropertyValue("EndDate", ref _endDate, value); }
        }

        public string NPWP
        {
            get { return _npwp; }
            set { SetPropertyValue("NPWP", ref _npwp, value); }
        }

        public bool IsExpatriate
        {
            get { return _isExPatriate; }
            set { SetPropertyValue("IsExpatriate", ref _isExPatriate, value); }
        }

        public TaxCalculationMethod TaxCalculationMethod
        {
            get { return _taxCalculationMethod; }
            set { SetPropertyValue("TaxCalculationMethod", ref _taxCalculationMethod, value); }
        }

        public NonTaxableIncomeType NonTaxableIncomeStatus
        {
            get { return _nonTaxableIncomeStatus; }
            set { SetPropertyValue("NonTaxableIncomeStatus", ref _nonTaxableIncomeStatus, value); }
        }

        [Association("Currency-Employees")]
        public  Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        
        public string InsuranceNumber
        {
            get { return _insuranceNumber; }
            set { SetPropertyValue("InsuranceNumber", ref _insuranceNumber, value); }
        }

        public string BPJSTK
        {
            get { return _BPJSTK; }
            set { SetPropertyValue("BPJS Ketenagakerjaan", ref _BPJSTK, value); }
        }

        public DateTime BPJSTKStartDate
        {
            get { return _BPJSTKStartDate; }
            set { SetPropertyValue("BPJSTKStartDate", ref _BPJSTKStartDate, value); }
        }

        public double TaxDeductionFromPreviousCompany
        {
            get { return _taxDeductionFromPreviousCompany; }
            set { SetPropertyValue("TaxDeductionFromPreviousCompany", ref _taxDeductionFromPreviousCompany, value); }
        }

        public Grade Grade
        {
            get { return _grade; }
            set { SetPropertyValue("Grade", ref _grade, value); }
        }

        [Association("SalaryGroup-Employees")]
        public SalaryGroup SalaryGroup
        {
            get { return _salaryGroup; }
            set { SetPropertyValue("SalaryGroup", ref _salaryGroup, value); }
        }

        public bool IsImported
        {
            get { return _isImported; }
            set { SetPropertyValue("IsImported", ref _isImported, value); }
        }

        public string TaxStreet
        {
            get { return _taxStreet; }
            set { SetPropertyValue("TaxStreet", ref _taxStreet, value); }
        }

        public string TaxCity
        {
            get { return _taxCity; }
            set { SetPropertyValue("TaxCity", ref _taxCity, value); }
        }

        public string TaxCountry
        {
            get { return _taxCountry; }
            set { SetPropertyValue("TaxCountry", ref _taxCountry, value); }
        }

        [Size(512)]
        public string Remark
        {
            get { return _remark; }
            set { SetPropertyValue("Remark", ref _remark, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        [Association("Employee-UserAcesses")]
        public XPCollection<UserAccess>UserAccesses
        {
            get { return GetCollection<UserAccess>("UserAccesses"); }
        }

        [Association("Employee-Identities")]
        public XPCollection<Identity> Identities
        {
            get { return GetCollection<Identity>("Identities"); }
        }

        [Association("Employee-Addresses")]
        public XPCollection<Address> Addresses
        {
            get { return GetCollection<Address>("Addresses"); }
        }

        [Association("Employee-PhoneNumbers")]
        public XPCollection<PhoneNumber> PhoneNumbers
        {
            get { return GetCollection<PhoneNumber>("PhoneNumbers"); }
        }

        [Association("Employee-Families")]
        public XPCollection<Family> Families
        {
            get { return GetCollection<Family>("Families"); }
        }

        [Association("Employee-Educations")]
        public XPCollection<Education> Educations
        {
            get { return GetCollection<Education>("Educations"); }
        }

        [Association("Employee-Trainings")]
        public XPCollection<Training> Trainings
        {
            get { return GetCollection<Training>("Trainings"); }
        }

        [Association("Employee-Specialities")]
        public XPCollection<Speciality> Specialities
        {
            get { return GetCollection<Speciality>("Specialities"); }
        }

        [Association("Employee-WorkingExperiences")]
        public XPCollection<WorkingExperience> WorkingExperiences
        {
            get { return GetCollection<WorkingExperience>("WorkingExperiences"); }
        }

        [Association("Employee-WorkingContracts")]
        public XPCollection<WorkingContract> WorkingContracts
        {
            get { return GetCollection<WorkingContract>("WorkingContracts"); }
        }

        [Association("Employee-SalaryBasics")]
        public XPCollection<SalaryBasic> SalaryBasic
        {
            get { return GetCollection<SalaryBasic>("SalaryBasic"); }
        }

        [Association("Employee-PayrollProcesses")]
        public XPCollection<PayrollProcess> PayrollProcesses
        {
            get { return GetCollection<PayrollProcess>("PayrollProcesses"); }
        }       

       

        [Association("Employee-TaxProcesses")]
        public XPCollection<TaxProcess> TaxProcesses
        {
            get { return GetCollection<TaxProcess>("TaxProcesses"); }
        }

        [Association("Employee-LoanProposals")]
        public XPCollection<LoanProposal> LoanProposals
        {
            get { return GetCollection<LoanProposal>("LoanProposals"); }
        }

        [Association("Employee-LoanProcesses")]
        public XPCollection<LoanProcess> LoanProcesses
        {
            get { return GetCollection<LoanProcess>("LoanProcesses"); }
        } 

        [Association("Employee-LoanApprovals")]
        public XPCollection<LoanApproval> LoanApprovals
        {
            get { return GetCollection<LoanApproval>("LoanApprovals"); }
        }

        [Association("Employee-InsuranceLines")]
        public XPCollection<InsuranceLine> InsuranceLines
        {
            get { return GetCollection<InsuranceLine>("InsuranceLines"); }
        }


        //[Association("SalaryGroups-Employee")]
        //public XPCollection<SalaryGroup> SalaryGroups
        //{
        //    get { return GetCollection<SalaryGroup>("SalaryGroups"); }
        //}

        //[Association("Employee-SalaryPeriods")]
        //public XPCollection<SalaryPeriod> SalaryPeriods
        //{
        //    get { return GetCollection<SalaryPeriod>("SalaryPeriods"); }
        //} 

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}

        //private void SetFullName()
        //{
        //    try
        //    {
        //        if (this._firstName != null && this._middleName == null && this._lastName == null)
        //        {
        //            this._fullName = String.Format("{0} ", this._firstName);
        //        }
        //        else if (this.FirstName != null && this._middleName != null && this._lastName == null)
        //        {
        //            this._fullName = String.Format("{0} {1}", this._firstName, this._middleName);
        //        }
        //        else if (this.FirstName != null && this._middleName == null && this._lastName != null)
        //        {
        //            this._fullName = String.Format("{0} {1}", this._firstName, this._lastName);
        //        }
        //        else if (this.FirstName != null && this._middleName != null && this._lastName != null)
        //        {
        //            this._fullName = String.Format("{0} {1} {2}", this._firstName, this._middleName, this._lastName);
        //        }
        //        else if (this.FirstName == null && this._middleName != null && this._lastName != null)
        //        {
        //            this._fullName = String.Format("{0} {1}", this._middleName, this._lastName);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Tracing.Tracer.LogError("Business Object = WireNumber ", ex.ToString());
        //    }
        //}
    }
}