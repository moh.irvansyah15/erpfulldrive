﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.SystemModule;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Editors;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Inventory")]
    [RuleCombinationOfPropertiesIsUnique("InventoryTransferOutRuleUnique", DefaultContexts.Save, "Code")]
    
    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class InventoryTransferOut : FullDriveSysBaseObject
    {
        #region Default

        private bool _activationPosting;
        private string _code;
        private string _name;
        private DirectionType _transferType;
        private InventoryMovingType _inventoryMovingType;
        private DocumentRule _documentRule;
        private ObjectList _objectList;
        XPCollection<DocumentType> _availableDocumentType;
        private DocumentType _documentType;
        private string _docNo;
        private BusinessPartner _businessPartner;
        private Country _businessPartnerCountry;
        private City _businessPartnerCity;
        private string _businessPartnerAddress;
        private PriceGroup _priceGroup;
        private Status _status;
        private DateTime _statusDate;
        private DateTime _estimatedDate;
        private XPCollection<Location> _availableLocation;
        private Location _location;
        private StockType _stockType;
        private bool _stockControlling;
        private bool _splitInvoice;
        private ProjectHeader _projectHeader;
        private PurchaseOrder _purchaseOrder;
        private PurchaseReturn _purchaseReturn;
        private InventoryTransferOrder _inventoryTransferOrder;
        private double _maxBill;
        private Company _company;
        private string _userAccess;
        private bool _sameWarehouse;
        private int _postedLotCount;
        private GlobalFunction _globFunc;

        public InventoryTransferOut(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.InventoryTransferOut);
                this.ObjectList = ObjectList.InventoryTransferOut;
                this.Status = Status.Open;
                this.StatusDate = now;

                _userAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                if (Session.IsNewObject(this))
                {
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null)
                            {
                                Company = _locUserAccess.Employee.Company;
                            }

                            WarehouseSetupDetail _locWarehouseSetupDetail = Session.FindObject<WarehouseSetupDetail>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("UserAccess", _locUserAccess),
                                                                        new BinaryOperator("ObjectList", ObjectList.InventoryTransferOut),
                                                                        new BinaryOperator("Active", true),
                                                                        new BinaryOperator("Default", true)));
                            if(_locWarehouseSetupDetail != null)
                            {
                                this.TransferType = _locWarehouseSetupDetail.TransferType;
                                this.InventoryMovingType = _locWarehouseSetupDetail.InventoryMovingType;
                                this.DocumentRule = _locWarehouseSetupDetail.DocumentRule;
                            }
                        }
                    }
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #endregion Default

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("InventoryTransferOutCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("InventoryTransferOutNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [ImmediatePostData()]
        [Appearance("InventoryTransferOutTransferTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DirectionType TransferType
        {
            get { return _transferType; }
            set { SetPropertyValue("TransferType", ref _transferType, value); }
        }

        [ImmediatePostData()]
        [Appearance("InventoryTransferOutInventoryMovingTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public InventoryMovingType InventoryMovingType
        {
            get { return _inventoryMovingType; }
            set { SetPropertyValue("InventoryMovingType", ref _inventoryMovingType, value); }
        }

        [ImmediatePostData()]
        [Appearance("InventoryTransferOutDocumentRuleClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DocumentRule DocumentRule
        {
            get { return _documentRule; }
            set { SetPropertyValue("DocumentRule", ref _documentRule, value); }
        }

        [ImmediatePostData()]
        [Browsable(false)]
        public ObjectList ObjectList
        {
            get { return _objectList; }
            set { SetPropertyValue("ObjectList", ref _objectList, value); }
        }

        [Browsable(false)]
        public XPCollection<DocumentType> AvailableDocumentType
        {
            get
            {
                if (ObjectList == ObjectList.None)
                {
                    _availableDocumentType = new XPCollection<DocumentType>(Session);
                }
                else
                {
                    _availableDocumentType = new XPCollection<DocumentType>(Session,
                                              new GroupOperator(GroupOperatorType.And,
                                              new BinaryOperator("ObjectList", this.ObjectList),
                                              new BinaryOperator("InventoryMovingType", this.InventoryMovingType),
                                              new BinaryOperator("TransferType", this.TransferType),
                                              new BinaryOperator("DocumentRule", this.DocumentRule),
                                              new BinaryOperator("Active", true)));
                }
                return _availableDocumentType;

            }
        }

        [Appearance("InventoryTransferOutDocumentTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableDocumentType", DataSourcePropertyIsNullMode.SelectAll)]
        public DocumentType DocumentType
        {
            get { return _documentType; }
            set
            {
                SetPropertyValue("DocumentType", ref _documentType, value);
                if (!IsLoading)
                {
                    if (_documentType != null)
                    {
                        _globFunc = new GlobalFunction();
                        if (_documentType.StockType != StockType.None)
                        {
                            this.StockType = this._documentType.StockType;
                        }
                        this.DocNo = _globFunc.GetDocumentNumberingUnlockOptimisticRecord(this.Session.DataLayer, this._documentType);
                    }
                }
            }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("InventoryTransferOutDocNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string DocNo
        {
            get { return _docNo; }
            set { SetPropertyValue("DocNo", ref _docNo, value); }
        }

        [Appearance("InventoryTransferOutBusinessPartnerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public BusinessPartner BusinessPartner
        {
            get { return _businessPartner; }
            set
            {
                SetPropertyValue("BusinessPartner", ref _businessPartner, value);
                if (!IsLoading)
                {
                    if (_businessPartner != null)
                    {
                        this.BusinessPartnerCountry = _businessPartner.Country;
                        this.BusinessPartnerCity = _businessPartner.City;
                        this.BusinessPartnerAddress = _businessPartner.Address;
                    }
                }
            }
        }

        [Appearance("InventoryTransferOutBusinessPartnerCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Country BusinessPartnerCountry
        {
            get { return _businessPartnerCountry; }
            set { SetPropertyValue("BusinessPartnerCountry", ref _businessPartnerCountry, value); }
        }

        [Appearance("InventoryTransferOutBusinessPartnerCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public City BusinessPartnerCity
        {
            get { return _businessPartnerCity; }
            set { SetPropertyValue("BusinessPartnerCity", ref _businessPartnerCity, value); }
        }

        [Size(512)]
        [Appearance("InventoryTransferOutBusinessPartnerAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string BusinessPartnerAddress
        {
            get { return _businessPartnerAddress; }
            set { SetPropertyValue("BusinessPartnerAddress", ref _businessPartnerAddress, value); }
        }

        [Appearance("InventoryTransferOutPriceGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set { SetPropertyValue("PriceGroup", ref _priceGroup, value); }
        }

        [Appearance("InventoryTransferOutStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("InventoryTransferOutStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("InventoryTransferOutEstimatedDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime EstimatedDate
        {
            get { return _estimatedDate; }
            set { SetPropertyValue("EstimatedDate", ref _estimatedDate, value); }
        }

        [Browsable(false)]
        public XPCollection<Location> AvailableLocation
        {
            get
            {
                _userAccess = SecuritySystem.CurrentUserName;
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                string _locLocationCode = null; 
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                if (_locUserAccess != null)
                {
                    if ((this.TransferType == DirectionType.External || this.TransferType == DirectionType.Internal) &&
                        (this.InventoryMovingType == InventoryMovingType.Deliver))
                    {
                        List<string> _stringLocation = new List<string>();

                        XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                                                  (Session, new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("UserAccess", _locUserAccess),
                                                                                   new BinaryOperator("ObjectList", ObjectList.InventoryTransferOut),
                                                                                   new BinaryOperator("Owner", true),
                                                                                   new BinaryOperator("TransferType", this.TransferType),
                                                                                   new BinaryOperator("InventoryMovingType", this.InventoryMovingType),
                                                                                   new BinaryOperator("LocationType", LocationType.Transit),
                                                                                   new BinaryOperator("Active", true)));

                        if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                        {
                            foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                            {
                                if(_locWhsSetupDetail.Location != null)
                                {
                                    if(_locWhsSetupDetail.Location.Code != null)
                                    {
                                        _locLocationCode = _locWhsSetupDetail.Location.Code;
                                        _stringLocation.Add(_locLocationCode);
                                    }
                                }
                                
                            }
                        }

                        IEnumerable<string> _stringArrayLocationFromDistinct = _stringLocation.Distinct();
                        string[] _stringArrayLocationFromList = _stringArrayLocationFromDistinct.ToArray();
                        if (_stringArrayLocationFromList.Length == 1)
                        {
                            for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                            {
                                Location _locLocationFrom = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                if (_locLocationFrom != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locLocationFrom.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArrayLocationFromList.Length > 1)
                        {
                            for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                            {
                                Location _locLocationFrom = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                if (_locLocationFrom != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locLocationFrom.Code + "'";
                                    }
                                    else
                                    {
                                        _endString = _endString + " OR [Code]=='" + _locLocationFrom.Code + "'";
                                    }
                                }
                            }
                        }
                        _fullString = _beginString + _endString;

                        if (_fullString != null)
                        {
                            _availableLocation = new XPCollection<Location>(Session, CriteriaOperator.Parse(_fullString));
                        }
                    }
                }
                else
                {
                    _availableLocation = new XPCollection<Location>(Session);
                }

                return _availableLocation;

            }
        }

        [Appearance("InventoryTransferOutLocationClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableLocation", DataSourcePropertyIsNullMode.SelectAll)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public Location Location
        {
            get { return _location; }
            set { SetPropertyValue("Location", ref _location, value); }
        }

        [Appearance("InventoryTransferOutStockTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockType StockType
        {
            get { return _stockType; }
            set { SetPropertyValue("StockType", ref _stockType, value); }
        }

        [Appearance("InventoryTransferOutStockControllingClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool StockControlling
        {
            get { return _stockControlling; }
            set { SetPropertyValue("StockControlling", ref _stockControlling, value); }
        }

        [Appearance("InventoryTransferOutSplitInvoiceClose", Enabled = false)]
        public bool SplitInvoice
        {
            get { return _splitInvoice; }
            set { SetPropertyValue("SplitInvoice", ref _splitInvoice, value); }
        }

        [Appearance("InventoryTransferOutProjectHeaderClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Association("ProjectHeader-InventoryTransferOuts")]
        public ProjectHeader ProjectHeader
        {
            get { return _projectHeader; }
            set { SetPropertyValue("ProjectHeader", ref _projectHeader, value); }
        }

        //Wahab 27-02-2020
        //[Association("PurchaseOrder-InventoryTransferOuts")]
        [Appearance("InventoryTransferOutPurchaseOrderClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PurchaseOrder PurchaseOrder
        {
            get { return _purchaseOrder; }
            set { SetPropertyValue("PurchaseOrder", ref _purchaseOrder, value); }
        }

        [Browsable(false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        //clm 310320
        [Appearance("InventoryTransferOutPostedLotCountClose", Enabled = false, Visibility = ViewItemVisibility.Hide)]
        public int PostedLotCount
        {
            get { return _postedLotCount; }
            set { SetPropertyValue("PostedLotCount", ref _postedLotCount, value); }
        }

        
        [Appearance("InventoryTransferOutPurchaseReturnClose", Criteria = "ActivationPosting = true", Enabled = false)]      
        public PurchaseReturn PurchaseReturn
        {
            get { return _purchaseReturn; }
            set { SetPropertyValue("PurchaseReturn", ref _purchaseReturn, value); }
        }

        public InventoryTransferOrder InventoryTransferOrder
        {
            get { return _inventoryTransferOrder; }
            set { SetPropertyValue("InventoryTransferOrder", ref _inventoryTransferOrder, value); }
        }

        [Browsable(false)]
        public double MaxBill
        {
            get { return _maxBill; }
            set { SetPropertyValue("MaxBill", ref _maxBill, value); }
        }

        [Browsable(false)]
        public bool SameWarehouse
        {
            get { return _sameWarehouse; }
            set { SetPropertyValue("SameWarehouse", ref _sameWarehouse, value); }
        }

        [Association("InventoryTransferOut-InventoryTransferOutLines")]
        public XPCollection<InventoryTransferOutLine> InventoryTransferOutLines
        {
            get { return GetCollection<InventoryTransferOutLine>("InventoryTransferOutLines"); }
        }

        [Association("InventoryTransferOut-InventorySalesCollections")]
        public XPCollection<InventorySalesCollection> InventorySalesCollections
        {
            get { return GetCollection<InventorySalesCollection>("InventorySalesCollections"); }
        }
        
        #endregion Field


    }
}