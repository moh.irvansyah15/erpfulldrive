﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Production")]
    [RuleCombinationOfPropertiesIsUnique("OutputProductionJournalRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class OutputProductionJournal : BaseObject
    {
        #region Default

        private string _code;
        private Location _location;
        private BinLocation _binLocation;
        private Item _item;
        private Company _company;
        private double _qtyOut;
        private double _qtyActual;
        private double _qtyReturn;
        private UnitOfMeasure _dUOM;
        private OutputProduction _outputProduction;
        private DateTime _journalDate;
        private GlobalFunction _globFunc;

        public OutputProductionJournal(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.OutputProductionJournal);
            }
        }

        #endregion Default

        #region Field

        [Appearance("OutputProductionJournalCodeEnabled", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("OutputProductionJournalLocationEnabled", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Location Location
        {
            get { return _location; }
            set { SetPropertyValue("Location", ref _location, value); }
        }

        [Appearance("OutputProductionJournalBinLocationEnabled", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public BinLocation BinLocation
        {
            get { return _binLocation; }
            set { SetPropertyValue("binLocation", ref _binLocation, value); }
        }

        [Appearance("OutputProductionJournalItemEnabled", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Item Item
        {
            get { return _item; }
            set { SetPropertyValue("Item", ref _item, value); }
        }

        [Appearance("OutputProductionJournalQtyOutEnabled", Enabled = false)]
        public double QtyOut
        {
            get { return _qtyOut; }
            set { SetPropertyValue("QtyOut", ref _qtyOut, value); }
        }

        [Appearance("OutputProductionJournalQtyActualEnabled", Enabled = false)]
        public double QtyActual
        {
            get { return _qtyActual; }
            set { SetPropertyValue("QtyActual", ref _qtyActual, value); }
        }

        [Appearance("OutputProductionJournalQtyReturnEnabled", Enabled = false)]
        public double QtyReturn
        {
            get { return _qtyReturn; }
            set { SetPropertyValue("QtyReturn", ref _qtyReturn, value); }
        }

        [Appearance("OutputProductionJournalDUOMEnabled", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM
        {
            get { return _dUOM; }
            set { SetPropertyValue("DUOM", ref _dUOM, value); }
        }

        [Appearance("OutputProductionJournalJournalDateEnabled", Enabled = false)]
        public DateTime JournalDate
        {
            get { return _journalDate; }
            set { SetPropertyValue("JournalDate", ref _journalDate, value); }
        }

        [Appearance("OutputProductionJournalCompanyEnabled", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("OutputProductionJournalOutputProductionEnabled", Enabled = false)]
        public OutputProduction OutputProduction
        {
            get { return _outputProduction; }
            set { SetPropertyValue("OutputProduction", ref _outputProduction, value); }
        }

        #endregion Field

    }
}