﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("PriceLineRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class PriceLine : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private string _name;
        private Item _item;
        private double _amount1;
        private double _amount2;
        private DateTime _startDate;
        private DateTime _endDate;
        private OrderType _priceType;
        private Company _company;
        private bool _active;
        private Price _price;
        private GlobalFunction _globFunc;

        public PriceLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PriceLine);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [DataSourceCriteria("Active = true")]
        public Item Item
        {
            get { return _item; }
            set { SetPropertyValue("Item", ref _item, value); }
        }

        // Approximate
        [ImmediatePostData()]
        public double Amount1
        {
            get { return _amount1; }
            set
            {
                SetPropertyValue("Amount1", ref _amount1, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();

                    if (this._amount1 > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.PriceLine, FieldName.Amount1) == true)
                        {
                            this._amount1 = _globFunc.GetRoundUp(Session, this._amount1, ObjectList.PriceLine, FieldName.Amount1);
                        }
                    }
                }
            }
        }

        // Offer
        [ImmediatePostData()]
        public double Amount2
        {
            get { return _amount2; }
            set
            {
                SetPropertyValue("Amount2", ref _amount2, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();

                    if (this._amount2 > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.PriceLine, FieldName.Amount2) == true)
                        {
                            this._amount2 = _globFunc.GetRoundUp(Session, this._amount2, ObjectList.PriceLine, FieldName.Amount2);
                        }
                    }
                }
            }
        }

        public DateTime StartDate
        {
            get { return _startDate; }
            set { SetPropertyValue("StartDate", ref _startDate, value); }
        }

        public DateTime EndDate
        {
            get { return _endDate; }
            set { SetPropertyValue("EndDate", ref _endDate, value); }
        }

        public OrderType PriceType
        {
            get { return _priceType; }
            set { SetPropertyValue("PriceType", ref _priceType, value); }
        }

        [DataSourceCriteria("Active = true")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        [ImmediatePostData]
        [Association("Price-PriceLines")]
        public Price Price
        {
            get { return _price; }
            set
            {
                SetPropertyValue("Price", ref _price, value);
                if (!IsLoading)
                {
                    if (this._price != null)
                    {
                        if (this._price.Item != null)
                        {
                            this.Item = this._price.Item;
                            if (this._price.Item.OrderType != OrderType.None)
                            {
                                this.PriceType = this._price.Item.OrderType;
                            }
                        }
                        if (this._price.Company != null)
                        {
                            this.Company = this._price.Company;
                        }
                    }
                }
            }
        }

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}
    }
}