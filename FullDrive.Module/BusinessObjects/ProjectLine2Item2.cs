﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.SystemModule;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Project Transaction")]
    [RuleCombinationOfPropertiesIsUnique("ProjectLine2Item2RuleUnique", DefaultContexts.Save, "Code")]
    [ListViewFilter("AllDataProjectLine2Item2", "", "All Data", "All Data In Project Line2 Item2", 1, true)]
    [ListViewFilter("OpenProjectLine2Item2", "Status = 'Open'", "Open", "Open Data Status In Project Line2 Item2", 2, true)]
    [ListViewFilter("ProgressProjectLine2Item2", "Status = 'Progress'", "Progress", "Progress Data Status In Project Line2 Item2", 3, true)]
    [ListViewFilter("LockProjectLine2Item2", "Status = 'Lock'", "Lock", "Lock Data Status In Project Line2 Item2", 4, true)]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class ProjectLine2Item2 : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private int _no;
        private string _code;
        private string _name;
        private Item _item;
        private double _dQty;
        private UnitOfMeasure _dUom;
        private double _qty;
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasure;
        private UnitOfMeasure _uom;
        private Brand _brand;
        private double _tQty;
        private double _unitAmount;
        private double _totalUnitAmount;
        private string _description;
        private Status _status;
        private DateTime _statusDate;
        private Company _company;
        private ProjectLineItem2 _projectLineItem2;
        private ProjectLine _projectLine;
        private ProjectLine2 _projectLine2;
        private ProjectHeader _projectHeader;
        private GlobalFunction _globFunc;

        public ProjectLine2Item2(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ProjectLine2Item2);
                DateTime now = DateTime.Now;
                this.Status = Status.Open;
                this.StatusDate = now;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            RecoveryDeleteNo();
        }

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("ProjectLineItem2NoClose", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ProjectLineItem2CodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("ProjectLineItem2NameLock", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("ProjectLineItem2ItemLock", Criteria = "ActivationPosting = true", Enabled = false)]
        public Item Item
        {
            get { return _item; }
            set { SetPropertyValue("Item", ref _item, value); }
        }

        #region DefaultQty

        [Appearance("ProjectLineItem2DQtyLock", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double DQty
        {
            get { return _dQty; }
            set
            {
                SetPropertyValue("DQty", ref _dQty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    SetTotalAmount();
                }
            }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("ProjectLineItem2DUOMLock", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public UnitOfMeasure DUOM
        {
            get { return _dUom; }
            set
            {
                SetPropertyValue("DUOM", ref _dUom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("ProjectLineItem2QtyLock", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double Qty
        {
            get { return _qty; }
            set
            {
                SetPropertyValue("Qty", ref _qty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    SetTotalAmount();
                }
            }
        }

        [Browsable(false)]
        public XPCollection<UnitOfMeasure> AvailableUnitOfMeasure
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                if (Item == null)
                {
                    _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session);
                }
                else
                {
                    List<string> _stringUOM = new List<string>();

                    XPCollection<ItemUnitOfMeasure> _locItemUnitOfMeasures = new XPCollection<ItemUnitOfMeasure>
                                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Item", this.Item),
                                                                               new BinaryOperator("Active", true)));

                    if (_locItemUnitOfMeasures != null && _locItemUnitOfMeasures.Count() > 0)
                    {
                        foreach (ItemUnitOfMeasure _locItemUnitOfMeasure in _locItemUnitOfMeasures)
                        {
                            _stringUOM.Add(_locItemUnitOfMeasure.UOM.Code);
                        }
                    }

                    IEnumerable<string> _stringArrayUOMDistinct = _stringUOM.Distinct();
                    string[] _stringArrayUOMList = _stringArrayUOMDistinct.ToArray();
                    if (_stringArrayUOMList.Length == 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArrayUOMList.Length > 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                                else
                                {
                                    _endString = " OR [Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session, CriteriaOperator.Parse(_fullString));
                    }

                }
                return _availableUnitOfMeasure;

            }
        }

        [Appearance("ProjectLineItem2UOMLock", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set
            {
                SetPropertyValue("UOM", ref _uom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    SetTotalAmount();
                }
            }
        }

        [Appearance("ProjectLineItem2TotalQtyClose", Enabled = false)]
        public double TQty
        {
            get { return _tQty; }
            set { SetPropertyValue("TQty", ref _tQty, value); }
        }

        #endregion DefaultQty

        #region CustomerPrice

        [DataSourceCriteria("Active = true")]
        [Appearance("ProjectLineItem2BrandLock", Criteria = "ActivationPosting = true", Enabled = false)]
        public Brand Brand
        {
            get { return _brand; }
            set { SetPropertyValue("Brand", ref _brand, value); }
        }

        [Appearance("ProjectLineItem2UnitAmountLock", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double UnitAmount
        {
            get { return _unitAmount; }
            set
            {
                SetPropertyValue("UnitAmount", ref _unitAmount, value);
                if (!IsLoading)
                {
                    _globFunc = new GlobalFunction();
                    if (this._unitAmount > 0)
                    {
                        if (_globFunc.GetRoundingList(Session, ObjectList.ProjectLine2Item2, FieldName.UnitAmount) == true)
                        {
                            this._unitAmount = _globFunc.GetRoundUp(Session, this._unitAmount, ObjectList.ProjectLine2Item2, FieldName.UnitAmount);
                        }
                    }
                    SetTotalAmount();
                }
            }
        }

        [Appearance("ProjectLineItem2TotalUnitAmountClose", Enabled = false)]
        public double TotalUnitAmount
        {
            get { return _totalUnitAmount; }
            set { SetPropertyValue("TotalUnitAmount", ref _totalUnitAmount, value); }
        }

        #endregion CustomerPrice

        [Appearance("ProjectLineItem2DescriptionLock", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Appearance("ProjectLineItem2StatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("ProjectLineItem2StatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("ProjectLineItem2CompanyLock", Criteria = "ActivationPosting = true", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("ProjectLineItem2ProjectLineClose", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        [ImmediatePostData()]
        [Association("ProjectLine2-ProjectLine2Item2s")]
        public ProjectLine2 ProjectLine2
        {
            get { return _projectLine2; }
            set
            {
                SetPropertyValue("ProjectLine2", ref _projectLine2, value);
                if (!IsLoading)
                {
                    if (this._projectLine2 != null)
                    {
                        if (this._projectLine2.ProjectHeader != null)
                        {
                            this.ProjectHeader = this._projectLine2.ProjectHeader;
                        }
                    }
                }
            }
        }

        [Browsable(false)]
        public ProjectLineItem2 ProjectLineItem2
        {
            get { return _projectLineItem2; }
            set
            {
                SetPropertyValue("ProjectLineItem2", ref _projectLineItem2, value);
                if (!IsLoading)
                {
                    if (this._projectLineItem2 != null)
                    {
                        if (this._projectLineItem2.Item != null)
                        {
                            if (this._projectLineItem2.Item.Name != null)
                            {
                                this.Name = (_projectLineItem2.Code) + " For " + this._projectLineItem2.Item.Name;
                            }
                        }
                    }
                }
            }
        }

        [Browsable(false)]
        public ProjectLine ProjectLine
        {
            get { return _projectLine; }
            set { SetPropertyValue("ProjectLine", ref _projectLine, value); }
        }

        [Browsable(false)]
        public ProjectHeader ProjectHeader
        {
            get { return _projectHeader; }
            set { SetPropertyValue("ProjectHeader", ref _projectHeader, value); }
        }

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}

        //================================================== Code Only ==================================================

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.ProjectLine2 != null)
                    {
                        object _makRecord = Session.Evaluate<ProjectLine2Item2>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("ProjectLine2=?", this.ProjectLine2));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectLine2Item2 " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.ProjectLine2 != null)
                {
                    ProjectLine2 _numHeader = Session.FindObject<ProjectLine2>
                                                (new BinaryOperator("Code", this.ProjectLine2.Code));

                    XPCollection<ProjectLine2Item2> _numLines = new XPCollection<ProjectLine2Item2>
                                                (Session, new BinaryOperator("ProjectLine2", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (ProjectLine2Item2 _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectLine2Item2 " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.ProjectLine2 != null)
                {
                    ProjectLine2 _numHeader = Session.FindObject<ProjectLine2>
                                                (new BinaryOperator("Code", this.ProjectLine2.Code));

                    XPCollection<ProjectLine2Item2> _numLines = new XPCollection<ProjectLine2Item2>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("ProjectLine2", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (ProjectLine2Item2 _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectLine2Item2 " + ex.ToString());
            }
        }

        #endregion No

        #region AmountMethod

        private void SetTotalAmount()
        {
            try
            {
                if (_tQty >= 0 && _unitAmount >= 0)
                {
                    if (_globFunc.GetRoundingList(Session, ObjectList.ProjectLine2Item2, FieldName.TotalUnitAmount) == true)
                    {
                        this.TotalUnitAmount = _globFunc.GetRoundUp(Session, (this.TQty * this.UnitAmount), ObjectList.ProjectLine2Item2, FieldName.TotalUnitAmount);
                    }
                    else
                    {
                        this.TotalUnitAmount = this.TQty * this.UnitAmount;
                    }
                    this.TotalUnitAmount = this.TQty * this.UnitAmount;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectLine2Item2 " + ex.ToString());
            }
        }

        #endregion AmountMethod

        #region QtyMethod

        public void SetTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.ProjectLine != null)
                {
                    if (this.Item != null && this.UOM != null && this.DUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty * _locItemUOM.DefaultConversion + this.DQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty / _locItemUOM.Conversion + this.DQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty + this.DQty;
                            }

                            this.TQty = _locInvLineTotal;
                        }
                    }

                    else
                    {
                        _locInvLineTotal = this.Qty + this.DQty;
                        this.TQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectLine2Item2 " + ex.ToString());
            }
        }

        #endregion QtyMethod

    }
}