﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Cost Of Goods Sold")]
    [RuleCombinationOfPropertiesIsUnique("OverheadCostEmployeeRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class OverheadCostEmployee : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private int _no;
        private string _code;
        private double _salary;
        private double _workingDay;
        private double _workingHour;
        private double _mpm;
        private double _totalCostEmployee;
        private OverheadCost _overheadCost;
        private GlobalFunction _globFunc;

        public OverheadCostEmployee(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if(!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(Session.DataLayer, ObjectList.OverheadCostEmployee);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            RecoveryDeleteNo();
        }

        [Appearance("OverheadCostEmployeeNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [ImmediatePostData()]
        public double Salary
        {
            get { return _salary; }
            set
            {
                SetPropertyValue("Salary", ref _salary, value);
                if (!IsLoading)
                {
                    SetTotalCostEmployee();
                }
            }
        }

        [ImmediatePostData()]
        public double WorkingDay
        {
            get { return _workingDay; }
            set
            {
                SetPropertyValue("WorkingDay", ref _workingDay, value);
                if (!IsLoading)
                {
                    SetTotalCostEmployee();
                }
            }
        }

        [ImmediatePostData()]
        public double WorkingHour
        {
            get { return _workingHour; }
            set
            {
                SetPropertyValue("WorkingHour", ref _workingHour, value);
                if (!IsLoading)
                {
                    SetTotalCostEmployee();
                }
            }
        }

        //[ImmediatePostData()]
        public double MPM
        {
            get { return _mpm; }
            set
            {
                SetPropertyValue("MPM", ref _mpm, value);
                if (!IsLoading)
                {
                    if (this._mpm > 0)
                    {
                        SetTotalCostEmployee();
                    }

                }
            }
        }

        [Appearance("OverheadCostEmployeeTotalCostEmployeeEnabled", Enabled = false)]
        public double TotalCostEmployee
        {
            get { return _totalCostEmployee; }
            set { SetPropertyValue("TotalCostEmployee", ref _totalCostEmployee, value); }
        }

        [Appearance("OverheadCostEmployeeSumCostEmployeeEnabled", Enabled = false)]
        public Double SumCostEmployee
        {
            get
            {
                if (this.TotalCostEmployee > 0)
                {
                    return GetTotalCostEmployee();
                }
                else
                {
                    return 0;
                }
            }
        }

        [Association("OverheadCost-OverheadCostEmployees")]
        [Appearance("OverheadCostEmployeeOverheadCostEnabled", Enabled = false)]
        public OverheadCost OverheadCost
        {
            get { return _overheadCost; }
            set
            {
                SetPropertyValue("OverheadCost", ref _overheadCost, value);
                if (!IsLoading)
                {
                    //this.KWH = _overheadCost.KWH;
                    this.MPM = _overheadCost.MPM;
                }
            }
        }

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}

        //================================================== Code Only ==================================================

        #region Numbering

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading)
                {
                    if (!(Session is NestedUnitOfWork) && Session.IsNewObject(this))
                    {
                        if (this.OverheadCost != null)
                        {
                            object _makRecord = Session.Evaluate<OverheadCostEmployee>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("OverheadCost=?", this.OverheadCost));
                            this.No = Convert.ToInt32(_makRecord) + 1;
                            this.Save();
                            RecoveryUpdateNo();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = OverheadCostEmployee " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.OverheadCost != null)
                {
                    OverheadCost _numHeader = Session.FindObject<OverheadCost>
                                                (new BinaryOperator("Code", this.OverheadCost.Code));

                    XPCollection<OverheadCostEmployee> _numLines = new XPCollection<OverheadCostEmployee>
                                                (Session, new BinaryOperator("OverheadCost", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (OverheadCostEmployee _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                        //Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = OverheadCostEmployee " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.OverheadCost != null)
                {
                    OverheadCost _numHeader = Session.FindObject<OverheadCost>
                                                (new BinaryOperator("Code", this.OverheadCost.Code));

                    XPCollection<OverheadCostEmployee> _numLines = new XPCollection<OverheadCostEmployee>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("OverheadCost", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (OverheadCostEmployee _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                        //Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = OverheadCostEmployee " + ex.ToString());
            }
        }

        #endregion Numbering

        private void SetTotalCostEmployee()
        {
            try
            {
                this._totalCostEmployee = this._salary / this._workingDay / this._workingHour / this._mpm;
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = OverheadCostEmployee " + ex.ToString());
            }
        }

        private double GetTotalCostEmployee()
        {
            double _result = 0;
            try
            {
                double _locTotalCostEmployee = 0;
                if (!IsLoading)
                {
                    if (this.OverheadCost != null)
                    {
                        XPCollection<OverheadCostEmployee> _locOverheadCostEmployees = new XPCollection<OverheadCostEmployee>
                                                    (Session, new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("OverheadCost", this.OverheadCost)));
                        if (_locOverheadCostEmployees.Count() > 0)
                        {
                            foreach (OverheadCostEmployee _locOverheadCostEmployee in _locOverheadCostEmployees)
                            {
                                _locTotalCostEmployee = _locTotalCostEmployee + _locOverheadCostEmployee.TotalCostEmployee;
                            }
                        }
                        _result = _locTotalCostEmployee;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = OverheadCostEmployee " + ex.ToString());
            }
            return _result;
        }

    }
}