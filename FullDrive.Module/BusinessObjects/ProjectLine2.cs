﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Title")]
    [NavigationItem("Project Transaction")]
    [RuleCombinationOfPropertiesIsUnique("ProjectLine2RuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class ProjectLine2 : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private int _no;
        private string _code;
        private string _title;
        private string _title2;
        private string _title3;
        private Status _status;
        private DateTime _statusDate;
        private string _description;
        private Company _company;
        private ProjectHeader _projectHeader;
        private ProjectLine _projectLine;
        private GlobalFunction _globFunc;
        //clm
        private bool _hideSumTotalItem;
        private bool _hideSumTotalService;

        public ProjectLine2(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ProjectLine2);
                DateTime now = DateTime.Now;
                this.Status = CustomProcess.Status.Open;
                this.StatusDate = now;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            RecoveryDeleteNo();
        }

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("ProjectLine2NoClose", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ProjectLine2CodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("ProjectLine2TitleLock", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Title
        {
            get { return _title; }
            set { SetPropertyValue("Title", ref _title, value); }
        }

        [Appearance("ProjectLine2Title2Lock", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Title2
        {
            get { return _title2; }
            set { SetPropertyValue("Title2", ref _title2, value); }
        }

        [Appearance("ProjectLine2Title3Lock", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Title3
        {
            get { return _title3; }
            set { SetPropertyValue("Title3", ref _title3, value); }
        }

        [Appearance("ProjectLine2StatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("ProjectLine2StatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("ProjectLine2DescriptionLock", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Appearance("ProjectLine2CompanyLock", Criteria = "ActivationPosting = true", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ProjectLineProjectHeaderClose", Enabled = false)]
        [Association("ProjectHeader-ProjectLine2s")]
        public ProjectHeader ProjectHeader
        {
            get { return _projectHeader; }
            set { SetPropertyValue("ProjectHeader", ref _projectHeader, value); }
        }

        [Browsable(false)]
        public ProjectLine ProjectLine
        {
            get { return _projectLine; }
            set { SetPropertyValue("ProjectLine", ref _projectLine, value); }
        }

        [Association("ProjectLine2-ProjectLine2Items")]
        public XPCollection<ProjectLine2Item> ProjectLine2Items
        {
            get { return GetCollection<ProjectLine2Item>("ProjectLine2Items"); }
        }

        [Association("ProjectLine2-ProjectLine2Item2s")]
        public XPCollection<ProjectLine2Item2> ProjectLine2Item2s
        {
            get { return GetCollection<ProjectLine2Item2>("ProjectLine2Item2s"); }
        }

        [Association("ProjectLine2-ProjectLine2Services")]
        public XPCollection<ProjectLine2Service> ProjectLine2Services
        {
            get { return GetCollection<ProjectLine2Service>("ProjectLine2Services"); }
        }

        #region CLM
        [Browsable(false)]
        public bool HideSumTotalItem
        {
            get { return _hideSumTotalItem; }
            set { SetPropertyValue("HideSumTotalItem", ref _hideSumTotalItem, value); }
        }

        [Appearance("ProjectLine2TotalL2I2Enabled", Criteria = "HideSumTotalItem = false", Visibility = ViewItemVisibility.Hide)]
        [Persistent("TotalPL2I2")]
        public double TotalPL2I2
        {
            get
            {
                double _result = 0;
                if (!IsLoading)
                {
                    if (GetTotalPL2I2() > 0)
                    {
                        _result = UpdateTotalPL2I2(true);
                    }
                }
                return _result;
            }
        }

        [Browsable(false)]
        public bool HideSumTotalService
        {
            get { return _hideSumTotalService; }
            set { SetPropertyValue("HideSumTotalService", ref _hideSumTotalService, value); }
        }

        [Appearance("ProjectLineTotalPL2SEnabled", Criteria = "HideSumTotalService = false", Visibility = ViewItemVisibility.Hide)]
        [Persistent("TotalPL2S")]
        public double TotalPL2S
        {
            get
            {
                double _result = 0;
                if (!IsLoading)
                {
                    if (GetTotalPL2S() > 0)
                    {
                        _result = UpdateTotalPL2S(true);
                    }
                }
                return _result;
            }
        }

        [Appearance("ProjectLineSumTotalPL2I2Enabled", Criteria = "HideSumTotalItem = false", Visibility = ViewItemVisibility.Hide)]
        public double SumTotalPL2I2
        {
            get
            {
                if (this.TotalPL2I2 > 0)
                {
                    return GetSumTotalPL2I2();
                }
                else
                {
                    return 0;
                }
            }
        }

        [Appearance("ProjectLineSumTotalPL2SEnabled", Criteria = "HideSumTotalItem = false", Visibility = ViewItemVisibility.Hide)]
        public double SumTotalPL2S
        {
            get
            {
                if (this.TotalPL2S > 0)
                {
                    return GetSumTotalPL2S();
                }
                else
                {
                    return 0;
                }
            }
        }
        #endregion CLM

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}

        //================================================== Code Only ==================================================

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.ProjectHeader != null)
                    {
                        object _makRecord = Session.Evaluate<ProjectLine2>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("ProjectHeader=?", this.ProjectHeader));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectLine2 " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.ProjectHeader != null)
                {
                    ProjectHeader _numHeader = Session.FindObject<ProjectHeader>
                                                (new BinaryOperator("Code", this.ProjectHeader.Code));

                    XPCollection<ProjectLine2> _numLines = new XPCollection<ProjectLine2>
                                                (Session, new BinaryOperator("ProjectHeader", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (ProjectLine2 _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectLine2 " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.ProjectHeader != null)
                {
                    ProjectHeader _numHeader = Session.FindObject<ProjectHeader>
                                                (new BinaryOperator("Code", this.ProjectHeader.Code));

                    XPCollection<ProjectLine2> _numLines = new XPCollection<ProjectLine2>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("ProjectHeader", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (ProjectLine2 _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectLine2 " + ex.ToString());
            }
        }

        #endregion No

        #region CLM

        public int GetTotalPL2I2()
        {
            int _result = 0;
            try
            {
                XPCollection<ProjectLine2Item2> _locProjectLine2Item2s = new XPCollection<ProjectLine2Item2>(Session, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ProjectLine2", this)));

                if (_locProjectLine2Item2s != null && _locProjectLine2Item2s.Count > 0)
                {
                    _result = _locProjectLine2Item2s.Count();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ProjectLine", ex.ToString());
            }
            return _result;
        }

        public double UpdateTotalPL2I2(bool forceChangeEvents)
        {
            double _result = 0;
            try
            {
                double _locTotalAmount = 0;
                if (!IsLoading)
                {
                    XPCollection<ProjectLine2Item2> _locProjectLine2Item2s = new XPCollection<ProjectLine2Item2>(Session, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("ProjectLine2", this)));

                    if (_locProjectLine2Item2s != null && _locProjectLine2Item2s.Count() > 0)
                    {
                        foreach (ProjectLine2Item2 _locProjectLine2Item2 in _locProjectLine2Item2s)
                        {
                            _locTotalAmount = _locTotalAmount + _locProjectLine2Item2.TotalUnitAmount;
                        }
                        _result = _locTotalAmount;
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectLine " + ex.ToString());
            }
            return _result;
        }

        public double GetSumTotalPL2I2()
        {
            double _result = 0;
            try
            {
                double _locSumTotalPL2I2 = 0;

                if (!IsLoading)
                {
                    if (_projectHeader != null)
                    {
                        XPCollection<ProjectLine2> _locProjectLines2 = new XPCollection<ProjectLine2>(Session, new GroupOperator(GroupOperatorType.And,
                                                                 new BinaryOperator("ProjectHeader", this.ProjectHeader)));

                        if (_locProjectLines2 != null && _locProjectLines2.Count > 0)
                        {
                            foreach (ProjectLine2 _locProjectLine2 in _locProjectLines2)
                            {
                                _locSumTotalPL2I2 = _locSumTotalPL2I2 + _locProjectLine2.TotalPL2I2;
                            }
                        }
                        _result = _locSumTotalPL2I2;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ProjectLine", ex.ToString());
            }
            return _result;
        }

        //====

        public int GetTotalPL2S()
        {
            int _result = 0;
            try
            {
                XPCollection<ProjectLine2Service> _locProjectLine2Services = new XPCollection<ProjectLine2Service>(Session, new GroupOperator(GroupOperatorType.And,
                                                                           new BinaryOperator("ProjectLine2", this)));

                if (_locProjectLine2Services != null && _locProjectLine2Services.Count > 0)
                {
                    _result = _locProjectLine2Services.Count();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ProjectLine", ex.ToString());
            }
            return _result;
        }

        public double UpdateTotalPL2S(bool forceChangeEvents)
        {
            double _result = 0;
            try
            {
                double _locTotalAmount = 0;
                if (!IsLoading)
                {
                    XPCollection<ProjectLine2Service> _locProjectLine2Services = new XPCollection<ProjectLine2Service>(Session, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("ProjectLine2", this)));

                    if (_locProjectLine2Services != null && _locProjectLine2Services.Count() > 0)
                    {
                        foreach (ProjectLine2Service _locProjectLine2Service in _locProjectLine2Services)
                        {
                            _locTotalAmount = _locTotalAmount + _locProjectLine2Service.TotalUnitAmount;
                        }
                        _result = _locTotalAmount;
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ProjectLine " + ex.ToString());
            }
            return _result;
        }

        public double GetSumTotalPL2S()
        {
            double _result = 0;
            try
            {
                double _locSumTotalPL2S = 0;

                if (!IsLoading)
                {
                    if (_projectHeader != null)
                    {
                        XPCollection<ProjectLine2> _locProjectLines2 = new XPCollection<ProjectLine2>(Session, new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("ProjectHeader", this.ProjectHeader)));

                        if (_locProjectLines2 != null && _locProjectLines2.Count > 0)
                        {
                            foreach (ProjectLine2 _locProjectLine2 in _locProjectLines2)
                            {
                                _locSumTotalPL2S = _locSumTotalPL2S + _locProjectLine2.TotalPL2S;
                            }
                        }
                        _result = _locSumTotalPL2S;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ProjectLine", ex.ToString());
            }
            return _result;
        }

        #endregion CLM

    }
}