﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Production")]
    [RuleCombinationOfPropertiesIsUnique("InventoryProductionLineRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class InventoryProductionLine : FullDriveSysBaseObject
    {
        #region Default

        private bool _activeApproved1;
        private bool _activeApproved2;
        private bool _activeApproved3;
        private bool _activationPosting;
        private int _no;
        private string _code;
        private Item _item;
        private string _description;
        private double _dQtyBegining;
        private double _dQtyMinimal;
        private double _dQtyAvailable;
        private double _bruto;
        private double _netto;
        private double _tara;
        private double _dQtySale;
        private Location _location;
        private BinLocation _binLocation;
        private UnitOfMeasure _defaultUom;
        private string _lotNumber;
        private double _brutoActual;
        private double _nettoActual;
        private double _taraActual;
        private UnitOfMeasure _unitPackage;
        private Company _company;
        private InventoryProduction _InventoryProduction;
        private bool _select;
        private QCPassed _qcPassed;
        private int _postCountQcPassed;
        private int _postCountQcReject;
        private Status _status;
        private DateTime _statusDate;
        private int _prosesCountLot;
        private int _postedCount;
        private GlobalFunction _globFunc;

        public InventoryProductionLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).

            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.InventoryProductionLine);
                DateTime now = DateTime.Now;
                this.Status = CustomProcess.Status.Open;
                this.Select = false;
                this.QcPassed = QCPassed.None;
                this.StatusDate = now;
            }
        }

        //No
        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }
        protected override void OnDeleting()
        {
            base.OnDeleting();
            RecoveryDeleteNo();
        }

        #endregion Default

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("ProjectLineNoClose", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [ImmediatePostData()]
        [Appearance("InventoryProductionYellowColor", Criteria = "ActiveApproved1 = true", BackColor = "#ffeaa7")]
        [Appearance("InventoryProductionGreenColor", Criteria = "ActiveApproved2 = true", BackColor = "#81ecec")]
        [Appearance("InventoryProductionRedColor", Criteria = "ActiveApproved3 = true", BackColor = "#fd79a8")]
        [Appearance("InventoryProductionLineCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        //[ImmediatePostData()]
        [Appearance("InventoryProductionItemEnabled", Criteria = "ActivationPosting = true", Enabled = false)]
        public Item Item
        {
            get { return _item; }
            set
            {
                SetPropertyValue("Item", ref _item, value);
                //if (!IsLoading)
                //{
                //    if (this._item != null)
                //    {
                //        this.DefaultUOM = this._item.BasedUOM;
                //    }
                //}
            }
        }

        [Size(512)]
        [Appearance("InventoryProductionLineDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Appearance("InventoryProductionLineDefaultUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public UnitOfMeasure DefaultUOM
        {
            get { return _defaultUom; }
            set { SetPropertyValue("DefaultUOM", ref _defaultUom, value); }
        }

        [Browsable(false)]
        [Appearance("InventoryProductionLineDQtyBeginningEnabled", Criteria = "ActivationPosting = true", Enabled = false)]
        public double DQtyBegining
        {
            get { return _dQtyBegining; }
            set { SetPropertyValue("QtyBegining", ref _dQtyBegining, value); }
        }

        [Browsable(false)]
        [Appearance("InventoryProductionLineDQtyMinimalEnabled", Criteria = "ActivationPosting = true", Enabled = false)]
        public double DQtyMinimal
        {
            get { return _dQtyMinimal; }
            set { SetPropertyValue("QtyMinimal", ref _dQtyMinimal, value); }
        }

        [Appearance("InventoryProductionLineQtyAvailableEnabled", Criteria = "ActivationPosting = true", Enabled = false)]
        public double QtyAvailable
        {
            get { return _dQtyAvailable; }
            set { SetPropertyValue("QtyAvailable", ref _dQtyAvailable, value); }
        }

        [Browsable(false)]
        [Appearance("InventoryProductionLineTQtyAvailableEnabled", Enabled = false)]
        [ImmediatePostData()]
        public double TQtyAvailable
        {
            get
            {
                if (this.QtyAvailable > 0)
                {
                    return GetTotalQtyAvailable();
                }
                else
                {
                    return 0;
                }
            }
        }

        [Browsable(false)]
        [Appearance("InventoryProductionLineQtySaleEnabled", Criteria = "ActivationPosting = true", Enabled = false)]
        public double QtySale
        {
            get { return _dQtySale; }
            set { SetPropertyValue("QtySale", ref _dQtySale, value); }
        }

        [Appearance("InventoryProductionLineBrutoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Bruto
        {
            get { return _bruto; }
            set { SetPropertyValue("Bruto", ref _bruto, value); }
        }

        [Appearance("InventoryProductionLineNettoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Netto
        {
            get { return _netto; }
            set { SetPropertyValue("Netto", ref _netto, value); }
        }

        [Appearance("InventoryProductionLineTaraClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Tara
        {
            get { return _tara; }
            set { SetPropertyValue("Tara", ref _tara, value); }
        }

        [Appearance("InventoryProductionLineLotNumberEnabled", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public string LotNumber
        {
            get { return _lotNumber; }
            set { SetPropertyValue("LotNumber", ref _lotNumber, value); }
        }

        [Appearance("InventoryProductionLineLotNumberNotificationEnabled", Enabled = false)]
        public string LotNumberNotification
        {
            get
            {
                string _locName = null;
                if (this.LotNumber != null)
                {
                    InventoryProductionLine _locProdPlanLine = Session.FindObject<InventoryProductionLine>
                                                              (new GroupOperator(GroupOperatorType.And,
                                                               new BinaryOperator("LotNumber", this.LotNumber)));
                    if (_locProdPlanLine != null)
                    {
                        _locName = "Lot Number Available";
                    }
                    else
                    {
                        _locName = "Lot Number UnAvailable";
                    }
                    return String.Format("{0}", _locName);
                }
                return null;
            }
        }

        [Appearance("InventoryProductionLineBrutoActualClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double BrutoActual
        {
            get { return _brutoActual; }
            set { SetPropertyValue("BrutoActual", ref _brutoActual, value); }
        }

        [Appearance("InventoryProductionLineNettoActualClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double NettoActual
        {
            get { return _nettoActual; }
            set { SetPropertyValue("Netto", ref _nettoActual, value); }
        }

        [Appearance("InventoryProductionLineTaraActualClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double TaraActual
        {
            get { return _taraActual; }
            set { SetPropertyValue("TaraActual", ref _taraActual, value); }
        }

        [Appearance("InventoryProductionLineUnitPackageEnabled", Criteria = "ActivationPosting = true", Enabled = false)]
        public UnitOfMeasure UnitPackage
        {
            get { return _unitPackage; }
            set { SetPropertyValue("UnitPackage", ref _unitPackage, value); }
        }

        [Appearance("InventoryProductionComapanyEnabled", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        public Location Location
        {
            get { return _location; }
            set { SetPropertyValue("Location", ref _location, value); }
        }

        public BinLocation BinLocation
        {
            get { return _binLocation; }
            set { SetPropertyValue("BinLocation", ref _binLocation, value); }
        }

        [ImmediatePostData()]
        [Association("InventoryProduction-InventoryProductionLines")]
        [Appearance("InventoryProductionLineInventoryProductionEnabled", Enabled = false)]
        public InventoryProduction InventoryProduction
        {
            get { return _InventoryProduction; }
            set
            {
                SetPropertyValue("InventoryProduction", ref _InventoryProduction, value);
                if (!IsLoading)
                {
                    if (this._InventoryProduction != null)
                    {
                        if (this._InventoryProduction.DefaultUOM != null)
                        {
                            this.DefaultUOM = this._InventoryProduction.DefaultUOM;
                        }
                        if (this._InventoryProduction.Item != null)
                        {
                            this.Item = this._InventoryProduction.Item;
                        }
                        if (this._InventoryProduction.Company != null)
                        {
                            this.Company = this._InventoryProduction.Company;
                        }
                        if (this._InventoryProduction.Location != null)
                        {
                            this.Location = this._InventoryProduction.Location;
                        }
                        if (this._InventoryProduction.BinLocation != null)
                        {
                            this.BinLocation = this._InventoryProduction.BinLocation;
                        }
                    }
                }
            }
        }

        [Browsable(false)]
        [Appearance("InventoryProductionSelectEnabled", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        [Appearance("InventoryProductionQcPassedEnabled", Enabled = false)]
        public QCPassed QcPassed
        {
            get { return _qcPassed; }
            set { SetPropertyValue("QcPassed", ref _qcPassed, value); }
        }

        [Browsable(false)]
        [Appearance("InventoryProductionPostCountQcPassedEnabled", Enabled = false)]
        public int PostCountQcPassed
        {
            get { return _postCountQcPassed; }
            set { SetPropertyValue("PostCountQcPassed", ref _postCountQcPassed, value); }
        }

        [Browsable(false)]
        [Appearance("InventoryProductionPostCountQcRejectEnabled", Enabled = false)]
        public int PostCountQcReject
        {
            get { return _postCountQcReject; }
            set { SetPropertyValue("PostCountQcReject", ref _postCountQcReject, value); }
        }

        [Appearance("InventoryProductionStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("InventoryProductionStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Browsable(false)]
        [Appearance("InventoryProductionProsesCountLotEnabled", Enabled = false)]
        public int ProsesCountLot
        {
            get { return _prosesCountLot; }
            set { SetPropertyValue("ProsesCountLot", ref _prosesCountLot, value); }
        }

        [Browsable(false)]
        [Appearance("InventoryProductionPostedCountEnabled", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        [Browsable(false)]
        [Appearance("InventoryProductionLineActiveApproved1Close", Enabled = false)]
        public bool ActiveApproved1
        {
            get { return _activeApproved1; }
            set { SetPropertyValue("ActiveApproved1", ref _activeApproved1, value); }
        }

        [Browsable(false)]
        [Appearance("InventoryProductionLineActiveApproved2Close", Enabled = false)]
        public bool ActiveApproved2
        {
            get { return _activeApproved2; }
            set { SetPropertyValue("ActiveApproved2", ref _activeApproved2, value); }
        }

        [Browsable(false)]
        [Appearance("InventoryProductionLineActiveApproved3Close", Enabled = false)]
        public bool ActiveApproved3
        {
            get { return _activeApproved3; }
            set { SetPropertyValue("ActiveApproved3", ref _activeApproved3, value); }
        }

        #endregion Field

        //=== Code Only ====

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.InventoryProduction != null)
                    {
                        object _makRecord = Session.Evaluate<InventoryProductionLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("InventoryProduction=?", this.InventoryProduction));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryProductionLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.InventoryProduction != null)
                {
                    InventoryProduction _numHeader = Session.FindObject<InventoryProduction>
                                                     (new BinaryOperator("Code", this.InventoryProduction.Code));

                    XPCollection<InventoryProductionLine> _numLines = new XPCollection<InventoryProductionLine>
                                                                      (Session, new BinaryOperator("InventoryProduction", _numHeader),
                                                                       new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (InventoryProductionLine _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryProductionLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.InventoryProduction != null)
                {
                    InventoryProduction _numHeader = Session.FindObject<InventoryProduction>
                                                     (new BinaryOperator("Code", this.InventoryProduction.Code));

                    XPCollection<InventoryProductionLine> _numLines = new XPCollection<InventoryProductionLine>
                                                                     (Session, new GroupOperator(GroupOperatorType.And,
                                                                      new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                                      new BinaryOperator("InventoryProduction", _numHeader)),
                                                                      new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (InventoryProductionLine _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryProductionLine " + ex.ToString());
            }
        }

        #endregion No

        private double GetTotalQtyAvailable()
        {
            double _result = 0;
            try
            {
                double _locTotalQtyAvailable = 0;

                if (!IsLoading)
                {
                    XPCollection<InventoryProductionLine> _locInventoryProductionLines = new XPCollection<InventoryProductionLine>(Session,
                                                                                         new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("InventoryProduction", this.InventoryProduction)));

                    if (_locInventoryProductionLines.Count() >= 0)
                    {
                        foreach (InventoryProductionLine _locInventoryProductionLine in _locInventoryProductionLines)
                        {
                            _locTotalQtyAvailable = _locTotalQtyAvailable + _locInventoryProductionLine.QtyAvailable;
                        }
                        _result = _locTotalQtyAvailable;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = InventoryProductionLine" + ex.ToString());
            }

            return _result;
        }

    }
}