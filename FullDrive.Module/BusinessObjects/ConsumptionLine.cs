﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [NavigationItem("Production")]
    [DefaultProperty("Code")]
    [RuleCombinationOfPropertiesIsUnique("ConsumptionLineRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class ConsumptionLine : FullDriveSysBaseObject
    {
        #region Default

        private bool _activationPosting;
        private bool _select;
        private int _no;
        private string _code;
        private string _name;
        private Item _item;
        private Location _location;
        private BinLocation _binLocation;
        private string _description;
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasure;
        #region InisialisasiMaxQty
        private double _mxDQTY;
        private UnitOfMeasure _mxDUom;
        private double _mxQty;
        private UnitOfMeasure _mxUom;
        private double _mxTQty;
        #endregion InisialisasiMaxQty
        #region InisialisasiQty
        private double _dQty;
        private UnitOfMeasure _dUom;
        private double _qty;
        private UnitOfMeasure _uom;
        private double _tQty;
        #endregion InisialisasiQty
        #region InitialRemainQty
        private double _rmDQty;
        private double _rmQty;
        private double _rmTQty;
        #endregion InitialRemainQty
        #region InitialPostingQuantityTo
        private double _pDQty;
        private UnitOfMeasure _pDUom;
        private double _pQty;
        private UnitOfMeasure _pUom;
        private double _pTQty;
        #endregion InitialPostingQuantityTo
        #region InitialActualQty
        private double _aDQty;
        private UnitOfMeasure _aDUom;
        private double _aQty;
        private UnitOfMeasure _aUom;
        private double _aTQty;
        #endregion InitialActualQty
        #region InitialReturnQty
        private double _rTQty;
        #endregion InitialReturnQty
        private BillOfMaterialVersion _billOfMaterialVersion;
        private XPCollection<BillOfMaterial> _availableBillOfMaterial;
        private BillOfMaterial _billOfMaterial;
        private Status _status;
        private DateTime _statusDate;
        private int _processCount;
        private Consumption _consumption;
        private Company _company;
        private GlobalFunction _globalFunc;

        public ConsumptionLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
            if (!IsLoading)
            {
                _globalFunc = new GlobalFunction();
                this.Code = _globalFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ConsumptionLine);
                DateTime Now = DateTime.Now;
                this.Status = CustomProcess.Status.Open;
                this.StatusDate = Now;
            }
        }

        //No
        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }
        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        #endregion Default

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("ConsumptionLineSelectEnabled", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        [Appearance("ConsumptionLineNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ConsumptionLineCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("ConsumptionLineNameEnabled", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Appearance("ConsumptionLineItemEnabled", Enabled = false)]
        public Item Item
        {
            get { return _item; }
            set { SetPropertyValue("Item", ref _item, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        [Appearance("ConsumptionLineLocationEnabled", Criteria = "ActivationPosting = true", Enabled = false)]
        public Location Location
        {
            get { return _location; }
            set { SetPropertyValue("Location", ref _location, value); }
        }

        [DataSourceCriteria("Active = true And Location = '@This.Location'")]
        [Appearance("ConsumptionLineBinLocationEnabled", Criteria = "ActivationPosting = true", Enabled = false)]
        public BinLocation BinLocation
        {
            get { return _binLocation; }
            set { SetPropertyValue("BinLocation", ref _binLocation, value); }
        }

        [Size(512)]
        [Appearance("ConsumptionLineDescriptionEnabled", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Browsable(false)]
        public XPCollection<UnitOfMeasure> AvailableUnitOfMeasure
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;

                if (Item == null)
                {
                    _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session);
                }
                else
                {
                    List<string> _stringUOM = new List<string>();

                    XPCollection<ItemUnitOfMeasure> _locItemUnitOfMeasures = new XPCollection<ItemUnitOfMeasure>
                                                                             (Session, new GroupOperator(GroupOperatorType.And,
                                                                              new BinaryOperator("Item", this.Item),
                                                                              new BinaryOperator("Active", true)));

                    if (_locItemUnitOfMeasures != null && _locItemUnitOfMeasures.Count() > 0)
                    {
                        foreach (ItemUnitOfMeasure _locItemUnitOfMeasure in _locItemUnitOfMeasures)
                        {
                            _stringUOM.Add(_locItemUnitOfMeasure.UOM.Code);
                        }
                    }

                    IEnumerable<string> _stringArrayUOMDistinct = _stringUOM.Distinct();
                    string[] _stringArrayUOMList = _stringArrayUOMDistinct.ToArray();
                    if (_stringArrayUOMList.Length == 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArrayUOMList.Length > 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                                else
                                {
                                    _endString = " OR [Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session, CriteriaOperator.Parse(_fullString));
                    }

                }
                return _availableUnitOfMeasure;
            }
        }

        #region MaxDefaultQty

        [Appearance("ConsumptionLineMxDQtyClose", Enabled = false)]
        [ImmediatePostData()]
        public double MxDQty
        {
            get { return _mxDQTY; }
            set
            {
                SetPropertyValue("MxDQty", ref _mxDQTY, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        [Appearance("ConsumptionLineMxDUOMClose", Enabled = false)]
        public UnitOfMeasure MxDUOM
        {
            get { return _mxDUom; }
            set
            {
                SetPropertyValue("MxDUOM", ref _mxDUom, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("ConsumptionLineMxQtyClose", Enabled = false)]
        public double MxQty
        {
            get { return _mxQty; }
            set
            {
                SetPropertyValue("MxQty", ref _mxQty, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        [Appearance("ConsumptionLineMxUOMClose", Enabled = false)]
        public UnitOfMeasure MxUOM
        {
            get { return _mxUom; }
            set
            {
                SetPropertyValue("MxUOM", ref _mxUom, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [Appearance("ConsumptionLineMxTQtyClose", Enabled = false)]
        public double MxTQty
        {
            get { return _mxTQty; }
            set { SetPropertyValue("MxTQty", ref _mxTQty, value); }
        }

        #endregion MaxDefaultQty

        #region DefaultQty

        [Appearance("ItemConsumptionLineDQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double DQty
        {
            get { return _dQty; }
            set
            {
                SetPropertyValue("DQty", ref _dQty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    if (this.MxDQty > 0)
                    {
                        if (this.ProcessCount > 0)
                        {
                            if (this.RmDQty == 0)
                            {
                                if (this._dQty > this.MxDQty || this._dQty <= this.MxDQty)
                                {
                                    this._dQty = 0;
                                    SetTotalQty();
                                }
                            }

                            if (this.RmDQty > 0)
                            {
                                if (this._dQty > this.RmDQty)
                                {
                                    this._dQty = this.RmDQty;
                                    SetTotalQty();
                                }
                            }
                        }
                        else
                        {
                            if (this._dQty > this.MxDQty)
                            {
                                this._dQty = this.MxDQty;
                                SetTotalQty();
                            }
                        }
                    }
                }
            }
        }


        [Appearance("ConsumptionLineDUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM
        {
            get { return _dUom; }
            set
            {
                SetPropertyValue("DUOM", ref _dUom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("ConsumptionLineQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double Qty
        {
            get { return _qty; }
            set
            {
                SetPropertyValue("Qty", ref _qty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    if (this.ProcessCount > 0)
                    {
                        if (this.RmQty == 0)
                        {
                            if (this._qty > this.MxQty || this._qty <= this.MxQty)
                            {
                                this._qty = 0;
                                SetTotalQty();
                            }
                        }

                        if (this.RmDQty > 0)
                        {
                            if (this._qty > this.RmDQty)
                            {
                                this._qty = this.RmDQty;
                                SetTotalQty();
                            }
                        }
                    }
                    else
                    {
                        if (this._qty > this.MxDQty)
                        {
                            this._qty = this.MxDQty;
                            SetTotalQty();
                        }
                    }
                }
            }
        }

        [Appearance("ConsumptionLineUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set
            {
                SetPropertyValue("UOM", ref _uom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("ConsumptionLineTotalQtyEnabled", Enabled = false)]
        public double TQty
        {
            get { return _tQty; }
            set { SetPropertyValue("TotalQty", ref _tQty, value); }
        }

        #endregion DefaultQty

        #region RemainQty

        [ImmediatePostData()]
        [Appearance("ConsumptionLineRmDQtyClose", Enabled = false)]
        public double RmDQty
        {
            get { return _rmDQty; }
            set
            {
                SetPropertyValue("RmDQty", ref _rmDQty, value);
                if (!IsLoading)
                {
                    SetRemainTotalQty();
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("ConsumptionLineRmQtyClose", Enabled = false)]
        public double RmQty
        {
            get { return _rmQty; }
            set
            {
                SetPropertyValue("RmQty", ref _rmQty, value);
                if (!IsLoading)
                {
                    SetRemainTotalQty();
                }
            }
        }

        [Appearance("ConsumptionLineRmTQtyClose", Enabled = false)]
        public double RmTQty
        {
            get { return _rmTQty; }
            set { SetPropertyValue("RmTQty", ref _rmTQty, value); }
        }

        #endregion RemainQty

        #region DefaultPostingQty

        [Appearance("ConsumptionLinePDQtyClose", Enabled = false)]
        public double PDQty
        {
            get { return _pDQty; }
            set
            {
                SetPropertyValue("PDQty", ref _pDQty, value);
                if (!IsLoading)
                {
                    SetTotalPQty();
                }
            }
        }

        [Appearance("ConsumptionLinePDUOMClose", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure PDUOM
        {
            get { return _pDUom; }
            set
            {
                SetPropertyValue("PDUOM", ref _pDUom, value);
                if (!IsLoading)
                {
                    SetTotalPQty();
                }
            }
        }

        [Appearance("ConsumptionLinePQtyToClose", Enabled = false)]
        public double PQty
        {
            get { return _pQty; }
            set
            {
                SetPropertyValue("PQty", ref _pQty, value);
                if (!IsLoading)
                {
                    SetTotalPQty();
                }
            }
        }

        [Appearance("ConsumptionLinePUOMClose", Enabled = false)]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure PUOM
        {
            get { return _pUom; }
            set
            {
                SetPropertyValue("PUOM", ref _pUom, value);
                if (!IsLoading)
                {
                    SetTotalPQty();
                }
            }
        }

        [Appearance("ConsumptionLinePTQtyEnabled", Enabled = false)]
        public double PTQty
        {
            get { return _pTQty; }
            set { SetPropertyValue("PTQty", ref _pTQty, value); }
        }

        #endregion DefaultPostingQty

        #region ActualQty

        [Appearance("ConsumptionLineADQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double ADQty
        {
            get { return _aDQty; }
            set
            {
                SetPropertyValue("ADQty", ref _aDQty, value);
                if (!IsLoading)
                {
                    SetTotalAQty();
                    SetTotalReturnQty();

                    if (this.MxDQty > 0)
                    {
                        if (this.ProcessCount > 0)
                        {
                            if (this.RmDQty == 0)
                            {
                                if (this._dQty > this.MxDQty || this._dQty <= this.MxDQty)
                                {
                                    this._dQty = 0;
                                    SetTotalQty();
                                }
                            }

                            if (this.RmDQty > 0)
                            {
                                if (this._dQty > this.RmDQty)
                                {
                                    this._dQty = this.RmDQty;
                                    SetTotalQty();
                                }
                            }
                        }

                        if (this.ProcessCount == 0)
                        {
                            if (this._dQty > this.MxDQty)
                            {
                                this._dQty = this.MxDQty;
                                SetTotalQty();
                            }
                        }
                    }
                }
            }
        }

        [Appearance("ConsumptionLineADUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure ADUOM
        {
            get { return _aDUom; }
            set
            {
                SetPropertyValue("ADUOM", ref _aDUom, value);
                if (!IsLoading)
                {
                    SetTotalAQty();
                }
            }
        }

        [Appearance("ConsumptionLineAQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double AQty
        {
            get { return _aQty; }
            set
            {
                SetPropertyValue("AQty", ref _aQty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    if (this.ProcessCount > 0)
                    {
                        if (this.RmQty == 0)
                        {
                            if (this._aQty > this.MxQty || this._aQty <= this.MxQty)
                            {
                                this._aQty = 0;
                                SetTotalAQty();
                            }
                        }

                        if (this.RmDQty > 0)
                        {
                            if (this._aQty > this.RmDQty)
                            {
                                this._aQty = this.RmDQty;
                                SetTotalAQty();
                            }
                        }
                    }
                    else
                    {
                        if (this._aQty > this.MxDQty)
                        {
                            this._aQty = this.MxDQty;
                            SetTotalQty();
                        }
                    }
                }
            }
        }

        [Appearance("ConsumptionLineAUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure AUOM
        {
            get { return _aUom; }
            set
            {
                SetPropertyValue("AUOM", ref _aUom, value);
                if (!IsLoading)
                {
                    SetTotalAQty();
                }
            }
        }

        [Appearance("ConsumptionLineTotalAQtyEnabled", Enabled = false)]
        public double ATQty
        {
            get { return _aTQty; }
            set { SetPropertyValue("ATQty", ref _aTQty, value); }
        }

        #endregion ActualQty

        #region ReturnQty

        [Appearance("ConsumptionLineRTQtyEnabled", Enabled = false)]
        public double RTQty
        {
            get { return _rTQty; }
            set { SetPropertyValue("RTQty", ref _rTQty, value); }
        }

        #endregion ReturnQty

        [ImmediatePostData()]
        [Appearance("ConsumptionLineBOMVEnabled", Enabled = false)]
        public BillOfMaterialVersion BillOfMaterialVersion
        {
            get { return _billOfMaterialVersion; }
            set { SetPropertyValue("BillOfMaterialVersion", ref _billOfMaterialVersion, value); }
        }

        [Browsable(false)]
        public XPCollection<BillOfMaterial> AvailableBillOfMaterial
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                if (Item == null)
                {
                    _availableBillOfMaterial = new XPCollection<BillOfMaterial>(Session);
                }
                else
                {
                    List<string> _stringBOM = new List<string>();

                    XPCollection<ItemBillOfMaterial> _locItemBillOfMaterials = new XPCollection<ItemBillOfMaterial>
                                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Item", this.Item),
                                                                               new BinaryOperator("BillOfMaterialVersion", this.BillOfMaterialVersion),
                                                                               new BinaryOperator("Active", true)));

                    if (_locItemBillOfMaterials != null && _locItemBillOfMaterials.Count() > 0)
                    {
                        foreach (ItemBillOfMaterial _locItemBillOfMaterial in _locItemBillOfMaterials)
                        {
                            _stringBOM.Add(_locItemBillOfMaterial.BillOfMaterial.Code);
                        }
                    }

                    IEnumerable<string> _stringArrayBOMDistinct = _stringBOM.Distinct();
                    string[] _stringArrayBOMList = _stringArrayBOMDistinct.ToArray();
                    if (_stringArrayBOMList.Length == 1)
                    {
                        for (int i = 0; i < _stringArrayBOMList.Length; i++)
                        {
                            BillOfMaterial _locBOM = Session.FindObject<BillOfMaterial>(new BinaryOperator("Code", _stringArrayBOMList[i]));
                            if (_locBOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locBOM.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArrayBOMList.Length > 1)
                    {
                        for (int i = 0; i < _stringArrayBOMList.Length; i++)
                        {
                            BillOfMaterial _locBOM = Session.FindObject<BillOfMaterial>(new BinaryOperator("Code", _stringArrayBOMList[i]));
                            if (_locBOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locBOM.Code + "'";
                                }
                                else
                                {
                                    _endString = " OR [Code]=='" + _locBOM.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableBillOfMaterial = new XPCollection<BillOfMaterial>(Session, CriteriaOperator.Parse(_fullString));
                    }

                }
                return _availableBillOfMaterial;

            }
        }

        [DataSourceProperty("AvailableBillOfMaterial", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        [Appearance("ConsumptionLineBOMEnabled", Enabled = false)]
        public BillOfMaterial BillOfMaterial
        {
            get { return _billOfMaterial; }
            set { SetPropertyValue("BillOfMaterial", ref _billOfMaterial, value); }
        }

        [Appearance("ConsumptionLineStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("ConsumptionLineStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("ConsumptionLineProcessCountEnabled", Enabled = false)]
        public int ProcessCount
        {
            get { return _processCount; }
            set { SetPropertyValue("ProcessCount", ref _processCount, value); }
        }

        [ImmediatePostData()]
        [Association("Consumption-ConsumptionLines")]
        [Appearance("ConsumptionLineConsumptionEnabled", Enabled = false)]
        public Consumption Consumption
        {
            get { return _consumption; }
            set { SetPropertyValue("Consumption", ref _consumption, value); }
        }

        //[Browsable(false)]
        [Appearance("ConsumptionLineCompanyEnabled", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        #endregion Field

        //==== Code Only ====

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.Consumption != null)
                    {
                        object _makRecord = Session.Evaluate<ConsumptionLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("Consumption=?", this.Consumption));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ConsumptionLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.Consumption != null)
                {
                    Consumption _numHeader = Session.FindObject<Consumption>
                                             (new BinaryOperator("Code", this.Consumption.Code));

                    XPCollection<ConsumptionLine> _numLines = new XPCollection<ConsumptionLine>
                                                              (Session, new BinaryOperator("Consumption", _numHeader),
                                                               new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (ConsumptionLine _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ConsumptionLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.Consumption != null)
                {
                    Consumption _numHeader = Session.FindObject<Consumption>
                                             (new BinaryOperator("Code", this.Consumption.Code));

                    XPCollection<ConsumptionLine> _numLines = new XPCollection<ConsumptionLine>
                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                               new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                               new BinaryOperator("Consumption", _numHeader)),
                                                               new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (ConsumptionLine _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ConsumptionLine " + ex.ToString());
            }
        }

        #endregion No

        #region Set

        private void SetTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.Consumption != null)
                {
                    if (this.Item != null && this.UOM != null && this.DUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty * _locItemUOM.DefaultConversion + this.DQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty / _locItemUOM.Conversion + this.DQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.Qty + this.DQty;
                            }

                            this.TQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.Qty + this.DQty;
                        this.TQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ConsumptionLine " + ex.ToString());
            }
        }

        private void SetMaxTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.Consumption != null)
                {
                    if (this.Item != null && this.MxUOM != null && this.MxDUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.MxUOM),
                                                         new BinaryOperator("DefaultUOM", this.MxDUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.MxQty * _locItemUOM.DefaultConversion + this.MxDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.MxQty / _locItemUOM.Conversion + this.MxDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.MxQty + this.MxDQty;
                            }

                            this.MxTQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.MxQty + this.MxDQty;
                        this.MxTQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkRequestLine " + ex.ToString());
            }
        }

        private void SetRemainTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.Consumption != null)
                {
                    ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                    if (_locItemUOM != null)
                    {
                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.RmQty * _locItemUOM.DefaultConversion + this.RmDQty;
                        }
                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.RmQty / _locItemUOM.Conversion + this.RmDQty;
                        }
                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.RmQty + this.RmDQty;
                        }

                        this.RmTQty = _locInvLineTotal;
                    }
                    else
                    {
                        _locInvLineTotal = this.RmQty + this.RmDQty;
                        this.RmTQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkRequisitionLine " + ex.ToString());
            }
        }

        private void SetTotalPQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.Consumption != null)
                {
                    if (this.Item != null && this.UOM != null && this.DUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.PQty * _locItemUOM.DefaultConversion + this.PDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.PQty / _locItemUOM.Conversion + this.PDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.PQty + this.PDQty;
                            }

                            this.PTQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.PQty + this.PDQty;
                        this.PTQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkRequisitionLine " + ex.ToString());
            }
        }

        private void SetTotalAQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.Consumption != null)
                {
                    if (this.Item != null && this.UOM != null && this.DUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.AQty * _locItemUOM.DefaultConversion + this.ADQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.AQty / _locItemUOM.Conversion + this.ADQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.AQty + this.ADQty;
                            }

                            this.ATQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.AQty + this.ADQty;
                        this.ATQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ConsumptionLine " + ex.ToString());
            }
        }

        private void SetTotalReturnQty()
        {
            try
            {
                DateTime now = DateTime.Now;

                if (this.Consumption != null)
                {
                    if (this.MxTQty != 0 && this.TQty != 0 && this.ATQty != 0 && this.ADUOM != null || this.AUOM != null && this.TQty > this.ATQty)
                    {
                        this.RTQty = this.TQty - this.ATQty;
                    }
                    else
                    {
                        this.RTQty = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ConsumptionLine " + ex.ToString());
            }
        }

        #endregion Set

    }
}