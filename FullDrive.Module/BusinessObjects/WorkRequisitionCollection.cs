﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [NavigationItem("PPIC")]
    [DefaultProperty("Code")]
    [RuleCombinationOfPropertiesIsUnique("WorkRequisitionCollectionRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    public class WorkRequisitionCollection : FullDriveSysBaseObject
    {
        #region Default

        private bool _activationPosting;
        private string _code;
        private WorkRequisition _workRequisition;
        private Status _status;
        private DateTime _statusDate;
        private Company _company;
        private SalesOrder _salesOrder;
        private ProjectHeader _projectHeader;
        private string _signCode;
        private GlobalFunction _globFunc;

        public WorkRequisitionCollection(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.WorkRequisitionCollection);
                this.Status = Status.Open;
                this.StatusDate = now;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #endregion Default

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("WorkRequisitionCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        public WorkRequisition WorkRequisition
        {
            get { return _workRequisition; }
            set { SetPropertyValue("WorkRequisition", ref _workRequisition, value); }
        }

        [Appearance("WorkRequisitionStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("WorkRequisitionStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Association("WorkRequisitionCollection-WorkRequisitionCollectionLines")]
        public XPCollection<WorkRequisitionCollectionLine> WorkRequisitionCollectionLines
        {
            get { return GetCollection<WorkRequisitionCollectionLine>("WorkRequisitionCollectionLines"); }
        }

        public SalesOrder SalesOrder
        {
            get { return _salesOrder; }
            set { SetPropertyValue("SalesOrder", ref _salesOrder, value); }
        }

        public ProjectHeader ProjectHeader
        {
            get { return _projectHeader; }
            set { SetPropertyValue("ProjectHeader", ref _projectHeader, value); }
        }

        [Appearance("workRequisitionSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        //[Browsable(false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        #endregion Field

    }
}