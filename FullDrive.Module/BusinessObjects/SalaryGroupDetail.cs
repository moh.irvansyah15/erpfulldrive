﻿        #region Default
using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;
using DevExpress.ExpressApp.ConditionalAppearance;
using System.Collections;
using FullDrive.Module.CustomProcess;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Payroll")]
    [RuleCombinationOfPropertiesIsUnique("SalaryGroupDetailRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class SalaryGroupDetail : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private string _name;
        private SalaryGroup _salaryGroup;
        private SalaryComponent _component;
        private double _componentValue;
        private string _componenyValueText;
        private string _componentFormula;
        private GlobalFunction _globFunc;
        private SalaryGroupDetail _salaryGroupDetail;
        public SalaryGroupDetail(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.SalaryGroupDetail);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }
        #endregion

        #region Field
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        public SalaryComponent Component
        {
            get { return _component; }
            set { SetPropertyValue("Component", ref _component, value); }
        }


        public double ComponentValue
        {
            get { return _componentValue; }
            set { SetPropertyValue("ComponentValue", ref _componentValue, value); }
        }

        public string ComponentValueText
        {
            get { return _componenyValueText; }
            set { SetPropertyValue("ComponentValueText", ref _componenyValueText, value); }
        }

        [Size(500)]
        public string ComponentFormula
        {
            get { return _componentFormula; }
            set { SetPropertyValue("ComponentFormula", ref _componentFormula, value); }
        }

        [Association("SalaryGroup-SalaryGroupDetails")]
        [Appearance("SalaryGroupDetailSalaryGroupEnabled", Enabled = false)]
        public SalaryGroup SalaryGroup
        {
            get { return _salaryGroup; }
            set { SetPropertyValue("SalaryGroup", ref _salaryGroup, value); }
        }
        
        [Association("SalaryGroupDetail-SalaryGroupDetails")]
        public SalaryGroupDetail SalaryGroupDetails
        {
            get { return _salaryGroupDetail; }
            set { SetPropertyValue("SalaryGroupDetail", ref _salaryGroupDetail, value);}
        }

        [Association("SalaryGroupDetail-SalaryGroupDetails")]
        public XPCollection<SalaryGroupDetail> SalaryGroupDetail2
        {
            get { return GetCollection<SalaryGroupDetail>("SalaryGroupDetails"); }
        }
        #endregion

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}
        //

        #region Calculate
        public void Calculate()
        {
            if (ComponentFormula != "" & this.SalaryGroupDetail2.Count > 0)
            {
                //var MyEval = new EvalFormula(SalaryGroupDetails);
                //ComponentValue = MyEval.Parse(ComponentFormula);
            }
        }
        #endregion
        //
    }
}