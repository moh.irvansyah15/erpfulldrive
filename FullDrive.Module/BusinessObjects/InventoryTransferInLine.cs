﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.SystemModule;
using FullDrive.Module.CustomProcess;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Inventory")]
    [RuleCombinationOfPropertiesIsUnique("InventoryTransferInLineRuleUnique", DefaultContexts.Save, "Code")]

    //[ListViewFilter("AllDataInventoryTransferInLine", "", "All Data", "All Data In Inventory Transfer In Line", 1, true)]
    //[ListViewFilter("OpenInventoryTransferInLine", "Status = 'Open'", "Open", "Open Data Status In Inventory Transfer In Line", 2, true)]
    //[ListViewFilter("ProgressInventoryTransferInLine", "Status = 'Progress'", "Progress", "Progress Data Status In Inventory Transfer In Line", 3, true)]
    //[ListViewFilter("LockInventoryTransferInLine", "Status = 'Close'", "Close", "Close Data Status In Inventory Transfer In Line", 4, true)]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class InventoryTransferInLine : FullDriveSysBaseObject
    {
        #region Default

        private bool _activationPosting;
        private int _no;
        private bool _select;
        private string _code;
        private string _name;
        private Item _item;
        private string _description;
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasure;
        #region InisialisasiMaxQty
        private double _mxDQTY;
        private UnitOfMeasure _mxDUom;
        private double _mxQty;
        private UnitOfMeasure _mxUom;
        private double _mxTQty;
        #endregion InisialisasiMaxQty
        #region InisialisasiDefaultQty
        private double _dQty;
        private UnitOfMeasure _dUom;
        private double _qty;
        private UnitOfMeasure _uom;
        private double _tQty;
        #endregion InisialisasiDefaultQty
        #region InisialisasiRemainQty
        private double _rmDQty;
        private double _rmQty;
        private double _rmTQty;
        #endregion InisialisasiRemainQty
        #region InitialPostingQuantityTo
        private double _pDQty;
        private UnitOfMeasure _pDUom;
        private double _pQty;
        private UnitOfMeasure _pUom;
        private double _pTQty;
        #endregion InitialPostingQuantityTo
        private XPCollection<Location> _availableLocation;
        private bool _closeLocation;
        private Location _location;
        private XPCollection<BinLocation> _availableBinLocation;
        private BinLocation _binLocation;
        private StockType _stockType;
        private bool _stockControlling;
        private XPCollection<Location> _availableLocationTo;
        private Location _locationTo;
        private XPCollection<BinLocation> _availableBinLocationTo;
        private BinLocation _binLocationTo;
        private StockType _stockTypeTo;
        private DateTime _estimatedDate;
        private DateTime _actualDate;
        private Status _status;
        private DateTime _statusDate;
        private ReturnType _returnType;
        private int _processCount;
        private string _signCode;
        private InventoryTransferIn _inventoryTransferIn;
        private Company _company;
        private string _userAccess;
        private GlobalFunction _globFunc;

        public InventoryTransferInLine(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.InventoryTransferInLine);
                this.Status = Status.Open;
                this.StatusDate = now;
                this.StockType = StockType.Good;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        #endregion Default

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("InventoryTransferInLineNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [Appearance("InventoryTransferInLineSelectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("InventoryTransferInLineCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("InventoryTransferInLineNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [ImmediatePostData()]
        [Appearance("InventoryTransferInLineItemClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Item Item
        {
            get { return _item; }
            set
            {
                SetPropertyValue("Item", ref _item, value);
                if (!IsLoading)
                {
                    if (this._item != null)
                    {
                        this.DUOM = this._item.BasedUOM;
                    }
                }
            }
        }

        [Appearance("InventoryTransferInLineDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Browsable(false)]
        public XPCollection<UnitOfMeasure> AvailableUnitOfMeasure
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                if (Item == null)
                {
                    _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session);
                }
                else
                {
                    List<string> _stringUOM = new List<string>();

                    XPCollection<ItemUnitOfMeasure> _locItemUnitOfMeasures = new XPCollection<ItemUnitOfMeasure>
                                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Item", this.Item),
                                                                               new BinaryOperator("Active", true)));

                    if (_locItemUnitOfMeasures != null && _locItemUnitOfMeasures.Count() > 0)
                    {
                        foreach (ItemUnitOfMeasure _locItemUnitOfMeasure in _locItemUnitOfMeasures)
                        {
                            _stringUOM.Add(_locItemUnitOfMeasure.UOM.Code);
                        }
                    }

                    IEnumerable<string> _stringArrayUOMDistinct = _stringUOM.Distinct();
                    string[] _stringArrayUOMList = _stringArrayUOMDistinct.ToArray();
                    if (_stringArrayUOMList.Length == 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArrayUOMList.Length > 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                                else
                                {
                                    _endString = " OR [Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session, CriteriaOperator.Parse(_fullString));
                    }

                }
                return _availableUnitOfMeasure;

            }
        }

        #region MaxDefaultQty

        [Appearance("InventoryTransferInLineMxDQtyClose", Enabled = false)]
        [ImmediatePostData()]
        public double MxDQty
        {
            get { return _mxDQTY; }
            set
            {
                SetPropertyValue("MxDQty", ref _mxDQTY, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        [Appearance("InventoryTransferInLineMxDUOMClose", Enabled = false)]
        public UnitOfMeasure MxDUOM
        {
            get { return _mxDUom; }
            set
            {
                SetPropertyValue("MxDUOM", ref _mxDUom, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("InventoryTransferInLineMxQtyClose", Enabled = false)]
        public double MxQty
        {
            get { return _mxQty; }
            set
            {
                SetPropertyValue("MxQty", ref _mxQty, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        [Appearance("InventoryTransferInLineMxUOMClose", Enabled = false)]
        public UnitOfMeasure MxUOM
        {
            get { return _mxUom; }
            set
            {
                SetPropertyValue("MxUOM", ref _mxUom, value);
                if (!IsLoading)
                {
                    SetMaxTotalQty();
                }
            }
        }

        [Appearance("InventoryTransferInLineMxTQtyClose", Enabled = false)]
        public double MxTQty
        {
            get { return _mxTQty; }
            set { SetPropertyValue("MxTQty", ref _mxTQty, value); }
        }

        #endregion MaxDefaultQty

        #region DefaultQty

        [Appearance("InventoryTransferInLineDQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double DQty
        {
            get { return _dQty; }
            set
            {
                SetPropertyValue("DQty", ref _dQty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    if(this.MxDQty > 0)
                    {
                        if (this.ProcessCount > 0)
                        {
                            if (this.RmDQty == 0)
                            {
                                if (this._dQty > this.MxDQty || this._dQty <= this.MxDQty)
                                {
                                    this._dQty = 0;
                                    SetTotalQty();
                                }
                            }

                            if (this.RmDQty > 0)
                            {
                                if (this._dQty > this.RmDQty)
                                {
                                    this._dQty = this.RmDQty;
                                    SetTotalQty();
                                }
                            }
                        }

                        if (this.ProcessCount == 0)
                        {
                            if (this._dQty > this.MxDQty)
                            {
                                this._dQty = this.MxDQty;
                                SetTotalQty();
                            }
                        }
                    } 
                }
            }
        }

        [Appearance("InventoryTransferInLineDUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM
        {
            get { return _dUom; }
            set
            {
                SetPropertyValue("DUOM", ref _dUom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("InventoryTransferInLineQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double Qty
        {
            get { return _qty; }
            set
            {
                SetPropertyValue("Qty", ref _qty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                    if(this.MxQty > 0)
                    {
                        if (this.ProcessCount > 0)
                        {
                            if (this.RmQty == 0)
                            {
                                if (this._qty > this.MxQty || this._qty <= this.MxQty)
                                {
                                    this._qty = 0;
                                    SetTotalQty();
                                }
                            }

                            if (this.RmQty > 0)
                            {
                                if (this._qty > this.RmQty)
                                {
                                    this._qty = this.RmQty;
                                    SetTotalQty();
                                }
                            }
                        }

                        if (this.ProcessCount == 0)
                        {
                            if (this._qty > this.MxQty)
                            {
                                this._qty = this.MxQty;
                                SetTotalQty();
                            }
                        }
                    }    
                }
            }
        }

        [Appearance("InventoryTransferInLineUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure UOM
        {
            get { return _uom; }
            set
            {
                SetPropertyValue("UOM", ref _uom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("InventoryTransferInLineTotalQtyEnabled", Enabled = false)]
        public double TQty
        {
            get { return _tQty; }
            set { SetPropertyValue("TQty", ref _tQty, value); }
        }

        #endregion DefaultQty

        #region RemainQty

        [ImmediatePostData()]
        [Appearance("InventoryTransferInLineRmDQtyClose", Enabled = false)]
        public double RmDQty
        {
            get { return _rmDQty; }
            set
            {
                SetPropertyValue("RmDQty", ref _rmDQty, value);
                if (!IsLoading)
                {
                    SetRemainTotalQty();
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("InventoryTransferInLineRmQtyClose", Enabled = false)]
        public double RmQty
        {
            get { return _rmQty; }
            set
            {
                SetPropertyValue("RmQty", ref _rmQty, value);
                if (!IsLoading)
                {
                    SetRemainTotalQty();
                }
            }
        }

        [Appearance("InventoryTransferInLineRmTQtyClose", Enabled = false)]
        public double RmTQty
        {
            get { return _rmTQty; }
            set { SetPropertyValue("RmTQty", ref _rmTQty, value); }
        }

        #endregion RemainQty

        #region PostingQty

        [Appearance("InventoryTransferInLinePDQtyClose", Enabled = false)]
        public double PDQty
        {
            get { return _pDQty; }
            set
            {
                SetPropertyValue("PDQty", ref _pDQty, value);
                if (!IsLoading)
                {
                    SetTotalPQty();
                }
            }
        }

        [Appearance("InventoryTransferInLinePDUOMClose", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure PDUOM
        {
            get { return _pDUom; }
            set
            {
                SetPropertyValue("PDUOM", ref _pDUom, value);
                if (!IsLoading)
                {
                    SetTotalPQty();
                }
            }
        }

        [Appearance("InventoryTransferInLinePQtyToClose", Enabled = false)]
        public double PQty
        {
            get { return _pQty; }
            set
            {
                SetPropertyValue("PQty", ref _pQty, value);
                if (!IsLoading)
                {
                    SetTotalPQty();
                }
            }
        }

        [Appearance("InventoryTransferInLinePUOMClose", Enabled = false)]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure PUOM
        {
            get { return _pUom; }
            set
            {
                SetPropertyValue("PUOM", ref _pUom, value);
                if (!IsLoading)
                {
                    SetTotalPQty();
                }
            }
        }

        [Appearance("InventoryTransferInLinePTQtyEnabled", Enabled = false)]
        public double PTQty
        {
            get { return _pTQty; }
            set { SetPropertyValue("PTQty", ref _pTQty, value); }
        }

        #endregion PostingQty

        #region Location

        [Browsable(false)]
        public XPCollection<Location> AvailableLocation
        {
            get
            {
                _userAccess = SecuritySystem.CurrentUserName;
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                string _locLocationCode = null;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                if (_locUserAccess != null)
                {
                    if(this.InventoryTransferIn != null)
                    {
                        if ((this.InventoryTransferIn.TransferType == DirectionType.External || this.InventoryTransferIn.TransferType == DirectionType.Internal)
                            && this.InventoryTransferIn.InventoryMovingType == InventoryMovingType.Receive)
                        {
                            List<string> _stringLocation = new List<string>();

                            XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                                                      (Session, new GroupOperator(GroupOperatorType.And,
                                                                                       new BinaryOperator("UserAccess", _locUserAccess),
                                                                                       new BinaryOperator("ObjectList", ObjectList.InventoryTransferIn),
                                                                                       new BinaryOperator("Owner", true),
                                                                                       new BinaryOperator("TransferType", this.InventoryTransferIn.TransferType),
                                                                                       new BinaryOperator("InventoryMovingType", this.InventoryTransferIn.InventoryMovingType),
                                                                                       new BinaryOperator("LocationType", LocationType.Transit),
                                                                                       new BinaryOperator("Active", true)));

                            if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                            {
                                foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                                {
                                    if(_locWhsSetupDetail.Location != null)
                                    {
                                        if(_locWhsSetupDetail.Location.Code != null)
                                        {
                                            _locLocationCode = _locWhsSetupDetail.Location.Code;
                                            _stringLocation.Add(_locLocationCode);
                                        }
                                    }
                                    
                                }
                            }

                            IEnumerable<string> _stringArrayLocationFromDistinct = _stringLocation.Distinct();
                            string[] _stringArrayLocationFromList = _stringArrayLocationFromDistinct.ToArray();
                            if (_stringArrayLocationFromList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                                {
                                    Location _locLocationFrom = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                    if (_locLocationFrom != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locLocationFrom.Code + "'";
                                        }
                                    }
                                }
                            }
                            else if (_stringArrayLocationFromList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                                {
                                    Location _locLocationFrom = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                    if (_locLocationFrom != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locLocationFrom.Code + "'";
                                        }
                                        else
                                        {
                                            _endString = " OR [Code]=='" + _locLocationFrom.Code + "'";
                                        }
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null)
                            {
                                _availableLocation = new XPCollection<Location>(Session, CriteriaOperator.Parse(_fullString));
                            }
                        }
                    }
                }
                else
                {
                    _availableLocation = new XPCollection<Location>(Session);
                }

                return _availableLocation;

            }
        }

        [Browsable(false)]
        public bool CloseLocation
        {
            get { return _closeLocation; }
            set { SetPropertyValue("CloseLocation", ref _closeLocation, value); }
        }

        [Appearance("InventoryTransferInLineLocationClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("InventoryTransferInLineLocationClose1", Criteria = "CloseLocation = true", Enabled = false)]
        [DataSourceProperty("AvailableLocation", DataSourcePropertyIsNullMode.SelectAll)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public Location Location
        {
            get { return _location; }
            set { SetPropertyValue("Location", ref _location, value); }
        }

        [Browsable(false)]
        public XPCollection<BinLocation> AvailableBinLocation
        {
            get
            {
                _userAccess = SecuritySystem.CurrentUserName;
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                string _locBinLocationCode = null;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                if (_locUserAccess != null)
                {
                    if (this.Location != null)
                    {
                        if (this.InventoryTransferIn != null)
                        {

                            if ((this.InventoryTransferIn.TransferType == DirectionType.External || this.InventoryTransferIn.TransferType == DirectionType.Internal)
                                && this.InventoryTransferIn.InventoryMovingType == InventoryMovingType.Receive)
                            {
                                List<string> _stringBinLocation = new List<string>();

                                XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                                                          (Session, new GroupOperator(GroupOperatorType.And,
                                                                                           new BinaryOperator("UserAccess", _locUserAccess),
                                                                                           new BinaryOperator("Location", this.Location),
                                                                                           new BinaryOperator("Owner", true),
                                                                                           new BinaryOperator("Active", true)));

                                if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                                {
                                    foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                                    {
                                        if(_locWhsSetupDetail.BinLocation != null)
                                        {
                                            if(_locWhsSetupDetail.BinLocation.Code != null)
                                            {
                                                _locBinLocationCode = _locWhsSetupDetail.BinLocation.Code;
                                                _stringBinLocation.Add(_locBinLocationCode);
                                            }
                                        }
                                        
                                    }
                                }

                                IEnumerable<string> _stringArrayBinLocationFromDistinct = _stringBinLocation.Distinct();
                                string[] _stringArrayBinLocationFromList = _stringArrayBinLocationFromDistinct.ToArray();
                                if (_stringArrayBinLocationFromList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayBinLocationFromList.Length; i++)
                                    {
                                        BinLocation _locBinLocationFrom = Session.FindObject<BinLocation>(new BinaryOperator("Code", _stringArrayBinLocationFromList[i]));
                                        if (_locBinLocationFrom != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locBinLocationFrom.Code + "'";
                                            }
                                        }
                                    }
                                }
                                else if (_stringArrayBinLocationFromList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayBinLocationFromList.Length; i++)
                                    {
                                        BinLocation _locBinLocationFrom = Session.FindObject<BinLocation>(new BinaryOperator("Code", _stringArrayBinLocationFromList[i]));
                                        if (_locBinLocationFrom != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locBinLocationFrom.Code + "'";
                                            }
                                            else
                                            {
                                                _endString = " OR [Code]=='" + _locBinLocationFrom.Code + "'";
                                            }
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availableBinLocation = new XPCollection<BinLocation>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                        }
                    }
                }
                else
                {
                    _availableBinLocation = new XPCollection<BinLocation>(Session);
                }

                return _availableBinLocation;

            }
        }

        [Appearance("InventoryTransferInLineBinLocationClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableBinLocation", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public BinLocation BinLocation
        {
            get { return _binLocation; }
            set { SetPropertyValue("BinLocation", ref _binLocation, value); }
        }

        [Appearance("InventoryTransferInLineStockTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockType StockType
        {
            get { return _stockType; }
            set { SetPropertyValue("StockType", ref _stockType, value); }
        }

        #endregion Location

        #region LocationTo

        [ImmediatePostData()]
        [Appearance("InventoryTransferInLineStockControllingClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool StockControlling
        {
            get { return _stockControlling; }
            set { SetPropertyValue("StockControlling", ref _stockControlling, value); }
        }

        [Browsable(false)]
        public XPCollection<Location> AvailableLocationTo
        {
            get
            {
                _userAccess = SecuritySystem.CurrentUserName;
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                string _locLocationCode = null;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                List<string> _stringLocation = new List<string>();
                if (_locUserAccess != null)
                {
                    if (this.InventoryTransferIn != null)
                    {
                        #region InventoryTransferIn
                        if (this.InventoryTransferIn.InventoryMovingType == InventoryMovingType.Receive)
                        {
                            XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                                                  (Session, new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("UserAccess", _locUserAccess),
                                                                                   new BinaryOperator("Owner", true),
                                                                                   new BinaryOperator("Active", true)));

                            if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                            {
                                foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                                {
                                    if (_locWhsSetupDetail.Location != null)
                                    {
                                        if (_locWhsSetupDetail.Location.Code != null)
                                        {
                                            _locLocationCode = _locWhsSetupDetail.Location.Code;
                                            _stringLocation.Add(_locLocationCode);
                                        }
                                    }
                                }
                            }

                            IEnumerable<string> _stringArrayLocationToDistinct = _stringLocation.Distinct();
                            string[] _stringArrayLocationToList = _stringArrayLocationToDistinct.ToArray();
                            if (_stringArrayLocationToList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayLocationToList.Length; i++)
                                {
                                    Location _locLocationTo = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationToList[i]));
                                    if (_locLocationTo != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locLocationTo.Code + "'";
                                        }
                                    }
                                }
                            }
                            else if (_stringArrayLocationToList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayLocationToList.Length; i++)
                                {
                                    Location _locLocationTo = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationToList[i]));
                                    if (_locLocationTo != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locLocationTo.Code + "'";
                                        }
                                        else
                                        {
                                            _endString = " OR [Code]=='" + _locLocationTo.Code + "'";
                                        }
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null)
                            {
                                _availableLocationTo = new XPCollection<Location>(Session, CriteriaOperator.Parse(_fullString));
                            }
                        }
                        #endregion InventoryTransferIn
                    }
                }
                else
                {
                    _availableLocationTo = new XPCollection<Location>(Session);
                }

                return _availableLocationTo;

            }
        }

        [Appearance("InventoryTransferInLineLocationToEnabled", Criteria = "StockControlling = false", Enabled = false)]
        [Appearance("InventoryTransferInLineLocationToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableLocationTo", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        [ImmediatePostData]
        public Location LocationTo
        {
            get { return _locationTo; }
            set { SetPropertyValue("LocationTo", ref _locationTo, value); }
        }

        [Browsable(false)]
        public XPCollection<BinLocation> AvailableBinLocationTo
        {
            get
            {
                _userAccess = SecuritySystem.CurrentUserName;
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                string _locBinLocationCode = null;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                List<string> _stringBinLocation = new List<string>();
                if (_locUserAccess != null)
                {
                    if (this.LocationTo != null)
                    {
                        if (this.InventoryTransferIn != null)
                        {
                            #region InventoryTransferIn
                            if (this.InventoryTransferIn.InventoryMovingType == InventoryMovingType.Receive)
                            {
                                XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                                                  (Session, new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("UserAccess", _locUserAccess),
                                                                                   new BinaryOperator("Location", this.LocationTo),
                                                                                   new BinaryOperator("Owner", true),
                                                                                   new BinaryOperator("Active", true)));

                                if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                                {
                                    foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                                    {
                                        if (_locWhsSetupDetail.BinLocation != null)
                                        {
                                            if (_locWhsSetupDetail.BinLocation.Code != null)
                                            {
                                                _locBinLocationCode = _locWhsSetupDetail.BinLocation.Code;
                                                _stringBinLocation.Add(_locBinLocationCode);
                                            }
                                        }
                                    }
                                }

                                IEnumerable<string> _stringArrayBinLocationToDistinct = _stringBinLocation.Distinct();
                                string[] _stringArrayBinLocationToList = _stringArrayBinLocationToDistinct.ToArray();
                                if (_stringArrayBinLocationToList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayBinLocationToList.Length; i++)
                                    {
                                        BinLocation _locBinLocationTo = Session.FindObject<BinLocation>(new BinaryOperator("Code", _stringArrayBinLocationToList[i]));
                                        if (_locBinLocationTo != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locBinLocationTo.Code + "'";
                                            }
                                        }
                                    }
                                }
                                else if (_stringArrayBinLocationToList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayBinLocationToList.Length; i++)
                                    {
                                        BinLocation _locBinLocationTo = Session.FindObject<BinLocation>(new BinaryOperator("Code", _stringArrayBinLocationToList[i]));
                                        if (_locBinLocationTo != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locBinLocationTo.Code + "'";
                                            }
                                            else
                                            {
                                                _endString = " OR [Code]=='" + _locBinLocationTo.Code + "'";
                                            }
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availableBinLocationTo = new XPCollection<BinLocation>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                            #endregion InventoryTransferIn
                        }

                    }
                }
                else
                {
                    _availableBinLocationTo = new XPCollection<BinLocation>(Session);
                }

                return _availableBinLocationTo;

            }
        }

        [Appearance("InventoryTransferInLineBinLocationToEnabled", Criteria = "StockControlling = false", Enabled = false)]
        [Appearance("InventoryTransferInLineBinLocationToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableBinLocationTo", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public BinLocation BinLocationTo
        {
            get { return _binLocationTo; }
            set { SetPropertyValue("BinLocationTo", ref _binLocationTo, value); }
        }

        [Appearance("InventoryTransferInLineStockTypeToEnabled", Criteria = "StockControlling = false", Enabled = false)]
        [Appearance("InventoryTransferInLineStockTypeToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockType StockTypeTo
        {
            get { return _stockTypeTo; }
            set { SetPropertyValue("StockTypeTo", ref _stockTypeTo, value); }
        }

        #endregion LocationTo

        [Appearance("InventoryTransferInLineestimatedDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime EstimatedDate
        {
            get { return _estimatedDate; }
            set { SetPropertyValue("EstimatedDate", ref _estimatedDate, value); }
        }

        [Appearance("InventoryTransferInLineActualDateEnabled", Enabled = false)]
        public DateTime ActualDate
        {
            get { return _actualDate; }
            set { SetPropertyValue("ActualDate", ref _actualDate, value); }
        }

        [Appearance("InventoryTransferInLineStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("InventoryTransferInLineStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }
        
        [Appearance("InventoryTransferInLineReturnTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public ReturnType ReturnType
        {
            get { return _returnType; }
            set { SetPropertyValue("ReturnType", ref _returnType, value); }
        }

        [Appearance("InventoryTransferInLineProcessCountEnabled", Enabled = false)]
        public int ProcessCount
        {
            get { return _processCount; }
            set { SetPropertyValue("ProcessCount", ref _processCount, value); }
        }

        [Appearance("InventoryTransferInLineSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [Appearance("InventoryTransferInLineInventoryTransferInEnabled", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        [ImmediatePostData()]
        [Association("InventoryTransferIn-InventoryTransferInLines")]
        public InventoryTransferIn InventoryTransferIn
        {
            get { return _inventoryTransferIn; }
            set
            {
                SetPropertyValue("InventoryTransferIn", ref _inventoryTransferIn, value);
                if (!IsLoading)
                {
                    if (_inventoryTransferIn != null)
                    {
                        if(_inventoryTransferIn.Location != null)
                        {
                            this.CloseLocation = true;
                            this.Location = _inventoryTransferIn.Location;
                        }
                        this.EstimatedDate = _inventoryTransferIn.EstimatedDate;
                        this.StockType = _inventoryTransferIn.StockType;
                        if (this._inventoryTransferIn.Company != null)
                        {
                            this.Company = this._inventoryTransferIn.Company;
                        }
                    }
                }
            }
        }

        [Association("InventoryTransferInLine-InventoryTransferInLots")]
        public XPCollection<InventoryTransferInLot> InventoryTransferInLots
        {
            get { return GetCollection<InventoryTransferInLot>("InventoryTransferInLots"); }
        }

        [Browsable(false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        #endregion Field

        //===== Code Only =====

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.InventoryTransferIn != null)
                    {
                        object _makRecord = Session.Evaluate<InventoryTransferInLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("InventoryTransferIn=?", this.InventoryTransferIn));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferInLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.InventoryTransferIn != null)
                {
                    InventoryTransferIn _numHeader = Session.FindObject<InventoryTransferIn>
                                                (new BinaryOperator("Code", this.InventoryTransferIn.Code));

                    XPCollection<InventoryTransferInLine> _numLines = new XPCollection<InventoryTransferInLine>
                                                (Session, new BinaryOperator("InventoryTransferIn", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (InventoryTransferInLine _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferInLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.InventoryTransferIn != null)
                {
                    InventoryTransferIn _numHeader = Session.FindObject<InventoryTransferIn>
                                                (new BinaryOperator("Code", this.InventoryTransferIn.Code));

                    XPCollection<InventoryTransferInLine> _numLines = new XPCollection<InventoryTransferInLine>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("InventoryTransferIn", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (InventoryTransferInLine _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferInLine " + ex.ToString());
            }
        }

        #endregion No

        #region Set

        private void SetTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.InventoryTransferIn != null)
                {
                    ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                    if (_locItemUOM != null)
                    {
                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.Qty * _locItemUOM.DefaultConversion + this.DQty;
                        }
                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.Qty / _locItemUOM.Conversion + this.DQty;
                        }
                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.Qty + this.DQty;
                        }

                        this.TQty = _locInvLineTotal;
                    }
                    else
                    {
                        _locInvLineTotal = this.Qty + this.DQty;
                        this.TQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferInLine " + ex.ToString());
            }
        }

        private void SetMaxTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.InventoryTransferIn != null)
                {
                    ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.MxUOM),
                                                         new BinaryOperator("DefaultUOM", this.MxDUOM),
                                                         new BinaryOperator("Active", true)));
                    if (_locItemUOM != null)
                    {
                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.MxQty * _locItemUOM.DefaultConversion + this.MxDQty;
                        }
                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.MxQty / _locItemUOM.Conversion + this.MxDQty;
                        }
                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.MxQty + this.MxDQty;
                        }

                        this.MxTQty = _locInvLineTotal;
                    }
                    else
                    {
                        _locInvLineTotal = this.MxQty + this.MxDQty;
                        this.MxTQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferInLine " + ex.ToString());
            }
        }

        private void SetRemainTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.InventoryTransferIn != null)
                {
                    ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                    if (_locItemUOM != null)
                    {
                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.RmQty * _locItemUOM.DefaultConversion + this.RmDQty;
                        }
                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.RmQty / _locItemUOM.Conversion + this.RmDQty;
                        }
                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                        {
                            _locInvLineTotal = this.RmQty + this.RmDQty;
                        }

                        this.RmTQty = _locInvLineTotal;
                    }
                    else
                    {
                        _locInvLineTotal = this.RmQty + this.RmDQty;
                        this.RmTQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferInLine " + ex.ToString());
            }
        }

        private void SetTotalPQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.InventoryTransferIn != null)
                {
                    if (this.Item != null && this.UOM != null && this.DUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.UOM),
                                                         new BinaryOperator("DefaultUOM", this.DUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.PQty * _locItemUOM.DefaultConversion + this.PDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.PQty / _locItemUOM.Conversion + this.PDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.PQty + this.PDQty;
                            }

                            this.PTQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.PQty + this.PDQty;
                        this.PTQty = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferInLine " + ex.ToString());
            }
        }

        #endregion Set

    }
}