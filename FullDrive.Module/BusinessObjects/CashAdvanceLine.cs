﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.ConditionalAppearance;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using System.IO;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("CashAdvanceLineRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class CashAdvanceLine : FullDriveSysBaseObject
    {
        #region Default

        private GlobalFunction _globFunc;
        private bool _activationPosting;
        private string _code;
        private string _name;
        private CashAdvance _cashAdvance;
        private Employee _employee;
        private DateTime _requestDate;
        private CashAdvanceType _cashAdvanceType;
        private double _cashAdvanceAmount;
        private DateTime _requiredDate;
        private string _description;
        private double _approvedAmount;
        private Status _status;
        private DateTime _statusDate;
        private int _processCount;
        private string _currentUser;
        private bool _activeApprovedAmount;
        //private bool _openCompany;
        private Company _company;
        //private bool _openDepartment;
        private Department _department;

        public CashAdvanceLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).

            _currentUser = SecuritySystem.CurrentUserName;
            this._globFunc = new GlobalFunction();
            this._code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.CashAdvanceLine);
            this._status = Status.Open;
            this._requestDate = DateTime.Now;
            this._statusDate = DateTime.Now;
            this._requiredDate = DateTime.Now;
        }

        #endregion Default

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [ImmediatePostData()]
        [Appearance("CashAdvanceCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [ImmediatePostData()]
        [Appearance("CashAdvanceLineNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Name
        {
            get
            {
                if (Code != null)
                {
                    if (Employee != null)
                    {
                        UpdateName();
                    }
                }
                return _name;
            }
            set
            {
                SetPropertyValue("Name", ref _name, value);
                UpdateName();
            }
        }

        [Association("CashAdvance-CashAdvanceLines")]
        [Appearance("CashAdvanceLineCashAdvanceClose", Enabled = false)]
        public CashAdvance CashAdvance
        {
            get { return _cashAdvance; }
            set { SetPropertyValue("CashAdvance", ref _cashAdvance, value); }
        }

        [ImmediatePostData()]
        [Appearance("CashAdvanceLineEmployeeClose", Enabled = false)]
        public Employee Employee
        {
            get
            {
                if (CashAdvance != null)
                {
                    this._employee = this._cashAdvance.Employee;
                }
                return _employee;
            }
            set
            {
                SetPropertyValue("Employee", ref _employee, value);
                UpdateName();
            }
        }

        [Appearance("CashAdvanceLineRequestDateClose", Enabled = false)]
        public DateTime RequestDate
        {
            get { return _requestDate; }
            set { SetPropertyValue("RequestDate", ref _requestDate, value); }
        }

        [Appearance("CashAdvanceLineCashAdvanceTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public CashAdvanceType CashAdvanceType
        {
            get { return _cashAdvanceType; }
            set { SetPropertyValue("CashAdvaceType", ref _cashAdvanceType, value); }
        }

        [Appearance("CashAdvanceLineCashAdvanceAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double CashAdvanceAmount
        {
            get { return _cashAdvanceAmount; }
            set { SetPropertyValue("CashAdvanceAmount", ref _cashAdvanceAmount, value); }
        }

        [Appearance("CashAdvanceLineRequiredDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime RequiredDate
        {
            get { return _requiredDate; }
            set { SetPropertyValue("RequiredDate", ref _requiredDate, value); }
        }

        [Size(512)]
        [Appearance("CashAdvanceLineDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [ImmediatePostData()]
        [Appearance("CashAdvanceLineApprovedAmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Appearance("CashAdvanceLineApprovedAmountEnabled", Criteria = "ActiveApprovedAmount = false", Enabled = false)]
        public double ApprovedAmount
        {
            get { return _approvedAmount; }
            set
            {
                SetPropertyValue("ApprovedAmount", ref _approvedAmount, value);
                if (!IsLoading)
                {
                    double _result = this._approvedAmount;
                    if (this._cashAdvanceAmount > 0)
                    {
                        if (this._approvedAmount > this.CashAdvanceAmount)
                        {
                            this._approvedAmount = 0;
                        }
                        else
                            if (this._approvedAmount < this.CashAdvanceAmount)
                        {
                            this._approvedAmount = _result;
                        }
                    }
                }
            }
        }

        [Appearance("CashAdvanceLineStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("CashAdvanceLineStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("CashAdvanceLineProcessCountEnabled", Enabled = false)]
        public int ProcessCount
        {
            get { return _processCount; }
            set { SetPropertyValue("ProcessCount", ref _processCount, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public bool ActiveApprovedAmount
        {
            get { return _activeApprovedAmount; }
            set { SetPropertyValue("ActiveApprovedAmount", ref _activeApprovedAmount, value); }
        }

        //[ImmediatePostData]
        //[Browsable(false)]
        //public bool OpenCompany
        //{
        //    get { return _openCompany; }
        //    set { SetPropertyValue("OpenCompany", ref _openCompany, value); }
        //}

        //[Appearance("CashAdvanceLineCompanyClose", Criteria = "OpenCompany = false", Enabled = false)]
        //[ImmediatePostData()]
        //[DataSourceCriteria("Active = true")]

        [Browsable(false)]
        [Appearance("CashAdvanceLineCompanyEnabled", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        //[ImmediatePostData]
        //[Browsable(false)]
        //public bool OpenDepartment
        //{
        //    get { return _openDepartment; }
        //    set { SetPropertyValue("OpenDepartment", ref _openDepartment, value); }
        //}

        //[Appearance("CashAdvanceLineDepartmentClose", Criteria = "OpenDepartment = false", Enabled = false)]
        //[ImmediatePostData()]
        //[DataSourceCriteria("Active = true")]

        [Browsable(false)]
        [Appearance("CashAdvanceLineDepartmentEnabled", Enabled = false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        #endregion Field

        //==== Code Only =====

        #region UpdateName

        private void UpdateName()
        {
            try
            {
                this._name = string.Format("Cash Advance Line {0} [ {1} ]", Code, Employee.Name);
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CashAdvanceLine" + ex.ToString());
            }
        }

        #endregion UpdateName

    }
}