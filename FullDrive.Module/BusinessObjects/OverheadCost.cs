﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Cost Of Goods Sold")]
    [RuleCombinationOfPropertiesIsUnique("OverheadCostRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class OverheadCost : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private int _no;
        private string _code;
        private string _process;
        private Machine _machine;
        private Item _item;
        private string _size;
        private double _ampere;
        private double _volt;
        private double _time;
        private double _sqrt3;
        private double _alpha;
        private double _kwh;
        private double _mpm;
        private GlobalFunction _globFunc;

        public OverheadCost(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if(!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(Session.DataLayer, ObjectList.OverheadCost);
                this.Volt = 380;
                this.Time = 0.016666667;
                this.Sqrt3 = 1.732050808;
                this.Alpha = 0.86;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            RecoveryDeleteNo();
        }

        [Appearance("OverheadCostNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        public string Process
        {
            get { return _process; }
            set { SetPropertyValue("Process", ref _process, value); }
        }

        [DataSourceCriteria("Active = true")]
        public Machine Machine
        {
            get { return _machine; }
            set { SetPropertyValue("Machine", ref _machine, value); }
        }

        [Association("Item-OverheadCosts")]
        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        public Item Item
        {
            get { return _item; }
            set { SetPropertyValue("Item", ref _item, value); }
        }

        public string Size
        {
            get { return _size; }
            set { SetPropertyValue("Size", ref _size, value); }
        }

        [ImmediatePostData()]
        public double Ampere
        {
            get { return _ampere; }
            set
            {
                SetPropertyValue("Ampere", ref _ampere, value);
                if(!IsLoading)
                {
                    SetKWH();
                }
            }
        }

        [ImmediatePostData]
        public double Volt
        {
            get { return _volt; }
            set
            {
                SetPropertyValue("Volt", ref _volt, value);
                if (!IsLoading)
                {
                    SetKWH();
                }
            }
        }

        [ImmediatePostData]
        public double Time
        {
            get { return _time; }
            set
            {
                SetPropertyValue("Time", ref _time, value);
                if (!IsLoading)
                {
                    SetKWH();
                }
            }
        }

        [ImmediatePostData]
        public double Sqrt3
        {
            get { return _sqrt3; }
            set
            {
                SetPropertyValue("Sqrt3", ref _sqrt3, value);
                if (!IsLoading)
                {
                    SetKWH();
                }
            }
        }

        [ImmediatePostData]
        public double Alpha
        {
            get { return _alpha; }
            set
            {
                SetPropertyValue("Alpha", ref _alpha, value);
                if (!IsLoading)
                {
                    SetKWH();
                }
            }
        }

        public double KWH
        {
            get { return _kwh; }
            set { SetPropertyValue("KWH", ref _kwh, value); }
        }

        public double MPM
        {
            get { return _mpm; }
            set { SetPropertyValue("MPM", ref _mpm, value); }
        }

        [Persistent("TotalOverheadCostElectric")]
        public double TotalOverheadCostElectric
        {
            get
            {
                double _result = 0;
                int _totalLineOverheadCostElectric = 0;
                if (!IsLoading)
                {
                    _totalLineOverheadCostElectric = Convert.ToInt32(Session.Evaluate<OverheadCost>(CriteriaOperator.Parse("OverheadCostElectrics.Count"), CriteriaOperator.Parse("Oid=?", Oid)));
                    if (_totalLineOverheadCostElectric > 0)
                    {
                        _result = UpdateTotalOverheadCostElectric(true);
                    }
                }
                return _result;
            }
        }

        [Persistent("TotalOverheadCostEmployee")]
        public double TotalOverheadCostEmployee
        {
            get
            {
                double _result = 0;
                int _totalLineOverheadCostEmployee = 0;
                if (!IsLoading)
                {
                    _totalLineOverheadCostEmployee = Convert.ToInt32(Session.Evaluate<OverheadCost>(CriteriaOperator.Parse("OverheadCostEmployees.Count"), CriteriaOperator.Parse("Oid=?", Oid)));
                    if (_totalLineOverheadCostEmployee > 0)
                    {
                        _result = UpdateTotalOverheadCostEmployee(true);
                    }
                }
                return _result;
            }
        }

        [Persistent("TotalOverheadCostDepreciation")]
        public double TotalOverheadCostDepreciation
        {
            get
            {
                double _result = 0;
                int _totalLineOverheadCostDepreciation = 0;
                if (!IsLoading)
                {
                    _totalLineOverheadCostDepreciation = Convert.ToInt32(Session.Evaluate<OverheadCost>(CriteriaOperator.Parse("OverheadCostDepreciations.Count"), CriteriaOperator.Parse("Oid=?", Oid)));
                    if (_totalLineOverheadCostDepreciation > 0)
                    {
                        _result = UpdateTotalOverheadCostDepreciation(true);
                    }
                }
                return _result;
            }
        }

        [Association("OverheadCost-OverheadCostElectrics")]
        public XPCollection<OverheadCostElectric> OverheadCostElectrics
        {
            get { return GetCollection<OverheadCostElectric>("OverheadCostElectrics"); }
        }

        [Association("OverheadCost-OverheadCostEmployees")]
        public XPCollection<OverheadCostEmployee> OverheadCostEmployees
        {
            get { return GetCollection<OverheadCostEmployee>("OverheadCostEmployees"); }
        }

        [Association("OverheadCost-OverheadCostDepreciations")]
        public XPCollection<OverheadCostDepreciation> OverheadCostDepreciations
        {
            get { return GetCollection<OverheadCostDepreciation>("OverheadCostDepreciations"); }
        }

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}

        //================================================== Code Only ==================================================

        #region Numbering
        public void UpdateNo()
        {
            try
            {
                if (!IsLoading)
                {
                    if (!(Session is NestedUnitOfWork) && Session.IsNewObject(this))
                    {
                        if (this.Item != null)
                        {
                            object _makRecord = Session.Evaluate<OverheadCost>(CriteriaOperator.Parse("Max(No)"),
                                                CriteriaOperator.Parse("Item=?", this.Item));
                            this.No = Convert.ToInt32(_makRecord) + 1;
                            this.Save();
                            RecoveryUpdateNo();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = OverheadCost " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.Item != null)
                {
                    Item _numHeader = Session.FindObject<Item>
                                                (new BinaryOperator("Code", this.Item.Code));

                    XPCollection<OverheadCost> _numLines = new XPCollection<OverheadCost>
                                                (Session, new BinaryOperator("Item", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (OverheadCost _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                        //Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = OverheadCost " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.Item != null)
                {
                    Item _numHeader = Session.FindObject<Item>
                                                (new BinaryOperator("Code", this.Item.Code));

                    XPCollection<OverheadCost> _numLines = new XPCollection<OverheadCost>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("Item", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (OverheadCost _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                        //Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = OverheadCost " + ex.ToString());
            }
        }
        #endregion Numbering

        private void SetKWH()
        {
            try
            {
                this._kwh = (this._ampere * this._volt * this._time * this._sqrt3 * this._alpha) / 1000;
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = OverheadCost " + ex.ToString());
            }
        }

        public double UpdateTotalOverheadCostElectric(bool forceChangeEvents)
        {
            double _result = 0;
            try
            {
                double _locTotalOverheadCostElectric = 0;
                if (!IsLoading)
                {
                    OverheadCostElectric _locOverheadCostElectric = Session.FindObject<OverheadCostElectric>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("OverheadCost", this)));
                    if (_locOverheadCostElectric != null)
                    {
                        _locTotalOverheadCostElectric = _locTotalOverheadCostElectric + _locOverheadCostElectric.TotalElectricCost;
                        
                    }
                    _result = _locTotalOverheadCostElectric;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = OverheadCostElectric " + ex.ToString());
            }
            return _result;
        }

        public double UpdateTotalOverheadCostEmployee(bool forceChangeEvents)
        {
            double _result = 0;
            try
            {
                double _locTotalOverheadCostEmployee = 0;
                if (!IsLoading)
                {
                    OverheadCostEmployee _locOverheadCostEmployee = Session.FindObject<OverheadCostEmployee>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("OverheadCost", this)));
                    if (_locOverheadCostEmployee != null)
                    {
                        _locTotalOverheadCostEmployee = _locTotalOverheadCostEmployee + _locOverheadCostEmployee.TotalCostEmployee;

                    }
                    _result = _locTotalOverheadCostEmployee;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = OverheadCostElectric " + ex.ToString());
            }
            return _result;
        }

        public double UpdateTotalOverheadCostDepreciation(bool forceChangeEvents)
        {
            double _result = 0;
            try
            {
                double _locTotalOverheadCostDepreciation = 0;
                if (!IsLoading)
                {
                    OverheadCostDepreciation _locOverheadCostDepreciation = Session.FindObject<OverheadCostDepreciation>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("OverheadCost", this)));
                    if (_locOverheadCostDepreciation != null)
                    {
                        _locTotalOverheadCostDepreciation = _locTotalOverheadCostDepreciation + _locOverheadCostDepreciation.TotalDepreciation;

                    }
                    _result = _locTotalOverheadCostDepreciation;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = OverheadCostElectric " + ex.ToString());
            }
            return _result;
        }

    }
}