﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [NavigationItem("PPIC")]
    [DefaultProperty("Code")]
    [RuleCombinationOfPropertiesIsUnique("WorkRequisitionCollectionLineRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    public class WorkRequisitionCollectionLine : FullDriveSysBaseObject
    {
        #region Default

        private bool _activationPosting;
        private string _code;
        private WorkRequisition _workRequisition;
        private WorkRequisitionCollection _workRequisitionCollection;
        private Status _status;
        private DateTime _statusDate;
        private GlobalFunction _globFunc;

        public WorkRequisitionCollectionLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).

            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.WorkRequisitionCollectionLine);
                this.Status = Status.Open;
                this.StatusDate = now;
            }
        }

        #endregion Default

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("WorkRequisitionCollectionLineCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        public WorkRequisition WorkRequisition
        {
            get { return _workRequisition; }
            set { SetPropertyValue("WorkRequisition", ref _workRequisition, value); }
        }

        [Appearance("WorkRequisitionCollectionLineStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("WorkRequisitionCollectionLineStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("WorkRequisitionCollectionLineWorkRequisitionCollectionEnabled", Enabled = false)]
        [Association("WorkRequisitionCollection-WorkRequisitionCollectionLines")]
        public WorkRequisitionCollection WorkRequisitionCollection
        {
            get { return _workRequisitionCollection; }
            set { SetPropertyValue("WorkRequisitionCollection", ref _workRequisitionCollection, value); }
        }

        #endregion Default

    }
}