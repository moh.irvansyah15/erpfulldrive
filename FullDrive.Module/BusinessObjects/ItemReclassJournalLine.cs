﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Inventory")]
    [RuleCombinationOfPropertiesIsUnique("ItemReclassJournalLineRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class ItemReclassJournalLine : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private int _no;
        private string _code;
        private string _name;
        private Item _itemFrom;
        private XPCollection<Location> _availableLocationFrom;
        private XPCollection<BinLocation> _availableBinLocationFrom;
        private Location _locationFrom;
        private BinLocation _binLocationFrom;
        private StockType _stockTypeFrom;
        #region InitialDefaultQuantityFrom
        private double _dQtyFrom;
        private UnitOfMeasure _dUomFrom;
        private double _qtyFrom;
        private UnitOfMeasure _uomFrom;
        private double _tQtyFrom;
        #endregion InitialDefaultQuantityFrom
        private XPCollection<Location> _availableLocationTo;
        private XPCollection<BinLocation> _availableBinLocationTo;
        private Location _locationTo;
        private BinLocation _binLocationTo;
        private StockType _stockTypeIn;
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasure;
        private ItemReclassJournal _itemReclassJournal;
        private string _userAccess;
        private GlobalFunction _globFunc;

        public ItemReclassJournalLine(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ItemReclassJournal);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("ItemReclassJournalLineNoClose", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ItemReclassJournalLineCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("ItemReclassJournalLineItemClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Item ItemFrom
        {
            get { return _itemFrom; }
            set
            {
                SetPropertyValue("ItemFrom", ref _itemFrom, value);
                if (!IsLoading)
                {
                    if (this._itemFrom != null)
                    {
                        this.DUOM_From = this._itemFrom.BasedUOM;
                        //this.PDUOM = this._itemFrom.BasedUOM;
                    }
                }
            }
        }

        #region From
        #region LocationFrom

        [Browsable(false)]
        public XPCollection<Location> AvailableLocationFrom
        {
            get
            {
                _userAccess = SecuritySystem.CurrentUserName;
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                string _locLocationCode = null;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                List<string> _stringLocation = new List<string>();
                if (_locUserAccess != null)
                {
                    if (this.ItemReclassJournal != null)
                    {
                        #region TransferOut
                        if (this.ItemReclassJournal.InventoryMovingType == InventoryMovingType.Adjust || this.ItemReclassJournal.InventoryMovingType == InventoryMovingType.Deformed)
                        {
                            XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                                                  (Session, new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("UserAccess", _locUserAccess),
                                                                                   new BinaryOperator("Owner", true),
                                                                                   new BinaryOperator("Active", true)));

                            if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                            {
                                foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                                {
                                    if (_locWhsSetupDetail.Location != null)
                                    {
                                        if (_locWhsSetupDetail.Location.Code != null)
                                        {
                                            _locLocationCode = _locWhsSetupDetail.Location.Code;
                                            _stringLocation.Add(_locLocationCode);
                                        }
                                    }
                                }
                            }

                            IEnumerable<string> _stringArrayLocationFromDistinct = _stringLocation.Distinct();
                            string[] _stringArrayLocationFromList = _stringArrayLocationFromDistinct.ToArray();
                            if (_stringArrayLocationFromList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                                {
                                    Location _locLocationFrom = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                    if (_locLocationFrom != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locLocationFrom.Code + "'";
                                        }
                                    }
                                }
                            }
                            else if (_stringArrayLocationFromList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                                {
                                    Location _locLocationFrom = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                    if (_locLocationFrom != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locLocationFrom.Code + "'";
                                        }
                                        else
                                        {
                                            _endString = " OR [Code]=='" + _locLocationFrom.Code + "'";
                                        }
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null)
                            {
                                _availableLocationFrom = new XPCollection<Location>(Session, CriteriaOperator.Parse(_fullString));
                            }

                        }
                        #endregion TransferOut
                    }
                }
                else
                {
                    _availableLocationFrom = new XPCollection<Location>(Session);
                }

                return _availableLocationFrom;

            }
        }

        [Appearance("ItemReclassJournalLineLocationFromClose", Enabled = false)]
        [DataSourceProperty("AvailableLocationFrom", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        [ImmediatePostData()]
        public Location LocationFrom
        {
            get { return _locationFrom; }
            set { SetPropertyValue("LocationFrom", ref _locationFrom, value); }
        }

        [Browsable(false)]
        public XPCollection<BinLocation> AvailableBinLocationFrom
        {
            get
            {
                _userAccess = SecuritySystem.CurrentUserName;
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                string _locBinLocationCode = null;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                List<string> _stringBinLocation = new List<string>();
                if (_locUserAccess != null)
                {
                    if (this.LocationFrom != null)
                    {
                        if (this.ItemReclassJournal != null)
                        {
                            #region TransferOut
                            if (this.ItemReclassJournal.InventoryMovingType == InventoryMovingType.Adjust || this.ItemReclassJournal.InventoryMovingType == InventoryMovingType.Deformed)
                            {

                                XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                                                  (Session, new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("UserAccess", _locUserAccess),
                                                                                   new BinaryOperator("Location", this.LocationFrom),
                                                                                   new BinaryOperator("Owner", true),
                                                                                   new BinaryOperator("Active", true)));

                                if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                                {
                                    foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                                    {
                                        if (_locWhsSetupDetail.BinLocation != null)
                                        {
                                            if (_locWhsSetupDetail.BinLocation.Code != null)
                                            {
                                                _locBinLocationCode = _locWhsSetupDetail.BinLocation.Code;
                                                _stringBinLocation.Add(_locBinLocationCode);
                                            }
                                        }
                                    }
                                }

                                IEnumerable<string> _stringArrayBinLocationFromDistinct = _stringBinLocation.Distinct();
                                string[] _stringArrayBinLocationFromList = _stringArrayBinLocationFromDistinct.ToArray();
                                if (_stringArrayBinLocationFromList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayBinLocationFromList.Length; i++)
                                    {
                                        BinLocation _locBinLocationFrom = Session.FindObject<BinLocation>(new BinaryOperator("Code", _stringArrayBinLocationFromList[i]));
                                        if (_locBinLocationFrom != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locBinLocationFrom.Code + "'";
                                            }
                                        }
                                    }
                                }
                                else if (_stringArrayBinLocationFromList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayBinLocationFromList.Length; i++)
                                    {
                                        BinLocation _locBinLocationFrom = Session.FindObject<BinLocation>(new BinaryOperator("Code", _stringArrayBinLocationFromList[i]));
                                        if (_locBinLocationFrom != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locBinLocationFrom.Code + "'";
                                            }
                                            else
                                            {
                                                _endString = " OR [Code]=='" + _locBinLocationFrom.Code + "'";
                                            }
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availableBinLocationFrom = new XPCollection<BinLocation>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                            #endregion TransferOut
                        }
                    }
                }
                else
                {
                    _availableBinLocationFrom = new XPCollection<BinLocation>(Session);
                }

                return _availableBinLocationFrom;

            }
        }

        [Appearance("ItemReclassJournalLineBinLocationFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableBinLocationFrom", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public BinLocation BinLocationFrom
        {
            get { return _binLocationFrom; }
            set { SetPropertyValue("BinLocationFrom", ref _binLocationFrom, value); }
        }

        [Appearance("ItemReclassJournalLineStockTypeFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockType StockTypeFrom
        {
            get { return _stockTypeFrom; }
            set { SetPropertyValue("StockTypeFrom", ref _stockTypeFrom, value); }
        }

        #endregion LocationFrom
        #region DefaultFromQty

        [Appearance("ItemReclassJournalLineDQtyFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double DQtyFrom
        {
            get { return _dQtyFrom; }
            set
            {
                SetPropertyValue("DQtyFrom", ref _dQtyFrom, value);
                if (!IsLoading)
                {
                    SetTotalQtyFrom();
                }
            }
        }

        [Appearance("ItemReclassJournalLineDUOM_FromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DUOM_From
        {
            get { return _dUomFrom; }
            set
            {
                SetPropertyValue("DUOM_From", ref _dUomFrom, value);
                if (!IsLoading)
                {
                    SetTotalQtyFrom();
                }
            }
        }

        [Appearance("ItemReclassJournalLineQtyFromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double QtyFrom
        {
            get { return _qtyFrom; }
            set
            {
                SetPropertyValue("QtyFrom", ref _qtyFrom, value);
                if (!IsLoading)
                {
                    SetTotalQtyFrom();
                }
            }
        }

        [Appearance("ItemReclassJournalLineUOM_FromClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure UOM_From
        {
            get { return _uomFrom; }
            set
            {
                SetPropertyValue("UOM_From", ref _uomFrom, value);
                if (!IsLoading)
                {
                    SetTotalQtyFrom();
                }
            }
        }

        [Appearance("ItemReclassJournalLineTQtyFromEnabled", Enabled = false)]
        public double TQtyFrom
        {
            get { return _tQtyFrom; }
            set { SetPropertyValue("TQtyFrom", ref _tQtyFrom, value); }
        }

        #endregion DefaultFromQty
        #endregion From

        #region LocationTo

        [Browsable(false)]
        public XPCollection<Location> AvailableLocationTo
        {
            get
            {
                _userAccess = SecuritySystem.CurrentUserName;
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                string _locLocationCode = null;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                List<string> _stringLocation = new List<string>();
                if (_locUserAccess != null)
                {
                    if (this.ItemReclassJournal != null)
                    {
                        #region TransferOut
                        if (this.ItemReclassJournal.InventoryMovingType == InventoryMovingType.Adjust || this.ItemReclassJournal.InventoryMovingType == InventoryMovingType.Deformed)
                        {
                            XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                                                  (Session, new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("UserAccess", _locUserAccess),
                                                                                   new BinaryOperator("Active", true)));

                            if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                            {
                                foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                                {
                                    if (_locWhsSetupDetail.Location != null)
                                    {
                                        if (_locWhsSetupDetail.Location.Code != null)
                                        {
                                            _locLocationCode = _locWhsSetupDetail.Location.Code;
                                        }
                                    }
                                    _stringLocation.Add(_locLocationCode);
                                }
                            }

                            IEnumerable<string> _stringArrayLocationToDistinct = _stringLocation.Distinct();
                            string[] _stringArrayLocationToList = _stringArrayLocationToDistinct.ToArray();
                            if (_stringArrayLocationToList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayLocationToList.Length; i++)
                                {
                                    Location _locLocationTo = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationToList[i]));
                                    if (_locLocationTo != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locLocationTo.Code + "'";
                                        }
                                    }
                                }
                            }
                            else if (_stringArrayLocationToList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayLocationToList.Length; i++)
                                {
                                    Location _locLocationTo = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationToList[i]));
                                    if (_locLocationTo != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locLocationTo.Code + "'";
                                        }
                                        else
                                        {
                                            _endString = " OR [Code]=='" + _locLocationTo.Code + "'";
                                        }
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null)
                            {
                                _availableLocationTo = new XPCollection<Location>(Session, CriteriaOperator.Parse(_fullString));
                            }
                        }
                        #endregion TransferOut
                    }
                }
                else
                {
                    _availableLocationTo = new XPCollection<Location>(Session);
                }

                return _availableLocationTo;

            }
        }

        [Appearance("ItemReclassJournalLineLocationToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableLocationTo", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        [ImmediatePostData]
        public Location LocationTo
        {
            get { return _locationTo; }
            set { SetPropertyValue("LocationTo", ref _locationTo, value); }
        }

        [Browsable(false)]
        public XPCollection<BinLocation> AvailableBinLocationTo
        {
            get
            {
                _userAccess = SecuritySystem.CurrentUserName;
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                string _locBinLocationCode = null;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                List<string> _stringBinLocation = new List<string>();
                if (_locUserAccess != null)
                {
                    if (this.LocationTo != null)
                    {
                        if (this.ItemReclassJournal != null)
                        {
                            #region TransferOut
                            if (this.ItemReclassJournal.InventoryMovingType == InventoryMovingType.Adjust || this.ItemReclassJournal.InventoryMovingType == InventoryMovingType.Deformed)
                            {
                                XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                                                  (Session, new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("UserAccess", _locUserAccess),
                                                                                   new BinaryOperator("Location", this.LocationTo),
                                                                                   new BinaryOperator("Active", true)));

                                if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                                {
                                    foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                                    {
                                        if (_locWhsSetupDetail.BinLocation != null)
                                        {
                                            if (_locWhsSetupDetail.BinLocation.Code != null)
                                            {
                                                _locBinLocationCode = _locWhsSetupDetail.BinLocation.Code;
                                            }
                                        }
                                        _stringBinLocation.Add(_locBinLocationCode);
                                    }
                                }

                                IEnumerable<string> _stringArrayBinLocationToDistinct = _stringBinLocation.Distinct();
                                string[] _stringArrayBinLocationToList = _stringArrayBinLocationToDistinct.ToArray();
                                if (_stringArrayBinLocationToList.Length == 1)
                                {
                                    for (int i = 0; i < _stringArrayBinLocationToList.Length; i++)
                                    {
                                        BinLocation _locBinLocationTo = Session.FindObject<BinLocation>(new BinaryOperator("Code", _stringArrayBinLocationToList[i]));
                                        if (_locBinLocationTo != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locBinLocationTo.Code + "'";
                                            }
                                        }
                                    }
                                }
                                else if (_stringArrayBinLocationToList.Length > 1)
                                {
                                    for (int i = 0; i < _stringArrayBinLocationToList.Length; i++)
                                    {
                                        BinLocation _locBinLocationTo = Session.FindObject<BinLocation>(new BinaryOperator("Code", _stringArrayBinLocationToList[i]));
                                        if (_locBinLocationTo != null)
                                        {
                                            if (i == 0)
                                            {
                                                _beginString = "[Code]=='" + _locBinLocationTo.Code + "'";
                                            }
                                            else
                                            {
                                                _endString = " OR [Code]=='" + _locBinLocationTo.Code + "'";
                                            }
                                        }
                                    }
                                }
                                _fullString = _beginString + _endString;

                                if (_fullString != null)
                                {
                                    _availableBinLocationTo = new XPCollection<BinLocation>(Session, CriteriaOperator.Parse(_fullString));
                                }
                            }
                            #endregion TransferOut
                        }

                    }
                }
                else
                {
                    _availableBinLocationTo = new XPCollection<BinLocation>(Session);
                }

                return _availableBinLocationTo;

            }
        }

        [Appearance("ItemReclassJournalLineBinLocationToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableBinLocationTo", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public BinLocation BinLocationTo
        {
            get { return _binLocationTo; }
            set { SetPropertyValue("BinLocationTo", ref _binLocationTo, value); }
        }

        [Appearance("ItemReclassJournalLineStockTypeToClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockType StockTypeTo
        {
            get { return _stockTypeIn; }
            set { SetPropertyValue("StockTypeTo", ref _stockTypeIn, value); }
        }

        #endregion LocationTo

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ItemReclassJournalLineItemReclassJournalEnabled", Enabled = false)]
        [Association("ItemReclassJournal-ItemReclassJournalLines")]
        public ItemReclassJournal ItemReclassJournal
        {
            get { return _itemReclassJournal; }
            set { SetPropertyValue("ItemReclassJournal", ref _itemReclassJournal, value); }
        }

        #endregion Field

        //================================================== Code Only ==================================================

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.ItemReclassJournal != null)
                    {
                        object _makRecord = Session.Evaluate<ItemReclassJournalLine>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("ItemReclassJournal=?", this.ItemReclassJournal));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ItemReclassJournalLine " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.ItemReclassJournal != null)
                {
                    ItemReclassJournal _numHeader = Session.FindObject<ItemReclassJournal>
                                                (new BinaryOperator("Code", this.ItemReclassJournal.Code));

                    XPCollection<ItemReclassJournalLine> _numLines = new XPCollection<ItemReclassJournalLine>
                                                (Session, new BinaryOperator("ItemReclassJournal", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (ItemReclassJournalLine _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ItemReclassJournalLine " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.ItemReclassJournal != null)
                {
                    TransferOut _numHeader = Session.FindObject<TransferOut>
                                                (new BinaryOperator("Code", this.ItemReclassJournal.Code));

                    XPCollection<ItemReclassJournalLine> _numLines = new XPCollection<ItemReclassJournalLine>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("ItemReclassJournal", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (ItemReclassJournalLine _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ItemReclassJournalLine " + ex.ToString());
            }
        }

        #endregion No

        private void SetTotalQtyFrom()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.ItemReclassJournal != null)
                {
                    if (this.ItemFrom != null && this.UOM_From != null && this.DUOM_From != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.ItemFrom),
                                                         new BinaryOperator("UOM", this.UOM_From),
                                                         new BinaryOperator("DefaultUOM", this.DUOM_From),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.QtyFrom * _locItemUOM.DefaultConversion + this.DQtyFrom;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.QtyFrom / _locItemUOM.Conversion + this.DQtyFrom;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.QtyFrom + this.DQtyFrom;
                            }

                            this.TQtyFrom = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.QtyFrom + this.DQtyFrom;
                        this.TQtyFrom = _locInvLineTotal;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ItemReclassJournalLine " + ex.ToString());
            }
        }

    }
}