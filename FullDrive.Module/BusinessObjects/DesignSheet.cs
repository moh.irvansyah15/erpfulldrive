﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Quality Assurance")]
    [RuleCombinationOfPropertiesIsUnique("DesignSheetRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class DesignSheet : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private ProductType _productType;
        private ProductCode _productCode;
        private DesignNumber _designNumber;
        private CoreNumber _coreNumber;
        private CrossSectionArea _crossSectionArea;
        private ShapeOfConductor _shapeOfConductor;
        //additional1
        private bool _additional1;
        private CoreNumber _coreNumberAdd1;
        private CrossSectionArea _crossSectionAreaAdd1;
        private ShapeOfConductor _shapeOfConductorAdd1;
        //additional2
        private bool _additional2;
        private CoreNumber _coreNumberAdd2;
        private CrossSectionArea _crossSectionAreaAdd2;
        private ShapeOfConductor _shapeOfConductorAdd2;
        private string _size;
        private RatedVoltage _voltage;
        private Standart _standart;
        private string _project;
        private DateTime _designSheetDate;
        //private byte[] _image;
        private GlobalFunction _globFunc;

        public DesignSheet(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.DesignSheet);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        [Appearance("DesignSheetCodeEnabled", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [DataSourceCriteria("Active = true "), ImmediatePostData()]
        [RuleRequiredField(DefaultContexts.Save)]
        public ProductType ProductType
        {
            get { return _productType; }
            set { SetPropertyValue("ProductType", ref _productType, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true And ProductType = '@This.ProductType'")]
        public ProductCode ProductCode
        {
            get { return _productCode; }
            set
            {
                SetPropertyValue("ProductCode", ref _productCode, value);
                if (!IsLoading)
                {
                    if (this._productCode != null)
                    {
                        if (this._productCode.CoreNumber != null && this._productCode.CrossSectionArea != null && this._productCode.ShapeOfConductor != null)
                        {
                            SetSize1();
                        }
                        if (this._productCode.CoreNumber != null && this._productCode.CrossSectionArea != null && this._productCode.ShapeOfConductor != null
                            && this._productCode.CoreNumberAdd1 != null && this._productCode.CrossSectionAreaAdd1 != null && this._productCode.ShapeOfConductorAdd1 != null)
                        {
                            SetSize2();
                        }
                        if (this._productCode.CoreNumber != null && this._productCode.CrossSectionArea != null && this._productCode.ShapeOfConductor != null
                            && this._productCode.CoreNumberAdd1 != null && this._productCode.CrossSectionAreaAdd1 != null && this._productCode.ShapeOfConductorAdd1 != null
                            && this._productCode.CoreNumberAdd2 != null && this._productCode.CrossSectionAreaAdd2 != null && this._productCode.ShapeOfConductorAdd2 != null)
                        {
                            SetSize3();
                        }
                        this.CoreNumber = this._productCode.CoreNumber;
                        this.CrossSectionArea = this._productCode.CrossSectionArea;
                        this.ShapeOfConductor = this._productCode.ShapeOfConductor;
                        this.Voltage = this._productCode.RatedVoltage;
                        this.Standart = this._productCode.Standart;
                        this.Additional1 = this._productCode.Additional1;
                        this.CoreNumberAdd1 = this._productCode.CoreNumberAdd1;
                        this.CrossSectionAreaAdd1 = this._productCode.CrossSectionAreaAdd1;
                        this.ShapeOfConductorAdd1 = this._productCode.ShapeOfConductorAdd1;
                        this.Additional2 = this._productCode.Additional2;
                        this.CoreNumberAdd2 = this._productCode.CoreNumberAdd2;
                        this.CrossSectionAreaAdd2 = this._productCode.CrossSectionAreaAdd2;
                        this.ShapeOfConductorAdd2 = this._productCode.ShapeOfConductorAdd2;
                    }
                }
            }
        }

        [DataSourceCriteria("Active = true And ProductType = '@This.ProductType'")]
        public DesignNumber DesignNumber
        {
            get { return _designNumber; }
            set { SetPropertyValue("DesignNumber", ref _designNumber, value); }
        }

        [DataSourceCriteria("Active = true")]
        public CoreNumber CoreNumber
        {
            get { return _coreNumber; }
            set { SetPropertyValue("CoreNumber", ref _coreNumber, value); }
        }

        [DataSourceCriteria("Active = true")]
        public CrossSectionArea CrossSectionArea
        {
            get { return _crossSectionArea; }
            set { SetPropertyValue("CrossSectionArea", ref _crossSectionArea, value); }
        }

        [DataSourceCriteria("Active = true")]
        public ShapeOfConductor ShapeOfConductor
        {
            get { return _shapeOfConductor; }
            set { SetPropertyValue("ShapeOfConductor", ref _shapeOfConductor, value); }
        }

        [ImmediatePostData()]
        public bool Additional1
        {
            get { return _additional1; }
            set { SetPropertyValue("Additional1", ref _additional1, value); }
        }

        [Appearance("DesignSheetCoreNumberAdd1Hide", Criteria = "Additional1 = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [DataSourceCriteria("Active = true")]
        public CoreNumber CoreNumberAdd1
        {
            get { return _coreNumberAdd1; }
            set { SetPropertyValue("CoreNumberAdd1", ref _coreNumberAdd1, value); }
        }

        [Appearance("DesignSheetCrossSectionAreaAdd1Hide", Criteria = "Additional1 = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [DataSourceCriteria("Active = true")]
        public CrossSectionArea CrossSectionAreaAdd1
        {
            get { return _crossSectionAreaAdd1; }
            set { SetPropertyValue("CrossSectionAreaAdd1", ref _crossSectionAreaAdd1, value); }
        }

        [Appearance("DesignSheetShapeOfConductorAdd1Hide", Criteria = "Additional1 = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [DataSourceCriteria("Active = true")]
        public ShapeOfConductor ShapeOfConductorAdd1
        {
            get { return _shapeOfConductorAdd1; }
            set { SetPropertyValue("ShapeOfConductorAdd1", ref _shapeOfConductorAdd1, value); }
        }

        [ImmediatePostData()]
        public bool Additional2
        {
            get { return _additional2; }
            set { SetPropertyValue("Additional2", ref _additional2, value); }
        }

        [Appearance("DesignSheetCoreNumberAdd2Hide", Criteria = "Additional2 = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [DataSourceCriteria("Active = true")]
        public CoreNumber CoreNumberAdd2
        {
            get { return _coreNumberAdd2; }
            set { SetPropertyValue("CoreNumberAdd2", ref _coreNumberAdd2, value); }
        }

        [Appearance("DesignSheetCrossSectionAreaAdd2Hide", Criteria = "Additional2 = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [DataSourceCriteria("Active = true")]
        public CrossSectionArea CrossSectionAreaAdd2
        {
            get { return _crossSectionAreaAdd2; }
            set { SetPropertyValue("CrossSectionAreaAdd2", ref _crossSectionAreaAdd2, value); }
        }

        [Appearance("DesignSheetShapeOfConductorAdd2Hide", Criteria = "Additional2 = false", Visibility = ViewItemVisibility.Hide, Context = "DetailView")]
        [DataSourceCriteria("Active = true")]
        public ShapeOfConductor ShapeOfConductorAdd2
        {
            get { return _shapeOfConductorAdd2; }
            set { SetPropertyValue("ShapeOfConductorAdd2", ref _shapeOfConductorAdd2, value); }
        }

        public string Size
        {
            get { return _size; }
            set { SetPropertyValue("Size", ref _size, value); }
        }

        [DataSourceCriteria("Active = true")]
        public RatedVoltage Voltage
        {
            get { return _voltage; }
            set { SetPropertyValue("Voltage", ref _voltage, value); }
        }

        [DataSourceCriteria("Active = true")]
        public Standart Standart
        {
            get { return _standart; }
            set { SetPropertyValue("Standart", ref _standart, value); }
        }

        public string Project
        {
            get { return _project; }
            set { SetPropertyValue("Project", ref _project, value); }
        }

        public DateTime DesignSheetDate
        {
            get { return _designSheetDate; }
            set { SetPropertyValue("DesignSheetDate", ref _designSheetDate, value); }
        }

        //[DevExpress.Xpo.Size(SizeAttribute.Unlimited)]
        //[VisibleInListViewAttribute(true)]
        //[ImageEditor(ListViewImageEditorMode = ImageEditorMode.PictureEdit, DetailViewImageEditorMode = ImageEditorMode.PictureEdit, ListViewImageEditorCustomHeight = 40)]
        //public byte[] Image
        //{
        //    get { return _image; }
        //    set { SetPropertyValue("Image", ref _image, value); }
        //}

        [Association("DesignSheet-DesignSheetLines")]
        public XPCollection<DesignSheetLine> DesignSheetLines
        {
            get { return GetCollection<DesignSheetLine>("DesignSheetLines"); }
        }

        //private string _PersistentProperty;
        //[XafDisplayName("My display name"), ToolTip("My hint message")]
        //[ModelDefault("EditMask", "(000)-00"), Index(0), VisibleInListView(false)]
        //[Persistent("DatabaseColumnName"), RuleRequiredField(DefaultContexts.Save)]
        //public string PersistentProperty {
        //    get { return _PersistentProperty; }
        //    set { SetPropertyValue("PersistentProperty", ref _PersistentProperty, value); }
        //}

        //[Action(Caption = "My UI Action", ConfirmationMessage = "Are you sure?", ImageName = "Attention", AutoCommit = true)]
        //public void ActionMethod() {
        //    // Trigger a custom business logic for the current record in the UI (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112619.aspx).
        //    this.PersistentProperty = "Paid";
        //}

        //=============================================== Code In Here ===============================================

        private void SetSize1()
        {
            try
            {
                if (this._productCode != null)
                {
                    if (this._productCode.CoreNumber != null && this._productCode.CrossSectionArea != null && this._productCode.ShapeOfConductor != null)
                    {
                        this.Size = String.Format("{0}x{1} {2}", this._productCode.CoreNumber.Name, this._productCode.CrossSectionArea.Name, this._productCode.ShapeOfConductor.Name);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = DesignSheet ", ex.ToString());
            }
        }

        private void SetSize2()
        {
            try
            {
                if (this._productCode != null)
                {
                    if (this._productCode.CoreNumber != null && this._productCode.CrossSectionArea != null && this._productCode.ShapeOfConductor != null
                        && this._productCode.CoreNumberAdd1 != null && this._productCode.CrossSectionAreaAdd1 != null && this._productCode.ShapeOfConductorAdd1 != null)
                    {
                        this.Size = String.Format("{0}x{1} {2} {3}x{4} {5}", this._productCode.CoreNumber.Name, this._productCode.CrossSectionArea.Name, this._productCode.ShapeOfConductor.Name,
                            this._productCode.CoreNumberAdd1.Name, this._productCode.CrossSectionAreaAdd1.Name, this._productCode.ShapeOfConductorAdd1.Name);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = DesignSheet ", ex.ToString());
            }
        }

        private void SetSize3()
        {
            try
            {
                if (this._productCode != null)
                {
                    if (this._productCode.CoreNumber != null && this._productCode.CrossSectionArea != null && this._productCode.ShapeOfConductor != null
                        && this._productCode.CoreNumberAdd1 != null && this._productCode.CrossSectionAreaAdd1 != null && this._productCode.ShapeOfConductorAdd1 != null
                        && this._productCode.CoreNumberAdd2 != null && this._productCode.CrossSectionAreaAdd2 != null && this._productCode.ShapeOfConductorAdd2 != null)
                    {
                        this.Size = String.Format("{0}x{1} {2} {3}x{4} {5} {6}x{7} {8}", this._productCode.CoreNumber.Name, this._productCode.CrossSectionArea.Name, this._productCode.ShapeOfConductor.Name,
                            this._productCode.CoreNumberAdd1.Name, this._productCode.CrossSectionAreaAdd1.Name, this._productCode.ShapeOfConductorAdd1.Name,
                            this._productCode.CoreNumberAdd2.Name, this._productCode.CrossSectionAreaAdd2.Name, this._productCode.ShapeOfConductorAdd2.Name);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = DesignSheet ", ex.ToString());
            }
        }

    }
}