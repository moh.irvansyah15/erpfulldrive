﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [NavigationItem("Production")]
    [DefaultProperty("Code")]
    [RuleCombinationOfPropertiesIsUnique("FinishProductionRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class FinishProduction : FullDriveSysBaseObject
    {
        #region Default

        private bool _activationPosting;
        private string _code;
        private string _name;
        private string _description;
        private DateTime _startDate;
        private DateTime _endDate;
        private DateTime _estimatedDate;
        private Status _status;
        private DateTime _statusDate;
        private Production _production;
        private MachineMapVersion _machineMapVersion;
        private MachineCost _machineCost;
        private Consumption _consumption;
        private WorkOrder _workOrder;
        private OutputProduction _outputProduction;
        private string _signCode;
        private Company _company;
        private string _userAccess;
        private GlobalFunction _globalFunc;
        //clm
        private bool _activeApproved1;
        private bool _activeApproved2;
        private bool _activeApproved3;

        public FinishProduction(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
            if (!IsLoading)
            {
                _globalFunc = new GlobalFunction();
                this.Code = _globalFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.FinishProduction);
                DateTime Now = DateTime.Now;
                this.Status = CustomProcess.Status.Open;
                this.StatusDate = Now;
            }

            #region UserAccess
            _userAccess = SecuritySystem.CurrentUserName;
            UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
            if (Session.IsNewObject(this))
            {
                if (_locUserAccess != null)
                {
                    if (_locUserAccess.Employee != null)
                    {
                        if (_locUserAccess.Employee.Company != null)
                        {
                            Company = _locUserAccess.Employee.Company;
                        }
                    }
                }
            }
            #endregion UserAccess
        }

        #endregion Default

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [ImmediatePostData()]
        [Appearance("FinishProductionCodeClose", Enabled = false)]
        [Appearance("FinishProductionRedColor", Criteria = "ActiveApproved1 = true", BackColor = "#fd79a8")]
        [Appearance("FinishProductionYellowColor", Criteria = "ActiveApproved2 = true", BackColor = "#ffeaa7")]
        [Appearance("FinishProductionGreenOColor", Criteria = "ActiveApproved3 = true", BackColor = "#81ecec")]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("FinishProductionCodeEnabled", Enabled = false)]
        public String Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("FinishProductionNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public String Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Size(512)]
        [Appearance("FinishProductionDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Appearance("FinishProductionStartDateEnabled", Enabled = false)]
        public DateTime StartDate
        {
            get { return _startDate; }
            set { SetPropertyValue("StartDate", ref _startDate, value); }
        }

        [Appearance("FinishProductionEndDateEnabled", Enabled = false)]
        public DateTime EndDate
        {
            get { return _endDate; }
            set { SetPropertyValue("EndDate", ref _endDate, value); }
        }

        [Appearance("FinishProductionEstimatedDateEnabled", Enabled = false)]
        public DateTime EstimatedDate
        {
            get { return _estimatedDate; }
            set { SetPropertyValue("EstimatedDate", ref _estimatedDate, value); }
        }

        [Appearance("FinishProductionStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("FinishProductionStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("FinishProductionProductionEnabled", Enabled = false)]
        [Association("Production-FinishProductions")]
        public Production Production
        {
            get { return _production; }
            set { SetPropertyValue("Production", ref _production, value); }
        }

        [Appearance("FinishProductionMachineMapVersionEnabled", Enabled = false)]
        public MachineMapVersion MachineMapVersion
        {
            get { return _machineMapVersion; }
            set { SetPropertyValue("MachineMapVersion", ref _machineMapVersion, value); }
        }

        [Appearance("FinishProductionMachineCostEnabled", Enabled = false)]
        public MachineCost MachineCost
        {
            get { return _machineCost; }
            set { SetPropertyValue("MachineCost", ref _machineCost, value); }
        }

        [Appearance("FinishProductionConsumptionEnabled", Enabled = false)]
        public Consumption Consumption
        {
            get { return _consumption; }
            set { SetPropertyValue("Consumption", ref _consumption, value); }
        }

        [Appearance("FinishProductionOutProductionEnabled", Enabled = false)]
        public OutputProduction OutputProduction
        {
            get { return _outputProduction; }
            set { SetPropertyValue("OutputProduction", ref _outputProduction, value); }
        }

        //Wahab 01-03-2020
        //[Association("WorkOrder-FinishProductions")]
        [Appearance("FinishProductionWorkOrderEnabled", Enabled = false)]      
        public WorkOrder WorkOrder
        {
            get { return _workOrder; }
            set { SetPropertyValue("WorkOrder", ref _workOrder, value); }
        }

        [Association("FinishProduction-FinishProductionLines")]
        public XPCollection<FinishProductionLine> FinishProductionLines
        {
            get { return GetCollection<FinishProductionLine>("FinishProductionLines"); }
        }

        [Appearance("FinishProductionCompanyEnabled", Enabled = false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Appearance("FinishProductionSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [Association("FinishProduction-ApprovalLines")]
        public XPCollection<ApprovalLine> ApprovalLines
        {
            get { return GetCollection<ApprovalLine>("ApprovalLines"); }
        }

        //clm
        [Browsable(false)]
        [Appearance("FinishProductionActiveApproved1Close", Enabled = false)]
        public bool ActiveApproved1
        {
            get { return _activeApproved1; }
            set { SetPropertyValue("ActiveApproved1", ref _activeApproved1, value); }
        }

        [Browsable(false)]
        [Appearance("FinishProductionActiveApproved2Close", Enabled = false)]
        public bool ActiveApproved2
        {
            get { return _activeApproved2; }
            set { SetPropertyValue("ActiveApproved2", ref _activeApproved2, value); }
        }

        [Browsable(false)]
        [Appearance("FinishProductionActiveApproved3Close", Enabled = false)]
        public bool ActiveApproved3
        {
            get { return _activeApproved3; }
            set { SetPropertyValue("ActiveApproved3", ref _activeApproved3, value); }
        }

        #endregion Field

    }
}