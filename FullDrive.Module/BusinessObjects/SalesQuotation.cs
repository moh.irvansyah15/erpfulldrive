﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Sales")]
    [RuleCombinationOfPropertiesIsUnique("SalesQuotationRuleUnique", DefaultContexts.Save, "Code")]
    //[DeferredDeletion]
    //[ListViewFilter("AllDataSQ", "", "All Data", "All Data In Sales Quotation", 1, true)]
    //[ListViewFilter("OpenSQ", "Status = 'Open'", "Open", "Open Data Status In Sales Quotation", 2, true)]
    //[ListViewFilter("ProgressSQ", "Status = 'Progress'", "Progress", "Progress Data Status In Sales Quotation", 3, true)]
    //[ListViewFilter("CloseSQ", "Status = 'Close'", "Close", "Close Data Status In Sales Quotation", 4, true)]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class SalesQuotation : FullDriveSysBaseObject
    {
        #region Default

        private bool _activationPosting;
        private string _code;
        private BusinessPartner _customer;
        private string _contact;        
        private Country _country;
        private City _city;
        private string _address;
        private TermOfPayment _top;
        private DateTime _quotationDate;
        private UserAccess _userAccess;
        private Currency _currency;
        private XPCollection<PriceGroup> _availablePriceGroup;
        private PriceGroup _priceGroup;
        private string _note;
        private Status _status;
        private DateTime _statusDate;
        private DirectionType _transferType;
        private Employee _internalPIC;
        private bool _codeInternalPIC;
        private int _internalPICCodeCount;
        private string _signCode;
        private Company _company;
        private Employee _employee;
        private string _localUserAccess;
        private ProjectHeader _projectHeader;
        private GlobalFunction _globFunc;
        //private SalesOrder _salesOrder;
        //clm
        private bool _activeApproved1;
        private bool _activeApproved2;
        private bool _activeApproved3;
        private Section _section;
        private Division _division;
        private Department _department;

        public SalesQuotation(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).

            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.SalesQuotation);
                DateTime now = DateTime.Now;
                this.QuotationDate = now;
                this.Status = CustomProcess.Status.Open;
                this.StatusDate = now;

                #region UserAccess
                _localUserAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _localUserAccess));
                if (Session.IsNewObject(this))
                {
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            this.Employee = _locUserAccess.Employee;

                            if (_locUserAccess.Employee.Company != null)
                            {
                                Company = _locUserAccess.Employee.Company;
                            }
                            if (_locUserAccess.Employee.Division != null)
                            {
                                Division = _locUserAccess.Employee.Division;
                            }
                            if (_locUserAccess.Employee.Department != null)
                            {
                                Department = _locUserAccess.Employee.Department;
                            }
                            if (_locUserAccess.Employee.Section != null)
                            {
                                Section = _locUserAccess.Employee.Section;
                            }
                        }
                    }
                }
                #endregion UserAccess

                this.Currency = _globFunc.GetDefaultCurrency(this.Session);
            }
        }

        #endregion Default

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                if (this.InternalPICCodeCount > 0 && this.CodeInternalPIC == false)
                {
                    this.CodeInternalPIC = true;
                    this.Save();
                }
            }
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [ImmediatePostData()]
        [Appearance("SalesQuotationCodeClose", Enabled = false)]
        [Appearance("SalesQuotationRedColor", Criteria = "ActiveApproved1 = true", BackColor = "#fd79a8")]
        [Appearance("SalesQuotationYellowColor", Criteria = "ActiveApproved2 = true", BackColor = "#ffeaa7")]
        [Appearance("SalesQuotationGreenOColor", Criteria = "ActiveApproved3 = true", BackColor = "#81ecec")]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("SalesQuotationCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        //auto fill field
        [ImmediatePostData()]
        [Appearance("SalesQuotationCustomerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner Customer
        {
            get { return _customer; }
            //set { SetPropertyValue("Customer", ref _customer, value); }
            set
            {
                SetPropertyValue("Customer", ref _customer, value);
                if (this._customer != null)
                {
                    this.Contact = this._customer.Contact;
                    this.Country = this._customer.Country;
                    this.City = this._customer.City;
                    this.Address = this._customer.Address;
                    this.TOP = this._customer.TOP;
                    if(this.InternalPIC == null)
                    {
                        this.InternalPIC = this._customer.InternalPIC;
                    }  
                }
            }
        }

        [Appearance("SalesQuotationContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Contact
        {
            get { return _contact; }
            set { SetPropertyValue("Contact", ref _contact, value); }
        }        

        [Appearance("SalesQuotationCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country Country
        {
            get { return _country; }
            set { SetPropertyValue("Country", ref _country, value); }
        }

        [Appearance("SalesQuotationCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City City
        {
            get { return _city; }
            set { SetPropertyValue("City", ref _city, value); }
        }

        [Size(512)]
        [Appearance("SalesQuotationAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Address
        {
            get { return _address; }
            set { SetPropertyValue("Address", ref _address, value); }
        }

        [Appearance("SalesQuotationTermOfPaymentClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public TermOfPayment TOP
        {
            get { return _top; }
            set { SetPropertyValue("TOP", ref _top, value); }
        }

        [Appearance("SalesQuotationQuotationDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime QuotationDate
        {
            get { return _quotationDate; }
            set { SetPropertyValue("QuotationDate", ref _quotationDate, value); }
        }

        [Browsable(false)]
        [ImmediatePostData()]
        public UserAccess UserAccess
        {
            get { return _userAccess; }
            set { SetPropertyValue("UserAccess", ref _userAccess, value); }
        }

        [Appearance("SalesQuotationCurrencyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        [Browsable(false)]
        public XPCollection<PriceGroup> AvailablePriceGroup
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                if (UserAccess == null)
                {
                    _availablePriceGroup = new XPCollection<PriceGroup>(Session);
                }
                else
                {
                    List<string> _stringPG = new List<string>();

                    XPCollection<SalesSetupDetail> _locSalesSetupDetails = new XPCollection<SalesSetupDetail>
                                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("UserAccess", this.UserAccess),
                                                                               new BinaryOperator("Active", true)));

                    if (_locSalesSetupDetails != null && _locSalesSetupDetails.Count() > 0)
                    {
                        foreach (SalesSetupDetail _locSalesSetupDetail in _locSalesSetupDetails)
                        {
                            _stringPG.Add(_locSalesSetupDetail.PriceGroup.Code);
                        }
                    }

                    IEnumerable<string> _stringArrayPGDistinct = _stringPG.Distinct();
                    string[] _stringArrayPGList = _stringArrayPGDistinct.ToArray();
                    if (_stringArrayPGList.Length == 1)
                    {
                        for (int i = 0; i < _stringArrayPGList.Length; i++)
                        {
                            PriceGroup _locPG = Session.FindObject<PriceGroup>(new BinaryOperator("Code", _stringArrayPGList[i]));
                            if (_locPG != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locPG.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArrayPGList.Length > 1)
                    {
                        for (int i = 0; i < _stringArrayPGList.Length; i++)
                        {
                            PriceGroup _locPG = Session.FindObject<PriceGroup>(new BinaryOperator("Code", _stringArrayPGList[i]));
                            if (_locPG != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locPG.Code + "'";
                                }
                                else
                                {
                                    _endString = " OR [Code]=='" + _locPG.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availablePriceGroup = new XPCollection<PriceGroup>(Session, CriteriaOperator.Parse(_fullString));
                    }

                }
                return _availablePriceGroup;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailablePriceGroup", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        [Appearance("SalesQuotationPriceGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set {
                SetPropertyValue("PriceGroup", ref _priceGroup, value);
                if(!IsLoading)
                {
                    string _locPICCode = null;
                    string _locPIC_Code = null;
                    string _locPriceGroup_Code = null;
                    _globFunc = new GlobalFunction();
                    if (this._priceGroup != null)
                    {
                        if(this._priceGroup.Code != null)
                        {
                            _locPriceGroup_Code = this._priceGroup.Code;
                            
                        }
                        if(this.InternalPIC != null)
                        {
                            if(this.InternalPIC.Code != null)
                            {
                                _locPIC_Code = this.InternalPIC.Code;
                            }
                        }
                        if(_locPIC_Code != null && _locPriceGroup_Code != null)
                        {
                            _locPICCode = _globFunc.GetNumberingPICUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.SalesQuotation, _locPIC_Code, _locPriceGroup_Code);
                            if (_locPICCode != null)
                            {
                                if (this.CodeInternalPIC == false)
                                {
                                    this.Code = _locPICCode;
                                    this.InternalPICCodeCount = this.InternalPICCodeCount + 1;
                                }
                            }
                        } 
                    }
                }
            }
        }

        [Size(512)]
        [Appearance("SalesQuotationNoteClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Note
        {
            get { return _note; }
            set { SetPropertyValue("Note", ref _note, value); }
        }

        [Appearance("SalesQuotationTransferTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DirectionType TransferType
        {
            get { return _transferType; }
            set { SetPropertyValue("TransferType", ref _transferType, value); }
        }

        [ImmediatePostData()]
        [Appearance("SalesQuotationInternalPICClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Employee InternalPIC
        {
            get { return _internalPIC; }
            set {
                SetPropertyValue("InternalPIC", ref _internalPIC, value);
                if(!IsLoading)
                {
                    string _locPICCode = null;
                    string _locPIC_Code = null;
                    string _locPriceGroup_Code = null;
                    _globFunc = new GlobalFunction();
                    if (this._internalPIC != null)
                    {
                        if(this._priceGroup != null)
                        {
                            if (this._priceGroup.Code != null)
                            {
                                _locPriceGroup_Code = this._priceGroup.Code;  
                            }
                        }

                        if (this.InternalPIC.Code != null)
                        {
                            _locPIC_Code = this.InternalPIC.Code;
                        }
                       
                        if (_locPIC_Code != null && _locPriceGroup_Code != null)
                        {
                            _locPICCode = _globFunc.GetNumberingPICUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.SalesQuotation, _locPIC_Code, _locPriceGroup_Code);
                            if (_locPICCode != null)
                            {
                                if (this.CodeInternalPIC == false)
                                {
                                    this.Code = _locPICCode;
                                    this.InternalPICCodeCount = this.InternalPICCodeCount + 1;
                                }
                            }
                        }
                    }
                }    
            }
        }

        [Browsable(false)]
        public bool CodeInternalPIC
        {
            get { return _codeInternalPIC; }
            set { SetPropertyValue("CodeInternalPIC", ref _codeInternalPIC, value); }
        }

        [Browsable(false)]
        public int InternalPICCodeCount
        {
            get { return _internalPICCodeCount; }
            set { SetPropertyValue("InternalPICCodeCount", ref _internalPICCodeCount, value); }
        }

        [Appearance("SalesQuotationSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [Appearance("SalesQuotationStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("SalesQuotationStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("SalesQuotationEmployeeClose", Enabled = false)]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        [Browsable(false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Browsable(false)]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        [Browsable(false)]
        public Department Department
        {
            get { return _department; }
            set { SetPropertyValue("Department", ref _department, value); }
        }

        [Browsable(false)]
        public Section Section
        {
            get { return _section; }
            set { SetPropertyValue("Section", ref _section, value); }
        }

        [Association("SalesQuotation-SalesQuotationLines")]
        public XPCollection<SalesQuotationLine> SalesQuotationLines
        {
            get { return GetCollection<SalesQuotationLine>("SalesQuotationLines"); }
        }

        [Association("SalesQuotation-ApprovalLines")]
        public XPCollection<ApprovalLine> ApprovalLines
        {
            get { return GetCollection<ApprovalLine>("ApprovalLines"); }
        }

        [Appearance("SalesQuotationProjectHeaderClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Association("ProjectHeader-SalesQuotations")]
        public ProjectHeader ProjectHeader
        {
            get { return _projectHeader; }
            set { SetPropertyValue("ProjectHeader", ref _projectHeader, value); }
        }

        //clm
        [Browsable(false)]
        [Appearance("SalesQuotationActiveApproved1Close", Enabled = false)]
        public bool ActiveApproved1
        {
            get { return _activeApproved1; }
            set { SetPropertyValue("ActiveApproved1", ref _activeApproved1, value); }
        }

        [Browsable(false)]
        [Appearance("SalesQuotationActiveApproved2Close", Enabled = false)]
        public bool ActiveApproved2
        {
            get { return _activeApproved2; }
            set { SetPropertyValue("ActiveApproved2", ref _activeApproved2, value); }
        }

        [Browsable(false)]
        [Appearance("SalesQuotationActiveApproved3Close", Enabled = false)]
        public bool ActiveApproved3
        {
            get { return _activeApproved3; }
            set { SetPropertyValue("ActiveApproved3", ref _activeApproved3, value); }
        }

        #endregion Field

    }
}