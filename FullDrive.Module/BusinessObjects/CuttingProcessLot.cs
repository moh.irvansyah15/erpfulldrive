﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.SystemModule;
using FullDrive.Module.CustomProcess;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Production")]
    [RuleCombinationOfPropertiesIsUnique("CuttingProcessLotRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class CuttingProcessLot : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private int _no;
        private string _code;
        private string _name;
        private Item _item;
        private string _description;
        #region InisialisasiLocation
        private XPCollection<Location> _availableLocation;
        private Location _location;
        private XPCollection<BinLocation> _availableBinLocation;
        private BinLocation _binLocation;
        private StockType _stockType;
        #endregion InisialisasiLocation
        private XPCollection<BeginingInventoryLine> _availableBeginingInventoryLine;
        private BeginingInventoryLine _begInvLine;
        private UnitOfMeasure _unitPack;
        private string _oldLotNumber;
        private XPCollection<UnitOfMeasure> _availableUnitOfMeasure;
        private double _rQtyAvailable;
        private bool _newLotNumberEnabled;
        private string _newLotNumber;
        #region InisialisasiNewDefaultQty
        private double _nDQty;
        private UnitOfMeasure _nDUom;
        private double _nQty;
        private UnitOfMeasure _nUom;
        private double _nTQty;
        #endregion InisialisasiNewDefaultQty
        private Status _status;
        private DateTime _statusDate;
        private int _processCount;
        private CuttingProcessLine _cuttingProcessLine;
        private Company _company;
        private string _userAccess;
        private GlobalFunction _globFunc;

        public CuttingProcessLot(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            _globFunc = new GlobalFunction();
            DateTime now = DateTime.Now;
            this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.CuttingProcessLot);
            this.Status = Status.Open;
            this.StatusDate = now;
            this.StockType = StockType.Good;
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsLoading && IsSaving)
            {
                UpdateNo();
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
            if (!IsLoading)
            {
                RecoveryDeleteNo();
            }
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("CuttingProcessLotNoEnabled", Enabled = false)]
        public int No
        {
            get { return _no; }
            set { SetPropertyValue("No", ref _no, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("CuttingProcessLotCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("CuttingProcessLotNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [ImmediatePostData()]
        [Appearance("CuttingProcessLotItemClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Item Item
        {
            get { return _item; }
            set
            {
                SetPropertyValue("Item", ref _item, value);
                if (!IsLoading)
                {
                    if (this._item != null)
                    {
                        this.NDUOM = this._item.BasedUOM;
                    }
                }
            }
        }

        [Appearance("CuttingProcessLotDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        #region Location

        [Browsable(false)]
        public XPCollection<Location> AvailableLocation
        {
            get
            {
                _userAccess = SecuritySystem.CurrentUserName;
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                string _locLocationCode = null;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                if (_locUserAccess != null)
                {
                    if (this.CuttingProcessLine != null)
                    {
                        List<string> _stringLocation = new List<string>();

                        XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                                                  (Session, new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("UserAccess", _locUserAccess),
                                                                                   new BinaryOperator("ObjectList", ObjectList.CuttingProcess),
                                                                                   new BinaryOperator("Owner", true),
                                                                                   new BinaryOperator("TransferType", DirectionType.Internal),
                                                                                   new BinaryOperator("InventoryMovingType", InventoryMovingType.Cutting),
                                                                                   new BinaryOperator("LocationType", LocationType.Main),
                                                                                   new BinaryOperator("Active", true)));

                        if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                        {
                            foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                            {
                                if (_locWhsSetupDetail.Location != null)
                                {
                                    if (_locWhsSetupDetail.Location.Code != null)
                                    {
                                        _locLocationCode = _locWhsSetupDetail.Location.Code;
                                        _stringLocation.Add(_locLocationCode);
                                    }
                                }

                            }
                        }

                        IEnumerable<string> _stringArrayLocationFromDistinct = _stringLocation.Distinct();
                        string[] _stringArrayLocationFromList = _stringArrayLocationFromDistinct.ToArray();
                        if (_stringArrayLocationFromList.Length == 1)
                        {
                            for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                            {
                                Location _locLocationFrom = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                if (_locLocationFrom != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locLocationFrom.Code + "'";
                                    }
                                }
                            }
                        }
                        else if (_stringArrayLocationFromList.Length > 1)
                        {
                            for (int i = 0; i < _stringArrayLocationFromList.Length; i++)
                            {
                                Location _locLocationFrom = Session.FindObject<Location>(new BinaryOperator("Code", _stringArrayLocationFromList[i]));
                                if (_locLocationFrom != null)
                                {
                                    if (i == 0)
                                    {
                                        _beginString = "[Code]=='" + _locLocationFrom.Code + "'";
                                    }
                                    else
                                    {
                                        _endString = " OR [Code]=='" + _locLocationFrom.Code + "'";
                                    }
                                }
                            }
                        }
                        _fullString = _beginString + _endString;

                        if (_fullString != null)
                        {
                            _availableLocation = new XPCollection<Location>(Session, CriteriaOperator.Parse(_fullString));
                        }
                        
                    }
                }
                else
                {
                    _availableLocation = new XPCollection<Location>(Session);
                }

                return _availableLocation;

            }
        }

        [Appearance("CuttingProcessLotLocationClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableLocation", DataSourcePropertyIsNullMode.SelectAll)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public Location Location
        {
            get { return _location; }
            set { SetPropertyValue("Location", ref _location, value); }
        }

        [Browsable(false)]
        public XPCollection<BinLocation> AvailableBinLocation
        {
            get
            {
                _userAccess = SecuritySystem.CurrentUserName;
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                string _locBinLocationCode = null;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                if (_locUserAccess != null)
                {
                    if (this.Location != null)
                    {
                        if (this.CuttingProcessLine != null)
                        {
                            List<string> _stringBinLocation = new List<string>();

                            XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>
                                                                                      (Session, new GroupOperator(GroupOperatorType.And,
                                                                                       new BinaryOperator("UserAccess", _locUserAccess),
                                                                                       new BinaryOperator("Location", this.Location),
                                                                                       new BinaryOperator("Owner", true),
                                                                                       new BinaryOperator("Active", true)));

                            if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                            {
                                foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                                {
                                    if (_locWhsSetupDetail.BinLocation != null)
                                    {
                                        if (_locWhsSetupDetail.BinLocation.Code != null)
                                        {
                                            _locBinLocationCode = _locWhsSetupDetail.BinLocation.Code;
                                            _stringBinLocation.Add(_locBinLocationCode);
                                        }
                                    }

                                }
                            }

                            IEnumerable<string> _stringArrayBinLocationFromDistinct = _stringBinLocation.Distinct();
                            string[] _stringArrayBinLocationFromList = _stringArrayBinLocationFromDistinct.ToArray();
                            if (_stringArrayBinLocationFromList.Length == 1)
                            {
                                for (int i = 0; i < _stringArrayBinLocationFromList.Length; i++)
                                {
                                    BinLocation _locBinLocationFrom = Session.FindObject<BinLocation>(new BinaryOperator("Code", _stringArrayBinLocationFromList[i]));
                                    if (_locBinLocationFrom != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locBinLocationFrom.Code + "'";
                                        }
                                    }
                                }
                            }
                            else if (_stringArrayBinLocationFromList.Length > 1)
                            {
                                for (int i = 0; i < _stringArrayBinLocationFromList.Length; i++)
                                {
                                    BinLocation _locBinLocationFrom = Session.FindObject<BinLocation>(new BinaryOperator("Code", _stringArrayBinLocationFromList[i]));
                                    if (_locBinLocationFrom != null)
                                    {
                                        if (i == 0)
                                        {
                                            _beginString = "[Code]=='" + _locBinLocationFrom.Code + "'";
                                        }
                                        else
                                        {
                                            _endString = " OR [Code]=='" + _locBinLocationFrom.Code + "'";
                                        }
                                    }
                                }
                            }
                            _fullString = _beginString + _endString;

                            if (_fullString != null)
                            {
                                _availableBinLocation = new XPCollection<BinLocation>(Session, CriteriaOperator.Parse(_fullString));
                            }
                        }
                    }
                }
                else
                {
                    _availableBinLocation = new XPCollection<BinLocation>(Session);
                }

                return _availableBinLocation;

            }
        }

        [Appearance("CuttingProcessLotBinLocationClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [DataSourceProperty("AvailableBinLocation", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public BinLocation BinLocation
        {
            get { return _binLocation; }
            set { SetPropertyValue("BinLocation", ref _binLocation, value); }
        }

        [Appearance("CuttingProcessLotStockTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public StockType StockType
        {
            get { return _stockType; }
            set { SetPropertyValue("StockType", ref _stockType, value); }
        }

        #endregion Location

        //Not Complete
        [Browsable(false)]
        public XPCollection<BeginingInventoryLine> AvailableBeginingInventoryLine
        {
            get
            {
                string _locItem = null;
                string _locLocation = null;
                string _locBinLocation = null;
                string _fullString = null;
                if(this.CuttingProcessLine != null)
                {
                    if (Item == null)
                    {
                        _availableBeginingInventoryLine = new XPCollection<BeginingInventoryLine>(Session);
                    }
                    else
                    {
                        //If For Item
                        if (this.Item != null) { _locItem = "[Item.Code]=='" + this.Item.Code + "'"; }
                        else { _locItem = ""; }

                        //If For Location
                        if (this.Location != null && this.Item != null)
                        { _locLocation = " AND [Location.Code]=='" + this.Location.Code + "'"; }
                        else if (this.Location != null && this.Item == null)
                        { _locLocation = " [Location.Code]=='" + this.Location.Code + "'"; }
                        else { _locLocation = ""; }

                        //If for BinLocation
                        if (this.BinLocation != null && (this.Item != null || this.Location != null))
                        { _locBinLocation = " AND [BinLocation.Code]=='" + this.BinLocation.Code + "'"; }
                        else if (this.BinLocation != null && this.Item != null && this.Location != null)
                        { _locBinLocation = "[BinLocation.Code] == '" + this.BinLocation.Code + "'"; }
                        else { _locBinLocation = ""; }

                        if (_locItem != null || _locLocation != null || _locBinLocation != null)
                        {
                            _fullString = _locItem + _locLocation + _locBinLocation;
                        }

                        if (_fullString != null)
                        {
                            _availableBeginingInventoryLine = new XPCollection<BeginingInventoryLine>(Session, CriteriaOperator.Parse(_fullString));
                        }
                    }         
                }

                return _availableBeginingInventoryLine;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableBeginingInventoryLine", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("CuttingProcessLotBegInvLineClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BeginingInventoryLine BegInvLine
        {
            get { return _begInvLine; }
            set {
                SetPropertyValue("BegInvLine", ref _begInvLine, value);
                if(!IsLoading)
                {
                    if(this._begInvLine != null)
                    {
                        if(this._begInvLine.LotNumber != null)
                        {
                            if (this._begInvLine.UnitPack != null)
                            {
                                this.UnitPack = this._begInvLine.UnitPack;
                            }
                            if(this._begInvLine.LotNumber != null)
                            {
                                this.OldLotNumber = this._begInvLine.LotNumber;
                                this.NewLotNumber = this._begInvLine.LotNumber;
                            }
                            if(this.NTQty > 0)
                            {
                                if (this._begInvLine.QtyAvailable < this.NTQty)
                                {
                                    this._begInvLine = null;
                                }
                                if (this._begInvLine.QtyAvailable == this.NTQty)
                                {
                                    this.NewLotNumberEnabled = true;
                                    this.NewLotNumber = null;
                                }else
                                {
                                    this.NewLotNumberEnabled = false;
                                    this.NewLotNumber = this._begInvLine.LotNumber;
                                }
                            }
                                     
                        }
                    }
                }
            }
        }

        [Appearance("CuttingProcessLotUnitPackClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public UnitOfMeasure UnitPack
        {
            get { return _unitPack; }
            set { SetPropertyValue("UnitPack", ref _unitPack, value); }
        }

        [Appearance("CuttingProcessLotOldLotNumberClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string OldLotNumber
        {
            get { return _oldLotNumber; }
            set { SetPropertyValue("OldLotNumber", ref _oldLotNumber, value); }
        }

        [Browsable(false)]
        public XPCollection<UnitOfMeasure> AvailableUnitOfMeasure
        {
            get
            {
                string _beginString = null;
                string _endString = null;
                string _fullString = null;
                if (Item == null)
                {
                    _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session);
                }
                else
                {
                    List<string> _stringUOM = new List<string>();

                    XPCollection<ItemUnitOfMeasure> _locItemUnitOfMeasures = new XPCollection<ItemUnitOfMeasure>
                                                                              (Session, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Item", this.Item),
                                                                               new BinaryOperator("Active", true)));

                    if (_locItemUnitOfMeasures != null && _locItemUnitOfMeasures.Count() > 0)
                    {
                        foreach (ItemUnitOfMeasure _locItemUnitOfMeasure in _locItemUnitOfMeasures)
                        {
                            _stringUOM.Add(_locItemUnitOfMeasure.UOM.Code);
                        }
                    }

                    IEnumerable<string> _stringArrayUOMDistinct = _stringUOM.Distinct();
                    string[] _stringArrayUOMList = _stringArrayUOMDistinct.ToArray();
                    if (_stringArrayUOMList.Length == 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArrayUOMList.Length > 1)
                    {
                        for (int i = 0; i < _stringArrayUOMList.Length; i++)
                        {
                            UnitOfMeasure _locUOM = Session.FindObject<UnitOfMeasure>(new BinaryOperator("Code", _stringArrayUOMList[i]));
                            if (_locUOM != null)
                            {
                                if (i == 0)
                                {
                                    _beginString = "[Code]=='" + _locUOM.Code + "'";
                                }
                                else
                                {
                                    _endString = " OR [Code]=='" + _locUOM.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString = _beginString + _endString;

                    if (_fullString != null)
                    {
                        _availableUnitOfMeasure = new XPCollection<UnitOfMeasure>(Session, CriteriaOperator.Parse(_fullString));
                    }

                }
                return _availableUnitOfMeasure;

            }
        }

        [Persistent]
        public double QtyAvailable
        {
            get
            {
                double _locQtyAvailable = 0;
                if (this.BegInvLine != null)
                {
                    if (this.BegInvLine.QtyAvailable > 0)
                    {
                        _locQtyAvailable = this.BegInvLine.QtyAvailable;
                    }
                    return _locQtyAvailable;
                }
                return 0;
            }
        }

        [Persistent]
        public string UOM
        {
            get
            {
                string _locName = null;
                if (this.BegInvLine != null )
                {
                    if (this.BegInvLine.FullName != null)
                    {
                        _locName = this.BegInvLine.FullName;
                    }
                    return String.Format("{0}", _locName);
                }
                return null;
            }
        }

        [Appearance("CuttingProcessLotRQtyAvailableClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double RQtyAvailable
        {
            get { return _rQtyAvailable; }
            set { SetPropertyValue("RQtyAvailable", ref _rQtyAvailable, value); }
        }

        
        [Browsable(false)]
        public bool NewLotNumberEnabled
        {
            get { return _newLotNumberEnabled; }
            set { SetPropertyValue("_newLotNumberEnabled", ref _newLotNumberEnabled, value); }
        }

        [Appearance("CuttingProcessLotNewLotNumberEnabled", Criteria = "NewLotNumberEnabled = true", Enabled = false)]
        [Appearance("CuttingProcessLotNewLotNumberClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public string NewLotNumber
        {
            get { return _newLotNumber; }
            set { SetPropertyValue("NewLotNumber", ref _newLotNumber, value); }
        }

        public string LotNumberNotification
        {
            get
            {
                string _locName = null;
                if (this.NewLotNumber != null)
                {
                    BeginingInventoryLine _locBegInvLine = Session.FindObject<BeginingInventoryLine>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("LotNumber", this.NewLotNumber)));
                    if(_locBegInvLine != null)
                    {
                        _locName = "Lot Number Available";
                    }else
                    {
                        _locName = "Lot Number UnAvailable";
                    }
                    return String.Format("{0}", _locName);
                }
                return null;
            }
        }

        #region NewDefaultQty

        [Appearance("CuttingProcessLotNDQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double NDQty
        {
            get { return _nDQty; }
            set
            {
                SetPropertyValue("NDQty", ref _nDQty, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("CuttingProcessLotNDUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure NDUOM
        {
            get { return _nDUom; }
            set
            {
                SetPropertyValue("NDUOM", ref _nDUom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("CuttingProcessLotNQtyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public double NQty
        {
            get { return _nQty; }
            set
            {
                SetPropertyValue("NQty", ref _nQty, value);
                if (!IsLoading)
                {
                    SetTotalQty(); 
                }
            }
        }

        [Appearance("CuttingProcessLotUOMClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceProperty("AvailableUnitOfMeasure", DataSourcePropertyIsNullMode.SelectAll)]
        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure NUOM
        {
            get { return _nUom; }
            set
            {
                SetPropertyValue("NUOM", ref _nUom, value);
                if (!IsLoading)
                {
                    SetTotalQty();
                }
            }
        }

        [Appearance("CuttingProcessLotNTQtyEnabled", Enabled = false)]
        public double NTQty
        {
            get { return _nTQty; }
            set { SetPropertyValue("NTQty", ref _nTQty, value); }
        }

        #endregion NewDefaultQty

        [Appearance("CuttingProcessLotStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("CuttingProcessLotStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("CuttingProcessLotProcessCountEnabled", Enabled = false)]
        public int ProcessCount
        {
            get { return _processCount; }
            set { SetPropertyValue("ProcessCount", ref _processCount, value); }
        }

        [Association("CuttingProcessLine-CuttingProcessLots")]
        public CuttingProcessLine CuttingProcessLine
        {
            get { return _cuttingProcessLine; }
            set
            {
                SetPropertyValue("CuttingProcessLine", ref _cuttingProcessLine, value);
                if(!IsLoading)
                {
                    if(this._cuttingProcessLine != null)
                    {
                        if(this._cuttingProcessLine.DUOM != null)
                        {
                            this.NDUOM = this._cuttingProcessLine.DUOM;
                        }
                        if (this._cuttingProcessLine.UOM != null)
                        {
                            this.NUOM = this._cuttingProcessLine.UOM;
                        }
                        this.NDQty = this._cuttingProcessLine.DQty;
                        this.NQty = this._cuttingProcessLine.Qty;
                        this.NTQty = this._cuttingProcessLine.TQty;
                        if(this._cuttingProcessLine.Item != null)
                        {
                            this.Item = this._cuttingProcessLine.Item;
                        }
                        if(this._cuttingProcessLine.Location != null)
                        {
                            this.Location = this._cuttingProcessLine.Location;
                        }
                        if(this._cuttingProcessLine.BinLocation != null)
                        {
                            this.BinLocation = this._cuttingProcessLine.BinLocation;
                        }
                        if(this._cuttingProcessLine.StockType != StockType.None)
                        {
                            this.StockType = this._cuttingProcessLine.StockType;
                        }
                    }
                }
            }
        }

        [Browsable(false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        #endregion Field

        //==================================================== Code Only ======================================================

        #region No

        public void UpdateNo()
        {
            try
            {
                if (!IsLoading && Session.IsNewObject(this))
                {
                    if (this.CuttingProcessLine != null)
                    {
                        object _makRecord = Session.Evaluate<CuttingProcessLot>(CriteriaOperator.Parse("Max(No)"), CriteriaOperator.Parse("CuttingProcessLine=?", this.CuttingProcessLine));
                        this.No = Convert.ToInt32(_makRecord) + 1;
                        this.Save();
                        RecoveryUpdateNo();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CuttingProcessLot " + ex.ToString());
            }
        }

        public void RecoveryUpdateNo()
        {
            try
            {
                if (this.CuttingProcessLine != null)
                {
                    CuttingProcessLine _numHeader = Session.FindObject<CuttingProcessLine>
                                                (new BinaryOperator("Code", this.CuttingProcessLine.Code));

                    XPCollection<CuttingProcessLot> _numLines = new XPCollection<CuttingProcessLot>
                                                (Session, new BinaryOperator("CuttingProcessLine", _numHeader),
                                                new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i1 = 0;
                        foreach (CuttingProcessLot _numLine in _numLines)
                        {
                            i1 += 1;
                            _numLine.No = i1;
                            _numLine.Save();
                        }
                        i1 = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CuttingProcessLot " + ex.ToString());
            }
        }

        public void RecoveryDeleteNo()
        {
            try
            {
                if (this.CuttingProcessLine != null)
                {
                    CuttingProcessLine _numHeader = Session.FindObject<CuttingProcessLine>
                                                (new BinaryOperator("Code", this.CuttingProcessLine.Code));

                    XPCollection<CuttingProcessLot> _numLines = new XPCollection<CuttingProcessLot>
                                                (Session, new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("This", this, BinaryOperatorType.NotEqual),
                                                 new BinaryOperator("CuttingProcessLine", _numHeader)),
                                                 new SortProperty("No", DevExpress.Xpo.DB.SortingDirection.Ascending));
                    if (_numLines != null)
                    {
                        int i = 0;
                        foreach (CuttingProcessLot _numLine in _numLines)
                        {
                            i += 1;
                            _numLine.No = i;
                            _numLine.Save();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CuttingProcessLot " + ex.ToString());
            }
        }

        #endregion No

        #region Set

        private void SetTotalQty()
        {
            try
            {
                double _locInvLineTotal = 0;
                DateTime now = DateTime.Now;
                if (this.CuttingProcessLine != null)
                {
                    if (this.Item != null && this.NUOM != null && this.NDUOM != null)
                    {
                        ItemUnitOfMeasure _locItemUOM = Session.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", this.Item),
                                                         new BinaryOperator("UOM", this.NUOM),
                                                         new BinaryOperator("DefaultUOM", this.NDUOM),
                                                         new BinaryOperator("Active", true)));
                        if (_locItemUOM != null)
                        {
                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.NQty * _locItemUOM.DefaultConversion + this.NDQty;
                            }
                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.NQty / _locItemUOM.Conversion + this.NDQty;
                            }
                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            {
                                _locInvLineTotal = this.NQty + this.NDQty;
                            }

                            this.NTQty = _locInvLineTotal;
                        }
                    }
                    else
                    {
                        _locInvLineTotal = this.NQty + this.NDQty;
                        this.NTQty = _locInvLineTotal;
                    }

                    if (this.BegInvLine != null)
                    {
                        if (this.BegInvLine.QtyAvailable > 0)
                        {
                            if (this.NDQty >= this.BegInvLine.QtyAvailable)
                            {
                                this.NewLotNumberEnabled = true;
                                this.NewLotNumber = null;
                            }
                            else
                            {
                                this.NewLotNumberEnabled = false;
                                if(this.BegInvLine.LotNumber != null)
                                {
                                    this.NewLotNumber = this.BegInvLine.LotNumber;
                                }  
                            }
                        }
                    }

                    if (CuttingProcessLine != null)
                    {
                        if (this.CuttingProcessLine.MxTQty > 0)
                        {
                            if(this.CuttingProcessLine.DQty <= this.CuttingProcessLine.MxTQty)
                            {
                                if (this.NDQty > this.CuttingProcessLine.DQty)
                                {
                                    this.NDQty = this.CuttingProcessLine.DQty;
                                }
                            }
                            
                        }
                        else
                        {
                            if (this.CuttingProcessLine.TQty > 0)
                            {
                                if(this.CuttingProcessLine.DQty <= this.CuttingProcessLine.TQty)
                                {
                                    if (this.NDQty > this.CuttingProcessLine.DQty)
                                    {
                                        this.NDQty = this.CuttingProcessLine.DQty;
                                    }
                                }  
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CuttingProcessLot " + ex.ToString());
            }
        }

        #endregion Set
    }
}