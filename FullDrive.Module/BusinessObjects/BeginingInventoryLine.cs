﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("FullName")]
    [NavigationItem("Inventory")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class BeginingInventoryLine : FullDriveSysBaseObject
    {
        #region Default

        private string _code;
        private Item _item;
        private Brand _brand;
        private Location _location;
        private BinLocation _binLocation;
        private UnitOfMeasure _unitPack;
        private string _description;
        private double _qtyBegining;
        private double _qtyMinimal;
        private double _qtyAvailable;
        private double _qtySale;
        private UnitOfMeasure _defaultUom;
        private StockType _stockType;
        private string _lotNumber;
        private Company _company;
        private bool _active;
        private ProjectHeader _projectHeader;
        private BeginingInventory _beginingInventory;
        private GlobalFunction _globFunc;

        public BeginingInventoryLine(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.BeginingInventoryLine);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #endregion Default

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("BeginingInventoryLineCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Persistent]
        public string FullName
        {
            get
            {
                string _locName = null;
                if (this.Item != null || this.LotNumber != null)
                {
                    if(this.Item.Name != null)
                    {
                        _locName = this.Item.Name;
                    }
                    return String.Format("({0})-({1})-({2})", _locName, this.LotNumber, this.QtyAvailable);
                }
                return null;
            }
        }

        [ImmediatePostData()]
        public Item Item
        {
            get { return _item; }
            set
            {
                SetPropertyValue("Item", ref _item, value);
                if (!IsLoading)
                {
                    if (this._item != null)
                    {
                        if(this._item.BasedUOM != null)
                        {
                            this.DefaultUOM = this._item.BasedUOM;
                        }
                        if(this._item.Brand != null)
                        {
                            this.Brand = this._item.Brand;
                        }
                    }
                }
            }
        }

        public Brand Brand
        {
            get { return _brand; }
            set { SetPropertyValue("Brand", ref _brand, value); }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public Location Location
        {
            get { return _location; }
            set { SetPropertyValue("Location", ref _location, value); }
        }

        [DataSourceCriteria("Active = true And Location = '@This.Location'")]
        public BinLocation BinLocation
        {
            get { return _binLocation; }
            set { SetPropertyValue("BinLocation", ref _binLocation, value); }
        }

        public UnitOfMeasure UnitPack
        {
            get { return _unitPack; }
            set { SetPropertyValue("UnitPack", ref _unitPack, value); }
        }

        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        public double QtyBegining
        {
            get { return _qtyBegining; }
            set { SetPropertyValue("QtyBegining", ref _qtyBegining, value); }
        }

        public double QtyMinimal
        {
            get { return _qtyMinimal; }
            set { SetPropertyValue("QtyMinimal", ref _qtyMinimal, value); }
        }

        [ImmediatePostData()]
        public double QtyAvailable
        {
            get { return _qtyAvailable; }
            set { SetPropertyValue("QtyAvailable", ref _qtyAvailable, value); }
        }

        public double QtySale
        {
            get { return _qtySale; }
            set { SetPropertyValue("QtySale", ref _qtySale, value); }
        }

        [DataSourceCriteria("Active = true")]
        public UnitOfMeasure DefaultUOM
        {
            get { return _defaultUom; }
            set { SetPropertyValue("DefaultUOM", ref _defaultUom, value); }
        }

        public StockType StockType
        {
            get { return _stockType; }
            set { SetPropertyValue("StockType", ref _stockType, value); }
        }

        [ImmediatePostData()]
        public string LotNumber
        {
            get { return _lotNumber; }
            set { SetPropertyValue("LotNumber", ref _lotNumber, value); }
        }

        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        public ProjectHeader ProjectHeader
        {
            get { return _projectHeader; }
            set { SetPropertyValue("ProjectHeader", ref _projectHeader, value); }
        }

        [ImmediatePostData()]
        [Association("BeginingInventory-BeginingInventoryLines")]
        public BeginingInventory BeginingInventory
        {
            get { return _beginingInventory; }
            set
            {
                SetPropertyValue("BeginingInventory", ref _beginingInventory, value);
                if (!IsLoading)
                {
                    if (this._beginingInventory != null)
                    {
                        if (this._beginingInventory.DefaultUOM != null)
                        {
                            this.DefaultUOM = this._beginingInventory.DefaultUOM;
                        }
                        if (this._beginingInventory.Item != null)
                        {
                            this.Item = this._beginingInventory.Item;
                        }
                        this.QtyBegining = this._beginingInventory.QtyBegining;
                        this.QtyMinimal = this._beginingInventory.QtyMinimal;
                        if (this._beginingInventory.Company != null)
                        {
                            this.Company = this._beginingInventory.Company;
                        }
                    }
                }
            }
        }

        #endregion Field

    }
}