﻿        #region Default
using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.ExpressApp.ConditionalAppearance;
using FullDrive.Module.CustomProcess;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Payroll")]
    [RuleCombinationOfPropertiesIsUnique("SalaryBasicRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class SalaryBasic : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private string _name;
        private double _salaryBasicValue;
        private string _salaryBasicValueText;
        private double _salaryBasicValueLCY;
        private Currency _currencyCode;
        private double _currencyRate;
        private Employee _employee;
        private Period _period;
        private bool _useActiveCurrencyForCurrencyRate;
        private GlobalFunction _globFunc;
        public SalaryBasic(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.SalaryBasic);
                this._useActiveCurrencyForCurrencyRate = true;
                //this._currencyRate = 1;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }
        #endregion

        #region Field
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [ImmediatePostData()]
        public string Name
        {
            get
            {
                if (Employee != null & Period != null)
                    return string.Format("Salary Basic - {0:0000} - {1:00} {2} [ {3} ]", Period.PeriodYear, Period.PeriodMonth, Employee.Name, Employee.Code);
                else
                    return "";
            }
        }

        [ImmediatePostData()]
        [Association("Employee-SalaryBasics")]
        public Employee Employee
        {
            get { return _employee; }
            set { SetPropertyValue("Employee", ref _employee, value); }
        }

        [ImmediatePostData()]
        [Association("Period-SalaryBasics")]
        public Period Period
        {
            get { return _period; }
            set { SetPropertyValue("Period", ref _period, value); }
        }

        [ImmediatePostData()]
        public double SalaryBasicValue
        {
            get { return _salaryBasicValue; }
            set {
                    SetPropertyValue("SalaryBasicValue", ref _salaryBasicValue, value);
                    if (SalaryBasicValue > 0)
                        {
                            this._salaryBasicValueText = this._salaryBasicValue.ToString();
                            this._salaryBasicValueLCY = this._salaryBasicValue * this.CurrencyRate;
                        }
                }
        }

        [ImmediatePostData()]
        public string SalaryBasicValueText
        {
            get { return _salaryBasicValueText; }
            set { SetPropertyValue("SalaryBasicValueText", ref _salaryBasicValueText, value); }
        }

        [ImmediatePostData()]
        public double SalaryBasicLCY
        {
            
            get {
                //return _salaryBasicValueLCY;
                if (SalaryBasicValue >= 0)
                {
                    _salaryBasicValueLCY = SalaryBasicValue * CurrencyRate;
                }
                else
                {
                    return _salaryBasicValueLCY;
                }
                return _salaryBasicValueLCY;
            }
            set { SetPropertyValue("SalaryBasicValueLCY", ref _salaryBasicValueLCY, value); }
            // SalaryValue * CurrencyRate
        }

        public Currency CurrencyCode
        {
            get
            {
                if (this.Employee == null)
                {
                    return null;
                }
                else if (this.Employee.Currency == null)
                {
                    return null;
                }
                else
                {
                    return this.Employee.Currency;
                }
            }
            //get { return _currencyCode; }
            //set { SetPropertyValue("CurrencyCode", ref _currencyCode, value); }
        }

        public double CurrencyRate
        {
            get {
                    if (Employee != null)
                    {
                        if ( Employee.Currency != null)
                        {
                        SetCurrencyRate();
                        //SetSalaryBasicLCY();
                    }
                }
                return _currencyRate; }
            set { SetPropertyValue("CurrencyRate", ref _currencyRate, value); }
        }
        
        [ImmediatePostData()]
        public bool UseActiveCurrencyForCurrencyRate
        {
            get { return _useActiveCurrencyForCurrencyRate; }
            set { SetPropertyValue("UseActiveCurrencyForCurrencyRate", ref _useActiveCurrencyForCurrencyRate, value); }
        }
        #endregion Field

        //#region GetCurrencyRate
        //public double UpdateCurrencyRate(IDataLayer _currIDataLayer)
        //{
        //    double _result = 0;
        //    try
        //    {
        //        //
        //        string _conversion = this._employee.Currency.ShortName;
        //        Session _generatorSession = new Session(_currIDataLayer);
        
        //        Currency _currency = _generatorSession.FindObject<Currency>(new GroupOperator(GroupOperatorType.And,
        //                             new BinaryOperator("Default", true), new BinaryOperator("Active", true)));
        //        if (_currency != null)
        //        {
        //            string _default = _currency.ToString();
        //            CurrencyRate _currencyRateXPO = _generatorSession.FindObject<CurrencyRate>
        //                                                (new GroupOperator(GroupOperatorType.And,
        //                                                 //new BinaryOperator("Currency1", this._employee.Currency),
        //                                                 new BinaryOperator("Currency", _currency)));
        //                                                 //new BinaryOperator("Active", true)));
        //            //new BinaryOperator("Currency2", this.Employee.Currency)));
        //            //new BinaryOperator("Active", true)));
        //            if (_currencyRateXPO != null)
        //            {

        //                {
        //                    _result = _currencyRateXPO.Rate1;
        //                    _currencyRateXPO.Save();
        //                    _currencyRateXPO.Session.CommitTransaction();
        //                }
        //            }    
        //            }
        //            }
        //    catch (Exception ex)
        //    {
        //        Tracing.Tracer.LogError(" BusinessObject = PayrollProcess " + ex.ToString());
        //    }
        //    return _result;
        //}
        //#endregion

        #region SetCurrencyRate
        public void SetCurrencyRate()
        {
            try
            {
                double _result = 0;
                if (Employee != null)
                {
                    if (Employee.Currency != null)
                    {
                        Currency _locCurrency = Session.FindObject<Currency>
                                                   (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Active", true),
                                                    new BinaryOperator("Default", true)));
                        if (_locCurrency != null)
                        {
                            //if (_locCurrency != this._employee.Currency)
                            //{
                                CurrencyRate _loccurrencyRate = Session.FindObject<CurrencyRate>
                                             (new GroupOperator(GroupOperatorType.And,
                                              new BinaryOperator("Currency1", _locCurrency),
                                              new BinaryOperator("Currency2", this._employee.Currency)));
                         
                                if (_loccurrencyRate != null)
                                {
                                _result = _loccurrencyRate.Rate1;
                                this._currencyRate = _result;
                                
                               
                            }
                                //this._currencyRate = 1;
                            //}
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalaryBasic" + ex.ToString());
            }
        }
        #endregion

        #region SetSalaryBasicLCY
        public void SetSalaryBasicLCY()
        {
            if (Employee != null)
            {
                if (Employee.Currency != null)
                {
                    if (SalaryBasicValue > 0)
                    {
                        if (CurrencyRate > 0)
                        {
                            this._salaryBasicValueLCY = this._salaryBasicValue * this._currencyRate;
                        }
                    }
                }
            }
        }
        #endregion

        //
    }
}