﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;


namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("TaxRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class Tax : FullDriveSysBaseObject
    {
        #region Default

        private string _code;
        private string _name;
        private TaxType _taxType;
        private TaxGroup _taxGroup;
        private TaxGroupType _taxGroupType;
        private TaxAccountGroup _taxAccountGroup;
        private double _value;
        private TaxNature _taxNature;
        private TaxMode _taxMode;
        private string _description;
        private bool _active;
        private bool _default;
        private GlobalFunction _globFunc;

        public Tax(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.Tax);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #endregion Default

        #region Field

        //[Appearance("TaxCodeEnabled", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [DataSourceCriteria("Active = true")]
        public TaxType TaxType
        {
            get { return _taxType; }
            set { SetPropertyValue("TaxType", ref _taxType, value); }
        }

        [DataSourceCriteria("Active = true")]
        public TaxGroup TaxGroup
        {
            get { return _taxGroup; }
            set
            {
                SetPropertyValue("TaxGroup", ref _taxGroup, value);
                if (!IsLoading)
                {
                    if (this._taxGroup != null)
                    {
                        this.TaxGroupType = this._taxGroup.TaxGroupType;
                    }
                }
            }
        }

        public TaxGroupType TaxGroupType
        {
            get { return _taxGroupType; }
            set { SetPropertyValue("TaxGroupType", ref _taxGroupType, value); }
        }

        [Association("TaxAccountGroup-Taxs")]
        [DataSourceCriteria("Active = true")]
        public TaxAccountGroup TaxAccountGroup
        {
            get { return _taxAccountGroup; }
            set { SetPropertyValue("TaxAccountGroup", ref _taxAccountGroup, value); }
        }

        public TaxNature TaxNature
        {
            get { return _taxNature; }
            set { SetPropertyValue("TaxNature", ref _taxNature, value); }
        }

        public TaxMode TaxMode
        {
            get { return _taxMode; }
            set { SetPropertyValue("TaxMode", ref _taxMode, value); }
        }

        public double Value
        {
            get { return _value; }
            set { SetPropertyValue("Value", ref _value, value); }
        }

        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        public bool Default
        {
            get { return _default; }
            set { SetPropertyValue("Default", ref _default, value); }
        }

        [Association("Tax-TaxLines")]
        public XPCollection<TaxLine> TaxLines
        {
            get { return GetCollection<TaxLine>("TaxLines"); }
        }

        #endregion Field
    }
}