﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Inventory")]
    [RuleCombinationOfPropertiesIsUnique("ItemConsumptionRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class ItemConsumption : FullDriveSysBaseObject
    {
        #region Default

        private bool _activationPosting;
        private string _code;
        private string _name;
        private ProjectHeader _projectHeader;
        private Location _location;
        private BinLocation _binLocation;
        XPCollection<DocumentType> _availableDocumentType;
        private string _docNo;
        private BusinessPartner _businessPartner;
        private Country _businessPartnerCountry;
        private City _businessPartnerCity;
        private string _businessPartnerAddress;
        private DocumentType _documentType;
        private DirectionType _transferType;
        private InventoryMovingType _inventoryMovingType;
        private ObjectList _objectList;
        private Status _status;
        private StockType _stockType;
        private DateTime _statusDate;
        private DateTime _estimatedDate;
        private string _description;
        private string _userAccess;
        private Company _company;
        private GlobalFunction _globFunc;
        //clm
        private bool _activeApproved1;
        private bool _activeApproved2;
        private bool _activeApproved3;

        public ItemConsumption(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ItemConsumption);
                DateTime now = DateTime.Now;
                this.Status = CustomProcess.Status.Open;
                this.StatusDate = now;
                this.InventoryMovingType = InventoryMovingType.Consumpt;
                this.ObjectList = ObjectList.ItemConsumption;

                _userAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                if (Session.IsNewObject(this))
                {
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null)
                            {
                                Company = _locUserAccess.Employee.Company;
                            }
                        }
                    }
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #endregion Default

        #region Field

        [Browsable(false)]
        [Appearance("ItemConsumptionActivationPostingEnabled", Enabled = false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [ImmediatePostData()]
        [Appearance("ItemConsumptionCodeClose", Enabled = false)]
        [Appearance("ItemConsumptionRedColor", Criteria = "ActiveApproved1 = true", BackColor = "#fd79a8")]
        [Appearance("ItemConsumptionYellowColor", Criteria = "ActiveApproved2 = true", BackColor = "#ffeaa7")]
        [Appearance("ItemConsumptionGreenOColor", Criteria = "ActiveApproved3 = true", BackColor = "#81ecec")]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("ItemConsumptionCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        //[Appearance("itemConsumptionTransferTypeEnabled", Enabled = false)]
        public DirectionType TransferType
        {
            get { return _transferType; }
            set { SetPropertyValue("TransferType", ref _transferType, value); }
        }

        //[Appearance("itemConsumptionInventoryMovingTypeEnabled", Enabled = false)]
        public InventoryMovingType InventoryMovingType
        {
            get { return _inventoryMovingType; }
            set { SetPropertyValue("InventoryMovingType", ref _inventoryMovingType, value); }
        }

        //[Appearance("itemConsumptionDocumentTypeEnabled", Enabled = false)]
        public DocumentType DocumentType
        {
            get { return _documentType; }
            set { SetPropertyValue("DocumentType", ref _documentType, value); }
        }

        //[Appearance("itemConsumptionDocCodeEnabled", Enabled = false)]
        public string DocNo
        {
            get { return _docNo; }
            set { SetPropertyValue("DocNo", ref _docNo, value); }
        }

        //[Appearance("ItemConsumptionBusinessPartnerClose", Enabled = false)]
        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public BusinessPartner BusinessPartner
        {
            get { return _businessPartner; }
            set
            {
                SetPropertyValue("BusinessPartner", ref _businessPartner, value);
                if (!IsLoading)
                {
                    if (_businessPartner != null)
                    {
                        this.BusinessPartnerCountry = _businessPartner.Country;
                        this.BusinessPartnerCity = _businessPartner.City;
                        this.BusinessPartnerAddress = _businessPartner.Address;
                    }
                }
            }
        }

        //[Appearance("ItemConsumptionBusinessPartnerCountryClose", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Country BusinessPartnerCountry
        {
            get { return _businessPartnerCountry; }
            set { SetPropertyValue("BusinessPartnerCountry", ref _businessPartnerCountry, value); }
        }

        //[Appearance("ItemConsumptionBusinessPartnerCityClose", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public City BusinessPartnerCity
        {
            get { return _businessPartnerCity; }
            set { SetPropertyValue("BusinessPartnerCity", ref _businessPartnerCity, value); }
        }

        //[Appearance("ItemConsumptionBusinessPartnerAddressClose", Enabled = false)]
        [Size(512)]
        public string BusinessPartnerAddress
        {
            get { return _businessPartnerAddress; }
            set { SetPropertyValue("BusinessPartnerAddress", ref _businessPartnerAddress, value); }
        }

        public ProjectHeader ProjectHeader
        {
            get { return _projectHeader; }
            set
            {
                SetPropertyValue("ProjectHeader", ref _projectHeader, value);
                if (!IsLoading)
                {
                    if (_projectHeader != null)
                    {
                        this.Company = this._company;
                    }
                }
            }
        }

        [Browsable(false)]
        public XPCollection<DocumentType> AvailableDocumentType
        {
            get
            {
                if (ObjectList == ObjectList.None)
                {
                    _availableDocumentType = new XPCollection<DocumentType>(Session);
                }
                else
                {
                    _availableDocumentType = new XPCollection<DocumentType>(Session,
                                              new GroupOperator(GroupOperatorType.And,
                                              new BinaryOperator("ObjectList", this.ObjectList),
                                              new BinaryOperator("InventoryMovingType", this.InventoryMovingType),
                                              new BinaryOperator("TransferType", this.TransferType),
                                              new BinaryOperator("Active", true)));
                }
                return _availableDocumentType;

            }
        }

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        public Location Location
        {
            get { return _location; }
            set { SetPropertyValue("Location", ref _location, value); }
        }

        //[Appearance("ItemConsumptionBinLocationClose", Enabled = false)]
        [DataSourceCriteria("Active = true And Location = '@This.Location'")]
        public BinLocation BinLocation
        {
            get { return _binLocation; }
            set { SetPropertyValue("BinLocation", ref _binLocation, value); }
        }

        [Browsable(false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Browsable(false)]
        public ObjectList ObjectList
        {
            get { return _objectList; }
            set { SetPropertyValue("ObjectList", ref _objectList, value); }
        }

        //[Appearance("ItemConsumptionStockTypeClose", Enabled = false)]
        public StockType StockType
        {
            get { return _stockType; }
            set { SetPropertyValue("StockType", ref _stockType, value); }
        }

        [Appearance("ItemConsumptionStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("ItemConsumptionStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        //[Appearance("ItemConsumptionEstimatedEnabled", Enabled = false)]
        public DateTime EstimatedDate
        {
            get { return _estimatedDate; }
            set { SetPropertyValue("EstimatedDate", ref _estimatedDate, value); }
        }

        [Size(512)]
        public String Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Association("ItemConsumption-ItemConsumptionLines")]
        public XPCollection<ItemConsumptionLine> ItemConsumptionLines
        {
            get { return GetCollection<ItemConsumptionLine>("ItemConsumptionLines"); }
        }

        [Association("ItemConsumption-ApprovalLines")]
        public XPCollection<ApprovalLine> ApprovalLines
        {
            get { return GetCollection<ApprovalLine>("ApprovalLines"); }
        }

        //clm
        [Browsable(false)]
        [Appearance("ItemConsumptionActiveApproved1Close", Enabled = false)]
        public bool ActiveApproved1
        {
            get { return _activeApproved1; }
            set { SetPropertyValue("ActiveApproved1", ref _activeApproved1, value); }
        }

        [Browsable(false)]
        [Appearance("ItemConsumptionActiveApproved2Close", Enabled = false)]
        public bool ActiveApproved2
        {
            get { return _activeApproved2; }
            set { SetPropertyValue("ActiveApproved2", ref _activeApproved2, value); }
        }

        [Browsable(false)]
        [Appearance("ItemConsumptionActiveApproved3Close", Enabled = false)]
        public bool ActiveApproved3
        {
            get { return _activeApproved3; }
            set { SetPropertyValue("ActiveApproved3", ref _activeApproved3, value); }
        }

        #endregion Default

    }
}