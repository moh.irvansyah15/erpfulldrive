﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Inventory")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class BeginingInventory : FullDriveSysBaseObject
    {
        #region Default

        private string _code;
        private Item _item;
        private string _productCode;
        private Country _country;
        private City _city;
        private double _qtyBegining;
        private double _qtyMinimal;
        private double _qtyAvailable;
        private double _qtySale;
        private InventoryProduction _inventoryProduction;
        private UnitOfMeasure _defaultUOM;
        private Brand _brand;
        private bool _active;
        private string _signCode;
        private string _userAccess;
        private Company _company;
        private GlobalFunction _globFunc;

        public BeginingInventory(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.BeginingInventory);

                _userAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                if (Session.IsNewObject(this))
                {
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null)
                            {
                                Company = _locUserAccess.Employee.Company;
                            }
                        }
                    }
                }
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #endregion Default

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("BeginingInventoryCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [ImmediatePostData()]
        public Item Item
        {
            get { return _item; }
            set {
                SetPropertyValue("Item", ref _item, value);
                if(!IsLoading)
                {
                    if(this._item != null)
                    {
                        if(this._item.Brand != null)
                        {
                            this.Brand = this._item.Brand;
                        }
                        if (this._item.Code != null)
                        {
                            this.ProductCode = this._item.Code;
                        }
                    }
                }
            }
        }

        public string ProductCode
        {
            get { return _productCode; }
            set { SetPropertyValue("ProductCode", ref _productCode, value); }
        }

        public Country Country
        {
            get { return _country; }
            set { SetPropertyValue("Country", ref _country, value); }
        }

        public City City
        {
            get { return _city; }
            set { SetPropertyValue("City", ref _city, value); }
        }

        public double QtyBegining
        {
            get { return _qtyBegining; }
            set { SetPropertyValue("QtyBegining", ref _qtyBegining, value); }
        }

        public double QtyMinimal
        {
            get { return _qtyMinimal; }
            set { SetPropertyValue("QtyMinimal", ref _qtyMinimal, value); }
        }

        public double QtyAvailable
        {
            get { return _qtyAvailable; }
            set { SetPropertyValue("QtyAvailable", ref _qtyAvailable, value); }
        }

        public double QtySale
        {
            get { return _qtySale; }
            set { SetPropertyValue("QtySale", ref _qtySale, value); }
        }

        [Appearance("BeginingInventoryInventoryProductionClose", Enabled = false)]
        public InventoryProduction InventoryProduction
        {
            get { return _inventoryProduction; }
            set { SetPropertyValue("InventoryProduction", ref _inventoryProduction, value); }
        }

        public UnitOfMeasure DefaultUOM
        {
            get { return _defaultUOM; }
            set { SetPropertyValue("DefaultUOM", ref _defaultUOM, value); }
        }

        public Brand Brand
        {
            get { return _brand; }
            set { SetPropertyValue("Brand", ref _brand, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Browsable(false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [Association("BeginingInventory-BeginingInventoryLines")]
        public XPCollection<BeginingInventoryLine> BeginingInventoryLines
        {
            get { return GetCollection<BeginingInventoryLine>("BeginingInventoryLines"); }
        }

        #endregion Field

        #region Code

        [Action(Caption = "ChangeQuantityAvailable", ConfirmationMessage = "Are you sure?", AutoCommit = true)]
        public void ChangeQuantityAvailable()
        {
            try
            {
                double _locQtyAvailable = 0;
                XPCollection<BeginingInventoryLine> _locBeginingInventoryLines = new XPCollection<BeginingInventoryLine>
                                                                (Session, new GroupOperator(GroupOperatorType.And, 
                                                                new BinaryOperator("BeginingInventory", this)));
                if (_locBeginingInventoryLines != null && _locBeginingInventoryLines.Count() > 0)
                {
                    foreach(BeginingInventoryLine _locBeginingInventoryLine in _locBeginingInventoryLines)
                    {
                        _locQtyAvailable = _locQtyAvailable + _locBeginingInventoryLine.QtyAvailable;
                    }

                    if (_locQtyAvailable != this.QtyAvailable)
                    {
                        this.QtyAvailable = _locQtyAvailable;
                        this.Save();
                        this.Session.CommitTransaction();
                    }
                }
                   
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = BeginingInventory ", ex.ToString());
            }
        }

        [Action(Caption = "UpdateProductCode", ConfirmationMessage = "Are you sure?", AutoCommit = true)]
        public void UpdateProductCode()
        {
            try
            {
                Item _locUpdate = Session.FindObject<Item>
                                 (new GroupOperator(GroupOperatorType.And,
                                  new BinaryOperator("Code", this.Item.Code)));

                if (_locUpdate != null)
                {
                    if (_locUpdate.Code != null)
                    {
                        this.ProductCode = _locUpdate.Code;
                        this.Save();
                        this.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Business Object = BeginingInventory ", ex.ToString());
            }
        }

        #endregion Code

    }
}