﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("CreditMemoCollectionRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class CreditMemoCollection : FullDriveSysBaseObject
    {
        #region Default

        private bool _activationPosting;
        private string _code;
        private BusinessPartner _buyFromVendor;
        private string _buyFromContact;
        private Country _buyFromCountry;
        private City _buyFromCity;
        private string _buyFromAddress;
        private TermOfPayment _top;
        private Status _status;
        private CreditMemo _creditMemo;
        private double _cm_amount;
        private DateTime _statusDate;
        private string _signCode;
        private PurchaseReturn _purchaseReturn;
        private PurchaseOrder _purchaseOrder;
        private InventoryTransferOut _inventoryTransferOut;
        private GlobalFunction _globFunc;
        private PurchaseInvoice _purchaseInvoice;
        private PurchaseInvoiceCollection _purchaseInvoiceCollection;

        public CreditMemoCollection(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.CreditMemoCollection);
                this.Status = Status.Open;
                this.StatusDate = now;
            }
        }

        #endregion Default

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("CreditMemoCollectionCodeEnabled", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("CreditMemoCollectionBuyFromVendorClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner BuyFromVendor
        {
            get { return _buyFromVendor; }
            set { SetPropertyValue("BuyFromVendor", ref _buyFromVendor, value); }
        }

        [Appearance("CreditMemoCollectionBuyFromContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string BuyFromContact
        {
            get { return _buyFromContact; }
            set { SetPropertyValue("BuyFromContact", ref _buyFromContact, value); }
        }

        [Appearance("CreditMemoCollectionBuyFromCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country BuyFromCountry
        {
            get { return _buyFromCountry; }
            set { SetPropertyValue("BuyFromCountry", ref _buyFromCountry, value); }
        }

        [Appearance("CreditMemoCollectionBuyFromBuyFromCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City BuyFromCity
        {
            get { return _buyFromCity; }
            set { SetPropertyValue("BuyFromCity", ref _buyFromCity, value); }
        }

        [Size(512)]
        [Appearance("CreditMemoCollectionBuyFromAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string BuyFromAddress
        {
            get { return _buyFromAddress; }
            set { SetPropertyValue("BuyFromAddress", ref _buyFromAddress, value); }
        }

        [Appearance("CreditMemoCollectionBuyFromTOPClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public TermOfPayment TOP
        {
            get { return _top; }
            set { SetPropertyValue("TOP", ref _top, value); }
        }

        [Appearance("CreditMemoCollectionStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("CreditMemoCollectionStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("CreditMemoCollectionSignCodeEnabled", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        //clm
        [Appearance("CreditMemoCollectionCreditMemoEnabled", Enabled = false)]
        public CreditMemo CreditMemo
        {
            get { return _creditMemo; }
            set { SetPropertyValue("CreditMemo", ref _creditMemo, value); }
        }

        [Appearance("CreditMemoCollectionCMAmountEnabled", Enabled = false)]
        public double CM_Amount
        {
            get { return _cm_amount; }
            set { SetPropertyValue("CM_Amount", ref _cm_amount, value); }
        }

        [Appearance("CreditMemoCollectionPurchaseReturnClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [ImmediatePostData()]
        public PurchaseReturn PurchaseReturn
        {
            get { return _purchaseReturn; }
            set
            {
                SetPropertyValue("PurchaseReturn", ref _purchaseReturn, value);
                if (!IsLoading)
                {
                    if (this._purchaseReturn != null)
                    {
                        if (this._purchaseReturn.PurchaseOrder != null)
                        {
                            this.PurchaseOrder = _purchaseReturn.PurchaseOrder;
                        }
                    }
                }
            }
        }

        [Appearance("CreditMemoCollectionPurchaseOrderClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PurchaseOrder PurchaseOrder
        {
            get { return _purchaseOrder; }
            set { SetPropertyValue("PurchaseOrder", ref _purchaseOrder, value); }
        }

        [Browsable(false)]
        [Appearance("CreditMemoCollectionInventoryTransferClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public InventoryTransferOut InventoryTransferOut
        {
            get { return _inventoryTransferOut; }
            set { SetPropertyValue("InventoryTransferOut", ref _inventoryTransferOut, value); }
        }

        //Wahab 27-02-2020
        //[Association("PurchaseInvoice-CreditMemoCollections")]
        public PurchaseInvoice PurchaseInvoice
        {
            get { return _purchaseInvoice; }
            set { SetPropertyValue("PurchaseInvoice", ref _purchaseInvoice, value); }
        }

        [Association("PurchaseInvoiceCollection-CreditMemoCollections")]
        public PurchaseInvoiceCollection PurchaseInvoiceCollection
        {
            get { return _purchaseInvoiceCollection; }
            set { SetPropertyValue("PurchaseInvoiceCollection", ref _purchaseInvoiceCollection, value); }
        }

        #endregion Field

    }
}