﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Inventory")]
    [RuleCombinationOfPropertiesIsUnique("ItemBillOfMaterialRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class ItemBillOfMaterial : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private string _name;
        private BillOfMaterialVersion _billOfMaterialVersion;
        private BillOfMaterial _billOfMaterial;
        private bool _active;
        private Item _item;
        private GlobalFunction _globFunc;

        public ItemBillOfMaterial(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.ItemBillOfMaterial);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }
        
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [ImmediatePostData()]
        public BillOfMaterialVersion BillOfMaterialVersion
        {
            get { return _billOfMaterialVersion; }
            set { SetPropertyValue("BillOfMaterialVersion", ref _billOfMaterialVersion, value); }
        }

        [DataSourceCriteria("Active = true And BillOfMaterialVersion = '@This.BillOfMaterialVersion' And Item = '@This.Item'"), ImmediatePostData()]
        public BillOfMaterial BillOfMaterial
        {
            get { return _billOfMaterial; }
            set { SetPropertyValue("BillOfMaterial", ref _billOfMaterial, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        [Association("Item-ItemBillOfMaterials")]
        public Item Item
        {
            get { return _item; }
            set {
                SetPropertyValue("Item", ref _item, value);
                if(!IsLoading)
                {
                    if(this._item != null)
                    {
                        if(this._item.BillOfMaterialVersion != null)
                        {
                            this.BillOfMaterialVersion = this._item.BillOfMaterialVersion;
                        }
                    }
                }
            }
        }

        #endregion Field
    }
}