﻿#region Default

using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.CustomProcess;

namespace FullDrive.Module.BusinessObjects
{
    #region Default

    [DefaultClassOptions]
    [DefaultProperty("Name")]
    [NavigationItem("Organization")]
    [RuleCombinationOfPropertiesIsUnique("DepartmentRuleUnique", DefaultContexts.Save, "Code")]

    #endregion Default

    // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
    public class Department : FullDriveSysBaseObject
    {
        #region Default

        private string _code;
        private string _name;
        private DepartmentAccountGroup _departmentAccountGroup;
        private Division _division;
        private bool _active;
        private GlobalFunction _globFunc;

        public Department(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.Department);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #endregion Default

        #region Field

        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [RuleRequiredField(DefaultContexts.Save)]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Association("DepartmentAccountGroup-Departments")]
        [DataSourceCriteria("Active = true")]
        public DepartmentAccountGroup DepartmentAccountGroup
        {
            get { return _departmentAccountGroup; }
            set { SetPropertyValue("DepartmentAccountGroup", ref _departmentAccountGroup, value); }
        }

        [Association("Division-Departments")]
        [DataSourceCriteria("Active = true"), ImmediatePostData()]
        public Division Division
        {
            get { return _division; }
            set { SetPropertyValue("Division", ref _division, value); }
        }

        public bool Active
        {
            get { return _active; }
            set { SetPropertyValue("Active", ref _active, value); }
        }

        [Association("Department-Sections")]
        public XPCollection<Section> Sections
        {
            get { return GetCollection<Section>("Sections"); }
        }

        [Association("Department-Employees")]
        public XPCollection<Employee> Employees
        {
            get { return GetCollection<Employee>("Employees"); }
        }

        #endregion Field

    }
}