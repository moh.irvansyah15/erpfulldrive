﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.Editors;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("SalesPrePaymentInvoiceRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class SalesPrePaymentInvoice : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private bool _activationPosting;
        private string _code;
        private bool _select;
        #region Sales
        private BusinessPartner _salesToCostumer;
        private string _salesToContact;
        private Country _salesToCountry;
        private City _salesToCity;
        private string _salesToAddress;
        #endregion Sales
        #region Bill
        private BusinessPartner _billToCostumer;
        private string _billToContact;
        private Country _billToCountry;
        private City _billToCity;
        private string _billToAddress;
        #endregion Bill
        private PriceGroup _priceGroup;
        private PaymentMethod _paymentMethod;
        private PaymentMethodType _paymentMethodType;
        private PaymentType _paymentType;
        private TermOfPayment _top;
        private Currency _currency;
        #region Bank
        private XPCollection<BankAccount> _availableBankAccountCompany;
        private BankAccount _bankAccountCompany;
        private string _companyAccountNo;
        private string _companyAccountName;
        private XPCollection<BankAccount> _availableBankAccounts;
        private BankAccount _bankAccount;
        private string _accountNo;
        private string _accountName;
        #endregion Bank
        private string _taxNo;
        private double _maxBill;
        private double _bill;
        private double _dp_percentage;
        private double _dp_Amount;
        private double _plan;
        private double _outstanding;
        private string _description;
        private int _postedCount;
        private Status _status;
        private Company _company;
        private DateTime _statusDate;
        private DateTime _estimatedDate;
        private string _signCode;
        private string _userAccess;
        private XPCollection<SalesOrder> _availableSalesOrder;
        private SalesOrder _salesOrder;
        private ReceivableTransaction _receivableTransaction;
        private ProjectHeader _projectHeader;
        private GlobalFunction _globFunc;

        private bool _activeApproved1;
        private bool _activeApproved2;
        private bool _activeApproved3;

        public SalesPrePaymentInvoice(Session session)
            : base(session)
        {
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.SalesPrePaymentInvoice);
                this.Status = CustomProcess.Status.Open;
                this.StatusDate = now;
                this.Select = true;

                _userAccess = SecuritySystem.CurrentUserName;
                UserAccess _locUserAccess = Session.FindObject<UserAccess>(new BinaryOperator("UserName", _userAccess));
                if (Session.IsNewObject(this))
                {
                    if (_locUserAccess != null)
                    {
                        if (_locUserAccess.Employee != null)
                        {
                            if (_locUserAccess.Employee.Company != null)
                            {
                                Company = _locUserAccess.Employee.Company;
                            }
                        }
                    }
                }

                this.Currency = _globFunc.GetDefaultCurrency(this.Session);
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        #region Field

        [Browsable(false)]
        public bool ActivationPosting
        {
            get { return _activationPosting; }
            set { SetPropertyValue("ActivationPosting", ref _activationPosting, value); }
        }

        [Appearance("SalesOrderRedColor", Criteria = "ActiveApproved1 = true", BackColor = "#fd79a8")]
        [Appearance("SalesOrderYellowColor", Criteria = "ActiveApproved2 = true", BackColor = "#ffeaa7")]
        [Appearance("SalesOrderGreenOColor", Criteria = "ActiveApproved3 = true", BackColor = "#81ecec")]
        [RuleRequiredField(DefaultContexts.Save)]
        [Appearance("SalesPrePaymentInvoiceCodeClose", Enabled = false)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("SalesPrePaymentInvoiceSelectClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public bool Select
        {
            get { return _select; }
            set { SetPropertyValue("Select", ref _select, value); }
        }

        #region SalesToCostumer

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        [Appearance("SalesPrePaymentInvoiceSalesToCostumerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner SalesToCostumer
        {
            get { return _salesToCostumer; }
            set {
                SetPropertyValue("SalesToCostumer", ref _salesToCostumer, value);
                if (!IsLoading)
                {
                    if (_salesToCostumer != null)
                    {
                        this.SalesToContact = _salesToCostumer.Contact;
                        this.SalesToCountry = _salesToCostumer.Country;
                        this.SalesToCity = _salesToCostumer.City;
                        this.SalesToAddress = this.SalesToAddress;
                        this.BillToCostumer = _salesToCostumer;
                    }
                }
            }
        }

        [Appearance("SalesPrePaymentInvoiceSalesToContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string SalesToContact
        {
            get { return _salesToContact; }
            set { SetPropertyValue("SalesToContact", ref _salesToContact, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("SalesPrePaymentInvoiceSalesCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country SalesToCountry
        {
            get { return _salesToCountry; }
            set { SetPropertyValue("SalesToCountry", ref _salesToCountry, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("SalesPrePaymentInvoiceSalesToCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City SalesToCity
        {
            get { return _salesToCity; }
            set { SetPropertyValue("SalesToCity", ref _salesToCity, value); }
        }

        [Appearance("SalesPrePaymentInvoiceSalesToAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string SalesToAddress
        {
            get { return _salesToAddress; }
            set { SetPropertyValue("SalesToAddress", ref _salesToAddress, value); }
        }

        #endregion SalesToCostumer

        #region BillToCostumer

        [ImmediatePostData()]
        [DataSourceCriteria("Active = true")]
        [Appearance("SalesPrePaymentInvoiceBillToCostumerClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BusinessPartner BillToCostumer
        {
            get { return _billToCostumer; }
            set
            {
                SetPropertyValue("BillToCostumer", ref _billToCostumer, value);
                if (!IsLoading)
                {
                    if (this._billToCostumer != null)
                    {
                        this.BillToContact = this._billToCostumer.Contact;
                        this.BillToAddress = this._billToCostumer.Address;
                        this.BillToCountry = this._billToCostumer.Country;
                        this.BillToCity = this._billToCostumer.City;
                    }
                }
            }
        }

        [Appearance("SalesPrePaymentInvoiceBillToContactClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string BillToContact
        {
            get { return _billToContact; }
            set { SetPropertyValue("BillToContact", ref _billToContact, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("SalesPrePaymentInvoiceBillCountryClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Country BillToCountry
        {
            get { return _billToCountry; }
            set { SetPropertyValue("BillToCountry", ref _billToCountry, value); }
        }

        [DataSourceCriteria("Active = true")]
        [Appearance("SalesPrePaymentInvoiceBillToCityClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public City BillToCity
        {
            get { return _billToCity; }
            set { SetPropertyValue("BillToCity", ref _billToCity, value); }
        }

        [Appearance("SalesPrePaymentInvoiceBillToAddressClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string BillToAddress
        {
            get { return _billToAddress; }
            set { SetPropertyValue("BillToAddress", ref _billToAddress, value); }
        }

        #endregion BillToCostumer

        [ImmediatePostData()]
        [Appearance("SalesPrePaymentInvoicePriceGroupClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PriceGroup PriceGroup
        {
            get { return _priceGroup; }
            set { SetPropertyValue("PriceGroup", ref _priceGroup, value); }
        }

        [Appearance("SalesPrePaymentInvoicePaymentMethodClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentMethod PaymentMethod
        {
            get { return _paymentMethod; }
            set { SetPropertyValue("PaymentMethod", ref _paymentMethod, value); }
        }

        [Appearance("SalesPrePaymentInvoicePaymentMethodTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentMethodType PaymentMethodType
        {
            get { return _paymentMethodType; }
            set { SetPropertyValue("PaymentMethodType", ref _paymentMethodType, value); }
        }

        [Appearance("SalesPrePaymentInvoicePaymentTypeClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public PaymentType PaymentType
        {
            get { return _paymentType; }
            set { SetPropertyValue("PaymentType", ref _paymentType, value); }
        }

        [Appearance("SalesPrePaymentInvoiceTermOfPaymentClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public TermOfPayment TOP
        {
            get { return _top; }
            set { SetPropertyValue("TOP", ref _top, value); }
        }

        [Appearance("SalesPrePaymentInvoiceCurrencyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public Currency Currency
        {
            get { return _currency; }
            set { SetPropertyValue("Currency", ref _currency, value); }
        }

        #region Bank

        [Browsable(false)]
        public XPCollection<BankAccount> AvailableBankAccountCompany
        {
            get
            {
                if (this.Company == null)
                {
                    _availableBankAccountCompany = new XPCollection<BankAccount>(Session);
                }
                else
                {
                    _availableBankAccountCompany = new XPCollection<BankAccount>(Session, new GroupOperator
                                                (GroupOperatorType.And,
                                                new BinaryOperator("Company", this.Company)));
                }
                return _availableBankAccountCompany;

            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableBankAccountCompany", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("SalesPrePaymentInvoiceBankAccountCompanyClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BankAccount BankAccountCompany
        {
            get { return _bankAccountCompany; }
            set
            {
                SetPropertyValue("BankAccountCompany", ref _bankAccountCompany, value);
                if (!IsLoading)
                {
                    if (this._bankAccountCompany != null)
                    {
                        this.CompanyAccountNo = this._bankAccountCompany.AccountNo;
                        this.CompanyAccountName = this._bankAccountCompany.AccountName;
                    }
                }
            }
        }

        [Appearance("SalesPrePaymentInvoiceCompanyAccountNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string CompanyAccountNo
        {
            get { return _companyAccountNo; }
            set { SetPropertyValue("CompanyAccountNo", ref _companyAccountNo, value); }
        }

        [Appearance("SalesPrePaymentInvoiceCompanyAccountNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string CompanyAccountName
        {
            get { return _companyAccountName; }
            set { SetPropertyValue("CompanyAccountName", ref _companyAccountName, value); }
        }

        [Browsable(false)]
        public XPCollection<BankAccount> AvailableBankAccounts
        {
            get
            {
                if (this.BillToCostumer == null)
                {
                    _availableBankAccounts = new XPCollection<BankAccount>(Session);
                }
                else
                {
                    _availableBankAccounts = new XPCollection<BankAccount>(Session,
                                             new GroupOperator(GroupOperatorType.And,
                                             new BinaryOperator("BusinessPartner", this.BillToCostumer)));
                }

                return _availableBankAccounts;
            }
        }

        [ImmediatePostData()]
        [DataSourceProperty("AvailableBankAccounts", DataSourcePropertyIsNullMode.SelectAll)]
        [Appearance("SalesPrePaymentInvoiceBankAccountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public BankAccount BankAccount
        {
            get { return _bankAccount; }
            set
            {
                SetPropertyValue("BankAccount", ref _bankAccount, value);
                if (!IsLoading)
                {
                    if (this._bankAccount != null)
                    {
                        this.AccountNo = this._bankAccount.AccountNo;
                        this.AccountName = this._bankAccount.AccountName;
                    }
                }
            }
        }

        [Appearance("SalesPrePaymentInvoiceAccountNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string AccountNo
        {
            get { return _accountNo; }
            set { SetPropertyValue("AccountNo", ref _accountNo, value); }
        }

        [Appearance("SalesPrePaymentInvoiceAccountNameClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string AccountName
        {
            get { return _accountName; }
            set { SetPropertyValue("AccountName", ref _accountName, value); }
        }

        #endregion Bank

        [Appearance("SalesPrePaymentInvoiceTaxNoClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public string TaxNo
        {
            get { return _taxNo; }
            set { SetPropertyValue("TaxNo", ref _taxNo, value); }
        }

        [Appearance("SalesPrePaymentInvoiceMaxPayClose", Enabled = false)]
        public double MaxBill
        {
            get { return _maxBill; }
            set { SetPropertyValue("MaxBill", ref _maxBill, value); }
        }

        [ImmediatePostData()]
        [Appearance("SalesPrePaymentInvoicePayClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double Bill
        {
            get { return _bill; }
            set
            {
                SetPropertyValue("Bill", ref _bill, value);
                if (!IsLoading)
                {
                    if (this.PostedCount > 0)
                    {
                        if (this.Outstanding > 0)
                        {
                            if (this._bill > 0 && this._bill > this.Outstanding)
                            {
                                this._bill = this.Outstanding;
                            }
                        }
                    }

                    if (this.PostedCount == 0)
                    {
                        if (this._bill > 0 && this._bill > this.MaxBill)
                        {
                            this._bill = this.MaxBill;
                        }
                    }
                }
            }
        }

        [ImmediatePostData()]
        [Appearance("SalesOrderDP_PercentageClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double DP_Percentage
        {
            get { return _dp_percentage; }
            set {
                SetPropertyValue("DP_Percentage", ref _dp_percentage, value);
                if(!IsLoading)
                {
                    if(this._dp_percentage > 0)
                    {
                        this.DP_Amount = (this._dp_percentage / 100) * this.Bill;
                    }
                }
            }
        }

        [Appearance("SalesOrderDP_AmountClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public double DP_Amount
        {
            get { return _dp_Amount; }
            set { SetPropertyValue("DP_Amount", ref _dp_Amount, value); }
        }

        [Appearance("SalesPrePaymentInvoiceOutstandingClose", Enabled = false)]
        public double Outstanding
        {
            get { return _outstanding; }
            set { SetPropertyValue("Outstanding", ref _outstanding, value); }
        }

        [Appearance("SalesPrePaymentInvoicePlanClose", Enabled = false)]
        public double Plan
        {
            get { return _plan; }
            set { SetPropertyValue("Plan", ref _plan, value); }
        }

        [Appearance("SalesPrePaymentInvoiceDescriptionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        [Size(512)]
        public string Description
        {
            get { return _description; }
            set { SetPropertyValue("Description", ref _description, value); }
        }

        [Appearance("SalesPrePaymentInvoicePostedCountClose", Enabled = false)]
        public int PostedCount
        {
            get { return _postedCount; }
            set { SetPropertyValue("PostedCount", ref _postedCount, value); }
        }

        [Appearance("SalesPrePaymentInvoiceStatusClose", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("SalesPrePaymentInvoiceStatusDateClose", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("SalesPrePaymentInvoiceEstimatedDateClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public DateTime EstimatedDate
        {
            get { return _estimatedDate; }
            set { SetPropertyValue("EstimatedDate", ref _estimatedDate, value); }
        }

        [Appearance("SalesPrePaymentInvoiceSignCodeClose", Enabled = false)]
        public string SignCode
        {
            get { return _signCode; }
            set { SetPropertyValue("SignCode", ref _signCode, value); }
        }

        [Appearance("SalesPrePaymentInvoiceProjectHeaderClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public ProjectHeader ProjectHeader
        {
            get { return _projectHeader; }
            set { SetPropertyValue("ProjectHeader", ref _projectHeader, value); }
        }

        [Browsable(false)]
        public XPCollection<SalesOrder> AvailableSalesOrder
        {
            get
            {
                if (!IsLoading)
                {
                    PriceGroup _locPriceGroup = null;

                    if (this.SalesToCostumer != null && this.PriceGroup != null)
                    {
                        
                        XPCollection<SalesOrder> _locCollectionSOs = new XPCollection<SalesOrder>
                                                                        (Session, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ActiveApproved3", true),
                                                                        new BinaryOperator("SalesToCostumer", this.SalesToCostumer),
                                                                        new BinaryOperator("PriceGroup", _locPriceGroup),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Progress),
                                                                        new BinaryOperator("Status", Status.Posted)))
                                                                        );

                        if (_locCollectionSOs != null && _locCollectionSOs.Count() > 0)
                        {
                            _availableSalesOrder = _locCollectionSOs;
                        }
                    }
                }
                return _availableSalesOrder;
            }
        }

        [DataSourceProperty("AvailableSalesOrder", DataSourcePropertyIsNullMode.SelectNothing)]
        [Appearance("SalesPrePaymentInvoiceSalesOrderClose", Criteria = "ActivationPosting = true", Enabled = false)]        
        public SalesOrder SalesOrder
        {
            get { return _salesOrder; }
            set { SetPropertyValue("SalesOrder", ref _salesOrder, value); }
        }

        [Appearance("SalesPrePaymentInvoiceReceivableTransactionClose", Criteria = "ActivationPosting = true", Enabled = false)]
        public ReceivableTransaction ReceivableTransaction
        {
            get { return _receivableTransaction; }
            set { SetPropertyValue("ReceivableTransaction", ref _receivableTransaction, value); }
        }

        [Browsable(false)]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        [Association("SalesPrePaymentInvoice-PaymentInPlans")]
        public XPCollection<PaymentInPlan> PaymentInPlans
        {
            get { return GetCollection<PaymentInPlan>("PaymentInPlans"); }
        }

        [Association("SalesPrePaymentInvoice-ApprovalLines")]
        public XPCollection<ApprovalLine> ApprovalLines
        {
            get { return GetCollection<ApprovalLine>("ApprovalLines"); }
        }

        #region CLM

        [Browsable(false)]
        [Appearance("SalesInvoiceActiveApproved1Close", Enabled = false)]
        public bool ActiveApproved1
        {
            get { return _activeApproved1; }
            set { SetPropertyValue("ActiveApproved1", ref _activeApproved1, value); }
        }

        [Browsable(false)]
        [Appearance("SalesInvoiceActiveApproved2Close", Enabled = false)]
        public bool ActiveApproved2
        {
            get { return _activeApproved2; }
            set { SetPropertyValue("ActiveApproved2", ref _activeApproved2, value); }
        }

        [Browsable(false)]
        [Appearance("SalesInvoiceActiveApproved3Close", Enabled = false)]
        public bool ActiveApproved3
        {
            get { return _activeApproved3; }
            set { SetPropertyValue("ActiveApproved3", ref _activeApproved3, value); }
        }

        #endregion CLM

        #endregion Field
    }
}