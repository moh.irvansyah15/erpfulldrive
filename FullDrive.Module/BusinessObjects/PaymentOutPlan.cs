﻿using System;
using System.Linq;
using System.Text;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using System.ComponentModel;
using DevExpress.ExpressApp.DC;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.ConditionalAppearance;

namespace FullDrive.Module.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Code")]
    [NavigationItem("Finance & Accounting")]
    [RuleCombinationOfPropertiesIsUnique("PaymentOutPlanRuleUnique", DefaultContexts.Save, "Code")]
    //[ImageName("BO_Contact")]
    //[DefaultProperty("DisplayMemberNameForLookupEditorsOfThisType")]
    //[DefaultListViewOptions(MasterDetailMode.ListViewOnly, false, NewItemRowPosition.None)]
    //[Persistent("DatabaseTableName")]
    // Specify more UI options using a declarative approach (https://documentation.devexpress.com/#eXpressAppFramework/CustomDocument112701).
    public class PaymentOutPlan : FullDriveSysBaseObject
    { // Inherit from a different class to provide a custom primary key, concurrency and deletion behavior, etc. (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument113146.aspx).
        private string _code;
        private string _name;
        private PaymentMethod _paymentMethod;
        private double _plan;
        private double _paid;
        private double _outstanding;
        private DateTime _estimatedDate;
        private DateTime _actualDate;
        private Status _status;
        private DateTime _statusDate;
        private PurchaseInvoice _purchaseInvoice;
        private PurchaseOrder _purchaseOrder;
        private PayableTransaction _payableTransaction;
        private PurchaseInvoiceCollection _purchaseInvoiceCollection;
        private PurchasePrePaymentInvoice _purchasePrePaymentInvoice;
        private Company _company;
        private GlobalFunction _globFunc;

        public PaymentOutPlan(Session session)
            : base(session)
        {
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (!IsLoading)
            {
                _globFunc = new GlobalFunction();
                this.Code = _globFunc.GetNumberingUnlockOptimisticRecord(this.Session.DataLayer, ObjectList.PaymentOutPlan);
                this.Status = Status.Open;
            }
            // Place your initialization code here (https://documentation.devexpress.com/eXpressAppFramework/CustomDocument112834.aspx).
        }

        [Appearance("PaymentOutPlanCodeEnabled", Enabled = false)]
        [RuleRequiredField(DefaultContexts.Save)]
        public string Code
        {
            get { return _code; }
            set { SetPropertyValue("Code", ref _code, value); }
        }

        [Appearance("PaymentOutPlanNameEnabled", Enabled = false)]
        public string Name
        {
            get { return _code; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        [Appearance("PaymentOutPlanPaymentMethodEnabled", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public PaymentMethod PaymentMethod
        {
            get { return _paymentMethod; }
            set { SetPropertyValue("PaymentMethod", ref _paymentMethod, value); }
        }

        [Appearance("PaymentOutPlanPlanEnabled", Enabled = false)]
        public double Plan
        {
            get { return _plan; }
            set { SetPropertyValue("Plan", ref _plan, value); }
        }

        [Appearance("PaymentOutPlanPaidEnabled", Enabled = false)]
        public double Paid
        {
            get { return _paid; }
            set { SetPropertyValue("Paid", ref _paid, value); }
        }

        [Appearance("PaymentOutPlanOutstandingEnabled", Enabled = false)]
        public double Outstanding
        {
            get { return _outstanding; }
            set { SetPropertyValue("Outstanding", ref _outstanding, value); }
        }

        [Appearance("PaymentOutPlanEstimatedDateEnabled", Enabled = false)]
        public DateTime EstimatedDate
        {
            get { return _estimatedDate; }
            set { SetPropertyValue("EstimatedDate", ref _estimatedDate, value); }
        }

        [Appearance("PaymentOutPlanActualDateEnabled", Enabled = false)]
        public DateTime ActualDate
        {
            get { return _actualDate; }
            set { SetPropertyValue("ActualDate", ref _actualDate, value); }
        }

        [Appearance("PaymentOutPlanStatusEnabled", Enabled = false)]
        public Status Status
        {
            get { return _status; }
            set { SetPropertyValue("Status", ref _status, value); }
        }

        [Appearance("PaymentOutPlanStatusDateEnabled", Enabled = false)]
        public DateTime StatusDate
        {
            get { return _statusDate; }
            set { SetPropertyValue("StatusDate", ref _statusDate, value); }
        }

        [Appearance("PaymentOutPlanPurchaseInvoiceEnabled", Enabled = false)]
        [Association("PurchaseInvoice-PaymentOutPlans")]
        public PurchaseInvoice PurchaseInvoice
        {
            get { return _purchaseInvoice; }
            set { SetPropertyValue("PurchaseInvoice", ref _purchaseInvoice, value); }
        }

        //Wahab 27-02-2020
        //[Association("PurchaseOrder-PaymentOutPlans")]
        [Appearance("PaymentOutPlanPurchaseOrderEnabled", Enabled = false)]       
        public PurchaseOrder PurchaseOrder
        {
            get { return _purchaseOrder; }
            set { SetPropertyValue("PurchaseOrder", ref _purchaseOrder, value); }
        }

        [Appearance("PaymentOutPlanPayableTransactionEnabled", Enabled = false)]
        [Association("PayableTransaction-PaymentOutPlans")]
        public PayableTransaction PayableTransaction
        {
            get { return _payableTransaction; }
            set { SetPropertyValue("PayableTransaction", ref _payableTransaction, value); }
        }

        [Appearance("PaymentOutPlanPurchaseInvoiceCollectionEnabled", Enabled = false)]
        [Association("PurchaseInvoiceCollection-PaymentOutPlans")]
        public PurchaseInvoiceCollection PurchaseInvoiceCollection
        {
            get { return _purchaseInvoiceCollection; }
            set { SetPropertyValue("PurchaseInvoiceCollection", ref _purchaseInvoiceCollection, value); }
        }

        [Appearance("PaymentOutPlanPurchasePrePaymentInvoiceEnabled", Enabled = false)]
        [Association("PurchasePrePaymentInvoice-PaymentOutPlans")]
        public PurchasePrePaymentInvoice PurchasePrePaymentInvoice
        {
            get { return _purchasePrePaymentInvoice; }
            set { SetPropertyValue("PurchasePrePaymentInvoice", ref _purchasePrePaymentInvoice, value); }
        }

        [Appearance("PaymentOutPlanCompanyEnabled", Enabled = false)]
        [DataSourceCriteria("Active = true")]
        public Company Company
        {
            get { return _company; }
            set { SetPropertyValue("Company", ref _company, value); }
        }

        
    }
}