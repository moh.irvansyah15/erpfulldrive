﻿namespace FullDrive.Module.Controllers
{
    partial class MaterialRequisitionApprovalDetailController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.MaterialRequisitionApprovalDetail = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // MaterialRequisitionApprovalDetail
            // 
            this.MaterialRequisitionApprovalDetail.Caption = "Approval";
            this.MaterialRequisitionApprovalDetail.ConfirmationMessage = null;
            this.MaterialRequisitionApprovalDetail.Id = "MaterialRequisitionApprovalDetailId";
            this.MaterialRequisitionApprovalDetail.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.MaterialRequisitionApprovalDetail.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.MaterialRequisition);
            this.MaterialRequisitionApprovalDetail.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.MaterialRequisitionApprovalDetail.ToolTip = null;
            this.MaterialRequisitionApprovalDetail.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.MaterialRequisitionApprovalDetail.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.MaterialRequisitionApprovalDetail_Execute);
            // 
            // MaterialRequisitionApprovalDetailController
            // 
            this.Actions.Add(this.MaterialRequisitionApprovalDetail);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction MaterialRequisitionApprovalDetail;
    }
}
