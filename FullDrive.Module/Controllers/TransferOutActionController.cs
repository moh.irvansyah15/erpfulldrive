﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class TransferOutActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;

        public TransferOutActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            TransferOutListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                TransferOutListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void TransferOutProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Status _locStatus = Status.None;
                DateTime _locNow;
                Status _locStatus2 = Status.None;
                DateTime _locNow2;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        TransferOut _locTransferOutOS = (TransferOut)_objectSpace.GetObject(obj);

                        if (_locTransferOutOS != null)
                        {
                            if (_locTransferOutOS.Code != null)
                            {
                                _currObjectId = _locTransferOutOS.Code;

                                TransferOut _locTransferOutXPO = _currSession.FindObject<TransferOut>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locTransferOutXPO != null)
                                {
                                    if (_locTransferOutXPO.Status == Status.Open || _locTransferOutXPO.Status == Status.Progress)
                                    {
                                        if(_locTransferOutXPO.Status == Status.Open)
                                        {
                                            _locStatus = Status.Progress;
                                            _locNow = now;
                                        }
                                        else
                                        {
                                            _locStatus = _locTransferOutXPO.Status;
                                            _locNow = _locTransferOutXPO.StatusDate;
                                        }

                                        _locTransferOutXPO.Status = _locStatus;
                                        _locTransferOutXPO.StatusDate = _locNow;
                                        _locTransferOutXPO.Save();
                                        _locTransferOutXPO.Session.CommitTransaction();

                                        XPCollection<TransferOutLine> _locTransferOutLines = new XPCollection<TransferOutLine>
                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("TransferOut", _locTransferOutXPO)));

                                        if (_locTransferOutLines != null && _locTransferOutLines.Count > 0)
                                        {
                                            foreach (TransferOutLine _locTransferOutLine in _locTransferOutLines)
                                            {
                                                if (_locTransferOutLine.Status == Status.Open || _locTransferOutLine.Status == Status.Progress)
                                                {

                                                    if (_locTransferOutLine.Status == Status.Open)
                                                    {
                                                        _locStatus2 = Status.Progress;
                                                        _locNow2 = now;
                                                    }
                                                    else
                                                    {
                                                        _locStatus2 = _locTransferOutLine.Status;
                                                        _locNow2 = _locTransferOutLine.StatusDate;
                                                    }

                                                    _locTransferOutLine.Status = _locStatus2;
                                                    _locTransferOutLine.StatusDate = _locNow2;
                                                    _locTransferOutLine.Save();
                                                    _locTransferOutLine.Session.CommitTransaction();

                                                    XPCollection<TransferOutLot> _locTransferOutLots = new XPCollection<TransferOutLot>
                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("TransferOutLine", _locTransferOutLine),
                                                                                                        new BinaryOperator("Status", Status.Open)));

                                                    if (_locTransferOutLots != null && _locTransferOutLots.Count() > 0)
                                                    {
                                                        foreach (TransferOutLot _locTransferOutLot in _locTransferOutLots)
                                                        {
                                                            _locTransferOutLot.Status = Status.Progress;
                                                            _locTransferOutLot.StatusDate = now;
                                                            _locTransferOutLot.Save();
                                                            _locTransferOutLot.Session.CommitTransaction();
                                                        }
                                                    }
                                                } 
                                            }   
                                        }
                                    }
                                    SuccessMessageShow("Transfer Out has been successfully updated to progress");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Transfer Out Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Transfer Out Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOut " + ex.ToString());
            }
        }

        private void TransferOutPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        TransferOut _locTransferOutOS = (TransferOut)_objectSpace.GetObject(obj);

                        if (_locTransferOutOS != null)
                        {
                            if (_locTransferOutOS.Code != null)
                            {
                                _currObjectId = _locTransferOutOS.Code;

                                TransferOut _locTransferOutXPO = _currSession.FindObject<TransferOut>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locTransferOutXPO != null)
                                {
                                    if (_locTransferOutXPO.Status == Status.Progress || _locTransferOutXPO.Status == Status.Posted)
                                    {
                                        if (CheckAvailableStock(_currSession, _locTransferOutXPO) == true)
                                        {
                                            SetDeliverBeginingInventory(_currSession, _locTransferOutXPO);
                                            SetDeliverInventoryJournal(_currSession, _locTransferOutXPO);
                                            SetRemainDeliverQty(_currSession, _locTransferOutXPO);
                                            SetPostingDeliverQty(_currSession, _locTransferOutXPO);
                                            SetProcessCountDeliver(_currSession, _locTransferOutXPO);
                                            if (_locTransferOutXPO.SameWarehouse == false)
                                            {
                                                SetTransferOrder(_currSession, _locTransferOutXPO);
                                            }
                                            else
                                            {
                                                SetTransferIn(_currSession, _locTransferOutXPO);
                                            }
                                            SetStatusDeliverTransferOutLine(_currSession, _locTransferOutXPO);
                                            SetNormalDeliverQuantity(_currSession, _locTransferOutXPO);
                                            SetFinalStatusDeliverTransferOut(_currSession, _locTransferOutXPO);
                                            SuccessMessageShow(_locTransferOutXPO.Code + " has been change successfully to Deliver");
                                        }
                                        else
                                        {
                                            ErrorMessageShow("Please check stock quantity");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Transfer Out Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Transfer Out Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOut " + ex.ToString());
            }
        }

        private void TransferOutListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(TransferOut)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOut " + ex.ToString());
            }
        }

        #region TransferOut

        //Cek ketersediaan stock
        private bool CheckAvailableStock(Session _currSession, TransferOut _transferOutXPO)
        {
            bool _result = true;
            try
            {
                XPCollection<TransferOutLine> _locTransOutLines = new XPCollection<TransferOutLine>(_currSession,
                                                                        new BinaryOperator("TransferOut", _transferOutXPO));

                if (_locTransOutLines != null && _locTransOutLines.Count > 0)
                {
                    double _locTransOutLineTotal = 0;
                    BeginingInventory _locBegInventory = null;
                    XPCollection<BeginingInventoryLine> _locBegInvLines = null;
                    foreach (TransferOutLine _locTransOutLine in _locTransOutLines)
                    {
                        XPCollection<TransferOutLot> _locTransferOutLots = new XPCollection<TransferOutLot>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("TransferOutLine", _locTransOutLine)));

                        #region LotNumber
                        if (_locTransferOutLots != null && _locTransferOutLots.Count() > 0)
                        {
                            foreach (TransferOutLot _locTransferOutLot in _locTransferOutLots)
                            {
                                _locBegInventory = _currSession.FindObject<BeginingInventory>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Item", _locTransferOutLot.Item)));
                                if (_locBegInventory != null)
                                {
                                    if (_locTransferOutLot.BegInvLine != null)
                                    {
                                        if (_locTransferOutLot.BegInvLine.Code != null)
                                        {
                                            BeginingInventoryLine _locBegInvLine = _currSession.FindObject<BeginingInventoryLine>(
                                                                                        new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Code", _locTransferOutLot.BegInvLine.Code)));
                                            if (_locBegInvLine != null)
                                            {
                                                if (_locTransferOutLot.TQty > _locBegInvLine.QtyAvailable)
                                                {
                                                    _result = false;
                                                }
                                            }
                                            else
                                            {
                                                _result = false;
                                            }
                                        }
                                    }

                                }
                            }
                        }
                        #endregion LotNumber
                        #region NonLotNumber
                        else
                        {
                            if (_locTransOutLine.LocationFrom != null || _locTransOutLine.BinLocationFrom != null)
                            {
                                _locBegInventory = _currSession.FindObject<BeginingInventory>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Item", _locTransOutLine.Item)));

                                if (_locBegInventory != null)
                                {
                                    if (_locTransOutLine.LocationFrom != null && _locTransOutLine.BinLocationFrom != null)
                                    {
                                        _locBegInvLines = new XPCollection<BeginingInventoryLine>(_currSession,
                                                      new GroupOperator(GroupOperatorType.And,
                                                      new BinaryOperator("BeginingInventory", _locBegInventory),
                                                      new BinaryOperator("Item", _locTransOutLine.Item),
                                                      new BinaryOperator("Location", _locTransOutLine.LocationFrom),
                                                      new BinaryOperator("BinLocation", _locTransOutLine.BinLocationFrom),
                                                      new BinaryOperator("StockType", _locTransOutLine.StockTypeFrom)));
                                    }
                                    else if (_locTransOutLine.LocationFrom != null && _locTransOutLine.BinLocationFrom == null)
                                    {
                                        _locBegInvLines = new XPCollection<BeginingInventoryLine>(_currSession,
                                                      new GroupOperator(GroupOperatorType.And,
                                                      new BinaryOperator("BeginingInventory", _locBegInventory),
                                                      new BinaryOperator("Item", _locTransOutLine.Item),
                                                      new BinaryOperator("Location", _locTransOutLine.LocationFrom),
                                                      new BinaryOperator("StockType", _locTransOutLine.StockTypeFrom)));
                                    }


                                    if (_locBegInvLines != null && _locBegInvLines.Count() > 0)
                                    {
                                        foreach (BeginingInventoryLine _locBegInvLine in _locBegInvLines)
                                        {
                                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                             new BinaryOperator("Item", _locTransOutLine.Item),
                                                             new BinaryOperator("UOM", _locTransOutLine.UOM),
                                                             new BinaryOperator("DefaultUOM", _locTransOutLine.DUOM),
                                                             new BinaryOperator("Active", true)));

                                            if (_locItemUOM != null)
                                            {
                                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                {
                                                    _locTransOutLineTotal = _locTransOutLine.Qty * _locItemUOM.DefaultConversion + _locTransOutLine.DQty;
                                                }
                                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                {
                                                    _locTransOutLineTotal = _locTransOutLine.Qty / _locItemUOM.Conversion + _locTransOutLine.DQty;
                                                }
                                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                {
                                                    _locTransOutLineTotal = _locTransOutLine.Qty + _locTransOutLine.DQty;
                                                }

                                                if (_locTransOutLineTotal > _locBegInventory.QtyAvailable)
                                                {
                                                    _result = false;
                                                }
                                            }
                                            else
                                            {
                                                _locTransOutLineTotal = _locTransOutLine.Qty + _locTransOutLine.DQty;

                                                if (_locTransOutLineTotal > _locBegInventory.QtyAvailable)
                                                {
                                                    _result = false;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        #endregion NonLotNumber
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = TransferOut ", ex.ToString());
            }
            return _result;
        }

        //Mengurangkan Qty di Begining Inventory
        private void SetDeliverBeginingInventory(Session _currSession, TransferOut _transferOutXPO)
        {
            try
            {
                XPCollection<TransferOutLine> _locTransOutLines = new XPCollection<TransferOutLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("TransferOut", _transferOutXPO),
                                                                        new BinaryOperator("Select", true)));

                if (_locTransOutLines != null && _locTransOutLines.Count() > 0)
                {
                    GlobalFunction _globFunc = new GlobalFunction();
                    double _locInvLineTotal = 0;
                    XPCollection<BeginingInventoryLine> _locBegInventoryLines = null;
                    ItemUnitOfMeasure _locItemUOM = null;
                    string _fullString = null;
                    string _locItemParse = null;
                    string _locLocationParse = null;
                    string _locBinLocationParse = null;
                    string _locDUOMParse = null;
                    string _locStockTypeParse = null;
                    string _locProjectHeader = null;
                    string _locActiveParse = null;
                    string _locSignCode = null;

                    foreach (TransferOutLine _locTransOutLine in _locTransOutLines)
                    {
                        if (_locTransOutLine.Status == Status.Progress || _locTransOutLine.Status == Status.Posted)
                        {
                            if (_locTransOutLine.DQty > 0 || _locTransOutLine.Qty > 0)
                            {
                                XPCollection<TransferOutLot> _locTransferOutLots = new XPCollection<TransferOutLot>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("TransferOutLine", _locTransOutLine)));
                                #region NonLotNumber
                                if (_locTransferOutLots == null && _locTransferOutLots.Count() <= 0)
                                {
                                    if (_locTransOutLine.Item != null && _locTransOutLine.UOM != null && _locTransOutLine.DUOM != null)
                                    {
                                        //for conversion
                                        _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                             new BinaryOperator("Item", _locTransOutLine.Item),
                                                             new BinaryOperator("UOM", _locTransOutLine.UOM),
                                                             new BinaryOperator("DefaultUOM", _locTransOutLine.DUOM),
                                                             new BinaryOperator("Active", true)));
                                    }

                                    //for query
                                    BeginingInventory _locBeginingInventory = _currSession.FindObject<BeginingInventory>(new GroupOperator(GroupOperatorType.And,
                                                                              new BinaryOperator("Item", _locTransOutLine.Item)));

                                    #region UpdateBeginingInventory
                                    if (_locBeginingInventory != null)
                                    {
                                        #region code
                                        if (_locTransOutLine.Item != null) { _locItemParse = "[Item.Code]=='" + _locTransOutLine.Item.Code + "'"; }
                                        else { _locItemParse = ""; }

                                        if (_locTransOutLine.LocationFrom != null && (_locTransOutLine.Item != null))
                                        { _locLocationParse = " AND [Location.Code]=='" + _locTransOutLine.LocationFrom.Code + "'"; }
                                        else if (_locTransOutLine.LocationFrom != null && _locTransOutLine.Item == null)
                                        { _locLocationParse = " [Location.Code]=='" + _locTransOutLine.LocationFrom.Code + "'"; }
                                        else { _locLocationParse = ""; }

                                        if (_locTransOutLine.BinLocationFrom != null && (_locTransOutLine.Item != null || _locTransOutLine.LocationFrom != null))
                                        { _locBinLocationParse = " AND [BinLocation.Code]=='" + _locTransOutLine.BinLocationFrom.Code + "'"; }
                                        else if (_locTransOutLine.BinLocationFrom != null && _locTransOutLine.Item == null && _locTransOutLine.LocationFrom == null)
                                        { _locBinLocationParse = " [BinLocation.Code]=='" + _locTransOutLine.BinLocationFrom.Code + "'"; }
                                        else { _locBinLocationParse = ""; }

                                        if (_locTransOutLine.DUOM != null && (_locTransOutLine.Item != null || _locTransOutLine.LocationFrom != null || _locTransOutLine.BinLocationFrom != null))
                                        { _locDUOMParse = " AND [DefaultUOM.Code]=='" + _locTransOutLine.DUOM.Code + "'"; }
                                        else if (_locTransOutLine.DUOM != null && _locTransOutLine.Item == null && _locTransOutLine.LocationFrom == null && _locTransOutLine.BinLocationFrom == null)
                                        { _locDUOMParse = " [DefaultUOM.Code]=='" + _locTransOutLine.DUOM.Code + "'"; }
                                        else { _locDUOMParse = ""; }

                                        if (_locTransOutLine.StockTypeFrom != StockType.None && (_locTransOutLine.Item != null || _locTransOutLine.LocationFrom != null || _locTransOutLine.BinLocationFrom != null
                                            || _locTransOutLine.DUOM != null))
                                        { _locStockTypeParse = " AND [StockType]=='" + GetStockType(_locTransOutLine.StockTypeFrom).ToString() + "'"; }
                                        else if (_locTransOutLine.StockTypeFrom != StockType.None && _locTransOutLine.Item == null && _locTransOutLine.LocationFrom == null && _locTransOutLine.BinLocationFrom == null
                                            && _locTransOutLine.DUOM == null)
                                        { _locStockTypeParse = " [StockType]=='" + GetStockType(_locTransOutLine.StockTypeFrom).ToString() + "'"; }
                                        else { _locStockTypeParse = ""; }

                                        if (_locTransOutLine.Item == null && _locTransOutLine.LocationFrom == null && _locTransOutLine.BinLocationFrom == null
                                            && _locTransOutLine.DUOM == null && _locTransOutLine.StockTypeFrom != StockType.None)
                                        { _locActiveParse = " [Active]=='" + GetActive(true).ToString() + "'"; }
                                        else { _locActiveParse = " AND [Active]=='" + GetActive(true).ToString() + "'"; }

                                        if (_transferOutXPO.ProjectHeader != null)
                                        { _locProjectHeader = " AND [ProjectHeader.Code]=='" + _transferOutXPO.ProjectHeader.Code + "'"; }
                                        else
                                        { _locProjectHeader = ""; }

                                        if (_locItemParse != null || _locLocationParse != null || _locBinLocationParse != null || _locDUOMParse != null || _locStockTypeParse != null || _locProjectHeader != null)
                                        {
                                            _fullString = _locItemParse + _locLocationParse + _locBinLocationParse + _locDUOMParse + _locStockTypeParse + _locActiveParse + _locProjectHeader;
                                        }
                                        else
                                        {
                                            _fullString = _locActiveParse;
                                        }
                                        #endregion code

                                        _locBegInventoryLines = new XPCollection<BeginingInventoryLine>(_currSession, CriteriaOperator.Parse(_fullString));

                                        #region UpdateBeginingInventoryLine
                                        if (_locBegInventoryLines != null && _locBegInventoryLines.Count() > 0)
                                        {

                                            foreach (BeginingInventoryLine _locBegInventoryLine in _locBegInventoryLines)
                                            {
                                                if (_locItemUOM != null)
                                                {
                                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                    {
                                                        _locInvLineTotal = _locTransOutLine.Qty * _locItemUOM.DefaultConversion + _locTransOutLine.DQty;
                                                    }
                                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                    {
                                                        _locInvLineTotal = _locTransOutLine.Qty / _locItemUOM.Conversion + _locTransOutLine.DQty;
                                                    }
                                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                    {
                                                        _locInvLineTotal = _locTransOutLine.Qty + _locTransOutLine.DQty;
                                                    }
                                                }

                                                else
                                                {
                                                    _locInvLineTotal = _locTransOutLine.Qty + _locTransOutLine.DQty;
                                                }

                                                _locBegInventoryLine.QtyAvailable = _locBegInventoryLine.QtyAvailable - _locInvLineTotal;
                                                _locBegInventoryLine.Save();
                                                _locBegInventoryLine.Session.CommitTransaction();

                                                _locBeginingInventory.QtyAvailable = _locBeginingInventory.QtyAvailable - _locInvLineTotal;
                                                _locBeginingInventory.Save();
                                                _locBeginingInventory.Session.CommitTransaction();
                                            }
                                        }
                                        #endregion UpdateBeginingInventoryLine

                                        #region NewBeginingInventoryLine
                                        else
                                        {
                                            if (_locItemUOM != null)
                                            {
                                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                {
                                                    _locInvLineTotal = _locTransOutLine.Qty * _locItemUOM.DefaultConversion + _locTransOutLine.DQty;
                                                }
                                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                {
                                                    _locInvLineTotal = _locTransOutLine.Qty / _locItemUOM.Conversion + _locTransOutLine.DQty;
                                                }
                                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                {
                                                    _locInvLineTotal = _locTransOutLine.Qty + _locTransOutLine.DQty;
                                                }
                                            }
                                            else
                                            {
                                                _locInvLineTotal = _locTransOutLine.Qty + _locTransOutLine.DQty;
                                            }

                                            BeginingInventoryLine _locSaveDataBeginingInventory = new BeginingInventoryLine(_currSession)
                                            {
                                                Item = _locTransOutLine.Item,
                                                Location = _locTransOutLine.LocationFrom,
                                                BinLocation = _locTransOutLine.BinLocationFrom,
                                                QtyAvailable = _locInvLineTotal,
                                                Company = _transferOutXPO.Company,
                                                DefaultUOM = _locTransOutLine.DUOM,
                                                StockType = _locTransOutLine.StockTypeFrom,
                                                Active = true,
                                                BeginingInventory = _locBeginingInventory,
                                            };
                                            _locSaveDataBeginingInventory.Save();
                                            _locSaveDataBeginingInventory.Session.CommitTransaction();
                                        }
                                        #endregion NewBeginingInventoryLine
                                    }
                                    #endregion UpdateBeginingInventory

                                    #region CreateNewBeginingInventory
                                    else
                                    {
                                        _locSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.BeginingInventory);

                                        if (_locSignCode != null)
                                        {
                                            if (_locItemUOM != null)
                                            {
                                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                {
                                                    _locInvLineTotal = _locTransOutLine.Qty * _locItemUOM.DefaultConversion + _locTransOutLine.DQty;
                                                }
                                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                {
                                                    _locInvLineTotal = _locTransOutLine.Qty / _locItemUOM.Conversion + _locTransOutLine.DQty;
                                                }
                                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                {
                                                    _locInvLineTotal = _locTransOutLine.Qty + _locTransOutLine.DQty;
                                                }
                                            }

                                            else
                                            {
                                                _locInvLineTotal = _locTransOutLine.Qty + _locTransOutLine.DQty;
                                            }

                                            BeginingInventory _saveDataBegInv = new BeginingInventory(_currSession)
                                            {
                                                Item = _locTransOutLine.Item,
                                                QtyAvailable = _locInvLineTotal,
                                                DefaultUOM = _locTransOutLine.DUOM,
                                                Active = true,
                                                SignCode = _locSignCode,
                                                Company = _transferOutXPO.Company,
                                            };
                                            _saveDataBegInv.Save();
                                            _saveDataBegInv.Session.CommitTransaction();

                                            BeginingInventory _locBeginingInventory2 = _currSession.FindObject<BeginingInventory>(new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Item", _locTransOutLine.Item),
                                                                        new BinaryOperator("SignCode", _locSignCode)));

                                            if (_locBeginingInventory2 != null)
                                            {
                                                BeginingInventoryLine _saveDataBegInvLine = new BeginingInventoryLine(_currSession)
                                                {
                                                    Item = _locTransOutLine.Item,
                                                    Location = _locTransOutLine.LocationFrom,
                                                    BinLocation = _locTransOutLine.BinLocationFrom,
                                                    QtyAvailable = _locInvLineTotal,
                                                    DefaultUOM = _locBeginingInventory2.DefaultUOM,
                                                    StockType = _locTransOutLine.StockTypeFrom,
                                                    ProjectHeader = _transferOutXPO.ProjectHeader,
                                                    Active = true,
                                                    BeginingInventory = _locBeginingInventory2,
                                                    Company = _transferOutXPO.Company,
                                                };
                                                _saveDataBegInvLine.Save();
                                                _saveDataBegInvLine.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                    #endregion CreateNewBeginingInventory
                                }
                                #endregion NonLotNumber

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = TransferOut ", ex.ToString());
            }
        }

        //Membuat jurnal negatif di inventory journal
        private void SetDeliverInventoryJournal(Session _currSession, TransferOut _transferOutXPO)
        {
            try
            {
                if (_transferOutXPO != null)
                {
                    XPCollection<TransferOutLine> _locTransOutLines = new XPCollection<TransferOutLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("TransferOut", _transferOutXPO),
                                                                        new BinaryOperator("Select", true)));

                    if (_locTransOutLines != null && _locTransOutLines.Count > 0)
                    {
                        double _locInvLineTotal = 0;
                        DateTime now = DateTime.Now;

                        foreach (TransferOutLine _locTransOutLine in _locTransOutLines)
                        {
                            if (_locTransOutLine.Status == Status.Progress || _locTransOutLine.Status == Status.Posted)
                            {
                                if (_locTransOutLine.DQty > 0 || _locTransOutLine.Qty > 0)
                                {
                                    XPCollection<TransferOutLot> _locTransferOutLots = new XPCollection<TransferOutLot>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("TransferOutLine", _locTransOutLine)));

                                    #region LotNumber
                                    if (_locTransferOutLots != null && _locTransferOutLots.Count() > 0)
                                    {
                                        foreach (TransferOutLot _locTransferOutLot in _locTransferOutLots)
                                        {
                                            if (_locTransferOutLot.Select == true)
                                            {
                                                if (_locTransferOutLot.BegInvLine != null)
                                                {
                                                    if (_locTransferOutLot.BegInvLine.Code != null)
                                                    {
                                                        BeginingInventoryLine _locBegInvLine = _currSession.FindObject<BeginingInventoryLine>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("Code", _locTransferOutLot.BegInvLine.Code)));
                                                        if (_locBegInvLine != null)
                                                        {
                                                            InventoryJournal _locNegatifInventoryJournal = new InventoryJournal(_currSession)
                                                            {
                                                                DocumentType = _transferOutXPO.DocumentType,
                                                                DocNo = _transferOutXPO.DocNo,
                                                                Location = _locTransferOutLot.LocationFrom,
                                                                BinLocation = _locTransferOutLot.BinLocationFrom,
                                                                StockType = _locTransferOutLot.StockTypeFrom,
                                                                Item = _locTransferOutLot.Item,
                                                                Company = _locTransferOutLot.Company,
                                                                QtyPos = 0,
                                                                QtyNeg = _locTransferOutLot.TQty,
                                                                DUOM = _locTransferOutLot.DUOM,
                                                                JournalDate = now,
                                                                LotNumber = _locTransferOutLot.LotNumber,
                                                                ProjectHeader = _transferOutXPO.ProjectHeader,
                                                                TransferOut = _transferOutXPO,
                                                            };
                                                            _locNegatifInventoryJournal.Save();
                                                            _locNegatifInventoryJournal.Session.CommitTransaction();
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion LotNumber
                                    #region NonLotNumber
                                    else
                                    {
                                        if (_locTransOutLine.Item != null && _locTransOutLine.UOM != null && _locTransOutLine.DUOM != null)
                                        {
                                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                 new BinaryOperator("Item", _locTransOutLine.Item),
                                                                 new BinaryOperator("UOM", _locTransOutLine.UOM),
                                                                 new BinaryOperator("DefaultUOM", _locTransOutLine.DUOM),
                                                                 new BinaryOperator("Active", true)));
                                            if (_locItemUOM != null)
                                            {
                                                #region Code
                                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                {
                                                    _locInvLineTotal = _locTransOutLine.Qty * _locItemUOM.DefaultConversion + _locTransOutLine.DQty;
                                                }
                                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                {
                                                    _locInvLineTotal = _locTransOutLine.Qty / _locItemUOM.Conversion + _locTransOutLine.DQty;
                                                }
                                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                {
                                                    _locInvLineTotal = _locTransOutLine.Qty + _locTransOutLine.DQty;
                                                }
                                                #endregion Code

                                                InventoryJournal _locNegatifInventoryJournal = new InventoryJournal(_currSession)
                                                {
                                                    DocumentType = _transferOutXPO.DocumentType,
                                                    DocNo = _transferOutXPO.DocNo,
                                                    Location = _locTransOutLine.LocationFrom,
                                                    BinLocation = _locTransOutLine.BinLocationFrom,
                                                    StockType = _locTransOutLine.StockTypeFrom,
                                                    Item = _locTransOutLine.Item,
                                                    Company = _transferOutXPO.Company,
                                                    QtyPos = 0,
                                                    QtyNeg = _locInvLineTotal,
                                                    DUOM = _locTransOutLine.DUOM,
                                                    JournalDate = now,
                                                    ProjectHeader = _transferOutXPO.ProjectHeader,
                                                    TransferOut = _transferOutXPO,
                                                };
                                                _locNegatifInventoryJournal.Save();
                                                _locNegatifInventoryJournal.Session.CommitTransaction();

                                            }
                                        }
                                        else
                                        {
                                            _locInvLineTotal = _locTransOutLine.Qty + _locTransOutLine.DQty;

                                            InventoryJournal _locNegatifInventoryJournal = new InventoryJournal(_currSession)
                                            {
                                                DocumentType = _transferOutXPO.DocumentType,
                                                DocNo = _transferOutXPO.DocNo,
                                                Location = _locTransOutLine.LocationFrom,
                                                BinLocation = _locTransOutLine.BinLocationFrom,
                                                StockType = _locTransOutLine.StockTypeFrom,
                                                Item = _locTransOutLine.Item,
                                                Company = _transferOutXPO.Company,
                                                QtyPos = 0,
                                                QtyNeg = _locInvLineTotal,
                                                DUOM = _locTransOutLine.DUOM,
                                                JournalDate = now,
                                                ProjectHeader = _transferOutXPO.ProjectHeader,
                                                TransferOut = _transferOutXPO,
                                            };
                                            _locNegatifInventoryJournal.Save();
                                            _locNegatifInventoryJournal.Session.CommitTransaction();
                                        }
                                    }
                                    #endregion NonLotNumber

                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = TransferOut ", ex.ToString());
            }
        }

        //Menentukan sisa dari transaksi Deliver -> Done 10 Dec 2019
        private void SetRemainDeliverQty(Session _currSession, TransferOut _transferOutXPO)
        {
            try
            {
                if (_transferOutXPO != null)
                {
                    XPCollection<TransferOutLine> _locTransInLines = new XPCollection<TransferOutLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("TransferOut", _transferOutXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locTransInLines != null && _locTransInLines.Count > 0)
                    {
                        double _locRmDQty = 0;
                        double _locRmQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (TransferOutLine _locTransInLine in _locTransInLines)
                        {
                            #region ProcessCount=0
                            if (_locTransInLine.ProcessCount == 0)
                            {
                                #region MaxQuantity
                                if (_locTransInLine.MxDQty > 0 || _locTransInLine.MxQty > 0)
                                {
                                    if (_locTransInLine.DQty > 0 && _locTransInLine.DQty <= _locTransInLine.MxDQty)
                                    {
                                        _locRmDQty = _locTransInLine.MxDQty - _locTransInLine.DQty;
                                    }

                                    if (_locTransInLine.Qty > 0 && _locTransInLine.Qty <= _locTransInLine.MxQty)
                                    {
                                        _locRmQty = _locTransInLine.MxQty - _locTransInLine.Qty;
                                    }

                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locTransInLine.Item),
                                                                new BinaryOperator("UOM", _locTransInLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locTransInLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locTransInLine.DQty > 0)
                                    {
                                        _locRmDQty = 0;
                                    }

                                    if (_locTransInLine.Qty > 0)
                                    {
                                        _locRmQty = 0;
                                    }

                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locTransInLine.Item),
                                                                new BinaryOperator("UOM", _locTransInLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locTransInLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locTransInLine.ProcessCount > 0)
                            {
                                if (_locTransInLine.RmDQty > 0)
                                {
                                    _locRmDQty = _locTransInLine.RmDQty - _locTransInLine.DQty;
                                }

                                if (_locTransInLine.RmQty > 0)
                                {
                                    _locRmQty = _locTransInLine.RmQty - _locTransInLine.Qty;
                                }

                                if (_locTransInLine.MxDQty > 0 || _locTransInLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locTransInLine.Item),
                                                                new BinaryOperator("UOM", _locTransInLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locTransInLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locTransInLine.Item),
                                                                new BinaryOperator("UOM", _locTransInLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locTransInLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                }


                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locTransInLine.RmDQty = _locRmDQty;
                            _locTransInLine.RmQty = _locRmQty;
                            _locTransInLine.RmTQty = _locInvLineTotal;
                            _locTransInLine.Save();
                            _locTransInLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOut " + ex.ToString());
            }
        }

        //Menentukan jumlah quantity yang di posting -> Done 10 Dec 2019
        private void SetPostingDeliverQty(Session _currSession, TransferOut _transferOutXPO)
        {
            try
            {
                if (_transferOutXPO != null)
                {
                    XPCollection<TransferOutLine> _locTransOutLines = new XPCollection<TransferOutLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("TransferOut", _transferOutXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locTransOutLines != null && _locTransOutLines.Count > 0)
                    {
                        double _locPDQty = 0;
                        double _locPQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (TransferOutLine _locTransOutLine in _locTransOutLines)
                        {
                            #region ProcessCount=0
                            if (_locTransOutLine.ProcessCount == 0)
                            {
                                #region MaxQuantity
                                if (_locTransOutLine.MxDQty > 0 || _locTransOutLine.MxQty > 0)
                                {
                                    if (_locTransOutLine.DQty > 0 && _locTransOutLine.DQty <= _locTransOutLine.MxDQty)
                                    {
                                        _locPDQty = _locTransOutLine.DQty;
                                    }

                                    if (_locTransOutLine.Qty > 0 && _locTransOutLine.Qty <= _locTransOutLine.MxQty)
                                    {
                                        _locPQty = _locTransOutLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locTransOutLine.Item),
                                                                new BinaryOperator("UOM", _locTransOutLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locTransOutLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locTransOutLine.DQty > 0)
                                    {
                                        _locPDQty = _locTransOutLine.DQty;
                                    }

                                    if (_locTransOutLine.Qty > 0)
                                    {
                                        _locPQty = _locTransOutLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locTransOutLine.Item),
                                                                new BinaryOperator("UOM", _locTransOutLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locTransOutLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }

                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locTransOutLine.ProcessCount > 0)
                            {
                                if (_locTransOutLine.PDQty > 0)
                                {
                                    _locPDQty = _locTransOutLine.PDQty + _locTransOutLine.DQty;
                                }

                                if (_locTransOutLine.PQty > 0)
                                {
                                    _locPQty = _locTransOutLine.PQty + _locTransOutLine.Qty;
                                }

                                _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locTransOutLine.Item),
                                                            new BinaryOperator("UOM", _locTransOutLine.MxUOM),
                                                            new BinaryOperator("DefaultUOM", _locTransOutLine.MxDUOM),
                                                            new BinaryOperator("Active", true)));
                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locTransOutLine.PDQty = _locPDQty;
                            _locTransOutLine.PDUOM = _locTransOutLine.DUOM;
                            _locTransOutLine.PQty = _locPQty;
                            _locTransOutLine.PUOM = _locTransOutLine.UOM;
                            _locTransOutLine.PTQty = _locInvLineTotal;
                            _locTransOutLine.Save();
                            _locTransOutLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOut " + ex.ToString());
            }

        }

        //Menentukan Banyak process Deliver
        private void SetProcessCountDeliver(Session _currSession, TransferOut _transferOutXPO)
        {
            try
            {
                if (_transferOutXPO != null)
                {
                    XPCollection<TransferOutLine> _locTransOutLines = new XPCollection<TransferOutLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("TransferOut", _transferOutXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locTransOutLines != null && _locTransOutLines.Count > 0)
                    {

                        foreach (TransferOutLine _locTransOutLine in _locTransOutLines)
                        {
                            if (_locTransOutLine.Status == Status.Progress || _locTransOutLine.Status == Status.Posted)
                            {
                                _locTransOutLine.ProcessCount = _locTransOutLine.ProcessCount + 1;
                                _locTransOutLine.Save();
                                _locTransOutLine.Session.CommitTransaction();
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOut " + ex.ToString());
            }
        }

        //Membuat Transfer In
        private void SetTransferIn(Session _currSession, TransferOut _transferOutXPO)
        {
            try
            {

                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _locDocCode = null;
                string _locSignCodeTIL = null;

                ProjectHeader _locProjectHeader = null;

                if(_transferOutXPO != null)
                {
                    if (_transferOutXPO.ProjectHeader != null)
                    {
                        _locProjectHeader = _transferOutXPO.ProjectHeader;
                    }

                    DocumentType _locDocumentTypeXPO = _currSession.FindObject<DocumentType>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("TransferType", DirectionType.Internal),
                                                     new BinaryOperator("InventoryMovingType", InventoryMovingType.TransferIn),
                                                     new BinaryOperator("ObjectList", ObjectList.TransferIn),
                                                     new BinaryOperator("Active", true)));

                    if (_locDocumentTypeXPO != null)
                    {
                        _locDocCode = _globFunc.GetDocumentNumberingUnlockOptimisticRecord(_currSession.DataLayer, _locDocumentTypeXPO);

                        if (_locDocCode != null)
                        {
                            TransferIn _locTransferIn = _currSession.FindObject<TransferIn>(new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("TransferOut", _transferOutXPO),
                                                new BinaryOperator("DocumentType", _locDocumentTypeXPO),
                                                new BinaryOperator("DocNo", _locDocCode)));

                            if (_locTransferIn == null)
                            {
                                TransferIn _saveDataTI = new TransferIn(_currSession)
                                {
                                    TransferType = DirectionType.Internal,
                                    InventoryMovingType = InventoryMovingType.TransferIn,
                                    ObjectList = ObjectList.TransferIn,
                                    DocumentType = _locDocumentTypeXPO,
                                    DocNo = _locDocCode,
                                    TransferOut = _transferOutXPO,
                                    ProjectHeader = _locProjectHeader,
                                    TransferOrder = _transferOutXPO.TransferOrder,
                                };
                                _saveDataTI.Save();
                                _saveDataTI.Session.CommitTransaction();

                                TransferIn _locTransferIn2 = _currSession.FindObject<TransferIn>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("DocNo", _locDocCode),
                                                     new BinaryOperator("TransferOut", _transferOutXPO)));

                                if (_locTransferIn2 != null)
                                {
                                    XPCollection<TransferOutLine> _locTransOutLines = new XPCollection<TransferOutLine>(_currSession,
                                                                                        new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("TransferOut", _transferOutXPO),
                                                                                        new BinaryOperator("Select", true)));

                                    if (_locTransOutLines != null && _locTransOutLines.Count > 0)
                                    {
                                        //PerTransferOutLine
                                        foreach (TransferOutLine _locTransOutLine in _locTransOutLines)
                                        {
                                            if (_locTransOutLine.Status == Status.Progress || _locTransOutLine.Status == Status.Posted)
                                            {
                                                _locSignCodeTIL = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.TransferInLine);

                                                if (_locSignCodeTIL != null)
                                                {
                                                    
                                                    TransferInLine _saveDataTransferInLine = new TransferInLine(_currSession)
                                                    {
                                                        Select = true,
                                                        SignCode = _locSignCodeTIL,
                                                        LocationFrom = _locTransOutLine.LocationFrom,
                                                        BinLocationFrom = _locTransOutLine.BinLocationFrom,
                                                        StockTypeFrom = _locTransOutLine.StockTypeFrom,
                                                        LocationTo = _locTransOutLine.LocationTo,
                                                        BinLocationTo = _locTransOutLine.BinLocationTo,
                                                        StockTypeTo = _locTransOutLine.StockTypeTo,
                                                        Item = _locTransOutLine.Item,
                                                        MxDQty = _locTransOutLine.PDQty,
                                                        MxDUOM = _locTransOutLine.PDUOM,
                                                        MxQty = _locTransOutLine.PQty,
                                                        MxUOM = _locTransOutLine.PUOM,
                                                        MxTQty = _locTransOutLine.PTQty,
                                                        DQty = _locTransOutLine.PDQty,
                                                        DUOM = _locTransOutLine.PDUOM,
                                                        Qty = _locTransOutLine.PQty,
                                                        UOM = _locTransOutLine.PUOM,
                                                        TQty = _locTransOutLine.PTQty,
                                                        TransferIn = _locTransferIn2,

                                                    };
                                                    _saveDataTransferInLine.Save();
                                                    _saveDataTransferInLine.Session.CommitTransaction();

                                                    TransferInLine _locTransferInLine = _currSession.FindObject<TransferInLine>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("SignCode", _locSignCodeTIL)));
                                                    if(_locTransferInLine != null)
                                                    {
                                                        XPCollection<TransferOutLot> _locTransferOutLots = new XPCollection<TransferOutLot>(_currSession,
                                                                                                            new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("TransferOutLine", _locTransOutLine),
                                                                                                            new BinaryOperator("Select", true)));
                                                        if(_locTransferOutLots != null && _locTransferOutLots.Count() > 0)
                                                        {
                                                            foreach(TransferOutLot _locTransferOutLot in _locTransferOutLots)
                                                            {
                                                                TransferInLot _saveDataTransferInLot = new TransferInLot(_currSession)
                                                                {
                                                                    Select = true,
                                                                    LocationFrom = _locTransferOutLot.LocationFrom,
                                                                    BinLocationFrom = _locTransferOutLot.BinLocationFrom,
                                                                    StockTypeFrom = _locTransferOutLot.StockTypeFrom,
                                                                    LocationTo = _locTransferOutLot.LocationTo,
                                                                    BinLocationTo = _locTransferOutLot.BinLocationTo,
                                                                    StockTypeTo = _locTransferOutLot.StockTypeTo,
                                                                    Item = _locTransferOutLot.Item,
                                                                    BegInvLine = _locTransferOutLot.BegInvLine,
                                                                    LotNumber = _locTransferOutLot.LotNumber,
                                                                    DQty = _locTransferOutLot.DQty,
                                                                    DUOM = _locTransferOutLot.DUOM,
                                                                    Qty = _locTransferOutLot.Qty,
                                                                    UOM = _locTransferOutLot.UOM,
                                                                    TQty = _locTransferOutLot.TQty,
                                                                    TransferInLine = _locTransferInLine,

                                                                };
                                                                _saveDataTransferInLot.Save();
                                                                _saveDataTransferInLot.Session.CommitTransaction();
                                                            }
                                                        }
                                                    }
                                                }  
                                            }
                                        }
                                    }    
                                }
                            }
                        }
                    }
                }    
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = TransferOut ", ex.ToString());
            }
        }

        //Membuat Transfer Order
        private void SetTransferOrder(Session _currSession, TransferOut _transferOutXPO)
        {
            try
            {

                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _locDocCode = null;
                string _locSignCodeTOL = null;
                ProjectHeader _locProjectHeader = null;
                TransferOrder _locInternalTransferOrder = null;
                
                if(_transferOutXPO != null)
                {
                    if (_transferOutXPO.ProjectHeader != null)
                    {
                        _locProjectHeader = _transferOutXPO.ProjectHeader;
                    }

                    if (_transferOutXPO.TransferOrder != null)
                    {
                        _locInternalTransferOrder = _transferOutXPO.TransferOrder;
                    }

                    DocumentType _locDocumentTypeXPO = _currSession.FindObject<DocumentType>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("TransferType", DirectionType.Internal),
                                                     new BinaryOperator("InventoryMovingType", InventoryMovingType.TransferIn),
                                                     new BinaryOperator("ObjectList", ObjectList.TransferIn),
                                                     new BinaryOperator("Active", true)));

                    if (_locDocumentTypeXPO != null)
                    {
                        _locDocCode = _globFunc.GetDocumentNumberingUnlockOptimisticRecord(_currSession.DataLayer, _locDocumentTypeXPO);

                        if (_locDocCode != null)
                        {
                            TransferOrder _locTransferOrder = _currSession.FindObject<TransferOrder>(new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("DocumentType", _locDocumentTypeXPO),
                                                new BinaryOperator("DocNo", _locDocCode)));

                            if (_locTransferOrder == null)
                            {
                                TransferOrder _saveDataTO = new TransferOrder(_currSession)
                                {
                                    TransferType = DirectionType.Internal,
                                    InventoryMovingType = InventoryMovingType.TransferIn,
                                    ObjectList = ObjectList.TransferIn,
                                    DocumentType = _locDocumentTypeXPO,
                                    DocNo = _locDocCode,
                                    LocationFrom = _transferOutXPO.LocationFrom,
                                    LocationTo = _transferOutXPO.LocationTo,
                                    ProjectHeader = _locProjectHeader,
                                    InternalTransferOrder = _locInternalTransferOrder,
                                };
                                _saveDataTO.Save();
                                _saveDataTO.Session.CommitTransaction();

                                TransferOrder _locTransferOrder2 = _currSession.FindObject<TransferOrder>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("DocumentType", _locDocumentTypeXPO),
                                                     new BinaryOperator("DocNo", _locDocCode)));

                                if (_locTransferOrder2 != null)
                                {
                                    XPCollection<TransferOutLine> _locTransOutLines = new XPCollection<TransferOutLine>(_currSession,
                                                                                        new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("TransferOut", _transferOutXPO),
                                                                                        new BinaryOperator("Select", true)));

                                    if (_locTransOutLines != null && _locTransOutLines.Count > 0)
                                    {
                                        //PerTransferOutLine
                                        foreach (TransferOutLine _locTransOutLine in _locTransOutLines)
                                        {
                                            if (_locTransOutLine.Status == Status.Progress || _locTransOutLine.Status == Status.Posted)
                                            {
                                                _locSignCodeTOL = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.TransferOrderLine);

                                                if(_locSignCodeTOL != null)
                                                {
                                                    TransferOrderLine _saveDataTransferOrderLine = new TransferOrderLine(_currSession)
                                                    {

                                                        LocationFrom = _locTransOutLine.LocationFrom,
                                                        BinLocationFrom = _locTransOutLine.BinLocationFrom,
                                                        StockTypeFrom = _locTransOutLine.StockTypeFrom,
                                                        LocationTo = _locTransOutLine.LocationTo,
                                                        BinLocationTo = _locTransOutLine.BinLocationTo,
                                                        StockTypeTo = _locTransOutLine.StockTypeTo,
                                                        Item = _locTransOutLine.Item,
                                                        DQty = _locTransOutLine.PDQty,
                                                        DUOM = _locTransOutLine.PDUOM,
                                                        Qty = _locTransOutLine.PQty,
                                                        UOM = _locTransOutLine.PUOM,
                                                        TQty = _locTransOutLine.PTQty,
                                                        TransferOrder = _locTransferOrder2,

                                                    };
                                                    _saveDataTransferOrderLine.Save();
                                                    _saveDataTransferOrderLine.Session.CommitTransaction();

                                                    TransferOrderLine _locTransferOrderLine = _currSession.FindObject<TransferOrderLine>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("SignCode", _locSignCodeTOL)));
                                                    if (_locTransferOrderLine != null)
                                                    {
                                                        XPCollection<TransferOutLot> _locTransferOutLots = new XPCollection<TransferOutLot>(_currSession,
                                                                                                            new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("TransferOutLine", _locTransOutLine),
                                                                                                            new BinaryOperator("Select", true)));
                                                        if (_locTransferOutLots != null && _locTransferOutLots.Count() > 0)
                                                        {
                                                            foreach (TransferOutLot _locTransferOutLot in _locTransferOutLots)
                                                            {
                                                                TransferOrderLot _saveDataTransferOrderLot = new TransferOrderLot(_currSession)
                                                                {
                                                                    Select = true,
                                                                    LocationFrom = _locTransferOutLot.LocationFrom,
                                                                    BinLocationFrom = _locTransferOutLot.BinLocationFrom,
                                                                    StockTypeFrom = _locTransferOutLot.StockTypeFrom,
                                                                    LocationTo = _locTransferOutLot.LocationTo,
                                                                    BinLocationTo = _locTransferOutLot.BinLocationTo,
                                                                    StockTypeTo = _locTransferOutLot.StockTypeTo,
                                                                    Item = _locTransferOutLot.Item,
                                                                    BegInvLine = _locTransferOutLot.BegInvLine,
                                                                    LotNumber = _locTransferOutLot.LotNumber,
                                                                    DQty = _locTransferOutLot.DQty,
                                                                    DUOM = _locTransferOutLot.DUOM,
                                                                    Qty = _locTransferOutLot.Qty,
                                                                    UOM = _locTransferOutLot.UOM,
                                                                    TQty = _locTransferOutLot.TQty,
                                                                    TransferOrderLine = _locTransferOrderLine,

                                                                };
                                                                _saveDataTransferOrderLot.Save();
                                                                _saveDataTransferOrderLot.Session.CommitTransaction();
                                                            }
                                                        }
                                                    }
                                                }
                                                
                                            }

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = TransferOut ", ex.ToString());
            }
        }

        //Menentukan Status Deliver Pada Inventory Transfer Line
        private void SetStatusDeliverTransferOutLine(Session _currSession, TransferOut _transferOutXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_transferOutXPO != null)
                {
                    XPCollection<TransferOutLine> _locTransOutLines = new XPCollection<TransferOutLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("TransferOut", _transferOutXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locTransOutLines != null && _locTransOutLines.Count > 0)
                    {

                        foreach (TransferOutLine _locTransOutLine in _locTransOutLines)
                        {
                            if (_locTransOutLine.Status == Status.Progress || _locTransOutLine.Status == Status.Posted)
                            {
                                if (_locTransOutLine.RmDQty == 0 && _locTransOutLine.RmQty == 0 && _locTransOutLine.RmTQty == 0)
                                {
                                    _locTransOutLine.Status = Status.Close;
                                    _locTransOutLine.ActivationPosting = true;
                                    _locTransOutLine.StatusDate = now;
                                }
                                else
                                {
                                    _locTransOutLine.Status = Status.Posted;
                                    _locTransOutLine.StatusDate = now;
                                }

                                XPCollection<TransferOutLot> _locTransferOutLots = new XPCollection<TransferOutLot>(_currSession,
                                                                                         new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("TransferOutLine", _locTransOutLine),
                                                                                         new BinaryOperator("Select", true)));

                                if (_locTransferOutLots != null && _locTransferOutLots.Count() > 0)
                                {
                                    foreach (TransferOutLot _locTransferOutLot in _locTransferOutLots)
                                    {
                                        if (_locTransferOutLot.Status == Status.Progress || _locTransferOutLot.Status == Status.Posted)
                                        {
                                            _locTransferOutLot.Status = Status.Close;
                                            _locTransferOutLot.ActivationPosting = true;
                                            _locTransferOutLot.StatusDate = now;
                                            _locTransferOutLot.Save();
                                            _locTransferOutLot.Session.CommitTransaction();
                                        }
                                    }
                                }
                                _locTransOutLine.Save();
                                _locTransOutLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOut " + ex.ToString());
            }
        }

        //Menormalkan Quantity
        private void SetNormalDeliverQuantity(Session _currSession, TransferOut _transferOutXPO)
        {
            try
            {
                if (_transferOutXPO != null)
                {
                    XPCollection<TransferOutLine> _locTransOutLines = new XPCollection<TransferOutLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("TransferOut", _transferOutXPO),
                                                                                     new BinaryOperator("Select", true)));

                    if (_locTransOutLines != null && _locTransOutLines.Count > 0)
                    {
                        foreach (TransferOutLine _locTransOutLine in _locTransOutLines)
                        {
                            if (_locTransOutLine.Status == Status.Progress || _locTransOutLine.Status == Status.Posted || _locTransOutLine.Status == Status.Close)
                            {
                                if (_locTransOutLine.DQty > 0 || _locTransOutLine.Qty > 0)
                                {
                                    _locTransOutLine.Select = false;
                                    _locTransOutLine.DQty = 0;
                                    _locTransOutLine.Qty = 0;
                                    _locTransOutLine.Save();
                                    _locTransOutLine.Session.CommitTransaction();

                                    XPCollection<TransferOutLot> _locTransferOutLots = new XPCollection<TransferOutLot>(_currSession,
                                                                                         new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("TransferOutLine", _locTransOutLine),
                                                                                         new BinaryOperator("Select", true)));

                                    if (_locTransferOutLots != null && _locTransferOutLots.Count() > 0)
                                    {
                                        foreach (TransferOutLot _locTransferOutLot in _locTransferOutLots)
                                        {
                                            if (_locTransferOutLot.Status == Status.Progress || _locTransferOutLot.Status == Status.Posted)
                                            {
                                                _locTransferOutLot.Select = false;
                                                _locTransferOutLot.Save();
                                                _locTransferOutLot.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOut " + ex.ToString());
            }
        }

        //Menentukan Status Deliver Pada Inventory Transfer
        private void SetFinalStatusDeliverTransferOut(Session _currSession, TransferOut _transferOutXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locCount = 0;

                if (_transferOutXPO != null)
                {
                    XPCollection<TransferOutLine> _locTransOutLines = new XPCollection<TransferOutLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("TransferOut", _transferOutXPO)));

                    if (_locTransOutLines != null && _locTransOutLines.Count() > 0)
                    {
                        foreach (TransferOutLine _locTransOutLine in _locTransOutLines)
                        {
                            if (_locTransOutLine.Status == Status.Close)
                            {
                                _locCount = _locCount + 1;
                            }
                        }

                        if (_locCount == _locTransOutLines.Count())
                        {
                            _transferOutXPO.ActivationPosting = true;
                            _transferOutXPO.Status = Status.Close;
                            _transferOutXPO.StatusDate = now;
                            _transferOutXPO.Save();
                            _transferOutXPO.Session.CommitTransaction();
                        }
                        else
                        {
                            _transferOutXPO.Status = Status.Posted;
                            _transferOutXPO.StatusDate = now;
                            _transferOutXPO.Save();
                            _transferOutXPO.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferOut " + ex.ToString());
            }
        }

        #endregion TransferOut

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private int GetStockType(StockType objectName)
        {
            int _result = 0;
            try
            {
                if (objectName == StockType.Good)
                {
                    _result = 1;
                }
                else if (objectName == StockType.Bad)
                {
                    _result = 2;
                }
                else
                {
                    _result = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = TransferOut " + ex.ToString());
            }
            return _result;
        }

        private int GetActive(bool objectName)
        {
            int _result = 0;
            try
            {
                if (objectName == true)
                {
                    _result = 1;
                }
                else
                {
                    _result = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = TransferOut " + ex.ToString());
            }
            return _result;
        }

        #endregion Global Method

    }
}
