﻿namespace FullDrive.Module.Controllers
{
    partial class SalesQuotationActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SalesQuotationProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesQuotationPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesQuotationListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.SalesQuotationListviewFilterApprovalSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.SalesQuotationApprovalAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // SalesQuotationProgressAction
            // 
            this.SalesQuotationProgressAction.Caption = "Progress";
            this.SalesQuotationProgressAction.ConfirmationMessage = null;
            this.SalesQuotationProgressAction.Id = "SalesQuotationProgressActionId";
            this.SalesQuotationProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesQuotation);
            this.SalesQuotationProgressAction.ToolTip = null;
            this.SalesQuotationProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesQuotationProgressAction_Execute);
            // 
            // SalesQuotationPostingAction
            // 
            this.SalesQuotationPostingAction.Caption = "Posting";
            this.SalesQuotationPostingAction.ConfirmationMessage = null;
            this.SalesQuotationPostingAction.Id = "SalesQuotationPostingActionId";
            this.SalesQuotationPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesQuotation);
            this.SalesQuotationPostingAction.ToolTip = null;
            this.SalesQuotationPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesQuotationPostingAction_Execute);
            // 
            // SalesQuotationListviewFilterSelectionAction
            // 
            this.SalesQuotationListviewFilterSelectionAction.Caption = "Filter";
            this.SalesQuotationListviewFilterSelectionAction.ConfirmationMessage = null;
            this.SalesQuotationListviewFilterSelectionAction.Id = "SalesQuotationListviewFilterSelectionActionId";
            this.SalesQuotationListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.SalesQuotationListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesQuotation);
            this.SalesQuotationListviewFilterSelectionAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.SalesQuotationListviewFilterSelectionAction.ToolTip = null;
            this.SalesQuotationListviewFilterSelectionAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.SalesQuotationListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.SalesQuotationListviewFilterSelectionAction_Execute);
            // 
            // SalesQuotationListviewFilterApprovalSelectionAction
            // 
            this.SalesQuotationListviewFilterApprovalSelectionAction.Caption = "Filter Approval";
            this.SalesQuotationListviewFilterApprovalSelectionAction.ConfirmationMessage = null;
            this.SalesQuotationListviewFilterApprovalSelectionAction.Id = "SalesQuotationListviewFilterApprovalSelectionActionId";
            this.SalesQuotationListviewFilterApprovalSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.SalesQuotationListviewFilterApprovalSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesQuotation);
            this.SalesQuotationListviewFilterApprovalSelectionAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.SalesQuotationListviewFilterApprovalSelectionAction.ToolTip = null;
            this.SalesQuotationListviewFilterApprovalSelectionAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.SalesQuotationListviewFilterApprovalSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.SalesQuotationListviewFilterApprovalSelectionAction_Execute);
            // 
            // SalesQuotationApprovalAction
            // 
            this.SalesQuotationApprovalAction.Caption = "Approval";
            this.SalesQuotationApprovalAction.ConfirmationMessage = null;
            this.SalesQuotationApprovalAction.Id = "SalesQuotationApprovalActionId";
            this.SalesQuotationApprovalAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.SalesQuotationApprovalAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesQuotation);
            this.SalesQuotationApprovalAction.ToolTip = null;
            this.SalesQuotationApprovalAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.SalesQuotationApprovalAction_Execute);
            // 
            // SalesQuotationActionController
            // 
            this.Actions.Add(this.SalesQuotationProgressAction);
            this.Actions.Add(this.SalesQuotationPostingAction);
            this.Actions.Add(this.SalesQuotationListviewFilterSelectionAction);
            this.Actions.Add(this.SalesQuotationListviewFilterApprovalSelectionAction);
            this.Actions.Add(this.SalesQuotationApprovalAction);
            this.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesQuotation);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction SalesQuotationProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesQuotationPostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction SalesQuotationListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction SalesQuotationListviewFilterApprovalSelectionAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction SalesQuotationApprovalAction;
    }
}
