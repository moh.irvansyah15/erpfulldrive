﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class FinishProductionActionController : ViewController
    {
        #region Default

        private ChoiceActionItem _selectionListviewFilter;
        public FinishProductionActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.

            #region FilterStatus
            FinishProductionFilterAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                FinishProductionFilterAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterStatus

            #region FilterApproval
            FinishProductionListviewFilterApprovalSelectionAction.Items.Clear();
            foreach (object _currApprovalFilter in Enum.GetValues(typeof(CustomProcess.ApprovalFilter)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalFilter));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApprovalFilter), _currApprovalFilter);
                FinishProductionListviewFilterApprovalSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterApproval
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        private void FinishProductionProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        FinishProduction _locFinishProductionOS = (FinishProduction)_objectSpace.GetObject(obj);

                        if (_locFinishProductionOS != null)
                        {
                            if (_locFinishProductionOS.Code != null)
                            {
                                _currObjectId = _locFinishProductionOS.Code;

                                FinishProduction _locFinishProductionXPO = _currSession.FindObject<FinishProduction>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Code", _currObjectId)));

                                if (_locFinishProductionXPO != null)
                                {
                                    if (_locFinishProductionXPO.Status == Status.Open)
                                    {
                                        _locFinishProductionXPO.Status = Status.Progress;
                                        _locFinishProductionXPO.StatusDate = now;
                                        _locFinishProductionXPO.Save();
                                        _locFinishProductionXPO.Session.CommitTransaction();

                                        XPCollection<FinishProductionLine> _locFinishProductionLines = new XPCollection<FinishProductionLine>
                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("FinishProduction", _locFinishProductionXPO)));

                                        if (_locFinishProductionLines != null && _locFinishProductionLines.Count > 0)
                                        {
                                            foreach (FinishProductionLine _locFinishProductionLine in _locFinishProductionLines)
                                            {
                                                if (_locFinishProductionLine.Status == Status.Open)
                                                {
                                                    _locFinishProductionLine.Status = Status.Progress;
                                                    _locFinishProductionLine.StatusDate = now;
                                                    _locFinishProductionLine.Save();
                                                    _locFinishProductionLine.Session.CommitTransaction();
                                                }
                                            }
                                        }

                                        SuccessMessageShow(_locFinishProductionXPO.Code + " has been change successfully to Progress");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data FinishProduction Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data FinishProduction Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FinishProduction " + ex.ToString());
            }
        }

        private void FinishProductionPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        FinishProduction _locFinishProductionOS = (FinishProduction)_objectSpace.GetObject(obj);

                        if (_locFinishProductionOS != null)
                        {
                            if (_locFinishProductionOS.Code != null)
                            {
                                _currObjectId = _locFinishProductionOS.Code;

                                FinishProduction _locFinishProductionXPO = _currSession.FindObject<FinishProduction>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Code", _currObjectId)));

                                if (_locFinishProductionXPO != null)
                                {
                                    if (_locFinishProductionXPO.Status == Status.Progress || _locFinishProductionXPO.Status == Status.Posted)
                                    {
                                        ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("EndApproval", true),
                                                                            new BinaryOperator("FinishProduction", _locFinishProductionXPO)));

                                        if (_locApprovalLineXPO != null)
                                        {
                                            SetPostingQty(_currSession, _locFinishProductionXPO);
                                            SetRemainQty(_currSession, _locFinishProductionXPO);
                                            SetTransferOrder(_currSession, _locFinishProductionXPO);
                                            SetFinishJournal(_currSession, _locFinishProductionXPO);
                                            SetProcessCountForPostingTO(_currSession, _locFinishProductionXPO);
                                            SetStatusFinishProdLine(_currSession, _locFinishProductionXPO);
                                            SetNormalQty(_currSession, _locFinishProductionXPO);
                                            SetStatusFinishProd(_currSession, _locFinishProductionXPO);
                                        }
                                        else
                                        {
                                            ErrorMessageShow("Data Approval Not Available");
                                        }
                                    }
                                    else
                                    {
                                        ErrorMessageShow("Data FinishProduction Not Available");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data FinishProduction Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data FinishProduction Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Data FinishProduction Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FinishProduction " + ex.ToString());
            }
        }

        private void FinishProductionFilterAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(FinishProduction)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FinishProduction " + ex.ToString());
            }
        }

        private void FinishProductionListviewFilterApprovalSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(FinishProduction)))
                {
                    if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.Approval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal)));
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.EndApproval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal);
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.AllData)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved1", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal));
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FinishProduction " + ex.ToString());
            }
        }

        //==== Code Only ====

        #region PostingforTransferOrder

        //Menentukan jumlah quantity yang di posting
        private void SetPostingQty(Session _currSession, FinishProduction _locFinishProductionXPO)
        {
            try
            {
                if (_locFinishProductionXPO != null)
                {
                    XPCollection<FinishProductionLine> _locFinishProductionLines = new XPCollection<FinishProductionLine>(_currSession,
                                                                                   new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("FinishProduction", _locFinishProductionXPO),
                                                                                   new BinaryOperator("QCPassed", true),
                                                                                   new BinaryOperator("Select", true)));

                    if (_locFinishProductionLines != null && _locFinishProductionLines.Count > 0)
                    {
                        double _locPDQty = 0;
                        double _locPQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (FinishProductionLine _locFinishProductionLine in _locFinishProductionLines)
                        {
                            #region ProcessCount=0
                            if (_locFinishProductionLine.ProcessCount == 0)
                            {
                                #region MaxQuantity
                                if (_locFinishProductionLine.MxDQty > 0 || _locFinishProductionLine.MxQty > 0)
                                {
                                    if (_locFinishProductionLine.DQty > 0 && _locFinishProductionLine.DQty <= _locFinishProductionLine.MxDQty)
                                    {
                                        _locPDQty = _locFinishProductionLine.DQty;
                                    }

                                    if (_locFinishProductionLine.Qty > 0 && _locFinishProductionLine.Qty <= _locFinishProductionLine.MxQty)
                                    {
                                        _locPQty = _locFinishProductionLine.Qty;
                                    }

                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locFinishProductionLine.Item),
                                                   new BinaryOperator("UOM", _locFinishProductionLine.MxUOM),
                                                   new BinaryOperator("DefaultUOM", _locFinishProductionLine.MxDUOM),
                                                   new BinaryOperator("Active", true)));

                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }

                                }
                                #endregion MaxQuantity

                                #region NonMaxQuantity
                                else
                                {
                                    if (_locFinishProductionLine.DQty > 0)
                                    {
                                        _locPDQty = _locFinishProductionLine.DQty;
                                    }

                                    if (_locFinishProductionLine.Qty > 0)
                                    {
                                        _locPQty = _locFinishProductionLine.Qty;
                                    }

                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locFinishProductionLine.Item),
                                                   new BinaryOperator("UOM", _locFinishProductionLine.UOM),
                                                   new BinaryOperator("DefaultUOM", _locFinishProductionLine.DUOM),
                                                   new BinaryOperator("Active", true)));

                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locFinishProductionLine.ProcessCount > 0)
                            {
                                if (_locFinishProductionLine.PDQty > 0)
                                {
                                    _locPDQty = _locFinishProductionLine.PDQty + _locFinishProductionLine.DQty;
                                }

                                if (_locFinishProductionLine.PQty > 0)
                                {
                                    _locPQty = _locFinishProductionLine.PQty + _locFinishProductionLine.Qty;
                                }

                                if (_locFinishProductionLine.MxDQty > 0 || _locFinishProductionLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locFinishProductionLine.Item),
                                                   new BinaryOperator("UOM", _locFinishProductionLine.MxUOM),
                                                   new BinaryOperator("DefaultUOM", _locFinishProductionLine.MxDUOM),
                                                   new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locFinishProductionLine.Item),
                                                   new BinaryOperator("UOM", _locFinishProductionLine.UOM),
                                                   new BinaryOperator("DefaultUOM", _locFinishProductionLine.DUOM),
                                                   new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locFinishProductionLine.PDQty = _locPDQty;
                            _locFinishProductionLine.PDUOM = _locFinishProductionLine.DUOM;
                            _locFinishProductionLine.PQty = _locPQty;
                            _locFinishProductionLine.PUOM = _locFinishProductionLine.UOM;
                            _locFinishProductionLine.PTQty = _locInvLineTotal;
                            _locFinishProductionLine.Save();
                            _locFinishProductionLine.Session.CommitTransaction();
                        }
                    }
                    else
                    {
                        ErrorMessageShow("QC Passed Not Available");
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FinishProduction " + ex.ToString());
            }
        }

        //Menentukan sisa dari transaksi 
        private void SetRemainQty(Session _currSession, FinishProduction _locFinishProductionXPO)
        {
            try
            {
                if (_locFinishProductionXPO != null)
                {
                    XPCollection<FinishProductionLine> _locFinishProductionLines = new XPCollection<FinishProductionLine>(_currSession,
                                                                                   new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("FinishProduction", _locFinishProductionXPO),
                                                                                   new BinaryOperator("QCPassed", true),
                                                                                   new BinaryOperator("Select", true)));

                    if (_locFinishProductionLines != null && _locFinishProductionLines.Count > 0)
                    {
                        double _locRmDQty = 0;
                        double _locRmQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (FinishProductionLine _locFinishProductionLine in _locFinishProductionLines)
                        {
                            #region ProcessCount=0
                            if (_locFinishProductionLine.ProcessCount == 0)
                            {
                                #region MaxQuantity
                                if (_locFinishProductionLine.MxDQty > 0 || _locFinishProductionLine.MxQty > 0)
                                {
                                    if (_locFinishProductionLine.DQty > 0 && _locFinishProductionLine.DQty <= _locFinishProductionLine.MxDQty)
                                    {
                                        _locRmDQty = _locFinishProductionLine.MxDQty - _locFinishProductionLine.DQty;
                                    }

                                    if (_locFinishProductionLine.Qty > 0 && _locFinishProductionLine.Qty <= _locFinishProductionLine.MxQty)
                                    {
                                        _locRmQty = _locFinishProductionLine.MxQty - _locFinishProductionLine.Qty;
                                    }

                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locFinishProductionLine.Item),
                                                   new BinaryOperator("UOM", _locFinishProductionLine.MxUOM),
                                                   new BinaryOperator("DefaultUOM", _locFinishProductionLine.MxDUOM),
                                                   new BinaryOperator("Active", true)));

                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }

                                }
                                #endregion MaxQuantity

                                #region NonMaxQuantity
                                else
                                {
                                    if (_locFinishProductionLine.DQty > 0)
                                    {
                                        _locRmDQty = _locFinishProductionLine.DQty;
                                    }

                                    if (_locFinishProductionLine.Qty > 0)
                                    {
                                        _locRmQty = _locFinishProductionLine.Qty;
                                    }

                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locFinishProductionLine.Item),
                                                   new BinaryOperator("UOM", _locFinishProductionLine.UOM),
                                                   new BinaryOperator("DefaultUOM", _locFinishProductionLine.DUOM),
                                                   new BinaryOperator("Active", true)));

                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locFinishProductionLine.ProcessCount > 0)
                            {
                                if (_locFinishProductionLine.RmDQty > 0)
                                {
                                    _locRmDQty = _locFinishProductionLine.RmDQty - _locFinishProductionLine.DQty;
                                }

                                if (_locFinishProductionLine.RmQty > 0)
                                {
                                    _locRmQty = _locFinishProductionLine.RmQty - _locFinishProductionLine.Qty;
                                }

                                if (_locFinishProductionLine.MxDQty > 0 || _locFinishProductionLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locFinishProductionLine.Item),
                                                   new BinaryOperator("UOM", _locFinishProductionLine.MxUOM),
                                                   new BinaryOperator("DefaultUOM", _locFinishProductionLine.MxDUOM),
                                                   new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locFinishProductionLine.Item),
                                                   new BinaryOperator("UOM", _locFinishProductionLine.UOM),
                                                   new BinaryOperator("DefaultUOM", _locFinishProductionLine.DUOM),
                                                   new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locFinishProductionLine.RmDQty = _locRmDQty;
                            _locFinishProductionLine.RmQty = _locRmQty;
                            _locFinishProductionLine.RmTQty = _locInvLineTotal;
                            _locFinishProductionLine.Save();
                            _locFinishProductionLine.Session.CommitTransaction();
                        }
                    }
                    else
                    {
                        ErrorMessageShow("QC Passed Not Available");
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FinishProduction " + ex.ToString());
            }
        }

        //Membuat transferorder
        private void SetTransferOrder(Session _currSession, FinishProduction _locFinishProductionXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locFinishProductionXPO != null)
                {
                    TransferOrder _saveDataTransferOrder = new TransferOrder(_currSession)
                    {
                        StatusDate = _locFinishProductionXPO.StatusDate,
                        Company = _locFinishProductionXPO.Company,
                        FinishProduction = _locFinishProductionXPO,
                    };
                    _saveDataTransferOrder.Save();
                    _saveDataTransferOrder.Session.CommitTransaction();

                    XPCollection<FinishProductionLine> _numLineFinishProductionLines = new XPCollection<FinishProductionLine>(_currSession,
                                                                                       new GroupOperator(GroupOperatorType.And,
                                                                                       new BinaryOperator("Select", true),
                                                                                       new BinaryOperator("QCPassed", true),
                                                                                       new BinaryOperator("FinishProduction", _locFinishProductionXPO)));

                    if (_numLineFinishProductionLines != null)
                    {
                        TransferOrder _locTransferOrder2 = _currSession.FindObject<TransferOrder>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("FinishProduction", _locFinishProductionXPO)));

                        if (_locTransferOrder2 != null)
                        {
                            if (_numLineFinishProductionLines != null && _numLineFinishProductionLines.Count > 0)
                            {
                                foreach (FinishProductionLine _numLineFinishProductionLine in _numLineFinishProductionLines)
                                {
                                    if (_numLineFinishProductionLine.Status == Status.Progress || _numLineFinishProductionLine.Status == Status.Posted)
                                    {
                                        TransferOrderLine _saveDataTransferOrderLine = new TransferOrderLine(_currSession)
                                        {
                                            Item = _numLineFinishProductionLine.Item,
                                            MxDQty = _numLineFinishProductionLine.DQty,
                                            MxDUOM = _numLineFinishProductionLine.DUOM,
                                            MxQty = _numLineFinishProductionLine.Qty,
                                            MxUOM = _numLineFinishProductionLine.UOM,
                                            MxTQty = _numLineFinishProductionLine.TQty,
                                            DQty = _numLineFinishProductionLine.DQty,
                                            DUOM = _numLineFinishProductionLine.DUOM,
                                            Qty = _numLineFinishProductionLine.Qty,
                                            UOM = _numLineFinishProductionLine.UOM,
                                            TQty = _numLineFinishProductionLine.TQty,
                                            TransferOrder = _locTransferOrder2,
                                        };
                                        _saveDataTransferOrderLine.Save();
                                        _saveDataTransferOrderLine.Session.CommitTransaction();
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        ErrorMessageShow("QC Passed Not Available");
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = FinishProduction ", ex.ToString());
            }
        }

        //Membuat finish journal
        private void SetFinishJournal(Session _currSession, FinishProduction _locFinishProductionXPO)
        {
            try
            {
                XPCollection<FinishProductionLine> _locFinishProdLines = new XPCollection<FinishProductionLine>(_currSession,
                                                                         new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("FinishProduction", _locFinishProductionXPO),
                                                                         new BinaryOperator("QCPassed", true),
                                                                         new BinaryOperator("Select", true)));

                if (_locFinishProdLines != null && _locFinishProdLines.Count > 0)
                {
                    double _locFinishProdLineTotal = 0;
                    double _locFinishProdLineTotalActual = 0;
                    DateTime now = DateTime.Now;

                    foreach (FinishProductionLine _locFinishProdLine in _locFinishProdLines)
                    {
                        if (_locFinishProdLine.Status == Status.Progress || _locFinishProdLine.Status == Status.Posted)
                        {
                            if (_locFinishProdLine.DQty > 0 || _locFinishProdLine.Qty > 0)
                            {
                                if (_locFinishProdLine.Item != null && _locFinishProdLine.UOM != null && _locFinishProdLine.DUOM != null)
                                {
                                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Item", _locFinishProdLine.Item),
                                                                     new BinaryOperator("UOM", _locFinishProdLine.UOM),
                                                                     new BinaryOperator("DefaultUOM", _locFinishProdLine.DUOM),
                                                                     new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        #region Code
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locFinishProdLineTotal = _locFinishProdLine.Qty * _locItemUOM.DefaultConversion + _locFinishProdLine.DQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locFinishProdLineTotal = _locFinishProdLine.Qty / _locItemUOM.Conversion + _locFinishProdLine.DQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locFinishProdLineTotal = _locFinishProdLine.Qty + _locFinishProdLine.DQty;
                                        }
                                        #endregion Code

                                        #region Actual
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locFinishProdLineTotalActual = _locFinishProdLine.AQty * _locItemUOM.DefaultConversion + _locFinishProdLine.ADQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locFinishProdLineTotalActual = _locFinishProdLine.AQty / _locItemUOM.Conversion + _locFinishProdLine.ADQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locFinishProdLineTotalActual = _locFinishProdLine.AQty + _locFinishProdLine.ADQty;
                                        }
                                        #endregion Actual

                                        FinishProductionJournal _locFinishProdJournal = new FinishProductionJournal(_currSession)
                                        {
                                            Location = _locFinishProdLine.Location,
                                            BinLocation = _locFinishProdLine.BinLocation,
                                            Item = _locFinishProdLine.Item,
                                            Company = _locFinishProdLine.Company,
                                            QtyFinish = _locFinishProdLineTotal,
                                            QtyActual = _locFinishProdLineTotalActual,
                                            QtyReturn = _locFinishProdLineTotal - _locFinishProdLineTotalActual,
                                            DUOM = _locFinishProdLine.DUOM,
                                            JournalDate = now,
                                            FinishProduction = _locFinishProductionXPO,
                                        };

                                        _locFinishProdJournal.Save();
                                        _locFinishProdJournal.Session.CommitTransaction();
                                    }
                                }
                                else
                                {
                                    _locFinishProdLineTotal = _locFinishProdLine.Qty + _locFinishProdLine.DQty;
                                    _locFinishProdLineTotalActual = _locFinishProdLine.AQty + _locFinishProdLine.ADQty;

                                    FinishProductionJournal _locFinishProdJournal = new FinishProductionJournal(_currSession)
                                    {
                                        Location = _locFinishProdLine.Location,
                                        BinLocation = _locFinishProdLine.BinLocation,
                                        Item = _locFinishProdLine.Item,
                                        Company = _locFinishProdLine.Company,
                                        QtyFinish = _locFinishProdLineTotal,
                                        QtyActual = _locFinishProdLineTotalActual,
                                        QtyReturn = _locFinishProdLineTotal - _locFinishProdLineTotalActual,
                                        DUOM = _locFinishProdLine.DUOM,
                                        JournalDate = now,
                                        FinishProduction = _locFinishProductionXPO,
                                    };

                                    _locFinishProdJournal.Save();
                                    _locFinishProdJournal.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }
                else
                {
                    ErrorMessageShow("QC Passed Not Available");
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = FinishProduction ", ex.ToString());
            }
        }

        //Menentukan banyak proses posting
        private void SetProcessCountForPostingTO(Session _currSession, FinishProduction _locFinishProductionXPO)
        {
            try
            {
                if (_locFinishProductionXPO != null)
                {
                    XPCollection<FinishProductionLine> _locFinishProdLines = new XPCollection<FinishProductionLine>(_currSession,
                                                                             new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("FinishProduction", _locFinishProductionXPO),
                                                                             new BinaryOperator("QCPassed", true),
                                                                             new BinaryOperator("Select", true)));

                    if (_locFinishProdLines != null && _locFinishProdLines.Count > 0)
                    {
                        foreach (FinishProductionLine _locFinishProdLine in _locFinishProdLines)
                        {
                            if (_locFinishProdLine.Status == Status.Progress || _locFinishProdLine.Status == Status.Posted)
                            {
                                _locFinishProdLine.ProcessCount = _locFinishProdLine.ProcessCount + 1;
                                _locFinishProdLine.Save();
                                _locFinishProdLine.Session.CommitTransaction();
                            }
                        }
                    }
                    else
                    {
                        ErrorMessageShow("QC Passed Not Available");
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FinishProduction " + ex.ToString());
            }
        }

        //Menentukan status pada finishproductionline
        private void SetStatusFinishProdLine(Session _currSession, FinishProduction _locFinishProductionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locFinishProductionXPO != null)
                {
                    XPCollection<FinishProductionLine> _locFinishProdLines = new XPCollection<FinishProductionLine>(_currSession,
                                                                             new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("FinishProduction", _locFinishProductionXPO),
                                                                             new BinaryOperator("QCPassed", true),
                                                                             new BinaryOperator("Select", true)));

                    if (_locFinishProdLines != null && _locFinishProdLines.Count > 0)
                    {
                        foreach (FinishProductionLine _locFinishProdLine in _locFinishProdLines)
                        {
                            if (_locFinishProdLine.Status == Status.Progress || _locFinishProdLine.Status == Status.Posted)
                            {
                                if (_locFinishProdLine.DQty > 0 || _locFinishProdLine.Qty > 0)
                                {
                                    if (_locFinishProdLine.RmDQty == 0 && _locFinishProdLine.RmQty == 0 && _locFinishProdLine.RmTQty == 0)
                                    {
                                        _locFinishProdLine.Status = Status.Close;
                                        _locFinishProdLine.ActivationPosting = true;
                                        _locFinishProdLine.StatusDate = now;
                                    }
                                    else
                                    {
                                        _locFinishProdLine.Status = Status.Posted;
                                        _locFinishProdLine.StatusDate = now;
                                    }
                                    _locFinishProdLine.Save();
                                    _locFinishProdLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                    else
                    {
                        ErrorMessageShow("QC Passed Not Available");
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FinishProduction " + ex.ToString());
            }
        }

        //Menormalkan quantity
        private void SetNormalQty(Session _currSession, FinishProduction _locFinishProductionXPO)
        {
            try
            {
                if (_locFinishProductionXPO != null)
                {
                    XPCollection<FinishProductionLine> _locFinishProdLines = new XPCollection<FinishProductionLine>(_currSession,
                                                                             new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("FinishProduction", _locFinishProductionXPO),
                                                                             new BinaryOperator("QCPassed", true),
                                                                             new BinaryOperator("Select", true)));

                    if (_locFinishProdLines != null && _locFinishProdLines.Count > 0)
                    {
                        foreach (FinishProductionLine _locFinishProdLine in _locFinishProdLines)
                        {
                            if (_locFinishProdLine.Status == Status.Progress || _locFinishProdLine.Status == Status.Posted || _locFinishProdLine.Status == Status.Close)
                            {
                                if (_locFinishProdLine.DQty > 0 || _locFinishProdLine.Qty > 0)
                                {
                                    _locFinishProdLine.Select = false;
                                    _locFinishProdLine.DQty = 0;
                                    _locFinishProdLine.Qty = 0;
                                    _locFinishProdLine.Save();
                                    _locFinishProdLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                    else
                    {
                        ErrorMessageShow("QC Passed Not Available");
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FinishProduction " + ex.ToString());
            }
        }

        //Menentukan status pada finishproduction
        private void SetStatusFinishProd(Session _currSession, FinishProduction _locFinishProductionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locCountStatus = 0;
                bool _locActivationPosting = false;

                if (_locFinishProductionXPO != null)
                {
                    if (_locFinishProductionXPO.Status == Status.Posted)
                    {
                        SetCloseAllProductionProcess(_currSession, _locFinishProductionXPO);
                        SuccessMessageShow(_locFinishProductionXPO.Code + " has been change successfully to Posted");
                    }
                    else
                    {
                        XPCollection<FinishProductionLine> _locFinishProductionLines = new XPCollection<FinishProductionLine>
                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("Select", true),
                                                                                    new BinaryOperator("FinishProduction", _locFinishProductionXPO)));

                        if (_locFinishProductionLines != null && _locFinishProductionLines.Count() > 0)
                        {
                            foreach (FinishProductionLine _locFinishProductionLine in _locFinishProductionLines)
                            {
                                if (_locFinishProductionLine.Status != Status.Close)
                                {
                                    _locCountStatus = _locCountStatus + 1;
                                }
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Data FinishProductionLine Not Available");
                        }

                        if (_locCountStatus > 0)
                        {
                            if (_locFinishProductionLines.Count() == _locCountStatus)
                            {
                                _locActivationPosting = true;
                            }
                        }

                        _locFinishProductionXPO.Status = Status.Posted;
                        _locFinishProductionXPO.StatusDate = now;
                        _locFinishProductionXPO.ActivationPosting = _locActivationPosting;
                        _locFinishProductionXPO.Save();
                        _locFinishProductionXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkOrder " + ex.ToString());
            }
        }

        //Menentukan status close
        private void SetCloseAllProductionProcess(Session _currSession, FinishProduction _locFinishProductionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                bool _locActivationPosting = false;

                if (_locFinishProductionXPO.OutputProduction != null)
                {
                    XPCollection<FinishProduction> _locFinishProductions = new XPCollection<FinishProduction>(_currSession,
                                                                           new GroupOperator(GroupOperatorType.And,
                                                                           new BinaryOperator("OutputProduction", _locFinishProductionXPO.OutputProduction)));

                    if (_locFinishProductions != null && _locFinishProductions.Count > 0)
                    {
                        //Ubah status WorkOrder
                        #region WorkOrder
                        if (_locFinishProductionXPO.WorkOrder != null)
                        {
                            if (_locFinishProductionXPO.WorkOrder.Code != null)
                            {
                                WorkOrder _locWorkOrder = _currSession.FindObject<WorkOrder>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Code", _locFinishProductionXPO.WorkOrder.Code)));
                                if (_locWorkOrder != null)
                                {
                                    _locWorkOrder.Status = Status.Close;
                                    _locWorkOrder.StatusDate = now;
                                    _locWorkOrder.Save();
                                    _locWorkOrder.Session.CommitTransaction();
                                }
                            }
                        }
                        #endregion WorkOrder

                        //Ubah status Production
                        #region Production
                        if (_locFinishProductionXPO.Production != null)
                        {
                            if (_locFinishProductionXPO.Production.Code != null)
                            {
                                Production _locProduction = _currSession.FindObject<Production>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                             new BinaryOperator("Code", _locFinishProductionXPO.Production.Code)));
                                if (_locProduction != null)
                                {
                                    _locProduction.Status = Status.Close;
                                    _locProduction.StatusDate = now;
                                    _locProduction.Save();
                                    _locProduction.Session.CommitTransaction();
                                }
                            }
                        }
                        #endregion Production

                        //Ubah status Consumption
                        #region Consumption
                        if (_locFinishProductionXPO.Consumption.Code != null)
                        {
                            Consumption _locConsumption = _currSession.FindObject<Consumption>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Code", _locFinishProductionXPO.Consumption.Code)));
                            if (_locConsumption != null)
                            {
                                _locConsumption.Status = Status.Close;
                                _locConsumption.StatusDate = now;
                                _locConsumption.Save();
                                _locConsumption.Session.CommitTransaction();
                            }
                        }
                        #endregion Consumptio

                        //Ubah status OutputProduction
                        #region OutputProduction
                        if (_locFinishProductionXPO.OutputProduction != null)
                        {
                            if (_locFinishProductionXPO.OutputProduction.Code != null)
                            {
                                OutputProduction _locOutputProduction = _currSession.FindObject<OutputProduction>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Code", _locFinishProductionXPO.OutputProduction.Code)));

                                if (_locOutputProduction != null)
                                {
                                    _locOutputProduction.Status = Status.Close;
                                    _locOutputProduction.StatusDate = now;
                                    _locOutputProduction.Save();
                                    _locOutputProduction.Session.CommitTransaction();
                                }
                            }
                        }
                        #endregion OutputProduction

                        //Ubah status WorkorderMaterial
                        #region WorkOrderMaterial
                        if (_locFinishProductionXPO.WorkOrder != null)
                        {
                            if (_locFinishProductionXPO.WorkOrder.Code != null)
                            {
                                WorkOrder _locWorkOrder = _currSession.FindObject<WorkOrder>
                                                          (new GroupOperator(GroupOperatorType.And,
                                                          new BinaryOperator("Code", _locFinishProductionXPO.WorkOrder.Code)));

                                if (_locWorkOrder != null)
                                {
                                    WorkOrderMaterial _locWorkOrderMaterial = _currSession.FindObject<WorkOrderMaterial>
                                                                              (new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Code", _locFinishProductionXPO.WorkOrder.Code)));

                                    if (_locWorkOrderMaterial != null)
                                    {
                                        _locWorkOrderMaterial.Status = Status.Close;
                                        _locWorkOrderMaterial.StatusDate = now;
                                        _locWorkOrderMaterial.Save();
                                        _locWorkOrderMaterial.Session.CommitTransaction();
                                    }
                                }
                            }
                        }
                        #endregion WorkOrderMaterial

                        //Ubah status ProductionMaterial
                        #region ProductionMaterial
                        if (_locFinishProductionXPO.WorkOrder != null)
                        {
                            if (_locFinishProductionXPO.WorkOrder.Code != null)
                            {
                                Production _locProduction = _currSession.FindObject<Production>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                             new BinaryOperator("Code", _locFinishProductionXPO.Production.Code)));

                                if (_locProduction != null)
                                {
                                    ProductionMaterial _locProductionMaterial = _currSession.FindObject<ProductionMaterial>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("Code", _locFinishProductionXPO.Production.Code)));

                                    if (_locProductionMaterial != null)
                                    {
                                        _locProductionMaterial.Status = Status.Close;
                                        _locProductionMaterial.StatusDate = now;
                                        _locProductionMaterial.Save();
                                        _locProductionMaterial.Session.CommitTransaction();
                                    }
                                }
                            }
                        }
                        #endregion ProductionMaterial

                        foreach (FinishProduction _locFinishProduction in _locFinishProductions)
                        {
                            _locFinishProduction.Status = Status.Close;
                            _locActivationPosting = true;
                            _locFinishProduction.StatusDate = now;
                            _locFinishProduction.Save();
                            _locFinishProduction.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FinishProduction " + ex.ToString());
            }
        }

        #endregion PostingforTransferOrder

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method
        
    }
}
