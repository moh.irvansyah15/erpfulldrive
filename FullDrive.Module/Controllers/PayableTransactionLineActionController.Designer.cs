﻿namespace FullDrive.Module.Controllers
{
    partial class PayableTransactionLineActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PayableTransactionLineExchangeRatesAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PayableTransactionLineListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // PayableTransactionLineExchangeRatesAction
            // 
            this.PayableTransactionLineExchangeRatesAction.Caption = "Change Rates";
            this.PayableTransactionLineExchangeRatesAction.ConfirmationMessage = null;
            this.PayableTransactionLineExchangeRatesAction.Id = "PayableTransactionLineExchangeRatesActionId";
            this.PayableTransactionLineExchangeRatesAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PayableTransactionLine);
            this.PayableTransactionLineExchangeRatesAction.ToolTip = null;
            this.PayableTransactionLineExchangeRatesAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PayableTransactionLineExchangeRatesAction_Execute);
            // 
            // PayableTransactionLineListviewFilterSelectionAction
            // 
            this.PayableTransactionLineListviewFilterSelectionAction.Caption = "Filter";
            this.PayableTransactionLineListviewFilterSelectionAction.ConfirmationMessage = null;
            this.PayableTransactionLineListviewFilterSelectionAction.Id = "PayableTransactionLineListviewFilterSelectionActionId";
            this.PayableTransactionLineListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.PayableTransactionLineListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PayableTransactionLine);
            this.PayableTransactionLineListviewFilterSelectionAction.ToolTip = null;
            this.PayableTransactionLineListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.PayableTransactionLineListviewFilterSelectionAction_Execute);
            // 
            // PayableTransactionLineActionController
            // 
            this.Actions.Add(this.PayableTransactionLineExchangeRatesAction);
            this.Actions.Add(this.PayableTransactionLineListviewFilterSelectionAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction PayableTransactionLineExchangeRatesAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction PayableTransactionLineListviewFilterSelectionAction;
    }
}
