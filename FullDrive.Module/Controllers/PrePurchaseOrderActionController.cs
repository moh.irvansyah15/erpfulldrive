﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PrePurchaseOrderActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;

        public PrePurchaseOrderActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            PrePurchaseOrderListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {

                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                PrePurchaseOrderListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void PrePurchaseOrderProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PrePurchaseOrder _locPrePurchaseOrderOS = (PrePurchaseOrder)_objectSpace.GetObject(obj);

                        if (_locPrePurchaseOrderOS != null)
                        {
                            if (_locPrePurchaseOrderOS.Code != null)
                            {
                                _currObjectId = _locPrePurchaseOrderOS.Code;

                                PrePurchaseOrder _locPrePurchaseOrderXPO = _currSession.FindObject<PrePurchaseOrder>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locPrePurchaseOrderXPO != null)
                                {
                                    if (_locPrePurchaseOrderXPO.Status == Status.Open)
                                    {
                                        if (_locPrePurchaseOrderXPO.ProjectHeader != null)
                                        {
                                            ProjectHeader _locProjectHeaderXPO = _currSession.FindObject<ProjectHeader>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _locPrePurchaseOrderXPO.ProjectHeader.Code)));

                                            if (_locProjectHeaderXPO != null)
                                            {
                                                XPCollection<PrePurchaseOrder> _locPrePurchaseOrders = new XPCollection<PrePurchaseOrder>(_currSession,
                                                                                                    new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("ProjectHeader", _locProjectHeaderXPO),
                                                                                                    new BinaryOperator("Status", Status.Open)));

                                                if (_locPrePurchaseOrders != null && _locPrePurchaseOrders.Count() > 0)
                                                {
                                                    foreach (PrePurchaseOrder _locPrePurchaseOrder in _locPrePurchaseOrders)
                                                    {
                                                        _locPrePurchaseOrder.Status = Status.Progress;
                                                        _locPrePurchaseOrder.StatusDate = now;
                                                        _locPrePurchaseOrder.Save();
                                                        _locPrePurchaseOrder.Session.CommitTransaction();
                                                    }
                                                }
                                                SuccessMessageShow("Pre Purchase Order has been successfully updated to progress");
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Pre Pruchase Order Data Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Pre Pruchase Order Data Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PrePurchaseOrder " + ex.ToString());
            }
        }

        private void PrePurchaseOrderPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                string _currSignCode = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PrePurchaseOrder _locPrePurchaseOrderOS = (PrePurchaseOrder)_objectSpace.GetObject(obj);

                        if (_locPrePurchaseOrderOS != null)
                        {
                            if (_locPrePurchaseOrderOS.Code != null)
                            {
                                _currObjectId = _locPrePurchaseOrderOS.Code;

                                PrePurchaseOrder _locPrePurchaseOrderXPO = _currSession.FindObject<PrePurchaseOrder>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                #region PrePurchaseOrderXPO
                                if (_locPrePurchaseOrderXPO != null)
                                {

                                    XPQuery<PrePurchaseOrder> _prePurchaseOrdersQueryPH = new XPQuery<PrePurchaseOrder>(_currSession);
                                    XPQuery<PrePurchaseOrder> _prePurchaseOrdersQuery = new XPQuery<PrePurchaseOrder>(_currSession);

                                    var _prePurchaseOrderPHs = from ppph in _prePurchaseOrdersQueryPH
                                                               where (ppph.Select == true && (ppph.Status == Status.Progress || ppph.Status == Status.Posted))
                                                               group ppph by ppph.ProjectHeader into g
                                                               select new { ProjectHeader = g.Key };

                                    if (_prePurchaseOrderPHs != null && _prePurchaseOrderPHs.Count() > 0)
                                    {
                                        foreach (var _prePurchaseOrderPH in _prePurchaseOrderPHs)
                                        {
                                            var _prePurchaseOrders = from ppo in _prePurchaseOrdersQuery
                                                                     where (ppo.ProjectHeader == _prePurchaseOrderPH.ProjectHeader && ppo.Select == true && (ppo.Status == Status.Progress || ppo.Status == Status.Posted))
                                                                     group ppo by ppo.TransferType into g
                                                                     select new { TransferType = g.Key };

                                            if (_prePurchaseOrders != null && _prePurchaseOrders.Count() > 0)
                                            {
                                                foreach (var _prePurchaseOrder in _prePurchaseOrders)
                                                {
                                                    #region TransferTypeExternal
                                                    if (_prePurchaseOrder.TransferType == DirectionType.External)
                                                    {
                                                        _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.PurchaseOrder);
                                                        if (_currSignCode != null)
                                                        {
                                                            PurchaseOrder _saveDataPo = new PurchaseOrder(_currSession)
                                                            {
                                                                TransferType = DirectionType.External,
                                                                SignCode = _currSignCode,
                                                                Company = _locPrePurchaseOrderXPO.Company,
                                                                ProjectHeader = _prePurchaseOrderPH.ProjectHeader,
                                                            };
                                                            _saveDataPo.Save();
                                                            _saveDataPo.Session.CommitTransaction();

                                                            PurchaseOrder _locPurchaseOrder = _currSession.FindObject<PurchaseOrder>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("SignCode", _currSignCode)));

                                                            XPCollection<PrePurchaseOrder> _locPrePurchaseOrders = new XPCollection<PrePurchaseOrder>(_currSession,
                                                                                                                    new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("ProjectHeader", _prePurchaseOrderPH.ProjectHeader),
                                                                                                                    new BinaryOperator("TransferType", DirectionType.External),
                                                                                                                    new BinaryOperator("Select", true)));
                                                            if (_locPurchaseOrder != null)
                                                            {
                                                                if (_locPrePurchaseOrders != null && _locPrePurchaseOrders.Count > 0)
                                                                {

                                                                    foreach (PrePurchaseOrder _locPrePurchaseOrder in _locPrePurchaseOrders)
                                                                    {
                                                                        if (_locPrePurchaseOrder.Status == Status.Progress || _locPrePurchaseOrder.Status == Status.Posted)
                                                                        {

                                                                            PurchaseOrderLine _saveDataPol = new PurchaseOrderLine(_currSession)
                                                                            {
                                                                                PurchaseType = OrderType.Item,
                                                                                Item = _locPrePurchaseOrder.Item,
                                                                                MxDQty = _locPrePurchaseOrder.DQty,
                                                                                MxDUOM = _locPrePurchaseOrder.MxDUOM,
                                                                                MxQty = _locPrePurchaseOrder.Qty,
                                                                                MxUOM = _locPrePurchaseOrder.MxUOM,
                                                                                MxTQty = _locPrePurchaseOrder.TotalQty,
                                                                                DQty = _locPrePurchaseOrder.DQty,
                                                                                DUOM = _locPrePurchaseOrder.MxDUOM,
                                                                                Qty = _locPrePurchaseOrder.Qty,
                                                                                UOM = _locPrePurchaseOrder.MxUOM,
                                                                                TQty = _locPrePurchaseOrder.TotalQty,
                                                                                MxUAmount = _locPrePurchaseOrder.MxUAmount,
                                                                                MxTUAmount = _locPrePurchaseOrder.TotalQty * _locPrePurchaseOrder.MxUAmount,
                                                                                UAmount = _locPrePurchaseOrder.MxUAmount,
                                                                                TUAmount = _locPrePurchaseOrder.TotalQty * _locPrePurchaseOrder.MxUAmount,
                                                                                PurchaseOrder = _locPurchaseOrder,
                                                                                Company = _locPurchaseOrder.Company,
                                                                            };
                                                                            _saveDataPol.Save();
                                                                            _saveDataPol.Session.CommitTransaction();

                                                                            SetRemainQuantity(_currSession, _locPrePurchaseOrder);

                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    #endregion TransferTypeExternal

                                                    #region TransTypeRuleIndirect
                                                    if (_prePurchaseOrder.TransferType == DirectionType.Internal)
                                                    {
                                                        _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.PurchaseRequisition);
                                                        if (_currSignCode != null)
                                                        {
                                                            PurchaseRequisition _saveDataPo = new PurchaseRequisition(_currSession)
                                                            {
                                                                TransferType = DirectionType.Internal,
                                                                SignCode = _currSignCode,
                                                                Company = _locPrePurchaseOrderXPO.Company,
                                                                ProjectHeader = _prePurchaseOrderPH.ProjectHeader,
                                                            };
                                                            _saveDataPo.Save();
                                                            _saveDataPo.Session.CommitTransaction();

                                                            PurchaseRequisition _locPurchaseRequisition = _currSession.FindObject<PurchaseRequisition>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("SignCode", _currSignCode)));

                                                            XPCollection<PrePurchaseOrder> _locPrePurchaseOrders = new XPCollection<PrePurchaseOrder>(_currSession,
                                                                                                                    new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("ProjectHeader", _prePurchaseOrderPH.ProjectHeader),
                                                                                                                    new BinaryOperator("TransferType", DirectionType.Internal),
                                                                                                                    new BinaryOperator("Select", true)));
                                                            if (_locPurchaseRequisition != null)
                                                            {
                                                                if (_locPrePurchaseOrders != null && _locPrePurchaseOrders.Count > 0)
                                                                {
                                                                    foreach (PrePurchaseOrder _locPrePurchaseOrder in _locPrePurchaseOrders)
                                                                    {
                                                                        if (_locPrePurchaseOrder.Status == Status.Progress || _locPrePurchaseOrder.Status == Status.Posted)
                                                                        {
                                                                            PurchaseRequisitionLine _saveDataPol = new PurchaseRequisitionLine(_currSession)
                                                                            {
                                                                                PurchaseType = OrderType.Item,
                                                                                Item = _locPrePurchaseOrder.Item,
                                                                                MxDQty = _locPrePurchaseOrder.DQty,
                                                                                MxDUOM = _locPrePurchaseOrder.MxDUOM,
                                                                                MxQty = _locPrePurchaseOrder.Qty,
                                                                                MxUOM = _locPrePurchaseOrder.MxUOM,
                                                                                MxTQty = _locPrePurchaseOrder.TotalQty,
                                                                                MxUAmount = _locPrePurchaseOrder.MxUAmount,
                                                                                MxTUAmount = _locPrePurchaseOrder.TotalQty * _locPrePurchaseOrder.MxUAmount,
                                                                                DQty = _locPrePurchaseOrder.DQty,
                                                                                DUOM = _locPrePurchaseOrder.MxDUOM,
                                                                                Qty = _locPrePurchaseOrder.Qty,
                                                                                UOM = _locPrePurchaseOrder.MxUOM,
                                                                                TQty = _locPrePurchaseOrder.TotalQty,
                                                                                PurchaseRequisition = _locPurchaseRequisition,
                                                                                Company = _locPurchaseRequisition.Company,
                                                                            };
                                                                            _saveDataPol.Save();
                                                                            _saveDataPol.Session.CommitTransaction();

                                                                            SetRemainQuantity(_currSession, _locPrePurchaseOrder);

                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    #endregion TransTypeRuleIndirect
                                                }
                                                SuccessMessageShow("PrePurchase Order Successfully Posted");
                                            }
                                        }
                                    }
                                }
                                #endregion PrePurchaseOrderXPO

                            }
                            ErrorMessageShow("Project Header Not Available");
                        }
                        ErrorMessageShow("Project Header Not Available");
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Controller = PrePurchaseOrder" + ex.ToString());
            }
        }

        private void PrePurchaseOrderListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(PrePurchaseOrder)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PrePurchaseOrder " + ex.ToString());
            }
        }

        #region Global Method

        private void SetRemainQuantity(Session _currSession, PrePurchaseOrder _locPrePurchaseOrder)
        {
            try
            {
                DateTime now = DateTime.Now;

                #region Remain Qty  with PostedCount != 0
                if (_locPrePurchaseOrder.PostedCount > 0)
                {
                    double _locRmDqty = 0;
                    double _locRmQty = 0;
                    double _locInvLineTotal = 0;
                    double _locRmTQty = 0;
                    bool _locActivationPosting = false;
                    Status _locStatus = Status.Posted;

                    if (_locPrePurchaseOrder.RmDQty > 0 || _locPrePurchaseOrder.RmQty > 0 && _locPrePurchaseOrder.MxTQty != _locPrePurchaseOrder.RmTQty)
                    {
                        _locRmDqty = _locPrePurchaseOrder.RmDQty - _locPrePurchaseOrder.DQty;
                        _locRmQty = _locPrePurchaseOrder.RmQty - _locPrePurchaseOrder.Qty;

                        if (_locPrePurchaseOrder.Item != null && _locPrePurchaseOrder.MxUOM != null && _locPrePurchaseOrder.MxDUOM != null)
                        {
                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("Item", _locPrePurchaseOrder.Item),
                                                     new BinaryOperator("UOM", _locPrePurchaseOrder.MxUOM),
                                                     new BinaryOperator("DefaultUOM", _locPrePurchaseOrder.MxDUOM),
                                                     new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDqty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDqty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDqty;
                                }
                            }
                        }
                        else
                        {
                            _locInvLineTotal = _locRmQty + _locRmDqty;
                        }

                        if (_locRmDqty > 0 || _locRmQty > 0)
                        {
                            _locRmTQty = _locPrePurchaseOrder.RmTQty - _locPrePurchaseOrder.TotalQty;
                        }
                        else
                        {
                            _locRmTQty = _locPrePurchaseOrder.RmTQty - _locPrePurchaseOrder.TotalQty;
                            _locActivationPosting = true;
                            _locStatus = Status.Lock;
                        }


                        _locPrePurchaseOrder.ActivationPosting = _locActivationPosting;
                        _locPrePurchaseOrder.Select = false;
                        _locPrePurchaseOrder.RmDQty = _locRmDqty;
                        _locPrePurchaseOrder.RmQty = _locRmQty;
                        _locPrePurchaseOrder.RmTQty = _locRmTQty;
                        _locPrePurchaseOrder.PostedCount = _locPrePurchaseOrder.PostedCount + 1;
                        _locPrePurchaseOrder.DQty = 0;
                        _locPrePurchaseOrder.Qty = 0;
                        _locPrePurchaseOrder.TotalQty = 0;
                        _locPrePurchaseOrder.Status = _locStatus;
                        _locPrePurchaseOrder.StatusDate = now;
                        _locPrePurchaseOrder.Save();
                        _locPrePurchaseOrder.Session.CommitTransaction();
                    }
                }
                #endregion Remain Qty  with PostedCount != 0

                #region Remain Qty with PostedCount == 0
                else
                {
                    double _locRmDqty = 0;
                    double _locRmQty = 0;
                    double _locInvLineTotal = 0;
                    double _locRmTQty = 0;
                    _locRmDqty = _locPrePurchaseOrder.MxDQty - _locPrePurchaseOrder.DQty;
                    _locRmQty = _locPrePurchaseOrder.MxQty - _locPrePurchaseOrder.Qty;

                    #region Remain > 0
                    if (_locRmDqty > 0 || _locRmQty > 0)
                    {
                        if (_locPrePurchaseOrder.Item != null && _locPrePurchaseOrder.MxUOM != null && _locPrePurchaseOrder.MxDUOM != null)
                        {
                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("Item", _locPrePurchaseOrder.Item),
                                                     new BinaryOperator("UOM", _locPrePurchaseOrder.MxUOM),
                                                     new BinaryOperator("DefaultUOM", _locPrePurchaseOrder.MxDUOM),
                                                     new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDqty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDqty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDqty;
                                }
                            }
                        }
                        else
                        {
                            _locInvLineTotal = _locRmQty + _locRmDqty;
                        }

                        if (_locInvLineTotal > 0)
                        {
                            if (_locPrePurchaseOrder.RmTQty > 0)
                            {
                                _locRmTQty = _locPrePurchaseOrder.RmTQty - _locInvLineTotal;
                            }
                            else
                            {
                                _locRmTQty = _locInvLineTotal;
                            }

                        }

                        _locPrePurchaseOrder.RmDQty = _locRmDqty;
                        _locPrePurchaseOrder.RmQty = _locRmQty;
                        _locPrePurchaseOrder.RmTQty = _locRmTQty;
                        _locPrePurchaseOrder.PostedCount = _locPrePurchaseOrder.PostedCount + 1;
                        _locPrePurchaseOrder.Select = false;
                        _locPrePurchaseOrder.DQty = 0;
                        _locPrePurchaseOrder.Qty = 0;
                        _locPrePurchaseOrder.TotalQty = 0;
                        _locPrePurchaseOrder.Status = Status.Posted;
                        _locPrePurchaseOrder.StatusDate = now;
                        _locPrePurchaseOrder.Save();
                        _locPrePurchaseOrder.Session.CommitTransaction();
                    }
                    #endregion Remain > 0

                    #region Remain <= 0
                    if (_locRmDqty <= 0 && _locRmQty <= 0)
                    {
                        _locPrePurchaseOrder.ActivationPosting = true;
                        _locPrePurchaseOrder.RmDQty = 0;
                        _locPrePurchaseOrder.RmQty = 0;
                        _locPrePurchaseOrder.RmTQty = 0;
                        _locPrePurchaseOrder.PostedCount = _locPrePurchaseOrder.PostedCount + 1;
                        _locPrePurchaseOrder.Select = false;
                        _locPrePurchaseOrder.DQty = 0;
                        _locPrePurchaseOrder.Qty = 0;
                        _locPrePurchaseOrder.TotalQty = 0;
                        _locPrePurchaseOrder.Status = Status.Lock;
                        _locPrePurchaseOrder.StatusDate = now;
                        _locPrePurchaseOrder.Save();
                        _locPrePurchaseOrder.Session.CommitTransaction();
                    }
                    #endregion Remain < 0
                }
                #endregion Remain Qty with PostedCount == 0

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PrePurchaseOrder " + ex.ToString());
            }
        }

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

    }
}
