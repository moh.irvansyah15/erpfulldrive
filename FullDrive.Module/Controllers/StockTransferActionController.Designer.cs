﻿namespace FullDrive.Module.Controllers
{
    partial class StockTransferActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.StockTransferProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.StockTransferConfirmAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.StockTransferPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.StockTransferListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // StockTransferProgressAction
            // 
            this.StockTransferProgressAction.Caption = "Progress";
            this.StockTransferProgressAction.ConfirmationMessage = null;
            this.StockTransferProgressAction.Id = "StockTransferProgressActionId";
            this.StockTransferProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.StockTransfer);
            this.StockTransferProgressAction.ToolTip = null;
            this.StockTransferProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.StockTransferProgressAction_Execute);
            // 
            // StockTransferConfirmAction
            // 
            this.StockTransferConfirmAction.Caption = "Confirm";
            this.StockTransferConfirmAction.ConfirmationMessage = null;
            this.StockTransferConfirmAction.Id = "StockTransferConfirmActionId";
            this.StockTransferConfirmAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.StockTransfer);
            this.StockTransferConfirmAction.ToolTip = null;
            this.StockTransferConfirmAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.StockTransferConfirmAction_Execute);
            // 
            // StockTransferPostingAction
            // 
            this.StockTransferPostingAction.Caption = "Posting";
            this.StockTransferPostingAction.ConfirmationMessage = null;
            this.StockTransferPostingAction.Id = "StockTransferPostingActionId";
            this.StockTransferPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.StockTransfer);
            this.StockTransferPostingAction.ToolTip = null;
            this.StockTransferPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.StockTransferPostingAction_Execute);
            // 
            // StockTransferListviewFilterSelectionAction
            // 
            this.StockTransferListviewFilterSelectionAction.Caption = "Filter";
            this.StockTransferListviewFilterSelectionAction.ConfirmationMessage = null;
            this.StockTransferListviewFilterSelectionAction.Id = "StockTransferListviewFilterSelectionActionId";
            this.StockTransferListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.StockTransferListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.StockTransfer);
            this.StockTransferListviewFilterSelectionAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.StockTransferListviewFilterSelectionAction.ToolTip = null;
            this.StockTransferListviewFilterSelectionAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.StockTransferListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.StockTransferListviewFilterSelectionAction_Execute);
            // 
            // StockTransferActionController
            // 
            this.Actions.Add(this.StockTransferProgressAction);
            this.Actions.Add(this.StockTransferConfirmAction);
            this.Actions.Add(this.StockTransferPostingAction);
            this.Actions.Add(this.StockTransferListviewFilterSelectionAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction StockTransferProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction StockTransferConfirmAction;
        private DevExpress.ExpressApp.Actions.SimpleAction StockTransferPostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction StockTransferListviewFilterSelectionAction;
    }
}
