﻿namespace FullDrive.Module.Controllers
{
    partial class ProjectHeaderActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ProjectHeaderProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ProjectHeaderLockAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ProjectHeaderPrePOAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ProjectHeaderPreSOAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ProjectHeaderListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // ProjectHeaderProgressAction
            // 
            this.ProjectHeaderProgressAction.Caption = "Progress";
            this.ProjectHeaderProgressAction.ConfirmationMessage = null;
            this.ProjectHeaderProgressAction.Id = "ProjectHeaderProgressActionId";
            this.ProjectHeaderProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ProjectHeader);
            this.ProjectHeaderProgressAction.ToolTip = null;
            this.ProjectHeaderProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ProjectHeaderProgressAction_Execute);
            // 
            // ProjectHeaderLockAction
            // 
            this.ProjectHeaderLockAction.Caption = "Lock";
            this.ProjectHeaderLockAction.ConfirmationMessage = null;
            this.ProjectHeaderLockAction.Id = "ProjectHeaderLockActionId";
            this.ProjectHeaderLockAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ProjectHeader);
            this.ProjectHeaderLockAction.ToolTip = null;
            this.ProjectHeaderLockAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ProjectHeaderLockAction_Execute);
            // 
            // ProjectHeaderPrePOAction
            // 
            this.ProjectHeaderPrePOAction.Caption = "Pre PO Posting";
            this.ProjectHeaderPrePOAction.ConfirmationMessage = null;
            this.ProjectHeaderPrePOAction.Id = "ProjectHeaderPrePOActionId";
            this.ProjectHeaderPrePOAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ProjectHeader);
            this.ProjectHeaderPrePOAction.ToolTip = null;
            this.ProjectHeaderPrePOAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ProjectHeaderPrePOAction_Execute);
            // 
            // ProjectHeaderPreSOAction
            // 
            this.ProjectHeaderPreSOAction.Caption = "Pre SO Posting";
            this.ProjectHeaderPreSOAction.ConfirmationMessage = null;
            this.ProjectHeaderPreSOAction.Id = "ProjectHeaderPreSOActionId";
            this.ProjectHeaderPreSOAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ProjectHeader);
            this.ProjectHeaderPreSOAction.ToolTip = null;
            this.ProjectHeaderPreSOAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ProjectHeaderPreSOAction_Execute);
            // 
            // ProjectHeaderListviewFilterSelectionAction
            // 
            this.ProjectHeaderListviewFilterSelectionAction.Caption = "Filter";
            this.ProjectHeaderListviewFilterSelectionAction.ConfirmationMessage = null;
            this.ProjectHeaderListviewFilterSelectionAction.Id = "ProjectHeaderListviewFilterSelectionActionId";
            this.ProjectHeaderListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.ProjectHeaderListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ProjectHeader);
            this.ProjectHeaderListviewFilterSelectionAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.ProjectHeaderListviewFilterSelectionAction.ToolTip = null;
            this.ProjectHeaderListviewFilterSelectionAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.ProjectHeaderListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.ProjectHeaderListviewFilterSelectionAction_Execute);
            // 
            // ProjectHeaderActionController
            // 
            this.Actions.Add(this.ProjectHeaderProgressAction);
            this.Actions.Add(this.ProjectHeaderLockAction);
            this.Actions.Add(this.ProjectHeaderPrePOAction);
            this.Actions.Add(this.ProjectHeaderPreSOAction);
            this.Actions.Add(this.ProjectHeaderListviewFilterSelectionAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction ProjectHeaderProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction ProjectHeaderLockAction;
        private DevExpress.ExpressApp.Actions.SimpleAction ProjectHeaderPrePOAction;
        private DevExpress.ExpressApp.Actions.SimpleAction ProjectHeaderPreSOAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction ProjectHeaderListviewFilterSelectionAction;
    }
}
