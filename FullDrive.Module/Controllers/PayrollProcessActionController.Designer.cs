﻿namespace FullDrive.Module.Controllers
{
    partial class PayrollProcessActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CalculateWithTaxActionController = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PayrollProcessGetSalaryBasicActionController = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PayrollProcessCalculateTotalActionController = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PayrollProcessCalculateActionController = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // CalculateWithTaxActionController
            // 
            this.CalculateWithTaxActionController.Caption = "Calculate With Tax";
            this.CalculateWithTaxActionController.ConfirmationMessage = null;
            this.CalculateWithTaxActionController.Id = "CalculateWithTaxActionControllerId";
            this.CalculateWithTaxActionController.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PayrollProcess);
            this.CalculateWithTaxActionController.ToolTip = null;
            this.CalculateWithTaxActionController.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CalculateWithTaxActionController_Execute);
            // 
            // PayrollProcessGetSalaryBasicActionController
            // 
            this.PayrollProcessGetSalaryBasicActionController.Caption = "Get Salary Basic";
            this.PayrollProcessGetSalaryBasicActionController.ConfirmationMessage = null;
            this.PayrollProcessGetSalaryBasicActionController.Id = "PayrollProcessGetSalaryBasicActionControllerId";
            this.PayrollProcessGetSalaryBasicActionController.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PayrollProcess);
            this.PayrollProcessGetSalaryBasicActionController.ToolTip = null;
            this.PayrollProcessGetSalaryBasicActionController.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PayrollProcessGetSalaryBasicActionController_Execute);
            // 
            // PayrollProcessCalculateTotalActionController
            // 
            this.PayrollProcessCalculateTotalActionController.Caption = "Calculate Total";
            this.PayrollProcessCalculateTotalActionController.ConfirmationMessage = null;
            this.PayrollProcessCalculateTotalActionController.Id = "PayrollProcessCalculateTotalActionControllerId";
            this.PayrollProcessCalculateTotalActionController.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PayrollProcess);
            this.PayrollProcessCalculateTotalActionController.ToolTip = null;
            this.PayrollProcessCalculateTotalActionController.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PayrollProcessCalculateTotalActionController_Execute);
            // 
            // PayrollProcessCalculateActionController
            // 
            this.PayrollProcessCalculateActionController.Caption = "Calculate";
            this.PayrollProcessCalculateActionController.ConfirmationMessage = null;
            this.PayrollProcessCalculateActionController.Id = "PayrollProcessCalculateActionControllerId";
            this.PayrollProcessCalculateActionController.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PayrollProcess);
            this.PayrollProcessCalculateActionController.ToolTip = null;
            this.PayrollProcessCalculateActionController.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PayrollProcessCalculateActionController_Execute);
            // 
            // PayrollProcessActionController
            // 
            this.Actions.Add(this.CalculateWithTaxActionController);
            this.Actions.Add(this.PayrollProcessGetSalaryBasicActionController);
            this.Actions.Add(this.PayrollProcessCalculateTotalActionController);
            this.Actions.Add(this.PayrollProcessCalculateActionController);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction CalculateWithTaxActionController;
        private DevExpress.ExpressApp.Actions.SimpleAction PayrollProcessGetSalaryBasicActionController;
        private DevExpress.ExpressApp.Actions.SimpleAction PayrollProcessCalculateTotalActionController;
        private DevExpress.ExpressApp.Actions.SimpleAction PayrollProcessCalculateActionController;
    }
}
