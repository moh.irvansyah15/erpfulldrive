﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class TempBeginingInventoryLineActionController : ViewController
    {
        public TempBeginingInventoryLineActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void TempBeginingInventoryLineProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        TempBeginingInventoryLine _locTempBegInvLineOS = (TempBeginingInventoryLine)_objectSpace.GetObject(obj);

                        if (_locTempBegInvLineOS != null)
                        {
                            if (_locTempBegInvLineOS.Code != null)
                            {
                                _currObjectId = _locTempBegInvLineOS.Code;

                                TempBeginingInventoryLine _locTempBegInvLineXPO = _currSession.FindObject<TempBeginingInventoryLine>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locTempBegInvLineXPO != null)
                                {
                                    if (_locTempBegInvLineXPO.Status == Status.Open || _locTempBegInvLineXPO.Status == Status.Progress)
                                    {
                                        if (_locTempBegInvLineXPO.Status == Status.Open)
                                        {
                                            _locTempBegInvLineXPO.Status = Status.Progress;
                                            _locTempBegInvLineXPO.StatusDate = now;
                                            _locTempBegInvLineXPO.Save();
                                            _locTempBegInvLineXPO.Session.CommitTransaction();
                                        }  
                                    }
                                    SuccessMessageShow("Purchase Order has been successfully updated to progress");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Order Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data Purchase Order Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void TempBeginingInventoryLinePostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                ItemAccountGroup _itemAcctGroup = null;
                ItemGroup _itemGroup = null;
                ItemType _itemType = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        TempBeginingInventoryLine _locTempBegInvLineOS = (TempBeginingInventoryLine)_objectSpace.GetObject(obj);

                        if (_locTempBegInvLineOS != null)
                        {
                            if (_locTempBegInvLineOS.Code != null)
                            {
                                _currObjectId = _locTempBegInvLineOS.Code;

                                TempBeginingInventoryLine _locTempBegInvLineXPO = _currSession.FindObject<TempBeginingInventoryLine>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locTempBegInvLineXPO != null)
                                {
                                    if(_locTempBegInvLineXPO.Status == Status.Progress)
                                    {
                                        if (_locTempBegInvLineXPO.ItemTemp != null)
                                        {
                                            Item _locItem = _currSession.FindObject<Item>(new GroupOperator(GroupOperatorType.And,
                                                                                          new BinaryOperator("Name", _locTempBegInvLineXPO.ItemTemp)));
                                            #region ItemAvailable
                                            if (_locItem != null)
                                            {
                                                if(_locItem.ShortName != _locTempBegInvLineXPO.CodeProduct)
                                                {
                                                    _locItem.ShortName = _locTempBegInvLineXPO.CodeProduct;
                                                    _locItem.Save();
                                                    _locItem.Session.CommitTransaction();
                                                }
                                                
                                                BeginingInventory _locBegInv = _currSession.FindObject<BeginingInventory>(new GroupOperator(
                                                                                    GroupOperatorType.And,
                                                                                    new BinaryOperator("Item", _locItem)));
                                                
                                                if (_locBegInv != null)
                                                {
                                                    BeginingInventoryLine _locBegInvLine = _currSession.FindObject<BeginingInventoryLine>
                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("BeginingInventory", _locBegInv),
                                                                                            new BinaryOperator("Item", _locItem),
                                                                                            new BinaryOperator("LotNumber", _locTempBegInvLineXPO.LotNumber)
                                                                                            ));
                                                    if(_locBegInvLine != null)
                                                    {
                                                        if(_locBegInvLine.QtyAvailable > _locTempBegInvLineXPO.QtyAvailable)
                                                        {
                                                            _locBegInv.QtyAvailable = _locBegInv.QtyAvailable + _locTempBegInvLineXPO.QtyAvailable;
                                                            _locBegInv.QtySale = _locBegInv.QtySale + (_locBegInvLine.QtyAvailable - _locTempBegInvLineXPO.QtyAvailable);
                                                            _locBegInv.Save();
                                                            _locBegInv.Session.CommitTransaction();
                                                        }
                                                        _locBegInvLine.QtyAvailable = _locTempBegInvLineXPO.QtyAvailable;
                                                        _locBegInvLine.Save();
                                                        _locBegInvLine.Session.CommitTransaction();
                                                    }
                                                    else
                                                    {
                                                        BeginingInventoryLine _saveDataBegInvLine = new BeginingInventoryLine(_currSession)
                                                        {
                                                            Item = _locItem,
                                                            Brand = _locTempBegInvLineXPO.Brand,
                                                            Location = _locTempBegInvLineXPO.Location,
                                                            UnitPack = _locTempBegInvLineXPO.UnitPack,
                                                            QtyAvailable = _locTempBegInvLineXPO.QtyAvailable,
                                                            DefaultUOM = _locTempBegInvLineXPO.DefaultUOM,
                                                            StockType = _locTempBegInvLineXPO.StockType,
                                                            LotNumber = _locTempBegInvLineXPO.LotNumber,
                                                            Company = _locTempBegInvLineXPO.Company,
                                                            Active = true,
                                                            BeginingInventory = _locBegInv,
                                                        };
                                                        _saveDataBegInvLine.Save();
                                                        _saveDataBegInvLine.Session.CommitTransaction();

                                                        _locBegInv.QtyAvailable = _locBegInv.QtyAvailable + _locTempBegInvLineXPO.QtyAvailable;
                                                        _locBegInv.Save();
                                                        _locBegInv.Session.CommitTransaction();
                                                    }
                                                    
                                                }else
                                                {
                                                    BeginingInventory _saveDateBegInv = new BeginingInventory(_currSession)
                                                    {
                                                        Item = _locItem,
                                                        DefaultUOM = _locTempBegInvLineXPO.DefaultUOM,
                                                        Brand = _locTempBegInvLineXPO.Brand,
                                                        Company = _locTempBegInvLineXPO.Company,
                                                        Active = true,
                                                    };
                                                    _saveDateBegInv.Save();
                                                    _saveDateBegInv.Session.CommitTransaction();

                                                    BeginingInventory _locBegInv2 = _currSession.FindObject<BeginingInventory>(new GroupOperator(
                                                                                    GroupOperatorType.And,
                                                                                    new BinaryOperator("Item", _locItem)));
                                                    if(_locBegInv2 != null)
                                                    {
                                                        BeginingInventoryLine _saveDataBegInvLine = new BeginingInventoryLine(_currSession)
                                                        {
                                                            Item = _locItem,
                                                            Brand = _locTempBegInvLineXPO.Brand,
                                                            Location = _locTempBegInvLineXPO.Location,
                                                            UnitPack = _locTempBegInvLineXPO.UnitPack,
                                                            QtyAvailable = _locTempBegInvLineXPO.QtyAvailable,
                                                            DefaultUOM = _locTempBegInvLineXPO.DefaultUOM,
                                                            StockType = _locTempBegInvLineXPO.StockType,
                                                            LotNumber = _locTempBegInvLineXPO.LotNumber,
                                                            Company = _locTempBegInvLineXPO.Company,
                                                            Active = true,
                                                            BeginingInventory = _locBegInv2,
                                                        };
                                                        _saveDataBegInvLine.Save();
                                                        _saveDataBegInvLine.Session.CommitTransaction();

                                                        _locBegInv.QtyAvailable = _locBegInv.QtyAvailable + _locTempBegInvLineXPO.QtyAvailable;
                                                        _locBegInv.Save();
                                                        _locBegInv.Session.CommitTransaction();
                                                    }
                                                }

                                                _locTempBegInvLineXPO.Status = Status.Close;
                                                _locTempBegInvLineXPO.StatusDate = _locTempBegInvLineXPO.StatusDate;
                                                _locTempBegInvLineXPO.Save();
                                                _locTempBegInvLineXPO.Session.CommitTransaction();

                                            }
                                            #endregion ItemAvailable
                                            #region ItemUnavailable
                                            else
                                            {
                                                ItemAccountGroup _locItemAccoutGroup = _currSession.FindObject<ItemAccountGroup>(new GroupOperator(
                                                                                        GroupOperatorType.And,
                                                                                        new BinaryOperator("Name", "ITM Dalam Negeri")));
                                                if (_locItemAccoutGroup != null)
                                                {
                                                    _itemAcctGroup = _locItemAccoutGroup;
                                                }

                                                ItemGroup _locItemGroup = _currSession.FindObject<ItemGroup>(new GroupOperator(
                                                                                        GroupOperatorType.And,
                                                                                        new BinaryOperator("Name", "Barang Jadi")));
                                                if (_locItemGroup != null)
                                                {
                                                    _itemGroup = _locItemGroup;
                                                }

                                                ItemType _locItemType = _currSession.FindObject<ItemType>(new GroupOperator(
                                                                                        GroupOperatorType.And,
                                                                                        new BinaryOperator("Name", "Finish Good")));
                                                if (_locItemType != null)
                                                {
                                                    _itemType = _locItemType;
                                                }

                                                Item _saveDataItem = new Item(_currSession)
                                                {
                                                    ShortName = _locTempBegInvLineXPO.CodeProduct,
                                                    Name = _locTempBegInvLineXPO.ItemTemp,
                                                    BasedUOM = _locTempBegInvLineXPO.DefaultUOM,
                                                    ItemGroup = _itemGroup,
                                                    ItemType = _itemType,
                                                    ItemAccountGroup = _itemAcctGroup,
                                                    Brand = _locTempBegInvLineXPO.Brand,
                                                    OrderType = OrderType.Item,
                                                    Company = _locTempBegInvLineXPO.Company,
                                                    Active = true,
                                                };
                                                _saveDataItem.Save();
                                                _saveDataItem.Session.CommitTransaction();

                                                Item _locItem3 = _currSession.FindObject<Item>(new GroupOperator(
                                                                                        GroupOperatorType.And,
                                                                                        new BinaryOperator("ShortName", _locTempBegInvLineXPO.CodeProduct)));
                                                if(_locItem3 != null)
                                                {
                                                    BeginingInventory _saveDateBegInv = new BeginingInventory(_currSession)
                                                    {
                                                        Item = _locItem3,
                                                        DefaultUOM = _locTempBegInvLineXPO.DefaultUOM,
                                                        Brand = _locTempBegInvLineXPO.Brand,
                                                        Company = _locTempBegInvLineXPO.Company,
                                                        Active = true,
                                                    };
                                                    _saveDateBegInv.Save();
                                                    _saveDateBegInv.Session.CommitTransaction();

                                                    BeginingInventory _locBegInv = _currSession.FindObject<BeginingInventory>(new GroupOperator(
                                                                                        GroupOperatorType.And,
                                                                                        new BinaryOperator("Item", _locItem3)));
                                                    if(_locBegInv != null)
                                                    {
                                                        BeginingInventoryLine _saveDataBegInvLine = new BeginingInventoryLine(_currSession)
                                                        {
                                                            Item = _locItem3,
                                                            Brand = _locTempBegInvLineXPO.Brand,
                                                            Location = _locTempBegInvLineXPO.Location,
                                                            UnitPack = _locTempBegInvLineXPO.UnitPack,
                                                            QtyAvailable = _locTempBegInvLineXPO.QtyAvailable,
                                                            DefaultUOM = _locTempBegInvLineXPO.DefaultUOM,
                                                            StockType = _locTempBegInvLineXPO.StockType,
                                                            LotNumber = _locTempBegInvLineXPO.LotNumber,
                                                            Company = _locTempBegInvLineXPO.Company,
                                                            Active = true,
                                                            BeginingInventory = _locBegInv,
                                                        };
                                                        _saveDataBegInvLine.Save();
                                                        _saveDataBegInvLine.Session.CommitTransaction();

                                                        _locBegInv.QtyAvailable = _locBegInv.QtyAvailable + _locTempBegInvLineXPO.QtyAvailable;
                                                        _locBegInv.Save();
                                                        _locBegInv.Session.CommitTransaction();
                                                    }
                                                }

                                                _locTempBegInvLineXPO.Status = Status.Close;
                                                _locTempBegInvLineXPO.StatusDate = _locTempBegInvLineXPO.StatusDate;
                                                _locTempBegInvLineXPO.Save();
                                                _locTempBegInvLineXPO.Session.CommitTransaction();
                                            }
                                            #endregion ItemUnavailable
                                        }
                                    }
                                    

                                    SuccessMessageShow("Purchase Order has been successfully post");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Order Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Purchase Order Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

        
    }
}
