﻿namespace FullDrive.Module.Controllers
{
    partial class TransferInActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TransferInProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.TransferInPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.TransferInListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // TransferInProgressAction
            // 
            this.TransferInProgressAction.Caption = "Progress";
            this.TransferInProgressAction.ConfirmationMessage = null;
            this.TransferInProgressAction.Id = "TransferInProgressActionId";
            this.TransferInProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.TransferIn);
            this.TransferInProgressAction.ToolTip = null;
            this.TransferInProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.TransferInProgressAction_Execute);
            // 
            // TransferInPostingAction
            // 
            this.TransferInPostingAction.Caption = "Posting";
            this.TransferInPostingAction.ConfirmationMessage = null;
            this.TransferInPostingAction.Id = "TransferInPostingActionId";
            this.TransferInPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.TransferIn);
            this.TransferInPostingAction.ToolTip = null;
            this.TransferInPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.TransferInPostingAction_Execute);
            // 
            // TransferInListviewFilterSelectionAction
            // 
            this.TransferInListviewFilterSelectionAction.Caption = "Filter";
            this.TransferInListviewFilterSelectionAction.ConfirmationMessage = null;
            this.TransferInListviewFilterSelectionAction.Id = "TransferInListviewFilterSelectionActionId";
            this.TransferInListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.TransferInListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.TransferIn);
            this.TransferInListviewFilterSelectionAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.TransferInListviewFilterSelectionAction.ToolTip = null;
            this.TransferInListviewFilterSelectionAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.TransferInListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.TransferInListviewFilterSelectionAction_Execute);
            // 
            // TransferInActionController
            // 
            this.Actions.Add(this.TransferInProgressAction);
            this.Actions.Add(this.TransferInPostingAction);
            this.Actions.Add(this.TransferInListviewFilterSelectionAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction TransferInProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction TransferInPostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction TransferInListviewFilterSelectionAction;
    }
}
