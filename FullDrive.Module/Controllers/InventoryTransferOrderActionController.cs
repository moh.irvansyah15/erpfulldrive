﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class InventoryTransferOrderActionController : ViewController
    {
        #region Default

        private ChoiceActionItem _selectionListviewFilter;
        public InventoryTransferOrderActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.

            #region FilterStatus
            InventoryTransferOrderListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {

                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                InventoryTransferOrderListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterStatus

            #region FilterApproval
            InventoryTransferOrderListviewFilterApprovalSelectionAction.Items.Clear();
            foreach (object _currApprovalFilter in Enum.GetValues(typeof(CustomProcess.ApprovalFilter)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalFilter));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApprovalFilter), _currApprovalFilter);
                InventoryTransferOrderListviewFilterApprovalSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterApproval
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        private void InventoryTransferOrderProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Status _locStatus = Status.None;
                DateTime _locNow;
                Status _locStatus2 = Status.None;
                DateTime _locNow2;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        InventoryTransferOrder _locInventoryTransferOrderOS = (InventoryTransferOrder)_objectSpace.GetObject(obj);

                        if (_locInventoryTransferOrderOS != null)
                        {
                            if (_locInventoryTransferOrderOS.Code != null)
                            {
                                _currObjectId = _locInventoryTransferOrderOS.Code;

                                InventoryTransferOrder _locInventoryTransferOrderXPO = _currSession.FindObject<InventoryTransferOrder>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locInventoryTransferOrderXPO != null)
                                {
                                    if (_locInventoryTransferOrderXPO.Status == Status.Open || _locInventoryTransferOrderXPO.Status == Status.Progress)
                                    {
                                        if (_locInventoryTransferOrderXPO.Status == Status.Open)
                                        {
                                            _locStatus = Status.Progress;
                                            _locNow = now;
                                        }
                                        else
                                        {
                                            _locStatus = _locInventoryTransferOrderXPO.Status;
                                            _locNow = _locInventoryTransferOrderXPO.StatusDate;
                                        }

                                        _locInventoryTransferOrderXPO.Status = _locStatus;
                                        _locInventoryTransferOrderXPO.StatusDate = _locNow;
                                        _locInventoryTransferOrderXPO.Save();
                                        _locInventoryTransferOrderXPO.Session.CommitTransaction();

                                        XPCollection<InventoryTransferOrderLine> _locInventoryTransferOrderLines = new XPCollection<InventoryTransferOrderLine>
                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("InventoryTransferOrder", _locInventoryTransferOrderXPO)));

                                        if (_locInventoryTransferOrderLines != null && _locInventoryTransferOrderLines.Count > 0)
                                        {
                                            foreach (InventoryTransferOrderLine _locInventoryTransferOrderLine in _locInventoryTransferOrderLines)
                                            {
                                                if (_locInventoryTransferOrderLine.Status == Status.Open || _locInventoryTransferOrderLine.Status == Status.Progress)
                                                {
                                                    if (_locInventoryTransferOrderLine.Status == Status.Open)
                                                    {
                                                        _locStatus2 = Status.Progress;
                                                        _locNow2 = now;
                                                    }
                                                    else
                                                    {
                                                        _locStatus2 = _locInventoryTransferOrderLine.Status;
                                                        _locNow2 = _locInventoryTransferOrderLine.StatusDate;
                                                    }

                                                    _locInventoryTransferOrderLine.Status = _locStatus2;
                                                    _locInventoryTransferOrderLine.StatusDate = _locNow2;
                                                    _locInventoryTransferOrderLine.Save();
                                                    _locInventoryTransferOrderLine.Session.CommitTransaction();

                                                    XPCollection<InventoryTransferOrderLot> _locInventoryTransferOrderLots = new XPCollection<InventoryTransferOrderLot>
                                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("InventoryTransferOrderLine", _locInventoryTransferOrderLine),
                                                                                                            new BinaryOperator("Status", Status.Open)));

                                                    if (_locInventoryTransferOrderLots != null && _locInventoryTransferOrderLots.Count() > 0)
                                                    {
                                                        foreach (InventoryTransferOrderLot _locInventoryTransferOrderLot in _locInventoryTransferOrderLots)
                                                        {
                                                            _locInventoryTransferOrderLot.Status = Status.Progress;
                                                            _locInventoryTransferOrderLot.StatusDate = now;
                                                            _locInventoryTransferOrderLot.Save();
                                                            _locInventoryTransferOrderLot.Session.CommitTransaction();
                                                        }
                                                    }
                                                }                 
                                            }
                                        }
                                    }
                                    SuccessMessageShow("Inventory Transfer Order has been successfully updated to progress");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Inventory Transfer Order Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Inventory Transfer Order Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOrder " + ex.ToString());
            }
        }

        private void InventoryTransferOrderPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                var User = SecuritySystem.CurrentUserName;
                Session currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
                UserAccess _locUserAccess = currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        InventoryTransferOrder _locInventoryTransferOrderOS = (InventoryTransferOrder)_objectSpace.GetObject(obj);

                        if (_locInventoryTransferOrderOS != null)
                        {
                            if (_locInventoryTransferOrderOS.Code != null)
                            {
                                _currObjectId = _locInventoryTransferOrderOS.Code;

                                InventoryTransferOrder _locInventoryTransferOrderXPO = _currSession.FindObject<InventoryTransferOrder>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locInventoryTransferOrderXPO != null)
                                {
                                    if (_locInventoryTransferOrderXPO.Status == Status.Progress || _locInventoryTransferOrderXPO.Status == Status.Posted)
                                    {
                                        ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("EndApproval", true),
                                                                     new BinaryOperator("InventoryTransferOrder", _locInventoryTransferOrderXPO)));

                                        if (_locApprovalLineXPO != null)
                                        {
                                            if(_locInventoryTransferOrderXPO.Hold == true)
                                            {
                                                if(GetHoldAccess(_currSession, _locUserAccess) == true)
                                                {
                                                    if ((_locInventoryTransferOrderXPO.TransferType == DirectionType.Internal || _locInventoryTransferOrderXPO.TransferType == DirectionType.External)
                                                        && _locInventoryTransferOrderXPO.InventoryMovingType == InventoryMovingType.Deliver
                                                        && _locInventoryTransferOrderXPO.ObjectList == ObjectList.InventoryTransferOut)
                                                    {
                                                        if (CheckAvailableStock(_currSession, _locInventoryTransferOrderXPO) == true)
                                                        {
                                                            //Membuat Transfer Order
                                                            //SetTransferOrder(_currSession, _locInventoryTransferOrderXPO);
                                                            if (_locInventoryTransferOrderXPO.StockControlling == true)
                                                            {
                                                                SetInventoryTransferOutWithStockControlling(_currSession, _locInventoryTransferOrderXPO, _locUserAccess);
                                                            }
                                                            else
                                                            {
                                                                SetInventoryTransferOut(_currSession, _locInventoryTransferOrderXPO);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            ErrorMessageShow("Please check stock quantity");
                                                            break;
                                                        }
                                                    }

                                                    if ((_locInventoryTransferOrderXPO.TransferType == DirectionType.Internal || _locInventoryTransferOrderXPO.TransferType == DirectionType.External)
                                                        && _locInventoryTransferOrderXPO.InventoryMovingType == InventoryMovingType.Receive
                                                        && _locInventoryTransferOrderXPO.ObjectList == ObjectList.InventoryTransferIn)
                                                    {
                                                        SetInventoryTransferIn(_currSession, _locInventoryTransferOrderXPO);
                                                    }
                                                    SetRemainQty(_currSession, _locInventoryTransferOrderXPO);
                                                    SetPostingQty(_currSession, _locInventoryTransferOrderXPO);
                                                    SetProcessCount(_currSession, _locInventoryTransferOrderXPO);
                                                    SetStatusPostingInventoryTransferOrderLine(_currSession, _locInventoryTransferOrderXPO);
                                                    SetNormalPostingQuantity(_currSession, _locInventoryTransferOrderXPO);
                                                    SetFinalStatusPostingInventoryTransferOrder(_currSession, _locInventoryTransferOrderXPO);
                                                    SuccessMessageShow(_locInventoryTransferOrderXPO.Code + " has been change successfully to Posting");
                                                }
                                            }
                                            else
                                            {
                                                if ((_locInventoryTransferOrderXPO.TransferType == DirectionType.Internal || _locInventoryTransferOrderXPO.TransferType == DirectionType.External)
                                                        && _locInventoryTransferOrderXPO.InventoryMovingType == InventoryMovingType.Deliver
                                                        && _locInventoryTransferOrderXPO.ObjectList == ObjectList.InventoryTransferOut)
                                                {
                                                    if (CheckAvailableStock(_currSession, _locInventoryTransferOrderXPO) == true)
                                                    {
                                                        //Membuat Transfer Order
                                                        //SetTransferOrder(_currSession, _locInventoryTransferOrderXPO);
                                                        if (_locInventoryTransferOrderXPO.StockControlling == true)
                                                        {
                                                            SetInventoryTransferOutWithStockControlling(_currSession, _locInventoryTransferOrderXPO, _locUserAccess);
                                                        }
                                                        else
                                                        {
                                                            SetInventoryTransferOut(_currSession, _locInventoryTransferOrderXPO);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ErrorMessageShow("Please check stock quantity");
                                                        break;
                                                    }
                                                }

                                                if ((_locInventoryTransferOrderXPO.TransferType == DirectionType.Internal || _locInventoryTransferOrderXPO.TransferType == DirectionType.External)
                                                    && _locInventoryTransferOrderXPO.InventoryMovingType == InventoryMovingType.Receive
                                                    && _locInventoryTransferOrderXPO.ObjectList == ObjectList.InventoryTransferIn)
                                                {
                                                    SetInventoryTransferIn(_currSession, _locInventoryTransferOrderXPO);
                                                }
                                                SetRemainQty(_currSession, _locInventoryTransferOrderXPO);
                                                SetPostingQty(_currSession, _locInventoryTransferOrderXPO);
                                                SetProcessCount(_currSession, _locInventoryTransferOrderXPO);
                                                SetStatusPostingInventoryTransferOrderLine(_currSession, _locInventoryTransferOrderXPO);
                                                SetNormalPostingQuantity(_currSession, _locInventoryTransferOrderXPO);
                                                SetFinalStatusPostingInventoryTransferOrder(_currSession, _locInventoryTransferOrderXPO);
                                                SuccessMessageShow(_locInventoryTransferOrderXPO.Code + " has been change successfully to Posting");
                                            }
                                            
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Inventory Transfer Order Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Inventory Transfer Out Order Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOrder " + ex.ToString());
            }
        }

        private void InventoryTransferOrderListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(InventoryTransferOrder)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOrder " + ex.ToString());
            }
        }

        private void InventoryTransferOrderListviewFilterApprovalSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(InventoryTransferOrder)))
                {
                    if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.Approval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal)));
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.EndApproval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal);
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.AllData)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved1", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal));
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOrder " + ex.ToString());
            }
        }

        //==== Code Only ====

        #region InventoryTransferOrder

        //Cek ketersediaan stock
        private bool CheckAvailableStock(Session _currSession, InventoryTransferOrder _inventoryTransferOrderXPO)
        {
            bool _result = true;
            try
            {
                XPCollection<InventoryTransferOrderLine> _locInventoryTransOrderLines = new XPCollection<InventoryTransferOrderLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("InventoryTransferOrder", _inventoryTransferOrderXPO),
                                                                        new BinaryOperator("Select", true)));

                if (_locInventoryTransOrderLines != null && _locInventoryTransOrderLines.Count > 0)
                {
                    double _locTransOutLineTotal = 0;
                    BeginingInventory _locBegInventory = null;
                    XPCollection<BeginingInventoryLine> _locBegInvLines = null;
                    foreach (InventoryTransferOrderLine _locInventoryTransOrderLine in _locInventoryTransOrderLines)
                    {
                        XPCollection<InventoryTransferOrderLot> _locInventoryTransferOrderLots = new XPCollection<InventoryTransferOrderLot>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("InventoryTransferOrderLine", _locInventoryTransOrderLine)));

                        #region LotNumber
                        if (_locInventoryTransferOrderLots != null && _locInventoryTransferOrderLots.Count() > 0)
                        {
                            foreach (InventoryTransferOrderLot _locInventoryTransferOrderLot in _locInventoryTransferOrderLots)
                            {
                                _locBegInventory = _currSession.FindObject<BeginingInventory>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Item", _locInventoryTransferOrderLot.Item)));
                                if (_locBegInventory != null)
                                {
                                    if (_locInventoryTransferOrderLot.BegInvLine != null)
                                    {
                                        if (_locInventoryTransferOrderLot.BegInvLine.Code != null)
                                        {
                                            BeginingInventoryLine _locBegInvLine = _currSession.FindObject<BeginingInventoryLine>(
                                                                                        new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Code", _locInventoryTransferOrderLot.BegInvLine.Code)));
                                            if (_locBegInvLine != null)
                                            {
                                                if (_locInventoryTransferOrderLot.TQty > _locBegInvLine.QtyAvailable)
                                                {
                                                    _result = false;
                                                }
                                            }
                                            else
                                            {
                                                _result = false;
                                            }
                                        }
                                    }

                                }
                            }
                        }
                        #endregion LotNumber
                        #region NonLotNumber
                        else
                        {
                            if (_locInventoryTransOrderLine.LocationFrom != null || _locInventoryTransOrderLine.BinLocationFrom != null)
                            {
                                _locBegInventory = _currSession.FindObject<BeginingInventory>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Item", _locInventoryTransOrderLine.Item)));

                                if (_locBegInventory != null)
                                {
                                    if (_locInventoryTransOrderLine.LocationFrom != null && _locInventoryTransOrderLine.BinLocationFrom != null)
                                    {
                                        _locBegInvLines = new XPCollection<BeginingInventoryLine>(_currSession,
                                                          new GroupOperator(GroupOperatorType.And,
                                                          new BinaryOperator("BeginingInventory", _locBegInventory),
                                                          new BinaryOperator("Item", _locInventoryTransOrderLine.Item),
                                                          new BinaryOperator("Location", _locInventoryTransOrderLine.LocationFrom),
                                                          new BinaryOperator("BinLocation", _locInventoryTransOrderLine.BinLocationFrom),
                                                          new BinaryOperator("StockType", _locInventoryTransOrderLine.StockTypeFrom)));

                                    }
                                    else if (_locInventoryTransOrderLine.LocationFrom != null && _locInventoryTransOrderLine.BinLocationFrom == null)
                                    {
                                        _locBegInvLines = new XPCollection<BeginingInventoryLine>(_currSession,
                                                          new GroupOperator(GroupOperatorType.And,
                                                          new BinaryOperator("BeginingInventory", _locBegInventory),
                                                          new BinaryOperator("Item", _locInventoryTransOrderLine.Item),
                                                          new BinaryOperator("Location", _locInventoryTransOrderLine.LocationFrom),
                                                          new BinaryOperator("StockType", _locInventoryTransOrderLine.StockTypeFrom)));
                                    }


                                    if (_locBegInvLines != null && _locBegInvLines.Count() > 0)
                                    {
                                        foreach (BeginingInventoryLine _locBegInvLine in _locBegInvLines)
                                        {
                                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                             new BinaryOperator("Item", _locInventoryTransOrderLine.Item),
                                                             new BinaryOperator("UOM", _locInventoryTransOrderLine.UOM),
                                                             new BinaryOperator("DefaultUOM", _locInventoryTransOrderLine.DUOM),
                                                             new BinaryOperator("Active", true)));

                                            if (_locItemUOM != null)
                                            {
                                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                {
                                                    _locTransOutLineTotal = _locInventoryTransOrderLine.Qty * _locItemUOM.DefaultConversion + _locInventoryTransOrderLine.DQty;
                                                }
                                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                {
                                                    _locTransOutLineTotal = _locInventoryTransOrderLine.Qty / _locItemUOM.Conversion + _locInventoryTransOrderLine.DQty;
                                                }
                                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                {
                                                    _locTransOutLineTotal = _locInventoryTransOrderLine.Qty + _locInventoryTransOrderLine.DQty;
                                                }

                                                if (_locTransOutLineTotal > _locBegInventory.QtyAvailable)
                                                {
                                                    _result = false;
                                                }
                                            }
                                            else
                                            {
                                                _locTransOutLineTotal = _locInventoryTransOrderLine.Qty + _locInventoryTransOrderLine.DQty;

                                                if (_locTransOutLineTotal > _locBegInventory.QtyAvailable)
                                                {
                                                    _result = false;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        #endregion NonLotNumber  
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferOrder ", ex.ToString());
            }
            return _result;
        }

        //Menentukan sisa dari transaksi -> Done 10 Dec 2019
        private void SetRemainQty(Session _currSession, InventoryTransferOrder _locInventoryTransferOrderXPO)
        {
            try
            {
                if (_locInventoryTransferOrderXPO != null)
                {
                    XPCollection<InventoryTransferOrderLine> _locInventoryTransferOrderLines = new XPCollection<InventoryTransferOrderLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("InventoryTransferOrder", _locInventoryTransferOrderXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locInventoryTransferOrderLines != null && _locInventoryTransferOrderLines.Count > 0)
                    {
                        double _locRmDQty = 0;
                        double _locRmQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (InventoryTransferOrderLine _locInventoryTransferOrderLine in _locInventoryTransferOrderLines)
                        {
                            #region ProcessCount=0
                            if (_locInventoryTransferOrderLine.ProcessCount == 0)
                            {
                                #region MaxQuantity
                                if (_locInventoryTransferOrderLine.MxDQty > 0 || _locInventoryTransferOrderLine.MxQty > 0)
                                {
                                    if (_locInventoryTransferOrderLine.DQty > 0 && _locInventoryTransferOrderLine.DQty <= _locInventoryTransferOrderLine.MxDQty)
                                    {
                                        _locRmDQty = _locInventoryTransferOrderLine.MxDQty - _locInventoryTransferOrderLine.DQty;
                                    }

                                    if (_locInventoryTransferOrderLine.Qty > 0 && _locInventoryTransferOrderLine.Qty <= _locInventoryTransferOrderLine.MxQty)
                                    {
                                        _locRmQty = _locInventoryTransferOrderLine.MxQty - _locInventoryTransferOrderLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locInventoryTransferOrderLine.Item),
                                                                new BinaryOperator("UOM", _locInventoryTransferOrderLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locInventoryTransferOrderLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locInventoryTransferOrderLine.DQty > 0)
                                    {
                                        _locRmDQty = 0;
                                    }

                                    if (_locInventoryTransferOrderLine.Qty > 0)
                                    {
                                        _locRmQty = 0;
                                    }

                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locInventoryTransferOrderLine.Item),
                                                                new BinaryOperator("UOM", _locInventoryTransferOrderLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locInventoryTransferOrderLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locInventoryTransferOrderLine.ProcessCount > 0)
                            {
                                if (_locInventoryTransferOrderLine.RmDQty > 0)
                                {
                                    _locRmDQty = _locInventoryTransferOrderLine.RmDQty - _locInventoryTransferOrderLine.DQty;
                                }

                                if (_locInventoryTransferOrderLine.RmQty > 0)
                                {
                                    _locRmQty = _locInventoryTransferOrderLine.RmQty - _locInventoryTransferOrderLine.Qty;
                                }

                                if (_locInventoryTransferOrderLine.MxDQty > 0 || _locInventoryTransferOrderLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locInventoryTransferOrderLine.Item),
                                                                new BinaryOperator("UOM", _locInventoryTransferOrderLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locInventoryTransferOrderLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locInventoryTransferOrderLine.Item),
                                                                new BinaryOperator("UOM", _locInventoryTransferOrderLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locInventoryTransferOrderLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }

                            }
                            #endregion ProcessCount>0
                            _locInventoryTransferOrderLine.RmDQty = _locRmDQty;
                            _locInventoryTransferOrderLine.RmQty = _locRmQty;
                            _locInventoryTransferOrderLine.RmTQty = _locInvLineTotal;
                            _locInventoryTransferOrderLine.Save();
                            _locInventoryTransferOrderLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOrder " + ex.ToString());
            }

        }

        //Menentukan jumlah quantity yang telah di posting -> Done 10 Dec 2019
        private void SetPostingQty(Session _currSession, InventoryTransferOrder _inventoryTransferOrderXPO)
        {
            try
            {
                if (_inventoryTransferOrderXPO != null)
                {
                    XPCollection<InventoryTransferOrderLine> _locInventoryTransOrderLines = new XPCollection<InventoryTransferOrderLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("InventoryTransferOrder", _inventoryTransferOrderXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locInventoryTransOrderLines != null && _locInventoryTransOrderLines.Count > 0)
                    {
                        double _locPDQty = 0;
                        double _locPQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (InventoryTransferOrderLine _locInventoryTransOrderLine in _locInventoryTransOrderLines)
                        {
                            #region ProcessCount=0
                            if (_locInventoryTransOrderLine.ProcessCount == 0)
                            {
                                #region MaxQuantity
                                if (_locInventoryTransOrderLine.MxDQty > 0 || _locInventoryTransOrderLine.MxQty > 0)
                                {
                                    if (_locInventoryTransOrderLine.DQty > 0 && _locInventoryTransOrderLine.DQty <= _locInventoryTransOrderLine.MxDQty)
                                    {
                                        _locPDQty = _locInventoryTransOrderLine.DQty;
                                    }

                                    if (_locInventoryTransOrderLine.Qty > 0 && _locInventoryTransOrderLine.Qty <= _locInventoryTransOrderLine.MxQty)
                                    {
                                        _locPQty = _locInventoryTransOrderLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locInventoryTransOrderLine.Item),
                                                                new BinaryOperator("UOM", _locInventoryTransOrderLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locInventoryTransOrderLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locInventoryTransOrderLine.DQty > 0)
                                    {
                                        _locPDQty = _locInventoryTransOrderLine.DQty;
                                    }

                                    if (_locInventoryTransOrderLine.Qty > 0)
                                    {
                                        _locPQty = _locInventoryTransOrderLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locInventoryTransOrderLine.Item),
                                                                new BinaryOperator("UOM", _locInventoryTransOrderLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locInventoryTransOrderLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locInventoryTransOrderLine.ProcessCount > 0)
                            {
                                if (_locInventoryTransOrderLine.PDQty > 0)
                                {
                                    _locPDQty = _locInventoryTransOrderLine.PDQty + _locInventoryTransOrderLine.DQty;
                                }

                                if (_locInventoryTransOrderLine.PQty > 0)
                                {
                                    _locPQty = _locInventoryTransOrderLine.PQty + _locInventoryTransOrderLine.Qty;
                                }

                                if (_locInventoryTransOrderLine.MxDQty > 0 || _locInventoryTransOrderLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                 new BinaryOperator("Item", _locInventoryTransOrderLine.Item),
                                                                 new BinaryOperator("UOM", _locInventoryTransOrderLine.MxUOM),
                                                                 new BinaryOperator("DefaultUOM", _locInventoryTransOrderLine.MxDUOM),
                                                                 new BinaryOperator("Active", true)));

                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locInventoryTransOrderLine.Item),
                                                                new BinaryOperator("UOM", _locInventoryTransOrderLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locInventoryTransOrderLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                }


                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locInventoryTransOrderLine.PDQty = _locPDQty;
                            _locInventoryTransOrderLine.PDUOM = _locInventoryTransOrderLine.DUOM;
                            _locInventoryTransOrderLine.PQty = _locPQty;
                            _locInventoryTransOrderLine.PUOM = _locInventoryTransOrderLine.UOM;
                            _locInventoryTransOrderLine.PTQty = _locInvLineTotal;
                            _locInventoryTransOrderLine.Save();
                            _locInventoryTransOrderLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOrder " + ex.ToString());
            }
        }

        //Menentukan Banyak process posting
        private void SetProcessCount(Session _currSession, InventoryTransferOrder _locInventoryTransferOrderXPO)
        {
            try
            {
                if (_locInventoryTransferOrderXPO != null)
                {
                    XPCollection<InventoryTransferOrderLine> _locInventoryTransferOrderLines = new XPCollection<InventoryTransferOrderLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("InventoryTransferOrder", _locInventoryTransferOrderXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locInventoryTransferOrderLines != null && _locInventoryTransferOrderLines.Count > 0)
                    {

                        foreach (InventoryTransferOrderLine _locInventoryTransferOrderLine in _locInventoryTransferOrderLines)
                        {
                            if (_locInventoryTransferOrderLine.Status == Status.Progress || _locInventoryTransferOrderLine.Status == Status.Posted)
                            {
                                _locInventoryTransferOrderLine.ProcessCount = _locInventoryTransferOrderLine.ProcessCount + 1;
                                _locInventoryTransferOrderLine.Save();
                                _locInventoryTransferOrderLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOrder " + ex.ToString());
            }

        }

        //Membuat Inventory Transfer Out dengan setting Normal
        private void SetInventoryTransferOut(Session _currSession, InventoryTransferOrder _inventoryTransferOrderXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _locDocCode = null;
                string _locSignCodeITOL = null;
                ProjectHeader _locProjectHeader = null;
                SalesOrder _locSalesOrder = null;
                DocumentType _locDocumentTypeXPO = null;
                DocumentType _locDocumentType = null;

                if (_inventoryTransferOrderXPO != null)
                {
                    if (_inventoryTransferOrderXPO.ProjectHeader != null)
                    {
                        _locProjectHeader = _inventoryTransferOrderXPO.ProjectHeader;
                    }

                    if (_inventoryTransferOrderXPO.SalesOrder != null)
                    {
                        _locSalesOrder = _inventoryTransferOrderXPO.SalesOrder;
                    }

                    if (_inventoryTransferOrderXPO.DocumentType != null)
                    {
                        _locDocumentTypeXPO = _inventoryTransferOrderXPO.DocumentType;
                    }

                    _locDocumentType = _currSession.FindObject<DocumentType>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("TransferType", _inventoryTransferOrderXPO.TransferType),
                                                     new BinaryOperator("InventoryMovingType", InventoryMovingType.Deliver),
                                                     new BinaryOperator("ObjectList", ObjectList.InventoryTransferOut),
                                                     new BinaryOperator("Active", true)));

                    if (_locDocumentType != null)
                    {
                        if (_locDocumentTypeXPO != null && _locDocumentTypeXPO == _locDocumentType)
                        {

                            if (_inventoryTransferOrderXPO.DocNo != null)
                            {
                                _locDocCode = _inventoryTransferOrderXPO.DocNo;
                            }

                            if (_locDocCode != null)
                            {

                                InventoryTransferOut _locInventoryTransferOut = _currSession.FindObject<InventoryTransferOut>(new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("InventoryTransferOrder", _inventoryTransferOrderXPO),
                                                    new BinaryOperator("DocumentType", _locDocumentTypeXPO),
                                                    new BinaryOperator("DocNo", _locDocCode)));

                                if (_locInventoryTransferOut == null)
                                {
                                    InventoryTransferOut _saveDataITO = new InventoryTransferOut(_currSession)
                                    {
                                        TransferType = _inventoryTransferOrderXPO.TransferType,
                                        InventoryMovingType = InventoryMovingType.Deliver,
                                        ObjectList = ObjectList.InventoryTransferOut,
                                        DocumentType = _locDocumentTypeXPO,
                                        DocumentRule = _inventoryTransferOrderXPO.DocumentRule,
                                        BusinessPartner = _inventoryTransferOrderXPO.BusinessPartner,
                                        BusinessPartnerCountry = _inventoryTransferOrderXPO.BusinessPartnerCountry,
                                        BusinessPartnerCity = _inventoryTransferOrderXPO.BusinessPartnerCity,
                                        BusinessPartnerAddress = _inventoryTransferOrderXPO.BusinessPartnerAddress,
                                        EstimatedDate = _inventoryTransferOrderXPO.EstimatedDate,
                                        DocNo = _locDocCode,
                                        InventoryTransferOrder = _inventoryTransferOrderXPO,
                                        ProjectHeader = _locProjectHeader,
                                        Company = _inventoryTransferOrderXPO.Company,
                                    };
                                    _saveDataITO.Save();
                                    _saveDataITO.Session.CommitTransaction();

                                    InventoryTransferOut _locInventoryTransferOut2 = _currSession.FindObject<InventoryTransferOut>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("DocNo", _locDocCode),
                                                         new BinaryOperator("InventoryTransferOrder", _inventoryTransferOrderXPO)));

                                    if (_locInventoryTransferOut2 != null)
                                    {
                                        XPCollection<InventoryTransferOrderLine> _locInventoryTransOrderLines = new XPCollection<InventoryTransferOrderLine>(_currSession,
                                                                                            new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("InventoryTransferOrder", _inventoryTransferOrderXPO),
                                                                                            new BinaryOperator("Select", true)));

                                        if (_locInventoryTransOrderLines != null && _locInventoryTransOrderLines.Count > 0)
                                        {
                                            //PerTransferOutLine
                                            foreach (InventoryTransferOrderLine _locInventoryTransOrderLine in _locInventoryTransOrderLines)
                                            {

                                                if (_locInventoryTransOrderLine.Status == Status.Progress || _locInventoryTransOrderLine.Status == Status.Posted)
                                                {
                                                    _locSignCodeITOL = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.InventoryTransferOutLine);

                                                    if (_locSignCodeITOL != null)
                                                    {
                                                        InventoryTransferOutLine _saveDataInventoryTransferOutLine = new InventoryTransferOutLine(_currSession)
                                                        {
                                                            Select = true,
                                                            SignCode = _locSignCodeITOL,
                                                            Location = _locInventoryTransOrderLine.LocationFrom,
                                                            BinLocation = _locInventoryTransOrderLine.BinLocationFrom,
                                                            StockType = _locInventoryTransOrderLine.StockTypeFrom,
                                                            Item = _locInventoryTransOrderLine.Item,
                                                            MxDQty = _locInventoryTransOrderLine.DQty,
                                                            MxDUOM = _locInventoryTransOrderLine.DUOM,
                                                            MxQty = _locInventoryTransOrderLine.Qty,
                                                            MxUOM = _locInventoryTransOrderLine.UOM,
                                                            MxTQty = _locInventoryTransOrderLine.TQty,
                                                            DQty = _locInventoryTransOrderLine.DQty,
                                                            DUOM = _locInventoryTransOrderLine.DUOM,
                                                            Qty = _locInventoryTransOrderLine.Qty,
                                                            UOM = _locInventoryTransOrderLine.UOM,
                                                            TQty = _locInventoryTransOrderLine.TQty,
                                                            InventoryTransferOut = _locInventoryTransferOut2,
                                                            Company = _locInventoryTransferOut2.Company,
                                                        };
                                                        _saveDataInventoryTransferOutLine.Save();
                                                        _saveDataInventoryTransferOutLine.Session.CommitTransaction();

                                                        InventoryTransferOutLine _locInventoryTransferOutLine = _currSession.FindObject<InventoryTransferOutLine>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("SignCode", _locSignCodeITOL)));

                                                        if (_locInventoryTransferOutLine != null)
                                                        {
                                                            XPCollection<InventoryTransferOrderLot> _locInventoryTransferOrderLots = new XPCollection<InventoryTransferOrderLot>(_currSession,
                                                                                                                new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("InventoryTransferOrderLine", _locInventoryTransOrderLine),
                                                                                                                new BinaryOperator("Select", true)));
                                                            if (_locInventoryTransferOrderLots != null && _locInventoryTransferOrderLots.Count() > 0)
                                                            {
                                                                foreach (InventoryTransferOrderLot _locInventoryTransferOrderLot in _locInventoryTransferOrderLots)
                                                                {
                                                                    InventoryTransferOutLot _saveDataInventoryTransferOutLot = new InventoryTransferOutLot(_currSession)
                                                                    {
                                                                        Select = true,
                                                                        Location = _locInventoryTransferOrderLot.LocationFrom,
                                                                        BinLocation = _locInventoryTransferOrderLot.BinLocationFrom,
                                                                        StockType = _locInventoryTransferOrderLot.StockTypeFrom,
                                                                        Item = _locInventoryTransferOrderLot.Item,
                                                                        BegInvLine = _locInventoryTransferOrderLot.BegInvLine,
                                                                        LotNumber = _locInventoryTransferOrderLot.LotNumber,
                                                                        DQty = _locInventoryTransferOrderLot.DQty,
                                                                        DUOM = _locInventoryTransferOrderLot.DUOM,
                                                                        Qty = _locInventoryTransferOrderLot.Qty,
                                                                        UOM = _locInventoryTransferOrderLot.UOM,
                                                                        TQty = _locInventoryTransferOrderLot.TQty,
                                                                        InventoryTransferOutLine = _locInventoryTransferOutLine,
                                                                        UnitPack = _locInventoryTransferOrderLot.UnitPack,
                                                                    };
                                                                    _saveDataInventoryTransferOutLot.Save();
                                                                    _saveDataInventoryTransferOutLot.Session.CommitTransaction();
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferOrder ", ex.ToString());
            }
        }

        //Membuat Inventory Transfer In
        private void SetInventoryTransferIn(Session _currSession, InventoryTransferOrder _inventoryTransferOrderXPO)
        {
            try
            {

                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _locDocCode = null;
                string _locSignCodeITIL = null;
                ProjectHeader _locProjectHeader = null;
                DocumentType _locDocumentTypeXPO = null;
                DocumentType _locDocumentType = null;

                if (_inventoryTransferOrderXPO != null)
                {
                    if (_inventoryTransferOrderXPO.ProjectHeader != null)
                    {
                        _locProjectHeader = _inventoryTransferOrderXPO.ProjectHeader;
                    }

                    if (_inventoryTransferOrderXPO.DocumentType != null)
                    {
                        _locDocumentTypeXPO = _inventoryTransferOrderXPO.DocumentType;
                    }

                    _locDocumentType = _currSession.FindObject<DocumentType>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("TransferType", _inventoryTransferOrderXPO.TransferType),
                                                     new BinaryOperator("InventoryMovingType", InventoryMovingType.Receive),
                                                     new BinaryOperator("ObjectList", ObjectList.InventoryTransferIn),
                                                     new BinaryOperator("Active", true)));

                    if (_locDocumentType != null)
                    {
                        if (_locDocumentTypeXPO != null && _locDocumentTypeXPO == _locDocumentType)
                        {
                            _locDocCode = _globFunc.GetDocumentNumberingUnlockOptimisticRecord(_currSession.DataLayer, _locDocumentTypeXPO);

                            if (_inventoryTransferOrderXPO.DocNo != null)
                            {
                                _locDocCode = _inventoryTransferOrderXPO.DocNo;
                            }

                            if (_locDocCode != null)
                            {
                                InventoryTransferIn _locInventoryTransferIn = _currSession.FindObject<InventoryTransferIn>(new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("InventoryTransferOrder", _inventoryTransferOrderXPO),
                                                    new BinaryOperator("DocumentType", _locDocumentTypeXPO),
                                                    new BinaryOperator("DocNo", _locDocCode)));

                                if (_locInventoryTransferIn == null)
                                {
                                    InventoryTransferIn _saveDataTI = new InventoryTransferIn(_currSession)
                                    {
                                        TransferType = _inventoryTransferOrderXPO.TransferType,
                                        InventoryMovingType = InventoryMovingType.Receive,
                                        ObjectList = ObjectList.InventoryTransferIn,
                                        DocumentType = _locDocumentTypeXPO,
                                        DocNo = _locDocCode,
                                        DocumentRule = _inventoryTransferOrderXPO.DocumentRule,
                                        BusinessPartner = _inventoryTransferOrderXPO.BusinessPartner,
                                        BusinessPartnerCountry = _inventoryTransferOrderXPO.BusinessPartnerCountry,
                                        BusinessPartnerCity = _inventoryTransferOrderXPO.BusinessPartnerCity,
                                        BusinessPartnerAddress = _inventoryTransferOrderXPO.BusinessPartnerAddress,
                                        EstimatedDate = _inventoryTransferOrderXPO.EstimatedDate,
                                        InventoryTransferOrder = _inventoryTransferOrderXPO,
                                        ProjectHeader = _locProjectHeader,
                                        Company = _inventoryTransferOrderXPO.Company,
                                    };
                                    _saveDataTI.Save();
                                    _saveDataTI.Session.CommitTransaction();

                                    InventoryTransferIn _locInventoryTransferIn2 = _currSession.FindObject<InventoryTransferIn>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("DocNo", _locDocCode),
                                                         new BinaryOperator("InventoryTransferOrder", _inventoryTransferOrderXPO)));

                                    if (_locInventoryTransferIn2 != null)
                                    {
                                        XPCollection<InventoryTransferOrderLine> _locInventoryTransOrderLines = new XPCollection<InventoryTransferOrderLine>(_currSession,
                                                                                            new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("InventoryTransferOrder", _inventoryTransferOrderXPO),
                                                                                            new BinaryOperator("Select", true)));

                                        if (_locInventoryTransOrderLines != null && _locInventoryTransOrderLines.Count > 0)
                                        {
                                            //PerTransferOutLine
                                            foreach (InventoryTransferOrderLine _locInventoryTransOrderLine in _locInventoryTransOrderLines)
                                            {
                                                if (_locInventoryTransOrderLine.Status == Status.Progress || _locInventoryTransOrderLine.Status == Status.Posted)
                                                {
                                                    _locSignCodeITIL = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.InventoryTransferInLine);

                                                    if (_locSignCodeITIL != null)
                                                    {
                                                        InventoryTransferInLine _saveDataInventoryTransferInLine = new InventoryTransferInLine(_currSession)
                                                        {
                                                            Select = true,
                                                            SignCode = _locSignCodeITIL,
                                                            Location = _locInventoryTransOrderLine.LocationFrom,
                                                            BinLocation = _locInventoryTransOrderLine.BinLocationFrom,
                                                            StockType = _locInventoryTransOrderLine.StockTypeFrom,
                                                            Item = _locInventoryTransOrderLine.Item,
                                                            MxDQty = _locInventoryTransOrderLine.DQty,
                                                            MxDUOM = _locInventoryTransOrderLine.DUOM,
                                                            MxQty = _locInventoryTransOrderLine.Qty,
                                                            MxUOM = _locInventoryTransOrderLine.UOM,
                                                            MxTQty = _locInventoryTransOrderLine.TQty,
                                                            DQty = _locInventoryTransOrderLine.DQty,
                                                            DUOM = _locInventoryTransOrderLine.DUOM,
                                                            Qty = _locInventoryTransOrderLine.Qty,
                                                            UOM = _locInventoryTransOrderLine.UOM,
                                                            TQty = _locInventoryTransOrderLine.TQty,
                                                            InventoryTransferIn = _locInventoryTransferIn2,
                                                            Company = _locInventoryTransOrderLine.Company,

                                                        };
                                                        _saveDataInventoryTransferInLine.Save();
                                                        _saveDataInventoryTransferInLine.Session.CommitTransaction();

                                                        InventoryTransferInLine _locInventoryTransferInLine = _currSession.FindObject<InventoryTransferInLine>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("SignCode", _locSignCodeITIL)));

                                                        if (_locInventoryTransferInLine != null)
                                                        {
                                                            XPCollection<InventoryTransferOrderLot> _locInventoryTransferOrderLots = new XPCollection<InventoryTransferOrderLot>(_currSession,
                                                                                                                new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("InventoryTransferOrderLine", _locInventoryTransOrderLine),
                                                                                                                new BinaryOperator("Select", true)));
                                                            if (_locInventoryTransferOrderLots != null && _locInventoryTransferOrderLots.Count() > 0)
                                                            {
                                                                foreach (InventoryTransferOrderLot _locInventoryTransferOrderLot in _locInventoryTransferOrderLots)
                                                                {
                                                                    InventoryTransferInLot _saveDataInventoryTransferInLot = new InventoryTransferInLot(_currSession)
                                                                    {
                                                                        Select = true,
                                                                        Location = _locInventoryTransferOrderLot.LocationFrom,
                                                                        BinLocation = _locInventoryTransferOrderLot.BinLocationFrom,
                                                                        StockType = _locInventoryTransferOrderLot.StockTypeFrom,
                                                                        Item = _locInventoryTransferOrderLot.Item,
                                                                        BegInvLine = _locInventoryTransferOrderLot.BegInvLine,
                                                                        LotNumber = _locInventoryTransferOrderLot.LotNumber,
                                                                        DQty = _locInventoryTransferOrderLot.DQty,
                                                                        DUOM = _locInventoryTransferOrderLot.DUOM,
                                                                        Qty = _locInventoryTransferOrderLot.Qty,
                                                                        UOM = _locInventoryTransferOrderLot.UOM,
                                                                        TQty = _locInventoryTransferOrderLot.TQty,
                                                                        InventoryTransferInLine = _locInventoryTransferInLine,
                                                                        UnitPack = _locInventoryTransferOrderLot.UnitPack,
                                                                    };
                                                                    _saveDataInventoryTransferInLot.Save();
                                                                    _saveDataInventoryTransferInLot.Session.CommitTransaction();
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferOrder ", ex.ToString());
            }
        }

        //Menormalkan Quantity
        private void SetNormalPostingQuantity(Session _currSession, InventoryTransferOrder _inventoryTransferOrderXPO)
        {
            try
            {
                if (_inventoryTransferOrderXPO != null)
                {
                    XPCollection<InventoryTransferOrderLine> _locInventoryTransOrderLines = new XPCollection<InventoryTransferOrderLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("InventoryTransferOrder", _inventoryTransferOrderXPO),
                                                                                     new BinaryOperator("Select", true)));

                    if (_locInventoryTransOrderLines != null && _locInventoryTransOrderLines.Count > 0)
                    {
                        foreach (InventoryTransferOrderLine _locInventoryTransOrderLine in _locInventoryTransOrderLines)
                        {
                            if (_locInventoryTransOrderLine.Status == Status.Progress || _locInventoryTransOrderLine.Status == Status.Posted || _locInventoryTransOrderLine.Status == Status.Close)
                            {
                                if (_locInventoryTransOrderLine.DQty > 0 || _locInventoryTransOrderLine.Qty > 0)
                                {
                                    _locInventoryTransOrderLine.Select = false;
                                    _locInventoryTransOrderLine.DQty = 0;
                                    _locInventoryTransOrderLine.Qty = 0;
                                    _locInventoryTransOrderLine.Save();
                                    _locInventoryTransOrderLine.Session.CommitTransaction();

                                    XPCollection<InventoryTransferOrderLot> _locInventoryTransferOrderLots = new XPCollection<InventoryTransferOrderLot>(_currSession,
                                                                                         new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("InventoryTransferOrderLine", _locInventoryTransOrderLine),
                                                                                         new BinaryOperator("Select", true)));

                                    if (_locInventoryTransferOrderLots != null && _locInventoryTransferOrderLots.Count() > 0)
                                    {
                                        foreach (InventoryTransferOrderLot _locInventoryTransferOrderLot in _locInventoryTransferOrderLots)
                                        {
                                            if (_locInventoryTransferOrderLot.Status == Status.Progress || _locInventoryTransferOrderLot.Status == Status.Posted)
                                            {
                                                _locInventoryTransferOrderLot.Select = false;
                                                _locInventoryTransferOrderLot.Save();
                                                _locInventoryTransferOrderLot.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOrder " + ex.ToString());
            }
        }

        //Menentukan Status Deliver Pada Inventory Transfer Order Line
        private void SetStatusPostingInventoryTransferOrderLine(Session _currSession, InventoryTransferOrder _inventoryTransferOrderXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_inventoryTransferOrderXPO != null)
                {
                    XPCollection<InventoryTransferOrderLine> _locInventoryTransOrderLines = new XPCollection<InventoryTransferOrderLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("InventoryTransferOrder", _inventoryTransferOrderXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locInventoryTransOrderLines != null && _locInventoryTransOrderLines.Count > 0)
                    {

                        foreach (InventoryTransferOrderLine _locInventoryTransOrderLine in _locInventoryTransOrderLines)
                        {
                            if (_locInventoryTransOrderLine.Status == Status.Progress || _locInventoryTransOrderLine.Status == Status.Posted)
                            {
                                if (_locInventoryTransOrderLine.RmDQty == 0 && _locInventoryTransOrderLine.RmQty == 0 && _locInventoryTransOrderLine.RmTQty == 0)
                                {
                                    _locInventoryTransOrderLine.Status = Status.Close;
                                    _locInventoryTransOrderLine.ActivationPosting = true;
                                    _locInventoryTransOrderLine.StatusDate = now;
                                }
                                else
                                {
                                    _locInventoryTransOrderLine.Status = Status.Posted;
                                    _locInventoryTransOrderLine.StatusDate = now;
                                }

                                XPCollection<InventoryTransferOrderLot> _locInventoryTransferOrderLots = new XPCollection<InventoryTransferOrderLot>(_currSession,
                                                                                         new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("InventoryTransferOrderLine", _locInventoryTransOrderLine),
                                                                                         new BinaryOperator("Select", true)));

                                if (_locInventoryTransferOrderLots != null && _locInventoryTransferOrderLots.Count() > 0)
                                {
                                    foreach (InventoryTransferOrderLot _locInventoryTransferOrderLot in _locInventoryTransferOrderLots)
                                    {
                                        if (_locInventoryTransferOrderLot.Status == Status.Progress || _locInventoryTransferOrderLot.Status == Status.Posted)
                                        {
                                            _locInventoryTransferOrderLot.Status = Status.Close;
                                            _locInventoryTransferOrderLot.ActivationPosting = true;
                                            _locInventoryTransferOrderLot.StatusDate = now;
                                            _locInventoryTransferOrderLot.Save();
                                            _locInventoryTransferOrderLot.Session.CommitTransaction();
                                        }
                                    }
                                }

                                _locInventoryTransOrderLine.Save();
                                _locInventoryTransOrderLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOrder " + ex.ToString());
            }
        }

        //Menentukan Status Deliver Pada Inventory Transfer Order
        private void SetFinalStatusPostingInventoryTransferOrder(Session _currSession, InventoryTransferOrder _inventoryTransferOrderXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locCount = 0;

                if (_inventoryTransferOrderXPO != null)
                {
                    XPCollection<InventoryTransferOrderLine> _locInventoryTransOrderLines = new XPCollection<InventoryTransferOrderLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("InventoryTransferOrder", _inventoryTransferOrderXPO)));

                    if (_locInventoryTransOrderLines != null && _locInventoryTransOrderLines.Count() > 0)
                    {
                        foreach (InventoryTransferOrderLine _locInventoryTransOrderLine in _locInventoryTransOrderLines)
                        {
                            if (_locInventoryTransOrderLine.Status == Status.Close)
                            {
                                _locCount = _locCount + 1;
                            }
                        }

                        if (_locCount == _locInventoryTransOrderLines.Count())
                        {
                            _inventoryTransferOrderXPO.ActivationPosting = true;
                            _inventoryTransferOrderXPO.Status = Status.Close;
                            _inventoryTransferOrderXPO.StatusDate = now;
                            _inventoryTransferOrderXPO.Save();
                            _inventoryTransferOrderXPO.Session.CommitTransaction();
                        }
                        else
                        {
                            _inventoryTransferOrderXPO.Status = Status.Posted;
                            _inventoryTransferOrderXPO.StatusDate = now;
                            _inventoryTransferOrderXPO.Save();
                            _inventoryTransferOrderXPO.Session.CommitTransaction();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOrder " + ex.ToString());
            }
        }

        //Membuat Transfer Order
        private void SetTransferOrder(Session _currSession, InventoryTransferOrder _inventoryTransferOrderXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _locDocCode = null;
                string _locSignCodeTOL = null;
                ProjectHeader _locProjectHeader = null;

                if (_inventoryTransferOrderXPO != null)
                {
                    if (_inventoryTransferOrderXPO.ProjectHeader != null)
                    {
                        _locProjectHeader = _inventoryTransferOrderXPO.ProjectHeader;
                    }

                    DocumentType _locDocumentTypeXPO = _currSession.FindObject<DocumentType>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("TransferType", DirectionType.Internal),
                                                     new BinaryOperator("InventoryMovingType", InventoryMovingType.TransferOut),
                                                     new BinaryOperator("ObjectList", ObjectList.TransferOrder),
                                                     new BinaryOperator("Active", true)));

                    if (_locDocumentTypeXPO != null)
                    {
                        _locDocCode = _globFunc.GetDocumentNumberingUnlockOptimisticRecord(_currSession.DataLayer, _locDocumentTypeXPO);

                        if (_locDocCode != null)
                        {
                            TransferOrder _locTransferOrder = _currSession.FindObject<TransferOrder>(new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("TransferType", _inventoryTransferOrderXPO.TransferType),
                                                new BinaryOperator("InventoryTransferOrder", _inventoryTransferOrderXPO),
                                                new BinaryOperator("DocumentType", _locDocumentTypeXPO),
                                                new BinaryOperator("DocNo", _locDocCode)));

                            if (_locTransferOrder == null)
                            {
                                TransferOrder _saveDataTO = new TransferOrder(_currSession)
                                {
                                    TransferType = DirectionType.Internal,
                                    InventoryMovingType = InventoryMovingType.TransferOut,
                                    ObjectList = ObjectList.TransferOrder,
                                    DocumentType = _locDocumentTypeXPO,
                                    DocNo = _locDocCode,
                                    EstimatedDate = _inventoryTransferOrderXPO.EstimatedDate,
                                    InventoryTransferOrder = _inventoryTransferOrderXPO,
                                    LocationTo = _inventoryTransferOrderXPO.Location,
                                    ProjectHeader = _locProjectHeader,
                                    Company = _inventoryTransferOrderXPO.Company,
                                };
                                _saveDataTO.Save();
                                _saveDataTO.Session.CommitTransaction();

                                TransferOrder _locTransferOrder2 = _currSession.FindObject<TransferOrder>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("DocNo", _locDocCode),
                                                     new BinaryOperator("InventoryTransferOrder", _inventoryTransferOrderXPO)));

                                if (_locTransferOrder2 != null)
                                {
                                    XPCollection<InventoryTransferOrderLine> _locInventoryTransOrderLines = new XPCollection<InventoryTransferOrderLine>(_currSession,
                                                                                        new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("InventoryTransferOrder", _inventoryTransferOrderXPO),
                                                                                        new BinaryOperator("Select", true)));

                                    if (_locInventoryTransOrderLines != null && _locInventoryTransOrderLines.Count > 0)
                                    {
                                        //PerTransferOutLine
                                        foreach (InventoryTransferOrderLine _locInventoryTransOrderLine in _locInventoryTransOrderLines)
                                        {
                                            if (_locInventoryTransOrderLine.Status == Status.Progress || _locInventoryTransOrderLine.Status == Status.Posted)
                                            {
                                                _locSignCodeTOL = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.TransferOrderLine);

                                                if (_locSignCodeTOL != null)
                                                {
                                                    TransferOrderLine _saveDataTransferOrderLine = new TransferOrderLine(_currSession)
                                                    {
                                                        SignCode = _locSignCodeTOL,
                                                        LocationFrom = _locInventoryTransOrderLine.LocationFrom,
                                                        BinLocationFrom = _locInventoryTransOrderLine.BinLocationFrom,
                                                        StockTypeFrom = _locInventoryTransOrderLine.StockTypeFrom,
                                                        LocationTo = _locInventoryTransOrderLine.LocationTo,
                                                        BinLocationTo = _locInventoryTransOrderLine.BinLocationTo,
                                                        StockTypeTo = _locInventoryTransOrderLine.StockTypeTo,
                                                        Item = _locInventoryTransOrderLine.Item,
                                                        MxDQty = _locInventoryTransOrderLine.DQty,
                                                        MxDUOM = _locInventoryTransOrderLine.DUOM,
                                                        MxQty = _locInventoryTransOrderLine.Qty,
                                                        MxUOM = _locInventoryTransOrderLine.UOM,
                                                        MxTQty = _locInventoryTransOrderLine.TQty,
                                                        DQty = _locInventoryTransOrderLine.DQty,
                                                        DUOM = _locInventoryTransOrderLine.DUOM,
                                                        Qty = _locInventoryTransOrderLine.Qty,
                                                        UOM = _locInventoryTransOrderLine.UOM,
                                                        TQty = _locInventoryTransOrderLine.TQty,
                                                        TransferOrder = _locTransferOrder2,
                                                    };
                                                    _saveDataTransferOrderLine.Save();
                                                    _saveDataTransferOrderLine.Session.CommitTransaction();

                                                    TransferOrderLine _locTransferOrderLine = _currSession.FindObject<TransferOrderLine>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("SignCode", _locSignCodeTOL)));
                                                    if (_locTransferOrderLine != null)
                                                    {
                                                        XPCollection<InventoryTransferOrderLot> _locInventoryTransferOrderLots = new XPCollection<InventoryTransferOrderLot>(_currSession,
                                                                                                            new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("InventoryTransferOrderLine", _locInventoryTransOrderLine),
                                                                                                            new BinaryOperator("Select", true)));
                                                        if (_locInventoryTransferOrderLots != null && _locInventoryTransferOrderLots.Count() > 0)
                                                        {
                                                            foreach (InventoryTransferOrderLot _locInventoryTransferOrderLot in _locInventoryTransferOrderLots)
                                                            {
                                                                TransferOrderLot _saveDataTransferOrderLot = new TransferOrderLot(_currSession)
                                                                {
                                                                    Select = true,
                                                                    LocationFrom = _locInventoryTransferOrderLot.LocationFrom,
                                                                    BinLocationFrom = _locInventoryTransferOrderLot.BinLocationFrom,
                                                                    StockTypeFrom = _locInventoryTransferOrderLot.StockTypeFrom,
                                                                    LocationTo = _locInventoryTransferOrderLot.LocationTo,
                                                                    BinLocationTo = _locInventoryTransferOrderLot.BinLocationTo,
                                                                    StockTypeTo = _locInventoryTransferOrderLot.StockTypeTo,
                                                                    Item = _locInventoryTransferOrderLot.Item,
                                                                    BegInvLine = _locInventoryTransferOrderLot.BegInvLine,
                                                                    LotNumber = _locInventoryTransferOrderLot.LotNumber,
                                                                    DQty = _locInventoryTransferOrderLot.DQty,
                                                                    DUOM = _locInventoryTransferOrderLot.DUOM,
                                                                    Qty = _locInventoryTransferOrderLot.Qty,
                                                                    UOM = _locInventoryTransferOrderLot.UOM,
                                                                    TQty = _locInventoryTransferOrderLot.TQty,
                                                                    TransferOrderLine = _locTransferOrderLine,
                                                                    UnitPack = _locInventoryTransferOrderLot.UnitPack,
                                                                };
                                                                _saveDataTransferOrderLot.Save();
                                                                _saveDataTransferOrderLot.Session.CommitTransaction();
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferOrder ", ex.ToString());
            }
        }

        //Membuat Inventory Transfer Out dengan setting Stock Controlling 
        private void SetInventoryTransferOutWithStockControlling(Session _currSession, InventoryTransferOrder _inventoryTransferOrderXPO, UserAccess _locUserAccess)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _locDocCode = null;
                string _locSignCodeITOL = null;
                ProjectHeader _locProjectHeader = null;
                SalesOrder _locSalesOrder = null;
                DocumentType _locDocumentTypeXPO = null;
                DocumentType _locDocumentType = null;
                int _locCountSameWarehouse = 0;
                int _locCountUnSameWarehouse = 0;

                if (_inventoryTransferOrderXPO != null)
                {
                    if (_inventoryTransferOrderXPO.ProjectHeader != null)
                    {
                        _locProjectHeader = _inventoryTransferOrderXPO.ProjectHeader;
                    }

                    if (_inventoryTransferOrderXPO.SalesOrder != null)
                    {
                        _locSalesOrder = _inventoryTransferOrderXPO.SalesOrder;
                    }

                    //Menentukan jumlah SameWarehouse dng yg tidak
                    #region MakeCountSameWarehouseOrNot
                    XPCollection<InventoryTransferOrderLine> _locInventoryTransOrderLines = new XPCollection<InventoryTransferOrderLine>(_currSession,
                                                                                        new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("InventoryTransferOrder", _inventoryTransferOrderXPO),
                                                                                        new BinaryOperator("Select", true)));

                    if (_locInventoryTransOrderLines != null && _locInventoryTransOrderLines.Count > 0)
                    {
                        foreach (InventoryTransferOrderLine _locInventoryTransOrderLine in _locInventoryTransOrderLines)
                        {
                            if (CheckAvailableWarehouseSetupDetail(_currSession, _locInventoryTransOrderLine.LocationFrom, _locUserAccess) == true
                                    && CheckAvailableWarehouseSetupDetail(_currSession, _locInventoryTransOrderLine.LocationTo, _locUserAccess) == true)
                            {
                                _locCountSameWarehouse = _locCountSameWarehouse + 1;
                            }
                            else
                            {
                                _locCountUnSameWarehouse = _locCountUnSameWarehouse + 1;
                            }
                        }
                    }
                    #endregion MakeCountSameWarehouseOrNot

                    #region CreateInventoryTransferOutWithSameWarehouse

                    if (_locCountSameWarehouse > 0)
                    {

                        if (_inventoryTransferOrderXPO.DocumentType != null)
                        {
                            _locDocumentTypeXPO = _inventoryTransferOrderXPO.DocumentType;
                        }

                        _locDocumentType = _currSession.FindObject<DocumentType>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("TransferType", _inventoryTransferOrderXPO.TransferType),
                                                     new BinaryOperator("InventoryMovingType", InventoryMovingType.Deliver),
                                                     new BinaryOperator("ObjectList", ObjectList.InventoryTransferOut),
                                                     new BinaryOperator("Active", true)));

                        if (_locDocumentType != null)
                        {
                            if (_locDocumentTypeXPO != null && _locDocumentTypeXPO == _locDocumentType)
                            {
                                if (_inventoryTransferOrderXPO.DocNo != null)
                                {
                                    _locDocCode = _inventoryTransferOrderXPO.DocNo;
                                }

                                if (_locDocCode != null)
                                {

                                    InventoryTransferOut _locInventoryTransferOut = _currSession.FindObject<InventoryTransferOut>(new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("InventoryTransferOrder", _inventoryTransferOrderXPO),
                                                        new BinaryOperator("DocumentType", _locDocumentTypeXPO),
                                                        new BinaryOperator("DocNo", _locDocCode)));

                                    if (_locInventoryTransferOut == null)
                                    {
                                        InventoryTransferOut _saveDataITO = new InventoryTransferOut(_currSession)
                                        {
                                            TransferType = _inventoryTransferOrderXPO.TransferType,
                                            InventoryMovingType = InventoryMovingType.Deliver,
                                            ObjectList = ObjectList.InventoryTransferOut,
                                            DocumentType = _locDocumentTypeXPO,
                                            DocumentRule = _inventoryTransferOrderXPO.DocumentRule,
                                            BusinessPartner = _inventoryTransferOrderXPO.BusinessPartner,
                                            BusinessPartnerCountry = _inventoryTransferOrderXPO.BusinessPartnerCountry,
                                            BusinessPartnerCity = _inventoryTransferOrderXPO.BusinessPartnerCity,
                                            BusinessPartnerAddress = _inventoryTransferOrderXPO.BusinessPartnerAddress,
                                            EstimatedDate = _inventoryTransferOrderXPO.EstimatedDate,
                                            DocNo = _locDocCode,
                                            InventoryTransferOrder = _inventoryTransferOrderXPO,
                                            ProjectHeader = _locProjectHeader,
                                            Company = _inventoryTransferOrderXPO.Company,
                                            SameWarehouse = true,
                                            StockControlling = _inventoryTransferOrderXPO.StockControlling,
                                        };
                                        _saveDataITO.Save();
                                        _saveDataITO.Session.CommitTransaction();

                                        InventoryTransferOut _locInventoryTransferOut2 = _currSession.FindObject<InventoryTransferOut>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                             new BinaryOperator("DocNo", _locDocCode),
                                                             new BinaryOperator("InventoryTransferOrder", _inventoryTransferOrderXPO)));

                                        if (_locInventoryTransferOut2 != null)
                                        {
                                            XPCollection<InventoryTransferOrderLine> _locSWInventoryTransOrderLines = new XPCollection<InventoryTransferOrderLine>(_currSession,
                                                                                                new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("InventoryTransferOrder", _inventoryTransferOrderXPO),
                                                                                                new BinaryOperator("Select", true)));

                                            if (_locSWInventoryTransOrderLines != null && _locSWInventoryTransOrderLines.Count > 0)
                                            {
                                                //PerTransferOutLine
                                                foreach (InventoryTransferOrderLine _locSWInventoryTransOrderLine in _locSWInventoryTransOrderLines)
                                                {
                                                    if (_locSWInventoryTransOrderLine.Status == Status.Progress || _locSWInventoryTransOrderLine.Status == Status.Posted)
                                                    {
                                                        if (CheckAvailableWarehouseSetupDetail(_currSession, _locSWInventoryTransOrderLine.LocationFrom, _locUserAccess) == true
                                                            && CheckAvailableWarehouseSetupDetail(_currSession, _locSWInventoryTransOrderLine.LocationTo, _locUserAccess) == true)
                                                        {
                                                            _locSignCodeITOL = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.InventoryTransferOutLine);

                                                            if (_locSignCodeITOL != null)
                                                            {
                                                                InventoryTransferOutLine _saveDataInventoryTransferOutLine = new InventoryTransferOutLine(_currSession)
                                                                {
                                                                    Select = true,
                                                                    Location = _locSWInventoryTransOrderLine.LocationFrom,
                                                                    BinLocation = _locSWInventoryTransOrderLine.BinLocationFrom,
                                                                    StockType = _locSWInventoryTransOrderLine.StockTypeFrom,
                                                                    LocationTo = _locSWInventoryTransOrderLine.LocationTo,
                                                                    BinLocationTo = _locSWInventoryTransOrderLine.BinLocationTo,
                                                                    StockTypeTo = _locSWInventoryTransOrderLine.StockTypeTo,
                                                                    Item = _locSWInventoryTransOrderLine.Item,
                                                                    MxDQty = _locSWInventoryTransOrderLine.DQty,
                                                                    MxDUOM = _locSWInventoryTransOrderLine.DUOM,
                                                                    MxQty = _locSWInventoryTransOrderLine.Qty,
                                                                    MxUOM = _locSWInventoryTransOrderLine.UOM,
                                                                    MxTQty = _locSWInventoryTransOrderLine.TQty,
                                                                    DQty = _locSWInventoryTransOrderLine.DQty,
                                                                    DUOM = _locSWInventoryTransOrderLine.DUOM,
                                                                    Qty = _locSWInventoryTransOrderLine.Qty,
                                                                    UOM = _locSWInventoryTransOrderLine.UOM,
                                                                    TQty = _locSWInventoryTransOrderLine.TQty,
                                                                    InventoryTransferOut = _locInventoryTransferOut2,
                                                                    Company = _locInventoryTransferOut2.Company,
                                                                    StockControlling = _locSWInventoryTransOrderLine.StockControlling,
                                                                };
                                                                _saveDataInventoryTransferOutLine.Save();
                                                                _saveDataInventoryTransferOutLine.Session.CommitTransaction();

                                                                InventoryTransferOutLine _locInventoryTransferOutLine = _currSession.FindObject<InventoryTransferOutLine>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("SignCode", _locSignCodeITOL)));

                                                                if (_locInventoryTransferOutLine != null)
                                                                {
                                                                    XPCollection<InventoryTransferOrderLot> _locInventoryTransferOrderLots = new XPCollection<InventoryTransferOrderLot>(_currSession,
                                                                                                                        new GroupOperator(GroupOperatorType.And,
                                                                                                                        new BinaryOperator("InventoryTransferOrderLine", _locSWInventoryTransOrderLine),
                                                                                                                        new BinaryOperator("Select", true)));
                                                                    if (_locInventoryTransferOrderLots != null && _locInventoryTransferOrderLots.Count() > 0)
                                                                    {
                                                                        foreach (InventoryTransferOrderLot _locInventoryTransferOrderLot in _locInventoryTransferOrderLots)
                                                                        {
                                                                            InventoryTransferOutLot _saveDataInventoryTransferOutLot = new InventoryTransferOutLot(_currSession)
                                                                            {
                                                                                Select = true,
                                                                                Location = _locInventoryTransferOrderLot.LocationFrom,
                                                                                BinLocation = _locInventoryTransferOrderLot.BinLocationFrom,
                                                                                StockType = _locInventoryTransferOrderLot.StockTypeFrom,
                                                                                Item = _locInventoryTransferOrderLot.Item,
                                                                                BegInvLine = _locInventoryTransferOrderLot.BegInvLine,
                                                                                LotNumber = _locInventoryTransferOrderLot.LotNumber,
                                                                                DQty = _locInventoryTransferOrderLot.DQty,
                                                                                DUOM = _locInventoryTransferOrderLot.DUOM,
                                                                                Qty = _locInventoryTransferOrderLot.Qty,
                                                                                UOM = _locInventoryTransferOrderLot.UOM,
                                                                                TQty = _locInventoryTransferOrderLot.TQty,
                                                                                InventoryTransferOutLine = _locInventoryTransferOutLine,
                                                                                UnitPack = _locInventoryTransferOrderLot.UnitPack,
                                                                            };
                                                                            _saveDataInventoryTransferOutLot.Save();
                                                                            _saveDataInventoryTransferOutLot.Session.CommitTransaction();
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    #endregion CreateInventoryTransferOutWithSameWarehouse

                    #region CreateInventoryTransferOutWithoutSameWarehouse

                    if (_locCountUnSameWarehouse > 0)
                    {

                        if (_inventoryTransferOrderXPO.DocumentType != null)
                        {
                            _locDocumentTypeXPO = _inventoryTransferOrderXPO.DocumentType;
                        }

                        _locDocumentType = _currSession.FindObject<DocumentType>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("TransferType", _inventoryTransferOrderXPO.TransferType),
                                                     new BinaryOperator("InventoryMovingType", InventoryMovingType.Deliver),
                                                     new BinaryOperator("ObjectList", ObjectList.InventoryTransferOut),
                                                     new BinaryOperator("Active", true)));

                        if (_locDocumentType != null)
                        {
                            if (_locDocumentTypeXPO != null && _locDocumentTypeXPO == _locDocumentType)
                            {

                                if (_inventoryTransferOrderXPO.DocNo != null)
                                {
                                    _locDocCode = _inventoryTransferOrderXPO.DocNo;
                                }

                                if (_locDocCode != null)
                                {

                                    InventoryTransferOut _locInventoryTransferOut = _currSession.FindObject<InventoryTransferOut>(new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("InventoryTransferOrder", _inventoryTransferOrderXPO),
                                                        new BinaryOperator("DocumentType", _locDocumentTypeXPO),
                                                        new BinaryOperator("DocNo", _locDocCode)));

                                    if (_locInventoryTransferOut == null)
                                    {
                                        InventoryTransferOut _saveDataITO = new InventoryTransferOut(_currSession)
                                        {
                                            TransferType = _inventoryTransferOrderXPO.TransferType,
                                            InventoryMovingType = InventoryMovingType.Deliver,
                                            ObjectList = ObjectList.InventoryTransferOut,
                                            DocumentType = _locDocumentTypeXPO,
                                            DocumentRule = _inventoryTransferOrderXPO.DocumentRule,
                                            BusinessPartner = _inventoryTransferOrderXPO.BusinessPartner,
                                            BusinessPartnerCountry = _inventoryTransferOrderXPO.BusinessPartnerCountry,
                                            BusinessPartnerCity = _inventoryTransferOrderXPO.BusinessPartnerCity,
                                            BusinessPartnerAddress = _inventoryTransferOrderXPO.BusinessPartnerAddress,
                                            EstimatedDate = _inventoryTransferOrderXPO.EstimatedDate,
                                            DocNo = _locDocCode,
                                            InventoryTransferOrder = _inventoryTransferOrderXPO,
                                            ProjectHeader = _locProjectHeader,
                                            Company = _inventoryTransferOrderXPO.Company,
                                            SameWarehouse = false,
                                            StockControlling = _inventoryTransferOrderXPO.StockControlling,
                                        };
                                        _saveDataITO.Save();
                                        _saveDataITO.Session.CommitTransaction();

                                        InventoryTransferOut _locInventoryTransferOut2 = _currSession.FindObject<InventoryTransferOut>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                             new BinaryOperator("DocNo", _locDocCode),
                                                             new BinaryOperator("InventoryTransferOrder", _inventoryTransferOrderXPO)));

                                        if (_locInventoryTransferOut2 != null)
                                        {
                                            XPCollection<InventoryTransferOrderLine> _locNSWInventoryTransOrderLines = new XPCollection<InventoryTransferOrderLine>(_currSession,
                                                                                                new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("InventoryTransferOrder", _inventoryTransferOrderXPO),
                                                                                                new BinaryOperator("Select", true)));

                                            if (_locNSWInventoryTransOrderLines != null && _locNSWInventoryTransOrderLines.Count > 0)
                                            {
                                                //PerTransferOutLine
                                                foreach (InventoryTransferOrderLine _locNSWInventoryTransOrderLine in _locNSWInventoryTransOrderLines)
                                                {


                                                    if (_locNSWInventoryTransOrderLine.Status == Status.Progress || _locNSWInventoryTransOrderLine.Status == Status.Posted)
                                                    {
                                                        _locSignCodeITOL = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.InventoryTransferOutLine);

                                                        if (_locSignCodeITOL != null)
                                                        {
                                                            InventoryTransferOutLine _saveDataInventoryTransferOutLine = new InventoryTransferOutLine(_currSession)
                                                            {
                                                                Select = true,
                                                                Location = _locNSWInventoryTransOrderLine.LocationFrom,
                                                                BinLocation = _locNSWInventoryTransOrderLine.BinLocationFrom,
                                                                StockType = _locNSWInventoryTransOrderLine.StockTypeFrom,
                                                                LocationTo = _locNSWInventoryTransOrderLine.LocationTo,
                                                                BinLocationTo = _locNSWInventoryTransOrderLine.BinLocationTo,
                                                                StockTypeTo = _locNSWInventoryTransOrderLine.StockTypeTo,
                                                                Item = _locNSWInventoryTransOrderLine.Item,
                                                                MxDQty = _locNSWInventoryTransOrderLine.DQty,
                                                                MxDUOM = _locNSWInventoryTransOrderLine.DUOM,
                                                                MxQty = _locNSWInventoryTransOrderLine.Qty,
                                                                MxUOM = _locNSWInventoryTransOrderLine.UOM,
                                                                MxTQty = _locNSWInventoryTransOrderLine.TQty,
                                                                DQty = _locNSWInventoryTransOrderLine.DQty,
                                                                DUOM = _locNSWInventoryTransOrderLine.DUOM,
                                                                Qty = _locNSWInventoryTransOrderLine.Qty,
                                                                UOM = _locNSWInventoryTransOrderLine.UOM,
                                                                TQty = _locNSWInventoryTransOrderLine.TQty,
                                                                InventoryTransferOut = _locInventoryTransferOut2,
                                                                Company = _locInventoryTransferOut2.Company,
                                                                StockControlling = _locNSWInventoryTransOrderLine.StockControlling,
                                                            };
                                                            _saveDataInventoryTransferOutLine.Save();
                                                            _saveDataInventoryTransferOutLine.Session.CommitTransaction();

                                                            InventoryTransferOutLine _locInventoryTransferOutLine = _currSession.FindObject<InventoryTransferOutLine>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("SignCode", _locSignCodeITOL)));

                                                            if (_locInventoryTransferOutLine != null)
                                                            {
                                                                XPCollection<InventoryTransferOrderLot> _locInventoryTransferOrderLots = new XPCollection<InventoryTransferOrderLot>(_currSession,
                                                                                                                    new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("InventoryTransferOrderLine", _locNSWInventoryTransOrderLine),
                                                                                                                    new BinaryOperator("Select", true)));
                                                                if (_locInventoryTransferOrderLots != null && _locInventoryTransferOrderLots.Count() > 0)
                                                                {
                                                                    foreach (InventoryTransferOrderLot _locInventoryTransferOrderLot in _locInventoryTransferOrderLots)
                                                                    {
                                                                        InventoryTransferOutLot _saveDataInventoryTransferOutLot = new InventoryTransferOutLot(_currSession)
                                                                        {
                                                                            Select = true,
                                                                            Location = _locInventoryTransferOrderLot.LocationFrom,
                                                                            BinLocation = _locInventoryTransferOrderLot.BinLocationFrom,
                                                                            StockType = _locInventoryTransferOrderLot.StockTypeFrom,
                                                                            Item = _locInventoryTransferOrderLot.Item,
                                                                            BegInvLine = _locInventoryTransferOrderLot.BegInvLine,
                                                                            LotNumber = _locInventoryTransferOrderLot.LotNumber,
                                                                            DQty = _locInventoryTransferOrderLot.DQty,
                                                                            DUOM = _locInventoryTransferOrderLot.DUOM,
                                                                            Qty = _locInventoryTransferOrderLot.Qty,
                                                                            UOM = _locInventoryTransferOrderLot.UOM,
                                                                            TQty = _locInventoryTransferOrderLot.TQty,
                                                                            InventoryTransferOutLine = _locInventoryTransferOutLine,
                                                                            UnitPack = _locInventoryTransferOrderLot.UnitPack,
                                                                        };
                                                                        _saveDataInventoryTransferOutLot.Save();
                                                                        _saveDataInventoryTransferOutLot.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }
                                                        }

                                                    }

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    #endregion CreateInventoryTransferOutWithoutSameWarehouse

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferOrder ", ex.ToString());
            }
        }

        private bool CheckAvailableWarehouseSetupDetail(Session _currSession, Location _locLocation, UserAccess _locUserAccess)
        {
            bool _result = false;
            int _countResult = 0;
            try
            {
                if (_locUserAccess != null)
                {
                    if (_locLocation != null)
                    {

                        XPCollection<WarehouseSetupDetail> _locWhsSetupDetails = new XPCollection<WarehouseSetupDetail>(_currSession,
                                                                                        new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("UserAccess", _locUserAccess),
                                                                                        new BinaryOperator("Owner", true),
                                                                                        new BinaryOperator("Active", true)));
                        if (_locWhsSetupDetails != null && _locWhsSetupDetails.Count() > 0)
                        {
                            foreach (WarehouseSetupDetail _locWhsSetupDetail in _locWhsSetupDetails)
                            {
                                if (_locWhsSetupDetail.Location == _locLocation)
                                {
                                    _countResult = _countResult + 1;
                                }
                            }
                        }

                        if (_countResult > 0)
                        {
                            _result = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = TransferOrder ", ex.ToString());
            }
            return _result;
        }

        public bool GetHoldAccess(Session _currSession, UserAccess _numLineUserAccess)
        {
            bool _result = false;
            try
            {
                if (_numLineUserAccess != null)
                {
                    ApplicationSetup _numLineAppSetup = _currSession.FindObject<ApplicationSetup>(new GroupOperator
                                                (GroupOperatorType.And, new BinaryOperator("Active", true),
                                                new BinaryOperator("DefaultSystem", true)));

                    if (_numLineAppSetup != null)
                    {
                        SalesSetupDetail _locSalesSetupDetail = _currSession.FindObject<SalesSetupDetail>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                 new BinaryOperator("UserAccess", _numLineUserAccess),
                                                                 new BinaryOperator("Active", true),
                                                                 new BinaryOperator("Hold", true),
                                                                 new BinaryOperator("ApplicationSetup", _numLineAppSetup)));

                        if (_locSalesSetupDetail != null)
                        {
                            _result = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOrder " + ex.ToString());
            }
            return _result;
        }

        #endregion InventoryTransferOrder        

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

    }
}
