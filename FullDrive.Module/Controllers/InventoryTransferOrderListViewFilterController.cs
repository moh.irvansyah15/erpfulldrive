﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;
using FullDrive.Module.BusinessObjects;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class InventoryTransferOrderListViewFilterController : ViewController
    {
        public InventoryTransferOrderListViewFilterController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            RegisterActions(components);
            TargetViewId = "InventoryTransferOrder_ListView";
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
            var User = SecuritySystem.CurrentUserName;
            string _holdString1 = null;
            var ListView = View as ListView;
            Session currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
            UserAccess _locUserAccess = currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));

            if(_locUserAccess != null)
            {
                ApplicationSetup _numLineAppSetup = currentSession.FindObject<ApplicationSetup>(new GroupOperator
                                                (GroupOperatorType.And, new BinaryOperator("Active", true),
                                                new BinaryOperator("DefaultSystem", true)));

                if (_numLineAppSetup != null)
                {
                    SalesSetupDetail _locSalesSetupDetail = currentSession.FindObject<SalesSetupDetail>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                             new BinaryOperator("UserAccess", _locUserAccess),
                                                             new BinaryOperator("Active", true),
                                                             new BinaryOperator("Hold", true),
                                                             new BinaryOperator("ApplicationSetup", _numLineAppSetup)));

                    if (_locSalesSetupDetail != null)
                    {
                        _holdString1 = "[Hold]=='TRUE' OR [Hold]==1";
                    }
                }

                ListView.CollectionSource.Criteria["InventoryTransferFilter"] = CriteriaOperator.Parse(_holdString1);
            }
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }
    }
}
