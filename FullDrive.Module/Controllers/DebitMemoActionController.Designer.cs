﻿namespace FullDrive.Module.Controllers
{
    partial class DebitMemoActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.DebitMemoProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.DebitMemoPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.DebitMemoFilterAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // DebitMemoProgressAction
            // 
            this.DebitMemoProgressAction.Caption = "Progress";
            this.DebitMemoProgressAction.ConfirmationMessage = null;
            this.DebitMemoProgressAction.Id = "DebitMemoProgressActionId";
            this.DebitMemoProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.DebitMemo);
            this.DebitMemoProgressAction.ToolTip = null;
            this.DebitMemoProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.DebitMemoProgressAction_Execute);
            // 
            // DebitMemoPostingAction
            // 
            this.DebitMemoPostingAction.Caption = "Posting";
            this.DebitMemoPostingAction.ConfirmationMessage = null;
            this.DebitMemoPostingAction.Id = "DebitMemoPostingActionId";
            this.DebitMemoPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.DebitMemo);
            this.DebitMemoPostingAction.ToolTip = null;
            this.DebitMemoPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.DebitMemoPostingAction_Execute);
            // 
            // DebitMemoFilterAction
            // 
            this.DebitMemoFilterAction.Caption = "Filter";
            this.DebitMemoFilterAction.ConfirmationMessage = null;
            this.DebitMemoFilterAction.Id = "DebitMemoFilterActionId";
            this.DebitMemoFilterAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.DebitMemoFilterAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.DebitMemo);
            this.DebitMemoFilterAction.ToolTip = null;
            this.DebitMemoFilterAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.DebitMemoFilterAction_Execute);
            // 
            // DebitMemoActionController
            // 
            this.Actions.Add(this.DebitMemoProgressAction);
            this.Actions.Add(this.DebitMemoPostingAction);
            this.Actions.Add(this.DebitMemoFilterAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction DebitMemoProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction DebitMemoPostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction DebitMemoFilterAction;
    }
}
