﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PurchasePrePaymentActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;
        public PurchasePrePaymentActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            PurchasePrePaymentInvoiceListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                PurchasePrePaymentInvoiceListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void PurchasePrePaymentInvoiceProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchasePrePaymentInvoice _locPurchasePrePaymentInvoiceOS = (PurchasePrePaymentInvoice)_objectSpace.GetObject(obj);

                        if (_locPurchasePrePaymentInvoiceOS != null)
                        {
                            if (_locPurchasePrePaymentInvoiceOS.Code != null)
                            {
                                _currObjectId = _locPurchasePrePaymentInvoiceOS.Code;

                                PurchasePrePaymentInvoice _locPurchasePrePaymentInvoiceXPO = _currSession.FindObject<PurchasePrePaymentInvoice>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locPurchasePrePaymentInvoiceXPO != null)
                                {
                                    if (_locPurchasePrePaymentInvoiceXPO.Status == Status.Open )
                                    {
                            
                                        _locPurchasePrePaymentInvoiceXPO.Status = Status.Progress;
                                        _locPurchasePrePaymentInvoiceXPO.StatusDate = now;
                                        _locPurchasePrePaymentInvoiceXPO.Save();
                                        _locPurchasePrePaymentInvoiceXPO.Session.CommitTransaction();

                                        
                                        SuccessMessageShow(_locPurchasePrePaymentInvoiceXPO.Code + " has been change successfully to Progress");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Pre Payment Invoice Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data Purchase Pre Payment Invoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchasePrePaymentInvoice " + ex.ToString());
            }
        }

        private void PurchasePrePaymentInvoicePostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchasePrePaymentInvoice _locPurchasePrePaymentInvoiceOS = (PurchasePrePaymentInvoice)_objectSpace.GetObject(obj);

                        if (_locPurchasePrePaymentInvoiceOS != null)
                        {
                            if (_locPurchasePrePaymentInvoiceOS.Code != null)
                            {
                                _currObjectId = _locPurchasePrePaymentInvoiceOS.Code;

                                PurchasePrePaymentInvoice _locPurchasePrePaymentInvoiceXPO = _currSession.FindObject<PurchasePrePaymentInvoice>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locPurchasePrePaymentInvoiceXPO != null)
                                {
                                    if (_locPurchasePrePaymentInvoiceXPO.Status == Status.Progress || _locPurchasePrePaymentInvoiceXPO.Status == Status.Posted)
                                    {
                                        if (_locPurchasePrePaymentInvoiceXPO.Pay > 0)
                                        {
                                            if (GetAvailableGeneralJournal(_currSession, _locPurchasePrePaymentInvoiceXPO) == false)
                                            {
                                                SetPrePaymentJournal(_currSession, _locPurchasePrePaymentInvoiceXPO);
                                            }

                                            SetPayableTransaction(_currSession, _locPurchasePrePaymentInvoiceXPO);
                                            SetRemainAmount(_currSession, _locPurchasePrePaymentInvoiceXPO);
                                            SetRemainAmountSplitInvoiceForPIC(_currSession, _locPurchasePrePaymentInvoiceXPO);
                                            SetNormalPay(_currSession, _locPurchasePrePaymentInvoiceXPO);
                                            SetFinalPurchasePrePaymentInvoice(_currSession, _locPurchasePrePaymentInvoiceXPO);
                                            SuccessMessageShow(_locPurchasePrePaymentInvoiceXPO.Code + " has been change successfully to Posted");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Invoice Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data PurchaseInvoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchasePrePaymentInvoice " + ex.ToString());
            }
        }

        private void PurchasePrePaymentInvoiceListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(PurchasePrePaymentInvoice)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchasePrePaymentInvoice " + ex.ToString());
            }
        }

        #region PostingMethod

        private bool GetAvailableGeneralJournal(Session _currSession, PurchasePrePaymentInvoice _locPurchasePrePaymentInvoiceXPO)
        {
            bool _result = false;
            try
            {
                XPCollection<GeneralJournal> _locGeneralJournals = new XPCollection<GeneralJournal>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("PurchasePrePaymentInvoice", _locPurchasePrePaymentInvoiceXPO)));

                if (_locGeneralJournals != null && _locGeneralJournals.Count() > 0)
                {
                    _result = true;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchasePrePaymentInvoice ", ex.ToString());
            }

            return _result;
        }

        private void SetPrePaymentJournal(Session _currSession, PurchasePrePaymentInvoice _locPurchasePrePaymentInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locDP = 0;
                
                #region CreateInvoiceAPJournalDownPayment

                if (_locPurchasePrePaymentInvoiceXPO.Pay > 0)
                {
                    _locDP = _locPurchasePrePaymentInvoiceXPO.MaxPay;

                    #region JournalMapCompanyAccountGroup
                    if (_locPurchasePrePaymentInvoiceXPO.Company != null)
                    {
                        if (_locPurchasePrePaymentInvoiceXPO.Company.CompanyAccountGroup != null)
                        {
                            XPCollection<JournalMap> _locJournalMapByCompanys = new XPCollection<JournalMap>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("CompanyAccountGroup", _locPurchasePrePaymentInvoiceXPO.Company.CompanyAccountGroup)));

                            if (_locJournalMapByCompanys != null && _locJournalMapByCompanys.Count() > 0)
                            {
                                foreach (JournalMap _locJournalMapByCompany in _locJournalMapByCompanys)
                                {
                                    XPCollection<JournalMapLine> _locJournalMapLineByCompanys = new XPCollection<JournalMapLine>
                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                         new BinaryOperator("JournalMap", _locJournalMapByCompany)));

                                    if (_locJournalMapLineByCompanys != null && _locJournalMapLineByCompanys.Count() > 0)
                                    {
                                        foreach (JournalMapLine _locJournalMapLineByCompany in _locJournalMapLineByCompanys)
                                        {
                                            AccountMap _locAccountMapByBusinessPartner = _currSession.FindObject<AccountMap>
                                                                                         (new GroupOperator(GroupOperatorType.And,
                                                                                          new BinaryOperator("Code", _locJournalMapLineByCompany.AccountMap.Code),
                                                                                          new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                          new BinaryOperator("OrderType", OrderType.Item),
                                                                                          new BinaryOperator("PostingMethod", PostingMethod.PrePayment),
                                                                                          new BinaryOperator("PostingMethodType", PostingMethodType.DownPayment)));

                                            if (_locAccountMapByBusinessPartner != null)
                                            {
                                                XPCollection<AccountMapLine> _locAccountMapLineByBusinessPartners = new XPCollection<AccountMapLine>
                                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                     new BinaryOperator("AccountMap", _locAccountMapByBusinessPartner),
                                                                                                                     new BinaryOperator("Active", true)));

                                                if (_locAccountMapLineByBusinessPartners != null && _locAccountMapLineByBusinessPartners.Count() > 0)
                                                {
                                                    double _locTotalAmountDebit = 0;
                                                    double _locTotalAmountCredit = 0;
                                                    double _locTotalBalance = 0;

                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLineByBusinessPartners)
                                                    {
                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                        {
                                                            _locTotalAmountDebit = _locDP;
                                                        }
                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                        {
                                                            _locTotalAmountCredit = _locDP;
                                                        }

                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                        {
                                                            PostingDate = now,
                                                            PostingType = PostingType.Purchase,
                                                            PostingMethod = PostingMethod.PrePayment,
                                                            Account = _locAccountMapLine.Account,
                                                            Debit = _locTotalAmountDebit,
                                                            Credit = _locTotalAmountCredit,
                                                            PurchaseOrder = _locPurchasePrePaymentInvoiceXPO.PurchaseOrder,
                                                            PurchasePrePaymentInvoice = _locPurchasePrePaymentInvoiceXPO,
                                                            Company = _locPurchasePrePaymentInvoiceXPO.Company,
                                                        };
                                                        _saveGeneralJournal.Save();
                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                        if (_locAccountMapLine.Account.Code != null)
                                                        {
                                                            ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                      new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                            if (_locCOA != null)
                                                            {
                                                                if (_locCOA.BalanceType == BalanceType.Change)
                                                                {
                                                                    if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                    {
                                                                        if (_locTotalAmountDebit > 0)
                                                                        {
                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                        }
                                                                        if (_locTotalAmountCredit > 0)
                                                                        {
                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                        }
                                                                    }
                                                                    if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                    {
                                                                        if (_locTotalAmountDebit > 0)
                                                                        {
                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                        }
                                                                        if (_locTotalAmountCredit > 0)
                                                                        {
                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                        }
                                                                    }
                                                                }

                                                                _locCOA.Balance = _locTotalBalance;
                                                                _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                _locCOA.Save();
                                                                _locCOA.Session.CommitTransaction();
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion JournalMapCompanyAccountGroup
                    #region JournalMapBusinessPartnerAccountGroup
                    if (_locPurchasePrePaymentInvoiceXPO.PayToVendor != null)
                    {
                        if (_locPurchasePrePaymentInvoiceXPO.PayToVendor.BusinessPartnerAccountGroup != null)
                        {
                            XPCollection<JournalMap> _locJournalMapByBusinessPartners = new XPCollection<JournalMap>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("BusinessPartnerAccountGroup", _locPurchasePrePaymentInvoiceXPO.PayToVendor.BusinessPartnerAccountGroup)));

                            if (_locJournalMapByBusinessPartners != null && _locJournalMapByBusinessPartners.Count() > 0)
                            {
                                foreach (JournalMap _locJournalMapByBusinessPartner in _locJournalMapByBusinessPartners)
                                {
                                    XPCollection<JournalMapLine> _locJournalMapLineByBusinessPartners = new XPCollection<JournalMapLine>
                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                         new BinaryOperator("JournalMap", _locJournalMapByBusinessPartner)));

                                    if (_locJournalMapLineByBusinessPartners != null && _locJournalMapLineByBusinessPartners.Count() > 0)
                                    {
                                        foreach (JournalMapLine _locJournalMapLineByBusinessPartner in _locJournalMapLineByBusinessPartners)
                                        {
                                            AccountMap _locAccountMapByBusinessPartner = _currSession.FindObject<AccountMap>
                                                                                         (new GroupOperator(GroupOperatorType.And,
                                                                                          new BinaryOperator("Code", _locJournalMapLineByBusinessPartner.AccountMap.Code),
                                                                                          new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                          new BinaryOperator("OrderType", OrderType.Item),
                                                                                          new BinaryOperator("PostingMethod", PostingMethod.PrePayment),
                                                                                          new BinaryOperator("PostingMethodType", PostingMethodType.DownPayment)));

                                            if (_locAccountMapByBusinessPartner != null)
                                            {
                                                XPCollection<AccountMapLine> _locAccountMapLineByBusinessPartners = new XPCollection<AccountMapLine>
                                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                     new BinaryOperator("AccountMap", _locAccountMapByBusinessPartner),
                                                                                                                     new BinaryOperator("Active", true)));

                                                if (_locAccountMapLineByBusinessPartners != null && _locAccountMapLineByBusinessPartners.Count() > 0)
                                                {
                                                    double _locTotalAmountDebit = 0;
                                                    double _locTotalAmountCredit = 0;
                                                    double _locTotalBalance = 0;

                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLineByBusinessPartners)
                                                    {
                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                        {
                                                            _locTotalAmountDebit = _locDP;
                                                        }
                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                        {
                                                            _locTotalAmountCredit = _locDP;
                                                        }

                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                        {
                                                            PostingDate = now,
                                                            PostingType = PostingType.Purchase,
                                                            PostingMethod = PostingMethod.PrePayment,
                                                            Account = _locAccountMapLine.Account,
                                                            Debit = _locTotalAmountDebit,
                                                            Credit = _locTotalAmountCredit,
                                                            PurchaseOrder = _locPurchasePrePaymentInvoiceXPO.PurchaseOrder,
                                                            PurchasePrePaymentInvoice = _locPurchasePrePaymentInvoiceXPO,
                                                            Company = _locPurchasePrePaymentInvoiceXPO.Company,
                                                        };
                                                        _saveGeneralJournal.Save();
                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                        if (_locAccountMapLine.Account.Code != null)
                                                        {
                                                            ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                      new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                            if (_locCOA != null)
                                                            {
                                                                if (_locCOA.BalanceType == BalanceType.Change)
                                                                {
                                                                    if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                    {
                                                                        if (_locTotalAmountDebit > 0)
                                                                        {
                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                        }
                                                                        if (_locTotalAmountCredit > 0)
                                                                        {
                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                        }
                                                                    }
                                                                    if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                    {
                                                                        if (_locTotalAmountDebit > 0)
                                                                        {
                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                        }
                                                                        if (_locTotalAmountCredit > 0)
                                                                        {
                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                        }
                                                                    }
                                                                }

                                                                _locCOA.Balance = _locTotalBalance;
                                                                _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                _locCOA.Save();
                                                                _locCOA.Session.CommitTransaction();
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion JournalMapBusinessPartnerAccountGroup
                }

                #endregion CreateInvoiceAPJournalDownPayment

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchasePrePaymentInvoice ", ex.ToString());
            }
        }

        private void SetPayableTransaction(Session _currSession, PurchasePrePaymentInvoice _locPurchasePrePaymentInvoiceXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _locSignCode = null;
                double _locTotalUnitPrice = _locPurchasePrePaymentInvoiceXPO.Pay;
                if (_locPurchasePrePaymentInvoiceXPO != null)
                {
                    if (_locPurchasePrePaymentInvoiceXPO.PayToVendor != null)
                    {
                        if (_locPurchasePrePaymentInvoiceXPO.PayToVendor.Code != null)
                        {
                            BusinessPartner _locBusinessPartner = _currSession.FindObject<BusinessPartner>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Code", _locPurchasePrePaymentInvoiceXPO.PayToVendor.Code)));


                            if (_locBusinessPartner != null)
                            {
                                _locSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.PayableTransaction);
                                if (_locSignCode != null)
                                {
                                    PayableTransaction _savePayableTransaction = new PayableTransaction(_currSession)
                                    {
                                        PostingType = PostingType.Purchase,
                                        PostingMethod = PostingMethod.Payment,
                                        PaymentMethod = _locPurchasePrePaymentInvoiceXPO.PaymentMethod,
                                        EstimatedDate = _locPurchasePrePaymentInvoiceXPO.EstimatedDate,
                                        SignCode = _locSignCode,
                                        PurchaseOrder = _locPurchasePrePaymentInvoiceXPO.PurchaseOrder,
                                        PurchasePrePaymentInvoice = _locPurchasePrePaymentInvoiceXPO,
                                        CompanyDefault = _locPurchasePrePaymentInvoiceXPO.Company,
                                    };
                                    _savePayableTransaction.Save();
                                    _savePayableTransaction.Session.CommitTransaction();

                                    PayableTransaction _locPayableTransaction = _currSession.FindObject<PayableTransaction>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("SignCode", _locSignCode)));
                                    if (_locPayableTransaction != null)
                                    {
                                        //Create Mapping for BusinessPartner
                                        if (_locBusinessPartner.BusinessPartnerAccountGroup != null)
                                        {
                                            PayableTransactionLine _savePayableTransactionLine = new PayableTransactionLine(_currSession)
                                            {
                                                PostingType = PostingType.Purchase,
                                                PostingMethod = PostingMethod.Payment,
                                                OpenVendor = true,
                                                Vendor = _locBusinessPartner,
                                                BankAccount = _locPurchasePrePaymentInvoiceXPO.BankAccount,
                                                PayableTransaction = _locPayableTransaction,
                                                CloseCredit = true,
                                                Debit = _locPurchasePrePaymentInvoiceXPO.Pay,
                                                CompanyDefault = _locPayableTransaction.CompanyDefault,
                                            };
                                            _savePayableTransactionLine.Save();
                                            _savePayableTransactionLine.Session.CommitTransaction();
                                        }

                                        //Create Mapping For CompanyDefault
                                        if (_locPayableTransaction.CompanyDefault != null)
                                        {
                                            BankAccount _locBankAccount = _currSession.FindObject<BankAccount>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", _locPayableTransaction.CompanyDefault),
                                                                            new BinaryOperator("Default", true),
                                                                            new BinaryOperator("Active", true)));
                                            if (_locBankAccount != null)
                                            {
                                                PayableTransactionLine _savePayableTransactionLine = new PayableTransactionLine(_currSession)
                                                {
                                                    PostingType = PostingType.Purchase,
                                                    PostingMethod = PostingMethod.Payment,
                                                    OpenCompany = true,
                                                    Company = _locPayableTransaction.CompanyDefault,
                                                    BankAccount = _locBankAccount,
                                                    PayableTransaction = _locPayableTransaction,
                                                    CloseDebit = true,
                                                    Credit = _locPurchasePrePaymentInvoiceXPO.Pay,
                                                    CompanyDefault = _locPayableTransaction.CompanyDefault,
                                                };
                                                _savePayableTransactionLine.Save();
                                                _savePayableTransactionLine.Session.CommitTransaction();
                                            }

                                        }

                                        //Tambahkan PaymentOutPlan
                                        SetPaymentOutPlan(_currSession, _locPurchasePrePaymentInvoiceXPO, _locPayableTransaction);
                                    }
                                }
                            }
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseInvoice ", ex.ToString());
            }
        }

        private void SetPaymentOutPlan(Session _currSession, PurchasePrePaymentInvoice _locPurchasePrePaymentInvoiceXPO, PayableTransaction _PayableTransaction)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                double _locTotalUnitPrice = _locPurchasePrePaymentInvoiceXPO.Pay;
                PurchaseInvoiceCollection _locPurchaseInvoiceCollection = null;

                if (_locPurchasePrePaymentInvoiceXPO != null && _PayableTransaction != null)
                {
                    if (_locPurchasePrePaymentInvoiceXPO.PurchaseOrder != null)
                    {
                        _locPurchaseInvoiceCollection = _currSession.FindObject<PurchaseInvoiceCollection>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("PurchaseOrder", _locPurchasePrePaymentInvoiceXPO.PurchaseOrder)));

                        if (_locPurchaseInvoiceCollection != null)
                        {
                            PaymentOutPlan _savePaymentOutPlan = new PaymentOutPlan(_currSession)
                            {
                                PaymentMethod = _locPurchasePrePaymentInvoiceXPO.PaymentMethod,
                                EstimatedDate = _locPurchasePrePaymentInvoiceXPO.EstimatedDate,
                                Plan = _locPurchasePrePaymentInvoiceXPO.Pay,
                                Outstanding = _locPurchasePrePaymentInvoiceXPO.Pay,
                                PurchaseOrder = _locPurchasePrePaymentInvoiceXPO.PurchaseOrder,
                                PurchasePrePaymentInvoice = _locPurchasePrePaymentInvoiceXPO,
                                PayableTransaction = _PayableTransaction,
                                PurchaseInvoiceCollection = _locPurchaseInvoiceCollection,
                                Company = _locPurchasePrePaymentInvoiceXPO.Company,
                            };
                            _savePaymentOutPlan.Save();
                            _savePaymentOutPlan.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseInvoice ", ex.ToString());
            }
        }

        private void SetRemainAmount(Session _currSession, PurchasePrePaymentInvoice _locPurchasePrePaymentInvoiceXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locPurchasePrePaymentInvoiceXPO != null)
                {
                    double _locPlan = 0;
                    double _locOutstanding = 0;
                    double _locMaxPay = 0;
                    bool _locActivationPosting = false;
                    Status _locStatus = Status.Posted;

                    #region RemainAmountWithPostedCount!=0

                    if (_locPurchasePrePaymentInvoiceXPO.PostedCount > 0)
                    {
                        _locMaxPay = _locPurchasePrePaymentInvoiceXPO.MaxPay;
                        if (_locPurchasePrePaymentInvoiceXPO.Outstanding > 0 && _locMaxPay != _locPurchasePrePaymentInvoiceXPO.Plan)
                        {
                            if (_locPurchasePrePaymentInvoiceXPO.Pay > 0)
                            {
                                _locPlan = _locPurchasePrePaymentInvoiceXPO.Plan + _locPurchasePrePaymentInvoiceXPO.Pay;
                                _locOutstanding = _locPurchasePrePaymentInvoiceXPO.Outstanding - _locPurchasePrePaymentInvoiceXPO.Pay;

                                if (_locOutstanding == 0 && _locPlan == _locMaxPay)
                                {
                                    _locActivationPosting = true;
                                    _locStatus = Status.Posted;
                                }

                                _locPurchasePrePaymentInvoiceXPO.Plan = _locPlan;
                                _locPurchasePrePaymentInvoiceXPO.Outstanding = _locOutstanding;
                                _locPurchasePrePaymentInvoiceXPO.Status = _locStatus;
                                _locPurchasePrePaymentInvoiceXPO.StatusDate = now;
                                _locPurchasePrePaymentInvoiceXPO.ActivationPosting = _locActivationPosting;
                                _locPurchasePrePaymentInvoiceXPO.PostedCount = _locPurchasePrePaymentInvoiceXPO.PostedCount + 1;
                                _locPurchasePrePaymentInvoiceXPO.Save();
                                _locPurchasePrePaymentInvoiceXPO.Session.CommitTransaction();
                            }
                        }
                    }
                    #endregion RemainAmountWithPostedCount!=0

                    #region RemainAmountWithPostedCount=0
                    else
                    {
                        if (_locPurchasePrePaymentInvoiceXPO.Pay > 0)
                        {
                            _locPlan = _locPurchasePrePaymentInvoiceXPO.Pay;
                            _locMaxPay = _locPurchasePrePaymentInvoiceXPO.MaxPay;
                            _locOutstanding = _locMaxPay - _locPlan;


                            if (_locPlan == _locMaxPay && _locOutstanding == 0)
                            {
                                _locStatus = Status.Posted;
                                _locActivationPosting = true;
                            }

                            _locPurchasePrePaymentInvoiceXPO.Plan = _locPlan;
                            _locPurchasePrePaymentInvoiceXPO.Outstanding = _locOutstanding;
                            _locPurchasePrePaymentInvoiceXPO.Status = _locStatus;
                            _locPurchasePrePaymentInvoiceXPO.StatusDate = now;
                            _locPurchasePrePaymentInvoiceXPO.ActivationPosting = _locActivationPosting;
                            _locPurchasePrePaymentInvoiceXPO.PostedCount = _locPurchasePrePaymentInvoiceXPO.PostedCount + 1;
                            _locPurchasePrePaymentInvoiceXPO.Save();
                            _locPurchasePrePaymentInvoiceXPO.Session.CommitTransaction();

                        }
                    }
                    #endregion RemainAmountWithPostedCount=0
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchasePrePaymentInvoice ", ex.ToString());
            }
        }

        private void SetRemainAmountSplitInvoiceForPIC(Session _currSession, PurchasePrePaymentInvoice _locPurchasePrePaymentInvoiceXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                double _locPlan = 0;
                double _locOutstanding = 0;
                double _locMaxPay = 0;
                double _locDP_Amount = 0;
                bool _locDP_Posting = false;
                bool _locActivationPosting = false;
                Status _locStatus = Status.Posted;

                if (_locPurchasePrePaymentInvoiceXPO != null)
                {
                    if (_locPurchasePrePaymentInvoiceXPO.PurchaseOrder != null)
                    {
                        PurchaseInvoiceCollection _locPurchaseInvoiceCollection = _currSession.FindObject<PurchaseInvoiceCollection>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("PurchaseOrder", _locPurchasePrePaymentInvoiceXPO.PurchaseOrder)));
                        if (_locPurchaseInvoiceCollection != null)
                        {
                            #region RemainAmountWithPostedCount!=0

                            if (_locPurchaseInvoiceCollection.PostedCount > 0)
                            {
                                _locMaxPay = _locPurchaseInvoiceCollection.MaxPay - _locPurchaseInvoiceCollection.CM_Amount;
                                if (_locPurchaseInvoiceCollection.Outstanding > 0 && _locPurchaseInvoiceCollection.MaxPay != _locPurchaseInvoiceCollection.Plan)
                                {
                                    if (_locPurchasePrePaymentInvoiceXPO.Pay > 0)
                                    {

                                        if (_locPurchasePrePaymentInvoiceXPO.Pay <= _locPurchaseInvoiceCollection.DP_Amount)
                                        {
                                            _locDP_Amount = _locPurchaseInvoiceCollection.DP_Amount - _locPurchasePrePaymentInvoiceXPO.Pay;
                                        }
                                        else
                                        {
                                            _locDP_Amount = 0;
                                        }

                                        if (_locDP_Amount > 0)
                                        {
                                            _locDP_Posting = true;
                                        }

                                        _locPlan = _locPurchaseInvoiceCollection.Plan + _locPurchasePrePaymentInvoiceXPO.Pay;
                                        _locOutstanding = _locPurchaseInvoiceCollection.Outstanding - _locPurchasePrePaymentInvoiceXPO.Pay;

                                        if (_locOutstanding == 0 && _locPlan == _locMaxPay)
                                        {
                                            _locActivationPosting = true;
                                            _locStatus = Status.Posted;
                                        }

                                        _locPurchaseInvoiceCollection.DP_Posting = _locDP_Posting;
                                        _locPurchaseInvoiceCollection.DP_Amount = _locDP_Amount;
                                        _locPurchaseInvoiceCollection.Plan = _locPlan;
                                        _locPurchaseInvoiceCollection.Outstanding = _locOutstanding;
                                        _locPurchaseInvoiceCollection.Status = _locStatus;
                                        _locPurchaseInvoiceCollection.StatusDate = now;
                                        _locPurchaseInvoiceCollection.ActivationPosting = _locActivationPosting;
                                        _locPurchaseInvoiceCollection.PostedCount = _locPurchaseInvoiceCollection.PostedCount + 1;
                                        _locPurchaseInvoiceCollection.Save();
                                        _locPurchaseInvoiceCollection.Session.CommitTransaction();

                                        PurchaseInvoiceCollectionLine _locPurchaseInvoiceCollectionPC = _currSession.FindObject<PurchaseInvoiceCollectionLine>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                     new BinaryOperator("PurchaseInvoiceCollection", _locPurchaseInvoiceCollection),
                                                                                                     new BinaryOperator("PurchasePrePaymentInvoice", _locPurchasePrePaymentInvoiceXPO)));
                                        if (_locPurchaseInvoiceCollectionPC == null)
                                        {
                                            PurchaseInvoiceCollectionLine _saveDataPurchaseInvoiceCollectionLine = new PurchaseInvoiceCollectionLine(_currSession)
                                            {
                                                PurchasePrePaymentInvoice = _locPurchasePrePaymentInvoiceXPO,
                                                PurchaseInvoiceCollection = _locPurchaseInvoiceCollection,
                                            };
                                            _saveDataPurchaseInvoiceCollectionLine.Save();
                                            _saveDataPurchaseInvoiceCollectionLine.Session.CommitTransaction();
                                        }
                                    }
                                }
                            }
                            #endregion RemainAmountWithPostedCount!=0

                            #region RemainAmountWithPostedCount=0
                            else
                            {
                                if (_locPurchasePrePaymentInvoiceXPO.Pay > 0)
                                {
                                    if (_locPurchasePrePaymentInvoiceXPO.Pay <= _locPurchaseInvoiceCollection.DP_Amount)
                                    {
                                        _locDP_Amount = _locPurchaseInvoiceCollection.DP_Amount - _locPurchasePrePaymentInvoiceXPO.Pay;
                                    }
                                    else
                                    {
                                        _locDP_Amount = 0;
                                    }

                                    if (_locDP_Amount > 0)
                                    {
                                        _locDP_Posting = true;
                                    }

                                    _locPlan = _locPurchasePrePaymentInvoiceXPO.Pay;
                                    _locMaxPay = _locPurchaseInvoiceCollection.MaxPay - _locPurchaseInvoiceCollection.CM_Amount;
                                    _locOutstanding = _locMaxPay - _locPurchasePrePaymentInvoiceXPO.Pay;

                                    if (_locPlan == _locMaxPay && _locOutstanding == 0)
                                    {
                                        _locStatus = Status.Posted;
                                        _locActivationPosting = true;
                                    }

                                    _locPurchaseInvoiceCollection.DP_Posting = _locDP_Posting;
                                    _locPurchaseInvoiceCollection.DP_Amount = _locDP_Amount;
                                    _locPurchaseInvoiceCollection.Plan = _locPlan;
                                    _locPurchaseInvoiceCollection.Outstanding = _locOutstanding;
                                    _locPurchaseInvoiceCollection.Status = _locStatus;
                                    _locPurchaseInvoiceCollection.StatusDate = now;
                                    _locPurchaseInvoiceCollection.ActivationPosting = _locActivationPosting;
                                    _locPurchaseInvoiceCollection.PostedCount = _locPurchaseInvoiceCollection.PostedCount + 1;
                                    _locPurchaseInvoiceCollection.Save();
                                    _locPurchaseInvoiceCollection.Session.CommitTransaction();

                                    PurchaseInvoiceCollectionLine _locPurchaseInvoiceCollectionPC = _currSession.FindObject<PurchaseInvoiceCollectionLine>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                     new BinaryOperator("PurchaseInvoiceCollection", _locPurchaseInvoiceCollection),
                                                                                                     new BinaryOperator("PurchasePrePaymentInvoice", _locPurchasePrePaymentInvoiceXPO)));
                                    if (_locPurchaseInvoiceCollectionPC == null)
                                    {
                                        PurchaseInvoiceCollectionLine _saveDataPurchaseInvoiceCollectionLine = new PurchaseInvoiceCollectionLine(_currSession)
                                        {
                                            PurchasePrePaymentInvoice = _locPurchasePrePaymentInvoiceXPO,
                                            PurchaseInvoiceCollection = _locPurchaseInvoiceCollection,
                                        };
                                        _saveDataPurchaseInvoiceCollectionLine.Save();
                                        _saveDataPurchaseInvoiceCollectionLine.Session.CommitTransaction();
                                    }
                                }
                            }
                            #endregion RemainAmountWithPostedCount=0
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchasePrePaymentInvoice ", ex.ToString());
            }
        }

        private void SetNormalPay(Session _currSession, PurchasePrePaymentInvoice _locPurchasePrePaymentInvoiceXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locPurchasePrePaymentInvoiceXPO != null)
                {
                    _locPurchasePrePaymentInvoiceXPO.Pay = 0;
                    _locPurchasePrePaymentInvoiceXPO.Save();
                    _locPurchasePrePaymentInvoiceXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchasePrePaymentInvoice ", ex.ToString());
            }
        }

        private void SetFinalPurchasePrePaymentInvoice(Session _currSession, PurchasePrePaymentInvoice _locPurchasePrePaymentInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locMaxPay = 0;

                if (_locPurchasePrePaymentInvoiceXPO != null)
                {

                    if (_locMaxPay == _locPurchasePrePaymentInvoiceXPO.Plan && _locPurchasePrePaymentInvoiceXPO.Outstanding == 0)
                    {
                        _locPurchasePrePaymentInvoiceXPO.ActivationPosting = true;
                        _locPurchasePrePaymentInvoiceXPO.Status = Status.Posted;
                        _locPurchasePrePaymentInvoiceXPO.StatusDate = now;
                        _locPurchasePrePaymentInvoiceXPO.Save();
                        _locPurchasePrePaymentInvoiceXPO.Session.CommitTransaction();
                    }
                    else
                    {
                        _locPurchasePrePaymentInvoiceXPO.Status = Status.Posted;
                        _locPurchasePrePaymentInvoiceXPO.StatusDate = now;
                        _locPurchasePrePaymentInvoiceXPO.Save();
                        _locPurchasePrePaymentInvoiceXPO.Session.CommitTransaction();
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchasePrePaymentInvoice " + ex.ToString());
            }
        }

        #endregion PostingMethod

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }


        #endregion Global Method

        
    }
}
