﻿namespace FullDrive.Module.Controllers
{
    partial class ProductionApprovalActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ProductionApprovalAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // ProductionApprovalAction
            // 
            this.ProductionApprovalAction.Caption = "Approval";
            this.ProductionApprovalAction.ConfirmationMessage = null;
            this.ProductionApprovalAction.Id = "ProductionApprovalActionId";
            this.ProductionApprovalAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.ProductionApprovalAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.Production);
            this.ProductionApprovalAction.ToolTip = null;
            this.ProductionApprovalAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.ProductionApprovalAction_Execute);
            // 
            // ProductionApprovalActionController
            // 
            this.Actions.Add(this.ProductionApprovalAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction ProductionApprovalAction;
    }
}
