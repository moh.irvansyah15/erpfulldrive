﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using FullDrive.Module.BusinessObjects;
using DevExpress.ExpressApp.Xpo;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ReceivableTransactionLineActionController : ViewController
    {
        public ReceivableTransactionLineActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void ReceivableTransactionLineExchangeRateAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        ReceivableTransactionLine _locReceivableTransactionLineOS = (ReceivableTransactionLine)_objectSpace.GetObject(obj);

                        if (_locReceivableTransactionLineOS != null)
                        {
                            if (_locReceivableTransactionLineOS.Debit != 0 && _locReceivableTransactionLineOS.Credit != 0)
                            {
                                if (_locReceivableTransactionLineOS.Code != null)
                                {
                                    _currObjectId = _locReceivableTransactionLineOS.Code;

                                    ReceivableTransactionLine _locReceivableTransactionLineXPO = _currSession.FindObject<ReceivableTransactionLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Code", _currObjectId)));

                                    if (_locReceivableTransactionLineXPO != null)
                                    {
                                        GetChangeRates(_currSession, _locReceivableTransactionLineOS);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = ReceivableTransactionLine" + ex.ToString());
            }
        }

        #region Method

        private void GetChangeRates(Session _currSession, ReceivableTransactionLine _receivableTransactionLine)
        {
            try
            {
                XPCollection<ExchangeRate> _locExchangeRates = new XPCollection<ExchangeRate>(_currSession,
                                                               new BinaryOperator("ReceivableTransactionLine", _receivableTransactionLine));

                if (_locExchangeRates.Count() > 0)
                {
                    foreach (ExchangeRate _locExchangeRate in _locExchangeRates)
                    {
                        if (_locExchangeRate.Rate > 0 && _locExchangeRate.BillDebit > 0 || _locExchangeRate.BillCredit > 0)
                        {
                            ReceivableTransactionLine _locReceivableTransactionLine = _currSession.FindObject<ReceivableTransactionLine>
                                                                                      (new GroupOperator(GroupOperatorType.And,
                                                                                       new BinaryOperator("Code", _receivableTransactionLine.Code)));

                            if (_receivableTransactionLine != null)
                            {
                                _receivableTransactionLine.Debit = _locExchangeRate.ForeignAmountDebit;
                                _receivableTransactionLine.Credit = _locExchangeRate.ForeignAmountCredit;
                                _receivableTransactionLine.Save();
                                _receivableTransactionLine.Session.CommitTransaction();
                            }
                        }
                        else
                        {
                            ReceivableTransactionLine _locReceivableTransactionLine = _currSession.FindObject<ReceivableTransactionLine>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                 new BinaryOperator("Code", _receivableTransactionLine.Code)));

                            _receivableTransactionLine.Debit = 0;
                            _receivableTransactionLine.Credit = 0;
                            _receivableTransactionLine.Save();
                            _receivableTransactionLine.Session.CommitTransaction();
                        }
                    }
                }
                else
                {
                    ReceivableTransactionLine _locReceivableTransactionLine = _currSession.FindObject<ReceivableTransactionLine>
                                                                              (new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Code", _receivableTransactionLine.Code)));

                    _receivableTransactionLine.Debit = _receivableTransactionLine.Debit;
                    _receivableTransactionLine.Credit = _receivableTransactionLine.Credit;
                    _receivableTransactionLine.Save();
                    _receivableTransactionLine.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = ReceivableTransactionLine" + ex.ToString());
            }
        }

        #endregion Method

    }
}
