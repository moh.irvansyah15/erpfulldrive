﻿namespace FullDrive.Module.Controllers
{
    partial class SalesInvoiceLineActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SalesInvoiceLineTaxCalculationAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesInvoiceLineDiscCalculationAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // SalesInvoiceLineTaxCalculationAction
            // 
            this.SalesInvoiceLineTaxCalculationAction.Caption = "Tax Calculation";
            this.SalesInvoiceLineTaxCalculationAction.ConfirmationMessage = null;
            this.SalesInvoiceLineTaxCalculationAction.Id = "SalesInvoiceLineTaxCalculationActionId";
            this.SalesInvoiceLineTaxCalculationAction.ToolTip = null;
            this.SalesInvoiceLineTaxCalculationAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesInvoiceLineTaxCalculationAction_Execute);
            // 
            // SalesInvoiceLineDiscCalculationAction
            // 
            this.SalesInvoiceLineDiscCalculationAction.Caption = "Disc Calculation";
            this.SalesInvoiceLineDiscCalculationAction.ConfirmationMessage = null;
            this.SalesInvoiceLineDiscCalculationAction.Id = "SalesInvoiceLineDiscCalculationActionId";
            this.SalesInvoiceLineDiscCalculationAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesInvoiceLine);
            this.SalesInvoiceLineDiscCalculationAction.ToolTip = null;
            this.SalesInvoiceLineDiscCalculationAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesInvoiceLineDiscCalculationAction_Execute);
            // 
            // SalesInvoiceLineActionController
            // 
            this.Actions.Add(this.SalesInvoiceLineTaxCalculationAction);
            this.Actions.Add(this.SalesInvoiceLineDiscCalculationAction);
            this.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesInvoiceLine);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction SalesInvoiceLineTaxCalculationAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesInvoiceLineDiscCalculationAction;
    }
}
