﻿namespace FullDrive.Module.Controllers
{
    partial class CashAdvanceApprovalController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CashAdvanceApprovalActionController = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // CashAdvanceApprovalActionController
            // 
            this.CashAdvanceApprovalActionController.Caption = "Approval";
            this.CashAdvanceApprovalActionController.ConfirmationMessage = null;
            this.CashAdvanceApprovalActionController.Id = "CashAdvanceApprovalActionControllerId";
            this.CashAdvanceApprovalActionController.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.CashAdvanceApprovalActionController.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.CashAdvance);
            this.CashAdvanceApprovalActionController.ToolTip = null;
            this.CashAdvanceApprovalActionController.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.CashAdvanceApprovalActionController_Execute);
            // 
            // CashAdvanceApprovalController
            // 
            this.Actions.Add(this.CashAdvanceApprovalActionController);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction CashAdvanceApprovalActionController;
    }
}
