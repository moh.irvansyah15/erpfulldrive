﻿namespace FullDrive.Module.Controllers
{
    partial class PaymentRealizationApprovalActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PaymentRealizationApprovalAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // PaymentRealizationApprovalAction
            // 
            this.PaymentRealizationApprovalAction.Caption = "Approval";
            this.PaymentRealizationApprovalAction.ConfirmationMessage = null;
            this.PaymentRealizationApprovalAction.Id = "PaymentRealizationApprovalActionId";
            this.PaymentRealizationApprovalAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.PaymentRealizationApprovalAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PaymentRealization);
            this.PaymentRealizationApprovalAction.ToolTip = null;
            this.PaymentRealizationApprovalAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.PaymentRealizationApprovalAction_Execute);
            // 
            // PaymentRealizationApprovalActionController
            // 
            this.Actions.Add(this.PaymentRealizationApprovalAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction PaymentRealizationApprovalAction;
    }
}
