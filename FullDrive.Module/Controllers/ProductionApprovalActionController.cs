﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Xpo;
using System.Web.UI.WebControls;
using System.Collections;
using DevExpress.Xpo;

#region Email

using System.Net;
using System.Web;
using System.IO;
using System.Data;
using System.Web.UI;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.Configuration;

#endregion Email

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ProductionApprovalActionController : ViewController
    {
        #region Default

        private ChoiceActionItem _setApprovalLevel;
        public ProductionApprovalActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            TargetViewId = "Production_ListView";
            RegisterActions(components);
        }
        protected override void OnActivated()
        {
            TypeOfView = typeof(ObjectView);
            base.OnActivated();
            // Perform various tasks depending on the target View.

            var _locUser = SecuritySystem.CurrentUserName;
            GlobalFunction _globalFunction = new GlobalFunction();
            Session _currentSession = null;
            if (this.ObjectSpace != null)
            {
                _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
            }

            if (_currentSession != null)
            {
                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", _locUser));

                if (_locUserAccess != null)
                {
                    ProductionApprovalAction.Items.Clear();

                    XPCollection<ApplicationSetupDetail> _locAppSetupDetails = new XPCollection<ApplicationSetupDetail>
                                                                               (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("UserAccess", _locUserAccess),
                                                                                new BinaryOperator("ObjectList", CustomProcess.ObjectList.Production),
                                                                                new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                new BinaryOperator("Active", true)));

                    if (_locAppSetupDetails != null && _locAppSetupDetails.Count > 0)
                    {

                        foreach (ApplicationSetupDetail _locAppSetupDetail in _locAppSetupDetails)
                        {
                            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.ApprovalLevel)))
                            {
                                if (_locAppSetupDetail.ApprovalLevel != CustomProcess.ApprovalLevel.None)
                                {
                                    EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalLevel));
                                    _setApprovalLevel = new ChoiceActionItem(_ed.GetCaption(_locAppSetupDetail.ApprovalLevel), _locAppSetupDetail.ApprovalLevel);
                                }
                            }
                            ProductionApprovalAction.Items.Add(_setApprovalLevel);
                        }
                    }
                }
            }
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        private void ProductionApprovalAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            GlobalFunction _globFunc = new GlobalFunction();
            ApplicationSetupDetail _locAppSetDetail = null;
            IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
            ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
            DateTime now = DateTime.Now;
            var User = SecuritySystem.CurrentUserName;
            Session _currentSession = null;
            string _currObjectId = null;

            if (this.ObjectSpace != null)
            {
                _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
            }

            UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));

            foreach (Object obj in _objectsToProcess)
            {
                Production _objInNewObjectSpace = (Production)_objectSpace.GetObject(obj);

                if (_objInNewObjectSpace != null)
                {
                    if (_objInNewObjectSpace.Code != null)
                    {
                        _currObjectId = _objInNewObjectSpace.Code;
                    }
                }

                if (_currObjectId != null)
                {
                    Production _locProductionXPO = _currentSession.FindObject<Production>
                                                   (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Code", _currObjectId)));

                    if (_locProductionXPO != null)
                    {
                        if (_locProductionXPO.Status == Status.Progress)
                        {
                            #region Approval Level 1
                            if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level1)
                            {
                                _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level1, ObjectList.Production);

                                if (_locAppSetDetail != null)
                                {
                                    //Buat bs input langsung ke approvalline
                                    ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                       (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Production", _locProductionXPO),
                                                                        new BinaryOperator("ApprovalLevel", ApprovalLevel.Level1)));

                                    if (_locApprovalLineXPO == null)
                                    {
                                        if (_locAppSetDetail.EndApproval == true)
                                        {
                                            ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                            {
                                                ApprovalDate = now,
                                                ApprovalStatus = Status.Approved,
                                                ApprovalLevel = ApprovalLevel.Level1,
                                                EndApproval = true,
                                                Production = _locProductionXPO,
                                            };
                                            _saveDataAL.Save();
                                            _saveDataAL.Session.CommitTransaction();
                                        }
                                        else
                                        {
                                            ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                            {
                                                ApprovalDate = now,
                                                ApprovalStatus = Status.Approved,
                                                ApprovalLevel = ApprovalLevel.Level1,
                                                Production = _locProductionXPO,
                                            };
                                            _saveDataAL.Save();
                                            _saveDataAL.Session.CommitTransaction();
                                        }

                                        //Send Email
                                        ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("Production", _locProductionXPO)));

                                        SetApprovalProduction(_currentSession, _locProductionXPO, Status.Progress);
                                        SendEmail(_currentSession, _locProductionXPO, Status.Progress, _locApprovalLineXPO2);
                                        SuccessMessageShow("Production has successfully Approve");
                                    }
                                }
                            }

                            #endregion Approval Level 1

                            #region Approval Level 2
                            if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level2)
                            {
                                _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level2, ObjectList.Production);

                                if (_locAppSetDetail != null)
                                {
                                    ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                       (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Production", _locProductionXPO),
                                                                        new BinaryOperator("ApprovalLevel", ApprovalLevel.Level2)));

                                    if (_locApprovalLineXPO == null)
                                    {
                                        if (_locAppSetDetail.EndApproval == true)
                                        {
                                            ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                            {
                                                ApprovalDate = now,
                                                ApprovalStatus = Status.Approved,
                                                ApprovalLevel = ApprovalLevel.Level2,
                                                EndApproval = true,
                                                Production = _locProductionXPO,
                                            };
                                            _saveDataAL.Save();
                                            _saveDataAL.Session.CommitTransaction();
                                        }
                                        else
                                        {
                                            ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                            {
                                                ApprovalDate = now,
                                                ApprovalStatus = Status.Approved,
                                                ApprovalLevel = ApprovalLevel.Level2,
                                                Production = _locProductionXPO,
                                            };
                                            _saveDataAL.Save();
                                            _saveDataAL.Session.CommitTransaction();

                                            SetApprovalLine(_currentSession, _locProductionXPO, ApprovalLevel.Level1);
                                        }

                                        //Send Email
                                        ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("Production", _locProductionXPO)));

                                        SetApprovalProduction(_currentSession, _locProductionXPO, Status.Progress);
                                        SendEmail(_currentSession, _locProductionXPO, Status.Progress, _locApprovalLineXPO2);
                                        SuccessMessageShow("Production has successfully Approve");
                                    }
                                }
                            }
                            #endregion Approval Level 2

                            #region Approval Level 3
                            if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level3)
                            {
                                _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level3, ObjectList.Production);

                                if (_locAppSetDetail != null)
                                {
                                    ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                       (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Production", _locProductionXPO),
                                                                        new BinaryOperator("ApprovalLevel", ApprovalLevel.Level3)));

                                    if (_locApprovalLineXPO == null)
                                    {
                                        if (_locAppSetDetail.EndApproval == true)
                                        {
                                            ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                            {
                                                ApprovalDate = now,
                                                ApprovalStatus = Status.Approved,
                                                ApprovalLevel = ApprovalLevel.Level3,
                                                EndApproval = true,
                                                Production = _locProductionXPO,
                                            };
                                            _saveDataAL.Save();
                                            _saveDataAL.Session.CommitTransaction();
                                        }
                                        else
                                        {
                                            ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                            {
                                                ApprovalDate = now,
                                                ApprovalStatus = Status.Approved,
                                                ApprovalLevel = ApprovalLevel.Level3,
                                                Production = _locProductionXPO,
                                            };
                                            _saveDataAL.Save();
                                            _saveDataAL.Session.CommitTransaction();

                                            SetApprovalLine(_currentSession, _locProductionXPO, ApprovalLevel.Level2);
                                            SetApprovalLine(_currentSession, _locProductionXPO, ApprovalLevel.Level1);
                                        }

                                        //Send Email
                                        ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("Production", _locProductionXPO)));

                                        SetApprovalProduction(_currentSession, _locProductionXPO, Status.Progress);
                                        SendEmail(_currentSession, _locProductionXPO, Status.Progress, _locApprovalLineXPO2);
                                        SuccessMessageShow("Production has successfully Approve");
                                    }
                                }
                            }
                            #endregion Approval Level 3
                        }
                        else
                        {
                            ErrorMessageShow("Production Status Not Available");
                        }
                    }
                    else
                    {
                        ErrorMessageShow("Production Not Available");
                    }
                }
            }

            if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
            {
                _objectSpace.CommitChanges();
            }
            if (View is ListView)
            {
                _objectSpace.CommitChanges();
                View.ObjectSpace.Refresh();
            }
        }

        //==== Code Only ====

        #region Approval Process

        private void SetApprovalLine(Session _currentSession, Production _locProductionXPO, ApprovalLevel _locApprovalLevel)
        {
            try
            {
                DateTime now = DateTime.Now;

                ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                   (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Production", _locProductionXPO),
                                                    new BinaryOperator("ApprovalLevel", _locApprovalLevel)));

                if (_locApprovalLineXPO == null)
                {
                    ApprovalLine _saveDataAL2 = new ApprovalLine(_currentSession)
                    {
                        ApprovalDate = now,
                        ApprovalStatus = Status.Approved,
                        ApprovalLevel = _locApprovalLevel,
                        Production = _locProductionXPO,
                    };
                    _saveDataAL2.Save();
                    _saveDataAL2.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Production " + ex.ToString());
            }
        }

        private void SetApprovalProduction(Session _currentSession, Production _locProductionXPO, Status _locStatus)
        {
            try
            {
                DateTime now = DateTime.Now;

                Production _locProductions = _currentSession.FindObject<Production>
                                               (new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("Code", _locProductionXPO.Code),
                                                new BinaryOperator("Status", _locStatus)));

                if (_locProductions != null)
                {
                    ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("EndApproval", true),
                                                         new BinaryOperator("Production", _locProductions)));

                    if (_locApprovalLineXPO2 != null)
                    {
                        #region True
                            
                        if (_locProductions != null)
                        {
                            _locProductions.ActivationPosting = true;
                            _locProductions.ActiveApproved1 = false;
                            _locProductions.ActiveApproved2 = false;
                            _locProductions.ActiveApproved3 = true;
                            _locProductions.Save();
                            _locProductions.Session.CommitTransaction();

                            XPCollection<ProductionLine> _locProductionLines = new XPCollection<ProductionLine>
                                                                                (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Production", _locProductionXPO),
                                                                                new BinaryOperator("Status", Status.Progress)));
                            if (_locProductionLines != null)
                            {
                                foreach (ProductionLine _locProductionLine in _locProductionLines)
                                {
                                    _locProductionLine.ActivationPosting = true;
                                    _locProductionLine.Save();
                                    _locProductionLine.Session.CommitTransaction();
                                }
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Data Production Not Available");
                        }

                        #endregion True
                    }
                    else if (_locApprovalLineXPO2 == null)
                    {
                        #region False

                        if (_locProductions.ActiveApproved1 == false && _locProductions.ActiveApproved2 == false && _locProductions.ActiveApproved3 == false)
                        {
                            #region Lv1
                            if (_locProductions != null)
                            {
                                _locProductions.ActivationPosting = true;
                                _locProductions.ActiveApproved1 = true;
                                _locProductions.ActiveApproved2 = false;
                                _locProductions.ActiveApproved3 = false;
                                _locProductions.Save();
                                _locProductions.Session.CommitTransaction();

                                XPCollection<ProductionLine> _locProductionLines = new XPCollection<ProductionLine>
                                                                                                     (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Production", _locProductionXPO),
                                                                                                      new BinaryOperator("Status", Status.Progress)));
                                if (_locProductionLines != null)
                                {
                                    foreach (ProductionLine _locProductionLine in _locProductionLines)
                                    {
                                        _locProductionLine.ActivationPosting = true;
                                        _locProductionLine.Save();
                                        _locProductionLine.Session.CommitTransaction();
                                    }
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Production Not Available");
                            }
                            #endregion Lv1
                        }
                        else if (_locProductions.ActiveApproved1 == true && _locProductions.ActiveApproved2 == false && _locProductions.ActiveApproved3 == false)
                        {
                            #region Lv2
                            if (_locProductions != null)
                            {
                                _locProductions.ActivationPosting = true;
                                _locProductions.ActiveApproved2 = true;
                                _locProductions.ActiveApproved1 = false;
                                _locProductions.ActiveApproved3 = false;
                                _locProductions.Save();
                                _locProductions.Session.CommitTransaction();

                                XPCollection<ProductionLine> _locProductionLines = new XPCollection<ProductionLine>
                                                                                                     (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Production", _locProductionXPO),
                                                                                                      new BinaryOperator("Status", Status.Progress)));
                                if (_locProductionLines != null)
                                {
                                    foreach (ProductionLine _locProductionLine in _locProductionLines)
                                    {
                                        _locProductionLine.ActivationPosting = true;
                                        _locProductionLine.Save();
                                        _locProductionLine.Session.CommitTransaction();
                                    }
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Production Not Available");
                            }
                            #endregion Lv2
                        }
                        else if (_locProductions.ActiveApproved1 == false && _locProductions.ActiveApproved2 == true && _locProductions.ActiveApproved3 == false)
                        {
                            #region Lv3
                            if (_locProductions != null)
                            {
                                _locProductions.ActivationPosting = true;
                                _locProductions.ActiveApproved3 = true;
                                _locProductions.ActiveApproved1 = false;
                                _locProductions.ActiveApproved2 = false;
                                _locProductions.Save();
                                _locProductions.Session.CommitTransaction();

                                XPCollection<ProductionLine> _locProductionLines = new XPCollection<ProductionLine>
                                                                                   (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("Production", _locProductionXPO),
                                                                                    new BinaryOperator("Status", Status.Progress)));
                                if (_locProductionLines != null)
                                {
                                    foreach (ProductionLine _locProductionLine in _locProductionLines)
                                    {
                                        _locProductionLine.ActivationPosting = true;
                                        _locProductionLine.Save();
                                        _locProductionLine.Session.CommitTransaction();
                                    }
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Production Not Available");
                            }
                            #endregion Lv3
                        }
                        else
                        {
                            ErrorMessageShow("Data Approval For Production Not Available");
                        }

                        #endregion False
                    }
                    else
                    {
                        ErrorMessageShow("Data Approval For Production Not Available");
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Production " + ex.ToString());
            }
        }

        #endregion Approval Process

        #region Email

        private string BackgroundBody(Session _currentSession, Production _locProductionXPO, MailSetupDetail _locMailSetDetail)
        {
            string body = string.Empty;

            Production _locProductions = _currentSession.FindObject<Production>
                                           (new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Code", _locProductionXPO.Code)));

            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                               (new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("Production", _locProductions)));

            string Status = _locApprovalLineXPO.ApprovalStatus.ToString();

            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Email/EmailTemplate.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{Code}", _locProductionXPO.Code);

            #region Level
            if (_locProductions.ActiveApproved1 == true)
            {
                string Active = "Level 1";
                body = body.Replace("{Level}", Active);
            }
            else if (_locProductions.ActiveApproved2 == true)
            {
                string Active = "Level 2";
                body = body.Replace("{Level}", Active);
            }
            else if (_locProductions.ActiveApproved3 == true)
            {
                string Active = "Level 3";
                body = body.Replace("{Level}", Active);
            }
            else
            {
                string Active = "Not Available";
                body = body.Replace("{Level}", Active);
            }
            #endregion Level

            body = body.Replace("{Status}", Status);
            body = body.Replace("{Message}", _locMailSetDetail.MessageBody);
            return body;
        }

        protected void SendEmail(Session _currentSession, Production _locProductionXPO, Status _locStatus, ApprovalLine _locApprovalLineXPO2)
        {
            DateTime now = DateTime.Now;
            var User = SecuritySystem.CurrentUserName;
            GlobalFunction _globFunc = new GlobalFunction();
            UserAccess _numLineUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));
            string _locMessageBody = null;

            try
            {
                if (_locProductionXPO != null)
                {
                    XPCollection<MailSetupDetail> _locMailSetDetails = new XPCollection<MailSetupDetail>
                                                                       (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ObjectList", ObjectList.Production),
                                                                        new BinaryOperator("UserAccess", _numLineUserAccess),
                                                                        new BinaryOperator("Active", true)));

                    if (_locMailSetDetails != null && _locMailSetDetails.Count > 0)
                    {
                        foreach (MailSetupDetail _locMailSetDetail in _locMailSetDetails)
                        {
                            if (_locMailSetDetail.MessageBody != null)
                            {
                                if (_locApprovalLineXPO2.EndApproval == true)
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locProductionXPO, _locMailSetDetail);
                                }
                                else
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locProductionXPO, _locMailSetDetail);
                                }
                            }

                            _globFunc.SetAndSendMail(_locMailSetDetail.SmtpHost, _locMailSetDetail.SmtpPort,
                            _locMailSetDetail.MailFrom, _locMailSetDetail.MailFromPassword, _locMailSetDetail.MailTo, _locMailSetDetail.MailSubject, _locMessageBody);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Production " + ex.ToString());
            }
        }

        #endregion Email

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

    }
}
