﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class DebitMemoActionController : ViewController
    {
        #region Default

        private ChoiceActionItem _selectionListviewFilter;
        public DebitMemoActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            DebitMemoFilterAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                DebitMemoFilterAction.Items.Add(_selectionListviewFilter);
            }
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        private void DebitMemoProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        DebitMemo _locDebitMemoOS = (DebitMemo)_objectSpace.GetObject(obj);

                        if (_locDebitMemoOS != null)
                        {
                            if (_locDebitMemoOS.Code != null)
                            {
                                _currObjectId = _locDebitMemoOS.Code;

                                DebitMemo _locDebitMemoXPO = _currSession.FindObject<DebitMemo>
                                                             (new GroupOperator(GroupOperatorType.And,
                                                              new BinaryOperator("Code", _currObjectId)));

                                if (_locDebitMemoXPO != null)
                                {
                                    if (_locDebitMemoXPO.Status == Status.Open)
                                    {
                                        _locDebitMemoXPO.Status = Status.Progress;
                                        _locDebitMemoXPO.StatusDate = now;
                                        _locDebitMemoXPO.Save();
                                        _locDebitMemoXPO.Session.CommitTransaction();

                                        XPCollection<DebitMemoLine> _locDebitMemoLines = new XPCollection<DebitMemoLine>
                                                                                         (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                          new BinaryOperator("DebitMemo", _locDebitMemoXPO)));

                                        if (_locDebitMemoLines != null && _locDebitMemoLines.Count > 0)
                                        {
                                            foreach (DebitMemoLine _locDebitMemoLine in _locDebitMemoLines)
                                            {
                                                if (_locDebitMemoLine.Status == Status.Open)
                                                {
                                                    _locDebitMemoLine.Status = Status.Progress;
                                                    _locDebitMemoLine.StatusDate = now;
                                                    _locDebitMemoLine.Save();
                                                    _locDebitMemoLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        SuccessMessageShow(_locDebitMemoXPO.Code + " has been change successfully to Progress");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Inventory Transfer Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data Inventory Transfer Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DebitMemo " + ex.ToString());
            }
        }

        private void DebitMemoPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        DebitMemo _locDebitMemoOS = (DebitMemo)_objectSpace.GetObject(obj);

                        if (_locDebitMemoOS != null)
                        {
                            if (_locDebitMemoOS.Code != null)
                            {
                                _currObjectId = _locDebitMemoOS.Code;

                                DebitMemo _locDebitMemoXPO = _currSession.FindObject<DebitMemo>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Code", _currObjectId)));

                                if (_locDebitMemoXPO != null)
                                {
                                    if(_locDebitMemoXPO.SalesOrder != null)
                                    {
                                        SetDebitMemoCollectionInSICAndSI(_currSession, _locDebitMemoXPO);
                                    }
                                    
                                    SetStatusPostingDebitMemoLine(_currSession, _locDebitMemoXPO);
                                    SetInvoiceARReturnJournal(_currSession, _locDebitMemoXPO);
                                    SetReverseGoodsReturnJournal(_currSession, _locDebitMemoXPO);
                                    SetFinalStatusReturnDebitMemo(_currSession, _locDebitMemoXPO);
                                    SuccessMessageShow(_locDebitMemoXPO.Code + " has been change successfully to Posted");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Inventory Transfer Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data Inventory Transfer Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DebitMemo " + ex.ToString());
            }
        }

        private void DebitMemoFilterAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(DebitMemo)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DebitMemo " + ex.ToString());
            }
        }

        #region Posting

        private void SetReverseGoodsReturnJournal(Session _currSession, DebitMemo _locDebitMemoXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locTotalUnitPrice = 0;
                double _currCountPosted = 0;

                //Bikin Group by  dan Totalkan jumlah uang pergroup
                if (_locDebitMemoXPO != null)
                {
                    XPQuery<DebitMemoLine> _debitMemoLinesQuery = new XPQuery<DebitMemoLine>(_currSession);

                    var _debitMemoLines = from dml in _debitMemoLinesQuery
                                          where (dml.DebitMemo == _locDebitMemoXPO && (dml.Status == Status.Progress || dml.Status == Status.Posted))
                                          group dml by dml.Item.ItemAccountGroup into g
                                          select new { ItemAccountGroup = g.Key };

                    if (_debitMemoLines != null && _debitMemoLines.Count() > 0)
                    {
                        foreach (var _debitMemoLine in _debitMemoLines)
                        {
                            XPCollection<DebitMemoLine> _locDeMemLines = new XPCollection<DebitMemoLine>(_currSession,
                                                                          new GroupOperator(GroupOperatorType.And,
                                                                          new BinaryOperator("DebitMemo", _locDebitMemoXPO),
                                                                          new BinaryOperator("Item.ItemAccountGroup", _debitMemoLine.ItemAccountGroup)));

                            if (_locDeMemLines != null && _locDeMemLines.Count > 0)
                            {
                                foreach (DebitMemoLine _locDeMemLine in _locDeMemLines)
                                {
                                    if (_locDeMemLine.Status == Status.Posted || _locDeMemLine.Status == Status.Close)
                                    {
                                        _currCountPosted = _currCountPosted + 1;
                                    }
                                }

                                if (_currCountPosted == 0)
                                {
                                    #region MenghitungTotalUnitPriceBasedOnItemAccountGroup
                                    foreach (DebitMemoLine _locDeMemLine in _locDeMemLines)
                                    {
                                        if (_locDeMemLine.Status == Status.Progress || _locDeMemLine.Status == Status.Posted)
                                        {
                                            if (_locDebitMemoXPO.SalesReturn != null)
                                            {
                                                SalesReturnLine _locSalesReturnLine = _currSession.FindObject<SalesReturnLine>
                                                                                      (new GroupOperator(GroupOperatorType.And,
                                                                                       new BinaryOperator("SalesReturn", _locDebitMemoXPO.SalesReturn),
                                                                                       new BinaryOperator("Item", _locDeMemLine.Item)));

                                                if (_locSalesReturnLine != null)
                                                {
                                                    _locTotalUnitPrice = _locTotalUnitPrice + _locSalesReturnLine.TUAmount;
                                                }
                                            }
                                        }
                                    }
                                    #endregion MenghitungTotalUnitPriceBasedOnItemAccountGroup

                                    #region CreateReverseGoodsReceiptJournal

                                    #region CreateReverseGoodsReceiptJournalByItems
                                    XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("ItemAccountGroup", _debitMemoLine.ItemAccountGroup)));

                                    if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                    {
                                        foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                        {
                                            XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("JournalMap", _locJournalMapItem)));

                                            if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                            {
                                                foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                {
                                                    AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                 new BinaryOperator("PostingType", PostingType.Sales),
                                                                                 new BinaryOperator("OrderType", OrderType.Item),
                                                                                 new BinaryOperator("PostingMethod", PostingMethod.Return),
                                                                                 new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                    if (_locAccountMap != null)
                                                    {
                                                        XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                            new BinaryOperator("Active", true)));

                                                        if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                        {
                                                            double _locTotalAmountDebit = 0;
                                                            double _locTotalAmountCredit = 0;
                                                            double _locTotalBalance = 0;

                                                            foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                            {
                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                {
                                                                    _locTotalAmountCredit = _locTotalUnitPrice;
                                                                }
                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                {
                                                                    _locTotalAmountDebit = _locTotalUnitPrice;
                                                                }

                                                                GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                {
                                                                    PostingDate = now,
                                                                    PostingType = PostingType.Sales,
                                                                    PostingMethod = PostingMethod.DebitMemo,
                                                                    Account = _locAccountMapLine.Account,
                                                                    Debit = _locTotalAmountDebit,
                                                                    Credit = _locTotalAmountCredit,
                                                                    SalesOrder = _locDebitMemoXPO.SalesOrder,
                                                                    SalesReturn = _locDebitMemoXPO.SalesReturn,
                                                                    DebitMemo = _locDebitMemoXPO,
                                                                };
                                                                _saveGeneralJournal.Save();
                                                                _saveGeneralJournal.Session.CommitTransaction();

                                                                if (_locAccountMapLine.Account.Code != null)
                                                                {
                                                                    ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                             (new GroupOperator(GroupOperatorType.And,
                                                                                              new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                    if (_locCOA != null)
                                                                    {
                                                                        if (_locCOA.BalanceType == BalanceType.Change)
                                                                        {
                                                                            if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                            {
                                                                                if (_locTotalAmountDebit > 0)
                                                                                {
                                                                                    _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                }
                                                                                if (_locTotalAmountCredit > 0)
                                                                                {
                                                                                    _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                }
                                                                            }
                                                                            if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                            {
                                                                                if (_locTotalAmountDebit > 0)
                                                                                {
                                                                                    _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                }
                                                                                if (_locTotalAmountCredit > 0)
                                                                                {
                                                                                    _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                }
                                                                            }
                                                                        }

                                                                        _locCOA.Balance = _locTotalBalance;
                                                                        _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                        _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                        _locCOA.Save();
                                                                        _locCOA.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion CreateReverseGoodsReceiptJournalByItems

                                    #region CreateReverseGoodsReceiptJournalByBusinessPartner
                                    if (_locDebitMemoXPO.BillToCustomer != null)
                                    {
                                        if (_locDebitMemoXPO.BillToCustomer.BusinessPartnerAccountGroup != null)
                                        {
                                            XPCollection<JournalMap> _locJournalMapBusinessPartners = new XPCollection<JournalMap>
                                                                                                      (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                       new BinaryOperator("BusinessPartnerAccountGroup", _locDebitMemoXPO.BillToCustomer.BusinessPartnerAccountGroup)));

                                            if (_locJournalMapBusinessPartners != null && _locJournalMapBusinessPartners.Count() > 0)
                                            {
                                                foreach (JournalMap _locJournalMapBusinessPartner in _locJournalMapBusinessPartners)
                                                {
                                                    XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("JournalMap", _locJournalMapBusinessPartner)));

                                                    if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                    {
                                                        foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                        {
                                                            AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                         new BinaryOperator("PostingType", PostingType.Sales),
                                                                                         new BinaryOperator("OrderType", OrderType.Item),
                                                                                         new BinaryOperator("PostingMethod", PostingMethod.Return),
                                                                                         new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                            if (_locAccountMap != null)
                                                            {
                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                new BinaryOperator("Active", true)));

                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                {
                                                                    double _locTotalAmountDebit = 0;
                                                                    double _locTotalAmountCredit = 0;
                                                                    double _locTotalBalance = 0;

                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                    {
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            _locTotalAmountCredit = _locTotalUnitPrice;
                                                                        }
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            _locTotalAmountDebit = _locTotalUnitPrice;
                                                                        }

                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                        {
                                                                            PostingDate = now,
                                                                            PostingType = PostingType.Sales,
                                                                            PostingMethod = PostingMethod.DebitMemo,
                                                                            Account = _locAccountMapLine.Account,
                                                                            Debit = _locTotalAmountDebit,
                                                                            Credit = _locTotalAmountCredit,
                                                                            SalesOrder = _locDebitMemoXPO.SalesOrder,
                                                                            SalesReturn = _locDebitMemoXPO.SalesReturn,
                                                                            DebitMemo = _locDebitMemoXPO,
                                                                        };
                                                                        _saveGeneralJournal.Save();
                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                        {
                                                                            ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                            if (_locCOA != null)
                                                                            {
                                                                                if (_locCOA.BalanceType == BalanceType.Change)
                                                                                {
                                                                                    if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                    if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                }

                                                                                _locCOA.Balance = _locTotalBalance;
                                                                                _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                _locCOA.Save();
                                                                                _locCOA.Session.CommitTransaction();
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion CreateReverseGoodsReceiptJournalByBusinessPartner

                                    #endregion CreateReverseGoodsReceiptJournal
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = DebitMemo ", ex.ToString());
            }
        }

        private void SetInvoiceARReturnJournal(Session _currSession, DebitMemo _locDebitMemoXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locTotalUnitPrice = 0;

                #region CreateInvoiceAPReturnJournal

                if (_locDebitMemoXPO != null)
                {
                    XPQuery<DebitMemoLine> _debitMemoLinesQuery = new XPQuery<DebitMemoLine>(_currSession);

                    var _debitMemoLines = from cml in _debitMemoLinesQuery
                                          where (cml.DebitMemo == _locDebitMemoXPO && (cml.Status == Status.Progress || cml.Status == Status.Posted))
                                          group cml by cml.Item.ItemAccountGroup into g
                                          select new { ItemAccountGroup = g.Key };

                    if (_debitMemoLines != null && _debitMemoLines.Count() > 0)
                    {
                        foreach (var _debitMemoLine in _debitMemoLines)
                        {
                            XPCollection<DebitMemoLine> _locDeMemLines = new XPCollection<DebitMemoLine>(_currSession,
                                                                         new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("DebitMemo", _locDebitMemoXPO),
                                                                         new BinaryOperator("Item.ItemAccountGroup", _debitMemoLine.ItemAccountGroup)));

                            if (_locDeMemLines != null && _locDeMemLines.Count() > 0)
                            {
                                #region MenghitungTotalUnitPriceBasedOnItemAccountGroup
                                foreach (DebitMemoLine _locDeMemLine in _locDeMemLines)
                                {
                                    if (_locDeMemLine.Status == Status.Progress || _locDeMemLine.Status == Status.Posted)
                                    {
                                        _locTotalUnitPrice = _locTotalUnitPrice + _locDeMemLine.TUAmount;
                                    }
                                }
                                #endregion MenghitungTotalUnitPriceBasedOnItemAccountGroup

                                #region CreateInvoiceAPReturnJournalByItems

                                XPCollection<JournalMap> _locJournalMapByItems = new XPCollection<JournalMap>
                                                                                 (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("ItemAccountGroup", _debitMemoLine.ItemAccountGroup)));

                                if (_locJournalMapByItems != null && _locJournalMapByItems.Count() > 0)
                                {
                                    foreach (JournalMap _locJournalMapByItem in _locJournalMapByItems)
                                    {
                                        XPCollection<JournalMapLine> _locJournalMapLineByItems = new XPCollection<JournalMapLine>
                                                                                                 (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                  new BinaryOperator("JournalMap", _locJournalMapByItem)));

                                        if (_locJournalMapLineByItems != null && _locJournalMapLineByItems.Count() > 0)
                                        {
                                            foreach (JournalMapLine _locJournalMapLineByItem in _locJournalMapLineByItems)
                                            {
                                                AccountMap _locAccountMapByItem = _currSession.FindObject<AccountMap>
                                                                                  (new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("Code", _locJournalMapLineByItem.AccountMap.Code),
                                                                                   new BinaryOperator("PostingType", PostingType.Sales),
                                                                                   new BinaryOperator("OrderType", OrderType.Item),
                                                                                   new BinaryOperator("PostingMethod", PostingMethod.DebitMemo),
                                                                                   new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                if (_locAccountMapByItem != null)
                                                {
                                                    XPCollection<AccountMapLine> _locAccountMapLineByItems = new XPCollection<AccountMapLine>
                                                                                                             (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                              new BinaryOperator("AccountMap", _locAccountMapByItem),
                                                                                                              new BinaryOperator("Active", true)));

                                                    if (_locAccountMapLineByItems != null && _locAccountMapLineByItems.Count() > 0)
                                                    {
                                                        double _locTotalAmountDebit = 0;
                                                        double _locTotalAmountCredit = 0;
                                                        double _locTotalBalance = 0;

                                                        foreach (AccountMapLine _locAccountMapLineByItem in _locAccountMapLineByItems)
                                                        {
                                                            if (_locAccountMapLineByItem.AccountCharge == AccountCharge.Debit)
                                                            {
                                                                _locTotalAmountDebit = _locTotalUnitPrice;
                                                            }
                                                            if (_locAccountMapLineByItem.AccountCharge == AccountCharge.Credit)
                                                            {
                                                                _locTotalAmountCredit = _locTotalUnitPrice;
                                                            }

                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                            {
                                                                PostingDate = now,
                                                                PostingType = PostingType.Sales,
                                                                PostingMethod = PostingMethod.InvoiceAP,
                                                                Account = _locAccountMapLineByItem.Account,
                                                                Debit = _locTotalAmountDebit,
                                                                Credit = _locTotalAmountCredit,
                                                                SalesOrder = _locDebitMemoXPO.SalesOrder,
                                                                DebitMemo = _locDebitMemoXPO,
                                                            };
                                                            _saveGeneralJournal.Save();
                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                            if (_locAccountMapLineByItem.Account.Code != null)
                                                            {
                                                                ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Code", _locAccountMapLineByItem.Account.Code)));
                                                                if (_locCOA != null)
                                                                {
                                                                    if (_locCOA.BalanceType == BalanceType.Change)
                                                                    {
                                                                        if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            if (_locTotalAmountDebit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                            }
                                                                            if (_locTotalAmountCredit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                            }
                                                                        }
                                                                        if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            if (_locTotalAmountDebit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                            }
                                                                            if (_locTotalAmountCredit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                            }
                                                                        }
                                                                    }

                                                                    _locCOA.Balance = _locTotalBalance;
                                                                    _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                    _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                    _locCOA.Save();
                                                                    _locCOA.Session.CommitTransaction();
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                #endregion CreateInvoiceAPReturnJournalByItems

                                #region CreateInvoiceAPJournalByBusinessPartner
                                if (_locDebitMemoXPO.BillToCustomer != null)
                                {
                                    if (_locDebitMemoXPO.BillToCustomer.BusinessPartnerAccountGroup != null)
                                    {
                                        XPCollection<JournalMap> _locJournalMapByBusinessPartners = new XPCollection<JournalMap>
                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                     new BinaryOperator("BusinessPartnerAccountGroup", _locDebitMemoXPO.BillToCustomer.BusinessPartnerAccountGroup)));

                                        if (_locJournalMapByBusinessPartners != null && _locJournalMapByBusinessPartners.Count() > 0)
                                        {
                                            foreach (JournalMap _locJournalMapByBusinessPartner in _locJournalMapByBusinessPartners)
                                            {
                                                XPCollection<JournalMapLine> _locJournalMapLineByBusinessPartners = new XPCollection<JournalMapLine>
                                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                     new BinaryOperator("JournalMap", _locJournalMapByBusinessPartner)));

                                                if (_locJournalMapLineByBusinessPartners != null && _locJournalMapLineByBusinessPartners.Count() > 0)
                                                {
                                                    foreach (JournalMapLine _locJournalMapLineByBusinessPartner in _locJournalMapLineByBusinessPartners)
                                                    {
                                                        AccountMap _locAccountMapByBusinessPartner = _currSession.FindObject<AccountMap>
                                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _locJournalMapLineByBusinessPartner.AccountMap.Code),
                                                                                                      new BinaryOperator("PostingType", PostingType.Sales),
                                                                                                      new BinaryOperator("OrderType", OrderType.Item),
                                                                                                      new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                                                                                                      new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                        if (_locAccountMapByBusinessPartner != null)
                                                        {
                                                            XPCollection<AccountMapLine> _locAccountMapLineByBusinessPartners = new XPCollection<AccountMapLine>
                                                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                 new BinaryOperator("AccountMap", _locAccountMapByBusinessPartner),
                                                                                                                                 new BinaryOperator("Active", true)));

                                                            if (_locAccountMapLineByBusinessPartners != null && _locAccountMapLineByBusinessPartners.Count() > 0)
                                                            {
                                                                double _locTotalAmountDebit = 0;
                                                                double _locTotalAmountCredit = 0;
                                                                double _locTotalBalance = 0;

                                                                foreach (AccountMapLine _locAccountMapLineByBusinessPartner in _locAccountMapLineByBusinessPartners)
                                                                {
                                                                    if (_locAccountMapLineByBusinessPartner.AccountCharge == AccountCharge.Debit)
                                                                    {
                                                                        _locTotalAmountDebit = _locTotalUnitPrice;
                                                                    }
                                                                    if (_locAccountMapLineByBusinessPartner.AccountCharge == AccountCharge.Credit)
                                                                    {
                                                                        _locTotalAmountCredit = _locTotalUnitPrice;
                                                                    }

                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                    {
                                                                        PostingDate = now,
                                                                        PostingType = PostingType.Purchase,
                                                                        PostingMethod = PostingMethod.InvoiceAP,
                                                                        Account = _locAccountMapLineByBusinessPartner.Account,
                                                                        Debit = _locTotalAmountDebit,
                                                                        Credit = _locTotalAmountCredit,
                                                                        SalesOrder = _locDebitMemoXPO.SalesOrder,
                                                                        DebitMemo = _locDebitMemoXPO,
                                                                    };
                                                                    _saveGeneralJournal.Save();
                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                    if (_locAccountMapLineByBusinessPartner.Account.Code != null)
                                                                    {
                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Code", _locAccountMapLineByBusinessPartner.Account.Code)));
                                                                        if (_locCOA != null)
                                                                        {
                                                                            if (_locCOA.BalanceType == BalanceType.Change)
                                                                            {
                                                                                if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                {
                                                                                    if (_locTotalAmountDebit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                    }
                                                                                    if (_locTotalAmountCredit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                    }
                                                                                }
                                                                                if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                {
                                                                                    if (_locTotalAmountDebit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                    }
                                                                                    if (_locTotalAmountCredit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                    }
                                                                                }
                                                                            }

                                                                            _locCOA.Balance = _locTotalBalance;
                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                            _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                            _locCOA.Save();
                                                                            _locCOA.Session.CommitTransaction();
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion CreateInvoiceAPJournalByBusinessPartner
                            }
                        }
                    }
                }

                #endregion CreateInvoiceAPReturnJournal
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CreditMemo ", ex.ToString());
            }
        }

        private void SetDebitMemoCollectionInSICAndSI(Session _currSession, DebitMemo _locDebitMemoXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _locSignCode = null;
                double _locTUAmount = 0;
                double _locDM_Amount = 0;

                if (_locDebitMemoXPO != null)
                {
                    if(_locDebitMemoXPO.SalesOrder != null)
                    {
                        #region NonSplitInvoice
                        SalesInvoiceCollection _locSalesInvoiceCollection = _currSession.FindObject<SalesInvoiceCollection>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("SalesOrder", _locDebitMemoXPO.SalesOrder)));

                        if (_locSalesInvoiceCollection != null)
                        {
                            XPCollection<DebitMemoLine> _locDebitMemoLines = new XPCollection<DebitMemoLine>(_currSession,
                                                                             new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("SalesReturn", _locDebitMemoXPO.SalesReturn),
                                                                             new BinaryOperator("DebitMemo", _locDebitMemoXPO)));

                            if (_locDebitMemoLines != null && _locDebitMemoLines.Count() > 0)
                            {
                                _locSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.DebitMemoCollection);

                                if (_locSignCode != null)
                                {
                                    //Menghitung Total di DebitMemoLine
                                    foreach (DebitMemoLine _locDebitMemoLine in _locDebitMemoLines)
                                    {
                                        _locTUAmount = _locTUAmount + _locDebitMemoLine.TUAmount;
                                    }

                                    DebitMemoCollection _locSaveDataDMC = new DebitMemoCollection(_currSession)
                                    {
                                        BillToCustomer = _locDebitMemoXPO.BillToCustomer,
                                        BillToAddress = _locDebitMemoXPO.BillToAddress,
                                        BillToCity = _locDebitMemoXPO.BillToCity,
                                        BillToContact = _locDebitMemoXPO.BillToContact,
                                        BillToCountry = _locDebitMemoXPO.BillToCountry,
                                        DM_Amount = _locTUAmount,
                                        TOP = _locDebitMemoXPO.SalesOrder.TOP,
                                        SignCode = _locSignCode,
                                        SalesReturn = _locDebitMemoXPO.SalesReturn,
                                        SalesOrder = _locDebitMemoXPO.SalesOrder,
                                        SalesInvoiceCollection = _locSalesInvoiceCollection,
                                        DebitMemo = _locDebitMemoXPO,
                                    };

                                    _locSaveDataDMC.Save();
                                    _locSaveDataDMC.Session.CommitTransaction();

                                    //Menghitung Total Jumlah DebitMemoCollection based on SalesInvoiceCollection
                                    XPCollection<DebitMemoCollection> _locDebitMemoCollections = new XPCollection<DebitMemoCollection>
                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("SalesInvoiceCollection", _locSalesInvoiceCollection)));
                                    if (_locDebitMemoCollections != null && _locDebitMemoCollections.Count() > 0)
                                    {
                                        foreach (DebitMemoCollection _locDebitMemoCollection in _locDebitMemoCollections)
                                        {
                                            _locDM_Amount = _locDM_Amount + _locDebitMemoCollection.DM_Amount;
                                        }

                                        _locSalesInvoiceCollection.DM_Amount = _locDM_Amount;
                                        _locSalesInvoiceCollection.Save();
                                        _locSalesInvoiceCollection.Session.CommitTransaction();
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data DebitMemoLine Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data DebitMemoLine Not Available");
                            }
                        }
                      
                        #endregion NonSplitInvoice
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DebitMemo" + ex.ToString());
            }
        }

        private void SetStatusPostingDebitMemoLine(Session _currSession, DebitMemo _DebitMemoXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_DebitMemoXPO != null)
                {
                    XPCollection<DebitMemoLine> _locDebitMemoLines = new XPCollection<DebitMemoLine>(_currSession,
                                                                     new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("DebitMemo", _DebitMemoXPO)));

                    if (_locDebitMemoLines != null && _locDebitMemoLines.Count > 0)
                    {

                        foreach (DebitMemoLine _locDebitMemoLine in _locDebitMemoLines)
                        {
                            if (_locDebitMemoLine.Status == Status.Progress)
                            {
                                _locDebitMemoLine.Status = Status.Posted;
                                _locDebitMemoLine.StatusDate = now;
                                _locDebitMemoLine.Save();
                                _locDebitMemoLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemo " + ex.ToString());
            }
        }

        private void SetFinalStatusReturnDebitMemo(Session _currSession, DebitMemo _locDebitMemoXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locCount = 0;
                if (_locDebitMemoXPO != null)
                {
                    XPCollection<DebitMemoLine> _locDebitMemoLines = new XPCollection<DebitMemoLine>(_currSession,
                                                                     new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("DebitMemo", _locDebitMemoXPO)));

                    if (_locDebitMemoLines != null && _locDebitMemoLines.Count() > 0)
                    {

                        foreach (DebitMemoLine _locDebitMemoLine in _locDebitMemoLines)
                        {
                            if (_locDebitMemoLine.Status == Status.Close)
                            {
                                _locCount = _locCount + 1;
                            }
                        }

                        if (_locCount == _locDebitMemoLines.Count())
                        {
                            _locDebitMemoXPO.ActivationPosting = true;
                            _locDebitMemoXPO.Status = Status.Close;
                            _locDebitMemoXPO.StatusDate = now;
                            _locDebitMemoXPO.Save();
                            _locDebitMemoXPO.Session.CommitTransaction();
                        }
                        else
                        {
                            _locDebitMemoXPO.Status = Status.Posted;
                            _locDebitMemoXPO.StatusDate = now;
                            _locDebitMemoXPO.Save();
                            _locDebitMemoXPO.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = DebitMemo " + ex.ToString());
            }
        }

        #endregion Posting

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

    }
}
