﻿namespace FullDrive.Module.Controllers
{
    partial class SalesPrePaymentInvoiceActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SalesPrePaymentInvoiceProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesPrePaymentInvoicePostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesPrePaymentInvoiceListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.SalesPrePaymentInvoiceGetBillAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesPrePaymentInvoiceApprovalAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.SalesPrePaymentInvoiceSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesPrePaymentInvoiceUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // SalesPrePaymentInvoiceProgressAction
            // 
            this.SalesPrePaymentInvoiceProgressAction.Caption = "Progress";
            this.SalesPrePaymentInvoiceProgressAction.ConfirmationMessage = null;
            this.SalesPrePaymentInvoiceProgressAction.Id = "SalesPrePaymentInvoiceProgressActionId";
            this.SalesPrePaymentInvoiceProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesPrePaymentInvoice);
            this.SalesPrePaymentInvoiceProgressAction.ToolTip = null;
            this.SalesPrePaymentInvoiceProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesPrePaymentInvoiceProgressAction_Execute);
            // 
            // SalesPrePaymentInvoicePostingAction
            // 
            this.SalesPrePaymentInvoicePostingAction.Caption = "Posting";
            this.SalesPrePaymentInvoicePostingAction.ConfirmationMessage = null;
            this.SalesPrePaymentInvoicePostingAction.Id = "SalesPrePaymentInvoicePostingActionId";
            this.SalesPrePaymentInvoicePostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesPrePaymentInvoice);
            this.SalesPrePaymentInvoicePostingAction.ToolTip = null;
            this.SalesPrePaymentInvoicePostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesPrePaymentInvoicePostingAction_Execute);
            // 
            // SalesPrePaymentInvoiceListviewFilterSelectionAction
            // 
            this.SalesPrePaymentInvoiceListviewFilterSelectionAction.Caption = "Filter";
            this.SalesPrePaymentInvoiceListviewFilterSelectionAction.ConfirmationMessage = null;
            this.SalesPrePaymentInvoiceListviewFilterSelectionAction.Id = "SalesPrePaymentInvoiceListviewFilterSelectionActionId";
            this.SalesPrePaymentInvoiceListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.SalesPrePaymentInvoiceListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesPrePaymentInvoice);
            this.SalesPrePaymentInvoiceListviewFilterSelectionAction.ToolTip = null;
            this.SalesPrePaymentInvoiceListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.SalesPrePaymentInvoiceListviewFilterSelectionAction_Execute);
            // 
            // SalesPrePaymentInvoiceGetBillAction
            // 
            this.SalesPrePaymentInvoiceGetBillAction.Caption = "Get Bill";
            this.SalesPrePaymentInvoiceGetBillAction.ConfirmationMessage = null;
            this.SalesPrePaymentInvoiceGetBillAction.Id = "SalesPrePaymentInvoiceGetBillActionId";
            this.SalesPrePaymentInvoiceGetBillAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesPrePaymentInvoice);
            this.SalesPrePaymentInvoiceGetBillAction.ToolTip = null;
            this.SalesPrePaymentInvoiceGetBillAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesPrePaymentInvoiceGetBillAction_Execute);
            // 
            // SalesPrePaymentInvoiceApprovalAction
            // 
            this.SalesPrePaymentInvoiceApprovalAction.Caption = "Approval";
            this.SalesPrePaymentInvoiceApprovalAction.ConfirmationMessage = null;
            this.SalesPrePaymentInvoiceApprovalAction.Id = "SalesPrePaymentInvoiceApprovalActionId";
            this.SalesPrePaymentInvoiceApprovalAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.SalesPrePaymentInvoiceApprovalAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesPrePaymentInvoice);
            this.SalesPrePaymentInvoiceApprovalAction.ToolTip = null;
            this.SalesPrePaymentInvoiceApprovalAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.SalesPrePaymentInvoiceApprovalAction_Execute);
            // 
            // SalesPrePaymentInvoiceSelectAction
            // 
            this.SalesPrePaymentInvoiceSelectAction.Caption = "Select";
            this.SalesPrePaymentInvoiceSelectAction.ConfirmationMessage = null;
            this.SalesPrePaymentInvoiceSelectAction.Id = "SalesPrePaymentInvoiceSelectActionId";
            this.SalesPrePaymentInvoiceSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesPrePaymentInvoice);
            this.SalesPrePaymentInvoiceSelectAction.ToolTip = null;
            this.SalesPrePaymentInvoiceSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesPrePaymentInvoiceSelectAction_Execute);
            // 
            // SalesPrePaymentInvoiceUnselectAction
            // 
            this.SalesPrePaymentInvoiceUnselectAction.Caption = "Unselect";
            this.SalesPrePaymentInvoiceUnselectAction.ConfirmationMessage = null;
            this.SalesPrePaymentInvoiceUnselectAction.Id = "SalesPrePaymentInvoiceUnselectActionId";
            this.SalesPrePaymentInvoiceUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesPrePaymentInvoice);
            this.SalesPrePaymentInvoiceUnselectAction.ToolTip = null;
            this.SalesPrePaymentInvoiceUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesPrePaymentInvoiceUnselectAction_Execute);
            // 
            // SalesPrePaymentInvoiceActionController
            // 
            this.Actions.Add(this.SalesPrePaymentInvoiceProgressAction);
            this.Actions.Add(this.SalesPrePaymentInvoicePostingAction);
            this.Actions.Add(this.SalesPrePaymentInvoiceListviewFilterSelectionAction);
            this.Actions.Add(this.SalesPrePaymentInvoiceGetBillAction);
            this.Actions.Add(this.SalesPrePaymentInvoiceApprovalAction);
            this.Actions.Add(this.SalesPrePaymentInvoiceSelectAction);
            this.Actions.Add(this.SalesPrePaymentInvoiceUnselectAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction SalesPrePaymentInvoiceProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesPrePaymentInvoicePostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction SalesPrePaymentInvoiceListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesPrePaymentInvoiceGetBillAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction SalesPrePaymentInvoiceApprovalAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesPrePaymentInvoiceSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesPrePaymentInvoiceUnselectAction;
    }
}
