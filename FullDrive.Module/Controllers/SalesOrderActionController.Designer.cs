﻿namespace FullDrive.Module.Controllers
{
    partial class SalesOrderActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SalesOrderProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesOrderPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesOrderGetSQAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesOrderListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.SalesOrderSalesReturnAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.WorkRequisitionAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesOrderListviewFilterApprovalSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.SalesOrderApprovalAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // SalesOrderProgressAction
            // 
            this.SalesOrderProgressAction.Caption = "Progress";
            this.SalesOrderProgressAction.ConfirmationMessage = null;
            this.SalesOrderProgressAction.Id = "SalesOrderProgressActionId";
            this.SalesOrderProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesOrder);
            this.SalesOrderProgressAction.ToolTip = null;
            this.SalesOrderProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesOrderProgressAction_Execute);
            // 
            // SalesOrderPostingAction
            // 
            this.SalesOrderPostingAction.Caption = "Posting";
            this.SalesOrderPostingAction.ConfirmationMessage = null;
            this.SalesOrderPostingAction.Id = "SalesOrderPostingActionId";
            this.SalesOrderPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesOrder);
            this.SalesOrderPostingAction.ToolTip = null;
            this.SalesOrderPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesOrderPostingAction_Execute);
            // 
            // SalesOrderGetSQAction
            // 
            this.SalesOrderGetSQAction.Caption = "Get SQ";
            this.SalesOrderGetSQAction.ConfirmationMessage = null;
            this.SalesOrderGetSQAction.Id = "SalesOrderGetSQActionId";
            this.SalesOrderGetSQAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesOrder);
            this.SalesOrderGetSQAction.ToolTip = null;
            this.SalesOrderGetSQAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesOrderGetSQAction_Execute);
            // 
            // SalesOrderListviewFilterSelectionAction
            // 
            this.SalesOrderListviewFilterSelectionAction.Caption = "Filter";
            this.SalesOrderListviewFilterSelectionAction.ConfirmationMessage = null;
            this.SalesOrderListviewFilterSelectionAction.Id = "SalesOrderListviewFilterSelectionActionId";
            this.SalesOrderListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.SalesOrderListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesOrder);
            this.SalesOrderListviewFilterSelectionAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.SalesOrderListviewFilterSelectionAction.ToolTip = null;
            this.SalesOrderListviewFilterSelectionAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.SalesOrderListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.SalesOrderListviewFilterSelectionAction_Execute);
            // 
            // SalesOrderSalesReturnAction
            // 
            this.SalesOrderSalesReturnAction.Caption = "Sales Return";
            this.SalesOrderSalesReturnAction.ConfirmationMessage = null;
            this.SalesOrderSalesReturnAction.Id = "SalesOrderSalesReturnActionId";
            this.SalesOrderSalesReturnAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesOrder);
            this.SalesOrderSalesReturnAction.ToolTip = null;
            this.SalesOrderSalesReturnAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesOrderSalesReturnAction_Execute);
            // 
            // WorkRequisitionAction
            // 
            this.WorkRequisitionAction.Caption = "Work Requisition";
            this.WorkRequisitionAction.ConfirmationMessage = null;
            this.WorkRequisitionAction.Id = "WorkRequisitionActionId";
            this.WorkRequisitionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesOrder);
            this.WorkRequisitionAction.ToolTip = null;
            this.WorkRequisitionAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.WorkRequisitionAction_Execute);
            // 
            // SalesOrderListviewFilterApprovalSelectionAction
            // 
            this.SalesOrderListviewFilterApprovalSelectionAction.Caption = "Filter Approval";
            this.SalesOrderListviewFilterApprovalSelectionAction.ConfirmationMessage = null;
            this.SalesOrderListviewFilterApprovalSelectionAction.Id = "SalesOrderListviewFilterApprovalSelectionActionId";
            this.SalesOrderListviewFilterApprovalSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.SalesOrderListviewFilterApprovalSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesOrder);
            this.SalesOrderListviewFilterApprovalSelectionAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.SalesOrderListviewFilterApprovalSelectionAction.ToolTip = null;
            this.SalesOrderListviewFilterApprovalSelectionAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.SalesOrderListviewFilterApprovalSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.SalesOrderListviewFilterApprovalSelectionAction_Execute);
            // 
            // SalesOrderApprovalAction
            // 
            this.SalesOrderApprovalAction.Caption = "Approval";
            this.SalesOrderApprovalAction.ConfirmationMessage = null;
            this.SalesOrderApprovalAction.Id = "SalesOrderApprovalActionId";
            this.SalesOrderApprovalAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.SalesOrderApprovalAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesOrder);
            this.SalesOrderApprovalAction.ToolTip = null;
            this.SalesOrderApprovalAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.SalesOrderApprovalAction_Execute);
            // 
            // SalesOrderActionController
            // 
            this.Actions.Add(this.SalesOrderProgressAction);
            this.Actions.Add(this.SalesOrderPostingAction);
            this.Actions.Add(this.SalesOrderGetSQAction);
            this.Actions.Add(this.SalesOrderListviewFilterSelectionAction);
            this.Actions.Add(this.SalesOrderSalesReturnAction);
            this.Actions.Add(this.WorkRequisitionAction);
            this.Actions.Add(this.SalesOrderListviewFilterApprovalSelectionAction);
            this.Actions.Add(this.SalesOrderApprovalAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction SalesOrderProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesOrderPostingAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesOrderGetSQAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction SalesOrderListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesOrderSalesReturnAction;
        private DevExpress.ExpressApp.Actions.SimpleAction WorkRequisitionAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction SalesOrderListviewFilterApprovalSelectionAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction SalesOrderApprovalAction;
    }
}
