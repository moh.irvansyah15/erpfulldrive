﻿namespace FullDrive.Module.Controllers
{
    partial class SalesReturnActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SalesReturnProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesReturnPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesReturnFilterAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // SalesReturnProgressAction
            // 
            this.SalesReturnProgressAction.Caption = "Progress";
            this.SalesReturnProgressAction.ConfirmationMessage = null;
            this.SalesReturnProgressAction.Id = "SalesReturnProgressActionId";
            this.SalesReturnProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesReturn);
            this.SalesReturnProgressAction.ToolTip = null;
            this.SalesReturnProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesReturnProgressAction_Execute);
            // 
            // SalesReturnPostingAction
            // 
            this.SalesReturnPostingAction.Caption = "Posting";
            this.SalesReturnPostingAction.ConfirmationMessage = null;
            this.SalesReturnPostingAction.Id = "SalesReturnPostingActionId";
            this.SalesReturnPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesReturn);
            this.SalesReturnPostingAction.ToolTip = null;
            this.SalesReturnPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesReturnPostingAction_Execute);
            // 
            // SalesReturnFilterAction
            // 
            this.SalesReturnFilterAction.Caption = "Filter";
            this.SalesReturnFilterAction.ConfirmationMessage = null;
            this.SalesReturnFilterAction.Id = "SalesReturnFilterActionId";
            this.SalesReturnFilterAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.SalesReturnFilterAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesReturn);
            this.SalesReturnFilterAction.ToolTip = null;
            this.SalesReturnFilterAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.SalesReturnFilterAction_Execute);
            // 
            // SalesReturnActionController
            // 
            this.Actions.Add(this.SalesReturnProgressAction);
            this.Actions.Add(this.SalesReturnPostingAction);
            this.Actions.Add(this.SalesReturnFilterAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction SalesReturnProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesReturnPostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction SalesReturnFilterAction;
    }
}
