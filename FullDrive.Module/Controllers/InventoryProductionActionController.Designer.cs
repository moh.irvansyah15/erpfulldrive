﻿namespace FullDrive.Module.Controllers
{
    partial class InventoryProductionActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.InventoryProductionProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.InventoryProductionPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.InventoryProductionFilterAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.InventoryProductionGenerateLotAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.InventoryProductionQcPassedAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.InventoryProductionQcRejectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.InventoryProductionActualweightAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // InventoryProductionProgressAction
            // 
            this.InventoryProductionProgressAction.Caption = "Progress";
            this.InventoryProductionProgressAction.ConfirmationMessage = null;
            this.InventoryProductionProgressAction.Id = "InventoryProductionProgressActionId";
            this.InventoryProductionProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryProduction);
            this.InventoryProductionProgressAction.ToolTip = null;
            this.InventoryProductionProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InventoryProductionProgressAction_Execute);
            // 
            // InventoryProductionPostingAction
            // 
            this.InventoryProductionPostingAction.Caption = "Posting";
            this.InventoryProductionPostingAction.ConfirmationMessage = null;
            this.InventoryProductionPostingAction.Id = "InventoryProductionPostingActionId";
            this.InventoryProductionPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryProduction);
            this.InventoryProductionPostingAction.ToolTip = null;
            this.InventoryProductionPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InventoryProductionPostingAction_Execute);
            // 
            // InventoryProductionFilterAction
            // 
            this.InventoryProductionFilterAction.Caption = "Filter";
            this.InventoryProductionFilterAction.ConfirmationMessage = null;
            this.InventoryProductionFilterAction.Id = "InventoryProductionFilterActionId";
            this.InventoryProductionFilterAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.InventoryProductionFilterAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryProduction);
            this.InventoryProductionFilterAction.ToolTip = null;
            this.InventoryProductionFilterAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.InventoryProductionFilterAction_Execute);
            // 
            // InventoryProductionGenerateLotAction
            // 
            this.InventoryProductionGenerateLotAction.Caption = "Generate Lot";
            this.InventoryProductionGenerateLotAction.ConfirmationMessage = null;
            this.InventoryProductionGenerateLotAction.Id = "InventoryProductionGenerateLotActionId";
            this.InventoryProductionGenerateLotAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryProduction);
            this.InventoryProductionGenerateLotAction.ToolTip = null;
            this.InventoryProductionGenerateLotAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InventoryProductionGenerateLotAction_Execute);
            // 
            // InventoryProductionQcPassedAction
            // 
            this.InventoryProductionQcPassedAction.Caption = "Qc Passed";
            this.InventoryProductionQcPassedAction.ConfirmationMessage = null;
            this.InventoryProductionQcPassedAction.Id = "InventoryProductionQcPassedActionId";
            this.InventoryProductionQcPassedAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryProduction);
            this.InventoryProductionQcPassedAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.InventoryProductionQcPassedAction.ToolTip = null;
            this.InventoryProductionQcPassedAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.InventoryProductionQcPassedAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InventoryProductionQcPassedAction_Execute);
            // 
            // InventoryProductionQcRejectAction
            // 
            this.InventoryProductionQcRejectAction.Caption = "Qc Reject";
            this.InventoryProductionQcRejectAction.ConfirmationMessage = null;
            this.InventoryProductionQcRejectAction.Id = "InventoryProductionQcRejectActionId";
            this.InventoryProductionQcRejectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryProduction);
            this.InventoryProductionQcRejectAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.InventoryProductionQcRejectAction.ToolTip = null;
            this.InventoryProductionQcRejectAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.InventoryProductionQcRejectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InventoryProductionQcRejectAction_Execute);
            // 
            // InventoryProductionActualweightAction
            // 
            this.InventoryProductionActualweightAction.Caption = "Actual Weight";
            this.InventoryProductionActualweightAction.ConfirmationMessage = null;
            this.InventoryProductionActualweightAction.Id = "InventoryProductionActualweightActionId";
            this.InventoryProductionActualweightAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryProduction);
            this.InventoryProductionActualweightAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.InventoryProductionActualweightAction.ToolTip = null;
            this.InventoryProductionActualweightAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.InventoryProductionActualweightAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InventoryProductionActualweightAction_Execute);
            // 
            // InventoryProductionActionController
            // 
            this.Actions.Add(this.InventoryProductionProgressAction);
            this.Actions.Add(this.InventoryProductionPostingAction);
            this.Actions.Add(this.InventoryProductionFilterAction);
            this.Actions.Add(this.InventoryProductionGenerateLotAction);
            this.Actions.Add(this.InventoryProductionQcPassedAction);
            this.Actions.Add(this.InventoryProductionQcRejectAction);
            this.Actions.Add(this.InventoryProductionActualweightAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction InventoryProductionProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction InventoryProductionPostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction InventoryProductionFilterAction;
        private DevExpress.ExpressApp.Actions.SimpleAction InventoryProductionGenerateLotAction;
        private DevExpress.ExpressApp.Actions.SimpleAction InventoryProductionQcPassedAction;
        private DevExpress.ExpressApp.Actions.SimpleAction InventoryProductionQcRejectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction InventoryProductionActualweightAction;
    }
}
