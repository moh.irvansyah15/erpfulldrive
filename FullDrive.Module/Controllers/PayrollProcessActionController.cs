﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.ExpressApp.ConditionalAppearance;
using FullDrive.Module.CustomProcess;
using FullDrive.Module.BusinessObjects;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PayrollProcessActionController : ViewController
    {
        public PayrollProcessActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #region Calculate With Tax
        private void CalculateWithTaxActionController_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                object _currObjectId = null;
                string _currSignCode = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PayrollProcess _locPayrollProcessOS = (PayrollProcess)_objectSpace.GetObject(obj);
                        if (_locPayrollProcessOS != null)
                        {
                            if (_locPayrollProcessOS.Code != null)
                            {
                                _currObjectId = _locPayrollProcessOS.Period;

                                PayrollProcess _locPayrollProcessXPO = _currSession.FindObject<PayrollProcess>
                                                                       (new GroupOperator(GroupOperatorType.And,
                                                                       new BinaryOperator("Period", _currObjectId),
                                                                       new BinaryOperator("Employee", _locPayrollProcessOS.Employee)));
                                if (_locPayrollProcessXPO != null)
                                {
                                    _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.TaxProcess);
                                    
                                    {
                                            TaxProcess _saveDataTaxProcess = new TaxProcess(_currSession)
                                        {
                                            Employee = _locPayrollProcessXPO.Employee,
                                            Period = _locPayrollProcessXPO.Period,

                                        };
                                        //
                                        _saveDataTaxProcess.Save();
                                        //_saveDataTaxProcess.Calculate();
                                        //_saveDataTaxProcess.CalculatePPH21();
                                        _saveDataTaxProcess.Save();
                                        //_saveDataTaxProcess.Calculate();
                                        _saveDataTaxProcess.Save();
                                        _saveDataTaxProcess.Session.CommitTransaction();
                                    }
                                    SuccessMessageShow("Tax Process Has Successfully Made");
                                }
                                else
                                {
                                    ErrorMessageShow("Payroll Process Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Payroll Process Not Available");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PayrollProcess" + ex.ToString());
            }

        }
        #endregion Calculate With Tax

        #region PayrollProcess
        private void PayrollProcessProcessActionController_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
           
        }
        #endregion PayrollProcess

        #region Calculate
        private void CalculateActionController_Execute(object sender, SimpleActionExecuteEventArgs e)
        {

        }
        #endregion Calculate

        #region GetSalaryBasic
        private void PayrollProcessGetSalaryBasicActionController_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PayrollProcess _locPayrollProcessOS = (PayrollProcess)_objectSpace.GetObject(obj);

                        if (_locPayrollProcessOS != null)
                        {
                            if (_locPayrollProcessOS.SalaryBasic == 0 )
                            {
                                if (_locPayrollProcessOS.Code != null)
                                {
                                    _currObjectId = _locPayrollProcessOS.Code;

                                    PayrollProcess _locPurchaseOrderLineXPO = _currSession.FindObject<PayrollProcess>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Code", _currObjectId)));

                                    if (_locPurchaseOrderLineXPO == null)
                                    {
                                        GetSalaryBasic(_currSession, _locPayrollProcessOS);
                                    }
                                    SuccessMessageShow("Salary Basic Has Been Updated");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Salary Basic Not Available");
                                }
                            }
                            
                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseOrderLine" + ex.ToString());
            }

        }

        #endregion GetSalaryBasic
        
        private void PayrollProcessCalculateTotalActionController_Execute(object sender, SimpleActionExecuteEventArgs e)
        {

        }

        private void PayrollProcessCalculateActionController_Execute(object sender, SimpleActionExecuteEventArgs e)
        {

        }

        #region Global Method
        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }
        #endregion Global Method

        #region GetSalary
        private void GetSalaryBasic(Session _currSession, PayrollProcess _payrollProcess)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                DateTime now = DateTime.Now;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                XPCollection<SalaryBasic> _locSalaryBasics = new XPCollection<SalaryBasic>(_currSession,
                                                               new GroupOperator(GroupOperatorType.And,
                                                               new BinaryOperator("Employee", _payrollProcess.Employee),
                                                               new BinaryOperator("Period", _payrollProcess.Period)));

                if (_locSalaryBasics != null && _locSalaryBasics.Count() > 0)
                {
                    foreach (SalaryBasic _locSalaryBasic in _locSalaryBasics)
                    {
                        if (_locSalaryBasic.SalaryBasicValue > 0)
                        {
                            PayrollProcess _locPayrollProcess = _currSession.FindObject<PayrollProcess>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                 new BinaryOperator("Code", _payrollProcess.Code)));

                            if (_payrollProcess != null)
                            {
                                _payrollProcess.SalaryBasic = _locSalaryBasic.SalaryBasicValue;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PayrollProcess" + ex.ToString());
            }
        }
        #endregion

        //
    }
}
