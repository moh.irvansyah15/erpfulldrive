﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using FullDrive.Module.BusinessObjects;
using DevExpress.ExpressApp.Xpo;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CuttingProcessActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;

        public CuttingProcessActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            CuttingProcessListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                CuttingProcessListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void CuttingProcessProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Status _locStatus = Status.None;
                DateTime _locNow;
                Status _locStatus2 = Status.None;
                DateTime _locNow2;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        CuttingProcess _locCuttingProcessOS = (CuttingProcess)_objectSpace.GetObject(obj);

                        if (_locCuttingProcessOS != null)
                        {
                            if (_locCuttingProcessOS.Code != null)
                            {
                                _currObjectId = _locCuttingProcessOS.Code;

                                CuttingProcess _locCuttingProcessXPO = _currSession.FindObject<CuttingProcess>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locCuttingProcessXPO != null)
                                {
                                    if (_locCuttingProcessXPO.Status == Status.Open || _locCuttingProcessXPO.Status == Status.Progress)
                                    {
                                        if (_locCuttingProcessXPO.Status == Status.Open)
                                        {
                                            _locStatus = Status.Progress;
                                            _locNow = now;
                                        }
                                        else
                                        {
                                            _locStatus = _locCuttingProcessXPO.Status;
                                            _locNow = _locCuttingProcessXPO.StatusDate;
                                        }

                                        _locCuttingProcessXPO.Status = _locStatus;
                                        _locCuttingProcessXPO.StatusDate = _locNow;
                                        _locCuttingProcessXPO.Save();
                                        _locCuttingProcessXPO.Session.CommitTransaction();

                                        XPCollection<CuttingProcessLine> _locCuttingProcessLines = new XPCollection<CuttingProcessLine>
                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("CuttingProcess", _locCuttingProcessXPO),
                                                            new BinaryOperator("Status", Status.Open)));

                                        if (_locCuttingProcessLines != null && _locCuttingProcessLines.Count > 0)
                                        {
                                            foreach (CuttingProcessLine _locCuttingProcessLine in _locCuttingProcessLines)
                                            {
                                                if (_locCuttingProcessLine.Status == Status.Open)
                                                {
                                                    _locStatus2 = Status.Progress;
                                                    _locNow2 = now;
                                                }
                                                else
                                                {
                                                    _locStatus2 = _locCuttingProcessLine.Status;
                                                    _locNow2 = _locCuttingProcessLine.StatusDate;
                                                }

                                                _locCuttingProcessLine.Status = _locStatus2;
                                                _locCuttingProcessLine.StatusDate = _locNow2;
                                                _locCuttingProcessLine.Save();
                                                _locCuttingProcessLine.Session.CommitTransaction();

                                                XPCollection<CuttingProcessLot> _locCuttingProcessLots = new XPCollection<CuttingProcessLot>
                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("CuttingProcessLine", _locCuttingProcessLine),
                                                                                                            new BinaryOperator("Status", Status.Open)));
                                                if(_locCuttingProcessLots != null && _locCuttingProcessLots.Count > 0)
                                                {
                                                    foreach(CuttingProcessLot _locCuttingProcessLot in _locCuttingProcessLots)
                                                    {
                                                        SetRemainAndStatusAndCuttingProcessLot(_currSession, _locCuttingProcessLot);
                                                        SetNewBeginingInventory(_currSession, _locCuttingProcessLot);
                                                        SetInventoryJournal(_currSession, _locCuttingProcessLot);
                                                        SetStatusProgressCuttingProcessLot(_currSession, _locCuttingProcessLot);
                                                    }
                                                }
                                            }
                                        }
                                        SuccessMessageShow("CP has successfully updated to progress");
                                    }                      
                                }
                                else
                                {
                                    ErrorMessageShow("Data Cutting Process Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Cutting Process Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesQuotation " + ex.ToString());
            }
        }

        private void CuttingProcessPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        CuttingProcess _locCuttingProcessOS = (CuttingProcess)_objectSpace.GetObject(obj);

                        if (_locCuttingProcessOS != null)
                        {
                            if (_locCuttingProcessOS.Code != null)
                            {
                                _currObjectId = _locCuttingProcessOS.Code;

                                CuttingProcess _locCuttingProcessXPO = _currSession.FindObject<CuttingProcess>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locCuttingProcessXPO != null)
                                {
                                    if (_locCuttingProcessXPO.Status == Status.Progress || _locCuttingProcessXPO.Status == Status.Posted)
                                    {
                                        if(CheckStatusCuttingProcess(_currSession, _locCuttingProcessXPO) == false)
                                        {
                                            SetFinalStatusCuttingProcess(_currSession, _locCuttingProcessXPO);
                                            SuccessMessageShow("CP has successfully posted");
                                        }
                                        else
                                        {
                                            ErrorMessageShow("Data LotNumber Not Available");
                                        }
                                    }
                                    
                                }
                                else
                                {
                                    ErrorMessageShow("Data Cutting Process Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Cutting Process Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesQuotation " + ex.ToString());
            }
        }

        private void CuttingProcessListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(CuttingProcess)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CuttingProcess " + ex.ToString());
            }
        }

        #region Progress

        private void SetRemainAndStatusAndCuttingProcessLot(Session _currSession, CuttingProcessLot _locCuttingProcessLotXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locRQtyAvailvable = 0;
                Status _locStatus = Status.Progress;
                
                if (_locCuttingProcessLotXPO != null)
                {
                    if(_locCuttingProcessLotXPO.Status == Status.Open && _locCuttingProcessLotXPO.ProcessCount == 0)
                    {
                        if (_locCuttingProcessLotXPO.BegInvLine != null)
                        {
                            if (_locCuttingProcessLotXPO.BegInvLine.QtyAvailable >= _locCuttingProcessLotXPO.NTQty)
                            {
                                _locRQtyAvailvable = _locCuttingProcessLotXPO.BegInvLine.QtyAvailable - _locCuttingProcessLotXPO.NTQty;
                                if (_locRQtyAvailvable < 0)
                                {
                                    _locStatus = Status.Open;
                                }
                            }
                            
                            BeginingInventoryLine _locBegInvLine = _currSession.FindObject<BeginingInventoryLine>
                                                                  (new GroupOperator(GroupOperatorType.And,
                                                                   new BinaryOperator("Code", _locCuttingProcessLotXPO.BegInvLine.Code)));
                            if(_locBegInvLine != null)
                            {
                                _locBegInvLine.LotNumber = _locCuttingProcessLotXPO.OldLotNumber;
                                _locBegInvLine.QtyAvailable = _locRQtyAvailvable;
                                _locBegInvLine.Save();
                                _locBegInvLine.Session.CommitTransaction();
                            }

                            _locCuttingProcessLotXPO.RQtyAvailable = _locRQtyAvailvable;
                            _locCuttingProcessLotXPO.Status = _locStatus;
                            _locCuttingProcessLotXPO.StatusDate = now;
                            _locCuttingProcessLotXPO.ProcessCount = _locCuttingProcessLotXPO.ProcessCount + 1;
                            _locCuttingProcessLotXPO.Save();
                            _locCuttingProcessLotXPO.Session.CommitTransaction();
                        }
                    }
                    
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CuttingProcess " + ex.ToString());
            }
        }

        private void SetNewBeginingInventory(Session _currSession, CuttingProcessLot _locCuttingProcessLotXPO)
        {
            try
            {
                if(_locCuttingProcessLotXPO != null)
                {
                    if(_locCuttingProcessLotXPO.Status == Status.Progress && _locCuttingProcessLotXPO.ProcessCount == 1)
                    {
                        if (_locCuttingProcessLotXPO.BegInvLine != null)
                        {
                            //Create New Beg Inv Line
                            if(_locCuttingProcessLotXPO.NewLotNumber != null && _locCuttingProcessLotXPO.NewLotNumberEnabled == false)
                            {
                                if(_locCuttingProcessLotXPO.NewLotNumber != _locCuttingProcessLotXPO.OldLotNumber)
                                {
                                    if (_locCuttingProcessLotXPO.BegInvLine.BeginingInventory != null)
                                    {
                                        if (_locCuttingProcessLotXPO.BegInvLine.BeginingInventory.Code != null)
                                        {
                                            BeginingInventory _locBegInv = _currSession.FindObject<BeginingInventory>
                                                                              (new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Code", _locCuttingProcessLotXPO.BegInvLine.BeginingInventory.Code)));
                                            if (_locBegInv != null)
                                            {
                                                BeginingInventoryLine _locSaveDataBeginingInventory = new BeginingInventoryLine(_currSession)
                                                {
                                                    Item = _locCuttingProcessLotXPO.Item,
                                                    Location = _locCuttingProcessLotXPO.Location,
                                                    BinLocation = _locCuttingProcessLotXPO.BinLocation,
                                                    QtyAvailable = _locCuttingProcessLotXPO.NTQty,
                                                    DefaultUOM = _locBegInv.DefaultUOM,
                                                    StockType = _locCuttingProcessLotXPO.StockType,
                                                    LotNumber = _locCuttingProcessLotXPO.NewLotNumber,
                                                    Active = true,
                                                    BeginingInventory = _locBegInv,
                                                };
                                                _locSaveDataBeginingInventory.Save();
                                                _locSaveDataBeginingInventory.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                }  
                            }                           
                        }
                    }       
                } 
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CuttingProcess ", ex.ToString());
            }
        }

        private void SetInventoryJournal(Session _currSession, CuttingProcessLot _locCuttingProcessLotXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locCuttingProcessLotXPO != null)
                {
                    if(_locCuttingProcessLotXPO.Status == Status.Progress && _locCuttingProcessLotXPO.ProcessCount == 1)
                    {
                        if (_locCuttingProcessLotXPO.CuttingProcessLine != null)
                        {
                            if (_locCuttingProcessLotXPO.CuttingProcessLine.CuttingProcess != null)
                            {
                                if (_locCuttingProcessLotXPO.CuttingProcessLine.CuttingProcess.Code != null)
                                {
                                    CuttingProcess _locCuttingProcess = _currSession.FindObject<CuttingProcess>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _locCuttingProcessLotXPO.CuttingProcessLine.CuttingProcess.Code)));
                                    if (_locCuttingProcess != null)
                                    {
                                        if (_locCuttingProcessLotXPO.NewLotNumberEnabled == true && _locCuttingProcessLotXPO.NewLotNumber == null)
                                        {
                                            InventoryJournal _locNegatifInventoryJournal = new InventoryJournal(_currSession)
                                            {
                                                DocumentType = _locCuttingProcess.DocumentType,
                                                DocNo = _locCuttingProcess.DocNo,
                                                Location = _locCuttingProcessLotXPO.Location,
                                                BinLocation = _locCuttingProcessLotXPO.BinLocation,
                                                StockType = _locCuttingProcessLotXPO.StockType,
                                                Item = _locCuttingProcessLotXPO.Item,
                                                LotNumber = _locCuttingProcessLotXPO.OldLotNumber,
                                                QtyNeg = _locCuttingProcessLotXPO.NTQty,
                                                QtyPos = 0,
                                                DUOM = _locCuttingProcessLotXPO.NDUOM,
                                                JournalDate = now,
                                                CuttingProcessLot = _locCuttingProcessLotXPO,
                                                Company = _locCuttingProcess.Company,
                                            };
                                            _locNegatifInventoryJournal.Save();
                                            _locNegatifInventoryJournal.Session.CommitTransaction();
                                        }
                                        else if (_locCuttingProcessLotXPO.NewLotNumberEnabled == false && _locCuttingProcessLotXPO.NewLotNumber != null)
                                        {
                                            if (_locCuttingProcessLotXPO.NewLotNumber != _locCuttingProcessLotXPO.OldLotNumber)
                                            {
                                                InventoryJournal _locPositifInventoryJournal = new InventoryJournal(_currSession)
                                                {
                                                    DocumentType = _locCuttingProcess.DocumentType,
                                                    DocNo = _locCuttingProcess.DocNo,
                                                    Location = _locCuttingProcessLotXPO.Location,
                                                    BinLocation = _locCuttingProcessLotXPO.BinLocation,
                                                    StockType = _locCuttingProcessLotXPO.StockType,
                                                    Item = _locCuttingProcessLotXPO.Item,
                                                    LotNumber = _locCuttingProcessLotXPO.NewLotNumber,
                                                    QtyNeg = 0,
                                                    QtyPos = _locCuttingProcessLotXPO.NTQty,
                                                    DUOM = _locCuttingProcessLotXPO.NDUOM,
                                                    JournalDate = now,
                                                    CuttingProcessLot = _locCuttingProcessLotXPO,
                                                    Company = _locCuttingProcess.Company,
                                                };
                                                _locPositifInventoryJournal.Save();
                                                _locPositifInventoryJournal.Session.CommitTransaction();

                                                InventoryJournal _locNegatifInventoryJournal = new InventoryJournal(_currSession)
                                                {
                                                    DocumentType = _locCuttingProcess.DocumentType,
                                                    DocNo = _locCuttingProcess.DocNo,
                                                    Location = _locCuttingProcessLotXPO.Location,
                                                    BinLocation = _locCuttingProcessLotXPO.BinLocation,
                                                    StockType = _locCuttingProcessLotXPO.StockType,
                                                    Item = _locCuttingProcessLotXPO.Item,
                                                    LotNumber = _locCuttingProcessLotXPO.OldLotNumber,
                                                    QtyNeg = _locCuttingProcessLotXPO.NTQty,
                                                    QtyPos = 0,
                                                    DUOM = _locCuttingProcessLotXPO.NDUOM,
                                                    JournalDate = now,
                                                    CuttingProcessLot = _locCuttingProcessLotXPO,
                                                    Company = _locCuttingProcess.Company,
                                                };
                                                _locNegatifInventoryJournal.Save();
                                                _locNegatifInventoryJournal.Session.CommitTransaction();
                                            }

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CuttingProcess ", ex.ToString());
            }
        }

        private void SetStatusProgressCuttingProcessLot(Session _currSession, CuttingProcessLot _locCuttingProcessLotXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locCuttingProcessLotXPO != null)
                {
                    if(_locCuttingProcessLotXPO.Status == Status.Progress && _locCuttingProcessLotXPO.ProcessCount == 1)
                    {
                        _locCuttingProcessLotXPO.Status = Status.Close;
                        _locCuttingProcessLotXPO.StatusDate = now;
                        _locCuttingProcessLotXPO.Save();
                        _locCuttingProcessLotXPO.Session.CommitTransaction();
                    }    
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CuttingProcess " + ex.ToString());
            }
        }

        #endregion Progress

        #region Posting

        //Check All CuttingProcess
        //Set SalesQuotation
        //Set CloseStatus CusttingProcess
        private bool CheckStatusCuttingProcess(Session _currSession, CuttingProcess _locCuttingProcessXPO)
        {
            bool _result = false;
            try
            {
                if(_locCuttingProcessXPO != null)
                {
                    XPCollection<CuttingProcessLine> _locCuttingProcessLines = new XPCollection<CuttingProcessLine>
                                                                               (_currSession, new GroupOperator(GroupOperatorType.And, 
                                                                               new BinaryOperator("CuttingProcess", _locCuttingProcessXPO)));
                    if(_locCuttingProcessLines != null && _locCuttingProcessLines.Count() > 0)
                    {
                        foreach(CuttingProcessLine _locCuttingProcessLine in _locCuttingProcessLines)
                        {
                            XPCollection<CuttingProcessLot> _locCuttingProcessLots = new XPCollection<CuttingProcessLot>
                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("CuttingProcessLine", _locCuttingProcessLine)));

                            if(_locCuttingProcessLots != null && _locCuttingProcessLots.Count() > 0)
                            {
                                foreach(CuttingProcessLot _locCuttingProcessLot in _locCuttingProcessLots)
                                {
                                    if(_locCuttingProcessLot.Status == Status.Open && _locCuttingProcessLot.ProcessCount > 0)
                                    {
                                        _result = false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CuttingProcess ", ex.ToString());
            }
            return _result;
        }
        
        private void SetFinalStatusCuttingProcess(Session _currSession, CuttingProcess _locCuttingProcessXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locCuttingProcessXPO != null)
                {
                    XPCollection<CuttingProcessLine> _locCuttingProcessLines = new XPCollection<CuttingProcessLine>
                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("CuttingProcess", _locCuttingProcessXPO)));
                    if(_locCuttingProcessLines != null && _locCuttingProcessLines.Count() > 0)
                    {
                        foreach(CuttingProcessLine _locCuttingProcessLine in _locCuttingProcessLines)
                        {
                            XPCollection<CuttingProcessLot> _locCuttingProcessLots = new XPCollection<CuttingProcessLot>
                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("CuttingProcessLine", _locCuttingProcessLine)));
                            if(_locCuttingProcessLots != null && _locCuttingProcessLots.Count() > 0)
                            {
                                foreach(CuttingProcessLot _locCuttingProcessLot in _locCuttingProcessLots)
                                {
                                    _locCuttingProcessLot.ActivationPosting = true;
                                    _locCuttingProcessLot.Status = Status.Close;
                                    _locCuttingProcessLot.StatusDate = now;
                                    _locCuttingProcessLot.Save();
                                    _locCuttingProcessLot.Session.CommitTransaction();
                                }
                            }

                            _locCuttingProcessLine.ActivationPosting = true;
                            _locCuttingProcessLine.Status = Status.Close;
                            _locCuttingProcessLine.StatusDate = now;
                            _locCuttingProcessLine.Save();
                            _locCuttingProcessLine.Session.CommitTransaction();
                            
                        }
                        _locCuttingProcessXPO.ActivationPosting = true;
                        _locCuttingProcessXPO.Status = Status.Close;
                        _locCuttingProcessXPO.StatusDate = now;
                        _locCuttingProcessXPO.Save();
                        _locCuttingProcessXPO.Session.CommitTransaction();
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CuttingProcess ", ex.ToString());
            }
        }

        #endregion Posting

        #region Global

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }


        #endregion Global

        
    }
}
