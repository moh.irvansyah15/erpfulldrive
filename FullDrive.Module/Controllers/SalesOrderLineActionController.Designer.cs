﻿namespace FullDrive.Module.Controllers
{
    partial class SalesOrderLineActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SalesOrderLineTaxCalculationAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesOrderLineDiscCalculationAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesOrderLineListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.SalesOrderLineSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesOrderLineUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // SalesOrderLineTaxCalculationAction
            // 
            this.SalesOrderLineTaxCalculationAction.Caption = "Tax Calculation";
            this.SalesOrderLineTaxCalculationAction.ConfirmationMessage = null;
            this.SalesOrderLineTaxCalculationAction.Id = "SalesOrderLineTaxCalculationActionId";
            this.SalesOrderLineTaxCalculationAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesOrderLine);
            this.SalesOrderLineTaxCalculationAction.ToolTip = null;
            this.SalesOrderLineTaxCalculationAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesOrderLineTaxCalculationAction_Execute);
            // 
            // SalesOrderLineDiscCalculationAction
            // 
            this.SalesOrderLineDiscCalculationAction.Caption = "Disc Calculation";
            this.SalesOrderLineDiscCalculationAction.ConfirmationMessage = null;
            this.SalesOrderLineDiscCalculationAction.Id = "SalesOrderLineDiscCalculationActionId";
            this.SalesOrderLineDiscCalculationAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesOrderLine);
            this.SalesOrderLineDiscCalculationAction.ToolTip = null;
            this.SalesOrderLineDiscCalculationAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesOrderLineDiscCalculationAction_Execute);
            // 
            // SalesOrderLineListviewFilterSelectionAction
            // 
            this.SalesOrderLineListviewFilterSelectionAction.Caption = "Filter";
            this.SalesOrderLineListviewFilterSelectionAction.ConfirmationMessage = null;
            this.SalesOrderLineListviewFilterSelectionAction.Id = "SalesOrderLineListviewFilterSelectionActionId";
            this.SalesOrderLineListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.SalesOrderLineListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesOrderLine);
            this.SalesOrderLineListviewFilterSelectionAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.SalesOrderLineListviewFilterSelectionAction.ToolTip = null;
            this.SalesOrderLineListviewFilterSelectionAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.SalesOrderLineListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.SalesOrderLineListviewFilterSelectionAction_Execute);
            // 
            // SalesOrderLineSelectAction
            // 
            this.SalesOrderLineSelectAction.Caption = "Select";
            this.SalesOrderLineSelectAction.ConfirmationMessage = null;
            this.SalesOrderLineSelectAction.Id = "SalesOrderLineSelectActionId";
            this.SalesOrderLineSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesOrderLine);
            this.SalesOrderLineSelectAction.ToolTip = null;
            this.SalesOrderLineSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesOrderLineSelectAction_Execute);
            // 
            // SalesOrderLineUnselectAction
            // 
            this.SalesOrderLineUnselectAction.Caption = "Unselect";
            this.SalesOrderLineUnselectAction.ConfirmationMessage = null;
            this.SalesOrderLineUnselectAction.Id = "SalesOrderLineUnselectActionId";
            this.SalesOrderLineUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesOrderLine);
            this.SalesOrderLineUnselectAction.ToolTip = null;
            this.SalesOrderLineUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesOrderLineUnselectAction_Execute);
            // 
            // SalesOrderLineActionController
            // 
            this.Actions.Add(this.SalesOrderLineTaxCalculationAction);
            this.Actions.Add(this.SalesOrderLineDiscCalculationAction);
            this.Actions.Add(this.SalesOrderLineListviewFilterSelectionAction);
            this.Actions.Add(this.SalesOrderLineSelectAction);
            this.Actions.Add(this.SalesOrderLineUnselectAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction SalesOrderLineTaxCalculationAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesOrderLineDiscCalculationAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction SalesOrderLineListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesOrderLineSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesOrderLineUnselectAction;
    }
}
