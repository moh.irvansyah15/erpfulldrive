﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.BusinessObjects;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;
using FullDrive.Module.CustomProcess;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class InventoryProductionDetailController : ViewController
    {
        #region Default

        public InventoryProductionDetailController()
        {
            InitializeComponent();
            RegisterActions(components);
            TargetViewId = "InventoryProduction_DetailView";
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            TypeOfView = typeof(ObjectView);
            base.OnActivated();
            var _locUser = SecuritySystem.CurrentUserName;
            GlobalFunction _globalFunction = new GlobalFunction();
            Session _currentSession = null;

            InventoryProduction _locInventoryProduction = (InventoryProduction)View.CurrentObject;
            View.ObjectSpace.SetModified(_locInventoryProduction);

            if (this.ObjectSpace != null)
            {
                _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
            }

            if (_currentSession != null)
            {
                if (_locInventoryProduction != null)
                {
                    UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", _locUser));

                    if (_locUserAccess != null)
                    {
                        if (GetQC() == false)
                        {
                            _locInventoryProduction.SelectHide = false;
                        }
                        else
                        {
                            _locInventoryProduction.SelectHide = true;
                        }
                    }
                }
            }

            if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
            {
                View.ObjectSpace.CommitChanges();
            }
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        public bool GetQC()
        {
            bool _result = false;
            try
            {
                Session _locCurrentSession = ((XPObjectSpace)this.ObjectSpace).Session;
                var _locUserName = SecuritySystem.CurrentUserName;

                UserAccess _numLineUserAccess = _locCurrentSession.FindObject<UserAccess>
                                                (new BinaryOperator("UserName", _locUserName));

                if (_numLineUserAccess != null)
                {
                    ApplicationSetup _numLineAppSetup = _locCurrentSession.FindObject<ApplicationSetup>(new GroupOperator
                                                        (GroupOperatorType.And,
                                                         new BinaryOperator("Active", true),
                                                         new BinaryOperator("DefaultSystem", true)));

                    if (_numLineAppSetup != null)
                    {
                        ApplicationSetupDetail _locAppSetupDetail = _locCurrentSession.FindObject<ApplicationSetupDetail>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("UserAccess", _numLineUserAccess),
                                                                     new BinaryOperator("FunctionList", CustomProcess.FunctionList.QcPassed),
                                                                     new BinaryOperator("Active", true)));

                        if (_locAppSetupDetail != null)
                        {
                            _result = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = Pre Booking " + ex.ToString());
            }
            return _result;
        }
    }
}
