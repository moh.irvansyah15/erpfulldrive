﻿namespace FullDrive.Module.Controllers
{
    partial class InventoryTransferOrderActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.InventoryTransferOrderProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.InventoryTransferOrderPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.InventoryTransferOrderListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.InventoryTransferOrderListviewFilterApprovalSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // InventoryTransferOrderProgressAction
            // 
            this.InventoryTransferOrderProgressAction.Caption = "Progress";
            this.InventoryTransferOrderProgressAction.ConfirmationMessage = null;
            this.InventoryTransferOrderProgressAction.Id = "InventoryTransferOrderProgressActionId";
            this.InventoryTransferOrderProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryTransferOrder);
            this.InventoryTransferOrderProgressAction.ToolTip = null;
            this.InventoryTransferOrderProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InventoryTransferOrderProgressAction_Execute);
            // 
            // InventoryTransferOrderPostingAction
            // 
            this.InventoryTransferOrderPostingAction.Caption = "Posting";
            this.InventoryTransferOrderPostingAction.ConfirmationMessage = null;
            this.InventoryTransferOrderPostingAction.Id = "InventoryTransferOrderPostingActionId";
            this.InventoryTransferOrderPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryTransferOrder);
            this.InventoryTransferOrderPostingAction.ToolTip = null;
            this.InventoryTransferOrderPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InventoryTransferOrderPostingAction_Execute);
            // 
            // InventoryTransferOrderListviewFilterSelectionAction
            // 
            this.InventoryTransferOrderListviewFilterSelectionAction.Caption = "Filter";
            this.InventoryTransferOrderListviewFilterSelectionAction.ConfirmationMessage = null;
            this.InventoryTransferOrderListviewFilterSelectionAction.Id = "InventoryTransferOrderListviewFilterSelectionActionId";
            this.InventoryTransferOrderListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.InventoryTransferOrderListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryTransferOrder);
            this.InventoryTransferOrderListviewFilterSelectionAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.InventoryTransferOrderListviewFilterSelectionAction.ToolTip = null;
            this.InventoryTransferOrderListviewFilterSelectionAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.InventoryTransferOrderListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.InventoryTransferOrderListviewFilterSelectionAction_Execute);
            // 
            // InventoryTransferOrderListviewFilterApprovalSelectionAction
            // 
            this.InventoryTransferOrderListviewFilterApprovalSelectionAction.Caption = "Filter Approval";
            this.InventoryTransferOrderListviewFilterApprovalSelectionAction.ConfirmationMessage = null;
            this.InventoryTransferOrderListviewFilterApprovalSelectionAction.Id = "InventoryTransferOrderListviewFilterApprovalSelectionActionId";
            this.InventoryTransferOrderListviewFilterApprovalSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.InventoryTransferOrderListviewFilterApprovalSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryTransferOrder);
            this.InventoryTransferOrderListviewFilterApprovalSelectionAction.ToolTip = null;
            this.InventoryTransferOrderListviewFilterApprovalSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.InventoryTransferOrderListviewFilterApprovalSelectionAction_Execute);
            // 
            // InventoryTransferOrderActionController
            // 
            this.Actions.Add(this.InventoryTransferOrderProgressAction);
            this.Actions.Add(this.InventoryTransferOrderPostingAction);
            this.Actions.Add(this.InventoryTransferOrderListviewFilterSelectionAction);
            this.Actions.Add(this.InventoryTransferOrderListviewFilterApprovalSelectionAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction InventoryTransferOrderProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction InventoryTransferOrderPostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction InventoryTransferOrderListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction InventoryTransferOrderListviewFilterApprovalSelectionAction;
    }
}
