﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class InventoryProductionActionController : ViewController
    {
        #region Default

        public InventoryProductionActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        private void InventoryProductionProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        InventoryProduction _locInventoryProductionOS = (InventoryProduction)_objectSpace.GetObject(obj);

                        if (_locInventoryProductionOS != null)
                        {
                            if (_locInventoryProductionOS.Code != null)
                            {
                                _currObjectId = _locInventoryProductionOS.Code;

                                InventoryProduction _locInventoryProductionXPO = _currSession.FindObject<InventoryProduction>
                                                                               (new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Code", _currObjectId)));

                                if (_locInventoryProductionXPO != null)
                                {
                                    if (_locInventoryProductionXPO.Status == Status.Open)
                                    {
                                        _locInventoryProductionXPO.Status = Status.Progress;
                                        _locInventoryProductionXPO.StatusDate = now;
                                        _locInventoryProductionXPO.Save();
                                        _locInventoryProductionXPO.Session.CommitTransaction();

                                        XPCollection<InventoryProductionLine> _locInventoryProductionLines = new XPCollection<InventoryProductionLine>
                                                                                                             (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                              new BinaryOperator("InventoryProduction", _locInventoryProductionXPO)));

                                        if (_locInventoryProductionLines != null && _locInventoryProductionLines.Count > 0)
                                        {
                                            foreach (InventoryProductionLine _locInventoryProductionLine in _locInventoryProductionLines)
                                            {
                                                if (_locInventoryProductionLine.Status == Status.Open)
                                                {
                                                    _locInventoryProductionLine.Status = Status.Progress;
                                                    _locInventoryProductionLine.StatusDate = now;
                                                    _locInventoryProductionLine.Save();
                                                    _locInventoryProductionLine.Session.CommitTransaction();

                                                }
                                            }
                                        }
                                        SetBeginingInventory(_currSession, _locInventoryProductionXPO);
                                        SuccessMessageShow(_locInventoryProductionXPO.Code + " has been change successfully to Progress");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data InventoryProduction Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data InventoryProduction Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryProduction " + ex.ToString());
            }
        }

        private void InventoryProductionPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        InventoryProduction _locInventoryProductionOS = (InventoryProduction)_objectSpace.GetObject(obj);

                        if (_locInventoryProductionOS != null)
                        {
                            if (_locInventoryProductionOS.Code != null)
                            {
                                _currObjectId = _locInventoryProductionOS.Code;

                                InventoryProduction _locInventoryProductionXPO = _currSession.FindObject<InventoryProduction>
                                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("Code", _currObjectId)));

                                if (_locInventoryProductionXPO != null)
                                {
                                    XPCollection<InventoryProductionLine> _locInventoryProductionLineXPO = new XPCollection<InventoryProductionLine>(_currSession,
                                                                                                           new GroupOperator(GroupOperatorType.And,
                                                                                                           new BinaryOperator("Status", Status.Progress),
                                                                                                           new BinaryOperator("QcPassed", QCPassed.Good),
                                                                                                           new BinaryOperator("InventoryProduction", _locInventoryProductionXPO)));

                                    foreach (InventoryProductionLine _locInventoryProductionLine in _locInventoryProductionLineXPO)
                                    {
                                        if (_locInventoryProductionXPO.Status == Status.Progress || _locInventoryProductionXPO.Status == Status.Posted)
                                        {
                                            if (_locInventoryProductionLineXPO != null && _locInventoryProductionLineXPO.Count > 0)
                                            {
                                                SetTransferIn(_currSession, _locInventoryProductionXPO);
                                                SetStatusInventoryProductionLine(_currSession, _locInventoryProductionXPO);
                                                SetPostedCount(_currSession, _locInventoryProductionXPO);
                                                SetFinalStatusInventoryProduction(_currSession, _locInventoryProductionXPO);
                                                SuccessMessageShow(_locInventoryProductionXPO.Code + " has been change successfully to Posted");
                                            }
                                            else
                                            {
                                                ErrorMessageShow("Data InventoryProductionLine Not Available");
                                            }
                                        }
                                        else
                                        {
                                            ErrorMessageShow("Data InventoryProduction Not Available");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data InventoryProduction Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data InventoryProduction Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Data InventoryProduction Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryProduction " + ex.ToString());
            }
        }

        private void InventoryProductionFilterAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(InventoryProduction)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryProduction " + ex.ToString());
            }
        }

        private void InventoryProductionGenerateLotAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        InventoryProduction _locInventoryProductionOS = (InventoryProduction)_objectSpace.GetObject(obj);

                        if (_locInventoryProductionOS != null)
                        {
                            if (_locInventoryProductionOS.Code != null)
                            {
                                _currObjectId = _locInventoryProductionOS.Code;

                                InventoryProduction _locInventoryProductionXPO = _currSession.FindObject<InventoryProduction>
                                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("Code", _currObjectId)));

                                if (_locInventoryProductionXPO != null)
                                {
                                    if (_locInventoryProductionXPO.Status == Status.Open)
                                    {
                                        if (_locInventoryProductionXPO.Count > 0 && _locInventoryProductionXPO.QtyPackage > 0)
                                        {
                                            SetInventoryProductionLotLooping(_currSession, _locInventoryProductionXPO);
                                            SetInventoryProductionPostedCount(_currSession, _locInventoryProductionXPO);
                                            SuccessMessageShow(_locInventoryProductionXPO.Code + " has been change successfully to Posted");
                                        }
                                        else
                                        {
                                            ErrorMessageShow("Data InventoryProduction Count & QtyPackage Not Available");
                                        }
                                    }
                                    else
                                    {
                                        ErrorMessageShow("Data Status InventoryProduction Not Available");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data InventoryProduction Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data InventoryProduction Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Data InventoryProduction Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryProduction " + ex.ToString());
            }
        }

        private void InventoryProductionActualweightAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        InventoryProduction _locInventoryProductionOS = (InventoryProduction)_objectSpace.GetObject(obj);

                        if (_locInventoryProductionOS != null)
                        {
                            if (_locInventoryProductionOS.Code != null)
                            {
                                _currObjectId = _locInventoryProductionOS.Code;

                                InventoryProduction _locInventoryProductionXPO = _currSession.FindObject<InventoryProduction>
                                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("Code", _currObjectId)));

                                if (_locInventoryProductionXPO != null)
                                {
                                    if (_locInventoryProductionXPO.Status == Status.Open || _locInventoryProductionXPO.Status == Status.Progress)
                                    {
                                        UpdateInventoryProductionWeightLooping(_currSession, _locInventoryProductionXPO);
                                        SetInventoryProductionPostedCountWeight(_currSession, _locInventoryProductionXPO);
                                        SuccessMessageShow(_locInventoryProductionXPO.Code + " has been change successfully to Posted");
                                    }
                                    else
                                    {
                                        ErrorMessageShow("Data Status InventoryProduction Not Available");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data InventoryProduction Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data InventoryProduction Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Data InventoryProduction Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryProduction " + ex.ToString());
            }
        }

        private void InventoryProductionQcPassedAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                var _locUser = SecuritySystem.CurrentUserName;
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Location _locLocation = null;
                BinLocation _locBinLocation = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        InventoryProduction _locInventoryProductionOS = (InventoryProduction)_objectSpace.GetObject(obj);

                        if (_locInventoryProductionOS != null)
                        {
                            UserAccess _locUserAccess = _currSession.FindObject<UserAccess>(new BinaryOperator("UserName", _locUser));

                            if (_locUserAccess != null)
                            {
                                if (_locInventoryProductionOS.Code != null)
                                {
                                    _currObjectId = _locInventoryProductionOS.Code;

                                    InventoryProduction _locInventoryProductionXPO = _currSession.FindObject<InventoryProduction>
                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                      new BinaryOperator("Code", _currObjectId)));

                                    if (_locInventoryProductionXPO != null)
                                    {
                                        WarehouseSetupDetail _locWarehouseSetupDetail = _currSession.FindObject<WarehouseSetupDetail>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("UserAccess", _locUserAccess),
                                                                                         new BinaryOperator("Default", true),
                                                                                         new BinaryOperator("Active", true),
                                                                                         new BinaryOperator("LocationType", LocationType.Main),
                                                                                         new BinaryOperator("LocationType2", LocationType2.QC)));
                                        if (_locWarehouseSetupDetail != null)
                                        {
                                            if (_locWarehouseSetupDetail.Location != null)
                                            {
                                                _locLocation = _locWarehouseSetupDetail.Location;
                                            }
                                            if (_locWarehouseSetupDetail.BinLocation != null)
                                            {
                                                _locBinLocation = _locWarehouseSetupDetail.BinLocation;
                                            }
                                        }

                                        XPCollection<InventoryProductionLine> _locInventoryProductionLines = new XPCollection<InventoryProductionLine>
                                                                                                             (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                              new BinaryOperator("Select", true),
                                                                                                              new BinaryOperator("Status", Status.Progress),
                                                                                                              new BinaryOperator("InventoryProduction", _locInventoryProductionXPO)));

                                        if (_locInventoryProductionLines != null && _locInventoryProductionLines.Count > 0)
                                        {
                                            foreach (InventoryProductionLine _locInventoryProductionLine in _locInventoryProductionLines)
                                            {
                                                if (_locInventoryProductionLine.PostCountQcPassed < 1)
                                                {
                                                    _locInventoryProductionLine.PostCountQcReject = 0;
                                                    _locInventoryProductionLine.Location = _locLocation;
                                                    _locInventoryProductionLine.BinLocation = _locBinLocation;
                                                    _locInventoryProductionLine.QcPassed = QCPassed.Good;
                                                    _locInventoryProductionLine.PostCountQcPassed = 1;
                                                    _locInventoryProductionLine.Select = false;
                                                    _locInventoryProductionLine.ActiveApproved1 = false;
                                                    _locInventoryProductionLine.ActiveApproved2 = true;
                                                    _locInventoryProductionLine.ActiveApproved3 = false;
                                                    _locInventoryProductionLine.StatusDate = now;
                                                    _locInventoryProductionLine.Save();
                                                    _locInventoryProductionLine.Session.CommitTransaction();

                                                    SetBeginingInventory2(_currSession, _locInventoryProductionLine, _locLocation, _locBinLocation);
                                                    SuccessMessageShow("InventoryProductionLine has been Passed");
                                                }
                                            }
                                            _locInventoryProductionXPO.Location = _locLocation;
                                            _locInventoryProductionXPO.BinLocation = _locBinLocation;
                                            _locInventoryProductionXPO.Save();
                                            _locInventoryProductionXPO.Session.CommitTransaction();
                                        }
                                        else
                                        {
                                            ErrorMessageShow("Data InventoryProductionLine Not Available");
                                        }
                                    }
                                    else
                                    {
                                        ErrorMessageShow("Data InventoryProduction Not Available");
                                    }
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = InventoryProduction" + ex.ToString());
            }
        }

        private void InventoryProductionQcRejectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        InventoryProduction _locInventoryProductionOS = (InventoryProduction)_objectSpace.GetObject(obj);

                        if (_locInventoryProductionOS != null)
                        {
                            if (_locInventoryProductionOS.Code != null)
                            {
                                _currObjectId = _locInventoryProductionOS.Code;

                                InventoryProduction _locInventoryProductionXPO = _currSession.FindObject<InventoryProduction>
                                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("Code", _currObjectId)));

                                if (_locInventoryProductionXPO != null)
                                {
                                    XPCollection<InventoryProductionLine> _locInventoryProductionLines = new XPCollection<InventoryProductionLine>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("InventoryProduction", _locInventoryProductionXPO),
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Select", true)));

                                    if (_locInventoryProductionLines != null && _locInventoryProductionLines.Count > 0)
                                    {
                                        foreach (InventoryProductionLine _locInventoryProductionLine in _locInventoryProductionLines)
                                        {
                                            if (_locInventoryProductionLine.PostCountQcReject < 1)
                                            {
                                                _locInventoryProductionLine.QcPassed = QCPassed.Bad;
                                                _locInventoryProductionLine.PostCountQcReject = 1;
                                                _locInventoryProductionLine.Select = false;
                                                _locInventoryProductionLine.ActiveApproved1 = false;
                                                _locInventoryProductionLine.ActiveApproved2 = false;
                                                _locInventoryProductionLine.ActiveApproved3 = true;
                                                _locInventoryProductionLine.StatusDate = now;
                                                _locInventoryProductionLine.Save();
                                                _locInventoryProductionLine.Session.CommitTransaction();
                                                SuccessMessageShow("InventoryProductionLine has been Reject");
                                            }
                                        }
                                    }
                                    else
                                    {
                                        ErrorMessageShow("Data InventoryProductionLine Not Available");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data InventoryProduction Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = InventoryProduction" + ex.ToString());
            }
        }

        //==== Code Only =====

        private void SetBeginingInventory(Session _currSession, InventoryProduction _locInventoryProductionXPO)
        {
            try
            {
                XPCollection<InventoryProductionLine> _locInvProdLines = new XPCollection<InventoryProductionLine>(_currSession,
                                                                         new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("InventoryProduction", _locInventoryProductionXPO)));

                if (_locInvProdLines != null && _locInvProdLines.Count() > 0)
                {
                    GlobalFunction _globFunc = new GlobalFunction();
                    //double _locInvLineTotal = 0;
                    string _locSignCode = null;

                    if (_locInvProdLines != null && _locInvProdLines.Count() > 0)
                    {
                        foreach (InventoryProductionLine _locInvProdLine in _locInvProdLines)
                        {
                            BeginingInventory _locBeginingInventory = _currSession.FindObject<BeginingInventory>
                                                                      (new GroupOperator(GroupOperatorType.And,
                                                                       new BinaryOperator("Item", _locInvProdLine.Item)));

                            #region UpdateBeginingInventory
                            if (_locBeginingInventory != null)
                            {
                                BeginingInventoryLine _locSaveDataBeginingInventory = new BeginingInventoryLine(_currSession)
                                {
                                    Item = _locInvProdLine.Item,
                                    QtyAvailable = _locInvProdLine.QtyAvailable,
                                    StockType = StockType.Good,
                                    Location = _locInvProdLine.Location,
                                    BinLocation = _locInvProdLine.BinLocation,
                                    LotNumber = _locInvProdLine.LotNumber,
                                    Active = true,
                                    BeginingInventory = _locBeginingInventory,
                                };
                                _locSaveDataBeginingInventory.Save();
                                _locSaveDataBeginingInventory.Session.CommitTransaction();
                            }
                            #endregion UpdateBeginingInventory

                            #region CreateNewBeginingInventory
                            else
                            {
                                BeginingInventory _saveDataBegInv = new BeginingInventory(_currSession)
                                {
                                    Item = _locInvProdLine.Item,
                                    QtyAvailable = _locInvProdLine.QtyAvailable,
                                    Active = true,
                                    SignCode = _locSignCode,
                                    InventoryProduction = _locInventoryProductionXPO,
                                    Company = _locInventoryProductionXPO.Company,
                                };
                                _saveDataBegInv.Save();
                                _saveDataBegInv.Session.CommitTransaction();

                                BeginingInventory _locBeginingInventory2 = _currSession.FindObject<BeginingInventory>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Item", _locInvProdLine.Item)));

                                if (_locBeginingInventory2 != null)
                                {
                                    BeginingInventoryLine _saveDataBegInvLine = new BeginingInventoryLine(_currSession)
                                    {
                                        Item = _locInvProdLine.Item,
                                        QtyAvailable = _locBeginingInventory2.QtyAvailable,
                                        StockType = StockType.Good,
                                        DefaultUOM = _locBeginingInventory2.DefaultUOM,
                                        Location = _locInvProdLine.Location,
                                        BinLocation = _locInvProdLine.BinLocation,
                                        LotNumber = _locInvProdLine.LotNumber,
                                        Active = true,
                                        BeginingInventory = _locBeginingInventory2,
                                        Company = _locBeginingInventory2.Company,
                                    };
                                    _saveDataBegInvLine.Save();
                                    _saveDataBegInvLine.Session.CommitTransaction();
                                }

                                _locSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.BeginingInventory);


                            }
                            #endregion CreateNewBeginingInventory

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryProduction ", ex.ToString());
            }
        }

        private void SetBeginingInventory2(Session _currSession, InventoryProductionLine _locInventoryProductionLine, Location _locLocation, BinLocation _locBinLocation)
        {
            try
            {
                if (_locInventoryProductionLine != null)
                {
                    BeginingInventory _locBeginingInventory = _currSession.FindObject<BeginingInventory>
                                                                      (new GroupOperator(GroupOperatorType.And,
                                                                       new BinaryOperator("Item", _locInventoryProductionLine.Item)));

                    #region UpdateBeginingInventory
                    if (_locBeginingInventory != null)
                    {
                        BeginingInventoryLine _locBegInvLine = _currSession.FindObject<BeginingInventoryLine>
                                                                      (new GroupOperator(GroupOperatorType.And,
                                                                       new BinaryOperator("Item", _locInventoryProductionLine.Item),
                                                                       new BinaryOperator("LotNumber", _locInventoryProductionLine.LotNumber),
                                                                       new BinaryOperator("BeginingInventory", _locBeginingInventory)));

                        if (_locBegInvLine != null)
                        {
                            _locBegInvLine.Location = _locLocation;
                            _locBegInvLine.BinLocation = _locBinLocation;
                            _locBegInvLine.Save();
                            _locBegInvLine.Session.CommitTransaction();
                        }
                    }
                    #endregion UpdateBeginingInventory
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryProduction ", ex.ToString());
            }
        }

        #region Posting

        private void SetTransferIn(Session _currSession, InventoryProduction _locInventoryProductionXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _locDocCode = null;
                string _currSignCode = null;
                DocumentType _locDocumentType = null;
                double _locTDQTy = 0;
                Location _locLocation = null;
                BinLocation _locBinLocation = null;

                if (_locInventoryProductionXPO != null)
                {


                    _locDocumentType = _currSession.FindObject<DocumentType>
                                       (new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("TransferType", DirectionType.Internal),
                                        new BinaryOperator("InventoryMovingType", InventoryMovingType.TransferIn),
                                        new BinaryOperator("ObjectList", ObjectList.TransferIn),
                                        new BinaryOperator("Active", true)));

                    if (_locDocumentType != null)
                    {
                        if (_locInventoryProductionXPO.DocNo != null)
                        {
                            _locDocCode = _locInventoryProductionXPO.DocNo;
                        }

                        if (_locDocCode != null)
                        {
                            #region GetWarehouseLocation
                            WarehouseSetupDetail _locWarehouseSetupDetail = _currSession.FindObject<WarehouseSetupDetail>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                       new BinaryOperator("Default", true),
                                                                                       new BinaryOperator("Active", true),
                                                                                       new BinaryOperator("LocationType", LocationType.Main),
                                                                                       new BinaryOperator("LocationType2", LocationType2.FG)));
                            if (_locWarehouseSetupDetail != null)
                            {
                                if (_locWarehouseSetupDetail.Location != null)
                                {
                                    _locLocation = _locWarehouseSetupDetail.Location;
                                }
                                if (_locWarehouseSetupDetail.BinLocation != null)
                                {
                                    _locBinLocation = _locWarehouseSetupDetail.BinLocation;
                                }
                            }
                            #endregion GetWarehouseLocation

                            TransferIn _locTransferIn = _currSession.FindObject<TransferIn>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("InventoryProduction", _locInventoryProductionXPO),
                                                         new BinaryOperator("DocumentType", _locDocumentType),
                                                         new BinaryOperator("DocNo", _locDocCode)));

                            if (_locTransferIn == null)
                            {
                                TransferIn _saveDataTI = new TransferIn(_currSession)
                                {
                                    TransferType = DirectionType.Internal,
                                    InventoryMovingType = InventoryMovingType.TransferIn,
                                    ObjectList = ObjectList.TransferIn,
                                    DocumentType = _locDocumentType,
                                    LocationFrom = _locInventoryProductionXPO.Location,
                                    DocNo = _locDocCode,
                                    InventoryProduction = _locInventoryProductionXPO,
                                };
                                _saveDataTI.Save();
                                _saveDataTI.Session.CommitTransaction();

                                TransferIn _locTransferIn2 = _currSession.FindObject<TransferIn>
                                                             (new GroupOperator(GroupOperatorType.And,
                                                              new BinaryOperator("DocNo", _locDocCode),
                                                              new BinaryOperator("InventoryProduction", _locInventoryProductionXPO)));

                                if (_locTransferIn2 != null)
                                {
                                    XPCollection<InventoryProductionLine> _locInvProdLines = new XPCollection<InventoryProductionLine>
                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("QcPassed", QCPassed.Good),
                                                                                                new BinaryOperator("InventoryProduction", _locInventoryProductionXPO)));
                                    if (_locInvProdLines != null && _locInvProdLines.Count() > 0)
                                    {
                                        foreach (InventoryProductionLine _locInvProdLine in _locInvProdLines)
                                        {
                                            if (_locInvProdLine.Status != Status.Close)
                                            {
                                                _locTDQTy = _locTDQTy + _locInvProdLine.QtyAvailable;
                                            }
                                        }
                                    }

                                    _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.TransferInLine);

                                    if (_currSignCode != null)
                                    {
                                        TransferInLine _saveDataTIL = new TransferInLine(_currSession)
                                        {
                                            SignCode = _currSignCode,
                                            Select = true,
                                            LocationFrom = _locInventoryProductionXPO.Location,
                                            BinLocationFrom = _locInventoryProductionXPO.BinLocation,
                                            LocationTo = _locLocation,
                                            BinLocationTo = _locBinLocation,
                                            Item = _locInventoryProductionXPO.Item,
                                            DUOM = _locInventoryProductionXPO.DefaultUOM,
                                            DQty = _locTDQTy,
                                            TransferIn = _locTransferIn2,
                                        };
                                        _saveDataTIL.Save();
                                        _saveDataTIL.Session.CommitTransaction();
                                    }

                                    TransferInLine _locTransferInLine = _currSession.FindObject<TransferInLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("TransferIn", _locTransferIn2),
                                                                         new BinaryOperator("SignCode", _currSignCode)));

                                    if (_locTransferInLine != null)
                                    {
                                        BeginingInventory _locBeginingInventory = _currSession.FindObject<BeginingInventory>
                                                                                          (new GroupOperator(GroupOperatorType.And,
                                                                                           new BinaryOperator("Item", _locInventoryProductionXPO.Item)));
                                        if (_locBeginingInventory != null)
                                        {
                                            XPCollection<InventoryProductionLine> _locInventoryProductionLines = new XPCollection<InventoryProductionLine>(_currSession,
                                                                                                             new GroupOperator(GroupOperatorType.And,
                                                                                                             new BinaryOperator("Status", Status.Progress),
                                                                                                             new BinaryOperator("QcPassed", QCPassed.Good),
                                                                                                             new BinaryOperator("InventoryProduction", _locInventoryProductionXPO)));

                                            if (_locInventoryProductionLines != null && _locInventoryProductionLines.Count > 0)
                                            {
                                                foreach (InventoryProductionLine _locInventoryProductionLine in _locInventoryProductionLines)
                                                {
                                                    //Cari begining inventory line yang ada lotnumbernya sama
                                                    BeginingInventoryLine _locBeginingInventoryLine = _currSession.FindObject<BeginingInventoryLine>(
                                                                                                                         new GroupOperator(GroupOperatorType.And,
                                                                                                                         new BinaryOperator("LotNumber", _locInventoryProductionLine.LotNumber),
                                                                                                                         new BinaryOperator("BeginingInventory", _locBeginingInventory)));

                                                    if (_locBeginingInventoryLine != null)
                                                    {
                                                        TransferInLot _saveDataTransferInLot = new TransferInLot(_currSession)
                                                        {
                                                            LocationFrom = _locBeginingInventoryLine.Location,
                                                            BinLocationFrom = _locBeginingInventoryLine.BinLocation,
                                                            LocationTo = _locLocation,
                                                            BinLocationTo = _locBinLocation,
                                                            Item = _locBeginingInventoryLine.Item,
                                                            DUOM = _locBeginingInventoryLine.DefaultUOM,
                                                            LotNumber = _locBeginingInventoryLine.LotNumber,
                                                            DQty = _locBeginingInventoryLine.QtyAvailable,
                                                            BegInvLine = _locBeginingInventoryLine,
                                                            UnitPack = _locBeginingInventoryLine.UnitPack,
                                                            InventoryProductionLine = _locInventoryProductionLine,
                                                            TransferInLine = _locTransferInLine,
                                                        };
                                                        _saveDataTransferInLot.Save();
                                                        _saveDataTransferInLot.Session.CommitTransaction();
                                                    }
                                                }
                                            }
                                        }


                                    }
                                    else
                                    {
                                        ErrorMessageShow("Data TransferInLine Not Available");
                                    }
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryProduction ", ex.ToString());
            }
        }

        private void SetStatusInventoryProductionLine(Session _currSession, InventoryProduction _locInventoryProductionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locInventoryProductionXPO != null)
                {
                    XPCollection<InventoryProductionLine> _locInventoryProductionLines = new XPCollection<InventoryProductionLine>(_currSession,
                                                                                         new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("Status", Status.Progress),
                                                                                         new BinaryOperator("QcPassed", QCPassed.Good),
                                                                                         new BinaryOperator("InventoryProduction", _locInventoryProductionXPO)));

                    if (_locInventoryProductionLines != null && _locInventoryProductionLines.Count > 0)
                    {
                        foreach (InventoryProductionLine _locInventoryProductionLine in _locInventoryProductionLines)
                        {
                            if (_locInventoryProductionLine.Status == Status.Progress)
                            {
                                _locInventoryProductionLine.ActivationPosting = true;
                                _locInventoryProductionLine.Status = Status.Close;
                                _locInventoryProductionLine.StatusDate = now;
                                _locInventoryProductionLine.Save();
                                _locInventoryProductionLine.Session.CommitTransaction();
                            }
                            else
                            {
                                ErrorMessageShow("Data Status InventoryProductionLine Not Available");
                            }
                        }
                    }
                    else
                    {
                        ErrorMessageShow("Data InventoryProductionLine Not Available");
                    }
                }
                else
                {
                    ErrorMessageShow("Data Status InventoryProduction Not Available");
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryProduction " + ex.ToString());
            }
        }

        private void SetPostedCount(Session _currSession, InventoryProduction _locInventoryProductionXPO)
        {
            try
            {
                if (_locInventoryProductionXPO != null)
                {
                    XPCollection<InventoryProductionLine> _locInventoryProductionLines = new XPCollection<InventoryProductionLine>(_currSession,
                                                                                         new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("Status", Status.Close),
                                                                                         new BinaryOperator("QcPassed", QCPassed.Good),
                                                                                         new BinaryOperator("InventoryProduction", _locInventoryProductionXPO)));

                    if (_locInventoryProductionLines != null && _locInventoryProductionLines.Count > 0)
                    {
                        foreach (InventoryProductionLine _locInventoryProductionLine in _locInventoryProductionLines)
                        {
                            if (_locInventoryProductionLine.Status == Status.Close)
                            {
                                _locInventoryProductionLine.PostedCount = _locInventoryProductionLine.PostedCount + 1;
                                _locInventoryProductionLine.Save();
                                _locInventoryProductionLine.Session.CommitTransaction();
                            }
                            else
                            {
                                ErrorMessageShow("Data Status InventoryProductionLine Not Available");
                            }
                        }
                    }
                    else
                    {
                        ErrorMessageShow("Data InventoryProductionLine Not Available");
                    }
                }
                else
                {
                    ErrorMessageShow("Data InventoryProduction Not Available");
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryProduction " + ex.ToString());
            }
        }

        private void SetFinalStatusInventoryProduction(Session _currSession, InventoryProduction _locInventoryProductionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locPostedCountTI = 0;
                int _locInventoryProductionLineCount = 0;

                if (_locInventoryProductionXPO != null)
                {
                    XPCollection<InventoryProductionLine> _locInventoryProductionLines = new XPCollection<InventoryProductionLine>(_currSession,
                                                                                         new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("InventoryProduction", _locInventoryProductionXPO)));

                    if (_locInventoryProductionLines != null && _locInventoryProductionLines.Count() > 0)
                    {
                        _locInventoryProductionLineCount = _locInventoryProductionLines.Count();

                        foreach (InventoryProductionLine _locInventoryProductionLine in _locInventoryProductionLines)
                        {
                            if (_locInventoryProductionLine.Status == Status.Close)
                            {
                                _locPostedCountTI = _locPostedCountTI + 1;
                            }
                        }
                    }

                    if (_locPostedCountTI == _locInventoryProductionLineCount)
                    {
                        _locInventoryProductionXPO.ActivationPosting = true;
                        _locInventoryProductionXPO.Status = Status.Close;
                        _locInventoryProductionXPO.StatusDate = now;
                        _locInventoryProductionXPO.Save();
                        _locInventoryProductionXPO.Session.CommitTransaction();
                    }
                    else
                    {
                        _locInventoryProductionXPO.Status = Status.Posted;
                        _locInventoryProductionXPO.StatusDate = now;
                        _locInventoryProductionXPO.Save();
                        _locInventoryProductionXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryProduction " + ex.ToString());
            }
        }

        #endregion Posting

        #region PostingLot

        private void SetInventoryProductionLotLooping(Session _currSession, InventoryProduction _locInventoryProductionXPO)
        {
            try
            {
                string _locProductionYear = null;
                string _locOrderNumber = null;
                string _locI = null;

                if (_locInventoryProductionXPO != null)
                {
                    if (_locInventoryProductionXPO.PostCountLine < 1)
                    {
                        if (_locInventoryProductionXPO.Count > 0 && _locInventoryProductionXPO.QtyPackage > 0)
                        {
                            InventoryProductionLine _locInventoryProductions = _currSession.FindObject<InventoryProductionLine>
                                                                               (new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("InventoryProduction", _locInventoryProductionXPO)));

                            if (_locInventoryProductions == null)
                            {
                                if (_locInventoryProductionXPO.QtyPackage >= 1)
                                {
                                    if (_locInventoryProductionXPO.FromCount > 0)
                                    {
                                        for (int i = _locInventoryProductionXPO.FromCount; i <= (int)_locInventoryProductionXPO.Count + _locInventoryProductionXPO.FromCount; i++)
                                        {
                                            if (_locInventoryProductionXPO.ProductionYear > 0)
                                            {
                                                _locProductionYear = _locInventoryProductionXPO.ProductionYear.ToString();
                                            }
                                            if (_locInventoryProductionXPO.OrderNumber != null)
                                            {
                                                _locOrderNumber = _locInventoryProductionXPO.OrderNumber.ToString();
                                            }
                                            if (i > 0)
                                            {
                                                _locI = i.ToString();
                                            }
                                            InventoryProductionLine _saveData = new InventoryProductionLine(_currSession)
                                            {
                                                LotNumber = _locProductionYear + "-" + _locOrderNumber + "-" + _locI,
                                                Description = _locInventoryProductionXPO.Description,
                                                DefaultUOM = _locInventoryProductionXPO.DefaultUOM,
                                                DQtyBegining = _locInventoryProductionXPO.QtyPackage,
                                                QtyAvailable = _locInventoryProductionXPO.QtyPackage,
                                                Bruto = _locInventoryProductionXPO.Bruto,
                                                Netto = _locInventoryProductionXPO.Netto,
                                                Tara = _locInventoryProductionXPO.Tara,
                                                Status = Status.Open,
                                                UnitPackage = _locInventoryProductionXPO.UnitPackage,
                                                InventoryProduction = _locInventoryProductionXPO,
                                            };
                                            _saveData.Save();
                                            _saveData.Session.CommitTransaction();
                                            SuccessMessageShow("InventoryProduction has successfully posted");
                                        }
                                    }
                                    else
                                    {
                                        for (int i = 1; i <= (int)_locInventoryProductionXPO.Count; i++)
                                        {
                                            if (_locInventoryProductionXPO.ProductionYear > 0)
                                            {
                                                _locProductionYear = _locInventoryProductionXPO.ProductionYear.ToString();
                                            }
                                            if (_locInventoryProductionXPO.OrderNumber != null)
                                            {
                                                _locOrderNumber = _locInventoryProductionXPO.OrderNumber.ToString();
                                            }
                                            if (i > 0)
                                            {
                                                _locI = i.ToString();
                                            }
                                            InventoryProductionLine _saveData = new InventoryProductionLine(_currSession)
                                            {
                                                LotNumber = _locProductionYear + "-" + _locOrderNumber + "-" + _locI,
                                                Description = _locInventoryProductionXPO.Description,
                                                DefaultUOM = _locInventoryProductionXPO.DefaultUOM,
                                                DQtyBegining = _locInventoryProductionXPO.QtyPackage,
                                                QtyAvailable = _locInventoryProductionXPO.QtyPackage,
                                                Bruto = _locInventoryProductionXPO.Bruto,
                                                Netto = _locInventoryProductionXPO.Netto,
                                                Tara = _locInventoryProductionXPO.Tara,
                                                Status = Status.Open,
                                                UnitPackage = _locInventoryProductionXPO.UnitPackage,
                                                InventoryProduction = _locInventoryProductionXPO,
                                            };
                                            _saveData.Save();
                                            _saveData.Session.CommitTransaction();
                                            SuccessMessageShow("InventoryProduction has successfully posted");
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Data InventoryProduction Not Available");
                        }
                    }
                    else
                    {
                        ErrorMessageShow("Data InventoryProduction has been Sent");
                    }
                }
                else
                {
                    ErrorMessageShow("Data InventoryProduction Not Available");
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryProduction " + ex.ToString());
            }
        }

        private void SetInventoryProductionPostedCount(Session _currSession, InventoryProduction _locInventoryProductionXPO)
        {
            try
            {
                if (_locInventoryProductionXPO != null)
                {
                    if (_locInventoryProductionXPO.PostCountLine < 1)
                    {
                        _locInventoryProductionXPO.PostCountLine = _locInventoryProductionXPO.PostCountLine + 1;
                        _locInventoryProductionXPO.Save();
                        _locInventoryProductionXPO.Session.CommitTransaction();


                        XPCollection<InventoryProductionLine> _locInventoryProductionLines = new XPCollection<InventoryProductionLine>(_currSession,
                                                                                             new GroupOperator(GroupOperatorType.And,
                                                                                             new BinaryOperator("InventoryProduction", _locInventoryProductionXPO)));

                        if (_locInventoryProductionLines != null && _locInventoryProductionLines.Count > 0)
                        {
                            foreach (InventoryProductionLine _locInventoryProductionLine in _locInventoryProductionLines)
                            {
                                if (_locInventoryProductionLine.Status == Status.Progress)
                                {
                                    _locInventoryProductionLine.ProsesCountLot = _locInventoryProductionLine.ProsesCountLot + 1;
                                    _locInventoryProductionLine.Save();
                                    _locInventoryProductionLine.Session.CommitTransaction();
                                }
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Data InventoryProductionLine Not Available");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryProduction " + ex.ToString());
            }
        }

        #endregion PostingLot

        #region PostingWeight

        private void UpdateInventoryProductionWeightLooping(Session _currSession, InventoryProduction _locInventoryProductionXPO)
        {
            DateTime now = DateTime.Now;

            try
            {
                if (_locInventoryProductionXPO != null)
                {
                    InventoryProductionLine _locInventoryProductions = _currSession.FindObject<InventoryProductionLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("InventoryProduction", _locInventoryProductionXPO)));

                    if (_locInventoryProductions != null)
                    {
                        XPCollection<InventoryProductionLine> _locInventoryProductionLines = new XPCollection<InventoryProductionLine>
                                                                                             (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                              new BinaryOperator("Status", Status.Progress),
                                                                                              new BinaryOperator("InventoryProduction", _locInventoryProductionXPO)));

                        if (_locInventoryProductionLines != null && _locInventoryProductionLines.Count > 0)
                        {
                            foreach (InventoryProductionLine _locInventoryProductionLine in _locInventoryProductionLines)
                            {
                                _locInventoryProductionLine.BrutoActual = _locInventoryProductions.BrutoActual;
                                _locInventoryProductionLine.NettoActual = _locInventoryProductions.NettoActual;
                                _locInventoryProductionLine.TaraActual = _locInventoryProductions.TaraActual;
                                _locInventoryProductionLine.StatusDate = now;
                                _locInventoryProductionLine.Save();
                                _locInventoryProductionLine.Session.CommitTransaction();
                            }
                            _locInventoryProductionXPO.Save();
                            _locInventoryProductionXPO.Session.CommitTransaction();
                        }
                    }
                }
                else
                {
                    ErrorMessageShow("Data InventoryProduction Not Available");
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryProduction " + ex.ToString());
            }
        }

        private void SetInventoryProductionPostedCountWeight(Session _currSession, InventoryProduction _locInventoryProductionXPO)
        {
            try
            {
                if (_locInventoryProductionXPO != null)
                {
                    if (_locInventoryProductionXPO.PostCountWeight < 1)
                    {
                        _locInventoryProductionXPO.PostCountWeight = _locInventoryProductionXPO.PostCountWeight + 1;
                        _locInventoryProductionXPO.Save();
                        _locInventoryProductionXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryProduction " + ex.ToString());
            }
        }

        #endregion PostingWeight

        #region GlobalMethod

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion GlobalMethod

    }
}
