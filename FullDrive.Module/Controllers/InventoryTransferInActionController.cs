﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class InventoryTransferInActionController : ViewController
    {
        #region Default

        private ChoiceActionItem _selectionListviewFilter;
        public InventoryTransferInActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            InventoryTransferInListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                InventoryTransferInListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        private void InventoryTransferInProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Status _locStatus = Status.None;
                DateTime _locNow;
                Status _locStatus2 = Status.None;
                DateTime _locNow2;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        InventoryTransferIn _locInventoryTransferInOS = (InventoryTransferIn)_objectSpace.GetObject(obj);

                        if (_locInventoryTransferInOS != null)
                        {
                            if (_locInventoryTransferInOS.Code != null)
                            {
                                _currObjectId = _locInventoryTransferInOS.Code;

                                InventoryTransferIn _locInventoryTransferInXPO = _currSession.FindObject<InventoryTransferIn>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locInventoryTransferInXPO != null)
                                {
                                    if (_locInventoryTransferInXPO.Status == Status.Open || _locInventoryTransferInXPO.Status == Status.Progress || _locInventoryTransferInXPO.Status == Status.Posted)
                                    {
                                        if (_locInventoryTransferInXPO.Status == Status.Open)
                                        {
                                            _locStatus = Status.Progress;
                                            _locNow = now;
                                        }
                                        else
                                        {
                                            _locStatus = _locInventoryTransferInXPO.Status;
                                            _locNow = _locInventoryTransferInXPO.StatusDate;
                                        }

                                        _locInventoryTransferInXPO.Status = _locStatus;
                                        _locInventoryTransferInXPO.StatusDate = _locNow;
                                        _locInventoryTransferInXPO.Save();
                                        _locInventoryTransferInXPO.Session.CommitTransaction();

                                        XPCollection<InventoryTransferInLine> _locInventoryTransferInLines = new XPCollection<InventoryTransferInLine>
                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO)));

                                        if (_locInventoryTransferInLines != null && _locInventoryTransferInLines.Count > 0)
                                        {
                                            foreach (InventoryTransferInLine _locInventoryTransferInLine in _locInventoryTransferInLines)
                                            {
                                                if (_locInventoryTransferInLine.Status == Status.Open || _locInventoryTransferInLine.Status == Status.Progress || _locInventoryTransferInLine.Status == Status.Posted)
                                                {
                                                    if (_locInventoryTransferInLine.Status == Status.Open)
                                                    {
                                                        _locStatus2 = Status.Progress;
                                                        _locNow = now;
                                                    }
                                                    else
                                                    {
                                                        _locStatus2 = _locInventoryTransferInLine.Status;
                                                        _locNow2 = _locInventoryTransferInLine.StatusDate;
                                                    }

                                                    _locInventoryTransferInLine.Status = _locStatus2;
                                                    _locInventoryTransferInLine.StatusDate = _locNow;
                                                    _locInventoryTransferInLine.Save();
                                                    _locInventoryTransferInLine.Session.CommitTransaction();

                                                    XPCollection<InventoryTransferInLot> _locInventoryTransferInLots = new XPCollection<InventoryTransferInLot>
                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("InventoryTransferInLine", _locInventoryTransferInLine),
                                                                                                        new BinaryOperator("Status", Status.Open)));

                                                    if (_locInventoryTransferInLots != null && _locInventoryTransferInLots.Count() > 0)
                                                    {
                                                        foreach (InventoryTransferInLot _locInventoryTransferInLot in _locInventoryTransferInLots)
                                                        {
                                                            _locInventoryTransferInLot.Status = Status.Progress;
                                                            _locInventoryTransferInLot.StatusDate = now;
                                                            _locInventoryTransferInLot.Save();
                                                            _locInventoryTransferInLot.Session.CommitTransaction();
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        SuccessMessageShow(_locInventoryTransferInXPO.Code + " has been change successfully to Progress");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Inventory Transfer Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data Inventory Transfer Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferIn " + ex.ToString());
            }
        }

        private void InventoryTransferInPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        InventoryTransferIn _locInventoryTransferInOS = (InventoryTransferIn)_objectSpace.GetObject(obj);

                        if (_locInventoryTransferInOS != null)
                        {
                            if (_locInventoryTransferInOS.Status == Status.Progress || _locInventoryTransferInOS.Status == Status.Posted)
                            {
                                if (_locInventoryTransferInOS.Code != null)
                                {
                                    _currObjectId = _locInventoryTransferInOS.Code;

                                    InventoryTransferIn _locInventoryTransferInXPO = _currSession.FindObject<InventoryTransferIn>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId),
                                                                         new BinaryOperator("InventoryMovingType", InventoryMovingType.Receive)));

                                    if (_locInventoryTransferInXPO != null)
                                    {
                                        if(_locInventoryTransferInXPO.StockControlling == true)
                                        {
                                            SetReceiveBeginingInventorySC(_currSession, _locInventoryTransferInXPO);
                                            SetReceiveInventoryJournalSC(_currSession, _locInventoryTransferInXPO);
                                            SetRemainReceivedQtySC(_currSession, _locInventoryTransferInXPO);
                                            SetPostingReceivedQtySC(_currSession, _locInventoryTransferInXPO);
                                            SetProcessCountReceiveSC(_currSession, _locInventoryTransferInXPO);
                                            SetStatusReceiveInventoryTransferInLineSC(_currSession, _locInventoryTransferInXPO);
                                            SetNormalQuantityReceiveSC(_currSession, _locInventoryTransferInXPO);
                                            SetFinalStatusReceiveInventoryTransferInSC(_currSession, _locInventoryTransferInXPO);
                                        }
                                        else
                                        {
                                            SetReceiveBeginingInventory(_currSession, _locInventoryTransferInXPO);
                                            SetReceiveInventoryJournal(_currSession, _locInventoryTransferInXPO);
                                            SetRemainReceivedQty(_currSession, _locInventoryTransferInXPO);
                                            SetPostingReceivedQty(_currSession, _locInventoryTransferInXPO);
                                            SetProcessCountReceive(_currSession, _locInventoryTransferInXPO);

                                            if (_locInventoryTransferInXPO.TransferType == DirectionType.External && _locInventoryTransferInXPO.PurchaseOrder != null)
                                            {
                                                if(GetAvailableGeneralJournal(_currSession, _locInventoryTransferInXPO) == false)
                                                {
                                                    SetGoodsReceiptJournal(_currSession, _locInventoryTransferInXPO);
                                                }

                                                SetPurchaseInvoice(_currSession, _locInventoryTransferInXPO);

                                            }

                                            SetStatusReceiveInventoryTransferInLine(_currSession, _locInventoryTransferInXPO);
                                            SetNormalQuantityReceive(_currSession, _locInventoryTransferInXPO);
                                            SetFinalStatusReceiveInventoryTransferIn(_currSession, _locInventoryTransferInXPO);
                                        }
                                        SuccessMessageShow(_locInventoryTransferInXPO.Code + " has been change successfully to Receive");
                                    }
                                    else
                                    {
                                        ErrorMessageShow("Inventory Transfer Data Not Available");
                                    }
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Inventory Transfer Data Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferIn " + ex.ToString());
            }
        }

        private void InventoryTransferInListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(InventoryTransferIn)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferIn " + ex.ToString());
            }
        }

        //==== Code Only ====

        #region Receive

        #region InventoryReceive

        //Menambahkan Qty Available ke Begining Inventory
        private void SetReceiveBeginingInventory(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO)
        {
            try
            {
                XPCollection<InventoryTransferInLine> _locInvTransInLines = new XPCollection<InventoryTransferInLine>
                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO),
                                                                    new BinaryOperator("Select", true)));

                if (_locInvTransInLines != null && _locInvTransInLines.Count() > 0)
                {
                    GlobalFunction _globFunc = new GlobalFunction();
                    double _locInvLineTotal = 0;
                    XPCollection<BeginingInventoryLine> _locBegInventoryLines = null;
                    ItemUnitOfMeasure _locItemUOM = null;
                    string _fullString = null;
                    string _locItemParse = null;
                    string _locLocationParse = null;
                    string _locBinLocationParse = null;
                    string _locDUOMParse = null;
                    string _locStockTypeParse = null;
                    string _locProjectHeader = null;
                    string _locActiveParse = null;
                    string _locSignCode = null;

                    foreach (InventoryTransferInLine _locInvTransInLine in _locInvTransInLines)
                    {
                        if (_locInvTransInLine.Status == Status.Progress || _locInvTransInLine.Status == Status.Posted)
                        {
                            if (_locInvTransInLine.DQty > 0 || _locInvTransInLine.Qty > 0)
                            {
                                XPCollection<InventoryTransferInLot> _locInventoryTransferInLots = new XPCollection<InventoryTransferInLot>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("InventoryTransferInLine", _locInvTransInLine)));
                                #region LotNumber
                                if (_locInventoryTransferInLots != null && _locInventoryTransferInLots.Count() > 0)
                                {
                                    foreach (InventoryTransferInLot _locInventoryTransferInLot in _locInventoryTransferInLots)
                                    {
                                        if(_locInventoryTransferInLot.Select == true)
                                        {
                                            if (_locInventoryTransferInLot.Item != null && _locInventoryTransferInLot.UOM != null && _locInventoryTransferInLot.DUOM != null)
                                            {
                                                _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Item", _locInventoryTransferInLot.Item),
                                                                     new BinaryOperator("UOM", _locInventoryTransferInLot.UOM),
                                                                     new BinaryOperator("DefaultUOM", _locInventoryTransferInLot.DUOM),
                                                                     new BinaryOperator("Active", true)));
                                            }


                                            BeginingInventory _locBeginingInventory = _currSession.FindObject<BeginingInventory>(new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Item", _locInventoryTransferInLot.Item)));

                                            #region UpdateBeginingInventory
                                            if (_locBeginingInventory != null)
                                            {
                                                #region Code
                                                if (_locInventoryTransferInLot.Item != null) { _locItemParse = "[Item.Code]=='" + _locInventoryTransferInLot.Item.Code + "'"; }
                                                else { _locItemParse = ""; }

                                                if (_locInventoryTransferInLot.Location != null && (_locInventoryTransferInLot.Item != null))
                                                { _locLocationParse = " AND [Location.Code]=='" + _locInventoryTransferInLot.Location.Code + "'"; }
                                                else if (_locInventoryTransferInLot.Location != null && _locInventoryTransferInLot.Item == null)
                                                { _locLocationParse = " [Location.Code]=='" + _locInventoryTransferInLot.Location.Code + "'"; }
                                                else { _locLocationParse = ""; }

                                                if (_locInventoryTransferInLot.BinLocation != null && (_locInventoryTransferInLot.Item != null || _locInventoryTransferInLot.Location != null))
                                                { _locBinLocationParse = " AND [BinLocation.Code]=='" + _locInventoryTransferInLot.BinLocation.Code + "'"; }
                                                else if (_locInventoryTransferInLot.BinLocation != null && _locInventoryTransferInLot.Item == null && _locInventoryTransferInLot.Location == null)
                                                { _locBinLocationParse = " [BinLocation.Code]=='" + _locInventoryTransferInLot.BinLocation.Code + "'"; }
                                                else { _locBinLocationParse = ""; }

                                                if (_locInventoryTransferInLot.DUOM != null && (_locInventoryTransferInLot.Item != null || _locInventoryTransferInLot.Location != null || _locInventoryTransferInLot.BinLocation != null))
                                                { _locDUOMParse = " AND [DefaultUOM.Code]=='" + _locInventoryTransferInLot.DUOM.Code + "'"; }
                                                else if (_locInventoryTransferInLot.DUOM != null && _locInventoryTransferInLot.Item == null && _locInventoryTransferInLot.Location == null && _locInventoryTransferInLot.BinLocation == null)
                                                { _locDUOMParse = " [DefaultUOM.Code]=='" + _locInventoryTransferInLot.DUOM.Code + "'"; }
                                                else { _locDUOMParse = ""; }

                                                if (_locInventoryTransferInLot.StockType != StockType.None && (_locInventoryTransferInLot.Item != null || _locInventoryTransferInLot.Location != null || _locInventoryTransferInLot.BinLocation != null
                                                    || _locInventoryTransferInLot.DUOM != null))
                                                { _locStockTypeParse = " AND [StockType]=='" + GetStockType(_locInventoryTransferInLot.StockType).ToString() + "'"; }
                                                else if (_locInventoryTransferInLot.StockType != StockType.None && _locInventoryTransferInLot.Item == null && _locInventoryTransferInLot.Location == null && _locInventoryTransferInLot.BinLocation == null
                                                    && _locInventoryTransferInLot.DUOM == null)
                                                { _locStockTypeParse = " [StockType]=='" + GetStockType(_locInventoryTransferInLot.StockType).ToString() + "'"; }
                                                else { _locStockTypeParse = ""; }

                                                if (_locInventoryTransferInLot.Item == null && _locInventoryTransferInLot.Location == null && _locInventoryTransferInLot.BinLocation == null
                                                    && _locInventoryTransferInLot.DUOM == null && _locInventoryTransferInLot.StockType != StockType.None)
                                                { _locActiveParse = " [Active]=='" + GetActive(true).ToString() + "'"; }
                                                else { _locActiveParse = " AND [Active]=='" + GetActive(true).ToString() + "'"; }

                                                if (_locInventoryTransferInXPO.ProjectHeader != null)
                                                { _locProjectHeader = " AND [ProjectHeader.Code]=='" + _locInventoryTransferInXPO.ProjectHeader.Code + "'"; }
                                                else
                                                { _locProjectHeader = ""; }

                                                if (_locItemParse != null || _locLocationParse != null || _locBinLocationParse != null || _locDUOMParse != null || _locStockTypeParse != null || _locProjectHeader != null)
                                                {
                                                    _fullString = _locItemParse + _locLocationParse + _locBinLocationParse + _locDUOMParse + _locStockTypeParse + _locActiveParse + _locProjectHeader;
                                                }
                                                else
                                                {
                                                    _fullString = _locActiveParse;
                                                }

                                                _locBegInventoryLines = new XPCollection<BeginingInventoryLine>(_currSession, CriteriaOperator.Parse(_fullString));

                                                #endregion Code

                                                #region NewBeginingInventoryLine
                                                if (_locBegInventoryLines == null && _locBegInventoryLines.Count() == 0)
                                                {
                                                    if (_locItemUOM != null)
                                                    {
                                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                        {
                                                            _locInvLineTotal = _locInventoryTransferInLot.Qty * _locItemUOM.DefaultConversion + _locInventoryTransferInLot.DQty;
                                                        }
                                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                        {
                                                            _locInvLineTotal = _locInventoryTransferInLot.Qty / _locItemUOM.Conversion + _locInventoryTransferInLot.DQty;
                                                        }
                                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                        {
                                                            _locInvLineTotal = _locInventoryTransferInLot.Qty + _locInventoryTransferInLot.DQty;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        _locInvLineTotal = _locInventoryTransferInLot.Qty + _locInventoryTransferInLot.DQty;
                                                    }

                                                    BeginingInventoryLine _locSaveDataBeginingInventory = new BeginingInventoryLine(_currSession)
                                                    {
                                                        Item = _locInventoryTransferInLot.Item,
                                                        Location = _locInventoryTransferInLot.Location,
                                                        BinLocation = _locInventoryTransferInLot.BinLocation,
                                                        QtyAvailable = _locInvLineTotal,
                                                        DefaultUOM = _locInventoryTransferInLot.DUOM,
                                                        StockType = _locInventoryTransferInLot.StockType,
                                                        LotNumber = _locInventoryTransferInLot.LotNumber,
                                                        ProjectHeader = _locInventoryTransferInXPO.ProjectHeader,
                                                        Active = true,
                                                        BeginingInventory = _locBeginingInventory,
                                                    };
                                                    _locSaveDataBeginingInventory.Save();
                                                    _locSaveDataBeginingInventory.Session.CommitTransaction();

                                                    _locBeginingInventory.QtyAvailable = _locBeginingInventory.QtyAvailable + _locInvLineTotal;
                                                    _locBeginingInventory.Save();
                                                    _locBeginingInventory.Session.CommitTransaction();
                                                }
                                                #endregion NewBeginingInventoryLine
                                            }
                                            #endregion UpdateBeginingInventory

                                            #region CreateNewBeginingInventory
                                            else
                                            {
                                                _locSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.BeginingInventory);

                                                if (_locSignCode != null)
                                                {
                                                    if (_locItemUOM != null)
                                                    {
                                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                        {
                                                            _locInvLineTotal = _locInventoryTransferInLot.Qty * _locItemUOM.DefaultConversion + _locInventoryTransferInLot.DQty;
                                                        }
                                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                        {
                                                            _locInvLineTotal = _locInventoryTransferInLot.Qty / _locItemUOM.Conversion + _locInventoryTransferInLot.DQty;
                                                        }
                                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                        {
                                                            _locInvLineTotal = _locInventoryTransferInLot.Qty + _locInventoryTransferInLot.DQty;
                                                        }

                                                    }
                                                    else
                                                    {
                                                        _locInvLineTotal = _locInventoryTransferInLot.Qty + _locInventoryTransferInLot.DQty;
                                                    }

                                                    BeginingInventory _saveDataBegInv = new BeginingInventory(_currSession)
                                                    {
                                                        Item = _locInventoryTransferInLot.Item,
                                                        QtyAvailable = _locInvLineTotal,
                                                        DefaultUOM = _locInventoryTransferInLot.DUOM,
                                                        Active = true,
                                                        SignCode = _locSignCode,
                                                        Company = _locInventoryTransferInXPO.Company,
                                                    };
                                                    _saveDataBegInv.Save();
                                                    _saveDataBegInv.Session.CommitTransaction();

                                                    BeginingInventory _locBeginingInventory2 = _currSession.FindObject<BeginingInventory>(new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Item", _locInventoryTransferInLot.Item),
                                                                                new BinaryOperator("SignCode", _locSignCode)));

                                                    if (_locBeginingInventory2 != null)
                                                    {
                                                        BeginingInventoryLine _saveDataBegInvLine = new BeginingInventoryLine(_currSession)
                                                        {
                                                            Item = _locInventoryTransferInLot.Item,
                                                            Location = _locInventoryTransferInLot.Location,
                                                            BinLocation = _locInventoryTransferInLot.BinLocation,
                                                            QtyAvailable = _locBeginingInventory2.QtyAvailable,
                                                            DefaultUOM = _locBeginingInventory2.DefaultUOM,
                                                            StockType = _locInventoryTransferInLot.StockType,
                                                            LotNumber = _locInventoryTransferInLot.LotNumber,
                                                            ProjectHeader = _locInventoryTransferInXPO.ProjectHeader,
                                                            Active = true,
                                                            BeginingInventory = _locBeginingInventory2,
                                                            Company = _locBeginingInventory2.Company,
                                                        };
                                                        _saveDataBegInvLine.Save();
                                                        _saveDataBegInvLine.Session.CommitTransaction();
                                                    }
                                                }
                                            }
                                            #endregion CreateNewBeginingInventory
                                        }


                                    }
                                }
                                #endregion LotNumber
                                #region NonLotNumber
                                else
                                {
                                    if (_locInvTransInLine.Item != null && _locInvTransInLine.UOM != null && _locInvTransInLine.DUOM != null)
                                    {
                                        _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                             new BinaryOperator("Item", _locInvTransInLine.Item),
                                                             new BinaryOperator("UOM", _locInvTransInLine.UOM),
                                                             new BinaryOperator("DefaultUOM", _locInvTransInLine.DUOM),
                                                             new BinaryOperator("Active", true)));
                                    }


                                    BeginingInventory _locBeginingInventory = _currSession.FindObject<BeginingInventory>(new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Item", _locInvTransInLine.Item)));

                                    #region UpdateBeginingInventory
                                    if (_locBeginingInventory != null)
                                    {
                                        #region Code
                                        if (_locInvTransInLine.Item != null) { _locItemParse = "[Item.Code]=='" + _locInvTransInLine.Item.Code + "'"; }
                                        else { _locItemParse = ""; }

                                        if (_locInvTransInLine.Location != null && (_locInvTransInLine.Item != null))
                                        { _locLocationParse = " AND [Location.Code]=='" + _locInvTransInLine.Location.Code + "'"; }
                                        else if (_locInvTransInLine.Location != null && _locInvTransInLine.Item == null)
                                        { _locLocationParse = " [Location.Code]=='" + _locInvTransInLine.Location.Code + "'"; }
                                        else { _locLocationParse = ""; }

                                        if (_locInvTransInLine.BinLocation != null && (_locInvTransInLine.Item != null || _locInvTransInLine.Location != null))
                                        { _locBinLocationParse = " AND [BinLocation.Code]=='" + _locInvTransInLine.BinLocation.Code + "'"; }
                                        else if (_locInvTransInLine.BinLocation != null && _locInvTransInLine.Item == null && _locInvTransInLine.Location == null)
                                        { _locBinLocationParse = " [BinLocation.Code]=='" + _locInvTransInLine.BinLocation.Code + "'"; }
                                        else { _locBinLocationParse = ""; }

                                        if (_locInvTransInLine.DUOM != null && (_locInvTransInLine.Item != null || _locInvTransInLine.Location != null || _locInvTransInLine.BinLocation != null))
                                        { _locDUOMParse = " AND [DefaultUOM.Code]=='" + _locInvTransInLine.DUOM.Code + "'"; }
                                        else if (_locInvTransInLine.DUOM != null && _locInvTransInLine.Item == null && _locInvTransInLine.Location == null && _locInvTransInLine.BinLocation == null)
                                        { _locDUOMParse = " [DefaultUOM.Code]=='" + _locInvTransInLine.DUOM.Code + "'"; }
                                        else { _locDUOMParse = ""; }

                                        if (_locInvTransInLine.StockType != StockType.None && (_locInvTransInLine.Item != null || _locInvTransInLine.Location != null || _locInvTransInLine.BinLocation != null
                                            || _locInvTransInLine.DUOM != null))
                                        { _locStockTypeParse = " AND [StockType]=='" + GetStockType(_locInvTransInLine.StockType).ToString() + "'"; }
                                        else if (_locInvTransInLine.StockType != StockType.None && _locInvTransInLine.Item == null && _locInvTransInLine.Location == null && _locInvTransInLine.BinLocation == null
                                            && _locInvTransInLine.DUOM == null)
                                        { _locStockTypeParse = " [StockType]=='" + GetStockType(_locInvTransInLine.StockType).ToString() + "'"; }
                                        else { _locStockTypeParse = ""; }

                                        if (_locInvTransInLine.Item == null && _locInvTransInLine.Location == null && _locInvTransInLine.BinLocation == null
                                            && _locInvTransInLine.DUOM == null && _locInvTransInLine.StockType != StockType.None)
                                        { _locActiveParse = " [Active]=='" + GetActive(true).ToString() + "'"; }
                                        else { _locActiveParse = " AND [Active]=='" + GetActive(true).ToString() + "'"; }

                                        if (_locInventoryTransferInXPO.ProjectHeader != null)
                                        { _locProjectHeader = " AND [ProjectHeader.Code]=='" + _locInventoryTransferInXPO.ProjectHeader.Code + "'"; }
                                        else
                                        { _locProjectHeader = ""; }

                                        if (_locItemParse != null || _locLocationParse != null || _locBinLocationParse != null || _locDUOMParse != null || _locStockTypeParse != null || _locProjectHeader != null)
                                        {
                                            _fullString = _locItemParse + _locLocationParse + _locBinLocationParse + _locDUOMParse + _locStockTypeParse + _locActiveParse + _locProjectHeader;
                                        }
                                        else
                                        {
                                            _fullString = _locActiveParse;
                                        }

                                        _locBegInventoryLines = new XPCollection<BeginingInventoryLine>(_currSession, CriteriaOperator.Parse(_fullString));

                                        #endregion Code

                                        #region UpdateBeginingInventoryLine
                                        if (_locBegInventoryLines != null && _locBegInventoryLines.Count() > 0)
                                        {

                                            foreach (BeginingInventoryLine _locBegInventoryLine in _locBegInventoryLines)
                                            {
                                                if (_locItemUOM != null)
                                                {
                                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                    {
                                                        _locInvLineTotal = _locInvTransInLine.Qty * _locItemUOM.DefaultConversion + _locInvTransInLine.DQty;
                                                    }
                                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                    {
                                                        _locInvLineTotal = _locInvTransInLine.Qty / _locItemUOM.Conversion + _locInvTransInLine.DQty;
                                                    }
                                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                    {
                                                        _locInvLineTotal = _locInvTransInLine.Qty + _locInvTransInLine.DQty;
                                                    }

                                                }
                                                else
                                                {
                                                    _locInvLineTotal = _locInvTransInLine.Qty + _locInvTransInLine.DQty;
                                                }
                                                _locBegInventoryLine.QtyAvailable = _locBegInventoryLine.QtyAvailable + _locInvLineTotal;
                                                _locBegInventoryLine.Save();
                                                _locBegInventoryLine.Session.CommitTransaction();

                                                _locBeginingInventory.QtyAvailable = _locBeginingInventory.QtyAvailable + _locInvLineTotal;
                                                _locBeginingInventory.Save();
                                                _locBeginingInventory.Session.CommitTransaction();

                                            }
                                        }
                                        #endregion UpdateBeginingInventoryLine

                                        #region NewBeginingInventoryLine
                                        else
                                        {
                                            if (_locItemUOM != null)
                                            {
                                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                {
                                                    _locInvLineTotal = _locInvTransInLine.Qty * _locItemUOM.DefaultConversion + _locInvTransInLine.DQty;
                                                }
                                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                {
                                                    _locInvLineTotal = _locInvTransInLine.Qty / _locItemUOM.Conversion + _locInvTransInLine.DQty;
                                                }
                                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                {
                                                    _locInvLineTotal = _locInvTransInLine.Qty + _locInvTransInLine.DQty;
                                                }
                                            }
                                            else
                                            {
                                                _locInvLineTotal = _locInvTransInLine.Qty + _locInvTransInLine.DQty;
                                            }

                                            BeginingInventoryLine _locSaveDataBeginingInventory = new BeginingInventoryLine(_currSession)
                                            {
                                                Item = _locInvTransInLine.Item,
                                                Location = _locInvTransInLine.Location,
                                                BinLocation = _locInvTransInLine.BinLocation,
                                                QtyAvailable = _locInvLineTotal,
                                                DefaultUOM = _locInvTransInLine.DUOM,
                                                StockType = _locInvTransInLine.StockType,
                                                Active = true,
                                                BeginingInventory = _locBeginingInventory,
                                            };
                                            _locSaveDataBeginingInventory.Save();
                                            _locSaveDataBeginingInventory.Session.CommitTransaction();

                                            _locBeginingInventory.QtyAvailable = _locBeginingInventory.QtyAvailable + _locInvLineTotal;
                                            _locBeginingInventory.Save();
                                            _locBeginingInventory.Session.CommitTransaction();
                                        }
                                        #endregion NewBeginingInventoryLine
                                    }
                                    #endregion UpdateBeginingInventory

                                    #region CreateNewBeginingInventory
                                    else
                                    {
                                        _locSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.BeginingInventory);

                                        if (_locSignCode != null)
                                        {
                                            if (_locItemUOM != null)
                                            {
                                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                {
                                                    _locInvLineTotal = _locInvTransInLine.Qty * _locItemUOM.DefaultConversion + _locInvTransInLine.DQty;
                                                }
                                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                {
                                                    _locInvLineTotal = _locInvTransInLine.Qty / _locItemUOM.Conversion + _locInvTransInLine.DQty;
                                                }
                                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                {
                                                    _locInvLineTotal = _locInvTransInLine.Qty + _locInvTransInLine.DQty;
                                                }

                                            }
                                            else
                                            {
                                                _locInvLineTotal = _locInvTransInLine.Qty + _locInvTransInLine.DQty;
                                            }

                                            BeginingInventory _saveDataBegInv = new BeginingInventory(_currSession)
                                            {
                                                Item = _locInvTransInLine.Item,
                                                QtyAvailable = _locInvLineTotal,
                                                DefaultUOM = _locInvTransInLine.DUOM,
                                                Active = true,
                                                SignCode = _locSignCode,
                                                Company = _locInventoryTransferInXPO.Company,
                                            };
                                            _saveDataBegInv.Save();
                                            _saveDataBegInv.Session.CommitTransaction();

                                            BeginingInventory _locBeginingInventory2 = _currSession.FindObject<BeginingInventory>(new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Item", _locInvTransInLine.Item),
                                                                        new BinaryOperator("SignCode", _locSignCode)));

                                            if (_locBeginingInventory2 != null)
                                            {
                                                BeginingInventoryLine _saveDataBegInvLine = new BeginingInventoryLine(_currSession)
                                                {
                                                    Item = _locInvTransInLine.Item,
                                                    Location = _locInvTransInLine.Location,
                                                    BinLocation = _locInvTransInLine.BinLocation,
                                                    QtyAvailable = _locBeginingInventory2.QtyAvailable,
                                                    DefaultUOM = _locBeginingInventory2.DefaultUOM,
                                                    StockType = _locInvTransInLine.StockType,
                                                    ProjectHeader = _locInventoryTransferInXPO.ProjectHeader,
                                                    Active = true,
                                                    BeginingInventory = _locBeginingInventory2,
                                                    Company = _locBeginingInventory2.Company,
                                                };
                                                _saveDataBegInvLine.Save();
                                                _saveDataBegInvLine.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                    #endregion CreateNewBeginingInventory
                                }
                                #endregion NonLotNumber

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferIn ", ex.ToString());
            }
        }

        //Membuat jurnal positif di inventory journal
        private void SetReceiveInventoryJournal(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO)
        {
            try
            {
                XPCollection<InventoryTransferInLine> _locInvTransInLines = new XPCollection<InventoryTransferInLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO),
                                                                        new BinaryOperator("Select", true)));

                if (_locInvTransInLines != null && _locInvTransInLines.Count > 0)
                {
                    double _locInvLineTotal = 0;
                    DateTime now = DateTime.Now;

                    foreach (InventoryTransferInLine _locInvTransInLine in _locInvTransInLines)
                    {
                        if (_locInvTransInLine.Status == Status.Progress || _locInvTransInLine.Status == Status.Posted)
                        {
                            if (_locInvTransInLine.DQty > 0 || _locInvTransInLine.Qty > 0)
                            {
                                XPCollection<InventoryTransferInLot> _locInventoryTransferInLots = new XPCollection<InventoryTransferInLot>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("InventoryTransferInLine", _locInvTransInLine)));
                                #region LotNumber
                                if(_locInventoryTransferInLots != null && _locInventoryTransferInLots.Count() > 0)
                                {
                                    foreach(InventoryTransferInLot _locInventoryTransferInLot in _locInventoryTransferInLots)
                                    {
                                        if(_locInventoryTransferInLot.Select == true)
                                        {
                                            BeginingInventoryLine _locBegInvLine = _currSession.FindObject<BeginingInventoryLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Location", _locInventoryTransferInLot.Location),
                                                                                new BinaryOperator("BinLocation", _locInventoryTransferInLot.BinLocation),
                                                                                new BinaryOperator("Item", _locInventoryTransferInLot.Item),
                                                                                new BinaryOperator("LotNumber", _locInventoryTransferInLot.LotNumber)));
                                            if (_locBegInvLine != null)
                                            {
                                                InventoryJournal _locPositifInventoryJournal = new InventoryJournal(_currSession)
                                                {
                                                    DocumentType = _locInventoryTransferInXPO.DocumentType,
                                                    DocNo = _locInventoryTransferInXPO.DocNo,
                                                    Location = _locInventoryTransferInLot.Location,
                                                    BinLocation = _locInventoryTransferInLot.BinLocation,
                                                    StockType = _locInventoryTransferInLot.StockType,
                                                    Item = _locInventoryTransferInLot.Item,
                                                    QtyNeg = 0,
                                                    QtyPos = _locInventoryTransferInLot.TQty,
                                                    DUOM = _locInventoryTransferInLot.DUOM,
                                                    JournalDate = now,
                                                    LotNumber = _locInventoryTransferInLot.LotNumber,
                                                    ProjectHeader = _locInventoryTransferInXPO.ProjectHeader,
                                                    InventoryTransferIn = _locInventoryTransferInXPO,
                                                    Company = _locInventoryTransferInXPO.Company,
                                                };
                                                _locPositifInventoryJournal.Save();
                                                _locPositifInventoryJournal.Session.CommitTransaction();
                                            }
                                        } 
                                    }
                                }
                                #endregion LotNumber
                                #region NonLotNumber
                                else
                                {
                                    if (_locInvTransInLine.Item != null && _locInvTransInLine.UOM != null && _locInvTransInLine.DUOM != null)
                                    {
                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                             new BinaryOperator("Item", _locInvTransInLine.Item),
                                                             new BinaryOperator("UOM", _locInvTransInLine.UOM),
                                                             new BinaryOperator("DefaultUOM", _locInvTransInLine.DUOM),
                                                             new BinaryOperator("Active", true)));
                                        if (_locItemUOM != null)
                                        {

                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                            {
                                                _locInvLineTotal = _locInvTransInLine.Qty * _locItemUOM.DefaultConversion + _locInvTransInLine.DQty;
                                            }
                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                            {
                                                _locInvLineTotal = _locInvTransInLine.Qty / _locItemUOM.Conversion + _locInvTransInLine.DQty;
                                            }
                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                            {
                                                _locInvLineTotal = _locInvTransInLine.Qty + _locInvTransInLine.DQty;
                                            }

                                            InventoryJournal _locPositifInventoryJournal = new InventoryJournal(_currSession)
                                            {
                                                DocumentType = _locInventoryTransferInXPO.DocumentType,
                                                DocNo = _locInventoryTransferInXPO.DocNo,
                                                Location = _locInvTransInLine.Location,
                                                BinLocation = _locInvTransInLine.BinLocation,
                                                StockType = _locInvTransInLine.StockType,
                                                Item = _locInvTransInLine.Item,
                                                QtyNeg = 0,
                                                QtyPos = _locInvLineTotal,
                                                DUOM = _locInvTransInLine.DUOM,
                                                JournalDate = now,
                                                ProjectHeader = _locInventoryTransferInXPO.ProjectHeader,
                                                InventoryTransferIn = _locInventoryTransferInXPO,
                                                Company = _locInventoryTransferInXPO.Company,
                                            };
                                            _locPositifInventoryJournal.Save();
                                            _locPositifInventoryJournal.Session.CommitTransaction();

                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locInvTransInLine.Qty + _locInvTransInLine.DQty;

                                        InventoryJournal _locPositifInventoryJournal = new InventoryJournal(_currSession)
                                        {

                                            DocumentType = _locInventoryTransferInXPO.DocumentType,
                                            DocNo = _locInventoryTransferInXPO.DocNo,
                                            Location = _locInvTransInLine.Location,
                                            BinLocation = _locInvTransInLine.BinLocation,
                                            StockType = _locInvTransInLine.StockType,
                                            Item = _locInvTransInLine.Item,
                                            QtyNeg = 0,
                                            QtyPos = _locInvLineTotal,
                                            DUOM = _locInvTransInLine.DUOM,
                                            JournalDate = now,
                                            ProjectHeader = _locInventoryTransferInXPO.ProjectHeader,
                                            InventoryTransferIn = _locInventoryTransferInXPO,
                                            Company = _locInventoryTransferInXPO.Company,
                                        };
                                        _locPositifInventoryJournal.Save();
                                        _locPositifInventoryJournal.Session.CommitTransaction();
                                    }
                                }
                                #endregion NonLotNumber
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferIn ", ex.ToString());
            }
        }

        //Menentukan sisa dari transaksi receive -> Done 10 Dec 2019
        private void SetRemainReceivedQty(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO)
        {
            try
            {
                if (_locInventoryTransferInXPO != null)
                {
                    XPCollection<InventoryTransferInLine> _locInventoryTransferInLines = new XPCollection<InventoryTransferInLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locInventoryTransferInLines != null && _locInventoryTransferInLines.Count > 0)
                    {
                        double _locRmDQty = 0;
                        double _locRmQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (InventoryTransferInLine _locInventoryTransferInLine in _locInventoryTransferInLines)
                        {
                            #region ProcessCount=0
                            if (_locInventoryTransferInLine.ProcessCount == 0)
                            {
                                #region MaxQuantity
                                if (_locInventoryTransferInLine.MxDQty > 0 || _locInventoryTransferInLine.MxQty > 0)
                                {
                                    if (_locInventoryTransferInLine.DQty > 0 && _locInventoryTransferInLine.DQty <= _locInventoryTransferInLine.MxDQty)
                                    {
                                        _locRmDQty = _locInventoryTransferInLine.MxDQty - _locInventoryTransferInLine.DQty;
                                    }

                                    if (_locInventoryTransferInLine.Qty > 0 || _locInventoryTransferInLine.Qty <= _locInventoryTransferInLine.MxQty)
                                    {
                                        _locRmQty = _locInventoryTransferInLine.MxQty - _locInventoryTransferInLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locInventoryTransferInLine.Item),
                                                                new BinaryOperator("UOM", _locInventoryTransferInLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locInventoryTransferInLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locInventoryTransferInLine.DQty > 0)
                                    {
                                        _locRmDQty = 0;
                                    }

                                    if (_locInventoryTransferInLine.Qty > 0)
                                    {
                                        _locRmQty = 0;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locInventoryTransferInLine.Item),
                                                                new BinaryOperator("UOM", _locInventoryTransferInLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locInventoryTransferInLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locInventoryTransferInLine.ProcessCount > 0)
                            {
                                if (_locInventoryTransferInLine.RmDQty > 0)
                                {
                                    _locRmDQty = _locInventoryTransferInLine.RmDQty - _locInventoryTransferInLine.DQty;
                                }

                                if (_locInventoryTransferInLine.RmQty > 0)
                                {
                                    _locRmQty = _locInventoryTransferInLine.RmQty - _locInventoryTransferInLine.Qty;
                                }

                                if(_locInventoryTransferInLine.MxDQty > 0 || _locInventoryTransferInLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locInventoryTransferInLine.Item),
                                                                new BinaryOperator("UOM", _locInventoryTransferInLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locInventoryTransferInLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                }else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locInventoryTransferInLine.Item),
                                                                new BinaryOperator("UOM", _locInventoryTransferInLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locInventoryTransferInLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                }
                                
                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }

                            }
                            #endregion ProcessCount>0
                            _locInventoryTransferInLine.RmDQty = _locRmDQty;
                            _locInventoryTransferInLine.RmQty = _locRmQty;
                            _locInventoryTransferInLine.RmTQty = _locInvLineTotal;
                            _locInventoryTransferInLine.Save();
                            _locInventoryTransferInLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferIn " + ex.ToString());
            }

        }

        //Menentukan jumlah quantity yang di posting -> Done 10 Dec 2019
        private void SetPostingReceivedQty(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO)
        {
            try
            {
                if (_locInventoryTransferInXPO != null)
                {
                    XPCollection<InventoryTransferInLine> _locInventoryTransferInLines = new XPCollection<InventoryTransferInLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locInventoryTransferInLines != null && _locInventoryTransferInLines.Count > 0)
                    {
                        double _locPDQty = 0;
                        double _locPQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (InventoryTransferInLine _locInventoryTransferInLine in _locInventoryTransferInLines)
                        {
                            #region ProcessCount=0
                            if (_locInventoryTransferInLine.ProcessCount == 0)
                            {
                                #region MaxQuantity
                                if (_locInventoryTransferInLine.MxDQty > 0)
                                {
                                    if (_locInventoryTransferInLine.DQty > 0 && _locInventoryTransferInLine.DQty <= _locInventoryTransferInLine.MxDQty)
                                    {
                                        _locPDQty = _locInventoryTransferInLine.DQty;
                                    }

                                    if (_locInventoryTransferInLine.Qty > 0 && _locInventoryTransferInLine.Qty <= _locInventoryTransferInLine.MxQty)
                                    {
                                        _locPQty = _locInventoryTransferInLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locInventoryTransferInLine.Item),
                                                                new BinaryOperator("UOM", _locInventoryTransferInLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locInventoryTransferInLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locInventoryTransferInLine.DQty > 0)
                                    {
                                        _locPDQty = _locInventoryTransferInLine.DQty;
                                    }

                                    if (_locInventoryTransferInLine.Qty > 0)
                                    {
                                        _locPQty = _locInventoryTransferInLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locInventoryTransferInLine.Item),
                                                                new BinaryOperator("UOM", _locInventoryTransferInLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locInventoryTransferInLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locInventoryTransferInLine.ProcessCount > 0)
                            {
                                if (_locInventoryTransferInLine.PDQty > 0)
                                {
                                    _locPDQty = _locInventoryTransferInLine.PDQty + _locInventoryTransferInLine.DQty;
                                }

                                if (_locInventoryTransferInLine.PQty > 0)
                                {
                                    _locPQty = _locInventoryTransferInLine.PQty + _locInventoryTransferInLine.Qty;
                                }

                                if(_locInventoryTransferInLine.MxDQty > 0 || _locInventoryTransferInLine.MxQty > 0)
                                {

                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locInventoryTransferInLine.Item),
                                                                new BinaryOperator("UOM", _locInventoryTransferInLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locInventoryTransferInLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                }else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locInventoryTransferInLine.Item),
                                                                new BinaryOperator("UOM", _locInventoryTransferInLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locInventoryTransferInLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locInventoryTransferInLine.PDQty = _locPDQty;
                            _locInventoryTransferInLine.PDUOM = _locInventoryTransferInLine.DUOM;
                            _locInventoryTransferInLine.PQty = _locPQty;
                            _locInventoryTransferInLine.PUOM = _locInventoryTransferInLine.UOM;
                            _locInventoryTransferInLine.PTQty = _locInvLineTotal;
                            _locInventoryTransferInLine.Save();
                            _locInventoryTransferInLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferIn " + ex.ToString());
            }

        }

        //Menentukan Banyak process receive
        private void SetProcessCountReceive(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO)
        {
            try
            {
                if (_locInventoryTransferInXPO != null)
                {
                    XPCollection<InventoryTransferInLine> _locInventoryTransferInLines = new XPCollection<InventoryTransferInLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locInventoryTransferInLines != null && _locInventoryTransferInLines.Count > 0)
                    {

                        foreach (InventoryTransferInLine _locInventoryTransferInLine in _locInventoryTransferInLines)
                        {
                            if (_locInventoryTransferInLine.Status == Status.Progress || _locInventoryTransferInLine.Status == Status.Posted)
                            {
                                _locInventoryTransferInLine.ProcessCount = _locInventoryTransferInLine.ProcessCount + 1;
                                _locInventoryTransferInLine.Save();
                                _locInventoryTransferInLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferIn " + ex.ToString());
            }

        }

        private void SetStatusReceiveInventoryTransferInLine(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locInventoryTransferInXPO != null)
                {
                    XPCollection<InventoryTransferInLine> _locInventoryTransferInLines = new XPCollection<InventoryTransferInLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locInventoryTransferInLines != null && _locInventoryTransferInLines.Count > 0)
                    {

                        foreach (InventoryTransferInLine _locInventoryTransferInLine in _locInventoryTransferInLines)
                        {
                            if (_locInventoryTransferInLine.Status == Status.Progress || _locInventoryTransferInLine.Status == Status.Posted)
                            {
                                if (_locInventoryTransferInLine.RmDQty == 0 && _locInventoryTransferInLine.RmQty == 0 && _locInventoryTransferInLine.RmTQty == 0)
                                {
                                    _locInventoryTransferInLine.Status = Status.Close;
                                    _locInventoryTransferInLine.ActivationPosting = true;
                                    _locInventoryTransferInLine.StatusDate = now;
                                }
                                else
                                {
                                    _locInventoryTransferInLine.Status = Status.Posted;
                                    _locInventoryTransferInLine.StatusDate = now;
                                }

                                XPCollection<InventoryTransferInLot> _locInventoryTransferInLots = new XPCollection<InventoryTransferInLot>(_currSession,
                                                                                         new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("InventoryTransferInLine", _locInventoryTransferInLine),
                                                                                         new BinaryOperator("Select", true)));

                                if (_locInventoryTransferInLots != null && _locInventoryTransferInLots.Count() > 0)
                                {
                                    foreach (InventoryTransferInLot _locInventoryTransferInLot in _locInventoryTransferInLots)
                                    {
                                        if (_locInventoryTransferInLot.Status == Status.Progress || _locInventoryTransferInLot.Status == Status.Posted)
                                        {
                                            _locInventoryTransferInLot.Status = Status.Close;
                                            _locInventoryTransferInLot.ActivationPosting = true;
                                            _locInventoryTransferInLot.StatusDate = now;
                                            _locInventoryTransferInLot.Save();
                                            _locInventoryTransferInLot.Session.CommitTransaction();
                                        }
                                    }
                                }

                                _locInventoryTransferInLine.Save();
                                _locInventoryTransferInLine.Session.CommitTransaction();

                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferIn " + ex.ToString());
            }
        }

        #region PurchaseInvoice

        private void SetPurchaseInvoice(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                string _currSignCode = null;
                DateTime now = DateTime.Now;
                PurchaseInvoice _locPurchaseInvoice1 = null;
                PurchaseOrder _locPurchaseOrder = null;
                string _fullString = null;
                string _locPurchaseOrderParse = null;
                string _locProjectHeaderParse = null;
                string _locInventoryTransferParse = null;
                double _locInvLineTotal = 0;
                double _locTUAmount = 0;
                double _locTxAmount = 0;
                double _locDiscAmount = 0;
                double _locTAmount = 0;
                bool _locCBD = false;
                Status _locStatus = Status.Open;

                XPCollection<InventoryTransferInLine> _locInvTransInLines = new XPCollection<InventoryTransferInLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO),
                                                                        new BinaryOperator("Select", true)));

                if (_locInvTransInLines != null && _locInvTransInLines.Count > 0)
                {
                    #region MengecekPurchaseInvoice
                    if(_locInventoryTransferInXPO.PurchaseOrder != null )
                    {
                        if(_locInventoryTransferInXPO.PurchaseOrder.CBD == true)
                        {
                            _locCBD = true;
                            _locStatus = Status.Close;
                        }
                    }

                    if(_locCBD == true)
                    {
                        if (_locInventoryTransferInXPO.PurchaseOrder != null) { _locPurchaseOrderParse = "[PurchaseOrder.Code]=='" + _locInventoryTransferInXPO.PurchaseOrder.Code + "'"; }
                        else { _locPurchaseOrderParse = ""; }

                        if (_locInventoryTransferInXPO.ProjectHeader != null && (_locInventoryTransferInXPO.PurchaseOrder != null))
                        { _locProjectHeaderParse = " AND [ProjectHeader.Code]=='" + _locInventoryTransferInXPO.ProjectHeader.Code + "'"; }
                        else if (_locInventoryTransferInXPO.ProjectHeader != null && _locInventoryTransferInXPO.PurchaseOrder == null)
                        { _locProjectHeaderParse = " [ProjectHeader.Code]=='" + _locInventoryTransferInXPO.ProjectHeader.Code + "'"; }
                        else { _locProjectHeaderParse = ""; }

                    }
                    else
                    {
                        if (_locInventoryTransferInXPO.PurchaseOrder != null) { _locPurchaseOrderParse = "[PurchaseOrder.Code]=='" + _locInventoryTransferInXPO.PurchaseOrder.Code + "'"; }
                        else { _locPurchaseOrderParse = ""; }

                        if (_locInventoryTransferInXPO.ProjectHeader != null && (_locInventoryTransferInXPO.PurchaseOrder != null))
                        { _locProjectHeaderParse = " AND [ProjectHeader.Code]=='" + _locInventoryTransferInXPO.ProjectHeader.Code + "'"; }
                        else if (_locInventoryTransferInXPO.ProjectHeader != null && _locInventoryTransferInXPO.PurchaseOrder == null)
                        { _locProjectHeaderParse = " [ProjectHeader.Code]=='" + _locInventoryTransferInXPO.ProjectHeader.Code + "'"; }
                        else { _locProjectHeaderParse = ""; }

                        if (_locInventoryTransferInXPO != null && (_locInventoryTransferInXPO.PurchaseOrder != null || _locInventoryTransferInXPO.ProjectHeader != null))
                        { _locInventoryTransferParse = " AND [InventoryTransferIn.Code]=='" + _locInventoryTransferInXPO.Code + "'"; }
                        else if (_locInventoryTransferInXPO != null && _locInventoryTransferInXPO.PurchaseOrder == null && _locInventoryTransferInXPO.ProjectHeader == null)
                        { _locInventoryTransferParse = "[InventoryTransferIn.Code] == '" + _locInventoryTransferInXPO.Code + "'"; }
                        else { _locInventoryTransferParse = ""; }
                    }
                    

                    if (_locPurchaseOrderParse != null || _locProjectHeaderParse != null || _locInventoryTransferParse != null)
                    {
                        _fullString = _locPurchaseOrderParse + _locProjectHeaderParse + _locInventoryTransferParse;
                    }

                    _locPurchaseInvoice1 = _currSession.FindObject<PurchaseInvoice>(CriteriaOperator.Parse(_fullString));

                    #region PurchaseInvoiceAvailable
                    if (_locPurchaseInvoice1 != null)
                    {
                        if (_locPurchaseInvoice1.InventoryTransferIn == null)
                        {
                            _locPurchaseInvoice1.InventoryTransferIn = _locInventoryTransferInXPO;
                            _locPurchaseInvoice1.Save();
                            _locPurchaseInvoice1.Session.CommitTransaction();
                        }


                        foreach (InventoryTransferInLine _locInvTransInLine in _locInvTransInLines)
                        {
                            if (_locInventoryTransferInXPO.PurchaseOrder != null)
                            {
                                if (_locInventoryTransferInXPO.PurchaseOrder.Code != null)
                                {
                                    PurchaseOrder _locPurchOrder = _currSession.FindObject<PurchaseOrder>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Code", _locInventoryTransferInXPO.PurchaseOrder.Code)));

                                    if (_locPurchOrder != null)
                                    {
                                        PurchaseOrderLine _locPurchOrderLine = _currSession.FindObject<PurchaseOrderLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("PurchaseOrder", _locPurchOrder),
                                                                                new BinaryOperator("Item", _locInvTransInLine.Item)));

                                        if (_locPurchOrderLine != null)
                                        {
                                            #region ItemUnitOfMeasure
                                            if (_locInvTransInLine.Item != null && _locInvTransInLine.UOM != null && _locInvTransInLine.DUOM != null)
                                            {
                                                ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Item", _locInvTransInLine.Item),
                                                                     new BinaryOperator("UOM", _locInvTransInLine.UOM),
                                                                     new BinaryOperator("DefaultUOM", _locInvTransInLine.DUOM),
                                                                     new BinaryOperator("Active", true)));
                                                if (_locItemUOM != null)
                                                {

                                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                    {
                                                        _locInvLineTotal = _locInvTransInLine.Qty * _locItemUOM.DefaultConversion + _locInvTransInLine.DQty;
                                                    }
                                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                    {
                                                        _locInvLineTotal = _locInvTransInLine.Qty / _locItemUOM.Conversion + _locInvTransInLine.DQty;
                                                    }
                                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                    {
                                                        _locInvLineTotal = _locInvTransInLine.Qty + _locInvTransInLine.DQty;
                                                    }

                                                    _locTUAmount = _locInvLineTotal * _locPurchOrderLine.UAmount;
                                                    if (_locPurchOrderLine.TxValue > 0)
                                                    {
                                                        _locTxAmount = _locInvLineTotal * _locPurchOrderLine.UAmount * _locPurchOrderLine.TxValue / 100;
                                                    }
                                                    else
                                                    {
                                                        _locTxAmount = _locInvLineTotal * _locPurchOrderLine.TxAmount;
                                                    }
                                                    if (_locPurchOrderLine.Disc > 0)
                                                    {
                                                        _locDiscAmount = _locInvLineTotal * _locPurchOrderLine.UAmount * _locPurchOrderLine.Disc / 100;
                                                    }
                                                    else
                                                    {
                                                        _locDiscAmount = _locInvLineTotal * _locPurchOrderLine.DiscAmount;
                                                    }

                                                    _locTAmount = _locTUAmount + _locTxAmount - _locDiscAmount;

                                                    PurchaseInvoiceLine _saveDataPurchaseInvoiceLine = new PurchaseInvoiceLine(_currSession)
                                                    {
                                                        ActivationPosting = true,
                                                        Select = true,
                                                        PurchaseType = _locPurchOrderLine.PurchaseType,
                                                        Item = _locInvTransInLine.Item,
                                                        Description = _locInvTransInLine.Description,
                                                        MxDQty = _locPurchOrderLine.MxDQty,
                                                        MxDUOM = _locPurchOrderLine.MxDUOM,
                                                        MxQty = _locPurchOrderLine.MxQty,
                                                        MxUOM = _locPurchOrderLine.MxUOM,
                                                        MxTQty = _locPurchOrderLine.MxTQty,
                                                        MxUAmount = _locPurchOrderLine.MxUAmount,
                                                        DQty = _locInvTransInLine.DQty,
                                                        DUOM = _locInvTransInLine.DUOM,
                                                        Qty = _locInvTransInLine.Qty,
                                                        UOM = _locInvTransInLine.UOM,
                                                        TQty = _locInvLineTotal,
                                                        UAmount = _locPurchOrderLine.UAmount,
                                                        TUAmount = _locTUAmount,
                                                        Tax = _locPurchOrderLine.Tax,
                                                        TxValue = _locPurchOrderLine.TxValue,
                                                        TxAmount = _locTxAmount,
                                                        Discount = _locPurchOrderLine.Discount,
                                                        Disc = _locPurchOrderLine.Disc,
                                                        DiscAmount = _locDiscAmount,
                                                        TAmount = _locTAmount,
                                                        InventoryTransferInLine = _locInvTransInLine,
                                                        PurchaseInvoice = _locPurchaseInvoice1,
                                                        Status = _locStatus,
                                                        Company = _locPurchaseInvoice1.Company,
                                                    };
                                                    _saveDataPurchaseInvoiceLine.Save();
                                                    _saveDataPurchaseInvoiceLine.Session.CommitTransaction();

                                                }
                                            }
                                            #endregion ItemUnitOfMeasure
                                            #region NonItemUnitOfMeasure
                                            else
                                            {
                                                _locInvLineTotal = _locInvTransInLine.Qty + _locInvTransInLine.DQty;
                                                _locTUAmount = _locInvLineTotal * _locPurchOrderLine.UAmount;
                                                if (_locPurchOrderLine.TxValue > 0)
                                                {
                                                    _locTxAmount = _locInvLineTotal * _locPurchOrderLine.UAmount * _locPurchOrderLine.TxValue / 100;
                                                }
                                                else
                                                {
                                                    _locTxAmount = _locInvLineTotal * _locPurchOrderLine.TxAmount;
                                                }
                                                if (_locPurchOrderLine.Disc > 0)
                                                {
                                                    _locDiscAmount = _locInvLineTotal * _locPurchOrderLine.UAmount * _locPurchOrderLine.Disc / 100;
                                                }
                                                else
                                                {
                                                    _locDiscAmount = _locInvLineTotal * _locPurchOrderLine.DiscAmount;
                                                }
                                                _locTAmount = _locTUAmount + _locTxAmount - _locDiscAmount;

                                                PurchaseInvoiceLine _saveDataPurchaseInvoiceLine = new PurchaseInvoiceLine(_currSession)
                                                {
                                                    ActivationPosting = true,
                                                    Select = true,
                                                    PurchaseType = _locPurchOrderLine.PurchaseType,
                                                    Item = _locInvTransInLine.Item,
                                                    Description = _locInvTransInLine.Description,
                                                    MxDQty = _locPurchOrderLine.MxDQty,
                                                    MxDUOM = _locPurchOrderLine.MxDUOM,
                                                    MxQty = _locPurchOrderLine.MxQty,
                                                    MxUOM = _locPurchOrderLine.MxUOM,
                                                    MxTQty = _locPurchOrderLine.MxTQty,
                                                    MxUAmount = _locPurchOrderLine.MxUAmount,
                                                    DQty = _locInvTransInLine.DQty,
                                                    DUOM = _locInvTransInLine.DUOM,
                                                    Qty = _locInvTransInLine.Qty,
                                                    UOM = _locInvTransInLine.UOM,
                                                    TQty = _locInvLineTotal,
                                                    UAmount = _locPurchOrderLine.UAmount,
                                                    TUAmount = _locTUAmount,
                                                    Tax = _locPurchOrderLine.Tax,
                                                    TxValue = _locPurchOrderLine.TxValue,
                                                    TxAmount = _locTxAmount,
                                                    Discount = _locPurchOrderLine.Discount,
                                                    Disc = _locPurchOrderLine.Disc,
                                                    DiscAmount = _locDiscAmount,
                                                    TAmount = _locTAmount,
                                                    InventoryTransferInLine = _locInvTransInLine,
                                                    PurchaseInvoice = _locPurchaseInvoice1,
                                                    Status = _locStatus,
                                                    Company = _locPurchaseInvoice1.Company,
                                                };
                                                _saveDataPurchaseInvoiceLine.Save();
                                                _saveDataPurchaseInvoiceLine.Session.CommitTransaction();
                                            }
                                            #endregion NonItemUnitOfMeasure
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion PurchaseInvoiceAvailable

                    #region PurchaseInvoiceNotAvailable
                    else
                    {
                        _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.PurchaseInvoice);

                        if (_currSignCode != null)
                        {
                            if (_locInventoryTransferInXPO.PurchaseOrder != null)
                            {
                                if (_locInventoryTransferInXPO.PurchaseOrder.Code != null)
                                {
                                    _locPurchaseOrder = _currSession.FindObject<PurchaseOrder>(new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Code", _locInventoryTransferInXPO.PurchaseOrder.Code)));
                                }
                            }

                            if (_locPurchaseOrder != null)
                            {
                                PurchaseInvoice _saveDataPurchaseInvoice = new PurchaseInvoice(_currSession)
                                {
                                    SignCode = _currSignCode,
                                    InventoryTransferIn = _locInventoryTransferInXPO,
                                    BuyFromVendor = _locPurchaseOrder.BuyFromVendor,
                                    BuyFromContact = _locPurchaseOrder.BuyFromContact,
                                    BuyFromCountry = _locPurchaseOrder.BuyFromCountry,
                                    BuyFromCity = _locPurchaseOrder.BuyFromCity,
                                    BuyFromAddress = _locPurchaseOrder.BuyFromAddress,
                                    PayToVendor = _locPurchaseOrder.BuyFromVendor,
                                    PayToContact = _locPurchaseOrder.BuyFromContact,
                                    PayToCountry = _locPurchaseOrder.BuyFromCountry,
                                    PayToCity = _locPurchaseOrder.BuyFromCity,
                                    PayToAddress = _locPurchaseOrder.BuyFromAddress,
                                    TaxNo = _locPurchaseOrder.TaxNo,
                                    TOP = _locPurchaseOrder.TOP,
                                    PaymentMethod = _locPurchaseOrder.PaymentMethod,
                                    PurchaseOrder = _locPurchaseOrder,
                                    ProjectHeader = _locInventoryTransferInXPO.ProjectHeader,
                                    MaxPay = _locInventoryTransferInXPO.MaxPay,
                                    Company = _locInventoryTransferInXPO.Company,
                                };
                                _saveDataPurchaseInvoice.Save();
                                _saveDataPurchaseInvoice.Session.CommitTransaction();

                                PurchaseInvoice _locPurchaseInvoice2 = _currSession.FindObject<PurchaseInvoice>(new GroupOperator(GroupOperatorType.And,
                                                                 new BinaryOperator("SignCode", _currSignCode)));

                                if (_locPurchaseInvoice2 != null)
                                {
                                    foreach (InventoryTransferInLine _locInvTransInLine in _locInvTransInLines)
                                    {

                                        if (_locInvTransInLine.Status == Status.Progress || _locInvTransInLine.Status == Status.Posted)
                                        {
                                            if (_locInvTransInLine.DQty > 0 || _locInvTransInLine.Qty > 0)
                                            {
                                                
                                                PurchaseOrderLine _locPurchOrderLine = _currSession.FindObject<PurchaseOrderLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("PurchaseOrder", _locPurchaseOrder),
                                                                            new BinaryOperator("Item", _locInvTransInLine.Item)));

                                                if (_locPurchOrderLine != null)
                                                {
                                                    if (_locInvTransInLine.Item != null && _locInvTransInLine.UOM != null && _locInvTransInLine.DUOM != null)
                                                    {

                                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("Item", _locInvTransInLine.Item),
                                                                             new BinaryOperator("UOM", _locInvTransInLine.UOM),
                                                                             new BinaryOperator("DefaultUOM", _locInvTransInLine.DUOM),
                                                                             new BinaryOperator("Active", true)));
                                                        if (_locItemUOM != null)
                                                        {

                                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                            {
                                                                _locInvLineTotal = _locInvTransInLine.Qty * _locItemUOM.DefaultConversion + _locInvTransInLine.DQty;
                                                            }
                                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                            {
                                                                _locInvLineTotal = _locInvTransInLine.Qty / _locItemUOM.Conversion + _locInvTransInLine.DQty;
                                                            }
                                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                            {
                                                                _locInvLineTotal = _locInvTransInLine.Qty + _locInvTransInLine.DQty;
                                                            }

                                                            _locTUAmount = _locInvLineTotal * _locPurchOrderLine.UAmount;
                                                            if (_locPurchOrderLine.TxValue > 0)
                                                            {
                                                                _locTxAmount = _locInvLineTotal * _locPurchOrderLine.UAmount * _locPurchOrderLine.TxValue / 100;
                                                            }
                                                            else
                                                            {
                                                                _locTxAmount = 0;
                                                            }
                                                            if (_locPurchOrderLine.Disc > 0)
                                                            {
                                                                _locDiscAmount = _locInvLineTotal * _locPurchOrderLine.UAmount * _locPurchOrderLine.Disc / 100;
                                                            }
                                                            else
                                                            {
                                                                _locDiscAmount = 0;
                                                            }

                                                            _locTAmount = _locTUAmount + _locTxAmount - _locDiscAmount;

                                                            PurchaseInvoiceLine _saveDataPurchaseInvoiceLine = new PurchaseInvoiceLine(_currSession)
                                                            {
                                                                ActivationPosting = true,
                                                                Select = true,
                                                                PurchaseType = _locPurchOrderLine.PurchaseType,
                                                                Item = _locInvTransInLine.Item,
                                                                Description = _locInvTransInLine.Description,
                                                                MxDQty = _locInvTransInLine.DQty,
                                                                MxDUOM = _locInvTransInLine.DUOM,
                                                                MxQty = _locInvTransInLine.Qty,
                                                                MxUOM = _locInvTransInLine.UOM,
                                                                MxTQty = _locInvLineTotal,
                                                                MxUAmount = _locPurchOrderLine.UAmount,
                                                                DQty = _locInvTransInLine.DQty,
                                                                DUOM = _locInvTransInLine.DUOM,
                                                                Qty = _locInvTransInLine.Qty,
                                                                UOM = _locInvTransInLine.UOM,
                                                                TQty = _locInvLineTotal,
                                                                UAmount = _locPurchOrderLine.UAmount,
                                                                TUAmount = _locTUAmount,
                                                                Tax = _locPurchOrderLine.Tax,
                                                                TxValue = _locPurchOrderLine.TxValue,
                                                                TxAmount = _locTxAmount,
                                                                Discount = _locPurchOrderLine.Discount,
                                                                Disc = _locPurchOrderLine.Disc,
                                                                DiscAmount = _locDiscAmount,
                                                                TAmount = _locTAmount,
                                                                InventoryTransferInLine = _locInvTransInLine,
                                                                PurchaseInvoice = _locPurchaseInvoice2,
                                                                Company = _locPurchaseInvoice2.Company,
                                                            };
                                                            _saveDataPurchaseInvoiceLine.Save();
                                                            _saveDataPurchaseInvoiceLine.Session.CommitTransaction();

                                                        }
                                                    }
                                                    else
                                                    {
                                                        _locInvLineTotal = _locInvTransInLine.Qty + _locInvTransInLine.DQty;
                                                        _locTUAmount = _locInvLineTotal * _locPurchOrderLine.UAmount;
                                                        if (_locPurchOrderLine.TxValue > 0)
                                                        {
                                                            _locTxAmount = _locInvLineTotal * _locPurchOrderLine.UAmount * _locPurchOrderLine.TxValue / 100;
                                                        }
                                                        else
                                                        {
                                                            _locTxAmount = _locInvLineTotal * _locPurchOrderLine.TxAmount;
                                                        }
                                                        if (_locPurchOrderLine.Disc > 0)
                                                        {
                                                            _locDiscAmount = _locInvLineTotal * _locPurchOrderLine.UAmount * _locPurchOrderLine.Disc / 100;
                                                        }
                                                        else
                                                        {
                                                            _locDiscAmount = _locInvLineTotal * _locPurchOrderLine.DiscAmount;
                                                        }
                                                        _locTAmount = _locTUAmount + _locTxAmount - _locDiscAmount;

                                                        PurchaseInvoiceLine _saveDataPurchaseInvoiceLine = new PurchaseInvoiceLine(_currSession)
                                                        {
                                                            ActivationPosting = true,
                                                            Select = true,
                                                            PurchaseType = _locPurchOrderLine.PurchaseType,
                                                            Item = _locInvTransInLine.Item,
                                                            Description = _locInvTransInLine.Description,
                                                            MxDQty = _locInvTransInLine.DQty,
                                                            MxDUOM = _locInvTransInLine.DUOM,
                                                            MxQty = _locInvTransInLine.Qty,
                                                            MxUOM = _locInvTransInLine.DUOM,
                                                            MxTQty = _locInvLineTotal,
                                                            MxUAmount = _locPurchOrderLine.UAmount,
                                                            DQty = _locInvTransInLine.DQty,
                                                            DUOM = _locInvTransInLine.DUOM,
                                                            Qty = _locInvTransInLine.Qty,
                                                            UOM = _locInvTransInLine.UOM,
                                                            TQty = _locInvLineTotal,
                                                            UAmount = _locPurchOrderLine.UAmount,
                                                            TUAmount = _locTUAmount,
                                                            Tax = _locPurchOrderLine.Tax,
                                                            TxValue = _locPurchOrderLine.TxValue,
                                                            TxAmount = _locTxAmount,
                                                            Discount = _locPurchOrderLine.Discount,
                                                            Disc = _locPurchOrderLine.Disc,
                                                            DiscAmount = _locDiscAmount,
                                                            TAmount = _locTAmount,
                                                            InventoryTransferInLine = _locInvTransInLine,
                                                            PurchaseInvoice = _locPurchaseInvoice2,
                                                            Company = _locPurchaseInvoice2.Company,
                                                        };
                                                        _saveDataPurchaseInvoiceLine.Save();
                                                        _saveDataPurchaseInvoiceLine.Session.CommitTransaction();
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    SetPurchaseInvoiceCollection(_currSession, _locInventoryTransferInXPO, _locPurchaseInvoice2);
                                }
                            }
                        }
                    }
                    #endregion PurchaseInvoiceNotAvailable

                    #endregion MengecekPurchaseInvoice
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferIn ", ex.ToString());
            }
        }

        //Membuat Purchase Invoice Collection
        private void SetPurchaseInvoiceCollection(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                //SalesInvoiceCollection based On Sales Order
                GlobalFunction _globFunc = new GlobalFunction();
                PurchaseOrder _locPurchaseOrder = null;
                string _currSignCode = null;
                BusinessPartner _locBusinessPartner = null;
                ProjectHeader _locProjectHeader = null;

                if (_locInventoryTransferInXPO != null)
                {
                    if (_locInventoryTransferInXPO.PurchaseOrder != null)
                    {
                        _locPurchaseOrder = _locInventoryTransferInXPO.PurchaseOrder;
                        if (_locInventoryTransferInXPO.PurchaseOrder.BuyFromVendor != null)
                        {
                            _locBusinessPartner = _locInventoryTransferInXPO.PurchaseOrder.BuyFromVendor;
                        }
                    }
                    if (_locInventoryTransferInXPO.ProjectHeader != null)
                    {
                        _locProjectHeader = _locInventoryTransferInXPO.ProjectHeader;
                    }
                }

                PurchaseInvoiceCollection _locPurchaseInvoiceCollection = _currSession.FindObject<PurchaseInvoiceCollection>(new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("PurchaseOrder", _locPurchaseOrder)));

                if (_locPurchaseInvoiceCollection != null)
                {
                    PurchaseInvoiceCollectionLine _locPurchaseInvoiceCollectionLine = _currSession.FindObject<PurchaseInvoiceCollectionLine>(new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("PurchaseInvoiceCollection", _locPurchaseInvoiceCollection),
                                                                    new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                                    new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO)));
                    if (_locPurchaseInvoiceCollectionLine == null)
                    {
                        PurchaseInvoiceCollectionLine _saveDataPurchaseInvoiceCollectionLine = new PurchaseInvoiceCollectionLine(_currSession)
                        {
                            InventoryTransferIn = _locInventoryTransferInXPO,
                            PurchaseInvoice = _locPurchaseInvoiceXPO,
                            PurchaseInvoiceCollection = _locPurchaseInvoiceCollection,
                        };
                        _saveDataPurchaseInvoiceCollectionLine.Save();
                        _saveDataPurchaseInvoiceCollectionLine.Session.CommitTransaction();
                    }

                }
                else
                {
                    _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.PurchaseInvoiceCollection);
                    if (_currSignCode != null)
                    {
                        PurchaseInvoiceCollection _saveDataPurchaseInvoiceCollection = new PurchaseInvoiceCollection(_currSession)
                        {
                            MaxPay = _locInventoryTransferInXPO.MaxPay,
                            SignCode = _currSignCode,
                            BuyFromVendor = _locPurchaseOrder.BuyFromVendor,
                            BuyFromContact = _locPurchaseOrder.BuyFromContact,
                            BuyFromCountry = _locPurchaseOrder.BuyFromCountry,
                            BuyFromCity = _locPurchaseOrder.BuyFromCity,
                            BuyFromAddress = _locPurchaseOrder.BuyFromAddress,
                            TaxNo = _locPurchaseOrder.TaxNo,
                            TOP = _locPurchaseOrder.TOP,
                            PurchaseOrder = _locPurchaseOrder,
                            ProjectHeader = _locProjectHeader,
                            Company = _locInventoryTransferInXPO.Company,
                        };
                        _saveDataPurchaseInvoiceCollection.Save();
                        _saveDataPurchaseInvoiceCollection.Session.CommitTransaction();

                        PurchaseInvoiceCollection _locPurchaseInvoiceCollection2 = _currSession.FindObject<PurchaseInvoiceCollection>(new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("SignCode", _currSignCode)));
                        if (_locPurchaseInvoiceCollection2 != null)
                        {
                            PurchaseInvoiceCollectionLine _saveDataPurchaseInvoiceCollectionLine = new PurchaseInvoiceCollectionLine(_currSession)
                            {
                                InventoryTransferIn = _locInventoryTransferInXPO,
                                PurchaseInvoice = _locPurchaseInvoiceXPO,
                                PurchaseInvoiceCollection = _locPurchaseInvoiceCollection2,
                            };
                            _saveDataPurchaseInvoiceCollectionLine.Save();
                            _saveDataPurchaseInvoiceCollectionLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferIn ", ex.ToString());
            }
        }

        #endregion PurchaseInvoice

        //Menormalkan Quantity
        private void SetNormalQuantityReceive(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO)
        {
            try
            {
                if (_locInventoryTransferInXPO != null)
                {
                    XPCollection<InventoryTransferInLine> _locInventoryTransferInLines = new XPCollection<InventoryTransferInLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locInventoryTransferInLines != null && _locInventoryTransferInLines.Count > 0)
                    {

                        foreach (InventoryTransferInLine _locInventoryTransferInLine in _locInventoryTransferInLines)
                        {
                            if (_locInventoryTransferInLine.Status == Status.Progress || _locInventoryTransferInLine.Status == Status.Posted || _locInventoryTransferInLine.Status == Status.Close)
                            {
                                if (_locInventoryTransferInLine.DQty > 0 || _locInventoryTransferInLine.Qty > 0)
                                {
                                    _locInventoryTransferInLine.Select = false;
                                    _locInventoryTransferInLine.DQty = 0;
                                    _locInventoryTransferInLine.Qty = 0;
                                    _locInventoryTransferInLine.Save();
                                    _locInventoryTransferInLine.Session.CommitTransaction();

                                    XPCollection<InventoryTransferInLot> _locInventoryTransferInLots = new XPCollection<InventoryTransferInLot>(_currSession,
                                                                                         new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("InventoryTransferInLine", _locInventoryTransferInLine),
                                                                                         new BinaryOperator("Select", true)));

                                    if (_locInventoryTransferInLots != null && _locInventoryTransferInLots.Count() > 0)
                                    {
                                        foreach (InventoryTransferInLot _locInventoryTransferInLot in _locInventoryTransferInLots)
                                        {
                                            if (_locInventoryTransferInLot.Status == Status.Progress || _locInventoryTransferInLot.Status == Status.Posted)
                                            {
                                                _locInventoryTransferInLot.Select = false;
                                                _locInventoryTransferInLot.Save();
                                                _locInventoryTransferInLot.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                } 
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferIn " + ex.ToString());
            }
        }

        //Menentukan Status Receive Pada Inventory Transfer
        private void SetFinalStatusReceiveInventoryTransferIn(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locCount = 0;
                if (_locInventoryTransferInXPO != null)
                {
                    XPCollection<InventoryTransferInLine> _locInventoryTransferInLines = new XPCollection<InventoryTransferInLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO)));

                    if (_locInventoryTransferInLines != null && _locInventoryTransferInLines.Count() > 0)
                    {

                        foreach (InventoryTransferInLine _locInventoryTransferInLine in _locInventoryTransferInLines)
                        {
                            if (_locInventoryTransferInLine.Status == Status.Close)
                            {
                                _locCount = _locCount + 1;
                            }
                        }

                        if (_locCount == _locInventoryTransferInLines.Count())
                        {
                            _locInventoryTransferInXPO.ActivationPosting = true;
                            _locInventoryTransferInXPO.Status = Status.Close;
                            _locInventoryTransferInXPO.StatusDate = now;
                            _locInventoryTransferInXPO.Save();
                            _locInventoryTransferInXPO.Session.CommitTransaction();
                        }
                        else
                        {
                            _locInventoryTransferInXPO.Status = Status.Posted;
                            _locInventoryTransferInXPO.StatusDate = now;
                            _locInventoryTransferInXPO.Save();
                            _locInventoryTransferInXPO.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferIn " + ex.ToString());
            }
        }

        #endregion InventoryReceive

        #region GoodsReceiveJournal

        private void SetGoodsReceiptJournal(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locTotalUnitAmount = 0;
                double _locInvLineTotal = 0;

                //GetTotalPostedStatus
                if(_locInventoryTransferInXPO != null)
                {
                    XPQuery<InventoryTransferInLine> _inventoryTransferInLinesQuery = new XPQuery<InventoryTransferInLine>(_currSession);

                    var _inventoryTransferInLines = from itl in _inventoryTransferInLinesQuery
                                                    where (itl.InventoryTransferIn == _locInventoryTransferInXPO
                                                    && (itl.Status == Status.Progress || itl.Status == Status.Posted))
                                                    group itl by itl.Item.ItemAccountGroup into g
                                                    select new { ItemAccountGroup = g.Key };

                    if (_inventoryTransferInLines != null && _inventoryTransferInLines.Count() > 0)
                    {
                        foreach (var _inventoryTransferInLine in _inventoryTransferInLines)
                        {
                            //Menjumlahkan total uang pergroupnya
                            XPCollection<InventoryTransferInLine> _locInvTransInLines = new XPCollection<InventoryTransferInLine>(_currSession,
                                                                                    new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO),
                                                                                    new BinaryOperator("Item.ItemAccountGroup", _inventoryTransferInLine.ItemAccountGroup)));

                            if (_locInvTransInLines != null && _locInvTransInLines.Count > 0)
                            {
                                #region MenghitungTotalUnitPriceBasedOnItemAccountGroup
                                foreach (InventoryTransferInLine _locInvTransInLine in _locInvTransInLines)
                                {
                                    if (_locInvTransInLine.Status == Status.Progress || _locInvTransInLine.Status == Status.Posted)
                                    {
                                        if (_locInventoryTransferInXPO.PurchaseOrder != null)
                                        {
                                            PurchaseOrderLine _locPurchaseOrderLine = _currSession.FindObject<PurchaseOrderLine>
                                                                             (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("PurchaseOrder", _locInventoryTransferInXPO.PurchaseOrder),
                                                                             new BinaryOperator("Item", _locInvTransInLine.Item)));

                                            if (_locPurchaseOrderLine.PurchaseType == OrderType.Item)
                                            {
                                                if (_locInvTransInLine.Item != null && _locInvTransInLine.UOM != null && _locInvTransInLine.DUOM != null)
                                                {
                                                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Item", _locInvTransInLine.Item),
                                                                         new BinaryOperator("UOM", _locInvTransInLine.UOM),
                                                                         new BinaryOperator("DefaultUOM", _locInvTransInLine.DUOM),
                                                                         new BinaryOperator("Active", true)));
                                                    if (_locItemUOM != null)
                                                    {

                                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                        {
                                                            _locInvLineTotal = _locInvTransInLine.Qty * _locItemUOM.DefaultConversion + _locInvTransInLine.DQty;
                                                        }
                                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                        {
                                                            _locInvLineTotal = _locInvTransInLine.Qty / _locItemUOM.Conversion + _locInvTransInLine.DQty;
                                                        }
                                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                        {
                                                            _locInvLineTotal = _locInvTransInLine.Qty + _locInvTransInLine.DQty;
                                                        }
                                                        else if (_locInvTransInLine.UOM == _locInvTransInLine.DUOM)
                                                        {
                                                            _locInvLineTotal = _locInvTransInLine.Qty + _locInvTransInLine.DQty;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (_locInvTransInLine.UOM != null && _locInvTransInLine.DUOM != null)
                                                    {
                                                        if (_locInvTransInLine.UOM == _locInvTransInLine.DUOM)
                                                        {
                                                            _locInvLineTotal = _locInvTransInLine.Qty + _locInvTransInLine.DQty;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        _locInvLineTotal = _locInvTransInLine.Qty + _locInvTransInLine.DQty;
                                                    }
                                                }
                                                _locTotalUnitAmount = _locInvLineTotal * _locPurchaseOrderLine.UAmount;

                                            }
                                        }
                                    }
                                }
                                #endregion MenghitungTotalUnitPriceBasedOnItemAccountGroup

                                #region CreateGoodsReceiptJournal

                                #region JournalMapByItems
                                XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("ItemAccountGroup", _inventoryTransferInLine.ItemAccountGroup)));

                                if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                {
                                    foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                    {
                                        XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("JournalMap", _locJournalMapItem)));

                                        if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                        {
                                            foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                            {
                                                AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                             (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                             new BinaryOperator("PostingType", PostingType.Purchase),
                                                                             new BinaryOperator("OrderType", OrderType.Item),
                                                                             new BinaryOperator("PostingMethod", PostingMethod.Receipt),
                                                                             new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                if (_locAccountMap != null)
                                                {
                                                    XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                        new BinaryOperator("Active", true)));

                                                    if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                    {
                                                        double _locTotalAmountDebit = 0;
                                                        double _locTotalAmountCredit = 0;
                                                        double _locTotalBalance = 0;

                                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                        {
                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                            {
                                                                _locTotalAmountDebit = _locTotalUnitAmount;
                                                            }
                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                            {
                                                                _locTotalAmountCredit = _locTotalUnitAmount;
                                                            }

                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                            {
                                                                PostingDate = now,
                                                                PostingType = PostingType.Purchase,
                                                                PostingMethod = PostingMethod.Receipt,
                                                                Account = _locAccountMapLine.Account,
                                                                Debit = _locTotalAmountDebit,
                                                                Credit = _locTotalAmountCredit,
                                                                PurchaseOrder = _locInventoryTransferInXPO.PurchaseOrder,
                                                                InventoryTransferIn = _locInventoryTransferInXPO,
                                                                Company = _locInventoryTransferInXPO.Company,
                                                            };
                                                            _saveGeneralJournal.Save();
                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                            if (_locAccountMapLine.Account.Code != null)
                                                            {
                                                                ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                         (new GroupOperator(GroupOperatorType.And,
                                                                                          new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                if (_locCOA != null)
                                                                {
                                                                    if (_locCOA.BalanceType == BalanceType.Change)
                                                                    {
                                                                        if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            if (_locTotalAmountDebit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                            }
                                                                            if (_locTotalAmountCredit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                            }
                                                                        }
                                                                        if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            if (_locTotalAmountDebit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                            }
                                                                            if (_locTotalAmountCredit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                            }
                                                                        }
                                                                    }

                                                                    _locCOA.Balance = _locTotalBalance;
                                                                    _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                    _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                    _locCOA.Save();
                                                                    _locCOA.Session.CommitTransaction();
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion JournalMapByItems

                                #region JournalMapByBusinessPartner
                                if (_locInventoryTransferInXPO.BusinessPartner != null)
                                {
                                    if (_locInventoryTransferInXPO.BusinessPartner.BusinessPartnerAccountGroup != null)
                                    {
                                        XPCollection<JournalMap> _locJournalMapBusinessPartners = new XPCollection<JournalMap>
                                                                                                  (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                   new BinaryOperator("BusinessPartnerAccountGroup", _locInventoryTransferInXPO.BusinessPartner.BusinessPartnerAccountGroup)));

                                        if (_locJournalMapBusinessPartners != null && _locJournalMapBusinessPartners.Count() > 0)
                                        {
                                            foreach (JournalMap _locJournalMapBusinessPartner in _locJournalMapBusinessPartners)
                                            {
                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("JournalMap", _locJournalMapBusinessPartner)));

                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                {
                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                    {
                                                        AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                     new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                     new BinaryOperator("OrderType", OrderType.Item),
                                                                                     new BinaryOperator("PostingMethod", PostingMethod.Receipt),
                                                                                     new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                        if (_locAccountMap != null)
                                                        {
                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                                new BinaryOperator("Active", true)));

                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                            {
                                                                double _locTotalAmountDebit = 0;
                                                                double _locTotalAmountCredit = 0;
                                                                double _locTotalBalance = 0;

                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                {
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                    {
                                                                        _locTotalAmountDebit = _locTotalUnitAmount;
                                                                    }
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                    {
                                                                        _locTotalAmountCredit = _locTotalUnitAmount;
                                                                    }

                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                    {
                                                                        PostingDate = now,
                                                                        PostingType = PostingType.Purchase,
                                                                        PostingMethod = PostingMethod.Receipt,
                                                                        Account = _locAccountMapLine.Account,
                                                                        Debit = _locTotalAmountDebit,
                                                                        Credit = _locTotalAmountCredit,
                                                                        PurchaseOrder = _locInventoryTransferInXPO.PurchaseOrder,
                                                                        InventoryTransferIn = _locInventoryTransferInXPO,
                                                                        Company = _locInventoryTransferInXPO.Company,
                                                                    };
                                                                    _saveGeneralJournal.Save();
                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                    {
                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                        if (_locCOA != null)
                                                                        {
                                                                            if (_locCOA.BalanceType == BalanceType.Change)
                                                                            {
                                                                                if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                {
                                                                                    if (_locTotalAmountDebit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                    }
                                                                                    if (_locTotalAmountCredit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                    }
                                                                                }
                                                                                if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                {
                                                                                    if (_locTotalAmountDebit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                    }
                                                                                    if (_locTotalAmountCredit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                    }
                                                                                }
                                                                            }

                                                                            _locCOA.Balance = _locTotalBalance;
                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                            _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                            _locCOA.Save();
                                                                            _locCOA.Session.CommitTransaction();
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion JournalMapByBusinessPartner

                                #endregion CreateGoodsReceiptJournal
                            }
                        }
                    }
                }
               
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferIn ", ex.ToString());
            }
        }

        private bool GetPostedStatusInInventoryTransferInLine(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO)
        {
            bool _result = false;
            try
            {
                double _currCount = 0;
                XPCollection<InventoryTransferInLine> _locInvTransInLines = new XPCollection<InventoryTransferInLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO)));

                if (_locInvTransInLines != null && _locInvTransInLines.Count() > 0)
                {
                    foreach (InventoryTransferInLine _locInvTransInLine in _locInvTransInLines)
                    {
                        if (_locInvTransInLine.Status == Status.Posted)
                        {
                            _currCount = _currCount + 1;
                        }
                    }

                    if (_currCount > 0)
                    {
                        _result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferIn ", ex.ToString());
            }

            return _result;
        }

        private bool GetAvailablePurchaseInvoice(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO)
        {
            bool _result = false;
            try
            {
                PurchaseInvoice _locPurchaseInvoice = _currSession.FindObject<PurchaseInvoice>(new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO)));

                if (_locPurchaseInvoice != null)
                {
                    _result = true;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferIn ", ex.ToString());
            }

            return _result;
        }

        private bool GetSplitGoodsReceive(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO)
        {
            bool _result = false;
            BusinessPartner _locBusinessPartner = null;
            try
            {
                if (_locInventoryTransferInXPO != null)
                {
                    if(_locInventoryTransferInXPO.PurchaseOrder != null)
                    {
                        if(_locInventoryTransferInXPO.PurchaseOrder.BuyFromVendor != null)
                        {
                            _locBusinessPartner = _locInventoryTransferInXPO.PurchaseOrder.BuyFromVendor;
                        }
                    }
                    PurchaseSetupDetail _locPurchaseSetupDetailAll = _currSession.FindObject<PurchaseSetupDetail>(new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("AllSplitGoodsReceive", true),
                                                     new BinaryOperator("Active", true)));

                    if(_locPurchaseSetupDetailAll != null)
                    {
                        _result = true;
                    }else
                    {
                        XPCollection<PurchaseSetupDetail> _locPurchaseSetupDetailSplits = new XPCollection<PurchaseSetupDetail>(_currSession, 
                                                                                                new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("SplitGoodsReceive", true),
                                                                                                new BinaryOperator("Active", true)));
                        
                        if (_locPurchaseSetupDetailSplits != null && _locPurchaseSetupDetailSplits.Count() > 0)
                        {
                            foreach(PurchaseSetupDetail _locPurchaseSetupDetailSplit in _locPurchaseSetupDetailSplits)
                            {
                                if(_locPurchaseSetupDetailSplit.BusinessPartner != null && _locPurchaseSetupDetailSplit.BusinessPartnerActive == true)
                                {
                                    if(_locPurchaseSetupDetailSplit.BusinessPartner == _locBusinessPartner)
                                    {
                                        _result = true;
                                    }
                                }
                            }
                        }
                    }

                }
                
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferIn ", ex.ToString());
            }

            return _result;
        }

        private bool GetAvailableGeneralJournal(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO)
        {
            bool _result = false;
            try
            {
                XPCollection<GeneralJournal> _locGeneralJournals = new XPCollection<GeneralJournal>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO),
                                                                        new BinaryOperator("PostingType", PostingType.Purchase),
                                                                        new BinaryOperator("PostingMethod", PostingMethod.Receipt)));

                if (_locGeneralJournals != null && _locGeneralJournals.Count() > 0)
                {
                    _result = true;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferIn ", ex.ToString());
            }

            return _result;
        }

        #endregion GoodsReceiveJournal

        #region PurchaseInvoiceOld
        //Membuat Purchase Invoice Split Receive
        private void SetPurchaseInvoiceSplitInvoice(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                string _currSignCode = null;
                DateTime now = DateTime.Now;
                PurchaseOrder _locPurchaseOrder = null;
                double _locInvLineTotal = 0;
                double _locTUAmount = 0;
                double _locTxAmount = 0;
                double _locDiscAmount = 0;
                double _locTAmount = 0;
                double _locMaxPay = 0;

                XPCollection<InventoryTransferInLine> _locInvTransInLines = new XPCollection<InventoryTransferInLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO),
                                                                        new BinaryOperator("Select", true)));

                if (_locInvTransInLines != null && _locInvTransInLines.Count > 0)
                {
                    _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.PurchaseInvoice);

                    if (_currSignCode != null)
                    {
                        if (_locInventoryTransferInXPO.PurchaseOrder != null)
                        {
                            if (_locInventoryTransferInXPO.PurchaseOrder.Code != null)
                            {
                                _locPurchaseOrder = _currSession.FindObject<PurchaseOrder>(new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Code", _locInventoryTransferInXPO.PurchaseOrder.Code)));
                            }
                        }

                        if (_locPurchaseOrder != null)
                        {
                            PurchaseInvoice _saveDataPurchaseInvoice = new PurchaseInvoice(_currSession)
                            {
                                SignCode = _currSignCode,
                                InventoryTransferIn = _locInventoryTransferInXPO,
                                BuyFromVendor = _locPurchaseOrder.BuyFromVendor,
                                BuyFromContact = _locPurchaseOrder.BuyFromContact,
                                BuyFromCountry = _locPurchaseOrder.BuyFromCountry,
                                BuyFromCity = _locPurchaseOrder.BuyFromCity,
                                BuyFromAddress = _locPurchaseOrder.BuyFromAddress,
                                PayToVendor = _locPurchaseOrder.BuyFromVendor,
                                PayToContact = _locPurchaseOrder.BuyFromContact,
                                PayToCountry = _locPurchaseOrder.BuyFromCountry,
                                PayToCity = _locPurchaseOrder.BuyFromCity,
                                PayToAddress = _locPurchaseOrder.BuyFromAddress,
                                TaxNo = _locPurchaseOrder.TaxNo,
                                TOP = _locPurchaseOrder.TOP,
                                PurchaseOrder = _locPurchaseOrder,
                                ProjectHeader = _locInventoryTransferInXPO.ProjectHeader,
                                Company = _locInventoryTransferInXPO.Company,
                            };
                            _saveDataPurchaseInvoice.Save();
                            _saveDataPurchaseInvoice.Session.CommitTransaction();

                            PurchaseInvoice _locPurchaseInvoice2 = _currSession.FindObject<PurchaseInvoice>(new GroupOperator(GroupOperatorType.And,
                                                             new BinaryOperator("SignCode", _currSignCode)));

                            if (_locPurchaseInvoice2 != null)
                            {
                                foreach (InventoryTransferInLine _locInvTransInLine in _locInvTransInLines)
                                {

                                    if (_locInvTransInLine.Status == Status.Progress || _locInvTransInLine.Status == Status.Posted)
                                    {
                                        if (_locInvTransInLine.DQty > 0 || _locInvTransInLine.Qty > 0)
                                        {

                                            PurchaseOrderLine _locPurchOrderLine = _currSession.FindObject<PurchaseOrderLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("PurchaseOrder", _locPurchaseOrder),
                                                                        new BinaryOperator("Item", _locInvTransInLine.Item)));

                                            if (_locPurchOrderLine != null)
                                            {
                                                if (_locInvTransInLine.Item != null && _locInvTransInLine.UOM != null && _locInvTransInLine.DUOM != null)
                                                {

                                                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Item", _locInvTransInLine.Item),
                                                                         new BinaryOperator("UOM", _locInvTransInLine.UOM),
                                                                         new BinaryOperator("DefaultUOM", _locInvTransInLine.DUOM),
                                                                         new BinaryOperator("Active", true)));
                                                    if (_locItemUOM != null)
                                                    {

                                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                        {
                                                            _locInvLineTotal = _locInvTransInLine.Qty * _locItemUOM.DefaultConversion + _locInvTransInLine.DQty;
                                                        }
                                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                        {
                                                            _locInvLineTotal = _locInvTransInLine.Qty / _locItemUOM.Conversion + _locInvTransInLine.DQty;
                                                        }
                                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                        {
                                                            _locInvLineTotal = _locInvTransInLine.Qty + _locInvTransInLine.DQty;
                                                        }

                                                        _locTUAmount = _locInvLineTotal * _locPurchOrderLine.UAmount;
                                                        if (_locPurchOrderLine.TxValue > 0)
                                                        {
                                                            _locTxAmount = _locInvLineTotal * _locPurchOrderLine.UAmount * _locPurchOrderLine.TxValue / 100;
                                                        }
                                                        else
                                                        {
                                                            _locTxAmount = _locInvLineTotal * _locPurchOrderLine.TxAmount;
                                                        }
                                                        if (_locPurchOrderLine.Disc > 0)
                                                        {
                                                            _locDiscAmount = _locInvLineTotal * _locPurchOrderLine.UAmount * _locPurchOrderLine.Disc / 100;
                                                        }
                                                        else
                                                        {
                                                            _locDiscAmount = _locInvLineTotal * _locPurchOrderLine.DiscAmount;
                                                        }

                                                        _locTAmount = _locTUAmount + _locTxAmount - _locDiscAmount;

                                                        _locMaxPay = _locMaxPay + _locTAmount;

                                                        PurchaseInvoiceLine _saveDataPurchaseInvoiceLine = new PurchaseInvoiceLine(_currSession)
                                                        {
                                                            PurchaseType = _locPurchOrderLine.PurchaseType,
                                                            Item = _locInvTransInLine.Item,
                                                            Description = _locInvTransInLine.Description,
                                                            MxDQty = _locInvTransInLine.DQty,
                                                            MxDUOM = _locInvTransInLine.DUOM,
                                                            MxQty = _locInvTransInLine.Qty,
                                                            MxUOM = _locInvTransInLine.UOM,
                                                            MxTQty = _locInvLineTotal,
                                                            MxUAmount = _locPurchOrderLine.UAmount,
                                                            DQty = _locInvTransInLine.DQty,
                                                            DUOM = _locInvTransInLine.DUOM,
                                                            Qty = _locInvTransInLine.Qty,
                                                            UOM = _locInvTransInLine.UOM,
                                                            TQty = _locInvLineTotal,
                                                            UAmount = _locPurchOrderLine.UAmount,
                                                            TUAmount = _locTUAmount,
                                                            Tax = _locPurchOrderLine.Tax,
                                                            TxValue = _locPurchOrderLine.TxValue,
                                                            TxAmount = _locTxAmount,
                                                            Discount = _locPurchOrderLine.Discount,
                                                            Disc = _locPurchOrderLine.Disc,
                                                            DiscAmount = _locDiscAmount,
                                                            TAmount = _locTAmount,
                                                            InventoryTransferInLine = _locInvTransInLine,
                                                            PurchaseInvoice = _locPurchaseInvoice2,
                                                            Company = _locPurchaseInvoice2.Company,
                                                        };
                                                        _saveDataPurchaseInvoiceLine.Save();
                                                        _saveDataPurchaseInvoiceLine.Session.CommitTransaction();

                                                    }
                                                }
                                                else
                                                {
                                                    _locInvLineTotal = _locInvTransInLine.Qty + _locInvTransInLine.DQty;
                                                    _locTUAmount = _locInvLineTotal * _locPurchOrderLine.UAmount;
                                                    if (_locPurchOrderLine.TxValue > 0)
                                                    {
                                                        _locTxAmount = _locInvLineTotal * _locPurchOrderLine.UAmount * _locPurchOrderLine.TxValue / 100;
                                                    }
                                                    else
                                                    {
                                                        _locTxAmount = _locInvLineTotal * _locPurchOrderLine.TxAmount;
                                                    }
                                                    if (_locPurchOrderLine.Disc > 0)
                                                    {
                                                        _locDiscAmount = _locInvLineTotal * _locPurchOrderLine.UAmount * _locPurchOrderLine.Disc / 100;
                                                    }
                                                    else
                                                    {
                                                        _locDiscAmount = _locInvLineTotal * _locPurchOrderLine.DiscAmount;
                                                    }
                                                    _locTAmount = _locTUAmount + _locTxAmount - _locDiscAmount;

                                                    _locMaxPay = _locMaxPay + _locTAmount;

                                                    PurchaseInvoiceLine _saveDataPurchaseInvoiceLine = new PurchaseInvoiceLine(_currSession)
                                                    {
                                                        PurchaseType = _locPurchOrderLine.PurchaseType,
                                                        Item = _locInvTransInLine.Item,
                                                        Description = _locInvTransInLine.Description,
                                                        MxDQty = _locInvTransInLine.DQty,
                                                        MxDUOM = _locInvTransInLine.DUOM,
                                                        MxQty = _locInvTransInLine.Qty,
                                                        MxUOM = _locInvTransInLine.UOM,
                                                        MxTQty = _locInvLineTotal,
                                                        MxUAmount = _locPurchOrderLine.UAmount,
                                                        DQty = _locInvTransInLine.DQty,
                                                        DUOM = _locInvTransInLine.DUOM,
                                                        Qty = _locInvTransInLine.Qty,
                                                        UOM = _locInvTransInLine.UOM,
                                                        TQty = _locInvLineTotal,
                                                        UAmount = _locPurchOrderLine.UAmount,
                                                        TUAmount = _locTUAmount,
                                                        Tax = _locPurchOrderLine.Tax,
                                                        TxValue = _locPurchOrderLine.TxValue,
                                                        TxAmount = _locTxAmount,
                                                        Discount = _locPurchOrderLine.Discount,
                                                        Disc = _locPurchOrderLine.Disc,
                                                        DiscAmount = _locDiscAmount,
                                                        TAmount = _locTAmount,
                                                        InventoryTransferInLine = _locInvTransInLine,
                                                        PurchaseInvoice = _locPurchaseInvoice2,
                                                        Company = _locPurchaseInvoice2.Company,
                                                    };
                                                    _saveDataPurchaseInvoiceLine.Save();
                                                    _saveDataPurchaseInvoiceLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                    }
                                    _locPurchaseInvoice2.MaxPay = _locMaxPay;
                                    _locPurchaseInvoice2.Save();
                                    _locPurchaseInvoice2.Session.CommitTransaction();
                                }
                                //Reverse
                                SetReverseGoodsReceiptJournal(_currSession, _locInventoryTransferInXPO, _locPurchaseInvoice2);
                                SetPurchaseInvoiceCollection(_currSession, _locInventoryTransferInXPO, _locPurchaseInvoice2);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferIn ", ex.ToString());
            }
        }

        #endregion PurchaseInvoiceOld

        #region GoodsReceiveJournalOld

        private void SetGoodsReceiptJournalOld(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locTotalUnitPrice = 0;

                //GetTotalPostedStatus
                if (GetPostedStatusInInventoryTransferInLine(_currSession, _locInventoryTransferInXPO) == false)
                {
                    XPQuery<InventoryTransferInLine> _inventoryTransferInLinesQuery = new XPQuery<InventoryTransferInLine>(_currSession);

                    var _inventoryTransferInLines = from itl in _inventoryTransferInLinesQuery
                                                    where (itl.InventoryTransferIn == _locInventoryTransferInXPO
                                                    && (itl.Status == Status.Progress || itl.Status == Status.Posted)
                                                    && itl.Select == true)
                                                    group itl by itl.Item.ItemAccountGroup into g
                                                    select new { ItemAccountGroup = g.Key };

                    if (_inventoryTransferInLines != null && _inventoryTransferInLines.Count() > 0)
                    {
                        foreach (var _inventoryTransferInLine in _inventoryTransferInLines)
                        {
                            //Menjumlahkan total uang pergroupnya
                            XPCollection<InventoryTransferInLine> _locInvTransInLines = new XPCollection<InventoryTransferInLine>(_currSession,
                                                                                    new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO),
                                                                                    new BinaryOperator("Item.ItemAccountGroup", _inventoryTransferInLine.ItemAccountGroup)));

                            if (_locInvTransInLines != null && _locInvTransInLines.Count > 0)
                            {
                                #region MenghitungTotalUnitPriceBasedOnItemAccountGroup
                                foreach (InventoryTransferInLine _locInvTransInLine in _locInvTransInLines)
                                {
                                    if (_locInvTransInLine.Status == Status.Progress || _locInvTransInLine.Status == Status.Posted)
                                    {
                                        if (_locInventoryTransferInXPO.PurchaseOrder != null)
                                        {
                                            PurchaseOrderLine _locPurchaseOrderLine = _currSession.FindObject<PurchaseOrderLine>
                                                                             (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("PurchaseOrder", _locInventoryTransferInXPO.PurchaseOrder),
                                                                             new BinaryOperator("Item", _locInvTransInLine.Item)));

                                            if (_locPurchaseOrderLine.PurchaseType == OrderType.Item)
                                            {
                                                _locTotalUnitPrice = _locTotalUnitPrice + _locPurchaseOrderLine.TUAmount;

                                            }
                                        }
                                    }
                                }
                                #endregion MenghitungTotalUnitPriceBasedOnItemAccountGroup

                                #region CreateGoodsReceiptJournal

                                #region JournalMapByItems
                                XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("ItemAccountGroup", _inventoryTransferInLine.ItemAccountGroup)));

                                if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                {
                                    foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                    {
                                        XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("JournalMap", _locJournalMapItem)));

                                        if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                        {
                                            foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                            {
                                                AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                             (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                             new BinaryOperator("PostingType", PostingType.Purchase),
                                                                             new BinaryOperator("OrderType", OrderType.Item),
                                                                             new BinaryOperator("PostingMethod", PostingMethod.Receipt),
                                                                             new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                if (_locAccountMap != null)
                                                {
                                                    XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                        new BinaryOperator("Active", true)));

                                                    if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                    {
                                                        double _locTotalPriceDebit = 0;
                                                        double _locTotalPriceCredit = 0;

                                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                        {
                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                            {
                                                                _locTotalPriceDebit = _locTotalUnitPrice;
                                                            }
                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                            {
                                                                _locTotalPriceCredit = _locTotalUnitPrice;
                                                            }

                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                            {
                                                                PostingDate = now,
                                                                PostingType = PostingType.Purchase,
                                                                PostingMethod = PostingMethod.Receipt,
                                                                Account = _locAccountMapLine.Account,
                                                                Debit = _locTotalPriceDebit,
                                                                Credit = _locTotalPriceCredit,
                                                                PurchaseOrder = _locInventoryTransferInXPO.PurchaseOrder,
                                                                InventoryTransferIn = _locInventoryTransferInXPO,
                                                                Company = _locInventoryTransferInXPO.Company,
                                                            };
                                                            _saveGeneralJournal.Save();
                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                            if (_locAccountMapLine.Account.Code != null)
                                                            {
                                                                ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                         (new GroupOperator(GroupOperatorType.And,
                                                                                          new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                if (_locCOA != null)
                                                                {
                                                                    _locCOA.Debit = _locCOA.Debit + _locTotalPriceDebit;
                                                                    _locCOA.Credit = _locCOA.Credit + _locTotalPriceCredit;
                                                                    _locCOA.Save();
                                                                    _locCOA.Session.CommitTransaction();
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion JournalMapByItems

                                #region JournalMapByBusinessPartner
                                if (_locInventoryTransferInXPO.BusinessPartner != null)
                                {
                                    if (_locInventoryTransferInXPO.BusinessPartner.BusinessPartnerAccountGroup != null)
                                    {
                                        XPCollection<JournalMap> _locJournalMapBusinessPartners = new XPCollection<JournalMap>
                                                                                                  (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                   new BinaryOperator("BusinessPartnerAccountGroup", _locInventoryTransferInXPO.BusinessPartner.BusinessPartnerAccountGroup)));

                                        if (_locJournalMapBusinessPartners != null && _locJournalMapBusinessPartners.Count() > 0)
                                        {
                                            foreach (JournalMap _locJournalMapBusinessPartner in _locJournalMapBusinessPartners)
                                            {
                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("JournalMap", _locJournalMapBusinessPartner)));

                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                {
                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                    {
                                                        AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                     new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                     new BinaryOperator("OrderType", OrderType.Item),
                                                                                     new BinaryOperator("PostingMethod", PostingMethod.Receipt),
                                                                                     new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                        if (_locAccountMap != null)
                                                        {
                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                                new BinaryOperator("Active", true)));

                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                            {
                                                                double _locTotalPriceDebit = 0;
                                                                double _locTotalPriceCredit = 0;

                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                {
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                    {
                                                                        _locTotalPriceDebit = _locTotalUnitPrice;
                                                                    }
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                    {
                                                                        _locTotalPriceCredit = _locTotalUnitPrice;
                                                                    }

                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                    {
                                                                        PostingDate = now,
                                                                        PostingType = PostingType.Purchase,
                                                                        PostingMethod = PostingMethod.Receipt,
                                                                        Account = _locAccountMapLine.Account,
                                                                        Debit = _locTotalPriceDebit,
                                                                        Credit = _locTotalPriceCredit,
                                                                        PurchaseOrder = _locInventoryTransferInXPO.PurchaseOrder,
                                                                        InventoryTransferIn = _locInventoryTransferInXPO,
                                                                        Company = _locInventoryTransferInXPO.Company,
                                                                    };
                                                                    _saveGeneralJournal.Save();
                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                    {
                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                        if (_locCOA != null)
                                                                        {
                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalPriceDebit;
                                                                            _locCOA.Credit = _locCOA.Credit + _locTotalPriceCredit;
                                                                            _locCOA.Save();
                                                                            _locCOA.Session.CommitTransaction();
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion JournalMapByBusinessPartner

                                #endregion CreateGoodsReceiptJournal
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransfer ", ex.ToString());
            }
        }

        private void SetReverseGoodsReceiptJournal(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locTotalUnitAmount = 0;
                double _locInvLineTotal = 0;

                //GetTotalPostedStatus
                if (_locInventoryTransferInXPO != null && _locPurchaseInvoiceXPO != null)
                {
                    XPQuery<InventoryTransferInLine> _inventoryTransferInLinesQuery = new XPQuery<InventoryTransferInLine>(_currSession);

                    var _inventoryTransferInLines = from itl in _inventoryTransferInLinesQuery
                                                    where (itl.InventoryTransferIn == _locInventoryTransferInXPO
                                                    && (itl.Status == Status.Progress || itl.Status == Status.Posted)
                                                    && itl.Select == true)
                                                    group itl by itl.Item.ItemAccountGroup into g
                                                    select new { ItemAccountGroup = g.Key };

                    if (_inventoryTransferInLines != null && _inventoryTransferInLines.Count() > 0)
                    {
                        foreach (var _inventoryTransferInLine in _inventoryTransferInLines)
                        {
                            //Menjumlahkan total uang pergroupnya
                            XPCollection<InventoryTransferInLine> _locInvTransInLines = new XPCollection<InventoryTransferInLine>(_currSession,
                                                                                    new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO),
                                                                                    new BinaryOperator("Item.ItemAccountGroup", _inventoryTransferInLine.ItemAccountGroup),
                                                                                    new BinaryOperator("Select", true)));

                            if (_locInvTransInLines != null && _locInvTransInLines.Count > 0)
                            {
                                #region MenghitungTotalUnitPriceBasedOnItemAccountGroup
                                foreach (InventoryTransferInLine _locInvTransInLine in _locInvTransInLines)
                                {
                                    if (_locInvTransInLine.Status == Status.Progress || _locInvTransInLine.Status == Status.Posted)
                                    {
                                        if (_locInventoryTransferInXPO.PurchaseOrder != null)
                                        {
                                            PurchaseOrderLine _locPurchaseOrderLine = _currSession.FindObject<PurchaseOrderLine>
                                                                             (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("PurchaseOrder", _locInventoryTransferInXPO.PurchaseOrder),
                                                                             new BinaryOperator("Item", _locInvTransInLine.Item)));

                                            if (_locPurchaseOrderLine.PurchaseType == OrderType.Item)
                                            {
                                                if (_locInvTransInLine.Item != null && _locInvTransInLine.UOM != null && _locInvTransInLine.DUOM != null)
                                                {
                                                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Item", _locInvTransInLine.Item),
                                                                         new BinaryOperator("UOM", _locInvTransInLine.UOM),
                                                                         new BinaryOperator("DefaultUOM", _locInvTransInLine.DUOM),
                                                                         new BinaryOperator("Active", true)));
                                                    if (_locItemUOM != null)
                                                    {

                                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                        {
                                                            _locInvLineTotal = _locInvTransInLine.Qty * _locItemUOM.DefaultConversion + _locInvTransInLine.DQty;
                                                        }
                                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                        {
                                                            _locInvLineTotal = _locInvTransInLine.Qty / _locItemUOM.Conversion + _locInvTransInLine.DQty;
                                                        }
                                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                        {
                                                            _locInvLineTotal = _locInvTransInLine.Qty + _locInvTransInLine.DQty;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (_locInvTransInLine.UOM != null && _locInvTransInLine.DUOM != null)
                                                    {
                                                        if (_locInvTransInLine.UOM == _locInvTransInLine.DUOM)
                                                        {
                                                            _locInvLineTotal = _locInvTransInLine.Qty + _locInvTransInLine.DQty;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        _locInvLineTotal = _locInvTransInLine.Qty + _locInvTransInLine.DQty;
                                                    }
                                                }
                                                _locTotalUnitAmount = _locInvLineTotal * _locPurchaseOrderLine.UAmount;

                                            }
                                        }
                                    }
                                }
                                #endregion MenghitungTotalUnitPriceBasedOnItemAccountGroup

                                #region CreateReverseGoodsReceiptJournal

                                #region JournalMapByItems
                                XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("ItemAccountGroup", _inventoryTransferInLine.ItemAccountGroup)));

                                if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                {
                                    foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                    {
                                        XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("JournalMap", _locJournalMapItem)));

                                        if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                        {
                                            foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                            {
                                                AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                             (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                             new BinaryOperator("PostingType", PostingType.Purchase),
                                                                             new BinaryOperator("OrderType", OrderType.Item),
                                                                             new BinaryOperator("PostingMethod", PostingMethod.Receipt),
                                                                             new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                if (_locAccountMap != null)
                                                {
                                                    XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                        new BinaryOperator("Active", true)));

                                                    if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                    {
                                                        double _locTotalAmountDebit = 0;
                                                        double _locTotalAmountCredit = 0;
                                                        double _locTotalBalance = 0;

                                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                        {
                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                            {
                                                                _locTotalAmountCredit = _locTotalUnitAmount;
                                                            }
                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                            {
                                                                _locTotalAmountDebit = _locTotalUnitAmount;
                                                            }

                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                            {
                                                                PostingDate = now,
                                                                PostingType = PostingType.Purchase,
                                                                PostingMethod = PostingMethod.InvoiceAP,
                                                                Account = _locAccountMapLine.Account,
                                                                Debit = _locTotalAmountDebit,
                                                                Credit = _locTotalAmountCredit,
                                                                PurchaseOrder = _locInventoryTransferInXPO.PurchaseOrder,
                                                                InventoryTransferIn = _locInventoryTransferInXPO,
                                                                PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                Company = _locInventoryTransferInXPO.Company,
                                                            };
                                                            _saveGeneralJournal.Save();
                                                            _saveGeneralJournal.Session.CommitTransaction();



                                                            if (_locAccountMapLine.Account.Code != null)
                                                            {
                                                                ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                         (new GroupOperator(GroupOperatorType.And,
                                                                                          new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                if (_locCOA != null)
                                                                {
                                                                    if (_locCOA.BalanceType == BalanceType.Change)
                                                                    {
                                                                        if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            if (_locTotalAmountDebit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                            }
                                                                            if (_locTotalAmountCredit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                            }
                                                                        }
                                                                        if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            if (_locTotalAmountDebit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                            }
                                                                            if (_locTotalAmountCredit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                            }
                                                                        }
                                                                    }

                                                                    _locCOA.Balance = _locTotalBalance;
                                                                    _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                    _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                    _locCOA.Save();
                                                                    _locCOA.Session.CommitTransaction();
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion JournalMapByItems

                                #region JournalMapByBusinessPartner
                                if (_locInventoryTransferInXPO.BusinessPartner != null)
                                {
                                    if (_locInventoryTransferInXPO.BusinessPartner.BusinessPartnerAccountGroup != null)
                                    {
                                        XPCollection<JournalMap> _locJournalMapBusinessPartners = new XPCollection<JournalMap>
                                                                                                  (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                   new BinaryOperator("BusinessPartnerAccountGroup", _locInventoryTransferInXPO.BusinessPartner.BusinessPartnerAccountGroup)));

                                        if (_locJournalMapBusinessPartners != null && _locJournalMapBusinessPartners.Count() > 0)
                                        {
                                            foreach (JournalMap _locJournalMapBusinessPartner in _locJournalMapBusinessPartners)
                                            {
                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("JournalMap", _locJournalMapBusinessPartner)));

                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                {
                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                    {
                                                        AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                     new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                     new BinaryOperator("OrderType", OrderType.Item),
                                                                                     new BinaryOperator("PostingMethod", PostingMethod.Receipt),
                                                                                     new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                        if (_locAccountMap != null)
                                                        {
                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                                new BinaryOperator("Active", true)));

                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                            {
                                                                double _locTotalAmountDebit = 0;
                                                                double _locTotalAmountCredit = 0;
                                                                double _locTotalBalance = 0;

                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                {
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                    {
                                                                        _locTotalAmountCredit = _locTotalUnitAmount;
                                                                    }
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                    {
                                                                        _locTotalAmountDebit = _locTotalUnitAmount;
                                                                    }

                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                    {
                                                                        PostingDate = now,
                                                                        PostingType = PostingType.Purchase,
                                                                        PostingMethod = PostingMethod.InvoiceAP,
                                                                        Account = _locAccountMapLine.Account,
                                                                        Debit = _locTotalAmountDebit,
                                                                        Credit = _locTotalAmountCredit,
                                                                        PurchaseOrder = _locInventoryTransferInXPO.PurchaseOrder,
                                                                        InventoryTransferIn = _locInventoryTransferInXPO,
                                                                        PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                        Company = _locInventoryTransferInXPO.Company,
                                                                    };
                                                                    _saveGeneralJournal.Save();
                                                                    _saveGeneralJournal.Session.CommitTransaction();



                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                    {
                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                        if (_locCOA != null)
                                                                        {
                                                                            if (_locCOA.BalanceType == BalanceType.Change)
                                                                            {
                                                                                if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                {
                                                                                    if (_locTotalAmountDebit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                    }
                                                                                    if (_locTotalAmountCredit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                    }
                                                                                }
                                                                                if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                {
                                                                                    if (_locTotalAmountDebit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                    }
                                                                                    if (_locTotalAmountCredit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                    }
                                                                                }
                                                                            }

                                                                            _locCOA.Balance = _locTotalBalance;
                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                            _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                            _locCOA.Save();
                                                                            _locCOA.Session.CommitTransaction();
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion JournalMapByBusinessPartner

                                #endregion CreateReverseGoodsReceiptJournal
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferIn ", ex.ToString());
            }
        }

        #endregion GoodsReceiveJournalOld

        #endregion Receive

        #region InventoryReceiveWithStockControlling

        //Menambahkan Qty Available ke Begining Inventory
        private void SetReceiveBeginingInventorySC(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO)
        {
            try
            {
                XPCollection<InventoryTransferInLine> _locInvTransInLines = new XPCollection<InventoryTransferInLine>
                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO),
                                                                    new BinaryOperator("Select", true)));

                if (_locInvTransInLines != null && _locInvTransInLines.Count() > 0)
                {
                    GlobalFunction _globFunc = new GlobalFunction();
                    double _locInvLineTotal = 0;
                    XPCollection<BeginingInventoryLine> _locBegInventoryLines = null;
                    ItemUnitOfMeasure _locItemUOM = null;
                    string _fullString = null;
                    string _locItemParse = null;
                    string _locLocationParse = null;
                    string _locBinLocationParse = null;
                    string _locDUOMParse = null;
                    string _locStockTypeParse = null;
                    string _locProjectHeader = null;
                    string _locActiveParse = null;
                    string _locSignCode = null;

                    foreach (InventoryTransferInLine _locInvTransInLine in _locInvTransInLines)
                    {
                        if (_locInvTransInLine.Status == Status.Progress || _locInvTransInLine.Status == Status.Posted)
                        {
                            if (_locInvTransInLine.DQty > 0 || _locInvTransInLine.Qty > 0)
                            {
                                XPCollection<InventoryTransferInLot> _locInventoryTransferInLots = new XPCollection<InventoryTransferInLot>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("InventoryTransferInLine", _locInvTransInLine)));
                                #region LotNumber
                                if(_locInventoryTransferInLots != null && _locInventoryTransferInLots.Count() > 0)
                                {
                                    foreach (InventoryTransferInLot _locInventoryTransferInLot in _locInventoryTransferInLots)
                                    {
                                        if (_locInventoryTransferInLot.Item != null && _locInventoryTransferInLot.UOM != null && _locInventoryTransferInLot.DUOM != null)
                                        {
                                            _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                 new BinaryOperator("Item", _locInventoryTransferInLot.Item),
                                                                 new BinaryOperator("UOM", _locInventoryTransferInLot.UOM),
                                                                 new BinaryOperator("DefaultUOM", _locInventoryTransferInLot.DUOM),
                                                                 new BinaryOperator("Active", true)));
                                        }


                                        BeginingInventory _locBeginingInventory = _currSession.FindObject<BeginingInventory>(new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Item", _locInventoryTransferInLot.Item)));

                                        #region UpdateBeginingInventory
                                        if (_locBeginingInventory != null)
                                        {
                                            #region Code
                                            if (_locInventoryTransferInLot.Item != null) { _locItemParse = "[Item.Code]=='" + _locInventoryTransferInLot.Item.Code + "'"; }
                                            else { _locItemParse = ""; }

                                            if (_locInventoryTransferInLot.Location != null && (_locInventoryTransferInLot.Item != null))
                                            { _locLocationParse = " AND [Location.Code]=='" + _locInventoryTransferInLot.Location.Code + "'"; }
                                            else if (_locInventoryTransferInLot.Location != null && _locInventoryTransferInLot.Item == null)
                                            { _locLocationParse = " [Location.Code]=='" + _locInventoryTransferInLot.Location.Code + "'"; }
                                            else { _locLocationParse = ""; }

                                            if (_locInventoryTransferInLot.BinLocation != null && (_locInventoryTransferInLot.Item != null || _locInventoryTransferInLot.Location != null))
                                            { _locBinLocationParse = " AND [BinLocation.Code]=='" + _locInventoryTransferInLot.BinLocation.Code + "'"; }
                                            else if (_locInventoryTransferInLot.BinLocation != null && _locInventoryTransferInLot.Item == null && _locInventoryTransferInLot.Location == null)
                                            { _locBinLocationParse = " [BinLocation.Code]=='" + _locInventoryTransferInLot.BinLocation.Code + "'"; }
                                            else { _locBinLocationParse = ""; }

                                            if (_locInventoryTransferInLot.DUOM != null && (_locInventoryTransferInLot.Item != null || _locInventoryTransferInLot.Location != null || _locInventoryTransferInLot.BinLocation != null))
                                            { _locDUOMParse = " AND [DefaultUOM.Code]=='" + _locInventoryTransferInLot.DUOM.Code + "'"; }
                                            else if (_locInventoryTransferInLot.DUOM != null && _locInventoryTransferInLot.Item == null && _locInventoryTransferInLot.Location == null && _locInventoryTransferInLot.BinLocation == null)
                                            { _locDUOMParse = " [DefaultUOM.Code]=='" + _locInventoryTransferInLot.DUOM.Code + "'"; }
                                            else { _locDUOMParse = ""; }

                                            if (_locInventoryTransferInLot.StockType != StockType.None && (_locInventoryTransferInLot.Item != null || _locInventoryTransferInLot.Location != null || _locInventoryTransferInLot.BinLocation != null
                                                || _locInventoryTransferInLot.DUOM != null))
                                            { _locStockTypeParse = " AND [StockType]=='" + GetStockType(_locInventoryTransferInLot.StockType).ToString() + "'"; }
                                            else if (_locInventoryTransferInLot.StockType != StockType.None && _locInventoryTransferInLot.Item == null && _locInventoryTransferInLot.Location == null && _locInventoryTransferInLot.BinLocation == null
                                                && _locInventoryTransferInLot.DUOM == null)
                                            { _locStockTypeParse = " [StockType]=='" + GetStockType(_locInventoryTransferInLot.StockType).ToString() + "'"; }
                                            else { _locStockTypeParse = ""; }

                                            if (_locInventoryTransferInLot.Item == null && _locInventoryTransferInLot.Location == null && _locInventoryTransferInLot.BinLocation == null
                                                && _locInventoryTransferInLot.DUOM == null && _locInventoryTransferInLot.StockType != StockType.None)
                                            { _locActiveParse = " [Active]=='" + GetActive(true).ToString() + "'"; }
                                            else { _locActiveParse = " AND [Active]=='" + GetActive(true).ToString() + "'"; }

                                            if (_locInventoryTransferInXPO.ProjectHeader != null)
                                            { _locProjectHeader = " AND [ProjectHeader.Code]=='" + _locInventoryTransferInXPO.ProjectHeader.Code + "'"; }
                                            else
                                            { _locProjectHeader = ""; }

                                            if (_locItemParse != null || _locLocationParse != null || _locBinLocationParse != null || _locDUOMParse != null || _locStockTypeParse != null || _locProjectHeader != null)
                                            {
                                                _fullString = _locItemParse + _locLocationParse + _locBinLocationParse + _locDUOMParse + _locStockTypeParse + _locActiveParse + _locProjectHeader;
                                            }
                                            else
                                            {
                                                _fullString = _locActiveParse;
                                            }

                                            _locBegInventoryLines = new XPCollection<BeginingInventoryLine>(_currSession, CriteriaOperator.Parse(_fullString));

                                            #endregion Code

                                            #region NewBeginingInventoryLine
                                            if (_locBegInventoryLines == null && _locBegInventoryLines.Count() == 0)
                                            {
                                                if (_locItemUOM != null)
                                                {
                                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                    {
                                                        _locInvLineTotal = _locInventoryTransferInLot.Qty * _locItemUOM.DefaultConversion + _locInventoryTransferInLot.DQty;
                                                    }
                                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                    {
                                                        _locInvLineTotal = _locInventoryTransferInLot.Qty / _locItemUOM.Conversion + _locInventoryTransferInLot.DQty;
                                                    }
                                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                    {
                                                        _locInvLineTotal = _locInventoryTransferInLot.Qty + _locInventoryTransferInLot.DQty;
                                                    }
                                                }
                                                else
                                                {
                                                    _locInvLineTotal = _locInventoryTransferInLot.Qty + _locInventoryTransferInLot.DQty;
                                                }

                                                BeginingInventoryLine _locSaveDataBeginingInventory = new BeginingInventoryLine(_currSession)
                                                {
                                                    Item = _locInventoryTransferInLot.Item,
                                                    Location = _locInventoryTransferInLot.Location,
                                                    BinLocation = _locInventoryTransferInLot.BinLocation,
                                                    QtyAvailable = _locInvLineTotal,
                                                    DefaultUOM = _locInventoryTransferInLot.DUOM,
                                                    StockType = _locInventoryTransferInLot.StockType,
                                                    LotNumber = _locInventoryTransferInLot.LotNumber,
                                                    ProjectHeader = _locInventoryTransferInXPO.ProjectHeader,
                                                    Active = true,
                                                    BeginingInventory = _locBeginingInventory,
                                                };
                                                _locSaveDataBeginingInventory.Save();
                                                _locSaveDataBeginingInventory.Session.CommitTransaction();

                                                _locBeginingInventory.QtyAvailable = _locBeginingInventory.QtyAvailable + _locInvLineTotal;
                                                _locBeginingInventory.Save();
                                                _locBeginingInventory.Session.CommitTransaction();
                                            }
                                            #endregion NewBeginingInventoryLine
                                        }
                                        #endregion UpdateBeginingInventory

                                        #region CreateNewBeginingInventory
                                        else
                                        {
                                            _locSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.BeginingInventory);

                                            if (_locSignCode != null)
                                            {
                                                if (_locItemUOM != null)
                                                {
                                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                    {
                                                        _locInvLineTotal = _locInventoryTransferInLot.Qty * _locItemUOM.DefaultConversion + _locInventoryTransferInLot.DQty;
                                                    }
                                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                    {
                                                        _locInvLineTotal = _locInventoryTransferInLot.Qty / _locItemUOM.Conversion + _locInventoryTransferInLot.DQty;
                                                    }
                                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                    {
                                                        _locInvLineTotal = _locInventoryTransferInLot.Qty + _locInventoryTransferInLot.DQty;
                                                    }

                                                }
                                                else
                                                {
                                                    _locInvLineTotal = _locInventoryTransferInLot.Qty + _locInventoryTransferInLot.DQty;
                                                }

                                                BeginingInventory _saveDataBegInv = new BeginingInventory(_currSession)
                                                {
                                                    Item = _locInventoryTransferInLot.Item,
                                                    QtyAvailable = _locInvLineTotal,
                                                    DefaultUOM = _locInventoryTransferInLot.DUOM,
                                                    Active = true,
                                                    SignCode = _locSignCode,
                                                    Company = _locInventoryTransferInXPO.Company,
                                                };
                                                _saveDataBegInv.Save();
                                                _saveDataBegInv.Session.CommitTransaction();

                                                BeginingInventory _locBeginingInventory2 = _currSession.FindObject<BeginingInventory>(new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Item", _locInventoryTransferInLot.Item),
                                                                            new BinaryOperator("SignCode", _locSignCode)));

                                                if (_locBeginingInventory2 != null)
                                                {
                                                    BeginingInventoryLine _saveDataBegInvLine = new BeginingInventoryLine(_currSession)
                                                    {
                                                        Item = _locInventoryTransferInLot.Item,
                                                        Location = _locInventoryTransferInLot.Location,
                                                        BinLocation = _locInventoryTransferInLot.BinLocation,
                                                        QtyAvailable = _locBeginingInventory2.QtyAvailable,
                                                        DefaultUOM = _locBeginingInventory2.DefaultUOM,
                                                        StockType = _locInventoryTransferInLot.StockType,
                                                        LotNumber = _locInventoryTransferInLot.LotNumber,
                                                        ProjectHeader = _locInventoryTransferInXPO.ProjectHeader,
                                                        Active = true,
                                                        BeginingInventory = _locBeginingInventory2,
                                                        Company = _locBeginingInventory2.Company,
                                                    };
                                                    _saveDataBegInvLine.Save();
                                                    _saveDataBegInvLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        #endregion CreateNewBeginingInventory

                                    }
                                }
                                #endregion LotNumber
                                #region NonLotNumber
                                else
                                {
                                    if (_locInvTransInLine.Item != null && _locInvTransInLine.UOM != null && _locInvTransInLine.DUOM != null)
                                    {
                                        _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                             new BinaryOperator("Item", _locInvTransInLine.Item),
                                                             new BinaryOperator("UOM", _locInvTransInLine.UOM),
                                                             new BinaryOperator("DefaultUOM", _locInvTransInLine.DUOM),
                                                             new BinaryOperator("Active", true)));
                                    }


                                    BeginingInventory _locBeginingInventory = _currSession.FindObject<BeginingInventory>(new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Item", _locInvTransInLine.Item)));

                                    #region UpdateBeginingInventory
                                    if (_locBeginingInventory != null)
                                    {
                                        #region Code
                                        if (_locInvTransInLine.Item != null) { _locItemParse = "[Item.Code]=='" + _locInvTransInLine.Item.Code + "'"; }
                                        else { _locItemParse = ""; }

                                        if (_locInvTransInLine.LocationTo != null && (_locInvTransInLine.Item != null))
                                        { _locLocationParse = " AND [Location.Code]=='" + _locInvTransInLine.LocationTo.Code + "'"; }
                                        else if (_locInvTransInLine.LocationTo != null && _locInvTransInLine.Item == null)
                                        { _locLocationParse = " [Location.Code]=='" + _locInvTransInLine.LocationTo.Code + "'"; }
                                        else { _locLocationParse = ""; }

                                        if (_locInvTransInLine.BinLocationTo != null && (_locInvTransInLine.Item != null || _locInvTransInLine.LocationTo != null))
                                        { _locBinLocationParse = " AND [BinLocation.Code]=='" + _locInvTransInLine.BinLocationTo.Code + "'"; }
                                        else if (_locInvTransInLine.BinLocationTo != null && _locInvTransInLine.Item == null && _locInvTransInLine.LocationTo == null)
                                        { _locBinLocationParse = " [BinLocation.Code]=='" + _locInvTransInLine.BinLocationTo.Code + "'"; }
                                        else { _locBinLocationParse = ""; }

                                        if (_locInvTransInLine.DUOM != null && (_locInvTransInLine.Item != null || _locInvTransInLine.LocationTo != null || _locInvTransInLine.BinLocationTo != null))
                                        { _locDUOMParse = " AND [DefaultUOM.Code]=='" + _locInvTransInLine.DUOM.Code + "'"; }
                                        else if (_locInvTransInLine.DUOM != null && _locInvTransInLine.Item == null && _locInvTransInLine.LocationTo == null && _locInvTransInLine.BinLocationTo == null)
                                        { _locDUOMParse = " [DefaultUOM.Code]=='" + _locInvTransInLine.DUOM.Code + "'"; }
                                        else { _locDUOMParse = ""; }

                                        if (_locInvTransInLine.StockTypeTo != StockType.None && (_locInvTransInLine.Item != null || _locInvTransInLine.LocationTo != null || _locInvTransInLine.BinLocationTo != null
                                            || _locInvTransInLine.DUOM != null))
                                        { _locStockTypeParse = " AND [StockType]=='" + GetStockType(_locInvTransInLine.StockTypeTo).ToString() + "'"; }
                                        else if (_locInvTransInLine.StockTypeTo != StockType.None && _locInvTransInLine.Item == null && _locInvTransInLine.LocationTo == null && _locInvTransInLine.BinLocationTo == null
                                            && _locInvTransInLine.DUOM == null)
                                        { _locStockTypeParse = " [StockType]=='" + GetStockType(_locInvTransInLine.StockTypeTo).ToString() + "'"; }
                                        else { _locStockTypeParse = ""; }

                                        if (_locInvTransInLine.Item == null && _locInvTransInLine.LocationTo == null && _locInvTransInLine.BinLocationTo == null
                                            && _locInvTransInLine.DUOM == null && _locInvTransInLine.StockTypeTo != StockType.None)
                                        { _locActiveParse = " [Active]=='" + GetActive(true).ToString() + "'"; }
                                        else { _locActiveParse = " AND [Active]=='" + GetActive(true).ToString() + "'"; }

                                        if (_locInventoryTransferInXPO.ProjectHeader != null)
                                        { _locProjectHeader = " AND [ProjectHeader.Code]=='" + _locInventoryTransferInXPO.ProjectHeader.Code + "'"; }
                                        else
                                        { _locProjectHeader = ""; }

                                        if (_locItemParse != null || _locLocationParse != null || _locBinLocationParse != null || _locDUOMParse != null || _locStockTypeParse != null || _locProjectHeader != null)
                                        {
                                            _fullString = _locItemParse + _locLocationParse + _locBinLocationParse + _locDUOMParse + _locStockTypeParse + _locActiveParse + _locProjectHeader;
                                        }
                                        else
                                        {
                                            _fullString = _locActiveParse;
                                        }

                                        _locBegInventoryLines = new XPCollection<BeginingInventoryLine>(_currSession, CriteriaOperator.Parse(_fullString));

                                        #endregion Code

                                        #region UpdateBeginingInventoryLine
                                        if (_locBegInventoryLines != null && _locBegInventoryLines.Count() > 0)
                                        {

                                            foreach (BeginingInventoryLine _locBegInventoryLine in _locBegInventoryLines)
                                            {
                                                if (_locItemUOM != null)
                                                {
                                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                    {
                                                        _locInvLineTotal = _locInvTransInLine.Qty * _locItemUOM.DefaultConversion + _locInvTransInLine.DQty;
                                                    }
                                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                    {
                                                        _locInvLineTotal = _locInvTransInLine.Qty / _locItemUOM.Conversion + _locInvTransInLine.DQty;
                                                    }
                                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                    {
                                                        _locInvLineTotal = _locInvTransInLine.Qty + _locInvTransInLine.DQty;
                                                    }

                                                }
                                                else
                                                {
                                                    _locInvLineTotal = _locInvTransInLine.Qty + _locInvTransInLine.DQty;
                                                }
                                                _locBegInventoryLine.QtyAvailable = _locBegInventoryLine.QtyAvailable + _locInvLineTotal;
                                                _locBegInventoryLine.Save();
                                                _locBegInventoryLine.Session.CommitTransaction();

                                                _locBeginingInventory.QtyAvailable = _locBeginingInventory.QtyAvailable + _locInvLineTotal;
                                                _locBeginingInventory.Save();
                                                _locBeginingInventory.Session.CommitTransaction();

                                            }
                                        }
                                        #endregion UpdateBeginingInventoryLine

                                        #region NewBeginingInventoryLine
                                        else
                                        {
                                            if (_locItemUOM != null)
                                            {
                                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                {
                                                    _locInvLineTotal = _locInvTransInLine.Qty * _locItemUOM.DefaultConversion + _locInvTransInLine.DQty;
                                                }
                                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                {
                                                    _locInvLineTotal = _locInvTransInLine.Qty / _locItemUOM.Conversion + _locInvTransInLine.DQty;
                                                }
                                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                {
                                                    _locInvLineTotal = _locInvTransInLine.Qty + _locInvTransInLine.DQty;
                                                }
                                            }
                                            else
                                            {
                                                _locInvLineTotal = _locInvTransInLine.Qty + _locInvTransInLine.DQty;
                                            }

                                            BeginingInventoryLine _locSaveDataBeginingInventory = new BeginingInventoryLine(_currSession)
                                            {
                                                Item = _locInvTransInLine.Item,
                                                Location = _locInvTransInLine.LocationTo,
                                                BinLocation = _locInvTransInLine.BinLocationTo,
                                                QtyAvailable = _locInvLineTotal,
                                                DefaultUOM = _locInvTransInLine.DUOM,
                                                StockType = _locInvTransInLine.StockTypeTo,
                                                Active = true,
                                                BeginingInventory = _locBeginingInventory,
                                            };
                                            _locSaveDataBeginingInventory.Save();
                                            _locSaveDataBeginingInventory.Session.CommitTransaction();
                                        }
                                        #endregion NewBeginingInventoryLine
                                    }
                                    #endregion UpdateBeginingInventory

                                    #region CreateNewBeginingInventory
                                    else
                                    {
                                        _locSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.BeginingInventory);

                                        if (_locSignCode != null)
                                        {
                                            if (_locItemUOM != null)
                                            {
                                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                {
                                                    _locInvLineTotal = _locInvTransInLine.Qty * _locItemUOM.DefaultConversion + _locInvTransInLine.DQty;
                                                }
                                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                {
                                                    _locInvLineTotal = _locInvTransInLine.Qty / _locItemUOM.Conversion + _locInvTransInLine.DQty;
                                                }
                                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                {
                                                    _locInvLineTotal = _locInvTransInLine.Qty + _locInvTransInLine.DQty;
                                                }

                                            }
                                            else
                                            {
                                                _locInvLineTotal = _locInvTransInLine.Qty + _locInvTransInLine.DQty;
                                            }

                                            BeginingInventory _saveDataBegInv = new BeginingInventory(_currSession)
                                            {
                                                Item = _locInvTransInLine.Item,
                                                QtyAvailable = _locInvLineTotal,
                                                DefaultUOM = _locInvTransInLine.DUOM,
                                                Active = true,
                                                SignCode = _locSignCode,
                                                Company = _locInventoryTransferInXPO.Company,
                                            };
                                            _saveDataBegInv.Save();
                                            _saveDataBegInv.Session.CommitTransaction();

                                            BeginingInventory _locBeginingInventory2 = _currSession.FindObject<BeginingInventory>(new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Item", _locInvTransInLine.Item),
                                                                        new BinaryOperator("SignCode", _locSignCode)));

                                            if (_locBeginingInventory2 != null)
                                            {
                                                BeginingInventoryLine _saveDataBegInvLine = new BeginingInventoryLine(_currSession)
                                                {
                                                    Item = _locInvTransInLine.Item,
                                                    Location = _locInvTransInLine.LocationTo,
                                                    BinLocation = _locInvTransInLine.BinLocationTo,
                                                    QtyAvailable = _locBeginingInventory2.QtyAvailable,
                                                    DefaultUOM = _locBeginingInventory2.DefaultUOM,
                                                    StockType = _locInvTransInLine.StockTypeTo,
                                                    ProjectHeader = _locInventoryTransferInXPO.ProjectHeader,
                                                    Active = true,
                                                    BeginingInventory = _locBeginingInventory2,
                                                    Company = _locBeginingInventory2.Company,
                                                };
                                                _saveDataBegInvLine.Save();
                                                _saveDataBegInvLine.Session.CommitTransaction();
                                            }
                                        }

                                    }
                                    #endregion CreateNewBeginingInventory
                                }
                                #endregion NonLotNumber

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferIn ", ex.ToString());
            }
        }

        //Membuat jurnal positif di inventory journal
        private void SetReceiveInventoryJournalSC(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO)
        {
            try
            {
                XPCollection<InventoryTransferInLine> _locInvTransInLines = new XPCollection<InventoryTransferInLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO),
                                                                        new BinaryOperator("Select", true)));

                if (_locInvTransInLines != null && _locInvTransInLines.Count > 0)
                {
                    double _locInvLineTotal = 0;
                    DateTime now = DateTime.Now;

                    foreach (InventoryTransferInLine _locInvTransInLine in _locInvTransInLines)
                    {
                        if (_locInvTransInLine.Status == Status.Progress || _locInvTransInLine.Status == Status.Posted)
                        {
                            if (_locInvTransInLine.DQty > 0 || _locInvTransInLine.Qty > 0)
                            {
                                XPCollection<InventoryTransferInLot> _locInventoryTransferInLots = new XPCollection<InventoryTransferInLot>
                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                       new BinaryOperator("InventoryTransferInLine", _locInvTransInLine)));
                                #region LotNumber
                                if(_locInventoryTransferInLots != null && _locInventoryTransferInLots.Count() > 0)
                                {
                                    foreach (InventoryTransferInLot _locInventoryTransferInLot in _locInventoryTransferInLots)
                                    {
                                        BeginingInventoryLine _locBegInvLine = _currSession.FindObject<BeginingInventoryLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Location", _locInventoryTransferInLot.Location),
                                                                                new BinaryOperator("BinLocation", _locInventoryTransferInLot.BinLocation),
                                                                                new BinaryOperator("Item", _locInventoryTransferInLot.Item),
                                                                                new BinaryOperator("LotNumber", _locInventoryTransferInLot.LotNumber)));
                                        if (_locBegInvLine != null)
                                        {
                                            InventoryJournal _locPositifInventoryJournal = new InventoryJournal(_currSession)
                                            {
                                                DocumentType = _locInventoryTransferInXPO.DocumentType,
                                                DocNo = _locInventoryTransferInXPO.DocNo,
                                                Location = _locInventoryTransferInLot.Location,
                                                BinLocation = _locInventoryTransferInLot.BinLocation,
                                                StockType = _locInventoryTransferInLot.StockType,
                                                Item = _locInventoryTransferInLot.Item,
                                                QtyNeg = 0,
                                                QtyPos = _locInventoryTransferInLot.TQty,
                                                DUOM = _locInventoryTransferInLot.DUOM,
                                                JournalDate = now,
                                                ProjectHeader = _locInventoryTransferInXPO.ProjectHeader,
                                                InventoryTransferIn = _locInventoryTransferInXPO,
                                                Company = _locInventoryTransferInXPO.Company,
                                            };
                                            _locPositifInventoryJournal.Save();
                                            _locPositifInventoryJournal.Session.CommitTransaction();
                                        }
                                    }
                                }
                                #endregion LotNumber
                                #region NonLotNumber
                                else
                                {
                                    if (_locInvTransInLine.Item != null && _locInvTransInLine.UOM != null && _locInvTransInLine.DUOM != null)
                                    {
                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                             new BinaryOperator("Item", _locInvTransInLine.Item),
                                                             new BinaryOperator("UOM", _locInvTransInLine.UOM),
                                                             new BinaryOperator("DefaultUOM", _locInvTransInLine.DUOM),
                                                             new BinaryOperator("Active", true)));
                                        if (_locItemUOM != null)
                                        {

                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                            {
                                                _locInvLineTotal = _locInvTransInLine.Qty * _locItemUOM.DefaultConversion + _locInvTransInLine.DQty;
                                            }
                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                            {
                                                _locInvLineTotal = _locInvTransInLine.Qty / _locItemUOM.Conversion + _locInvTransInLine.DQty;
                                            }
                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                            {
                                                _locInvLineTotal = _locInvTransInLine.Qty + _locInvTransInLine.DQty;
                                            }

                                            InventoryJournal _locPositifInventoryJournal = new InventoryJournal(_currSession)
                                            {
                                                DocumentType = _locInventoryTransferInXPO.DocumentType,
                                                DocNo = _locInventoryTransferInXPO.DocNo,
                                                Location = _locInvTransInLine.LocationTo,
                                                BinLocation = _locInvTransInLine.BinLocationTo,
                                                StockType = _locInvTransInLine.StockTypeTo,
                                                Item = _locInvTransInLine.Item,
                                                QtyNeg = 0,
                                                QtyPos = _locInvLineTotal,
                                                DUOM = _locInvTransInLine.DUOM,
                                                JournalDate = now,
                                                ProjectHeader = _locInventoryTransferInXPO.ProjectHeader,
                                                Company = _locInventoryTransferInXPO.Company,
                                                InventoryTransferIn = _locInventoryTransferInXPO,
                                            };
                                            _locPositifInventoryJournal.Save();
                                            _locPositifInventoryJournal.Session.CommitTransaction();

                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locInvTransInLine.Qty + _locInvTransInLine.DQty;

                                        InventoryJournal _locPositifInventoryJournal = new InventoryJournal(_currSession)
                                        {

                                            DocumentType = _locInventoryTransferInXPO.DocumentType,
                                            DocNo = _locInventoryTransferInXPO.DocNo,
                                            Location = _locInvTransInLine.LocationTo,
                                            BinLocation = _locInvTransInLine.BinLocationTo,
                                            StockType = _locInvTransInLine.StockTypeTo,
                                            Item = _locInvTransInLine.Item,
                                            QtyNeg = 0,
                                            QtyPos = _locInvLineTotal,
                                            DUOM = _locInvTransInLine.DUOM,
                                            JournalDate = now,
                                            ProjectHeader = _locInventoryTransferInXPO.ProjectHeader,
                                            Company = _locInventoryTransferInXPO.Company,
                                            InventoryTransferIn = _locInventoryTransferInXPO,
                                        };
                                        _locPositifInventoryJournal.Save();
                                        _locPositifInventoryJournal.Session.CommitTransaction();
                                    }
                                }
                                #endregion NonLotNumber
                                
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferIn ", ex.ToString());
            }
        }

        //Menentukan sisa dari transaksi receive -> Done 10 Dec 2019
        private void SetRemainReceivedQtySC(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO)
        {
            try
            {
                if (_locInventoryTransferInXPO != null)
                {
                    XPCollection<InventoryTransferInLine> _locInvTransInLines = new XPCollection<InventoryTransferInLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locInvTransInLines != null && _locInvTransInLines.Count > 0)
                    {
                        double _locRmDQty = 0;
                        double _locRmQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (InventoryTransferInLine _locInvTransInLine in _locInvTransInLines)
                        {
                            #region ProcessCount=0
                            if (_locInvTransInLine.ProcessCount == 0)
                            {
                                #region MaxQuantity
                                if (_locInvTransInLine.MxDQty > 0)
                                {
                                    if (_locInvTransInLine.DQty > 0 && _locInvTransInLine.DQty <= _locInvTransInLine.MxDQty)
                                    {
                                        _locRmDQty = _locInvTransInLine.MxDQty - _locInvTransInLine.DQty;
                                    }

                                    if (_locInvTransInLine.Qty > 0 && _locInvTransInLine.Qty <= _locInvTransInLine.MxQty)
                                    {
                                        _locRmQty = _locInvTransInLine.MxQty - _locInvTransInLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locInvTransInLine.Item),
                                                                new BinaryOperator("UOM", _locInvTransInLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locInvTransInLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locInvTransInLine.DQty > 0)
                                    {
                                        _locRmDQty = 0;
                                    }

                                    if (_locInvTransInLine.Qty > 0)
                                    {
                                        _locRmQty = 0;
                                    }

                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locInvTransInLine.Item),
                                                                new BinaryOperator("UOM", _locInvTransInLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locInvTransInLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0
                            #region ProcessCount>0
                            if (_locInvTransInLine.ProcessCount > 0)
                            {
                                if (_locInvTransInLine.RmDQty > 0)
                                {
                                    _locRmDQty = _locInvTransInLine.RmDQty - _locInvTransInLine.DQty;
                                }

                                if (_locInvTransInLine.RmQty > 0)
                                {
                                    _locRmQty = _locInvTransInLine.RmQty - _locInvTransInLine.Qty;
                                }

                                if (_locInvTransInLine.MxDQty > 0 || _locInvTransInLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locInvTransInLine.Item),
                                                                new BinaryOperator("UOM", _locInvTransInLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locInvTransInLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locInvTransInLine.Item),
                                                                new BinaryOperator("UOM", _locInvTransInLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locInvTransInLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locInvTransInLine.RmDQty = _locRmDQty;
                            _locInvTransInLine.RmQty = _locRmQty;
                            _locInvTransInLine.RmTQty = _locInvLineTotal;
                            _locInvTransInLine.Save();
                            _locInvTransInLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = TransferIn " + ex.ToString());
            }
        }

        //Menentukan jumlah quantity yang di posting -> Done 10 Dec 2019
        private void SetPostingReceivedQtySC(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO)
        {
            try
            {
                if (_locInventoryTransferInXPO != null)
                {
                    XPCollection<InventoryTransferInLine> _locInvTransInLines = new XPCollection<InventoryTransferInLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("TransferIn", _locInventoryTransferInXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locInvTransInLines != null && _locInvTransInLines.Count > 0)
                    {
                        double _locPDQty = 0;
                        double _locPQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (InventoryTransferInLine _locInvTransInLine in _locInvTransInLines)
                        {
                            #region ProcessCount=0
                            if (_locInvTransInLine.ProcessCount == 0)
                            {
                                #region MaxQuantity
                                if (_locInvTransInLine.MxDQty > 0)
                                {
                                    if (_locInvTransInLine.DQty > 0 && _locInvTransInLine.DQty <= _locInvTransInLine.MxDQty)
                                    {
                                        _locPDQty = _locInvTransInLine.DQty;
                                    }

                                    if (_locInvTransInLine.Qty > 0 && _locInvTransInLine.Qty <= _locInvTransInLine.MxQty)
                                    {
                                        _locPQty = _locInvTransInLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locInvTransInLine.Item),
                                                                new BinaryOperator("UOM", _locInvTransInLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locInvTransInLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locInvTransInLine.DQty > 0)
                                    {
                                        _locPDQty = _locInvTransInLine.DQty;
                                    }

                                    if (_locInvTransInLine.Qty > 0)
                                    {
                                        _locPQty = _locInvTransInLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locInvTransInLine.Item),
                                                                new BinaryOperator("UOM", _locInvTransInLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locInvTransInLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locInvTransInLine.ProcessCount > 0)
                            {
                                if (_locInvTransInLine.PDQty > 0)
                                {
                                    _locPDQty = _locInvTransInLine.PDQty + _locInvTransInLine.DQty;
                                }

                                if (_locInvTransInLine.PQty > 0)
                                {
                                    _locPQty = _locInvTransInLine.PQty + _locInvTransInLine.Qty;
                                }

                                if (_locInvTransInLine.MxDQty > 0 || _locInvTransInLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locInvTransInLine.Item),
                                                                new BinaryOperator("UOM", _locInvTransInLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locInvTransInLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locInvTransInLine.Item),
                                                                new BinaryOperator("UOM", _locInvTransInLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locInvTransInLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                }


                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locInvTransInLine.PDQty = _locPDQty;
                            _locInvTransInLine.PDUOM = _locInvTransInLine.DUOM;
                            _locInvTransInLine.PQty = _locPQty;
                            _locInvTransInLine.PUOM = _locInvTransInLine.UOM;
                            _locInvTransInLine.PTQty = _locInvLineTotal;
                            _locInvTransInLine.Save();
                            _locInvTransInLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferIn " + ex.ToString());
            }
        }

        //Menentukan Banyak process receive
        private void SetProcessCountReceiveSC(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO)
        {
            try
            {
                if (_locInventoryTransferInXPO != null)
                {
                    XPCollection<InventoryTransferInLine> _locInvTransInLines = new XPCollection<InventoryTransferInLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locInvTransInLines != null && _locInvTransInLines.Count > 0)
                    {

                        foreach (InventoryTransferInLine _locInvTransInLine in _locInvTransInLines)
                        {
                            if (_locInvTransInLine.Status == Status.Progress || _locInvTransInLine.Status == Status.Posted)
                            {
                                _locInvTransInLine.ProcessCount = _locInvTransInLine.ProcessCount + 1;
                                _locInvTransInLine.Save();
                                _locInvTransInLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferIn " + ex.ToString());
            }

        }

        //Menentukan Status Receive Pada Transfer In Line
        private void SetStatusReceiveInventoryTransferInLineSC(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locInventoryTransferInXPO != null)
                {
                    XPCollection<InventoryTransferInLine> _locInvTransInLines = new XPCollection<InventoryTransferInLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locInvTransInLines != null && _locInvTransInLines.Count > 0)
                    {

                        foreach (InventoryTransferInLine _locInvTransInLine in _locInvTransInLines)
                        {
                            if (_locInvTransInLine.Status == Status.Progress || _locInvTransInLine.Status == Status.Posted)
                            {
                                if (_locInvTransInLine.RmDQty == 0 && _locInvTransInLine.RmQty == 0 && _locInvTransInLine.RmTQty == 0)
                                {
                                    _locInvTransInLine.Status = Status.Close;
                                    _locInvTransInLine.ActivationPosting = true;
                                    _locInvTransInLine.StatusDate = now;
                                }
                                else
                                {
                                    _locInvTransInLine.Status = Status.Posted;
                                    _locInvTransInLine.StatusDate = now;
                                }

                                XPCollection<InventoryTransferInLot> _locInventoryTransferInLots = new XPCollection<InventoryTransferInLot>(_currSession,
                                                                                         new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("InventoryTransferInLine", _locInvTransInLine),
                                                                                         new BinaryOperator("Select", true)));

                                if (_locInventoryTransferInLots != null && _locInventoryTransferInLots.Count() > 0)
                                {
                                    foreach (InventoryTransferInLot _locInventoryTransferInLot in _locInventoryTransferInLots)
                                    {
                                        if (_locInventoryTransferInLot.Status == Status.Progress || _locInventoryTransferInLot.Status == Status.Posted)
                                        {
                                            _locInventoryTransferInLot.Status = Status.Close;
                                            _locInventoryTransferInLot.ActivationPosting = true;
                                            _locInventoryTransferInLot.StatusDate = now;
                                            _locInventoryTransferInLot.Save();
                                            _locInventoryTransferInLot.Session.CommitTransaction();
                                        }
                                    }
                                }

                                _locInvTransInLine.Save();
                                _locInvTransInLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferIn " + ex.ToString());
            }
        }

        //Menormalkan Quantity
        private void SetNormalQuantityReceiveSC(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO)
        {
            try
            {
                if (_locInventoryTransferInXPO != null)
                {
                    XPCollection<InventoryTransferInLine> _locInvTransInLines = new XPCollection<InventoryTransferInLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locInvTransInLines != null && _locInvTransInLines.Count > 0)
                    {

                        foreach (InventoryTransferInLine _locInvTransInLine in _locInvTransInLines)
                        {
                            if (_locInvTransInLine.Status == Status.Progress || _locInvTransInLine.Status == Status.Posted || _locInvTransInLine.Status == Status.Close)
                            {
                                if (_locInvTransInLine.DQty > 0 || _locInvTransInLine.Qty > 0)
                                {
                                    _locInvTransInLine.Select = false;
                                    _locInvTransInLine.DQty = 0;
                                    _locInvTransInLine.Qty = 0;
                                    _locInvTransInLine.Save();
                                    _locInvTransInLine.Session.CommitTransaction();
                                }

                                XPCollection<InventoryTransferInLot> _locInventoryTransferInLots = new XPCollection<InventoryTransferInLot>(_currSession,
                                                                                         new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("InventoryTransferInLine", _locInvTransInLine),
                                                                                         new BinaryOperator("Select", true)));

                                if (_locInventoryTransferInLots != null && _locInventoryTransferInLots.Count() > 0)
                                {
                                    foreach (InventoryTransferInLot _locInventoryTransferInLot in _locInventoryTransferInLots)
                                    {
                                        if (_locInventoryTransferInLot.Status == Status.Progress || _locInventoryTransferInLot.Status == Status.Posted)
                                        {
                                            _locInventoryTransferInLot.Select = false;
                                            _locInventoryTransferInLot.Save();
                                            _locInventoryTransferInLot.Session.CommitTransaction();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferIn " + ex.ToString());
            }
        }

        //Menentukan Status Receive Pada Transfer In
        private void SetFinalStatusReceiveInventoryTransferInSC(Session _currSession, InventoryTransferIn _locInventoryTransferInXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locCount = 0;
                if (_locInventoryTransferInXPO != null)
                {
                    XPCollection<InventoryTransferInLine> _locInvTransInLines = new XPCollection<InventoryTransferInLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("InventoryTransferIn", _locInventoryTransferInXPO)));

                    if (_locInvTransInLines != null && _locInvTransInLines.Count() > 0)
                    {

                        foreach (InventoryTransferInLine _locInvTransInLine in _locInvTransInLines)
                        {
                            if (_locInvTransInLine.Status == Status.Close)
                            {
                                _locCount = _locCount + 1;
                            }
                        }

                        if (_locCount == _locInvTransInLines.Count())
                        {
                            _locInventoryTransferInXPO.ActivationPosting = true;
                            _locInventoryTransferInXPO.Status = Status.Close;
                            _locInventoryTransferInXPO.StatusDate = now;
                            _locInventoryTransferInXPO.Save();
                            _locInventoryTransferInXPO.Session.CommitTransaction();
                        }
                        else
                        {
                            _locInventoryTransferInXPO.Status = Status.Posted;
                            _locInventoryTransferInXPO.StatusDate = now;
                            _locInventoryTransferInXPO.Save();
                            _locInventoryTransferInXPO.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferIn " + ex.ToString());
            }
        }

        #endregion InventoryReceiveWithStockControlling

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private int GetStockType(StockType objectName)
        {
            int _result = 0;
            try
            {
                if (objectName == StockType.Good)
                {
                    _result = 1;
                }
                else if (objectName == StockType.Bad)
                {
                    _result = 2;
                }
                else
                {
                    _result = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = InventoryTransferAction " + ex.ToString());
            }
            return _result;
        }

        private int GetActive(bool objectName)
        {
            int _result = 0;
            try
            {
                if (objectName == true)
                {
                    _result = 1;
                }
                else
                {
                    _result = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = InventoryTransferAction " + ex.ToString());
            }
            return _result;
        }

        private double GetUnitPriceByPurchaseOrder(Session _currSession, PurchaseOrder _locPurchaseOrder, Item _locItem)
        {
            double _result = 0;
            try
            {
                if (_locPurchaseOrder != null)
                {
                    PurchaseOrderLine _locPurchaseOrderLine = _currSession.FindObject<PurchaseOrderLine>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseOrder", _locPurchaseOrder),
                                                            new BinaryOperator("Item", _locItem)));
                    if (_locPurchaseOrderLine != null)
                    {
                        _result = _locPurchaseOrderLine.UAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransfer " + ex.ToString());
            }
            return _result;
        }

        #endregion Global Method

    }
}
