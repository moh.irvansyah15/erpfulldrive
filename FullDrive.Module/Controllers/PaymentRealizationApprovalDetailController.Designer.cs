﻿namespace FullDrive.Module.Controllers
{
    partial class PaymentRealizationApprovalDetailController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PaymentRealizationApprovalDetailAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // PaymentRealizationApprovalDetailAction
            // 
            this.PaymentRealizationApprovalDetailAction.Caption = "Approval";
            this.PaymentRealizationApprovalDetailAction.ConfirmationMessage = null;
            this.PaymentRealizationApprovalDetailAction.Id = "PaymentRealizationApprovalDetailActionId";
            this.PaymentRealizationApprovalDetailAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.PaymentRealizationApprovalDetailAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PaymentRealization);
            this.PaymentRealizationApprovalDetailAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.PaymentRealizationApprovalDetailAction.ToolTip = null;
            this.PaymentRealizationApprovalDetailAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.PaymentRealizationApprovalDetailAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.PaymentRealizationApprovalDetailAction_Execute);
            // 
            // PaymentRealizationApprovalDetailController
            // 
            this.Actions.Add(this.PaymentRealizationApprovalDetailAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction PaymentRealizationApprovalDetailAction;
    }
}
