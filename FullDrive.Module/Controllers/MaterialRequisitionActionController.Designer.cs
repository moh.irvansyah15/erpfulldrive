﻿namespace FullDrive.Module.Controllers
{
    partial class MaterialRequisitionActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.MaterialRequisitionProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.MaterialRequisitionListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.MaterialRequisitionPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.MaterialRequisitionListviewFilterApprovalSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // MaterialRequisitionProgressAction
            // 
            this.MaterialRequisitionProgressAction.Caption = "Progress";
            this.MaterialRequisitionProgressAction.ConfirmationMessage = null;
            this.MaterialRequisitionProgressAction.Id = "MaterialRequisitionProgressActionId";
            this.MaterialRequisitionProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.MaterialRequisition);
            this.MaterialRequisitionProgressAction.ToolTip = null;
            this.MaterialRequisitionProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.MaterialRequisitionProgressAction_Execute);
            // 
            // MaterialRequisitionListviewFilterSelectionAction
            // 
            this.MaterialRequisitionListviewFilterSelectionAction.Caption = "Filter";
            this.MaterialRequisitionListviewFilterSelectionAction.ConfirmationMessage = null;
            this.MaterialRequisitionListviewFilterSelectionAction.Id = "MaterialRequisitionListviewFilterSelectionActionId";
            this.MaterialRequisitionListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.MaterialRequisitionListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.MaterialRequisition);
            this.MaterialRequisitionListviewFilterSelectionAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.MaterialRequisitionListviewFilterSelectionAction.ToolTip = null;
            this.MaterialRequisitionListviewFilterSelectionAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.MaterialRequisitionListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.MaterialRequisitionListviewFilterSelectionAction_Execute);
            // 
            // MaterialRequisitionPostingAction
            // 
            this.MaterialRequisitionPostingAction.Caption = "Posting";
            this.MaterialRequisitionPostingAction.ConfirmationMessage = null;
            this.MaterialRequisitionPostingAction.Id = "MaterialRequisitionPostingActionId";
            this.MaterialRequisitionPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.MaterialRequisition);
            this.MaterialRequisitionPostingAction.ToolTip = null;
            this.MaterialRequisitionPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.MaterialRequisitionPostingAction_Execute);
            // 
            // MaterialRequisitionListviewFilterApprovalSelectionAction
            // 
            this.MaterialRequisitionListviewFilterApprovalSelectionAction.Caption = "Filter Approval";
            this.MaterialRequisitionListviewFilterApprovalSelectionAction.ConfirmationMessage = null;
            this.MaterialRequisitionListviewFilterApprovalSelectionAction.Id = "MaterialRequisitionListviewFilterApprovalSelectionActionId";
            this.MaterialRequisitionListviewFilterApprovalSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.MaterialRequisitionListviewFilterApprovalSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.MaterialRequisition);
            this.MaterialRequisitionListviewFilterApprovalSelectionAction.ToolTip = null;
            this.MaterialRequisitionListviewFilterApprovalSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.MaterialRequisitionListviewFilterApprovalSelectionAction_Execute);
            // 
            // MaterialRequisitionActionController
            // 
            this.Actions.Add(this.MaterialRequisitionProgressAction);
            this.Actions.Add(this.MaterialRequisitionListviewFilterSelectionAction);
            this.Actions.Add(this.MaterialRequisitionPostingAction);
            this.Actions.Add(this.MaterialRequisitionListviewFilterApprovalSelectionAction);
            this.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.MaterialRequisition);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction MaterialRequisitionProgressAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction MaterialRequisitionListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SimpleAction MaterialRequisitionPostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction MaterialRequisitionListviewFilterApprovalSelectionAction;
    }
}
