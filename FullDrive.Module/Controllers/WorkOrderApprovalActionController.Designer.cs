﻿namespace FullDrive.Module.Controllers
{
    partial class WorkOrderApprovalActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.WorkOrderApprovalAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // WorkOrderApprovalAction
            // 
            this.WorkOrderApprovalAction.Caption = "Approval";
            this.WorkOrderApprovalAction.ConfirmationMessage = null;
            this.WorkOrderApprovalAction.Id = "WorkOrderApprovalActionId";
            this.WorkOrderApprovalAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.WorkOrderApprovalAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.WorkOrder);
            this.WorkOrderApprovalAction.ToolTip = null;
            this.WorkOrderApprovalAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.WorkOrderApprovalAction_Execute);
            // 
            // WorkOrderApprovalActionController
            // 
            this.Actions.Add(this.WorkOrderApprovalAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction WorkOrderApprovalAction;
    }
}
