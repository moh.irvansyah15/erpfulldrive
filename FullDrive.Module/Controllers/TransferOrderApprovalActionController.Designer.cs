﻿namespace FullDrive.Module.Controllers
{
    partial class TransferOrderApprovalActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TransferOrderApprovalAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // TransferOrderApprovalAction
            // 
            this.TransferOrderApprovalAction.Caption = "Approval";
            this.TransferOrderApprovalAction.ConfirmationMessage = null;
            this.TransferOrderApprovalAction.Id = "TransferOrderApprovalActionId";
            this.TransferOrderApprovalAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.TransferOrderApprovalAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.TransferOrder);
            this.TransferOrderApprovalAction.ToolTip = null;
            this.TransferOrderApprovalAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.TransferOrderApprovalAction_Execute);
            // 
            // TransferOrderApprovalActionController
            // 
            this.Actions.Add(this.TransferOrderApprovalAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction TransferOrderApprovalAction;
    }
}
