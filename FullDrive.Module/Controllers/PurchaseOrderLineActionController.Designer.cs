﻿namespace FullDrive.Module.Controllers
{
    partial class PurchaseOrderLineActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PurchaseOrderLineTaxCalculationAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchaseOrderLineDiscountCalculationAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchaseOrderLineListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // PurchaseOrderLineTaxCalculationAction
            // 
            this.PurchaseOrderLineTaxCalculationAction.Caption = "Tax Calculation";
            this.PurchaseOrderLineTaxCalculationAction.ConfirmationMessage = null;
            this.PurchaseOrderLineTaxCalculationAction.Id = "PurchaseOrderLineTaxCalculationActionId";
            this.PurchaseOrderLineTaxCalculationAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseOrderLine);
            this.PurchaseOrderLineTaxCalculationAction.ToolTip = null;
            this.PurchaseOrderLineTaxCalculationAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseOrderLineTaxCalculationAction_Execute);
            // 
            // PurchaseOrderLineDiscountCalculationAction
            // 
            this.PurchaseOrderLineDiscountCalculationAction.Caption = "Discount Calculation";
            this.PurchaseOrderLineDiscountCalculationAction.ConfirmationMessage = null;
            this.PurchaseOrderLineDiscountCalculationAction.Id = "PurchaseOrderLineDiscountCalculationActionId";
            this.PurchaseOrderLineDiscountCalculationAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseOrderLine);
            this.PurchaseOrderLineDiscountCalculationAction.ToolTip = null;
            this.PurchaseOrderLineDiscountCalculationAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseOrderLineDiscountCalculationAction_Execute);
            // 
            // PurchaseOrderLineListviewFilterSelectionAction
            // 
            this.PurchaseOrderLineListviewFilterSelectionAction.Caption = "Filter";
            this.PurchaseOrderLineListviewFilterSelectionAction.ConfirmationMessage = null;
            this.PurchaseOrderLineListviewFilterSelectionAction.Id = "PurchaseOrderLineListviewFilterSelectionActionId";
            this.PurchaseOrderLineListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.PurchaseOrderLineListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseOrderLine);
            this.PurchaseOrderLineListviewFilterSelectionAction.ToolTip = null;
            this.PurchaseOrderLineListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.PurchaseOrderLineListviewFilterSelectionAction_Execute);
            // 
            // PurchaseOrderLineActionController
            // 
            this.Actions.Add(this.PurchaseOrderLineTaxCalculationAction);
            this.Actions.Add(this.PurchaseOrderLineDiscountCalculationAction);
            this.Actions.Add(this.PurchaseOrderLineListviewFilterSelectionAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseOrderLineTaxCalculationAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseOrderLineDiscountCalculationAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction PurchaseOrderLineListviewFilterSelectionAction;
    }
}
