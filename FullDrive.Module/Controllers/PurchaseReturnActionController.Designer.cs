﻿namespace FullDrive.Module.Controllers
{
    partial class PurchaseReturnActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PurchaseReturnProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchaseReturnPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchaseReturnListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // PurchaseReturnProgressAction
            // 
            this.PurchaseReturnProgressAction.Caption = "Progress";
            this.PurchaseReturnProgressAction.ConfirmationMessage = null;
            this.PurchaseReturnProgressAction.Id = "PurchaseReturnProgressActionId";
            this.PurchaseReturnProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseReturn);
            this.PurchaseReturnProgressAction.ToolTip = null;
            this.PurchaseReturnProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseReturnProgressAction_Execute);
            // 
            // PurchaseReturnPostingAction
            // 
            this.PurchaseReturnPostingAction.Caption = "Posting";
            this.PurchaseReturnPostingAction.ConfirmationMessage = null;
            this.PurchaseReturnPostingAction.Id = "PurchaseReturnPostingActionId";
            this.PurchaseReturnPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseReturn);
            this.PurchaseReturnPostingAction.ToolTip = null;
            this.PurchaseReturnPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseReturnPostingAction_Execute);
            // 
            // PurchaseReturnListviewFilterSelectionAction
            // 
            this.PurchaseReturnListviewFilterSelectionAction.Caption = "Filter";
            this.PurchaseReturnListviewFilterSelectionAction.ConfirmationMessage = null;
            this.PurchaseReturnListviewFilterSelectionAction.Id = "PurchaseReturnListviewFilterSelectionActionId";
            this.PurchaseReturnListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.PurchaseReturnListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseReturn);
            this.PurchaseReturnListviewFilterSelectionAction.ToolTip = null;
            this.PurchaseReturnListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.PurchaseReturnListviewFilterSelectionAction_Execute);
            // 
            // PurchaseReturnActionController
            // 
            this.Actions.Add(this.PurchaseReturnProgressAction);
            this.Actions.Add(this.PurchaseReturnPostingAction);
            this.Actions.Add(this.PurchaseReturnListviewFilterSelectionAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseReturnProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseReturnPostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction PurchaseReturnListviewFilterSelectionAction;
    }
}
