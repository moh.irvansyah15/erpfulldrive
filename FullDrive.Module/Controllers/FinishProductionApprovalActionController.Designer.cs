﻿namespace FullDrive.Module.Controllers
{
    partial class FinishProductionApprovalActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.FinishProductionApprovalAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // FinishProductionApprovalAction
            // 
            this.FinishProductionApprovalAction.Caption = "Approval";
            this.FinishProductionApprovalAction.ConfirmationMessage = null;
            this.FinishProductionApprovalAction.Id = "FinishProductionApprovalActionId";
            this.FinishProductionApprovalAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.FinishProductionApprovalAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.FinishProduction);
            this.FinishProductionApprovalAction.ToolTip = null;
            this.FinishProductionApprovalAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.FinishProductionApprovalAction_Execute);
            // 
            // FinishProductionApprovalActionController
            // 
            this.Actions.Add(this.FinishProductionApprovalAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction FinishProductionApprovalAction;
    }
}
