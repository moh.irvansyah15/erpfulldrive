﻿namespace FullDrive.Module.Controllers
{
    partial class InvoiceSalesCollectionActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.InvoiceSalesCollectionShowITOMAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // InvoiceSalesCollectionShowITOMAction
            // 
            this.InvoiceSalesCollectionShowITOMAction.Caption = "Show ITOM";
            this.InvoiceSalesCollectionShowITOMAction.ConfirmationMessage = null;
            this.InvoiceSalesCollectionShowITOMAction.Id = "InvoiceSalesCollectionShowITOMActionId";
            this.InvoiceSalesCollectionShowITOMAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InvoiceSalesCollection);
            this.InvoiceSalesCollectionShowITOMAction.ToolTip = null;
            this.InvoiceSalesCollectionShowITOMAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InvoiceSalesCollectionShowITOMAction_Execute);
            // 
            // InvoiceSalesCollectionActionController
            // 
            this.Actions.Add(this.InvoiceSalesCollectionShowITOMAction);

        }

        #endregion
        private DevExpress.ExpressApp.Actions.SimpleAction InvoiceSalesCollectionShowITOMAction;
    }
}
