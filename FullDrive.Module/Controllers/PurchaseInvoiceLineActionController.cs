﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PurchaseInvoiceLineActionController : ViewController
    {
        #region Default

        private ChoiceActionItem _selectionListviewFilter;
        public PurchaseInvoiceLineActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.

            #region FilterStatus
            PurchaseInvoiceLineListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                if (_ed.GetCaption(_currApproval) != "Approved" && _ed.GetCaption(_currApproval) != "Lock")
                {
                    _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                }
                PurchaseInvoiceLineListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterStatus
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        private void PurchaseInvoiceLineTaxCalculationAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseInvoiceLine _locPurchaseInvoiceLineOS = (PurchaseInvoiceLine)_objectSpace.GetObject(obj);

                        if (_locPurchaseInvoiceLineOS != null)
                        {
                            if (_locPurchaseInvoiceLineOS.TQty != 0 && _locPurchaseInvoiceLineOS.UAmount != 0)
                            {
                                if (_locPurchaseInvoiceLineOS.Code != null)
                                {
                                    _currObjectId = _locPurchaseInvoiceLineOS.Code;

                                    PurchaseInvoiceLine _locPurchaseInvoiceLineXPO = _currSession.FindObject<PurchaseInvoiceLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Code", _currObjectId)));

                                    if (_locPurchaseInvoiceLineXPO != null)
                                    {
                                        GetSumTotalTax(_currSession, _locPurchaseInvoiceLineXPO);
                                    }
                                }
                            }
                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseInvoiceLine" + ex.ToString());
            }
        }

        private void PurchaseInvoiceLineDiscountCalculationAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseInvoiceLine _locPurchaseInvoiceLineOS = (PurchaseInvoiceLine)_objectSpace.GetObject(obj);

                        if (_locPurchaseInvoiceLineOS != null)
                        {
                            if (_locPurchaseInvoiceLineOS.TQty != 0 && _locPurchaseInvoiceLineOS.UAmount != 0)
                            {
                                if (_locPurchaseInvoiceLineOS.Code != null)
                                {
                                    _currObjectId = _locPurchaseInvoiceLineOS.Code;

                                    PurchaseInvoiceLine _locPurchaseInvoiceLineXPO = _currSession.FindObject<PurchaseInvoiceLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Code", _currObjectId)));

                                    if (_locPurchaseInvoiceLineXPO != null)
                                    {
                                        GetSumTotalDisc(_currSession, _locPurchaseInvoiceLineXPO);
                                    }
                                }
                            }
                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseInvoiceLine" + ex.ToString());
            }
        }

        private void PurchaseInvoiceLineListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(PurchaseInvoiceLine)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoiceLine " + ex.ToString());
            }
        }

        //==== Code Only =====

        #region Method

        private void GetSumTotalTax(Session _currSession, PurchaseInvoiceLine _locPurchaseInvoiceLineXPO)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                DateTime now = DateTime.Now;
                double _locTxAmount = 0;
                double _locTxValue = 0;
                if (_locPurchaseInvoiceLineXPO != null)
                {
                    XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession,
                                                     new BinaryOperator("PurchaseInvoiceLine", _locPurchaseInvoiceLineXPO));

                    if (_locTaxLines != null && _locTaxLines.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLine in _locTaxLines)
                        {
                            if (_locTaxLine.TxValue > 0 && _locTaxLine.TxAmount > 0)
                            {
                                if (_locTaxLine.TaxNature == TaxNature.Increase)
                                {
                                    _locTxAmount = _locTxAmount + _locTaxLine.TxAmount;
                                    _locTxValue = _locTxValue + _locTaxLine.TxValue;

                                }
                                else if (_locTaxLine.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxAmount = _locTxAmount - _locTaxLine.TxAmount;
                                    _locTxValue = _locTxValue - _locTaxLine.TxValue;
                                }
                            }
                        }
                        _locPurchaseInvoiceLineXPO.TxValue = _locTxValue;
                        _locPurchaseInvoiceLineXPO.TxAmount = _locTxAmount;
                        _locPurchaseInvoiceLineXPO.Save();
                        _locPurchaseInvoiceLineXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseInvoiceLine" + ex.ToString());
            }
        }

        private void GetSumTotalDisc(Session _currSession, PurchaseInvoiceLine _locPurchaseInvoiceLineXPO)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                DateTime now = DateTime.Now;
                double _locDisc = 0;
                double _locDiscAmount = 0;

                XPCollection<DiscountLine> _locDiscountLines = new XPCollection<DiscountLine>(_currSession,
                                                               new BinaryOperator("PurchaseInvoiceLine", _locPurchaseInvoiceLineXPO));

                if (_locDiscountLines != null && _locDiscountLines.Count() > 0)
                {
                    foreach (DiscountLine _locDiscountLine in _locDiscountLines)
                    {
                        if (_locDiscountLine.Disc > 0 && _locDiscountLine.DiscAmount > 0)
                        {
                            _locDisc = _locDisc + _locDiscountLine.Disc;
                            _locDiscAmount = _locDiscAmount + _locDiscountLine.DiscAmount;
                        }
                    }

                    _locPurchaseInvoiceLineXPO.Disc = _locDisc;
                    _locPurchaseInvoiceLineXPO.DiscAmount = _locDiscAmount;
                    _locPurchaseInvoiceLineXPO.Save();
                    _locPurchaseInvoiceLineXPO.Session.CommitTransaction();
                }



            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseInvoiceLine" + ex.ToString());
            }
        }

        #endregion Method
        
    }
}
