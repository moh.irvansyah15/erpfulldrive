﻿namespace FullDrive.Module.Controllers
{
    partial class CashAdvanceActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CashAdvanceProgressActionController = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.CashAdvanceListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.CashAdvancePostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.CashAdvanceListviewFilterApprovalSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // CashAdvanceProgressActionController
            // 
            this.CashAdvanceProgressActionController.Caption = "Progress";
            this.CashAdvanceProgressActionController.ConfirmationMessage = null;
            this.CashAdvanceProgressActionController.Id = "CashAdvanceProgressActionControllerId";
            this.CashAdvanceProgressActionController.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.CashAdvance);
            this.CashAdvanceProgressActionController.ToolTip = null;
            this.CashAdvanceProgressActionController.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CashAdvanceProgressActionController_Execute);
            // 
            // CashAdvanceListviewFilterSelectionAction
            // 
            this.CashAdvanceListviewFilterSelectionAction.Caption = "Filter";
            this.CashAdvanceListviewFilterSelectionAction.ConfirmationMessage = null;
            this.CashAdvanceListviewFilterSelectionAction.Id = "CashAdvanceListviewFilterSelectionActionId";
            this.CashAdvanceListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.CashAdvanceListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.CashAdvance);
            this.CashAdvanceListviewFilterSelectionAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.CashAdvanceListviewFilterSelectionAction.ToolTip = null;
            this.CashAdvanceListviewFilterSelectionAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.CashAdvanceListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.CashAdvanceListviewFilterSelectionAction_Execute);
            // 
            // CashAdvancePostingAction
            // 
            this.CashAdvancePostingAction.Caption = "Posting";
            this.CashAdvancePostingAction.ConfirmationMessage = null;
            this.CashAdvancePostingAction.Id = "CashAdvancePostingActionId";
            this.CashAdvancePostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.CashAdvance);
            this.CashAdvancePostingAction.ToolTip = null;
            this.CashAdvancePostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CashAdvancePostingAction_Execute);
            // 
            // CashAdvanceListviewFilterApprovalSelectionAction
            // 
            this.CashAdvanceListviewFilterApprovalSelectionAction.Caption = "Filter Approval";
            this.CashAdvanceListviewFilterApprovalSelectionAction.ConfirmationMessage = null;
            this.CashAdvanceListviewFilterApprovalSelectionAction.Id = "CashAdvanceListviewFilterApprovalSelectionActionId";
            this.CashAdvanceListviewFilterApprovalSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.CashAdvanceListviewFilterApprovalSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.CashAdvance);
            this.CashAdvanceListviewFilterApprovalSelectionAction.ToolTip = null;
            this.CashAdvanceListviewFilterApprovalSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.CashAdvanceListviewFilterApprovalSelectionAction_Execute);
            // 
            // CashAdvanceActionController
            // 
            this.Actions.Add(this.CashAdvanceProgressActionController);
            this.Actions.Add(this.CashAdvanceListviewFilterSelectionAction);
            this.Actions.Add(this.CashAdvancePostingAction);
            this.Actions.Add(this.CashAdvanceListviewFilterApprovalSelectionAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction CashAdvanceProgressActionController;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction CashAdvanceListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SimpleAction CashAdvancePostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction CashAdvanceListviewFilterApprovalSelectionAction;
    }
}
