﻿namespace FullDrive.Module.Controllers
{
    partial class PurchasePrePaymentActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PurchasePrePaymentInvoiceProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchasePrePaymentInvoicePostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchasePrePaymentInvoiceListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // PurchasePrePaymentInvoiceProgressAction
            // 
            this.PurchasePrePaymentInvoiceProgressAction.Caption = "Progress";
            this.PurchasePrePaymentInvoiceProgressAction.ConfirmationMessage = null;
            this.PurchasePrePaymentInvoiceProgressAction.Id = "PurchasePrePaymentInvoiceProgressActionId";
            this.PurchasePrePaymentInvoiceProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchasePrePaymentInvoice);
            this.PurchasePrePaymentInvoiceProgressAction.ToolTip = null;
            this.PurchasePrePaymentInvoiceProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchasePrePaymentInvoiceProgressAction_Execute);
            // 
            // PurchasePrePaymentInvoicePostingAction
            // 
            this.PurchasePrePaymentInvoicePostingAction.Caption = "Posting";
            this.PurchasePrePaymentInvoicePostingAction.ConfirmationMessage = null;
            this.PurchasePrePaymentInvoicePostingAction.Id = "PurchasePrePaymentInvoicePostingActionId";
            this.PurchasePrePaymentInvoicePostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchasePrePaymentInvoice);
            this.PurchasePrePaymentInvoicePostingAction.ToolTip = null;
            this.PurchasePrePaymentInvoicePostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchasePrePaymentInvoicePostingAction_Execute);
            // 
            // PurchasePrePaymentInvoiceListviewFilterSelectionAction
            // 
            this.PurchasePrePaymentInvoiceListviewFilterSelectionAction.Caption = "Filter";
            this.PurchasePrePaymentInvoiceListviewFilterSelectionAction.ConfirmationMessage = null;
            this.PurchasePrePaymentInvoiceListviewFilterSelectionAction.Id = "PurchasePrePaymentInvoiceListviewFilterSelectionActionId";
            this.PurchasePrePaymentInvoiceListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.PurchasePrePaymentInvoiceListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchasePrePaymentInvoice);
            this.PurchasePrePaymentInvoiceListviewFilterSelectionAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.PurchasePrePaymentInvoiceListviewFilterSelectionAction.ToolTip = null;
            this.PurchasePrePaymentInvoiceListviewFilterSelectionAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.PurchasePrePaymentInvoiceListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.PurchasePrePaymentInvoiceListviewFilterSelectionAction_Execute);
            // 
            // PurchasePrePaymentActionController
            // 
            this.Actions.Add(this.PurchasePrePaymentInvoiceProgressAction);
            this.Actions.Add(this.PurchasePrePaymentInvoicePostingAction);
            this.Actions.Add(this.PurchasePrePaymentInvoiceListviewFilterSelectionAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction PurchasePrePaymentInvoiceProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PurchasePrePaymentInvoicePostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction PurchasePrePaymentInvoiceListviewFilterSelectionAction;
    }
}
