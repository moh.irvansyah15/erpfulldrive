﻿namespace FullDrive.Module.Controllers
{
    partial class FinishProductionApprovalDetailController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.FinishProductionApprovalDetail = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // FinishProductionApprovalDetail
            // 
            this.FinishProductionApprovalDetail.Caption = "Approval";
            this.FinishProductionApprovalDetail.ConfirmationMessage = null;
            this.FinishProductionApprovalDetail.Id = "FinishProductionApprovalDetailId";
            this.FinishProductionApprovalDetail.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.FinishProductionApprovalDetail.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.FinishProductionApprovalDetail.ToolTip = null;
            this.FinishProductionApprovalDetail.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.FinishProductionApprovalDetail.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.FinishProductionApprovalDetail_Execute);
            // 
            // FinishProductionApprovalDetailController
            // 
            this.Actions.Add(this.FinishProductionApprovalDetail);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction FinishProductionApprovalDetail;
    }
}
