﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;
using System.Web;
using System.IO;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class SalesInvoiceActionController : ViewController
    {
        #region Default

        private ChoiceActionItem _selectionlistViewFilter;
        private ChoiceActionItem _setApprovalLevel;

        public SalesInvoiceActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region FilterApproval
            SalesInvoiceListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionlistViewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                SalesInvoiceListviewFilterSelectionAction.Items.Add(_selectionlistViewFilter);
            }
            #endregion FilterApproval
        }
        protected override void OnActivated()
        {
            TypeOfView = typeof(ObjectView);
            base.OnActivated();
            // Perform various tasks depending on the target View.
            #region Approval
            var _locUser = SecuritySystem.CurrentUserName;
            GlobalFunction _globalFunction = new GlobalFunction();
            Session _currentSession = null;
            if (this.ObjectSpace != null)
            {
                _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
            }

            if (_currentSession != null)
            {
                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", _locUser));

                if (_locUserAccess != null)
                {
                    SalesInvoiceApprovalAction.Items.Clear();

                    XPCollection<ApplicationSetupDetail> _locAppSetupDetails = new XPCollection<ApplicationSetupDetail>
                                                                                 (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("UserAccess", _locUserAccess),
                                                                                  new BinaryOperator("ObjectList", CustomProcess.ObjectList.SalesInvoice),
                                                                                  new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                  new BinaryOperator("Active", true)));

                    if (_locAppSetupDetails != null && _locAppSetupDetails.Count > 0)
                    {

                        foreach (ApplicationSetupDetail _locAppSetupDetail in _locAppSetupDetails)
                        {
                            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.ApprovalLevel)))
                            {
                                if (_locAppSetupDetail.ApprovalLevel != CustomProcess.ApprovalLevel.None)
                                {
                                    EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalLevel));
                                    _setApprovalLevel = new ChoiceActionItem(_ed.GetCaption(_locAppSetupDetail.ApprovalLevel), _locAppSetupDetail.ApprovalLevel);
                                }
                            }
                            SalesInvoiceApprovalAction.Items.Add(_setApprovalLevel);
                        }
                    }
                }
            }
            #endregion Approval

        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        private void SalesInvoiceProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Status _locStatus = Status.Open;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        SalesInvoice _locSalesInvoiceOS = (SalesInvoice)_objectSpace.GetObject(obj);

                        if (_locSalesInvoiceOS != null)
                        {
                            if (_locSalesInvoiceOS.Code != null)
                            {
                                _currObjectId = _locSalesInvoiceOS.Code;

                                SalesInvoice _locSalesInvoiceXPO = _currSession.FindObject<SalesInvoice>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)));

                                if (_locSalesInvoiceXPO != null)
                                {
                                    if (_locSalesInvoiceXPO.Status == Status.Open || _locSalesInvoiceXPO.Status == Status.Progress || _locSalesInvoiceXPO.Status == Status.Posted)
                                    {
                                        if (_locSalesInvoiceXPO.Status == Status.Open || _locSalesInvoiceXPO.Status == Status.Progress)
                                        {
                                            _locStatus = Status.Progress;

                                        }
                                        else if (_locSalesInvoiceXPO.Status == Status.Posted)
                                        {
                                            _locStatus = Status.Posted;
                                        }

                                        _locSalesInvoiceXPO.Status = _locStatus;
                                        _locSalesInvoiceXPO.StatusDate = now;
                                        _locSalesInvoiceXPO.Save();
                                        _locSalesInvoiceXPO.Session.CommitTransaction();

                                        #region SalesInvoiceLine
                                        XPCollection<SalesInvoiceLine> _locSalesInvoiceLines = new XPCollection<SalesInvoiceLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO)));

                                        if (_locSalesInvoiceLines != null && _locSalesInvoiceLines.Count > 0)
                                        {
                                            foreach (SalesInvoiceLine _locSalesInvoiceLine in _locSalesInvoiceLines)
                                            {
                                                if (_locSalesInvoiceLine.Status == Status.Open)
                                                {
                                                    _locSalesInvoiceLine.Status = Status.Progress;
                                                    _locSalesInvoiceLine.StatusDate = now;
                                                    _locSalesInvoiceLine.Save();
                                                    _locSalesInvoiceLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        #endregion SalesInvoiceLine
                                        #region InvoiceSalesCollection
                                        {
                                            XPCollection<InvoiceSalesCollection> _locInvoiceSalesCollections = new XPCollection<BusinessObjects.InvoiceSalesCollection>
                                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO)));
                                            if(_locInvoiceSalesCollections != null && _locInvoiceSalesCollections.Count() > 0)
                                            {
                                                foreach(InvoiceSalesCollection _locInvoiceSalesCollection in _locInvoiceSalesCollections)
                                                {
                                                    _locInvoiceSalesCollection.Status = Status.Progress;
                                                    _locInvoiceSalesCollection.StatusDate = now;
                                                    _locInvoiceSalesCollection.Save();
                                                    _locInvoiceSalesCollection.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        #endregion InvoiceSalesCollection
                                        SuccessMessageShow(_locSalesInvoiceXPO.Code + " has been change successfully to Progress ");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Sales Invoice Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Sales Invoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoice " + ex.ToString());
            }
        }

        private void SalesInvoicePostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                //Mengirim informasi ke Receivable Transaction --HutangDagang Dan --BankAccount Perusahaan yg aktif default
                //Meng-akumulasi semua hal perhitungan lokal Sales Invoice termasuk close status

                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        SalesInvoice _locSalesInvoiceOS = (SalesInvoice)_objectSpace.GetObject(obj);

                        if (_locSalesInvoiceOS != null)
                        {
                            if (_locSalesInvoiceOS.Code != null)
                            {
                                _currObjectId = _locSalesInvoiceOS.Code;

                                SalesInvoice _locSalesInvoiceXPO = _currSession.FindObject<SalesInvoice>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)));

                                if (_locSalesInvoiceXPO != null)
                                {
                                    if (_locSalesInvoiceXPO.Status == Status.Progress || _locSalesInvoiceXPO.Status == Status.Posted)
                                    {
                                        ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("EndApproval", true),
                                                                            new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO)));

                                        if (_locApprovalLineXPO != null)
                                        {
                                            if (_locSalesInvoiceXPO.Bill > 0)
                                            {
                                                //Berdasarkan Outstanding dari PaymentInPlan yg mana di hitung berdasarkan jumlah mak Outstanding 
                                                if(_locSalesInvoiceXPO.Bill == GetSalesInvoiceLineTotalAmount(_currSession, _locSalesInvoiceXPO))
                                                {
                                                    SetSalesInvoiceMonitoring(_currSession, _locSalesInvoiceXPO);
                                                    SetReverseGoodsDeliverJournal(_currSession, _locSalesInvoiceXPO);
                                                    SetInvoiceARJournal(_currSession, _locSalesInvoiceXPO);
                                                    SetAfterPostingReport(_currSession, _locSalesInvoiceXPO);
                                                    SetRemainQty(_currSession, _locSalesInvoiceXPO);
                                                    SetPostingQty(_currSession, _locSalesInvoiceXPO);
                                                    SetProcessCount(_currSession, _locSalesInvoiceXPO);
                                                    SetStatusSalesInvoiceLine(_currSession, _locSalesInvoiceXPO);
                                                    SetNormalBill(_currSession, _locSalesInvoiceXPO);
                                                    SetNormalQuantity(_currSession, _locSalesInvoiceXPO);
                                                    SetFinalSalesInvoice(_currSession, _locSalesInvoiceXPO);
                                                    SuccessMessageShow(_locSalesInvoiceXPO.Code + " has been change successfully to Posted");
                                                }
                                            }
                                        }      
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data SalesInvoice Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data SalesInvoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoice " + ex.ToString());
            }
        }

        private void SalesInvoiceGetBillAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                double _locMaxBill = 0;
                double _locTotalBill = 0;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        SalesInvoice _locSalesInvoiceOS = (SalesInvoice)_objectSpace.GetObject(obj);

                        if (_locSalesInvoiceOS != null)
                        {
                            if (_locSalesInvoiceOS.Code != null)
                            {
                                _currObjectId = _locSalesInvoiceOS.Code;

                                SalesInvoice _locSalesInvoiceXPO = _currSession.FindObject<SalesInvoice>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)));

                                if (_locSalesInvoiceXPO != null)
                                {
                                    _locMaxBill = _locSalesInvoiceXPO.Outstanding;

                                    if (_locSalesInvoiceXPO.Status == Status.Progress || _locSalesInvoiceXPO.Status == Status.Posted)
                                    {
                                        ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("EndApproval", true),
                                                                            new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO)));

                                        if (_locApprovalLineXPO == null)
                                        {
                                            GetBill(_currSession, _locSalesInvoiceXPO);
                                            
                                            XPCollection<SalesInvoiceLine> _locSalesInvoiceLines = new XPCollection<SalesInvoiceLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                                                                new BinaryOperator("Select", true)));

                                            if (_locSalesInvoiceLines != null && _locSalesInvoiceLines.Count > 0)
                                            {
                                                foreach (SalesInvoiceLine _locSalesInvoiceLine in _locSalesInvoiceLines)
                                                {
                                                    if (_locSalesInvoiceLine.Status == Status.Lock )
                                                    {
                                                        _locTotalBill = _locTotalBill + _locSalesInvoiceLine.Bill;
                                                    }
                                                }
                                            }
                                        }
                                        
                                        SuccessMessageShow(_locSalesInvoiceXPO.Code + "Has been change successfully to Get Max Bill");
                                    }



                                    _locSalesInvoiceXPO.MaxBill = _locTotalBill;
                                    _locSalesInvoiceXPO.Bill = _locTotalBill;
                                    _locSalesInvoiceXPO.Save();
                                    _locSalesInvoiceXPO.Session.CommitTransaction();
                                }
                                else
                                {
                                    ErrorMessageShow("Data Sales Invoice Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data SalesInvoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoice " + ex.ToString());
            }
        }

        private void SalesInvoiceDMCalculationAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        SalesInvoice _locSalesInvoiceOS = (SalesInvoice)_objectSpace.GetObject(obj);

                        if (_locSalesInvoiceOS != null)
                        {
                            if (_locSalesInvoiceOS.Code != null)
                            {
                                _currObjectId = _locSalesInvoiceOS.Code;

                                SalesInvoice _locSalesInvoiceXPO = _currSession.FindObject<SalesInvoice>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)));

                                if (_locSalesInvoiceXPO != null)
                                {
                                    GetSumTotalDM(_currSession, _locSalesInvoiceOS);
                                }
                            }
                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesInvoice" + ex.ToString());
            }
        }

        private void SalesInvoiceListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(SalesInvoice)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoice " + ex.ToString());
            }
        }

        private void SalesInvoiceGetITOAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        SalesInvoice _locSalesInvoiceOS = (SalesInvoice)_objectSpace.GetObject(obj);

                        if (_locSalesInvoiceOS != null)
                        {
                            if (_locSalesInvoiceOS.Code != null)
                            {
                                _currObjectId = _locSalesInvoiceOS.Code;

                                SalesInvoice _locSalesInvoiceXPO = _currSession.FindObject<SalesInvoice>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locSalesInvoiceXPO != null)
                                {
                                    if (_locSalesInvoiceXPO.Status == Status.Open || _locSalesInvoiceXPO.Status == Status.Progress)
                                    {
                                        XPCollection<InvoiceSalesCollection> _locInvoiceSalesCollections = new XPCollection<InvoiceSalesCollection>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Progress))));

                                        if (_locInvoiceSalesCollections != null && _locInvoiceSalesCollections.Count() > 0)
                                        {
                                            foreach (InvoiceSalesCollection _locInvoiceSalesCollection in _locInvoiceSalesCollections)
                                            {
                                                if (_locInvoiceSalesCollection.SalesInvoice != null && _locInvoiceSalesCollection.InventoryTransferOut != null)
                                                {
                                                    GetInventoryTransferOutMonitoring(_currSession, _locInvoiceSalesCollection.InventoryTransferOut, _locSalesInvoiceXPO);
                                                }
                                            }
                                            SuccessMessageShow("InventoryTransferOut Has Been Successfully Getting into SalesInvoice");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("InventoryTransferOut Order Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("InventoryTransferOut Order Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoice " + ex.ToString());
            }
        }

        private void SalesInvoiceApprovalAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                ApplicationSetupDetail _locAppSetDetail = null;
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                var User = SecuritySystem.CurrentUserName;
                Session _currentSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));

                foreach (Object obj in _objectsToProcess)
                {
                    SalesInvoice _objInNewObjectSpace = (SalesInvoice)_objectSpace.GetObject(obj);

                    if (_objInNewObjectSpace != null)
                    {
                        if (_objInNewObjectSpace.Code != null)
                        {
                            _currObjectId = _objInNewObjectSpace.Code;
                        }
                    }

                    if (_currObjectId != null)
                    {
                        SalesInvoice _locSalesInvoiceXPO = _currentSession.FindObject<SalesInvoice>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId)));

                        if (_locSalesInvoiceXPO != null)
                        {
                            if (_locSalesInvoiceXPO.Status == Status.Progress)
                            {
                                ApprovalLine _locApprovalLine = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                                             new BinaryOperator("EndApproval", true)));
                                if (_locApprovalLine == null)
                                {
                                    #region Approval Level 1
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level1)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level1, ObjectList.SalesInvoice);
                                        if (_locAppSetDetail != null)
                                        {
                                            //Buat bs input langsung ke approvalline
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level1)));
                                            if (_locApprovalLineXPO == null)
                                            {

                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        EndApproval = true,
                                                        SalesInvoice = _locSalesInvoiceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locSalesInvoiceXPO.ActivationPosting = true;
                                                    _locSalesInvoiceXPO.ActiveApproved1 = false;
                                                    _locSalesInvoiceXPO.ActiveApproved2 = false;
                                                    _locSalesInvoiceXPO.ActiveApproved3 = true;
                                                    _locSalesInvoiceXPO.Save();
                                                    _locSalesInvoiceXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        SalesInvoice = _locSalesInvoiceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locSalesInvoiceXPO.ActiveApproved1 = true;
                                                    _locSalesInvoiceXPO.ActiveApproved2 = false;
                                                    _locSalesInvoiceXPO.ActiveApproved3 = false;
                                                    _locSalesInvoiceXPO.Save();
                                                    _locSalesInvoiceXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locSalesInvoiceXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("SalesInvoice has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 1

                                    #region Approval Level 2
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level2)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level2, ObjectList.SalesInvoice);

                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level2)));

                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        EndApproval = true,
                                                        SalesInvoice = _locSalesInvoiceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locSalesInvoiceXPO.ActivationPosting = true;
                                                    _locSalesInvoiceXPO.ActiveApproved1 = false;
                                                    _locSalesInvoiceXPO.ActiveApproved2 = false;
                                                    _locSalesInvoiceXPO.ActiveApproved3 = true;
                                                    _locSalesInvoiceXPO.Save();
                                                    _locSalesInvoiceXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        SalesInvoice = _locSalesInvoiceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locSalesInvoiceXPO, ApprovalLevel.Level1);

                                                    _locSalesInvoiceXPO.ActiveApproved1 = false;
                                                    _locSalesInvoiceXPO.ActiveApproved2 = true;
                                                    _locSalesInvoiceXPO.ActiveApproved3 = false;
                                                    _locSalesInvoiceXPO.Save();
                                                    _locSalesInvoiceXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locSalesInvoiceXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("SalesInvoice has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 2

                                    #region Approval Level 3
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level3)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level3, ObjectList.SalesInvoice);

                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level3)));

                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        EndApproval = true,
                                                        SalesInvoice = _locSalesInvoiceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locSalesInvoiceXPO.ActivationPosting = true;
                                                    _locSalesInvoiceXPO.ActiveApproved1 = false;
                                                    _locSalesInvoiceXPO.ActiveApproved2 = false;
                                                    _locSalesInvoiceXPO.ActiveApproved3 = true;
                                                    _locSalesInvoiceXPO.Save();
                                                    _locSalesInvoiceXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        SalesInvoice = _locSalesInvoiceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locSalesInvoiceXPO, ApprovalLevel.Level2);
                                                    SetApprovalLine(_currentSession, _locSalesInvoiceXPO, ApprovalLevel.Level1);

                                                    _locSalesInvoiceXPO.ActiveApproved1 = false;
                                                    _locSalesInvoiceXPO.ActiveApproved2 = false;
                                                    _locSalesInvoiceXPO.ActiveApproved3 = true;
                                                    _locSalesInvoiceXPO.Save();
                                                    _locSalesInvoiceXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO)));

                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locSalesInvoiceXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("SalesInvoice has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 3
                                }

                            }
                            else
                            {
                                ErrorMessageShow("SalesInvoice Status Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("SalesInvoice Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesOrder " + ex.ToString());
            }
        }

        //======================================== Code Only ==============================================

        #region CLM

        private void GetSumTotalDM(Session _currSession, SalesInvoice _salesInvoice)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                DateTime now = DateTime.Now;
                double _locTotalDMCL = 0;
                double _locOutstanding = 0;

                if (_salesInvoice != null)
                {
                    XPCollection<DebitMemoCollection> _locDMCs = new XPCollection<DebitMemoCollection>(_currSession,
                                                                 new BinaryOperator("SalesInvoice", _salesInvoice));

                    if (_locDMCs != null && _locDMCs.Count() > 0)
                    {
                        foreach (DebitMemoCollection _locDMC in _locDMCs)
                        {
                            if (_locDMC.DM_Amount >= 0)
                            {
                                _locTotalDMCL = _locDMC.DM_Amount;
                            }
                        }
                        if (_salesInvoice.PostedCount == 0)
                        {
                            _salesInvoice.DM_Amount = _locTotalDMCL;
                            _salesInvoice.Save();
                            _salesInvoice.Session.CommitTransaction();
                        }
                        if (_salesInvoice.PostedCount > 0)
                        {
                            if (_salesInvoice.DM_Amount != _locTotalDMCL)
                            {
                                if (_salesInvoice.DM_Amount < _locTotalDMCL)
                                {
                                    if (_locOutstanding - (_locTotalDMCL - _salesInvoice.DM_Amount) > 0)
                                    {
                                        _locOutstanding = _locOutstanding - (_locTotalDMCL - _salesInvoice.DM_Amount);
                                    }
                                    else
                                    {
                                        _locOutstanding = _salesInvoice.MaxBill - (_salesInvoice.Bill + _salesInvoice.DM_Amount);
                                        _locTotalDMCL = _salesInvoice.DM_Amount;
                                    }

                                    _salesInvoice.Outstanding = _locOutstanding;
                                    _salesInvoice.DM_Amount = _locTotalDMCL;
                                    _salesInvoice.Save();
                                    _salesInvoice.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                    else
                    {
                        _salesInvoice.DM_Amount = 0;
                        _salesInvoice.Save();
                        _salesInvoice.Session.CommitTransaction();
                    }
                }


            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesInvoice" + ex.ToString());
            }
        }

        #endregion CLM

        #region GetBill

        private void GetBill(Session _currSession, SalesInvoice _salesInvoiceXPO)
        {
            try
            {
                double _totBillPIP = 0;
                double _totBillSO = 0;
                DateTime now = DateTime.Now;

                if (_salesInvoiceXPO != null)
                {
                    XPQuery<SalesInvoiceLine> _salesInvoiceLineQuerys = new XPQuery<SalesInvoiceLine>(_currSession);

                    var _salesInvoiceLines = from sil in _salesInvoiceLineQuerys
                                                      where (sil.SalesInvoice == _salesInvoiceXPO
                                                      && sil.Select == true
                                                      && (sil.Status == Status.Progress))
                                                      group sil by sil.SalesOrderMonitoring.SalesOrder into g
                                                      select new { SalesOrder = g.Key };

                    if(_salesInvoiceLines != null && _salesInvoiceLines.Count() > 0)
                    {
                        foreach(var _salesInvoiceLine in _salesInvoiceLines)
                        {
                            if(_salesInvoiceLine.SalesOrder != null)
                            {
                                XPCollection<PaymentInPlan> _locPaymentInPlans = new XPCollection<PaymentInPlan>
                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("SalesOrder", _salesInvoiceLine.SalesOrder)));

                                if(_locPaymentInPlans != null && _locPaymentInPlans.Count() > 0)
                                {
                                    foreach(PaymentInPlan _locPaymentInPlan in _locPaymentInPlans)
                                    {
                                        _totBillPIP = _totBillPIP + _locPaymentInPlan.Plan;
                                    }
                                }

                                if(_totBillPIP > 0)
                                {
                                    if(_totBillPIP <= _salesInvoiceLine.SalesOrder.MaksBill)
                                    {
                                        _totBillSO = _salesInvoiceLine.SalesOrder.MaksBill - _totBillPIP;
                                        XPCollection<SalesInvoiceLine> _locSalesInvLines = new XPCollection<SalesInvoiceLine>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("SalesInvoice", _salesInvoiceXPO),
                                                                                new BinaryOperator("Select", true),
                                                                                new GroupOperator(GroupOperatorType.Or,
                                                                                new BinaryOperator("Status", Status.Progress))));
                                        if (_locSalesInvLines != null && _locSalesInvLines.Count() > 0)
                                        {
                                            foreach (SalesInvoiceLine _locSalesInvLine in _locSalesInvLines)
                                            {
                                                if (_locSalesInvLine.SalesOrderMonitoring != null)
                                                {
                                                    if (_locSalesInvLine.SalesOrderMonitoring.SalesOrder == _salesInvoiceLine.SalesOrder)
                                                    {
                                                        if (_locSalesInvLine.TAmount <= _totBillSO)
                                                        {
                                                            _locSalesInvLine.Bill = _locSalesInvLine.TAmount;
                                                            _locSalesInvLine.Status = Status.Lock;
                                                            _locSalesInvLine.StatusDate = now;
                                                            _locSalesInvLine.Save();
                                                            _locSalesInvLine.Session.CommitTransaction();
                                                            _totBillSO = _totBillSO - _locSalesInvLine.TAmount;
                                                        }
                                                        else
                                                        {
                                                            _locSalesInvLine.Bill = _totBillSO;
                                                            _locSalesInvLine.Status = Status.Lock;
                                                            _locSalesInvLine.StatusDate = now;
                                                            _locSalesInvLine.Save();
                                                            _locSalesInvLine.Session.CommitTransaction();
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }   
                                }
                                else
                                {
                                    XPCollection<SalesInvoiceLine> _locSalesInvLines = new XPCollection<SalesInvoiceLine>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("SalesInvoice", _salesInvoiceXPO),
                                                                                new BinaryOperator("Select", true),
                                                                                new GroupOperator(GroupOperatorType.Or,
                                                                                new BinaryOperator("Status", Status.Progress))));
                                    if(_locSalesInvLines != null && _locSalesInvLines.Count() > 0)
                                    {
                                        foreach(SalesInvoiceLine _locSalesInvLine in _locSalesInvLines)
                                        {
                                            _locSalesInvLine.Bill = _locSalesInvLine.TAmount;
                                            _locSalesInvLine.Status = Status.Lock;
                                            _locSalesInvLine.StatusDate = now;
                                            _locSalesInvLine.Save();
                                            _locSalesInvLine.Session.CommitTransaction();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesInvoice ", ex.ToString());
            }
        }

        #endregion GetBill

        #region Posting

        private double GetSalesInvoiceLineTotalAmount(Session _currSession, SalesInvoice _locSalesInvoiceXPO)
        {
            double _return = 0;
            try
            {
                if(_locSalesInvoiceXPO != null )
                {
                    XPCollection<SalesInvoiceLine> _locSalesInvoiceLines = new XPCollection<SalesInvoiceLine>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                                                                new BinaryOperator("Select", true),
                                                                                                new GroupOperator(GroupOperatorType.Or,
                                                                                                new BinaryOperator("Status", Status.Lock),
                                                                                                new BinaryOperator("Status", Status.Posted))));

                    if (_locSalesInvoiceLines != null && _locSalesInvoiceLines.Count > 0)
                    {
                        foreach (SalesInvoiceLine _locSalesInvoiceLine in _locSalesInvoiceLines)
                        {
                            _return = _return + _locSalesInvoiceLine.Bill;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesInvoice" + ex.ToString());
            }
            return _return;
        }

        private void SetSalesInvoiceMonitoring(Session _currSession, SalesInvoice _locSalesInvoiceXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;

                if (_locSalesInvoiceXPO != null)
                {
                    if (_locSalesInvoiceXPO.Status == Status.Progress || _locSalesInvoiceXPO.Status == Status.Posted)
                    {
                        XPCollection<SalesInvoiceLine> _locSalesInvoiceLines = new XPCollection<SalesInvoiceLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Lock),
                                                            new BinaryOperator("Status", Status.Posted))));

                        if (_locSalesInvoiceLines != null && _locSalesInvoiceLines.Count() > 0)
                        {
                            foreach (SalesInvoiceLine _locSalesInvoiceLine in _locSalesInvoiceLines)
                            {
                                if(_locSalesInvoiceLine.Bill > 0)
                                {
                                    _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.SalesInvoiceMonitoring);

                                    if (_currSignCode != null)
                                    {

                                        SalesInvoiceMonitoring _saveDataSalesInvoiceMonitoring = new SalesInvoiceMonitoring(_currSession)
                                        {
                                            SignCode = _currSignCode,
                                            SalesInvoice = _locSalesInvoiceXPO,
                                            SalesInvoiceLine = _locSalesInvoiceLine,
                                            SalesType = _locSalesInvoiceLine.SalesType,
                                            Item = _locSalesInvoiceLine.Item,
                                            DQty = _locSalesInvoiceLine.DQty,
                                            DUOM = _locSalesInvoiceLine.DUOM,
                                            Qty = _locSalesInvoiceLine.Qty,
                                            UOM = _locSalesInvoiceLine.UOM,
                                            TQty = _locSalesInvoiceLine.TQty,
                                            Currency = _locSalesInvoiceLine.Currency,
                                            PriceGroup = _locSalesInvoiceLine.PriceGroup,
                                            Price = _locSalesInvoiceLine.Price,
                                            PriceLine = _locSalesInvoiceLine.PriceLine,
                                            UAmount = _locSalesInvoiceLine.UAmount,
                                            TUAmount = _locSalesInvoiceLine.TUAmount,
                                            MultiTax = _locSalesInvoiceLine.MultiTax,
                                            Tax = _locSalesInvoiceLine.Tax,
                                            TxValue = _locSalesInvoiceLine.TxValue,
                                            TxAmount = _locSalesInvoiceLine.TxAmount,
                                            MultiDiscount = _locSalesInvoiceLine.MultiDiscount,
                                            Discount = _locSalesInvoiceLine.Discount,
                                            Disc = _locSalesInvoiceLine.Disc,
                                            DiscAmount = _locSalesInvoiceLine.DiscAmount,
                                            TAmount = _locSalesInvoiceLine.TAmount,
                                            Bill = _locSalesInvoiceLine.Bill,
                                            SalesOrderMonitoring = _locSalesInvoiceLine.SalesOrderMonitoring,
                                            InventoryTransferOutMonitoring = _locSalesInvoiceLine.InventoryTransferOutMonitoring,
                                        };
                                        _saveDataSalesInvoiceMonitoring.Save();
                                        _saveDataSalesInvoiceMonitoring.Session.CommitTransaction();

                                        SalesInvoiceMonitoring _locSalesInvoiceMonitoring = _currSession.FindObject<SalesInvoiceMonitoring>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("SignCode", _currSignCode)));
                                        if (_locSalesInvoiceMonitoring != null)
                                        {
                                            SetPaymentInPlan(_currSession, _locSalesInvoiceXPO, _locSalesInvoiceMonitoring);
                                        }
                                    }
                                }    
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesOrder ", ex.ToString());
            }
        }

        private void SetReverseGoodsDeliverJournal(Session _currSession, SalesInvoice _locSalesInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locTotalUnitPrice = 0;

                if (_locSalesInvoiceXPO != null)
                {
                    //SalesInvoice yg di reverse
                    XPCollection<SalesInvoiceLine> _locSalesInvoiceLines = new XPCollection<SalesInvoiceLine>
                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                                        new BinaryOperator("Select", true),
                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                        new BinaryOperator("Status", Status.Lock),
                                                                        new BinaryOperator("Status", Status.Posted))));

                    if(_locSalesInvoiceLines != null && _locSalesInvoiceLines.Count() > 0)
                    {
                        foreach(SalesInvoiceLine _locSalesInvoiceLine in _locSalesInvoiceLines)
                        {
                            //ngambilnya nanti dari COGS
                            _locTotalUnitPrice = _locSalesInvoiceLine.TUAmount;

                            if (_locSalesInvoiceLine.SalesOrderMonitoring != null && _locSalesInvoiceLine.InventoryTransferOutMonitoring != null)
                            {
                                if(_locSalesInvoiceLine.SalesOrderMonitoring.SalesOrder != null && _locSalesInvoiceLine.InventoryTransferOutMonitoring.InventoryTransferOut != null)
                                {
                                    #region CreateReverseGoodsDeliverJournalByItems
                                    if(_locSalesInvoiceLine.Item.ItemAccountGroup != null)
                                    {
                                        XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("ItemAccountGroup", _locSalesInvoiceLine.Item.ItemAccountGroup)));

                                        if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                        {
                                            foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                            {
                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("JournalMap", _locJournalMapItem)));

                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                {
                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                    {
                                                        AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                     new BinaryOperator("PostingType", PostingType.Sales),
                                                                                     new BinaryOperator("OrderType", _locSalesInvoiceLine.SalesType),
                                                                                     new BinaryOperator("PostingMethod", PostingMethod.Deliver),
                                                                                     new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                        if (_locAccountMap != null)
                                                        {
                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                                new BinaryOperator("Active", true)));

                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                            {
                                                                double _locTotalAmountDebit = 0;
                                                                double _locTotalAmountCredit = 0;
                                                                double _locTotalBalance = 0;

                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                {
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                    {
                                                                        _locTotalAmountCredit = _locTotalUnitPrice;
                                                                    }
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                    {
                                                                        _locTotalAmountDebit = _locTotalUnitPrice;
                                                                    }

                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                    {
                                                                        PostingDate = now,
                                                                        PostingType = PostingType.Sales,
                                                                        PostingMethod = PostingMethod.InvoiceAR,
                                                                        Account = _locAccountMapLine.Account,
                                                                        Debit = _locTotalAmountDebit,
                                                                        Credit = _locTotalAmountCredit,
                                                                        SalesOrder = _locSalesInvoiceLine.SalesOrderMonitoring.SalesOrder,
                                                                        InventoryTransferOut = _locSalesInvoiceLine.InventoryTransferOutMonitoring.InventoryTransferOut,
                                                                        SalesInvoice = _locSalesInvoiceXPO,
                                                                        Company = _locSalesInvoiceXPO.Company,
                                                                    };
                                                                    _saveGeneralJournal.Save();
                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                    {
                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                        if (_locCOA != null)
                                                                        {
                                                                            if (_locCOA.BalanceType == BalanceType.Change)
                                                                            {
                                                                                if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                {
                                                                                    if (_locTotalAmountDebit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                    }
                                                                                    if (_locTotalAmountCredit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                    }
                                                                                }
                                                                                if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                {
                                                                                    if (_locTotalAmountDebit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                    }
                                                                                    if (_locTotalAmountCredit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                    }
                                                                                }
                                                                            }

                                                                            _locCOA.Balance = _locTotalBalance;
                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                            _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                            _locCOA.Save();
                                                                            _locCOA.Session.CommitTransaction();
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    
                                    #endregion CreateReverseGoodsDeliverJournalByItems

                                    //Nanti di hapus kalau sudah ada COGS sistemnya
                                    #region CreateReverseGoodsReceiptJournalByBusinessPartner
                                    if (_locSalesInvoiceXPO.SalesToCostumer != null)
                                    {
                                        if (_locSalesInvoiceXPO.SalesToCostumer.BusinessPartnerAccountGroup != null)
                                        {
                                            XPCollection<JournalMap> _locJournalMapBusinessPartners = new XPCollection<JournalMap>
                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("BusinessPartnerAccountGroup", _locSalesInvoiceXPO.SalesToCostumer.BusinessPartnerAccountGroup)));

                                            if (_locJournalMapBusinessPartners != null && _locJournalMapBusinessPartners.Count() > 0)
                                            {
                                                foreach (JournalMap _locJournalMapBusinessPartner in _locJournalMapBusinessPartners)
                                                {
                                                    XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("JournalMap", _locJournalMapBusinessPartner)));

                                                    if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                    {
                                                        foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                        {
                                                            AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                         (new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                         new BinaryOperator("PostingType", PostingType.Sales),
                                                                                         new BinaryOperator("OrderType", _locSalesInvoiceLine.SalesType),
                                                                                         new BinaryOperator("PostingMethod", PostingMethod.Deliver),
                                                                                         new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                            if (_locAccountMap != null)
                                                            {
                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                new BinaryOperator("Active", true)));

                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                {
                                                                    double _locTotalAmountDebit = 0;
                                                                    double _locTotalAmountCredit = 0;
                                                                    double _locTotalBalance = 0;

                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                    {
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            _locTotalAmountCredit = _locTotalUnitPrice;
                                                                        }
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            _locTotalAmountDebit = _locTotalUnitPrice;
                                                                        }

                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                        {
                                                                            PostingDate = now,
                                                                            PostingType = PostingType.Sales,
                                                                            PostingMethod = PostingMethod.InvoiceAR,
                                                                            Account = _locAccountMapLine.Account,
                                                                            Debit = _locTotalAmountDebit,
                                                                            Credit = _locTotalAmountCredit,
                                                                            SalesOrder = _locSalesInvoiceLine.SalesOrderMonitoring.SalesOrder,
                                                                            InventoryTransferOut = _locSalesInvoiceLine.InventoryTransferOutMonitoring.InventoryTransferOut,
                                                                            SalesInvoice = _locSalesInvoiceXPO,
                                                                            Company = _locSalesInvoiceXPO.Company,
                                                                        };
                                                                        _saveGeneralJournal.Save();
                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                        {
                                                                            ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                            if (_locCOA != null)
                                                                            {
                                                                                if (_locCOA.BalanceType == BalanceType.Change)
                                                                                {
                                                                                    if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                    if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                }

                                                                                _locCOA.Balance = _locTotalBalance;
                                                                                _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                _locCOA.Save();
                                                                                _locCOA.Session.CommitTransaction();
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion CreateReverseGoodsReceiptJournalByBusinessPartner
                                }

                            }
                        }
                    } 
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesInvoice ", ex.ToString());
            }
        }

        private void SetInvoiceARJournal(Session _currSession, SalesInvoice _locSalesInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locBillTUAmount = 0;
                double _locTxOfAmount = 0;
                double _locDiscOfAmount = 0;

                if (_locSalesInvoiceXPO != null)
                {
                    #region CreateInvoiceARJournal

                    XPCollection<SalesInvoiceLine> _locSalesInvoiceLines = new XPCollection<SalesInvoiceLine>
                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                                                new BinaryOperator("Select", true),
                                                                                new GroupOperator(GroupOperatorType.Or,
                                                                                new BinaryOperator("Status", Status.Lock),
                                                                                new BinaryOperator("Status", Status.Posted))));
                    if (_locSalesInvoiceLines != null && _locSalesInvoiceLines.Count() > 0)
                    {
                        foreach (SalesInvoiceLine _locSalesInvoiceLine in _locSalesInvoiceLines)
                        {


                            if (_locSalesInvoiceLine.SalesOrderMonitoring != null)
                            {
                                if (_locSalesInvoiceLine.SalesOrderMonitoring.SalesOrder != null)
                                {
                                    #region CreateNormalInvoiceARJournal

                                    _locBillTUAmount = _locSalesInvoiceLine.TUAmount;

                                    #region JournalMapItemAccountGroup
                                    if (_locSalesInvoiceLine.Item != null)
                                    {
                                        if (_locSalesInvoiceLine.Item.ItemAccountGroup != null)
                                        {
                                            XPCollection<JournalMap> _locJournalMaps = new XPCollection<JournalMap>
                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("ItemAccountGroup", _locSalesInvoiceLine.Item.ItemAccountGroup)));

                                            if (_locJournalMaps != null && _locJournalMaps.Count() > 0)
                                            {
                                                foreach (JournalMap _locJournalMap in _locJournalMaps)
                                                {
                                                    XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("JournalMap", _locJournalMap)));

                                                    if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                    {
                                                        foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                        {
                                                            AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                         new BinaryOperator("PostingType", PostingType.Sales),
                                                                                         new BinaryOperator("OrderType", _locSalesInvoiceLine.SalesType),
                                                                                         new BinaryOperator("PostingMethod", PostingMethod.InvoiceAR),
                                                                                         new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                            if (_locAccountMap != null)
                                                            {
                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                                    new BinaryOperator("Active", true)));

                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                {
                                                                    double _locTotalAmountDebit = 0;
                                                                    double _locTotalAmountCredit = 0;
                                                                    double _locTotalBalance = 0;

                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                    {
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            _locTotalAmountDebit = _locBillTUAmount;
                                                                        }
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            _locTotalAmountCredit = _locBillTUAmount;
                                                                        }

                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                        {
                                                                            PostingDate = now,
                                                                            PostingType = PostingType.Sales,
                                                                            OrderType = _locSalesInvoiceLine.SalesType,
                                                                            PostingMethod = PostingMethod.InvoiceAR,
                                                                            PostingMethodType = PostingMethodType.Normal,
                                                                            Account = _locAccountMapLine.Account,
                                                                            Debit = _locTotalAmountDebit,
                                                                            Credit = _locTotalAmountCredit,
                                                                            SalesOrder = _locSalesInvoiceLine.SalesOrderMonitoring.SalesOrder,
                                                                            SalesInvoice = _locSalesInvoiceXPO,
                                                                            Company = _locSalesInvoiceXPO.Company,
                                                                        };
                                                                        _saveGeneralJournal.Save();
                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                        {
                                                                            ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                            if (_locCOA != null)
                                                                            {
                                                                                if (_locCOA.BalanceType == BalanceType.Change)
                                                                                {
                                                                                    if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                    if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                }

                                                                                _locCOA.Balance = _locTotalBalance;
                                                                                _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                _locCOA.Save();
                                                                                _locCOA.Session.CommitTransaction();
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    #endregion JournalMapItemAccountGroup

                                    #region JournalMapBusinessPartnerAccountGroup
                                    if (_locSalesInvoiceXPO.SalesToCostumer != null)
                                    {
                                        if (_locSalesInvoiceXPO.SalesToCostumer.BusinessPartnerAccountGroup != null)
                                        {
                                            XPCollection<JournalMap> _locJournalMapByBusinessPartners = new XPCollection<JournalMap>
                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                         new BinaryOperator("BusinessPartnerAccountGroup", _locSalesInvoiceXPO.SalesToCostumer.BusinessPartnerAccountGroup)));

                                            if (_locJournalMapByBusinessPartners != null && _locJournalMapByBusinessPartners.Count() > 0)
                                            {
                                                foreach (JournalMap _locJournalMapByBusinessPartner in _locJournalMapByBusinessPartners)
                                                {
                                                    XPCollection<JournalMapLine> _locJournalMapLineByBusinessPartners = new XPCollection<JournalMapLine>
                                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                         new BinaryOperator("JournalMap", _locJournalMapByBusinessPartner)));

                                                    if (_locJournalMapLineByBusinessPartners != null && _locJournalMapLineByBusinessPartners.Count() > 0)
                                                    {
                                                        foreach (JournalMapLine _locJournalMapLineByBusinessPartner in _locJournalMapLineByBusinessPartners)
                                                        {
                                                            AccountMap _locAccountMapByBusinessPartner = _currSession.FindObject<AccountMap>
                                                                                                         (new GroupOperator(GroupOperatorType.And,
                                                                                                          new BinaryOperator("Code", _locJournalMapLineByBusinessPartner.AccountMap.Code),
                                                                                                          new BinaryOperator("PostingType", PostingType.Sales),
                                                                                                          new BinaryOperator("OrderType", _locSalesInvoiceLine.SalesType),
                                                                                                          new BinaryOperator("PostingMethod", PostingMethod.InvoiceAR),
                                                                                                          new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                            if (_locAccountMapByBusinessPartner != null)
                                                            {
                                                                XPCollection<AccountMapLine> _locAccountMapLineByBusinessPartners = new XPCollection<AccountMapLine>
                                                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                     new BinaryOperator("AccountMap", _locAccountMapByBusinessPartner),
                                                                                                                                     new BinaryOperator("Active", true)));

                                                                if (_locAccountMapLineByBusinessPartners != null && _locAccountMapLineByBusinessPartners.Count() > 0)
                                                                {
                                                                    double _locTotalAmountDebit = 0;
                                                                    double _locTotalAmountCredit = 0;
                                                                    double _locTotalBalance = 0;

                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLineByBusinessPartners)
                                                                    {
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            _locTotalAmountDebit = _locBillTUAmount;
                                                                        }
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            _locTotalAmountCredit = _locBillTUAmount;
                                                                        }

                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                        {
                                                                            PostingDate = now,
                                                                            PostingType = PostingType.Sales,
                                                                            OrderType = _locSalesInvoiceLine.SalesType,
                                                                            PostingMethod = PostingMethod.InvoiceAR,
                                                                            PostingMethodType = PostingMethodType.Normal,
                                                                            Account = _locAccountMapLine.Account,
                                                                            Debit = _locTotalAmountDebit,
                                                                            Credit = _locTotalAmountCredit,
                                                                            SalesOrder = _locSalesInvoiceLine.SalesOrderMonitoring.SalesOrder,
                                                                            SalesInvoice = _locSalesInvoiceXPO,
                                                                            Company = _locSalesInvoiceXPO.Company,
                                                                        };
                                                                        _saveGeneralJournal.Save();
                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                        {
                                                                            ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                            if (_locCOA != null)
                                                                            {
                                                                                if (_locCOA.BalanceType == BalanceType.Change)
                                                                                {
                                                                                    if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                    if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                }

                                                                                _locCOA.Balance = _locTotalBalance;
                                                                                _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                _locCOA.Save();
                                                                                _locCOA.Session.CommitTransaction();
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion JournalMapBusinessPartnerAccountGroup

                                    #endregion CreateNormalInvoiceARJournal

                                    #region CreateInvoiceARWithTax
                                    if (_locSalesInvoiceLine.MultiTax == true)
                                    {
                                        XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>
                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("SalesInvoiceLine", _locSalesInvoiceLine)));
                                        if (_locTaxLines != null && _locTaxLines.Count() > 0)
                                        {
                                            foreach (TaxLine _locTaxLine in _locTaxLines)
                                            {
                                                if (_locTaxLine.Tax != null)
                                                {
                                                    _locTxOfAmount = (_locTaxLine.TxValue * _locSalesInvoiceLine.TUAmount) / 100;

                                                    #region JournalMapTaxAccountGroup
                                                    if (_locTaxLine.Tax.TaxAccountGroup != null)
                                                    {
                                                        XPCollection<JournalMap> _locJournalMapByTaxs = new XPCollection<JournalMap>
                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                     new BinaryOperator("TaxAccountGroup", _locTaxLine.Tax.TaxAccountGroup)));

                                                        if (_locJournalMapByTaxs != null && _locJournalMapByTaxs.Count() > 0)
                                                        {
                                                            foreach (JournalMap _locJournalMapByTax in _locJournalMapByTaxs)
                                                            {
                                                                XPCollection<JournalMapLine> _locJournalMapLineByTaxs = new XPCollection<JournalMapLine>
                                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                         new BinaryOperator("JournalMap", _locJournalMapByTax)));

                                                                if (_locJournalMapLineByTaxs != null && _locJournalMapLineByTaxs.Count() > 0)
                                                                {
                                                                    foreach (JournalMapLine _locJournalMapLineByTax in _locJournalMapLineByTaxs)
                                                                    {
                                                                        AccountMap _locAccountMapByTax = _currSession.FindObject<AccountMap>
                                                                                                         (new GroupOperator(GroupOperatorType.And,
                                                                                                          new BinaryOperator("Code", _locJournalMapLineByTax.AccountMap.Code),
                                                                                                          new BinaryOperator("PostingType", PostingType.Sales),
                                                                                                          new BinaryOperator("OrderType", _locSalesInvoiceLine.SalesType),
                                                                                                          new BinaryOperator("PostingMethod", PostingMethod.InvoiceAR),
                                                                                                          new BinaryOperator("PostingMethodType", PostingMethodType.Tax)));

                                                                        if (_locAccountMapByTax != null)
                                                                        {
                                                                            XPCollection<AccountMapLine> _locAccountMapLineByTaxs = new XPCollection<AccountMapLine>
                                                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                     new BinaryOperator("AccountMap", _locAccountMapByTax),
                                                                                                                                     new BinaryOperator("Active", true)));

                                                                            if (_locAccountMapLineByTaxs != null && _locAccountMapLineByTaxs.Count() > 0)
                                                                            {
                                                                                double _locTotalAmountDebit = 0;
                                                                                double _locTotalAmountCredit = 0;
                                                                                double _locTotalBalance = 0;

                                                                                foreach (AccountMapLine _locAccountMapLineByTax in _locAccountMapLineByTaxs)
                                                                                {
                                                                                    if (_locAccountMapLineByTax.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        _locTotalAmountDebit = _locTxOfAmount;
                                                                                    }
                                                                                    if (_locAccountMapLineByTax.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        _locTotalAmountCredit = _locTxOfAmount;
                                                                                    }

                                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                    {
                                                                                        PostingDate = now,
                                                                                        PostingType = PostingType.Sales,
                                                                                        OrderType = _locSalesInvoiceLine.SalesType,
                                                                                        PostingMethod = PostingMethod.InvoiceAR,
                                                                                        PostingMethodType = PostingMethodType.Tax,
                                                                                        Account = _locAccountMapLineByTax.Account,
                                                                                        Debit = _locTotalAmountDebit,
                                                                                        Credit = _locTotalAmountCredit,
                                                                                        SalesOrder = _locSalesInvoiceLine.SalesOrderMonitoring.SalesOrder,
                                                                                        SalesInvoice = _locSalesInvoiceXPO,
                                                                                        Company = _locSalesInvoiceXPO.Company,
                                                                                    };
                                                                                    _saveGeneralJournal.Save();
                                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                                    if (_locAccountMapLineByTax.Account.Code != null)
                                                                                    {
                                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                                                                  new BinaryOperator("Code", _locAccountMapLineByTax.Account.Code)));
                                                                                        if (_locCOA != null)
                                                                                        {
                                                                                            if (_locCOA.BalanceType == BalanceType.Change)
                                                                                            {
                                                                                                if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                                if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                            }

                                                                                            _locCOA.Balance = _locTotalBalance;
                                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                            _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                            _locCOA.Save();
                                                                                            _locCOA.Session.CommitTransaction();
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }

                                                    #endregion JournalMapTaxAccountGroup

                                                    #region JournalMapBusinessPartnerAccountGroup
                                                    if (_locSalesInvoiceXPO.SalesToCostumer != null)
                                                    {
                                                        if (_locSalesInvoiceXPO.SalesToCostumer.BusinessPartnerAccountGroup != null)
                                                        {
                                                            XPCollection<JournalMap> _locJournalMapByBusinessPartners = new XPCollection<JournalMap>
                                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                         new BinaryOperator("BusinessPartnerAccountGroup", _locSalesInvoiceXPO.SalesToCostumer.BusinessPartnerAccountGroup)));

                                                            if (_locJournalMapByBusinessPartners != null && _locJournalMapByBusinessPartners.Count() > 0)
                                                            {
                                                                foreach (JournalMap _locJournalMapByBusinessPartner in _locJournalMapByBusinessPartners)
                                                                {
                                                                    XPCollection<JournalMapLine> _locJournalMapLineByBusinessPartners = new XPCollection<JournalMapLine>
                                                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                         new BinaryOperator("JournalMap", _locJournalMapByBusinessPartner)));

                                                                    if (_locJournalMapLineByBusinessPartners != null && _locJournalMapLineByBusinessPartners.Count() > 0)
                                                                    {
                                                                        foreach (JournalMapLine _locJournalMapLineByBusinessPartner in _locJournalMapLineByBusinessPartners)
                                                                        {
                                                                            AccountMap _locAccountMapByBusinessPartner = _currSession.FindObject<AccountMap>
                                                                                                                         (new GroupOperator(GroupOperatorType.And,
                                                                                                                          new BinaryOperator("Code", _locJournalMapLineByBusinessPartner.AccountMap.Code),
                                                                                                                          new BinaryOperator("PostingType", PostingType.Sales),
                                                                                                                          new BinaryOperator("OrderType", OrderType.Item),
                                                                                                                          new BinaryOperator("PostingMethod", PostingMethod.InvoiceAR),
                                                                                                                          new BinaryOperator("PostingMethodType", PostingMethodType.Tax)));

                                                                            if (_locAccountMapByBusinessPartner != null)
                                                                            {
                                                                                XPCollection<AccountMapLine> _locAccountMapLineByBusinessPartners = new XPCollection<AccountMapLine>
                                                                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                                     new BinaryOperator("AccountMap", _locAccountMapByBusinessPartner),
                                                                                                                                                     new BinaryOperator("Active", true)));

                                                                                if (_locAccountMapLineByBusinessPartners != null && _locAccountMapLineByBusinessPartners.Count() > 0)
                                                                                {
                                                                                    double _locTotalAmountDebit = 0;
                                                                                    double _locTotalAmountCredit = 0;
                                                                                    double _locTotalBalance = 0;

                                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLineByBusinessPartners)
                                                                                    {
                                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                        {
                                                                                            _locTotalAmountDebit = _locTxOfAmount;
                                                                                        }
                                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                        {
                                                                                            _locTotalAmountCredit = _locTxOfAmount;
                                                                                        }

                                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                        {
                                                                                            PostingDate = now,
                                                                                            PostingType = PostingType.Sales,
                                                                                            PostingMethod = PostingMethod.InvoiceAR,
                                                                                            Account = _locAccountMapLine.Account,
                                                                                            Debit = _locTotalAmountDebit,
                                                                                            Credit = _locTotalAmountCredit,
                                                                                            SalesOrder = _locSalesInvoiceLine.SalesOrderMonitoring.SalesOrder,
                                                                                            SalesInvoice = _locSalesInvoiceXPO,
                                                                                            Company = _locSalesInvoiceXPO.Company,
                                                                                        };
                                                                                        _saveGeneralJournal.Save();
                                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                                        {
                                                                                            ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                                            if (_locCOA != null)
                                                                                            {
                                                                                                if (_locCOA.BalanceType == BalanceType.Change)
                                                                                                {
                                                                                                    if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                                    {
                                                                                                        if (_locTotalAmountDebit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                                        }
                                                                                                        if (_locTotalAmountCredit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                                        }
                                                                                                    }
                                                                                                    if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                                    {
                                                                                                        if (_locTotalAmountDebit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                                        }
                                                                                                        if (_locTotalAmountCredit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                                        }
                                                                                                    }
                                                                                                }

                                                                                                _locCOA.Balance = _locTotalBalance;
                                                                                                _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                                _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                                _locCOA.Save();
                                                                                                _locCOA.Session.CommitTransaction();
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    #endregion JournalMapBusinessPartnerAccountGroup
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {

                                        if (_locSalesInvoiceLine.Tax != null)
                                        {
                                            _locTxOfAmount = _locSalesInvoiceLine.TxAmount;

                                            #region JournalMapTaxAccountGroup
                                            if (_locSalesInvoiceLine.Tax.TaxAccountGroup != null)
                                            {
                                                XPCollection<JournalMap> _locJournalMapByTaxs = new XPCollection<JournalMap>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                             new BinaryOperator("TaxAccountGroup", _locSalesInvoiceLine.Tax.TaxAccountGroup)));

                                                if (_locJournalMapByTaxs != null && _locJournalMapByTaxs.Count() > 0)
                                                {
                                                    foreach (JournalMap _locJournalMapByTax in _locJournalMapByTaxs)
                                                    {
                                                        XPCollection<JournalMapLine> _locJournalMapLineByTaxs = new XPCollection<JournalMapLine>
                                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                 new BinaryOperator("JournalMap", _locJournalMapByTax)));

                                                        if (_locJournalMapLineByTaxs != null && _locJournalMapLineByTaxs.Count() > 0)
                                                        {
                                                            foreach (JournalMapLine _locJournalMapLineByTax in _locJournalMapLineByTaxs)
                                                            {
                                                                AccountMap _locAccountMapByTax = _currSession.FindObject<AccountMap>
                                                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                                                  new BinaryOperator("Code", _locJournalMapLineByTax.AccountMap.Code),
                                                                                                  new BinaryOperator("PostingType", PostingType.Sales),
                                                                                                  new BinaryOperator("OrderType", OrderType.Item),
                                                                                                  new BinaryOperator("PostingMethod", PostingMethod.InvoiceAR),
                                                                                                  new BinaryOperator("PostingMethodType", PostingMethodType.Tax)));

                                                                if (_locAccountMapByTax != null)
                                                                {
                                                                    XPCollection<AccountMapLine> _locAccountMapLineByTaxs = new XPCollection<AccountMapLine>
                                                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                             new BinaryOperator("AccountMap", _locAccountMapByTax),
                                                                                                                             new BinaryOperator("Active", true)));

                                                                    if (_locAccountMapLineByTaxs != null && _locAccountMapLineByTaxs.Count() > 0)
                                                                    {
                                                                        double _locTotalAmountDebit = 0;
                                                                        double _locTotalAmountCredit = 0;
                                                                        double _locTotalBalance = 0;

                                                                        foreach (AccountMapLine _locAccountMapLineByTax in _locAccountMapLineByTaxs)
                                                                        {
                                                                            if (_locAccountMapLineByTax.AccountCharge == AccountCharge.Debit)
                                                                            {
                                                                                _locTotalAmountDebit = _locTxOfAmount;
                                                                            }
                                                                            if (_locAccountMapLineByTax.AccountCharge == AccountCharge.Credit)
                                                                            {
                                                                                _locTotalAmountCredit = _locTxOfAmount;
                                                                            }

                                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                            {
                                                                                PostingDate = now,
                                                                                PostingType = PostingType.Sales,
                                                                                PostingMethod = PostingMethod.InvoiceAR,
                                                                                Account = _locAccountMapLineByTax.Account,
                                                                                Debit = _locTotalAmountDebit,
                                                                                Credit = _locTotalAmountCredit,
                                                                                SalesOrder = _locSalesInvoiceLine.SalesOrderMonitoring.SalesOrder,
                                                                                SalesInvoice = _locSalesInvoiceXPO,
                                                                                Company = _locSalesInvoiceXPO.Company,
                                                                            };
                                                                            _saveGeneralJournal.Save();
                                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                                            if (_locAccountMapLineByTax.Account.Code != null)
                                                                            {
                                                                                ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                         (new GroupOperator(GroupOperatorType.And,
                                                                                                          new BinaryOperator("Code", _locAccountMapLineByTax.Account.Code)));
                                                                                if (_locCOA != null)
                                                                                {
                                                                                    if (_locCOA.BalanceType == BalanceType.Change)
                                                                                    {
                                                                                        if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                        if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                    }

                                                                                    _locCOA.Balance = _locTotalBalance;
                                                                                    _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                    _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                    _locCOA.Save();
                                                                                    _locCOA.Session.CommitTransaction();
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            #endregion JournalMapTaxAccountGroup

                                            #region JournalMapBusinessPartnerAccountGroup
                                            if (_locSalesInvoiceXPO.SalesToCostumer != null)
                                            {
                                                if (_locSalesInvoiceXPO.SalesToCostumer.BusinessPartnerAccountGroup != null)
                                                {
                                                    XPCollection<JournalMap> _locJournalMapByBusinessPartners = new XPCollection<JournalMap>
                                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                 new BinaryOperator("BusinessPartnerAccountGroup", _locSalesInvoiceXPO.SalesToCostumer.BusinessPartnerAccountGroup)));

                                                    if (_locJournalMapByBusinessPartners != null && _locJournalMapByBusinessPartners.Count() > 0)
                                                    {
                                                        foreach (JournalMap _locJournalMapByBusinessPartner in _locJournalMapByBusinessPartners)
                                                        {
                                                            XPCollection<JournalMapLine> _locJournalMapLineByBusinessPartners = new XPCollection<JournalMapLine>
                                                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                 new BinaryOperator("JournalMap", _locJournalMapByBusinessPartner)));

                                                            if (_locJournalMapLineByBusinessPartners != null && _locJournalMapLineByBusinessPartners.Count() > 0)
                                                            {
                                                                foreach (JournalMapLine _locJournalMapLineByBusinessPartner in _locJournalMapLineByBusinessPartners)
                                                                {
                                                                    AccountMap _locAccountMapByBusinessPartner = _currSession.FindObject<AccountMap>
                                                                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                                                                  new BinaryOperator("Code", _locJournalMapLineByBusinessPartner.AccountMap.Code),
                                                                                                                  new BinaryOperator("PostingType", PostingType.Sales),
                                                                                                                  new BinaryOperator("OrderType", OrderType.Item),
                                                                                                                  new BinaryOperator("PostingMethod", PostingMethod.InvoiceAR),
                                                                                                                  new BinaryOperator("PostingMethodType", PostingMethodType.Tax)));

                                                                    if (_locAccountMapByBusinessPartner != null)
                                                                    {
                                                                        XPCollection<AccountMapLine> _locAccountMapLineByBusinessPartners = new XPCollection<AccountMapLine>
                                                                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                             new BinaryOperator("AccountMap", _locAccountMapByBusinessPartner),
                                                                                                                                             new BinaryOperator("Active", true)));

                                                                        if (_locAccountMapLineByBusinessPartners != null && _locAccountMapLineByBusinessPartners.Count() > 0)
                                                                        {
                                                                            double _locTotalAmountDebit = 0;
                                                                            double _locTotalAmountCredit = 0;
                                                                            double _locTotalBalance = 0;

                                                                            foreach (AccountMapLine _locAccountMapLine in _locAccountMapLineByBusinessPartners)
                                                                            {
                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                {
                                                                                    _locTotalAmountDebit = _locTxOfAmount;
                                                                                }
                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                {
                                                                                    _locTotalAmountCredit = _locTxOfAmount;
                                                                                }

                                                                                GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                {
                                                                                    PostingDate = now,
                                                                                    PostingType = PostingType.Sales,
                                                                                    PostingMethod = PostingMethod.InvoiceAR,
                                                                                    Account = _locAccountMapLine.Account,
                                                                                    Debit = _locTotalAmountDebit,
                                                                                    Credit = _locTotalAmountCredit,
                                                                                    SalesOrder = _locSalesInvoiceLine.SalesOrderMonitoring.SalesOrder,
                                                                                    SalesInvoice = _locSalesInvoiceXPO,
                                                                                    Company = _locSalesInvoiceXPO.Company,
                                                                                };
                                                                                _saveGeneralJournal.Save();
                                                                                _saveGeneralJournal.Session.CommitTransaction();

                                                                                if (_locAccountMapLine.Account.Code != null)
                                                                                {
                                                                                    ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                                    if (_locCOA != null)
                                                                                    {
                                                                                        if (_locCOA.BalanceType == BalanceType.Change)
                                                                                        {
                                                                                            if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                            if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                        }

                                                                                        _locCOA.Balance = _locTotalBalance;
                                                                                        _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                        _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                        _locCOA.Save();
                                                                                        _locCOA.Session.CommitTransaction();
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            #endregion JournalMapBusinessPartnerAccountGroup
                                        }
                                    }
                                    #endregion CreateInvoiceARWithTax

                                    #region CreateInvoiceARWithDiscount
                                    if (_locSalesInvoiceLine.MultiDiscount == true)
                                    {
                                        XPCollection<DiscountLine> _locDiscountLines = new XPCollection<DiscountLine>
                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("SalesInvoiceLine", _locSalesInvoiceLine)));
                                        if (_locDiscountLines != null && _locDiscountLines.Count() > 0)
                                        {
                                            foreach (DiscountLine _locDiscountLine in _locDiscountLines)
                                            {
                                                if (_locDiscountLine.Discount != null)
                                                {
                                                    _locDiscOfAmount = (_locDiscountLine.Disc * _locSalesInvoiceLine.TUAmount) / 100;

                                                    #region JournalMapDiscountAccountGroup
                                                    if (_locDiscountLine.Discount.DiscountAccountGroup != null)
                                                    {
                                                        XPCollection<JournalMap> _locJournalMapByDiscounts = new XPCollection<JournalMap>
                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                     new BinaryOperator("DiscountAccountGroup", _locDiscountLine.Discount.DiscountAccountGroup)));

                                                        if (_locJournalMapByDiscounts != null && _locJournalMapByDiscounts.Count() > 0)
                                                        {
                                                            foreach (JournalMap _locJournalMapByDiscount in _locJournalMapByDiscounts)
                                                            {
                                                                XPCollection<JournalMapLine> _locJournalMapLineByDiscounts = new XPCollection<JournalMapLine>
                                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                         new BinaryOperator("JournalMap", _locJournalMapByDiscount)));

                                                                if (_locJournalMapLineByDiscounts != null && _locJournalMapLineByDiscounts.Count() > 0)
                                                                {
                                                                    foreach (JournalMapLine _locJournalMapLineByDiscount in _locJournalMapLineByDiscounts)
                                                                    {
                                                                        AccountMap _locAccountMapByDiscount = _currSession.FindObject<AccountMap>
                                                                                                         (new GroupOperator(GroupOperatorType.And,
                                                                                                          new BinaryOperator("Code", _locJournalMapLineByDiscount.AccountMap.Code),
                                                                                                          new BinaryOperator("PostingType", PostingType.Sales),
                                                                                                          new BinaryOperator("OrderType", _locSalesInvoiceLine.SalesType),
                                                                                                          new BinaryOperator("PostingMethod", PostingMethod.InvoiceAR),
                                                                                                          new BinaryOperator("PostingMethodType", PostingMethodType.Discount)));

                                                                        if (_locAccountMapByDiscount != null)
                                                                        {
                                                                            XPCollection<AccountMapLine> _locAccountMapLineByDiscounts = new XPCollection<AccountMapLine>
                                                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                     new BinaryOperator("AccountMap", _locAccountMapByDiscount),
                                                                                                                                     new BinaryOperator("Active", true)));

                                                                            if (_locAccountMapLineByDiscounts != null && _locAccountMapLineByDiscounts.Count() > 0)
                                                                            {
                                                                                double _locTotalAmountDebit = 0;
                                                                                double _locTotalAmountCredit = 0;
                                                                                double _locTotalBalance = 0;

                                                                                foreach (AccountMapLine _locAccountMapLineByDiscount in _locAccountMapLineByDiscounts)
                                                                                {
                                                                                    if (_locAccountMapLineByDiscount.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        _locTotalAmountDebit = _locDiscOfAmount;
                                                                                    }
                                                                                    if (_locAccountMapLineByDiscount.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        _locTotalAmountCredit = _locDiscOfAmount;
                                                                                    }

                                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                    {
                                                                                        PostingDate = now,
                                                                                        PostingType = PostingType.Sales,
                                                                                        OrderType = _locSalesInvoiceLine.SalesType,
                                                                                        PostingMethod = PostingMethod.InvoiceAR,
                                                                                        PostingMethodType = PostingMethodType.Discount,
                                                                                        Account = _locAccountMapLineByDiscount.Account,
                                                                                        Debit = _locTotalAmountDebit,
                                                                                        Credit = _locTotalAmountCredit,
                                                                                        SalesOrder = _locSalesInvoiceLine.SalesOrderMonitoring.SalesOrder,
                                                                                        SalesInvoice = _locSalesInvoiceXPO,
                                                                                        Company = _locSalesInvoiceXPO.Company,
                                                                                    };
                                                                                    _saveGeneralJournal.Save();
                                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                                    if (_locAccountMapLineByDiscount.Account.Code != null)
                                                                                    {
                                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                                                                  new BinaryOperator("Code", _locAccountMapLineByDiscount.Account.Code)));
                                                                                        if (_locCOA != null)
                                                                                        {
                                                                                            if (_locCOA.BalanceType == BalanceType.Change)
                                                                                            {
                                                                                                if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                                if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                            }

                                                                                            _locCOA.Balance = _locTotalBalance;
                                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                            _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                            _locCOA.Save();
                                                                                            _locCOA.Session.CommitTransaction();
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }

                                                    #endregion JournalMapDiscountAccountGroup

                                                    #region JournalMapBusinessPartnerAccountGroup
                                                    if (_locSalesInvoiceXPO.SalesToCostumer != null)
                                                    {
                                                        if (_locSalesInvoiceXPO.SalesToCostumer.BusinessPartnerAccountGroup != null)
                                                        {
                                                            XPCollection<JournalMap> _locJournalMapByBusinessPartners = new XPCollection<JournalMap>
                                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                         new BinaryOperator("BusinessPartnerAccountGroup", _locSalesInvoiceXPO.SalesToCostumer.BusinessPartnerAccountGroup)));

                                                            if (_locJournalMapByBusinessPartners != null && _locJournalMapByBusinessPartners.Count() > 0)
                                                            {
                                                                foreach (JournalMap _locJournalMapByBusinessPartner in _locJournalMapByBusinessPartners)
                                                                {
                                                                    XPCollection<JournalMapLine> _locJournalMapLineByBusinessPartners = new XPCollection<JournalMapLine>
                                                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                         new BinaryOperator("JournalMap", _locJournalMapByBusinessPartner)));

                                                                    if (_locJournalMapLineByBusinessPartners != null && _locJournalMapLineByBusinessPartners.Count() > 0)
                                                                    {
                                                                        foreach (JournalMapLine _locJournalMapLineByBusinessPartner in _locJournalMapLineByBusinessPartners)
                                                                        {
                                                                            AccountMap _locAccountMapByBusinessPartner = _currSession.FindObject<AccountMap>
                                                                                                                         (new GroupOperator(GroupOperatorType.And,
                                                                                                                          new BinaryOperator("Code", _locJournalMapLineByBusinessPartner.AccountMap.Code),
                                                                                                                          new BinaryOperator("PostingType", PostingType.Sales),
                                                                                                                          new BinaryOperator("OrderType", _locSalesInvoiceLine.SalesType),
                                                                                                                          new BinaryOperator("PostingMethod", PostingMethod.InvoiceAR),
                                                                                                                          new BinaryOperator("PostingMethodType", PostingMethodType.Discount)));

                                                                            if (_locAccountMapByBusinessPartner != null)
                                                                            {
                                                                                XPCollection<AccountMapLine> _locAccountMapLineByBusinessPartners = new XPCollection<AccountMapLine>
                                                                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                                     new BinaryOperator("AccountMap", _locAccountMapByBusinessPartner),
                                                                                                                                                     new BinaryOperator("Active", true)));

                                                                                if (_locAccountMapLineByBusinessPartners != null && _locAccountMapLineByBusinessPartners.Count() > 0)
                                                                                {
                                                                                    double _locTotalAmountDebit = 0;
                                                                                    double _locTotalAmountCredit = 0;
                                                                                    double _locTotalBalance = 0;

                                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLineByBusinessPartners)
                                                                                    {
                                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                        {
                                                                                            _locTotalAmountDebit = _locDiscOfAmount;
                                                                                        }
                                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                        {
                                                                                            _locTotalAmountCredit = _locDiscOfAmount;
                                                                                        }

                                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                        {
                                                                                            PostingDate = now,
                                                                                            PostingType = PostingType.Sales,
                                                                                            OrderType = _locSalesInvoiceLine.SalesType,
                                                                                            PostingMethod = PostingMethod.InvoiceAR,
                                                                                            PostingMethodType = PostingMethodType.Discount,
                                                                                            Account = _locAccountMapLine.Account,
                                                                                            Debit = _locTotalAmountDebit,
                                                                                            Credit = _locTotalAmountCredit,
                                                                                            SalesOrder = _locSalesInvoiceLine.SalesOrderMonitoring.SalesOrder,
                                                                                            SalesInvoice = _locSalesInvoiceXPO,
                                                                                            Company = _locSalesInvoiceXPO.Company,
                                                                                        };
                                                                                        _saveGeneralJournal.Save();
                                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                                        {
                                                                                            ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                                                      new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                                            if (_locCOA != null)
                                                                                            {
                                                                                                if (_locCOA.BalanceType == BalanceType.Change)
                                                                                                {
                                                                                                    if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                                    {
                                                                                                        if (_locTotalAmountDebit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                                        }
                                                                                                        if (_locTotalAmountCredit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                                        }
                                                                                                    }
                                                                                                    if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                                    {
                                                                                                        if (_locTotalAmountDebit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                                        }
                                                                                                        if (_locTotalAmountCredit > 0)
                                                                                                        {
                                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                                        }
                                                                                                    }
                                                                                                }

                                                                                                _locCOA.Balance = _locTotalBalance;
                                                                                                _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                                _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                                _locCOA.Save();
                                                                                                _locCOA.Session.CommitTransaction();
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    #endregion JournalMapBusinessPartnerAccountGroup
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (_locSalesInvoiceLine.Discount != null)
                                        {
                                            _locDiscOfAmount = _locSalesInvoiceLine.DiscAmount;

                                            #region JournalMapDiscountAccountGroup
                                            if (_locSalesInvoiceLine.Discount.DiscountAccountGroup != null)
                                            {
                                                XPCollection<JournalMap> _locJournalMapByDiscounts = new XPCollection<JournalMap>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                             new BinaryOperator("DiscountAccountGroup", _locSalesInvoiceLine.Discount.DiscountAccountGroup)));

                                                if (_locJournalMapByDiscounts != null && _locJournalMapByDiscounts.Count() > 0)
                                                {
                                                    foreach (JournalMap _locJournalMapByDiscount in _locJournalMapByDiscounts)
                                                    {
                                                        XPCollection<JournalMapLine> _locJournalMapLineByDiscounts = new XPCollection<JournalMapLine>
                                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                 new BinaryOperator("JournalMap", _locJournalMapByDiscount)));

                                                        if (_locJournalMapLineByDiscounts != null && _locJournalMapLineByDiscounts.Count() > 0)
                                                        {
                                                            foreach (JournalMapLine _locJournalMapLineByDiscount in _locJournalMapLineByDiscounts)
                                                            {
                                                                AccountMap _locAccountMapByDiscount = _currSession.FindObject<AccountMap>
                                                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                                                  new BinaryOperator("Code", _locJournalMapLineByDiscount.AccountMap.Code),
                                                                                                  new BinaryOperator("PostingType", PostingType.Sales),
                                                                                                  new BinaryOperator("OrderType", _locSalesInvoiceLine.SalesType),
                                                                                                  new BinaryOperator("PostingMethod", PostingMethod.InvoiceAR),
                                                                                                  new BinaryOperator("PostingMethodType", PostingMethodType.Discount)));

                                                                if (_locAccountMapByDiscount != null)
                                                                {
                                                                    XPCollection<AccountMapLine> _locAccountMapLineByDiscounts = new XPCollection<AccountMapLine>
                                                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                             new BinaryOperator("AccountMap", _locAccountMapByDiscount),
                                                                                                                             new BinaryOperator("Active", true)));

                                                                    if (_locAccountMapLineByDiscounts != null && _locAccountMapLineByDiscounts.Count() > 0)
                                                                    {
                                                                        double _locTotalAmountDebit = 0;
                                                                        double _locTotalAmountCredit = 0;
                                                                        double _locTotalBalance = 0;

                                                                        foreach (AccountMapLine _locAccountMapLineByDiscount in _locAccountMapLineByDiscounts)
                                                                        {
                                                                            if (_locAccountMapLineByDiscount.AccountCharge == AccountCharge.Debit)
                                                                            {
                                                                                _locTotalAmountDebit = _locDiscOfAmount;
                                                                            }
                                                                            if (_locAccountMapLineByDiscount.AccountCharge == AccountCharge.Credit)
                                                                            {
                                                                                _locTotalAmountCredit = _locDiscOfAmount;
                                                                            }

                                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                            {
                                                                                PostingDate = now,
                                                                                PostingType = PostingType.Sales,
                                                                                OrderType = _locSalesInvoiceLine.SalesType,
                                                                                PostingMethod = PostingMethod.InvoiceAR,
                                                                                PostingMethodType = PostingMethodType.Discount,
                                                                                Account = _locAccountMapLineByDiscount.Account,
                                                                                Debit = _locTotalAmountDebit,
                                                                                Credit = _locTotalAmountCredit,
                                                                                SalesOrder = _locSalesInvoiceLine.SalesOrderMonitoring.SalesOrder,
                                                                                SalesInvoice = _locSalesInvoiceXPO,
                                                                                Company = _locSalesInvoiceXPO.Company,
                                                                            };
                                                                            _saveGeneralJournal.Save();
                                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                                            if (_locAccountMapLineByDiscount.Account.Code != null)
                                                                            {
                                                                                ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                         (new GroupOperator(GroupOperatorType.And,
                                                                                                          new BinaryOperator("Code", _locAccountMapLineByDiscount.Account.Code)));
                                                                                if (_locCOA != null)
                                                                                {
                                                                                    if (_locCOA.BalanceType == BalanceType.Change)
                                                                                    {
                                                                                        if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                        if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                        {
                                                                                            if (_locTotalAmountDebit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                            }
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                    }

                                                                                    _locCOA.Balance = _locTotalBalance;
                                                                                    _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                    _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                    _locCOA.Save();
                                                                                    _locCOA.Session.CommitTransaction();
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            #endregion JournalMapDiscountAccountGroup

                                            #region JournalMapBusinessPartnerAccountGroup
                                            if (_locSalesInvoiceXPO.SalesToCostumer != null)
                                            {
                                                if (_locSalesInvoiceXPO.SalesToCostumer.BusinessPartnerAccountGroup != null)
                                                {
                                                    XPCollection<JournalMap> _locJournalMapByBusinessPartners = new XPCollection<JournalMap>
                                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                 new BinaryOperator("BusinessPartnerAccountGroup", _locSalesInvoiceXPO.SalesToCostumer.BusinessPartnerAccountGroup)));

                                                    if (_locJournalMapByBusinessPartners != null && _locJournalMapByBusinessPartners.Count() > 0)
                                                    {
                                                        foreach (JournalMap _locJournalMapByBusinessPartner in _locJournalMapByBusinessPartners)
                                                        {
                                                            XPCollection<JournalMapLine> _locJournalMapLineByBusinessPartners = new XPCollection<JournalMapLine>
                                                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                 new BinaryOperator("JournalMap", _locJournalMapByBusinessPartner)));

                                                            if (_locJournalMapLineByBusinessPartners != null && _locJournalMapLineByBusinessPartners.Count() > 0)
                                                            {
                                                                foreach (JournalMapLine _locJournalMapLineByBusinessPartner in _locJournalMapLineByBusinessPartners)
                                                                {
                                                                    AccountMap _locAccountMapByBusinessPartner = _currSession.FindObject<AccountMap>
                                                                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                                                                  new BinaryOperator("Code", _locJournalMapLineByBusinessPartner.AccountMap.Code),
                                                                                                                  new BinaryOperator("PostingType", PostingType.Sales),
                                                                                                                  new BinaryOperator("OrderType", _locSalesInvoiceLine.SalesType),
                                                                                                                  new BinaryOperator("PostingMethod", PostingMethod.InvoiceAR),
                                                                                                                  new BinaryOperator("PostingMethodType", PostingMethodType.Discount)));

                                                                    if (_locAccountMapByBusinessPartner != null)
                                                                    {
                                                                        XPCollection<AccountMapLine> _locAccountMapLineByBusinessPartners = new XPCollection<AccountMapLine>
                                                                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                             new BinaryOperator("AccountMap", _locAccountMapByBusinessPartner),
                                                                                                                                             new BinaryOperator("Active", true)));

                                                                        if (_locAccountMapLineByBusinessPartners != null && _locAccountMapLineByBusinessPartners.Count() > 0)
                                                                        {
                                                                            double _locTotalAmountDebit = 0;
                                                                            double _locTotalAmountCredit = 0;
                                                                            double _locTotalBalance = 0;

                                                                            foreach (AccountMapLine _locAccountMapLine in _locAccountMapLineByBusinessPartners)
                                                                            {
                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                {
                                                                                    _locTotalAmountDebit = _locDiscOfAmount;
                                                                                }
                                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                {
                                                                                    _locTotalAmountCredit = _locDiscOfAmount;
                                                                                }

                                                                                GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                {
                                                                                    PostingDate = now,
                                                                                    PostingType = PostingType.Sales,
                                                                                    OrderType = _locSalesInvoiceLine.SalesType,
                                                                                    PostingMethod = PostingMethod.InvoiceAR,
                                                                                    PostingMethodType = PostingMethodType.Discount,
                                                                                    Account = _locAccountMapLine.Account,
                                                                                    Debit = _locTotalAmountDebit,
                                                                                    Credit = _locTotalAmountCredit,
                                                                                    SalesOrder = _locSalesInvoiceLine.SalesOrderMonitoring.SalesOrder,
                                                                                    SalesInvoice = _locSalesInvoiceXPO,
                                                                                    Company = _locSalesInvoiceXPO.Company,
                                                                                };
                                                                                _saveGeneralJournal.Save();
                                                                                _saveGeneralJournal.Session.CommitTransaction();

                                                                                if (_locAccountMapLine.Account.Code != null)
                                                                                {
                                                                                    ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                             (new GroupOperator(GroupOperatorType.And,
                                                                                                              new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                                    if (_locCOA != null)
                                                                                    {
                                                                                        if (_locCOA.BalanceType == BalanceType.Change)
                                                                                        {
                                                                                            if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                            if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                            {
                                                                                                if (_locTotalAmountDebit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                                }
                                                                                                if (_locTotalAmountCredit > 0)
                                                                                                {
                                                                                                    _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                                }
                                                                                            }
                                                                                        }

                                                                                        _locCOA.Balance = _locTotalBalance;
                                                                                        _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                        _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                        _locCOA.Save();
                                                                                        _locCOA.Session.CommitTransaction();
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            #endregion JournalMapBusinessPartnerAccountGroup
                                        }

                                    }
                                    #endregion CreateInvoiceARWithDiscount
                                }
                            }

                        }
                    }

                    #endregion CreateInvoiceARJournal
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesInvoice ", ex.ToString());
            }
        }

        private void SetPaymentInPlan(Session _currSession, SalesInvoice _locSalesInvoiceXPO, SalesInvoiceMonitoring _locSalesInvoiceMonitoringXPO) 
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locSalesInvoiceXPO != null && _locSalesInvoiceMonitoringXPO != null)
                {
                    PaymentInPlan _locPaymentInPlan = _currSession.FindObject<PaymentInPlan>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("SalesInvoiceMonitoring", _locSalesInvoiceMonitoringXPO)));
                    if(_locPaymentInPlan == null)
                    {
                        PaymentInPlan _savePaymentInPlan = new PaymentInPlan(_currSession)
                        {
                            PaymentMethod = _locSalesInvoiceXPO.PaymentMethod,
                            PaymentMethodType = _locSalesInvoiceXPO.PaymentMethodType,
                            PaymentType = _locSalesInvoiceXPO.PaymentType,
                            EstimatedDate = _locSalesInvoiceXPO.EstimatedDate,
                            Plan = _locSalesInvoiceMonitoringXPO.TAmount,
                            Outstanding = _locSalesInvoiceMonitoringXPO.TAmount,
                            SalesOrder = _locSalesInvoiceMonitoringXPO.SalesOrderMonitoring.SalesOrder,
                            SalesInvoice = _locSalesInvoiceXPO,
                            SalesInvoiceMonitoring = _locSalesInvoiceMonitoringXPO,
                            Company = _locSalesInvoiceXPO.Company,
                        };
                        _savePaymentInPlan.Save();
                        _savePaymentInPlan.Session.CommitTransaction();
                    } 
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesInvoice ", ex.ToString());
            }
        }

        private void SetNormalBill(Session _currSession, SalesInvoice _locSalesInvoiceXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locSalesInvoiceXPO != null)
                {
                    _locSalesInvoiceXPO.Bill = 0;
                    _locSalesInvoiceXPO.Save();
                    _locSalesInvoiceXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesInvoice ", ex.ToString());
            }
        }

        private void SetRemainQty(Session _currSession, SalesInvoice _locSalesInvoiceXPO)
        {
            try
            {
                if (_locSalesInvoiceXPO != null)
                {
                    XPCollection<SalesInvoiceLine> _locSalesInvoiceLines = new XPCollection<SalesInvoiceLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Lock),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locSalesInvoiceLines != null && _locSalesInvoiceLines.Count > 0)
                    {
                        double _locRmDQty = 0;
                        double _locRmQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (SalesInvoiceLine _locSalesInvoiceLine in _locSalesInvoiceLines)
                        {
                            #region ProcessCount=0
                            if (_locSalesInvoiceLine.ProcessCount == 0)
                            {
                                #region MaxQuantity
                                if (_locSalesInvoiceLine.MxDQty > 0)
                                {
                                    if (_locSalesInvoiceLine.DQty > 0 && _locSalesInvoiceLine.DQty <= _locSalesInvoiceLine.MxDQty)
                                    {
                                        _locRmDQty = _locSalesInvoiceLine.MxDQty - _locSalesInvoiceLine.DQty;
                                    }

                                    if (_locSalesInvoiceLine.Qty > 0 && _locSalesInvoiceLine.Qty <= _locSalesInvoiceLine.MxQty)
                                    {
                                        _locRmQty = _locSalesInvoiceLine.MxQty - _locSalesInvoiceLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locSalesInvoiceLine.Item),
                                                                new BinaryOperator("UOM", _locSalesInvoiceLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locSalesInvoiceLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locSalesInvoiceLine.DQty > 0)
                                    {
                                        _locRmDQty = 0;
                                    }

                                    if (_locSalesInvoiceLine.Qty > 0)
                                    {
                                        _locRmQty = 0;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locSalesInvoiceLine.Item),
                                                                new BinaryOperator("UOM", _locSalesInvoiceLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locSalesInvoiceLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locSalesInvoiceLine.ProcessCount > 0)
                            {
                                if (_locSalesInvoiceLine.RmDQty > 0)
                                {
                                    _locRmDQty = _locSalesInvoiceLine.RmDQty - _locSalesInvoiceLine.DQty;
                                }

                                if (_locSalesInvoiceLine.RmQty > 0)
                                {
                                    _locRmQty = _locSalesInvoiceLine.RmQty - _locSalesInvoiceLine.Qty;
                                }

                                if (_locSalesInvoiceLine.MxDQty > 0 || _locSalesInvoiceLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locSalesInvoiceLine.Item),
                                                                new BinaryOperator("UOM", _locSalesInvoiceLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locSalesInvoiceLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locSalesInvoiceLine.Item),
                                                                new BinaryOperator("UOM", _locSalesInvoiceLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locSalesInvoiceLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                }


                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locSalesInvoiceLine.RmDQty = _locRmDQty;
                            _locSalesInvoiceLine.RmQty = _locRmQty;
                            _locSalesInvoiceLine.RmTQty = _locInvLineTotal;
                            _locSalesInvoiceLine.Save();
                            _locSalesInvoiceLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoice " + ex.ToString());
            }
        }

        private void SetPostingQty(Session _currSession, SalesInvoice _locSalesInvoiceXPO)
        {
            try
            {
                if (_locSalesInvoiceXPO != null)
                {
                    XPCollection<SalesInvoiceLine> _locSalesInvoiceLines = new XPCollection<SalesInvoiceLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Lock),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locSalesInvoiceLines != null && _locSalesInvoiceLines.Count > 0)
                    {
                        double _locPDQty = 0;
                        double _locPQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (SalesInvoiceLine _locSalesInvoiceLine in _locSalesInvoiceLines)
                        {
                            #region ProcessCount=0
                            if (_locSalesInvoiceLine.ProcessCount == 0)
                            {
                                #region MaxQuantity
                                if (_locSalesInvoiceLine.MxDQty > 0)
                                {
                                    if (_locSalesInvoiceLine.DQty > 0 && _locSalesInvoiceLine.DQty <= _locSalesInvoiceLine.MxDQty)
                                    {
                                        _locPDQty = _locSalesInvoiceLine.DQty;
                                    }

                                    if (_locSalesInvoiceLine.Qty > 0 && _locSalesInvoiceLine.Qty <= _locSalesInvoiceLine.MxQty)
                                    {
                                        _locPQty = _locSalesInvoiceLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locSalesInvoiceLine.Item),
                                                                new BinaryOperator("UOM", _locSalesInvoiceLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locSalesInvoiceLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locSalesInvoiceLine.DQty > 0)
                                    {
                                        _locPDQty = _locSalesInvoiceLine.DQty;
                                    }

                                    if (_locSalesInvoiceLine.Qty > 0)
                                    {
                                        _locPQty = _locSalesInvoiceLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locSalesInvoiceLine.Item),
                                                                new BinaryOperator("UOM", _locSalesInvoiceLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locSalesInvoiceLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locSalesInvoiceLine.ProcessCount > 0)
                            {
                                if (_locSalesInvoiceLine.PDQty > 0)
                                {
                                    _locPDQty = _locSalesInvoiceLine.PDQty + _locSalesInvoiceLine.DQty;
                                }

                                if (_locSalesInvoiceLine.PQty > 0)
                                {
                                    _locPQty = _locSalesInvoiceLine.PQty + _locSalesInvoiceLine.Qty;
                                }

                                if (_locSalesInvoiceLine.MxDQty > 0 || _locSalesInvoiceLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locSalesInvoiceLine.Item),
                                                            new BinaryOperator("UOM", _locSalesInvoiceLine.MxUOM),
                                                            new BinaryOperator("DefaultUOM", _locSalesInvoiceLine.MxDUOM),
                                                            new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locSalesInvoiceLine.Item),
                                                            new BinaryOperator("UOM", _locSalesInvoiceLine.UOM),
                                                            new BinaryOperator("DefaultUOM", _locSalesInvoiceLine.DUOM),
                                                            new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locSalesInvoiceLine.PDQty = _locPDQty;
                            _locSalesInvoiceLine.PDUOM = _locSalesInvoiceLine.DUOM;
                            _locSalesInvoiceLine.PQty = _locPQty;
                            _locSalesInvoiceLine.PUOM = _locSalesInvoiceLine.UOM;
                            _locSalesInvoiceLine.PTQty = _locInvLineTotal;
                            _locSalesInvoiceLine.Save();
                            _locSalesInvoiceLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoice " + ex.ToString());
            }

        }

        private void SetProcessCount(Session _currSession, SalesInvoice _locSalesInvoiceXPO)
        {
            try
            {
                if (_locSalesInvoiceXPO != null)
                {
                    XPCollection<SalesInvoiceLine> _locSalesInvoiceLines = new XPCollection<SalesInvoiceLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Lock),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locSalesInvoiceLines != null && _locSalesInvoiceLines.Count > 0)
                    {

                        foreach (SalesInvoiceLine _locSalesInvoiceLine in _locSalesInvoiceLines)
                        {
                            _locSalesInvoiceLine.ProcessCount = _locSalesInvoiceLine.ProcessCount + 1;
                            _locSalesInvoiceLine.Save();
                            _locSalesInvoiceLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoice " + ex.ToString());
            }
        }

        private void SetStatusSalesInvoiceLine(Session _currSession, SalesInvoice _locSalesInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locSalesInvoiceXPO != null)
                {
                    XPCollection<SalesInvoiceLine> _locSalesInvoiceLines = new XPCollection<SalesInvoiceLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Lock),
                                                            new BinaryOperator("Status", Status.Posted))));

                    if (_locSalesInvoiceLines != null && _locSalesInvoiceLines.Count > 0)
                    {

                        foreach (SalesInvoiceLine _locSalesInvoiceLine in _locSalesInvoiceLines)
                        {
                            if (_locSalesInvoiceLine.RmDQty == 0 && _locSalesInvoiceLine.RmQty == 0 && _locSalesInvoiceLine.RmTQty == 0)
                            {
                                _locSalesInvoiceLine.Status = Status.Close;
                                _locSalesInvoiceLine.ActivationPosting = true;
                                _locSalesInvoiceLine.StatusDate = now;
                            }
                            else
                            {
                                _locSalesInvoiceLine.Status = Status.Posted;
                                _locSalesInvoiceLine.StatusDate = now;
                            }
                            _locSalesInvoiceLine.Save();
                            _locSalesInvoiceLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoice " + ex.ToString());
            }
        }

        private void SetNormalQuantity(Session _currSession, SalesInvoice _locSalesInvoiceXPO)
        {
            try
            {
                if (_locSalesInvoiceXPO != null)
                {
                    XPCollection<SalesInvoiceLine> _locSalesInvoiceLines = new XPCollection<SalesInvoiceLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                                                     new BinaryOperator("Select", true),
                                                                                     new GroupOperator(GroupOperatorType.Or,
                                                                                     new BinaryOperator("Status", Status.Lock),
                                                                                     new BinaryOperator("Status", Status.Posted),
                                                                                     new BinaryOperator("Status", Status.Close))));

                    if (_locSalesInvoiceLines != null && _locSalesInvoiceLines.Count > 0)
                    {
                        foreach (SalesInvoiceLine _locSalesInvoiceLine in _locSalesInvoiceLines)
                        {
                            if (_locSalesInvoiceLine.DQty > 0 || _locSalesInvoiceLine.Qty > 0)
                            {
                                _locSalesInvoiceLine.Select = false;
                                _locSalesInvoiceLine.DQty = 0;
                                _locSalesInvoiceLine.Qty = 0;
                                _locSalesInvoiceLine.Save();
                                _locSalesInvoiceLine.Session.CommitTransaction();
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoice " + ex.ToString());
            }
        }

        private void SetFinalSalesInvoice(Session _currSession, SalesInvoice _locSalesInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locPurchInvLineCount = 0;
          
                if (_locSalesInvoiceXPO != null)
                {

                    XPCollection<SalesInvoiceLine> _locSalesInvoiceLines = new XPCollection<SalesInvoiceLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO)));

                    if (_locSalesInvoiceLines != null && _locSalesInvoiceLines.Count() > 0)
                    {
                        _locPurchInvLineCount = _locSalesInvoiceLines.Count();

                        foreach (SalesInvoiceLine _locSalesInvoiceLine in _locSalesInvoiceLines)
                        {
                            if (_locSalesInvoiceLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }
                    }

                    if (_locStatusCount == _locPurchInvLineCount )
                    {
                        _locSalesInvoiceXPO.ActivationPosting = true;
                        _locSalesInvoiceXPO.Status = Status.Posted;
                        _locSalesInvoiceXPO.StatusDate = now;
                        _locSalesInvoiceXPO.Save();
                        _locSalesInvoiceXPO.Session.CommitTransaction();
                    }
                    else
                    {
                        _locSalesInvoiceXPO.Status = Status.Posted;
                        _locSalesInvoiceXPO.StatusDate = now;
                        _locSalesInvoiceXPO.Save();
                        _locSalesInvoiceXPO.Session.CommitTransaction();
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        private void SetAfterPostingReport(Session _currSession, SalesInvoice _locSalesInvoiceXPO)
        {
            //Memindahkan Nilai TotalPIL, TUAmount, ReportTPIL, ReportTDisc, ReportTaxPIL ke field Posting Untuk Report After Posting
            try
            {
                DateTime now = DateTime.Now;

                if (_locSalesInvoiceXPO != null)
                {
                    XPCollection<SalesInvoiceLine> _locSalesInvoiceLines = new XPCollection<SalesInvoiceLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO)));

                    if (_locSalesInvoiceLines != null && _locSalesInvoiceLines.Count() > 0)
                    {
                        foreach (SalesInvoiceLine _locSalesInvoiceLine in _locSalesInvoiceLines)
                        {
                            _locSalesInvoiceLine.PTotalPIL = _locSalesInvoiceLine.TotalSIL;
                            _locSalesInvoiceLine.PTUAmount = _locSalesInvoiceLine.TUAmount;
                            _locSalesInvoiceLine.PTAmountPIL = _locSalesInvoiceLine.TAmountSIL;
                            _locSalesInvoiceLine.PTPIL = _locSalesInvoiceLine.ReportTSIl;
                            _locSalesInvoiceLine.PDisc = _locSalesInvoiceLine.ReportDisc;
                            _locSalesInvoiceLine.PTax = _locSalesInvoiceLine.ReportTax;
                            _locSalesInvoiceLine.Save();
                            _locSalesInvoiceLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoice " + ex.ToString());
            }
        }

        #endregion PostingMethod

        #region GetITO

        private void GetInventoryTransferOutMonitoring(Session _currSession, InventoryTransferOut _locInventoryTransferOutXPO, SalesInvoice _locSalesInvoiceXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;
                double _locTxValue = 0;
                double _locTxAmount = 0;
                double _locDisc = 0;
                double _locDiscAmount = 0;
                double _locTAmount = 0;

                if (_locInventoryTransferOutXPO != null && _locSalesInvoiceXPO != null)
                {
                    //Harus mampu menghitung otomatis buat MultiTax dan MultiDiscount
                    XPCollection<InventoryTransferOutMonitoring> _locInventoryTransferOutMonitorings = new XPCollection<InventoryTransferOutMonitoring>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("InventoryTransferOut", _locInventoryTransferOutXPO),
                                                                                        new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                                                        new BinaryOperator("Select", true),
                                                                                        new BinaryOperator("PostedCount", 0)));
                    if (_locInventoryTransferOutMonitorings != null && _locInventoryTransferOutMonitorings.Count() > 0)
                    {
                        foreach(InventoryTransferOutMonitoring _locInventoryTransferOutMonitoring in _locInventoryTransferOutMonitorings)
                        {
                            if(_locInventoryTransferOutMonitoring.InventoryTransferOutLine != null && _locInventoryTransferOutMonitoring.SalesOrderMonitoring != null
                                && _locInventoryTransferOutMonitoring.SalesOrderMonitoring.SalesOrderLine != null)
                            {
                                _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.InventoryTransferOutLine);

                                if(_currSignCode != null)
                                {
                                    //Count Tax
                                    if (_locInventoryTransferOutMonitoring.SalesOrderMonitoring.MultiTax == true)
                                    {
                                        _locTxValue = GetSumTotalTaxValue(_currSession, _locInventoryTransferOutMonitoring.SalesOrderMonitoring.SalesOrderLine);
                                        _locTxAmount = GetSumTotalTaxAmount(_currSession, _locInventoryTransferOutMonitoring.SalesOrderMonitoring.SalesOrderLine, _locInventoryTransferOutMonitoring);
                                    }
                                    else
                                    {
                                        if (_locInventoryTransferOutMonitoring.SalesOrderMonitoring.TxValue > 0)
                                        {
                                            _locTxValue = _locInventoryTransferOutMonitoring.SalesOrderMonitoring.SalesOrderLine.TxValue;
                                            _locTxAmount = (_locTxValue / 100) * (_locInventoryTransferOutMonitoring.TQty * _locInventoryTransferOutMonitoring.SalesOrderMonitoring.UAmount);
                                        }
                                    }

                                    //Count Discount
                                    if (_locInventoryTransferOutMonitoring.SalesOrderMonitoring.MultiDiscount == true)
                                    {
                                        _locDisc = GetSumTotalDisc(_currSession, _locInventoryTransferOutMonitoring.SalesOrderMonitoring.SalesOrderLine);
                                        _locDiscAmount = GetSumTotalDiscAmount(_currSession, _locInventoryTransferOutMonitoring.SalesOrderMonitoring.SalesOrderLine, _locInventoryTransferOutMonitoring);
                                    }
                                    else
                                    {
                                        if (_locInventoryTransferOutMonitoring.SalesOrderMonitoring.Disc > 0)
                                        {
                                            _locDisc = _locInventoryTransferOutMonitoring.SalesOrderMonitoring.SalesOrderLine.Disc;
                                            _locDiscAmount = (_locDisc / 100) * (_locInventoryTransferOutMonitoring.TQty * _locInventoryTransferOutMonitoring.SalesOrderMonitoring.UAmount);
                                        }
                                    }

                                    _locTAmount = (_locInventoryTransferOutMonitoring.SalesOrderMonitoring.UAmount * _locInventoryTransferOutMonitoring.TQty) + _locTxAmount - _locDiscAmount;
                                    //Tempat untuk menghitung multitax dan multidiscount
                                    SalesInvoiceLine _saveDataSalesInvoiceLine = new SalesInvoiceLine(_currSession)
                                    {
                                        SignCode = _currSignCode,
                                        SalesType = _locInventoryTransferOutMonitoring.SalesOrderMonitoring.SalesOrderLine.SalesType,
                                        Item = _locInventoryTransferOutMonitoring.Item,
                                        Description = _locInventoryTransferOutMonitoring.InventoryTransferOutLine.Description,
                                        MxDQty = _locInventoryTransferOutMonitoring.DQty,
                                        MxDUOM = _locInventoryTransferOutMonitoring.DUOM,
                                        MxQty = _locInventoryTransferOutMonitoring.Qty,
                                        MxUOM = _locInventoryTransferOutMonitoring.UOM,
                                        MxTQty = _locInventoryTransferOutMonitoring.TQty,
                                        MxUAmount = _locInventoryTransferOutMonitoring.SalesOrderMonitoring.UAmount,
                                        MxTUAmount = _locInventoryTransferOutMonitoring.TQty * _locInventoryTransferOutMonitoring.SalesOrderMonitoring.UAmount,
                                        DQty = _locInventoryTransferOutMonitoring.DQty,
                                        DUOM = _locInventoryTransferOutMonitoring.DUOM,
                                        Qty = _locInventoryTransferOutMonitoring.Qty,
                                        UOM = _locInventoryTransferOutMonitoring.UOM,
                                        TQty = _locInventoryTransferOutMonitoring.TQty,
                                        Currency = _locInventoryTransferOutMonitoring.SalesOrderMonitoring.Currency,
                                        PriceGroup = _locInventoryTransferOutMonitoring.SalesOrderMonitoring.PriceGroup,
                                        Price = _locInventoryTransferOutMonitoring.SalesOrderMonitoring.Price,
                                        PriceLine = _locInventoryTransferOutMonitoring.SalesOrderMonitoring.PriceLine,
                                        UAmount = _locInventoryTransferOutMonitoring.SalesOrderMonitoring.UAmount,
                                        TUAmount = _locInventoryTransferOutMonitoring.TQty * _locInventoryTransferOutMonitoring.SalesOrderMonitoring.UAmount,
                                        MultiTax = _locInventoryTransferOutMonitoring.SalesOrderMonitoring.MultiTax,
                                        Tax = _locInventoryTransferOutMonitoring.SalesOrderMonitoring.Tax,
                                        TxValue = _locTxValue,
                                        TxAmount = _locTxAmount,
                                        MultiDiscount = _locInventoryTransferOutMonitoring.SalesOrderMonitoring.MultiDiscount,
                                        Discount = _locInventoryTransferOutMonitoring.SalesOrderMonitoring.Discount,
                                        Disc = _locDisc,
                                        DiscAmount = _locDiscAmount,
                                        TAmount = _locTAmount,
                                        InventoryTransferOutMonitoring = _locInventoryTransferOutMonitoring,
                                        SalesOrderMonitoring = _locInventoryTransferOutMonitoring.SalesOrderMonitoring,
                                        SalesInvoice = _locSalesInvoiceXPO,
                                    };
                                    _saveDataSalesInvoiceLine.Save();
                                    _saveDataSalesInvoiceLine.Session.CommitTransaction();

                                    SalesInvoiceLine _locSalInvoiceLine = _currSession.FindObject<SalesInvoiceLine>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("SignCode", _currSignCode)));
                                    if(_locSalInvoiceLine != null)
                                    {
                                        #region TaxLine
                                        if (_locInventoryTransferOutMonitoring.SalesOrderMonitoring.SalesOrderLine.MultiTax == true)
                                        {
                                            XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession, new GroupOperator(
                                                                                GroupOperatorType.And,
                                                                                new BinaryOperator("SalesOrderLine", _locInventoryTransferOutMonitoring.SalesOrderMonitoring.SalesOrderLine)));
                                            if (_locTaxLines != null && _locTaxLines.Count() > 0)
                                            {
                                                foreach (TaxLine _locTaxLine in _locTaxLines)
                                                {
                                                    TaxLine _saveDataTaxLine = new TaxLine(_currSession)
                                                    {
                                                        Tax = _locTaxLine.Tax,
                                                        Name = _locTaxLine.Name,
                                                        TaxType = _locTaxLine.TaxType,
                                                        TaxGroup = _locTaxLine.TaxGroup,
                                                        TaxGroupType = _locTaxLine.TaxGroupType,
                                                        TaxAccountGroup = _locTaxLine.TaxAccountGroup,
                                                        TaxNature = _locTaxLine.TaxNature,
                                                        TaxMode = _locTaxLine.TaxMode,
                                                        TxValue = _locTaxLine.TxValue,
                                                        Description = _locTaxLine.Description,
                                                        UAmount = _locSalInvoiceLine.UAmount,
                                                        TUAmount = _locSalInvoiceLine.TUAmount,
                                                        TxAmount = _locSalInvoiceLine.TxAmount,
                                                        SalesInvoiceLine = _locSalInvoiceLine,
                                                    };
                                                    _saveDataTaxLine.Save();
                                                    _saveDataTaxLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        #endregion TaxLine
                                        #region DiscountLine
                                        if (_locInventoryTransferOutMonitoring.SalesOrderMonitoring.SalesOrderLine.MultiDiscount == true)
                                        {
                                            XPCollection<DiscountLine> _locDiscountLines = new XPCollection<DiscountLine>(_currSession,
                                                                                new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("SalesOrderLine", _locInventoryTransferOutMonitoring.SalesOrderMonitoring.SalesOrderLine)));
                                            if (_locDiscountLines != null && _locDiscountLines.Count() > 0)
                                            {
                                                foreach (DiscountLine _locDiscountLine in _locDiscountLines)
                                                {
                                                    DiscountLine _saveDataDiscountLine = new DiscountLine(_currSession)
                                                    {
                                                        Discount = _locDiscountLine.Discount,
                                                        Name = _locDiscountLine.Name,
                                                        DiscountMode = _locDiscountLine.DiscountMode,
                                                        Description = _locDiscountLine.Description,
                                                        UAmount = _locSalInvoiceLine.UAmount,
                                                        Disc = _locDiscountLine.Disc,
                                                        DiscAmount = _locSalInvoiceLine.DiscAmount,
                                                        TUAmount = _locSalInvoiceLine.TUAmount,
                                                        SalesInvoiceLine = _locSalInvoiceLine,
                                                    };
                                                    _saveDataDiscountLine.Save();
                                                    _saveDataDiscountLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        #endregion DiscountLine

                                        _locInventoryTransferOutMonitoring.SalesInvoiceLine = _locSalInvoiceLine;
                                        _locInventoryTransferOutMonitoring.Status = Status.Close;
                                        _locInventoryTransferOutMonitoring.StatusDate = now;
                                        _locInventoryTransferOutMonitoring.PostedCount = _locInventoryTransferOutMonitoring.PostedCount + 1;
                                        _locInventoryTransferOutMonitoring.Save();
                                        _locInventoryTransferOutMonitoring.Session.CommitTransaction();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesInvoice ", ex.ToString());
            }
        }

        private double GetSumTotalTaxValue(Session _currSession, SalesOrderLine _salesOrderLineXPO)
        {
            double _result = 0;
            try
            {
                double _locTxValue = 0;

                if (_salesOrderLineXPO != null)
                {
                    XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession,
                                                     new BinaryOperator("SalesOrderLine", _salesOrderLineXPO));

                    if (_locTaxLines.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLine in _locTaxLines)
                        {
                            if (_locTaxLine.TxValue > 0 && _locTaxLine.TxAmount > 0)
                            {
                                if (_locTaxLine.TaxNature == TaxNature.Increase)
                                {
                                    _locTxValue = _locTxValue + _locTaxLine.TxValue;

                                }
                                else if (_locTaxLine.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxValue = _locTxValue - _locTaxLine.TxValue;
                                }
                            }
                        }
                        _result = _locTxValue;
                    }
                }


            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesInvoice " + ex.ToString());
            }
            return _result;
        }

        private double GetSumTotalTaxAmount(Session _currSession, SalesOrderLine _salesOrderLineXPO, InventoryTransferOutMonitoring _inventoryTransferOutMonitoringXPO)
        {
            double _return = 0;
            try
            {
                double _locTxAmount = 0;

                if (_salesOrderLineXPO != null)
                {
                    XPCollection<TaxLine> _locTaxLines = new XPCollection<TaxLine>(_currSession,
                                                     new BinaryOperator("SalesOrderLine", _salesOrderLineXPO));

                    if (_locTaxLines.Count() > 0)
                    {
                        foreach (TaxLine _locTaxLine in _locTaxLines)
                        {
                            if (_locTaxLine.TxValue > 0)
                            {
                                if (_locTaxLine.TaxNature == TaxNature.Increase)
                                {
                                    _locTxAmount = _locTxAmount + (_salesOrderLineXPO.UAmount * _inventoryTransferOutMonitoringXPO.TQty * _locTaxLine.TxValue / 100);
                                }
                                else if (_locTaxLine.TaxNature == TaxNature.Decrease)
                                {
                                    _locTxAmount = _locTxAmount - (_salesOrderLineXPO.UAmount * _inventoryTransferOutMonitoringXPO.TQty * _locTaxLine.TxValue / 100);
                                }
                            }
                        }
                        _return = _locTxAmount;
                    }
                }


            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesInvoice" + ex.ToString());
            }
            return _return;
        }

        private double GetSumTotalDisc(Session _currSession, SalesOrderLine _salesOrderLineXPO)
        {
            double _result = 0;
            try
            {
                double _locDisc = 0;


                if (_salesOrderLineXPO != null)
                {
                    XPCollection<DiscountLine> _locDiscountLines = new XPCollection<DiscountLine>(_currSession,
                                                               new BinaryOperator("SalesOrderLine", _salesOrderLineXPO));

                    if (_locDiscountLines.Count() > 0)
                    {
                        foreach (DiscountLine _locDiscountLine in _locDiscountLines)
                        {
                            if (_locDiscountLine.Disc > 0 && _locDiscountLine.DiscAmount > 0)
                            {
                                if (_locDiscountLine.Disc > 0 && _locDiscountLine.DiscAmount > 0)
                                {
                                    _locDisc = _locDisc + _locDiscountLine.Disc;
                                }
                            }
                        }
                        _result = _locDisc;
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesInvoice" + ex.ToString());
            }
            return _result;
        }

        private double GetSumTotalDiscAmount(Session _currSession, SalesOrderLine _salesOrderLineXPO, InventoryTransferOutMonitoring _inventoryTransferOutMonitoringXPO)
        {
            double _result = 0;
            try
            {
                double _locDiscAmount = 0;

                if (_salesOrderLineXPO != null)
                {
                    XPCollection<DiscountLine> _locDiscountLines = new XPCollection<DiscountLine>(_currSession,
                                                               new BinaryOperator("SalesOrderLine", _salesOrderLineXPO));

                    if (_locDiscountLines.Count() > 0)
                    {
                        foreach (DiscountLine _locDiscountLine in _locDiscountLines)
                        {
                            if (_locDiscountLine.Disc > 0)
                            {
                                _locDiscAmount = _locDiscAmount + (_salesOrderLineXPO.UAmount * _inventoryTransferOutMonitoringXPO.TQty * _locDiscountLine.Disc / 100);
                            }
                        }
                        _result = _locDiscAmount;
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesInvoice" + ex.ToString());
            }
            return _result;
        }

        #endregion GetITO

        #region Approval Process

        private void SetApprovalLine(Session _currentSession, SalesInvoice _locSalesInvoiceXPO, ApprovalLevel _locApprovalLevel)
        {
            try
            {
                DateTime now = DateTime.Now;

                ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("SalesInvoice", _locSalesInvoiceXPO),
                                                                         new BinaryOperator("ApprovalLevel", _locApprovalLevel)));

                if (_locApprovalLineXPO == null)
                {
                    ApprovalLine _saveDataAL2 = new ApprovalLine(_currentSession)
                    {
                        ApprovalDate = now,
                        ApprovalStatus = Status.Approved,
                        ApprovalLevel = _locApprovalLevel,
                        SalesInvoice = _locSalesInvoiceXPO,
                    };
                    _saveDataAL2.Save();
                    _saveDataAL2.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesQuotation " + ex.ToString());
            }
        }

        #endregion Approval Process

        #region Email

        private string BackgroundBody(Session _currentSession, SalesInvoice _locSalesInvoiceXPO, MailSetupDetail _locMailSetDetail)
        {
            string body = string.Empty;

            SalesInvoice _locSalesInvoice = _currentSession.FindObject<SalesInvoice>
                                           (new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Code", _locSalesInvoiceXPO.Code)));

            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                               (new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("SalesInvoice", _locSalesInvoice)));

            string Status = _locApprovalLineXPO.ApprovalStatus.ToString();

            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Email/EmailTemplate.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{Code}", _locSalesInvoiceXPO.Code);

            #region Level
            if (_locSalesInvoice.ActiveApproved1 == true)
            {
                string Active = "Level 1";
                body = body.Replace("{Level}", Active);
            }
            else if (_locSalesInvoice.ActiveApproved2 == true)
            {
                string Active = "Level 2";
                body = body.Replace("{Level}", Active);
            }
            else if (_locSalesInvoice.ActiveApproved3 == true)
            {
                string Active = "Level 3";
                body = body.Replace("{Level}", Active);
            }
            else
            {
                string Active = "Not Available";
                body = body.Replace("{Level}", Active);
            }
            #endregion Level

            body = body.Replace("{Status}", Status);
            body = body.Replace("{Message}", _locMailSetDetail.MessageBody);
            return body;
        }

        protected void SendEmail(Session _currentSession, SalesInvoice _locSalesInvoiceXPO, Status _locStatus, ApprovalLine _locApprovalLineXPO2)
        {
            DateTime now = DateTime.Now;
            var User = SecuritySystem.CurrentUserName;
            GlobalFunction _globFunc = new GlobalFunction();
            UserAccess _numLineUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));
            string _locMessageBody = null;

            try
            {
                if (_locSalesInvoiceXPO != null)
                {
                    XPCollection<MailSetupDetail> _locMailSetDetails = new XPCollection<MailSetupDetail>
                                                                       (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ObjectList", ObjectList.SalesOrder),
                                                                        new BinaryOperator("UserAccess", _numLineUserAccess),
                                                                        new BinaryOperator("Active", true)));

                    if (_locMailSetDetails != null && _locMailSetDetails.Count > 0)
                    {
                        foreach (MailSetupDetail _locMailSetDetail in _locMailSetDetails)
                        {
                            if (_locMailSetDetail.MessageBody != null)
                            {
                                if (_locApprovalLineXPO2.EndApproval == true)
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locSalesInvoiceXPO, _locMailSetDetail);
                                }
                                else
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locSalesInvoiceXPO, _locMailSetDetail);
                                }
                            }

                            _globFunc.SetAndSendMail(_locMailSetDetail.SmtpHost, _locMailSetDetail.SmtpPort,
                            _locMailSetDetail.MailFrom, _locMailSetDetail.MailFromPassword, _locMailSetDetail.MailTo, _locMailSetDetail.MailSubject, _locMessageBody);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesInvoice " + ex.ToString());
            }
        }

        #endregion Email

        #region Global

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Error;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global

        
    }
}
