﻿namespace FullDrive.Module.Controllers
{
    partial class PaymentRealizationActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PaymentRealizationProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PaymentRealizationPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PaymentRealizationListviewFilterApprovalSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // PaymentRealizationProgressAction
            // 
            this.PaymentRealizationProgressAction.Caption = "Progress";
            this.PaymentRealizationProgressAction.ConfirmationMessage = null;
            this.PaymentRealizationProgressAction.Id = "PaymentRealizationProgressActionId";
            this.PaymentRealizationProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PaymentRealization);
            this.PaymentRealizationProgressAction.ToolTip = null;
            this.PaymentRealizationProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PaymentRealizationProgressAction_Execute);
            // 
            // PaymentRealizationPostingAction
            // 
            this.PaymentRealizationPostingAction.Caption = "Posting";
            this.PaymentRealizationPostingAction.ConfirmationMessage = null;
            this.PaymentRealizationPostingAction.Id = "PaymentRealizationPostingActionId";
            this.PaymentRealizationPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PaymentRealization);
            this.PaymentRealizationPostingAction.ToolTip = null;
            this.PaymentRealizationPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PaymentRealizationPostingAction_Execute);
            // 
            // PaymentRealizationListviewFilterApprovalSelectionAction
            // 
            this.PaymentRealizationListviewFilterApprovalSelectionAction.Caption = "Filter Approval";
            this.PaymentRealizationListviewFilterApprovalSelectionAction.ConfirmationMessage = null;
            this.PaymentRealizationListviewFilterApprovalSelectionAction.Id = "PaymentRealizationListviewFilterApprovalSelectionActionId";
            this.PaymentRealizationListviewFilterApprovalSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.PaymentRealizationListviewFilterApprovalSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PaymentRealization);
            this.PaymentRealizationListviewFilterApprovalSelectionAction.ToolTip = null;
            this.PaymentRealizationListviewFilterApprovalSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.PaymentRealizationListviewFilterApprovalSelectionAction_Execute);
            // 
            // PaymentRealizationActionController
            // 
            this.Actions.Add(this.PaymentRealizationProgressAction);
            this.Actions.Add(this.PaymentRealizationPostingAction);
            this.Actions.Add(this.PaymentRealizationListviewFilterApprovalSelectionAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction PaymentRealizationProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PaymentRealizationPostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction PaymentRealizationListviewFilterApprovalSelectionAction;
    }
}
