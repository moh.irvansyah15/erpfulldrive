﻿namespace FullDrive.Module.Controllers
{
    partial class TempBeginingInventoryLineActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TempBeginingInventoryLineProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.TempBeginingInventoryLinePostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // TempBeginingInventoryLineProgressAction
            // 
            this.TempBeginingInventoryLineProgressAction.Caption = "Progress";
            this.TempBeginingInventoryLineProgressAction.ConfirmationMessage = null;
            this.TempBeginingInventoryLineProgressAction.Id = "TempBeginingInventoryLineProgressActionId";
            this.TempBeginingInventoryLineProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.TempBeginingInventoryLine);
            this.TempBeginingInventoryLineProgressAction.ToolTip = null;
            this.TempBeginingInventoryLineProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.TempBeginingInventoryLineProgressAction_Execute);
            // 
            // TempBeginingInventoryLinePostingAction
            // 
            this.TempBeginingInventoryLinePostingAction.Caption = "Posting";
            this.TempBeginingInventoryLinePostingAction.ConfirmationMessage = null;
            this.TempBeginingInventoryLinePostingAction.Id = "TempBeginingInventoryLinePostingActionId";
            this.TempBeginingInventoryLinePostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.TempBeginingInventoryLine);
            this.TempBeginingInventoryLinePostingAction.ToolTip = null;
            this.TempBeginingInventoryLinePostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.TempBeginingInventoryLinePostingAction_Execute);
            // 
            // TempBeginingInventoryLineActionController
            // 
            this.Actions.Add(this.TempBeginingInventoryLineProgressAction);
            this.Actions.Add(this.TempBeginingInventoryLinePostingAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction TempBeginingInventoryLineProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction TempBeginingInventoryLinePostingAction;
    }
}
