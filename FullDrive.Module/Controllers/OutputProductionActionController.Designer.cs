﻿namespace FullDrive.Module.Controllers
{
    partial class OutputProductionActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.OutputProductionProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.OutputProductionPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.OutputProductionFilterAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // OutputProductionProgressAction
            // 
            this.OutputProductionProgressAction.Caption = "Progress";
            this.OutputProductionProgressAction.ConfirmationMessage = null;
            this.OutputProductionProgressAction.Id = "OutputProductionProgressActionId";
            this.OutputProductionProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.OutputProduction);
            this.OutputProductionProgressAction.ToolTip = null;
            this.OutputProductionProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.OutputProductionProgressAction_Execute);
            // 
            // OutputProductionPostingAction
            // 
            this.OutputProductionPostingAction.Caption = "Posting";
            this.OutputProductionPostingAction.ConfirmationMessage = null;
            this.OutputProductionPostingAction.Id = "OutputProductionPostingActionId";
            this.OutputProductionPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.OutputProduction);
            this.OutputProductionPostingAction.ToolTip = null;
            this.OutputProductionPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.OutputProductionPostingAction_Execute);
            // 
            // OutputProductionFilterAction
            // 
            this.OutputProductionFilterAction.Caption = "Filter";
            this.OutputProductionFilterAction.ConfirmationMessage = null;
            this.OutputProductionFilterAction.Id = "OutputProductionFilterActionId";
            this.OutputProductionFilterAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.OutputProductionFilterAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.OutputProduction);
            this.OutputProductionFilterAction.ToolTip = null;
            this.OutputProductionFilterAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.OutputProductionFilterAction_Execute);
            // 
            // OutputProductionActionController
            // 
            this.Actions.Add(this.OutputProductionProgressAction);
            this.Actions.Add(this.OutputProductionPostingAction);
            this.Actions.Add(this.OutputProductionFilterAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction OutputProductionProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction OutputProductionPostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction OutputProductionFilterAction;
    }
}
