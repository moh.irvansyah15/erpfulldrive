﻿namespace FullDrive.Module.Controllers
{
    partial class PurchaseRequisitionApprovalActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PurchaseRequisitionApprovalAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // PurchaseRequisitionApprovalAction
            // 
            this.PurchaseRequisitionApprovalAction.Caption = "Approval";
            this.PurchaseRequisitionApprovalAction.ConfirmationMessage = null;
            this.PurchaseRequisitionApprovalAction.Id = "PurchaseRequisitionApprovalActionId";
            this.PurchaseRequisitionApprovalAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.PurchaseRequisitionApprovalAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseRequisition);
            this.PurchaseRequisitionApprovalAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.PurchaseRequisitionApprovalAction.ToolTip = null;
            this.PurchaseRequisitionApprovalAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.PurchaseRequisitionApprovalAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.PurchaseRequisitionApprovalAction_Execute);
            // 
            // PurchaseRequisitionApprovalActionController
            // 
            this.Actions.Add(this.PurchaseRequisitionApprovalAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction PurchaseRequisitionApprovalAction;
    }
}
