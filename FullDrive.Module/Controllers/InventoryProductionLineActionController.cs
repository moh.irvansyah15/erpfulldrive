﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class InventoryProductionLineActionController : ViewController
    {
        #region Default

        public InventoryProductionLineActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        private void InventoryProductionLineSelectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        InventoryProductionLine _locInventoryProductionLineOS = (InventoryProductionLine)_objectSpace.GetObject(obj);

                        if (_locInventoryProductionLineOS != null)
                        {
                            if (_locInventoryProductionLineOS.Code != null)
                            {
                                _currObjectId = _locInventoryProductionLineOS.Code;

                                XPCollection<InventoryProductionLine> _locInventoryProductionLines = new XPCollection<InventoryProductionLine>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new BinaryOperator("Status", Status.Progress)));

                                if (_locInventoryProductionLines != null && _locInventoryProductionLines.Count > 0)
                                {
                                    foreach (InventoryProductionLine _locInventoryProductionLine in _locInventoryProductionLines)
                                    {
                                        _locInventoryProductionLine.Select = true;
                                        _locInventoryProductionLine.ActiveApproved1 = true;
                                        _locInventoryProductionLine.StatusDate = now;
                                        _locInventoryProductionLine.Save();
                                        _locInventoryProductionLine.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("InventoryProductionLine has been Select");
                                }
                                else
                                {
                                    ErrorMessageShow("Data InventoryProductionLine Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = InventoryProduction" + ex.ToString());
            }
        }

        private void InventoryProductionLineUnselectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        InventoryProductionLine _locInventoryProductionLineOS = (InventoryProductionLine)_objectSpace.GetObject(obj);

                        if (_locInventoryProductionLineOS != null)
                        {
                            if (_locInventoryProductionLineOS.Code != null)
                            {
                                _currObjectId = _locInventoryProductionLineOS.Code;

                                XPCollection<InventoryProductionLine> _locInventoryProductionLines = new XPCollection<InventoryProductionLine>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new BinaryOperator("Status", Status.Progress)));

                                if (_locInventoryProductionLines != null && _locInventoryProductionLines.Count > 0)
                                {
                                    foreach (InventoryProductionLine _locInventoryProductionLine in _locInventoryProductionLines)
                                    {
                                        _locInventoryProductionLine.Select = false;
                                        _locInventoryProductionLine.ActiveApproved1 = false;
                                        _locInventoryProductionLine.StatusDate = now;
                                        _locInventoryProductionLine.Save();
                                        _locInventoryProductionLine.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("InventoryProductionLine has been Unselect");
                                }
                                else
                                {
                                    ErrorMessageShow("Data InventoryProductionLine Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = InventoryProduction" + ex.ToString());
            }
        }

        //==== Code Only ====

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0}", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

    }
}
