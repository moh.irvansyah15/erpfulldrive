﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class SalesQuotationMonitoringActionController : ViewController
    {
        public SalesQuotationMonitoringActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void SalesQuotationMonitoringSelectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        SalesQuotationMonitoring _locSalesQuotationMonitoringOS = (SalesQuotationMonitoring)_objectSpace.GetObject(obj);

                        if (_locSalesQuotationMonitoringOS != null)
                        {
                            if (_locSalesQuotationMonitoringOS.Code != null)
                            {
                                _currObjectId = _locSalesQuotationMonitoringOS.Code;

                                XPCollection<SalesQuotationMonitoring> _locSalesQuotationMonitorings = new XPCollection<SalesQuotationMonitoring>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locSalesQuotationMonitorings != null && _locSalesQuotationMonitorings.Count > 0)
                                {
                                    foreach (SalesQuotationMonitoring _locSalesQuotationMonitoring in _locSalesQuotationMonitorings)
                                    {
                                        _locSalesQuotationMonitoring.Select = true;
                                        _locSalesQuotationMonitoring.Save();
                                        _locSalesQuotationMonitoring.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("SalesQuotationMonitoring has been Select");
                                }
                                else
                                {
                                    ErrorMessageShow("Data SalesQuotationMonitoring Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesQuotationMonitoring" + ex.ToString());
            }
        }

        private void SalesQuotationMonitoringUnselectAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        SalesQuotationMonitoring _locSalesQuotationMonitoringOS = (SalesQuotationMonitoring)_objectSpace.GetObject(obj);

                        if (_locSalesQuotationMonitoringOS != null)
                        {
                            if (_locSalesQuotationMonitoringOS.Code != null)
                            {
                                _currObjectId = _locSalesQuotationMonitoringOS.Code;

                                XPCollection<SalesQuotationMonitoring> _locSalesQuotationMonitorings = new XPCollection<SalesQuotationMonitoring>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _currObjectId),
                                                                                                      new GroupOperator(GroupOperatorType.Or,
                                                                                                      new BinaryOperator("Status", Status.Progress),
                                                                                                      new BinaryOperator("Status", Status.Open)
                                                                                                      )));

                                if (_locSalesQuotationMonitorings != null && _locSalesQuotationMonitorings.Count > 0)
                                {
                                    foreach (SalesQuotationMonitoring _locSalesQuotationMonitoring in _locSalesQuotationMonitorings)
                                    {
                                        _locSalesQuotationMonitoring.Select = false;
                                        _locSalesQuotationMonitoring.Save();
                                        _locSalesQuotationMonitoring.Session.CommitTransaction();
                                    }

                                    SuccessMessageShow("SalesQuotationMonitoring has been Unselect");
                                }
                                else
                                {
                                    ErrorMessageShow("Data SalesQuotationMonitoring Not Available");
                                }
                            }
                        }
                    }
                    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                    {
                        _objectSpace.CommitChanges();
                        _objectSpace.Refresh();
                    }
                    if (View is ListView)
                    {
                        _objectSpace.CommitChanges();
                        View.ObjectSpace.Refresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = SalesQuotationMonitoring" + ex.ToString());
            }
        }

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0}", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method
    }
}
