﻿namespace FullDrive.Module.Controllers
{
    partial class PreSalesOrder2ActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PreSalesOrder2ProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PreSalesOrder2PostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PreSalesOrder2ListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // PreSalesOrder2ProgressAction
            // 
            this.PreSalesOrder2ProgressAction.Caption = "Progress";
            this.PreSalesOrder2ProgressAction.ConfirmationMessage = null;
            this.PreSalesOrder2ProgressAction.Id = "PreSalesOrder2ProgressActionId";
            this.PreSalesOrder2ProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PreSalesOrder2);
            this.PreSalesOrder2ProgressAction.ToolTip = null;
            this.PreSalesOrder2ProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PreSalesOrder2ProgressAction_Execute);
            // 
            // PreSalesOrder2PostingAction
            // 
            this.PreSalesOrder2PostingAction.Caption = "Posting";
            this.PreSalesOrder2PostingAction.ConfirmationMessage = null;
            this.PreSalesOrder2PostingAction.Id = "PreSalesOrder2PostingActionId";
            this.PreSalesOrder2PostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PreSalesOrder2);
            this.PreSalesOrder2PostingAction.ToolTip = null;
            this.PreSalesOrder2PostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PreSalesOrder2PostingAction_Execute);
            // 
            // PreSalesOrder2ListviewFilterSelectionAction
            // 
            this.PreSalesOrder2ListviewFilterSelectionAction.Caption = "Filter";
            this.PreSalesOrder2ListviewFilterSelectionAction.ConfirmationMessage = null;
            this.PreSalesOrder2ListviewFilterSelectionAction.Id = "PreSalesOrder2ListviewFilterSelectionActionId";
            this.PreSalesOrder2ListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.PreSalesOrder2ListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PreSalesOrder2);
            this.PreSalesOrder2ListviewFilterSelectionAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.PreSalesOrder2ListviewFilterSelectionAction.ToolTip = null;
            this.PreSalesOrder2ListviewFilterSelectionAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.PreSalesOrder2ListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.PreSalesOrder2ListviewFilterSelectionAction_Execute);
            // 
            // PreSalesOrder2ActionController
            // 
            this.Actions.Add(this.PreSalesOrder2ProgressAction);
            this.Actions.Add(this.PreSalesOrder2PostingAction);
            this.Actions.Add(this.PreSalesOrder2ListviewFilterSelectionAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction PreSalesOrder2ProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PreSalesOrder2PostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction PreSalesOrder2ListviewFilterSelectionAction;
    }
}
