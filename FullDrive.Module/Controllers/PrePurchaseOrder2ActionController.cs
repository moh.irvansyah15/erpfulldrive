﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PrePurchaseOrder2ActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;

        public PrePurchaseOrder2ActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            PrePurchaseOrder2ListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {

                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                PrePurchaseOrder2ListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void PrePurchaseOrder2ProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PrePurchaseOrder2 _locPrePurchaseOrder2OS = (PrePurchaseOrder2)_objectSpace.GetObject(obj);

                        if (_locPrePurchaseOrder2OS != null)
                        {
                            if (_locPrePurchaseOrder2OS.Code != null)
                            {
                                _currObjectId = _locPrePurchaseOrder2OS.Code;

                                PrePurchaseOrder2 _locPrePurchaseOrder2XPO = _currSession.FindObject<PrePurchaseOrder2>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locPrePurchaseOrder2XPO != null)
                                {
                                    if (_locPrePurchaseOrder2XPO.Status == Status.Open)
                                    {
                                        if (_locPrePurchaseOrder2XPO.ProjectHeader != null)
                                        {
                                            ProjectHeader _locProjectHeaderXPO = _currSession.FindObject<ProjectHeader>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _locPrePurchaseOrder2XPO.ProjectHeader.Code)));

                                            if (_locProjectHeaderXPO != null)
                                            {
                                                XPCollection<PrePurchaseOrder2> _locPrePurchaseOrder2s = new XPCollection<PrePurchaseOrder2>(_currSession,
                                                                                                    new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("ProjectHeader", _locProjectHeaderXPO),
                                                                                                    new BinaryOperator("Status", Status.Open)));

                                                if (_locPrePurchaseOrder2s != null && _locPrePurchaseOrder2s.Count() > 0)
                                                {
                                                    foreach (PrePurchaseOrder2 _locPrePurchaseOrder2 in _locPrePurchaseOrder2s)
                                                    {
                                                        _locPrePurchaseOrder2.Status = Status.Progress;
                                                        _locPrePurchaseOrder2.StatusDate = now;
                                                        _locPrePurchaseOrder2.Save();
                                                        _locPrePurchaseOrder2.Session.CommitTransaction();
                                                    }
                                                }
                                                SuccessMessageShow("Pre Purchase Order 2 has been successfully updated to progress");
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Pre Pruchase Order 2 Data Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Pre Pruchase Order 2 Data Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PrePurchaseOrder2 " + ex.ToString());
            }
        }

        private void PrePurchaseOrder2PostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                string _currSignCode = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PrePurchaseOrder2 _locPrePurchaseOrder2OS = (PrePurchaseOrder2)_objectSpace.GetObject(obj);

                        if (_locPrePurchaseOrder2OS != null)
                        {
                            if (_locPrePurchaseOrder2OS.Code != null)
                            {
                                _currObjectId = _locPrePurchaseOrder2OS.Code;

                                PrePurchaseOrder2 _locPrePurchaseOrder2XPO = _currSession.FindObject<PrePurchaseOrder2>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                #region PrePurchaseOrderXPO
                                if (_locPrePurchaseOrder2XPO != null)
                                {

                                    XPQuery<PrePurchaseOrder2> _prePurchaseOrder2sQuery = new XPQuery<PrePurchaseOrder2>(_currSession);

                                    var _prePurchaseOrder2s = from ppo in _prePurchaseOrder2sQuery
                                                              where (ppo.Select == true && (ppo.Status == Status.Progress || ppo.Status == Status.Posted))
                                                              group ppo by ppo.ProjectHeader into g
                                                              select new { ProjectHeader = g.Key };

                                    if (_prePurchaseOrder2s != null && _prePurchaseOrder2s.Count() > 0)
                                    {
                                        foreach (var _prePurchaseOrder2 in _prePurchaseOrder2s)
                                        {
                                            _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.PurchaseOrder);
                                            if (_currSignCode != null)
                                            {
                                                PurchaseOrder _saveDataPo = new PurchaseOrder(_currSession)
                                                {
                                                    TransferType = DirectionType.External,
                                                    SignCode = _currSignCode,
                                                    Company = _locPrePurchaseOrder2XPO.Company,
                                                    ProjectHeader = _prePurchaseOrder2.ProjectHeader,
                                                };
                                                _saveDataPo.Save();
                                                _saveDataPo.Session.CommitTransaction();

                                                PurchaseOrder _locPurchaseOrder = _currSession.FindObject<PurchaseOrder>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("SignCode", _currSignCode)));

                                                XPCollection<PrePurchaseOrder2> _locPrePurchaseOrder2s = new XPCollection<PrePurchaseOrder2>(_currSession,
                                                                                                        new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("ProjectHeader", _prePurchaseOrder2.ProjectHeader),
                                                                                                        new BinaryOperator("Select", true),
                                                                                                        new BinaryOperator("Status", Status.Progress)));

                                                if (_locPurchaseOrder != null)
                                                {
                                                    if (_locPrePurchaseOrder2s != null && _locPrePurchaseOrder2s.Count > 0)
                                                    {

                                                        foreach (PrePurchaseOrder2 _locPrePurchaseOrder2 in _locPrePurchaseOrder2s)
                                                        {
                                                            if (_locPrePurchaseOrder2.Status == Status.Progress)
                                                            {

                                                                PurchaseOrderLine _saveDataPol = new PurchaseOrderLine(_currSession)
                                                                {

                                                                    PurchaseType = OrderType.Service,
                                                                    Name = _locPrePurchaseOrder2.Name,
                                                                    Item = _locPrePurchaseOrder2.Item,
                                                                    MxDQty = _locPrePurchaseOrder2.Qty,
                                                                    MxDUOM = _locPrePurchaseOrder2.UOM,
                                                                    MxUAmount = _locPrePurchaseOrder2.UnitAmount,
                                                                    MxTUAmount = _locPrePurchaseOrder2.TotalUnitAmount,
                                                                    DQty = _locPrePurchaseOrder2.Qty,
                                                                    DUOM = _locPrePurchaseOrder2.UOM,
                                                                    TQty = _locPrePurchaseOrder2.Qty,
                                                                    Company = _locPurchaseOrder.Company,
                                                                    PurchaseOrder = _locPurchaseOrder,
                                                                };
                                                                _saveDataPol.Save();
                                                                _saveDataPol.Session.CommitTransaction();
                                                            }

                                                            _locPrePurchaseOrder2.Status = Status.Lock;
                                                            _locPrePurchaseOrder2.StatusDate = now;
                                                            _locPrePurchaseOrder2.ActivationPosting = true;
                                                            _locPrePurchaseOrder2.PostedCount = _locPrePurchaseOrder2.PostedCount + 1;
                                                            _locPrePurchaseOrder2.Select = false;
                                                            _locPrePurchaseOrder2.Qty = 0;
                                                            _locPrePurchaseOrder2.UnitAmount = 0;
                                                            _locPrePurchaseOrder2.TotalUnitAmount = 0;
                                                            _locPrePurchaseOrder2.Save();
                                                            _locPrePurchaseOrder2.Session.CommitTransaction();
                                                        }
                                                    }
                                                }
                                            }


                                        }
                                        SuccessMessageShow("PrePurchase Order Successfully Posted");
                                    }

                                }
                                #endregion PrePurchaseOrderXPO

                            }
                            ErrorMessageShow("Pre Purchase Order 2 Data Not Available");
                        }
                        ErrorMessageShow("Pre Purchase Order 2 Data Not Available");
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Controller = ProjectHeaderAction" + ex.ToString());
            }
        }

        private void PrePurchaseOrder2ListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(PrePurchaseOrder2)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PrePurchaseOrder2 " + ex.ToString());
            }
        }

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

    }
}
