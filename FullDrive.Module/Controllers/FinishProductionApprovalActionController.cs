﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using DevExpress.ExpressApp.ConditionalAppearance;
using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Model;
using System.Web.UI.WebControls;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

#region Email

using System.Net;
using System.Web;
using System.IO;
using System.Data;
using System.Web.UI;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.Configuration;

#endregion Email

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class FinishProductionApprovalActionController : ViewController
    {
        private ChoiceActionItem _setApprovalLevel;
        public FinishProductionApprovalActionController()
        {
            InitializeComponent();
            TargetViewId = "FinishProduction_ListView";
            RegisterActions(components);
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }

        protected override void OnActivated()
        {
            TypeOfView = typeof(ObjectView);
            base.OnActivated();
            // Perform various tasks depending on the target View.
            var _locUser = SecuritySystem.CurrentUserName;
            GlobalFunction _globalFunction = new GlobalFunction();
            Session _currentSession = null;
            if (this.ObjectSpace != null)
            {
                _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
            }

            if (_currentSession != null)
            {
                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", _locUser));

                if (_locUserAccess != null)
                {
                    FinishProductionApprovalAction.Items.Clear();

                    XPCollection<ApplicationSetupDetail> _locAppSetupDetails = new XPCollection<ApplicationSetupDetail>
                                                                                  (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("UserAccess", _locUserAccess),
                                                                                   new BinaryOperator("ObjectList", CustomProcess.ObjectList.FinishProduction),
                                                                                   new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                   new BinaryOperator("Active", true)));

                    if (_locAppSetupDetails != null && _locAppSetupDetails.Count > 0)
                    {

                        foreach (ApplicationSetupDetail _locAppSetupDetail in _locAppSetupDetails)
                        {
                            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.ApprovalLevel)))
                            {
                                if (_locAppSetupDetail.ApprovalLevel != CustomProcess.ApprovalLevel.None)
                                {
                                    EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalLevel));
                                    _setApprovalLevel = new ChoiceActionItem(_ed.GetCaption(_locAppSetupDetail.ApprovalLevel), _locAppSetupDetail.ApprovalLevel);
                                }
                            }
                            FinishProductionApprovalAction.Items.Add(_setApprovalLevel);
                        }
                    }
                }
            }
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void FinishProductionApprovalAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            GlobalFunction _globFunc = new GlobalFunction();
            ApplicationSetupDetail _locAppSetDetail = null;
            IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
            ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
            DateTime now = DateTime.Now;
            var User = SecuritySystem.CurrentUserName;
            Session _currentSession = null;
            string _currObjectId = null;

            if (this.ObjectSpace != null)
            {
                _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
            }

            UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));

            foreach (Object obj in _objectsToProcess)
            {
                FinishProduction _objInNewObjectSpace = (FinishProduction)_objectSpace.GetObject(obj);

                if (_objInNewObjectSpace != null)
                {
                    if (_objInNewObjectSpace.Code != null)
                    {
                        _currObjectId = _objInNewObjectSpace.Code;
                    }
                }

                if (_currObjectId != null)
                {
                    FinishProduction _locFinishProductionXPO = _currentSession.FindObject<FinishProduction>
                                                             (new GroupOperator(GroupOperatorType.And,
                                                              new BinaryOperator("Code", _currObjectId)));

                    if (_locFinishProductionXPO != null)
                    {
                        if (_locFinishProductionXPO.Status == Status.Progress)
                        {
                            #region Approval Level 1
                            if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level1)
                            {
                                _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level1, ObjectList.FinishProduction);
                                if (_locAppSetDetail != null)
                                {
                                    //Buat bs input langsung ke approvalline
                                    ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("FinishProduction", _locFinishProductionXPO),
                                                                         new BinaryOperator("ApprovalLevel", ApprovalLevel.Level1)));
                                    if (_locApprovalLineXPO == null)
                                    {

                                        if (_locAppSetDetail.EndApproval == true)
                                        {
                                            ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                            {
                                                ApprovalDate = now,
                                                ApprovalStatus = Status.Approved,
                                                ApprovalLevel = ApprovalLevel.Level1,
                                                EndApproval = true,
                                                FinishProduction = _locFinishProductionXPO,
                                            };
                                            _saveDataAL.Save();
                                            _saveDataAL.Session.CommitTransaction();
                                        }
                                        else
                                        {
                                            ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                            {
                                                ApprovalDate = now,
                                                ApprovalStatus = Status.Approved,
                                                ApprovalLevel = ApprovalLevel.Level1,
                                                FinishProduction = _locFinishProductionXPO,
                                            };
                                            _saveDataAL.Save();
                                            _saveDataAL.Session.CommitTransaction();
                                        }

                                        //Send Email
                                        ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("FinishProduction", _locFinishProductionXPO)));

                                        SetApprovalFinishProduction(_currentSession, _locFinishProductionXPO, Status.Progress);
                                        SendEmail(_currentSession, _locFinishProductionXPO, Status.Progress, _locApprovalLineXPO2);
                                        SuccessMessageShow("FinishProduction has successfully Approve");
                                    }
                                }
                            }
                            #endregion Approval Level 1

                            #region Approval Level 2
                            if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level2)
                            {
                                _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level2, ObjectList.FinishProduction);

                                if (_locAppSetDetail != null)
                                {
                                    ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("FinishProduction", _locFinishProductionXPO),
                                                                         new BinaryOperator("ApprovalLevel", ApprovalLevel.Level2)));

                                    if (_locApprovalLineXPO == null)
                                    {
                                        if (_locAppSetDetail.EndApproval == true)
                                        {
                                            ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                            {
                                                ApprovalDate = now,
                                                ApprovalStatus = Status.Approved,
                                                ApprovalLevel = ApprovalLevel.Level2,
                                                EndApproval = true,
                                                FinishProduction = _locFinishProductionXPO,
                                            };
                                            _saveDataAL.Save();
                                            _saveDataAL.Session.CommitTransaction();
                                        }
                                        else
                                        {
                                            ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                            {
                                                ApprovalDate = now,
                                                ApprovalStatus = Status.Approved,
                                                ApprovalLevel = ApprovalLevel.Level2,
                                                FinishProduction = _locFinishProductionXPO,
                                            };
                                            _saveDataAL.Save();
                                            _saveDataAL.Session.CommitTransaction();

                                            SetApprovalLine(_currentSession, _locFinishProductionXPO, ApprovalLevel.Level1);
                                        }

                                        //Send Email
                                        ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("FinishProduction", _locFinishProductionXPO)));

                                        SetApprovalFinishProduction(_currentSession, _locFinishProductionXPO, Status.Progress);
                                        SendEmail(_currentSession, _locFinishProductionXPO, Status.Progress, _locApprovalLineXPO2);
                                        SuccessMessageShow("FinishProduction has successfully Approve");
                                    }
                                }
                            }
                            #endregion Approval Level 2

                            #region Approval Level 3
                            if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level3)
                            {
                                _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level3, ObjectList.FinishProduction);

                                if (_locAppSetDetail != null)
                                {
                                    ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("FinishProduction", _locFinishProductionXPO),
                                                                         new BinaryOperator("ApprovalLevel", ApprovalLevel.Level3)));

                                    if (_locApprovalLineXPO == null)
                                    {
                                        if (_locAppSetDetail.EndApproval == true)
                                        {
                                            ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                            {
                                                ApprovalDate = now,
                                                ApprovalStatus = Status.Approved,
                                                ApprovalLevel = ApprovalLevel.Level3,
                                                EndApproval = true,
                                                FinishProduction = _locFinishProductionXPO,
                                            };
                                            _saveDataAL.Save();
                                            _saveDataAL.Session.CommitTransaction();
                                        }
                                        else
                                        {
                                            ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                            {
                                                ApprovalDate = now,
                                                ApprovalStatus = Status.Approved,
                                                ApprovalLevel = ApprovalLevel.Level3,
                                                FinishProduction = _locFinishProductionXPO,
                                            };
                                            _saveDataAL.Save();
                                            _saveDataAL.Session.CommitTransaction();

                                            SetApprovalLine(_currentSession, _locFinishProductionXPO, ApprovalLevel.Level2);
                                            SetApprovalLine(_currentSession, _locFinishProductionXPO, ApprovalLevel.Level1);
                                        }

                                        //Send Email
                                        ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("FinishProduction", _locFinishProductionXPO)));

                                        SetApprovalFinishProduction(_currentSession, _locFinishProductionXPO, Status.Progress);
                                        SendEmail(_currentSession, _locFinishProductionXPO, Status.Progress, _locApprovalLineXPO2);
                                        SuccessMessageShow("FinishProduction has successfully Approve");
                                    }
                                }
                            }
                            #endregion Approval Level 3
                        }
                        else
                        {
                            ErrorMessageShow("FinishProduction Status Not Available");
                        }
                    }
                    else
                    {
                        ErrorMessageShow("FinishProduction Not Available");
                    }
                }
            }

            if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
            {
                _objectSpace.CommitChanges();
            }
            if (View is ListView)
            {
                _objectSpace.CommitChanges();
                View.ObjectSpace.Refresh();
            }
        }

        #region ApprovalProcess

        private void SetApprovalLine(Session _currentSession, FinishProduction _locItemConsumptionXPO, ApprovalLevel _locApprovalLevel)
        {
            try
            {
                DateTime now = DateTime.Now;

                ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("FinishProduction", _locItemConsumptionXPO),
                                                                         new BinaryOperator("ApprovalLevel", _locApprovalLevel)));

                if (_locApprovalLineXPO == null)
                {
                    ApprovalLine _saveDataAL2 = new ApprovalLine(_currentSession)
                    {
                        ApprovalDate = now,
                        ApprovalStatus = Status.Approved,
                        ApprovalLevel = _locApprovalLevel,
                        FinishProduction = _locItemConsumptionXPO,
                    };
                    _saveDataAL2.Save();
                    _saveDataAL2.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FinishProduction " + ex.ToString());
            }
        }

        private void SetApprovalFinishProduction(Session _currentSession, FinishProduction _locItemConsumptionXPO, Status _locStatus)
        {
            try
            {
                DateTime now = DateTime.Now;

                FinishProduction _locItemConsumptions = _currentSession.FindObject<FinishProduction>
                                                       (new GroupOperator(GroupOperatorType.And,
                                                        new BinaryOperator("Code", _locItemConsumptionXPO.Code),
                                                        new BinaryOperator("Status", _locStatus)));

                if (_locItemConsumptions != null)
                {
                    ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("EndApproval", true),
                                                         new BinaryOperator("FinishProduction", _locItemConsumptions)));

                    if (_locApprovalLineXPO2 != null)
                    {
                        #region True

                        if (_locItemConsumptions != null)
                        {
                            _locItemConsumptions.ActivationPosting = true;
                            _locItemConsumptions.ActiveApproved1 = false;
                            _locItemConsumptions.ActiveApproved2 = false;
                            _locItemConsumptions.ActiveApproved3 = true;
                            _locItemConsumptions.Save();
                            _locItemConsumptions.Session.CommitTransaction();

                            XPCollection<FinishProductionLine> _locItemConsumptionLines = new XPCollection<FinishProductionLine>
                                                                                                        (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("FinishProduction", _locItemConsumptionXPO),
                                                                                                        new BinaryOperator("Status", Status.Progress)));
                            if (_locItemConsumptionLines != null)
                            {
                                foreach (FinishProductionLine _locItemConsumptionLine in _locItemConsumptionLines)
                                {
                                    _locItemConsumptionLine.ActivationPosting = true;
                                    _locItemConsumptionLine.Save();
                                    _locItemConsumptionLine.Session.CommitTransaction();
                                }
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Data ItemConsumption Not Available");
                        }

                        #endregion True
                    }
                    else if (_locApprovalLineXPO2 == null)
                    {
                        #region False

                        if (_locItemConsumptions.ActiveApproved1 == false && _locItemConsumptions.ActiveApproved2 == false && _locItemConsumptions.ActiveApproved3 == false)
                        {
                            #region Lv1
                            if (_locItemConsumptions != null)
                            {
                                _locItemConsumptions.ActivationPosting = true;
                                _locItemConsumptions.ActiveApproved1 = true;
                                _locItemConsumptions.ActiveApproved2 = false;
                                _locItemConsumptions.ActiveApproved3 = false;
                                _locItemConsumptions.Save();
                                _locItemConsumptions.Session.CommitTransaction();

                                XPCollection<FinishProductionLine> _locItemConsumptionLines = new XPCollection<FinishProductionLine>
                                                                                             (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                              new BinaryOperator("FinishProduction", _locItemConsumptionXPO),
                                                                                              new BinaryOperator("Status", Status.Progress)));
                                if (_locItemConsumptionLines != null)
                                {
                                    foreach (FinishProductionLine _locItemConsumptionLine in _locItemConsumptionLines)
                                    {
                                        _locItemConsumptionLine.ActivationPosting = true;
                                        _locItemConsumptionLine.Save();
                                        _locItemConsumptionLine.Session.CommitTransaction();
                                    }
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data FinishProduction Not Available");
                            }
                            #endregion Lv1
                        }
                        else if (_locItemConsumptions.ActiveApproved1 == true && _locItemConsumptions.ActiveApproved2 == false && _locItemConsumptions.ActiveApproved3 == false)
                        {
                            #region Lv2
                            if (_locItemConsumptions != null)
                            {
                                _locItemConsumptions.ActivationPosting = true;
                                _locItemConsumptions.ActiveApproved2 = true;
                                _locItemConsumptions.ActiveApproved1 = false;
                                _locItemConsumptions.ActiveApproved3 = false;
                                _locItemConsumptions.Save();
                                _locItemConsumptions.Session.CommitTransaction();

                                XPCollection<FinishProductionLine> _locItemConsumptionLines = new XPCollection<FinishProductionLine>
                                                                                             (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                              new BinaryOperator("FinishProduction", _locItemConsumptionXPO),
                                                                                              new BinaryOperator("Status", Status.Progress)));
                                if (_locItemConsumptionLines != null)
                                {
                                    foreach (FinishProductionLine _locItemConsumptionLine in _locItemConsumptionLines)
                                    {
                                        _locItemConsumptionLine.ActivationPosting = true;
                                        _locItemConsumptionLine.Save();
                                        _locItemConsumptionLine.Session.CommitTransaction();
                                    }
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data FinishProduction Not Available");
                            }
                            #endregion Lv2
                        }
                        else if (_locItemConsumptions.ActiveApproved1 == false && _locItemConsumptions.ActiveApproved2 == true && _locItemConsumptions.ActiveApproved3 == false)
                        {
                            #region Lv3
                            if (_locItemConsumptions != null)
                            {
                                _locItemConsumptions.ActivationPosting = true;
                                _locItemConsumptions.ActiveApproved1 = false;
                                _locItemConsumptions.ActiveApproved2 = false;
                                _locItemConsumptions.ActiveApproved3 = true;
                                _locItemConsumptions.Save();
                                _locItemConsumptions.Session.CommitTransaction();

                                XPCollection<FinishProductionLine> _locItemConsumptionLines = new XPCollection<FinishProductionLine>
                                                                                             (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                              new BinaryOperator("FinishProduction", _locItemConsumptionXPO),
                                                                                              new BinaryOperator("Status", Status.Progress)));
                                if (_locItemConsumptionLines != null)
                                {
                                    foreach (FinishProductionLine _locItemConsumptionLine in _locItemConsumptionLines)
                                    {
                                        _locItemConsumptionLine.ActivationPosting = true;
                                        _locItemConsumptionLine.Save();
                                        _locItemConsumptionLine.Session.CommitTransaction();
                                    }
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data FinishProduction Not Available");
                            }
                            #endregion Lv3
                        }
                        else
                        {
                            ErrorMessageShow("Data Approval For FinishProduction Not Available");
                        }

                        #endregion False
                    }
                    else
                    {
                        ErrorMessageShow("Data Approval For FinishProduction Not Available");
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FinishProduction " + ex.ToString());
            }
        }

        #endregion ApprovalProcess

        #region Email

        private string BackgroundBody(Session _currentSession, FinishProduction _locItemConsumptionXPO, MailSetupDetail _locMailSetDetail)
        {
            string body = string.Empty;

            FinishProduction _locItemConsumptions = _currentSession.FindObject<FinishProduction>
                                           (new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Code", _locItemConsumptionXPO.Code)));

            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                               (new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("FinishProduction", _locItemConsumptions)));

            string Status = _locApprovalLineXPO.ApprovalStatus.ToString();

            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Email/EmailTemplate.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{Code}", _locItemConsumptionXPO.Code);

            #region Level
            if (_locItemConsumptions.ActiveApproved1 == true)
            {
                string Active = "Level 1";
                body = body.Replace("{Level}", Active);
            }
            else if (_locItemConsumptions.ActiveApproved2 == true)
            {
                string Active = "Level 2";
                body = body.Replace("{Level}", Active);
            }
            else if (_locItemConsumptions.ActiveApproved3 == true)
            {
                string Active = "Level 3";
                body = body.Replace("{Level}", Active);
            }
            else
            {
                string Active = "Not Available";
                body = body.Replace("{Level}", Active);
            }
            #endregion Level

            body = body.Replace("{Status}", Status);
            body = body.Replace("{Message}", _locMailSetDetail.MessageBody);
            return body;
        }

        protected void SendEmail(Session _currentSession, FinishProduction _locItemConsumptionXPO, Status _locStatus, ApprovalLine _locApprovalLineXPO2)
        {
            DateTime now = DateTime.Now;
            var User = SecuritySystem.CurrentUserName;
            GlobalFunction _globFunc = new GlobalFunction();
            UserAccess _numLineUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));
            string _locMessageBody = null;

            try
            {
                if (_locItemConsumptionXPO != null)
                {
                    XPCollection<MailSetupDetail> _locMailSetDetails = new XPCollection<MailSetupDetail>
                                                                       (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ObjectList", ObjectList.ItemConsumption),
                                                                        new BinaryOperator("UserAccess", _numLineUserAccess),
                                                                        new BinaryOperator("Active", true)));

                    if (_locMailSetDetails != null && _locMailSetDetails.Count > 0)
                    {
                        foreach (MailSetupDetail _locMailSetDetail in _locMailSetDetails)
                        {
                            if (_locMailSetDetail.MessageBody != null)
                            {
                                if (_locApprovalLineXPO2.EndApproval == true)
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locItemConsumptionXPO, _locMailSetDetail);
                                }
                                else
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locItemConsumptionXPO, _locMailSetDetail);
                                }
                            }

                            _globFunc.SetAndSendMail(_locMailSetDetail.SmtpHost, _locMailSetDetail.SmtpPort,
                            _locMailSetDetail.MailFrom, _locMailSetDetail.MailFromPassword, _locMailSetDetail.MailTo, _locMailSetDetail.MailSubject, _locMessageBody);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = FinishProduction " + ex.ToString());
            }
        }

        #endregion Email

        #region GlobalMethod

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion GlobalMethod

    }
}
