﻿namespace FullDrive.Module.Controllers
{
    partial class PurchaseInvoiceActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PurchaseInvoiceProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchaseInvoicePostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchaseInvoiceListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.PurchaseInvoiceGetPayAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchaseInvoiceCMCalculationAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // PurchaseInvoiceProgressAction
            // 
            this.PurchaseInvoiceProgressAction.Caption = "Progress";
            this.PurchaseInvoiceProgressAction.ConfirmationMessage = null;
            this.PurchaseInvoiceProgressAction.Id = "PurchaseInvoiceProgressActionId";
            this.PurchaseInvoiceProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseInvoice);
            this.PurchaseInvoiceProgressAction.ToolTip = null;
            this.PurchaseInvoiceProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseInvoiceProgressAction_Execute);
            // 
            // PurchaseInvoicePostingAction
            // 
            this.PurchaseInvoicePostingAction.Caption = "Posting";
            this.PurchaseInvoicePostingAction.ConfirmationMessage = null;
            this.PurchaseInvoicePostingAction.Id = "PurchaseInvoicePostingActionId";
            this.PurchaseInvoicePostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseInvoice);
            this.PurchaseInvoicePostingAction.ToolTip = null;
            this.PurchaseInvoicePostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseInvoicePostingAction_Execute);
            // 
            // PurchaseInvoiceListviewFilterSelectionAction
            // 
            this.PurchaseInvoiceListviewFilterSelectionAction.Caption = "Filter";
            this.PurchaseInvoiceListviewFilterSelectionAction.ConfirmationMessage = null;
            this.PurchaseInvoiceListviewFilterSelectionAction.Id = "PurchaseInvoiceListviewFilterSelectionActionId";
            this.PurchaseInvoiceListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.PurchaseInvoiceListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseInvoice);
            this.PurchaseInvoiceListviewFilterSelectionAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.PurchaseInvoiceListviewFilterSelectionAction.ToolTip = null;
            this.PurchaseInvoiceListviewFilterSelectionAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.PurchaseInvoiceListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.PurchaseInvoiceListviewFilterSelectionAction_Execute);
            // 
            // PurchaseInvoiceGetPayAction
            // 
            this.PurchaseInvoiceGetPayAction.Caption = "Get Pay";
            this.PurchaseInvoiceGetPayAction.ConfirmationMessage = null;
            this.PurchaseInvoiceGetPayAction.Id = "PurchaseInvoiceGetPayActionId";
            this.PurchaseInvoiceGetPayAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseInvoice);
            this.PurchaseInvoiceGetPayAction.ToolTip = null;
            this.PurchaseInvoiceGetPayAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseInvoiceGetPayAction_Execute);
            // 
            // PurchaseInvoiceCMCalculationAction
            // 
            this.PurchaseInvoiceCMCalculationAction.Caption = "CM Calculation";
            this.PurchaseInvoiceCMCalculationAction.ConfirmationMessage = null;
            this.PurchaseInvoiceCMCalculationAction.Id = "PurchaseInvoiceCMCalculationActionId";
            this.PurchaseInvoiceCMCalculationAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseInvoice);
            this.PurchaseInvoiceCMCalculationAction.ToolTip = null;
            this.PurchaseInvoiceCMCalculationAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseInvoiceCMCalculationAction_Execute);
            // 
            // PurchaseInvoiceActionController
            // 
            this.Actions.Add(this.PurchaseInvoiceProgressAction);
            this.Actions.Add(this.PurchaseInvoicePostingAction);
            this.Actions.Add(this.PurchaseInvoiceListviewFilterSelectionAction);
            this.Actions.Add(this.PurchaseInvoiceGetPayAction);
            this.Actions.Add(this.PurchaseInvoiceCMCalculationAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseInvoiceProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseInvoicePostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction PurchaseInvoiceListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseInvoiceGetPayAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseInvoiceCMCalculationAction;
    }
}
