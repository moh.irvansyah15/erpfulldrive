﻿namespace FullDrive.Module.Controllers
{
    partial class InventoryTransferOrderApprovalDetailController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.InventoryTransferOrderApprovalDetailAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // InventoryTransferOrderApprovalDetailAction
            // 
            this.InventoryTransferOrderApprovalDetailAction.Caption = "Approval";
            this.InventoryTransferOrderApprovalDetailAction.ConfirmationMessage = null;
            this.InventoryTransferOrderApprovalDetailAction.Id = "InventoryTransferOrderApprovalDetailActionId";
            this.InventoryTransferOrderApprovalDetailAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.InventoryTransferOrderApprovalDetailAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryTransferOrder);
            this.InventoryTransferOrderApprovalDetailAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.InventoryTransferOrderApprovalDetailAction.ToolTip = null;
            this.InventoryTransferOrderApprovalDetailAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.InventoryTransferOrderApprovalDetailAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.InventoryTransferOrderApprovalDetailAction_Execute);
            // 
            // InventoryTransferOrderApprovalDetailController
            // 
            this.Actions.Add(this.InventoryTransferOrderApprovalDetailAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction InventoryTransferOrderApprovalDetailAction;
    }
}
