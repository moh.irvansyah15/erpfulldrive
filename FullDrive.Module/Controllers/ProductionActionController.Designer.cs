﻿namespace FullDrive.Module.Controllers
{
    partial class ProductionActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ProductionProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ProductionPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ProductionFilterAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.ProductionListviewFilterApprovalSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // ProductionProgressAction
            // 
            this.ProductionProgressAction.Caption = "Progress";
            this.ProductionProgressAction.ConfirmationMessage = null;
            this.ProductionProgressAction.Id = "ProductionProgressActionId";
            this.ProductionProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.Production);
            this.ProductionProgressAction.ToolTip = null;
            this.ProductionProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ProductionProgressAction_Execute);
            // 
            // ProductionPostingAction
            // 
            this.ProductionPostingAction.Caption = "Posting";
            this.ProductionPostingAction.ConfirmationMessage = null;
            this.ProductionPostingAction.Id = "ProductionPostingActionId";
            this.ProductionPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.Production);
            this.ProductionPostingAction.ToolTip = null;
            this.ProductionPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ProductionPostingAction_Execute);
            // 
            // ProductionFilterAction
            // 
            this.ProductionFilterAction.Caption = "Filter";
            this.ProductionFilterAction.ConfirmationMessage = null;
            this.ProductionFilterAction.Id = "ProductionFilterActionId";
            this.ProductionFilterAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.ProductionFilterAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.Production);
            this.ProductionFilterAction.ToolTip = null;
            this.ProductionFilterAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.ProductionFilterAction_Execute);
            // 
            // ProductionListviewFilterApprovalSelectionAction
            // 
            this.ProductionListviewFilterApprovalSelectionAction.Caption = "Filter Approval";
            this.ProductionListviewFilterApprovalSelectionAction.ConfirmationMessage = null;
            this.ProductionListviewFilterApprovalSelectionAction.Id = "ProductionListviewFilterApprovalSelectionActionId";
            this.ProductionListviewFilterApprovalSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.ProductionListviewFilterApprovalSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.Production);
            this.ProductionListviewFilterApprovalSelectionAction.ToolTip = null;
            this.ProductionListviewFilterApprovalSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.ProductionListviewFilterApprovalSelectionAction_Execute);
            // 
            // ProductionActionController
            // 
            this.Actions.Add(this.ProductionProgressAction);
            this.Actions.Add(this.ProductionPostingAction);
            this.Actions.Add(this.ProductionFilterAction);
            this.Actions.Add(this.ProductionListviewFilterApprovalSelectionAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction ProductionProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction ProductionPostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction ProductionFilterAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction ProductionListviewFilterApprovalSelectionAction;
    }
}
