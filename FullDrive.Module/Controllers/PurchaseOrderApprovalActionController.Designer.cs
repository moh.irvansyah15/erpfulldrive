﻿namespace FullDrive.Module.Controllers
{
    partial class PurchaseOrderApprovalActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PurchaseOrderApprovalAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // PurchaseOrderApprovalAction
            // 
            this.PurchaseOrderApprovalAction.Caption = "Approval";
            this.PurchaseOrderApprovalAction.ConfirmationMessage = null;
            this.PurchaseOrderApprovalAction.Id = "PurchaseOrderApprovalActionId";
            this.PurchaseOrderApprovalAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.PurchaseOrderApprovalAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseOrder);
            this.PurchaseOrderApprovalAction.ToolTip = null;
            this.PurchaseOrderApprovalAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.PurchaseOrderApprovalAction_Execute);
            // 
            // PurchaseOrderApprovalActionController
            // 
            this.Actions.Add(this.PurchaseOrderApprovalAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction PurchaseOrderApprovalAction;
    }
}
