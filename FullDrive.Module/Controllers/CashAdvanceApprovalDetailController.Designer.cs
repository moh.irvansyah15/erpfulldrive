﻿namespace FullDrive.Module.Controllers
{
    partial class CashAdvanceApprovalDetailController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CashAdvanceApprovalDetailAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // CashAdvanceApprovalDetailAction
            // 
            this.CashAdvanceApprovalDetailAction.Caption = "Approval";
            this.CashAdvanceApprovalDetailAction.ConfirmationMessage = null;
            this.CashAdvanceApprovalDetailAction.Id = "CashAdvanceApprovalDetailActionId";
            this.CashAdvanceApprovalDetailAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.CashAdvanceApprovalDetailAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.CashAdvanceApprovalDetailAction.ToolTip = null;
            this.CashAdvanceApprovalDetailAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.CashAdvanceApprovalDetailAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.CashAdvanceApprovalDetailAction_Execute);
            // 
            // CashAdvanceApprovalDetailController
            // 
            this.Actions.Add(this.CashAdvanceApprovalDetailAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction CashAdvanceApprovalDetailAction;
    }
}
