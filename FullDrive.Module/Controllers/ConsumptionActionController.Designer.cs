﻿namespace FullDrive.Module.Controllers
{
    partial class ConsumptionActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ConsumptionProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ConsumptionPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ConsumptionFilterAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // ConsumptionProgressAction
            // 
            this.ConsumptionProgressAction.Caption = "Progress";
            this.ConsumptionProgressAction.ConfirmationMessage = null;
            this.ConsumptionProgressAction.Id = "ConsumptionProgressActionId";
            this.ConsumptionProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.Consumption);
            this.ConsumptionProgressAction.ToolTip = null;
            this.ConsumptionProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ConsumptionProgressAction_Execute);
            // 
            // ConsumptionPostingAction
            // 
            this.ConsumptionPostingAction.Caption = "Posting";
            this.ConsumptionPostingAction.ConfirmationMessage = null;
            this.ConsumptionPostingAction.Id = "ConsumptionPostingActionId";
            this.ConsumptionPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.Consumption);
            this.ConsumptionPostingAction.ToolTip = null;
            this.ConsumptionPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ConsumptionPostingAction_Execute);
            // 
            // ConsumptionFilterAction
            // 
            this.ConsumptionFilterAction.Caption = "Filter";
            this.ConsumptionFilterAction.ConfirmationMessage = null;
            this.ConsumptionFilterAction.Id = "ConsumptionFilterActionId";
            this.ConsumptionFilterAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.ConsumptionFilterAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.Consumption);
            this.ConsumptionFilterAction.ToolTip = null;
            this.ConsumptionFilterAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.ConsumptionFilterAction_Execute);
            // 
            // ConsumptionActionController
            // 
            this.Actions.Add(this.ConsumptionProgressAction);
            this.Actions.Add(this.ConsumptionPostingAction);
            this.Actions.Add(this.ConsumptionFilterAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction ConsumptionProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction ConsumptionPostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction ConsumptionFilterAction;
    }
}
