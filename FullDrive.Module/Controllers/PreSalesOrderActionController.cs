﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using FullDrive.Module.BusinessObjects;
using DevExpress.ExpressApp.Xpo;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PreSalesOrderActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;

        public PreSalesOrderActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            PreSalesOrderListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                PreSalesOrderListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void PreSalesOrderProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PreSalesOrder _locPreSalesOrderOS = (PreSalesOrder)_objectSpace.GetObject(obj);

                        if (_locPreSalesOrderOS != null)
                        {
                            if (_locPreSalesOrderOS.Code != null)
                            {
                                _currObjectId = _locPreSalesOrderOS.Code;

                                PreSalesOrder _locPreSalesOrderXPO = _currSession.FindObject<PreSalesOrder>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locPreSalesOrderXPO != null)
                                {
                                    if (_locPreSalesOrderXPO.Status == Status.Open)
                                    {
                                        if (_locPreSalesOrderXPO.ProjectHeader != null)
                                        {
                                            ProjectHeader _locProjectHeaderXPO = _currSession.FindObject<ProjectHeader>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _locPreSalesOrderXPO.ProjectHeader.Code)));

                                            if (_locProjectHeaderXPO != null)
                                            {
                                                XPCollection<PreSalesOrder> _locPreSalesOrders = new XPCollection<PreSalesOrder>(_currSession,
                                                                                                    new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("ProjectHeader", _locProjectHeaderXPO),
                                                                                                    new BinaryOperator("Status", Status.Open)));

                                                if (_locPreSalesOrders != null && _locPreSalesOrders.Count() > 0)
                                                {
                                                    foreach (PreSalesOrder _locPreSalesOrder in _locPreSalesOrders)
                                                    {
                                                        _locPreSalesOrder.Status = Status.Progress;
                                                        _locPreSalesOrder.StatusDate = now;
                                                        _locPreSalesOrder.Save();
                                                        _locPreSalesOrder.Session.CommitTransaction();
                                                    }
                                                }
                                                SuccessMessageShow("Pre Sales Order has been successfully updated to progress");
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Pre Sales Order Data Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Pre Sales Order Data Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PreSalesOrder " + ex.ToString());
            }
        }

        private void PreSalesOrderPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                string _currSignCode = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PreSalesOrder _locPreSalesOrderOS = (PreSalesOrder)_objectSpace.GetObject(obj);

                        if (_locPreSalesOrderOS != null)
                        {
                            if (_locPreSalesOrderOS.Code != null)
                            {
                                _currObjectId = _locPreSalesOrderOS.Code;

                                PreSalesOrder _locPreSalesOrderXPO = _currSession.FindObject<PreSalesOrder>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                #region PreSalesOrderXPO
                                if (_locPreSalesOrderXPO != null)
                                {
                                    XPQuery<PreSalesOrder> _preSalesOrdersQueryPH = new XPQuery<PreSalesOrder>(_currSession);
                                    XPQuery<PreSalesOrder> _preSalesOrdersQuery = new XPQuery<PreSalesOrder>(_currSession);

                                    var _preSalesOrderPHs = from ppph in _preSalesOrdersQueryPH
                                                            where (ppph.Select == true && (ppph.Status == Status.Progress || ppph.Status == Status.Posted))
                                                            group ppph by ppph.ProjectHeader into g
                                                            select new { ProjectHeader = g.Key };

                                    if (_preSalesOrderPHs != null && _preSalesOrderPHs.Count() > 0)
                                    {
                                        foreach (var _preSalesOrderPH in _preSalesOrderPHs)
                                        {
                                            var _preSalesOrders = from ppo in _preSalesOrdersQuery
                                                                  where (ppo.ProjectHeader == _preSalesOrderPH.ProjectHeader && ppo.Select == true &&
                                                                        (ppo.Status == Status.Progress || ppo.Status == Status.Posted))
                                                                  group ppo by ppo.TransferType into g
                                                                  select new { TransferType = g.Key };

                                            if (_preSalesOrders != null && _preSalesOrders.Count() > 0)
                                            {
                                                foreach (var _preSalesOrder in _preSalesOrders)
                                                {
                                                    #region TransferTypeExternal
                                                    if (_preSalesOrder.TransferType == DirectionType.External)
                                                    {
                                                        _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.SalesOrder);
                                                        if (_currSignCode != null)
                                                        {
                                                            SalesOrder _saveDataSo = new SalesOrder(_currSession)
                                                            {
                                                                TransferType = DirectionType.External,
                                                                SignCode = _currSignCode,
                                                                Company = _locPreSalesOrderXPO.Company,
                                                                ProjectHeader = _preSalesOrderPH.ProjectHeader,
                                                            };
                                                            _saveDataSo.Save();
                                                            _saveDataSo.Session.CommitTransaction();

                                                            SalesOrder _locSalesOrder = _currSession.FindObject<SalesOrder>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("SignCode", _currSignCode)));

                                                            XPCollection<PreSalesOrder> _locPreSalesOrders = new XPCollection<PreSalesOrder>(_currSession,
                                                                                                                    new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("ProjectHeader", _preSalesOrderPH.ProjectHeader),
                                                                                                                    new BinaryOperator("TransferType", DirectionType.External),
                                                                                                                    new BinaryOperator("Select", true)));
                                                            if (_locSalesOrder != null)
                                                            {
                                                                if (_locPreSalesOrders != null && _locPreSalesOrders.Count > 0)
                                                                {
                                                                    foreach (PreSalesOrder _locPreSalesOrder in _locPreSalesOrders)
                                                                    {
                                                                        if (_locPreSalesOrder.Status == Status.Progress || _locPreSalesOrder.Status == Status.Posted)
                                                                        {
                                                                            SalesOrderLine _saveDataSol = new SalesOrderLine(_currSession)
                                                                            {
                                                                                SalesType = OrderType.Item,
                                                                                Item = _locPreSalesOrder.Item,
                                                                                MxDQty = _locPreSalesOrder.DQty,
                                                                                MxDUOM = _locPreSalesOrder.MxDUOM,
                                                                                MxQty = _locPreSalesOrder.Qty,
                                                                                MxUOM = _locPreSalesOrder.MxUOM,
                                                                                MxTQty = _locPreSalesOrder.TotalQty,
                                                                                DQty = _locPreSalesOrder.DQty,
                                                                                DUOM = _locPreSalesOrder.MxDUOM,
                                                                                Qty = _locPreSalesOrder.Qty,
                                                                                UOM = _locPreSalesOrder.MxUOM,
                                                                                TQty = _locPreSalesOrder.TotalQty,
                                                                                MxUAmount = _locPreSalesOrder.MxUAmount,
                                                                                MxTUAmount = _locPreSalesOrder.TotalQty * _locPreSalesOrder.MxUAmount,
                                                                                UAmount = _locPreSalesOrder.MxUAmount,
                                                                                TUAmount = _locPreSalesOrder.TotalQty * _locPreSalesOrder.MxUAmount,
                                                                                Company = _locSalesOrder.Company,
                                                                                SalesOrder = _locSalesOrder,
                                                                            };
                                                                            _saveDataSol.Save();
                                                                            _saveDataSol.Session.CommitTransaction();

                                                                            SetRemainQuantity(_currSession, _locPreSalesOrder);

                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    #endregion TransferTypeExternal

                                                    #region TransferTypeRuleIndirect
                                                    if (_preSalesOrder.TransferType == DirectionType.Internal)
                                                    {
                                                        _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.SalesQuotation);
                                                        if (_currSignCode != null)
                                                        {
                                                            SalesQuotation _saveDataSq = new SalesQuotation(_currSession)
                                                            {
                                                                TransferType = DirectionType.Internal,
                                                                SignCode = _currSignCode,
                                                                Company = _locPreSalesOrderXPO.Company,
                                                                ProjectHeader = _preSalesOrderPH.ProjectHeader,
                                                            };
                                                            _saveDataSq.Save();
                                                            _saveDataSq.Session.CommitTransaction();

                                                            SalesQuotation _locSalesQuotation = _currSession.FindObject<SalesQuotation>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("SignCode", _currSignCode)));

                                                            XPCollection<PreSalesOrder> _locPreSalesOrders = new XPCollection<PreSalesOrder>(_currSession,
                                                                                                             new GroupOperator(GroupOperatorType.And,
                                                                                                             new BinaryOperator("ProjectHeader", _preSalesOrderPH.ProjectHeader),
                                                                                                             new BinaryOperator("TransferType", DirectionType.Internal),
                                                                                                             new BinaryOperator("Select", true)));
                                                            if (_locSalesQuotation != null)
                                                            {
                                                                if (_locPreSalesOrders != null && _locPreSalesOrders.Count > 0)
                                                                {
                                                                    foreach (PreSalesOrder _locPreSalesOrder in _locPreSalesOrders)
                                                                    {
                                                                        if (_locPreSalesOrder.Status == Status.Progress || _locPreSalesOrder.Status == Status.Posted)
                                                                        {
                                                                            SalesQuotationLine _saveDataSql = new SalesQuotationLine(_currSession)
                                                                            {
                                                                                SalesType = OrderType.Item,
                                                                                Item = _locPreSalesOrder.Item,
                                                                                MxDQty = _locPreSalesOrder.DQty,
                                                                                MxDUOM = _locPreSalesOrder.MxDUOM,
                                                                                MxQty = _locPreSalesOrder.Qty,
                                                                                MxUOM = _locPreSalesOrder.MxUOM,
                                                                                MxTQty = _locPreSalesOrder.TotalQty,
                                                                                MxUAmount = _locPreSalesOrder.MxUAmount,
                                                                                MxTUAmount = _locPreSalesOrder.TotalQty * _locPreSalesOrder.MxUAmount,
                                                                                DQty = _locPreSalesOrder.DQty,
                                                                                DUOM = _locPreSalesOrder.MxDUOM,
                                                                                Qty = _locPreSalesOrder.Qty,
                                                                                UOM = _locPreSalesOrder.MxUOM,
                                                                                TQty = _locPreSalesOrder.TotalQty,
                                                                                Company = _locSalesQuotation.Company,
                                                                                SalesQuotation = _locSalesQuotation,
                                                                            };
                                                                            _saveDataSql.Save();
                                                                            _saveDataSql.Session.CommitTransaction();

                                                                            SetRemainQuantity(_currSession, _locPreSalesOrder);

                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    #endregion TransferTypeRuleIndirect
                                                }
                                                SuccessMessageShow("PreSales Order Successfully Posted");
                                            }
                                        }
                                    }
                                }
                                #endregion PreSalesOrderXPO

                            }
                            ErrorMessageShow("Project Header Not Available");
                        }
                        ErrorMessageShow("Project Header Not Available");
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Controller = PreSalesOrder" + ex.ToString());
            }
        }

        private void PreSalesOrderListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(PreSalesOrder)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PreSalesOrder" + ex.ToString());
            }
        }

        #region Method

        private void SetRemainQuantity(Session _currSession, PreSalesOrder _locPreSalesOrder)
        {
            try
            {
                DateTime now = DateTime.Now;

                #region Remain Qty  with PostedCount != 0
                if (_locPreSalesOrder.PostedCount > 0)
                {
                    double _locRmDqty = 0;
                    double _locRmQty = 0;
                    double _locInvLineTotal = 0;
                    double _locRmTQty = 0;
                    bool _locActivationPosting = false;
                    Status _locStatus = Status.Posted;

                    if (_locPreSalesOrder.RmDQty > 0 || _locPreSalesOrder.RmQty > 0 && _locPreSalesOrder.MxTQty != _locPreSalesOrder.RmTQty)
                    {
                        _locRmDqty = _locPreSalesOrder.RmDQty - _locPreSalesOrder.DQty;
                        _locRmQty = _locPreSalesOrder.RmQty - _locPreSalesOrder.Qty;

                        if (_locPreSalesOrder.Item != null && _locPreSalesOrder.MxUOM != null && _locPreSalesOrder.MxDUOM != null)
                        {
                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("Item", _locPreSalesOrder.Item),
                                                     new BinaryOperator("UOM", _locPreSalesOrder.MxUOM),
                                                     new BinaryOperator("DefaultUOM", _locPreSalesOrder.MxDUOM),
                                                     new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDqty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDqty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDqty;
                                }
                            }
                        }
                        else
                        {
                            _locInvLineTotal = _locRmQty + _locRmDqty;
                        }

                        if (_locRmDqty > 0 || _locRmQty > 0)
                        {
                            _locRmTQty = _locPreSalesOrder.RmTQty - _locPreSalesOrder.TotalQty;
                        }
                        else
                        {
                            _locRmTQty = _locPreSalesOrder.RmTQty - _locPreSalesOrder.TotalQty;
                            _locActivationPosting = true;
                            _locStatus = Status.Lock;
                        }


                        _locPreSalesOrder.ActivationPosting = _locActivationPosting;
                        _locPreSalesOrder.Select = false;
                        _locPreSalesOrder.RmDQty = _locRmDqty;
                        _locPreSalesOrder.RmQty = _locRmQty;
                        _locPreSalesOrder.RmTQty = _locRmTQty;
                        _locPreSalesOrder.PostedCount = _locPreSalesOrder.PostedCount + 1;
                        _locPreSalesOrder.DQty = 0;
                        _locPreSalesOrder.Qty = 0;
                        _locPreSalesOrder.TotalQty = 0;
                        _locPreSalesOrder.Status = _locStatus;
                        _locPreSalesOrder.StatusDate = now;
                        _locPreSalesOrder.Save();
                        _locPreSalesOrder.Session.CommitTransaction();
                    }
                }
                #endregion Remain Qty  with PostedCount != 0

                #region Remain Qty with PostedCount == 0
                else
                {
                    double _locRmDqty = 0;
                    double _locRmQty = 0;
                    double _locInvLineTotal = 0;
                    double _locRmTQty = 0;
                    _locRmDqty = _locPreSalesOrder.MxDQty - _locPreSalesOrder.DQty;
                    _locRmQty = _locPreSalesOrder.MxQty - _locPreSalesOrder.Qty;

                    #region Remain > 0
                    if (_locRmDqty > 0 || _locRmQty > 0)
                    {
                        if (_locPreSalesOrder.Item != null && _locPreSalesOrder.MxUOM != null && _locPreSalesOrder.MxDUOM != null)
                        {
                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("Item", _locPreSalesOrder.Item),
                                                     new BinaryOperator("UOM", _locPreSalesOrder.MxUOM),
                                                     new BinaryOperator("DefaultUOM", _locPreSalesOrder.MxDUOM),
                                                     new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDqty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDqty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDqty;
                                }
                            }
                        }
                        else
                        {
                            _locInvLineTotal = _locRmQty + _locRmDqty;
                        }

                        if (_locInvLineTotal > 0)
                        {
                            if (_locPreSalesOrder.RmTQty > 0)
                            {
                                _locRmTQty = _locPreSalesOrder.RmTQty - _locInvLineTotal;
                            }
                            else
                            {
                                _locRmTQty = _locInvLineTotal;
                            }

                        }

                        _locPreSalesOrder.RmDQty = _locRmDqty;
                        _locPreSalesOrder.RmQty = _locRmQty;
                        _locPreSalesOrder.RmTQty = _locRmTQty;
                        _locPreSalesOrder.PostedCount = _locPreSalesOrder.PostedCount + 1;
                        _locPreSalesOrder.Select = false;
                        _locPreSalesOrder.DQty = 0;
                        _locPreSalesOrder.Qty = 0;
                        _locPreSalesOrder.TotalQty = 0;
                        _locPreSalesOrder.Status = Status.Posted;
                        _locPreSalesOrder.StatusDate = now;
                        _locPreSalesOrder.Save();
                        _locPreSalesOrder.Session.CommitTransaction();
                    }
                    #endregion Remain > 0

                    #region Remain <= 0
                    if (_locRmDqty <= 0 && _locRmQty <= 0)
                    {
                        _locPreSalesOrder.ActivationPosting = true;
                        _locPreSalesOrder.RmDQty = 0;
                        _locPreSalesOrder.RmQty = 0;
                        _locPreSalesOrder.RmTQty = 0;
                        _locPreSalesOrder.PostedCount = _locPreSalesOrder.PostedCount + 1;
                        _locPreSalesOrder.Select = false;
                        _locPreSalesOrder.DQty = 0;
                        _locPreSalesOrder.Qty = 0;
                        _locPreSalesOrder.TotalQty = 0;
                        _locPreSalesOrder.Status = Status.Lock;
                        _locPreSalesOrder.StatusDate = now;
                        _locPreSalesOrder.Save();
                        _locPreSalesOrder.Session.CommitTransaction();
                    }
                    #endregion Remain < 0
                }
                #endregion Remain Qty with PostedCount == 0

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PreSalesOrder " + ex.ToString());
            }
        }

        #endregion Method

        #region Global

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion global

    }
}
