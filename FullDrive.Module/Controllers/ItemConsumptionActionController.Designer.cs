﻿namespace FullDrive.Module.Controllers
{
    partial class ItemConsumptionActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ItemConsumptionProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ItemConsumptionPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ItemConsumptionListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.ItemConsumptionListviewFilterApprovalSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // ItemConsumptionProgressAction
            // 
            this.ItemConsumptionProgressAction.Caption = "Progress";
            this.ItemConsumptionProgressAction.ConfirmationMessage = null;
            this.ItemConsumptionProgressAction.Id = "ItemConsumptionProgressActionId";
            this.ItemConsumptionProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ItemConsumption);
            this.ItemConsumptionProgressAction.ToolTip = null;
            this.ItemConsumptionProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ItemConsumptionProgressAction_Execute);
            // 
            // ItemConsumptionPostingAction
            // 
            this.ItemConsumptionPostingAction.Caption = "Posting";
            this.ItemConsumptionPostingAction.ConfirmationMessage = null;
            this.ItemConsumptionPostingAction.Id = "ItemConsumptionPostingActionId";
            this.ItemConsumptionPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ItemConsumption);
            this.ItemConsumptionPostingAction.ToolTip = null;
            this.ItemConsumptionPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ItemConsumptionPostingAction_Execute);
            // 
            // ItemConsumptionListviewFilterSelectionAction
            // 
            this.ItemConsumptionListviewFilterSelectionAction.Caption = "Filter";
            this.ItemConsumptionListviewFilterSelectionAction.ConfirmationMessage = null;
            this.ItemConsumptionListviewFilterSelectionAction.Id = "ItemConsumptionListviewFilterSelectionActionId";
            this.ItemConsumptionListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.ItemConsumptionListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ItemConsumption);
            this.ItemConsumptionListviewFilterSelectionAction.ToolTip = null;
            this.ItemConsumptionListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.ItemConsumptionListviewFilterSelectionAction_Execute);
            // 
            // ItemConsumptionListviewFilterApprovalSelectionAction
            // 
            this.ItemConsumptionListviewFilterApprovalSelectionAction.Caption = "Filter Approval";
            this.ItemConsumptionListviewFilterApprovalSelectionAction.ConfirmationMessage = null;
            this.ItemConsumptionListviewFilterApprovalSelectionAction.Id = "ItemConsumptionListviewFilterApprovalSelectionActionId";
            this.ItemConsumptionListviewFilterApprovalSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.ItemConsumptionListviewFilterApprovalSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ItemConsumption);
            this.ItemConsumptionListviewFilterApprovalSelectionAction.ToolTip = null;
            this.ItemConsumptionListviewFilterApprovalSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.ItemConsumptionListviewFilterApprovalSelectionAction_Execute);
            // 
            // ItemConsumptionActionController
            // 
            this.Actions.Add(this.ItemConsumptionProgressAction);
            this.Actions.Add(this.ItemConsumptionPostingAction);
            this.Actions.Add(this.ItemConsumptionListviewFilterSelectionAction);
            this.Actions.Add(this.ItemConsumptionListviewFilterApprovalSelectionAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction ItemConsumptionProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction ItemConsumptionPostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction ItemConsumptionListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction ItemConsumptionListviewFilterApprovalSelectionAction;
    }
}
