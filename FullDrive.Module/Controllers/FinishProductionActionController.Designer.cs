﻿namespace FullDrive.Module.Controllers
{
    partial class FinishProductionActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.FinishProductionProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.FinishProductionPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.FinishProductionFilterAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.FinishProductionListviewFilterApprovalSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // FinishProductionProgressAction
            // 
            this.FinishProductionProgressAction.Caption = "Progress";
            this.FinishProductionProgressAction.ConfirmationMessage = null;
            this.FinishProductionProgressAction.Id = "FinishProductionProgressActionId";
            this.FinishProductionProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.FinishProduction);
            this.FinishProductionProgressAction.ToolTip = null;
            this.FinishProductionProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.FinishProductionProgressAction_Execute);
            // 
            // FinishProductionPostingAction
            // 
            this.FinishProductionPostingAction.Caption = "Posting";
            this.FinishProductionPostingAction.ConfirmationMessage = null;
            this.FinishProductionPostingAction.Id = "FinishProductionPostingId";
            this.FinishProductionPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.FinishProduction);
            this.FinishProductionPostingAction.ToolTip = null;
            this.FinishProductionPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.FinishProductionPostingAction_Execute);
            // 
            // FinishProductionFilterAction
            // 
            this.FinishProductionFilterAction.Caption = "Filter";
            this.FinishProductionFilterAction.ConfirmationMessage = null;
            this.FinishProductionFilterAction.Id = "FinishProductionPostingActionId";
            this.FinishProductionFilterAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.FinishProductionFilterAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.FinishProduction);
            this.FinishProductionFilterAction.ToolTip = null;
            this.FinishProductionFilterAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.FinishProductionFilterAction_Execute);
            // 
            // FinishProductionListviewFilterApprovalSelectionAction
            // 
            this.FinishProductionListviewFilterApprovalSelectionAction.Caption = "Filter Approval";
            this.FinishProductionListviewFilterApprovalSelectionAction.ConfirmationMessage = null;
            this.FinishProductionListviewFilterApprovalSelectionAction.Id = "FinishProductionListviewFilterApprovalSelectionActionId";
            this.FinishProductionListviewFilterApprovalSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.FinishProductionListviewFilterApprovalSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.FinishProduction);
            this.FinishProductionListviewFilterApprovalSelectionAction.ToolTip = null;
            this.FinishProductionListviewFilterApprovalSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.FinishProductionListviewFilterApprovalSelectionAction_Execute);
            // 
            // FinishProductionActionController
            // 
            this.Actions.Add(this.FinishProductionProgressAction);
            this.Actions.Add(this.FinishProductionPostingAction);
            this.Actions.Add(this.FinishProductionFilterAction);
            this.Actions.Add(this.FinishProductionListviewFilterApprovalSelectionAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction FinishProductionProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction FinishProductionPostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction FinishProductionFilterAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction FinishProductionListviewFilterApprovalSelectionAction;
    }
}
