﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CashAdvanceActionController : ViewController
    {
        #region Default

        private ChoiceActionItem _selectionListviewFilter;
        public CashAdvanceActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.

            #region FilterStatus
            CashAdvanceListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                CashAdvanceListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterStatus

            #region FilterApproval
            CashAdvanceListviewFilterApprovalSelectionAction.Items.Clear();
            foreach (object _currApprovalFilter in Enum.GetValues(typeof(CustomProcess.ApprovalFilter)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalFilter));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApprovalFilter), _currApprovalFilter);
                CashAdvanceListviewFilterApprovalSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterApproval
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        private void CashAdvanceProgressActionController_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        CashAdvance _locCashAdvanceOS = (CashAdvance)_objectSpace.GetObject(obj);

                        if (_locCashAdvanceOS != null)
                        {
                            if (_locCashAdvanceOS.Code != null)
                            {
                                _currObjectId = _locCashAdvanceOS.Code;

                                CashAdvance _locCashAdvanceXPO = _currSession.FindObject<CashAdvance>
                                                                  (new GroupOperator(GroupOperatorType.And,
                                                                  new BinaryOperator("Code", _currObjectId)));

                                if (_locCashAdvanceXPO != null)
                                {
                                    if (_locCashAdvanceXPO.Status == Status.Open || _locCashAdvanceXPO.Status == Status.Progress)
                                    {
                                        if (_locCashAdvanceXPO.Status == Status.Open)
                                        {
                                            _locCashAdvanceXPO.Status = Status.Progress;
                                            _locCashAdvanceXPO.Save();
                                            _locCashAdvanceXPO.Session.CommitTransaction();
                                        }

                                        XPCollection<CashAdvanceLine> _locCashAdvanceLines = new XPCollection<CashAdvanceLine>
                                                                                             (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                              new BinaryOperator("CashAdvance", _locCashAdvanceXPO),
                                                                                              new BinaryOperator("Status", Status.Open)));

                                        if (_locCashAdvanceLines != null && _locCashAdvanceLines.Count > 0)
                                        {
                                            foreach (CashAdvanceLine _locCashAdvanceLine in _locCashAdvanceLines)
                                            {
                                                _locCashAdvanceLine.Status = Status.Progress;
                                                _locCashAdvanceLine.StatusDate = now;
                                                _locCashAdvanceLine.ActiveApprovedAmount = true;
                                                _locCashAdvanceLine.Save();
                                                _locCashAdvanceLine.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                    SuccessMessageShow("CashAdvance has successfully updated to progress");
                                }
                                else
                                {
                                    ErrorMessageShow("CashAdvacne Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Cash Advance Not Available");
                            }
                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = CashAdvance" + ex.ToString());
            }
        }

        private void CashAdvancePostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        CashAdvance _locCashAdvanceOS = (CashAdvance)_objectSpace.GetObject(obj);

                        if (_locCashAdvanceOS != null)
                        {
                            if (_locCashAdvanceOS.Code != null)
                            {
                                _currObjectId = _locCashAdvanceOS.Code;

                                CashAdvance _locCashAdvanceXPO = _currSession.FindObject<CashAdvance>
                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                  new BinaryOperator("Code", _currObjectId)));

                                if (_locCashAdvanceXPO != null)
                                {
                                    if (_locCashAdvanceXPO.Status == Status.Progress)
                                    {
                                        ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("EndApproval", true),
                                                                            new BinaryOperator("CashAdvance", _locCashAdvanceXPO)));

                                        if (_locApprovalLineXPO != null)
                                        {
                                            SetProcessCountForPostingCashAdvance(_currSession, _locCashAdvanceXPO);
                                            SetStatusCashAdvanceLine(_currSession, _locCashAdvanceXPO);
                                            SetPaymentRealization(_currSession, _locCashAdvanceXPO);
                                            SetJournalCashBon(_currSession, _locCashAdvanceXPO);
                                            SetStatusCashAdvance(_currSession, _locCashAdvanceXPO);
                                            SuccessMessageShow(_locCashAdvanceXPO.Code + " has been change successfully to Posted");
                                        }
                                        else
                                        {
                                            ErrorMessageShow("Data Approval Not Available");
                                        }
                                    }
                                    else
                                    {
                                        ErrorMessageShow("Data CashAdvance Not Available");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data CashAdvance Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data CashAdvance Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Data CashAdvance Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CashAdvance " + ex.ToString());
            }
        }

        private void CashAdvanceListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(CashAdvance)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CashAdvance " + ex.ToString());
            }
        }

        private void CashAdvanceListviewFilterApprovalSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(CashAdvance)))
                {
                    if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.Approval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal)));
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.EndApproval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal);
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.AllData)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved1", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal));
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CashAdvance " + ex.ToString());
            }
        }

        //==== Code Only ====

        #region ProcessForCashAdvance

        private void SetProcessCountForPostingCashAdvance(Session _currSession, CashAdvance _locCashAdvanceXPO)
        {
            try
            {
                if (_locCashAdvanceXPO != null)
                {
                    XPCollection<CashAdvanceLine> _locCALines = new XPCollection<CashAdvanceLine>(_currSession,
                                                                new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("CashAdvance", _locCashAdvanceXPO)));

                    if (_locCALines != null && _locCALines.Count > 0)
                    {
                        foreach (CashAdvanceLine _locCALine in _locCALines)
                        {
                            if (_locCALine.Status == Status.Approved)
                            {
                                _locCALine.ProcessCount = _locCALine.ProcessCount + 1;
                                _locCALine.Save();
                                _locCALine.Session.CommitTransaction();
                            }
                        }
                    }
                    else
                    {
                        ErrorMessageShow("Data CashAdvanceLine Not Available");
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CashAdvance " + ex.ToString());
            }
        }

        private void SetStatusCashAdvanceLine(Session _currSession, CashAdvance _locCashAdvanceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locCashAdvanceXPO != null)
                {
                    XPCollection<CashAdvanceLine> _locCALines = new XPCollection<CashAdvanceLine>(_currSession,
                                                                new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("CashAdvance", _locCashAdvanceXPO)));

                    if (_locCALines != null && _locCALines.Count > 0)
                    {
                        foreach (CashAdvanceLine _locCALine in _locCALines)
                        {
                            if (_locCALine.Status == Status.Progress)
                            {
                                _locCALine.ActivationPosting = true;
                                _locCALine.ActiveApprovedAmount = true;
                                _locCALine.Status = Status.Posted;
                                _locCALine.StatusDate = now;
                                _locCALine.Save();
                                _locCALine.Session.CommitTransaction();
                            }
                        }
                    }
                    else
                    {
                        ErrorMessageShow("Data CashAdvanceLine Not Available");
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CashAdvance " + ex.ToString());
            }
        }

        private void SetPaymentRealization(Session _currSession, CashAdvance _locCashAdvanceXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locCashAdvanceXPO != null)
                {
                    if (_locCashAdvanceXPO.Status == Status.Progress)
                    {
                        PaymentRealization _saveData = new PaymentRealization(_currSession)
                        {
                            CashAdvance = _locCashAdvanceXPO,
                            Name = _locCashAdvanceXPO.Name,
                            Employee = _locCashAdvanceXPO.Employee,
                            Position = _locCashAdvanceXPO.Position,
                            Section = _locCashAdvanceXPO.Section,
                            Division = _locCashAdvanceXPO.Division,
                            Department = _locCashAdvanceXPO.Department,
                            Company = _locCashAdvanceXPO.Company,
                            TotalAmount = _locCashAdvanceXPO.CashAdvanceTotalAmount,
                            Status = Status.Open,
                            StatusDate = DateTime.Now,
                        };
                        _saveData.Save();
                        _saveData.Session.CommitTransaction();
                    }
                    else
                    {
                        ErrorMessageShow("Data CashAdvance Not Available");
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CashAdvance ", ex.ToString());
            }
        }

        private void SetJournalCashBon(Session _currSession, CashAdvance _locCashAdvanceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locCAApproved = 0;
                double _locSetCAApproved = 0;
                double _locCAAmount = 0;
                double _locSetCAAmount = 0;

                if (_locCashAdvanceXPO != null)
                {
                    #region JournalCashBonByCompany
                    XPQuery<CashAdvanceLine> _CashAdvanceLinesQueryBasedCompany = new XPQuery<CashAdvanceLine>(_currSession);

                    var _CashAdvanceLineByCompanys = from cal in _CashAdvanceLinesQueryBasedCompany
                                                     where (cal.CashAdvance == _locCashAdvanceXPO
                                                     && cal.Company == _locCashAdvanceXPO.Company
                                                     && (cal.Status == Status.Progress || cal.Status == Status.Posted))
                                                     group cal by cal.Company into g
                                                     select new { Company = g.Key };

                    if (_CashAdvanceLineByCompanys != null)
                    {
                        foreach (var _CashAdvanceLineByCompany in _CashAdvanceLineByCompanys)
                        {
                            #region SetAmount

                            XPCollection<CashAdvanceLine> _locCashAdvanceLines = new XPCollection<CashAdvanceLine>(_currSession,
                                                                                 new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("CashAdvance", _locCashAdvanceXPO)));

                            if (_locCashAdvanceLines != null && _locCashAdvanceLines.Count() > 0)
                            {
                                foreach (CashAdvanceLine _locCashAdvanceLine in _locCashAdvanceLines)
                                {
                                    _locCAApproved = _locCAApproved + _locCashAdvanceLine.ApprovedAmount;
                                }
                            }

                            _locSetCAApproved = _locCAApproved;

                            #endregion SetAmount

                            if (_locSetCAApproved > 0)
                            {
                                #region JournalMap
                                XPCollection<JournalMap> _locJournalMaps = new XPCollection<JournalMap>
                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("CompanyAccountGroup", _CashAdvanceLineByCompany.Company.CompanyAccountGroup)));

                                if (_locJournalMaps != null && _locJournalMaps.Count() > 0)
                                {
                                    foreach (JournalMap _locJournalMap in _locJournalMaps)
                                    {
                                        XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("JournalMap", _locJournalMap)));

                                        if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                        {
                                            foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                            {
                                                AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                             new BinaryOperator("PostingType", PostingType.Advances),
                                                                             new BinaryOperator("OrderType", OrderType.Account),
                                                                             new BinaryOperator("PostingMethod", PostingMethod.CashAdvance),
                                                                             new BinaryOperator("PostingMethodType", PostingMethodType.Advances)));

                                                if (_locAccountMap != null)
                                                {
                                                    XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                        new BinaryOperator("Active", true)));

                                                    if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                    {
                                                        double _locTotalAmountDebit = 0;
                                                        double _locTotalAmountCredit = 0;
                                                        double _locTotalBalance = 0;

                                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                        {
                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                            {
                                                                _locTotalAmountDebit = _locSetCAApproved;
                                                            }
                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                            {
                                                                _locTotalAmountCredit = _locSetCAApproved;
                                                            }

                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                            {
                                                                PostingDate = now,
                                                                PostingType = PostingType.Advances,
                                                                PostingMethod = PostingMethod.CashAdvance,
                                                                Account = _locAccountMapLine.Account,
                                                                Debit = _locTotalAmountDebit,
                                                                Credit = _locTotalAmountCredit,
                                                                Company = _locCashAdvanceXPO.Company,
                                                                CashAdvance = _locCashAdvanceXPO,
                                                            };
                                                            _saveGeneralJournal.Save();
                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                            if (_locAccountMapLine.Account.Code != null)
                                                            {
                                                                #region COA
                                                                ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Code", _locAccountMapLine.Account.Code)));

                                                                if (_locCOA != null)
                                                                {
                                                                    if (_locCOA.BalanceType == BalanceType.Change)
                                                                    {
                                                                        if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            if (_locTotalAmountDebit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                            }
                                                                            if (_locTotalAmountCredit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                            }
                                                                        }
                                                                        if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            if (_locTotalAmountDebit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                            }
                                                                            if (_locTotalAmountCredit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                            }
                                                                        }
                                                                    }

                                                                    _locCOA.Balance = _locTotalBalance;
                                                                    _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                    _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                    _locCOA.Save();
                                                                    _locCOA.Session.CommitTransaction();
                                                                }
                                                                #endregion COA
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion JournalMap
                            }
                        }
                    }
                    #endregion JournalCashBonByCompany

                    #region JournalCashBonByDepartement
                    XPQuery<CashAdvanceLine> _CashAdvanceLinesQueryBasedDepartment = new XPQuery<CashAdvanceLine>(_currSession);

                    var _CashAdvanceLineByDepartments = from cal in _CashAdvanceLinesQueryBasedDepartment
                                                        where (cal.CashAdvance == _locCashAdvanceXPO
                                                        && cal.Department == _locCashAdvanceXPO.Department
                                                        && (cal.Status == Status.Progress || cal.Status == Status.Posted))
                                                        group cal by cal.Department into g
                                                        select new { Department = g.Key };

                    if (_CashAdvanceLineByDepartments != null)
                    {
                        foreach (var _CashAdvanceLineByDepartment in _CashAdvanceLineByDepartments)
                        {
                            #region SetAmount
                            XPCollection<CashAdvanceLine> _locCashAdvanceLines = new XPCollection<CashAdvanceLine>(_currSession,
                                                                                 new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("CashAdvance", _locCashAdvanceXPO)));

                            if (_locCashAdvanceLines != null && _locCashAdvanceLines.Count() > 0)
                            {
                                foreach (CashAdvanceLine _locCashAdvanceLine in _locCashAdvanceLines)
                                {
                                    _locCAAmount = _locCAAmount + _locCashAdvanceLine.CashAdvanceAmount;
                                }
                            }

                            _locSetCAAmount = _locCAAmount;
                            #endregion SetAmount

                            if (_locSetCAAmount > 0)
                            {
                                #region JournalMap
                                XPCollection<JournalMap> _locJournalMaps = new XPCollection<JournalMap>
                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("DepartmentAccountGroup", _CashAdvanceLineByDepartment.Department.DepartmentAccountGroup)));

                                if (_locJournalMaps != null && _locJournalMaps.Count() > 0)
                                {
                                    foreach (JournalMap _locJournalMap in _locJournalMaps)
                                    {
                                        XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("JournalMap", _locJournalMap)));

                                        if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                        {
                                            foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                            {
                                                AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                             new BinaryOperator("PostingType", PostingType.Advances),
                                                                             new BinaryOperator("OrderType", OrderType.Account),
                                                                             new BinaryOperator("PostingMethod", PostingMethod.CashAdvance),
                                                                             new BinaryOperator("PostingMethodType", PostingMethodType.Advances)));

                                                if (_locAccountMap != null)
                                                {
                                                    XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                        new BinaryOperator("Active", true)));

                                                    if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                    {
                                                        double _locTotalAmountDebit = 0;
                                                        double _locTotalAmountCredit = 0;
                                                        double _locTotalBalance = 0;

                                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                        {
                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                            {
                                                                _locTotalAmountDebit = _locSetCAAmount;
                                                            }
                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                            {
                                                                _locTotalAmountCredit = _locSetCAAmount;
                                                            }

                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                            {
                                                                PostingDate = now,
                                                                PostingType = PostingType.Advances,
                                                                PostingMethod = PostingMethod.CashAdvance,
                                                                Account = _locAccountMapLine.Account,
                                                                Debit = _locTotalAmountDebit,
                                                                Credit = _locTotalAmountCredit,
                                                                Company = _locCashAdvanceXPO.Company,
                                                                CashAdvance = _locCashAdvanceXPO
                                                            };
                                                            _saveGeneralJournal.Save();
                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                            if (_locAccountMapLine.Account.Code != null)
                                                            {
                                                                #region COA
                                                                ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Code", _locAccountMapLine.Account.Code)));

                                                                if (_locCOA != null)
                                                                {
                                                                    if (_locCOA.BalanceType == BalanceType.Change)
                                                                    {
                                                                        if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            if (_locTotalAmountDebit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                            }
                                                                            if (_locTotalAmountCredit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                            }
                                                                        }
                                                                        if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            if (_locTotalAmountDebit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                            }
                                                                            if (_locTotalAmountCredit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                            }
                                                                        }
                                                                    }

                                                                    _locCOA.Balance = _locTotalBalance;
                                                                    _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                    _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                    _locCOA.Save();
                                                                    _locCOA.Session.CommitTransaction();
                                                                }
                                                                #endregion COA
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion JournalMap
                            }
                        }
                    }
                    #endregion JournalCashBonByDepartement
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CashAdvance ", ex.ToString());
            }
        }

        private void SetStatusCashAdvance(Session _currSession, CashAdvance _locCashAdvanceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locCountStatus = 0;
                bool _locActivationPosting = false;

                if (_locCashAdvanceXPO != null)
                {
                    XPCollection<CashAdvanceLine> _locCALines = new XPCollection<CashAdvanceLine>
                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                 new BinaryOperator("CashAdvance", _locCashAdvanceXPO)));

                    if (_locCALines != null && _locCALines.Count() > 0)
                    {
                        foreach (CashAdvanceLine _locCALine in _locCALines)
                        {
                            if (_locCALine.Status != Status.Close)
                            {
                                _locCountStatus = _locCountStatus + 1;
                            }
                        }
                    }

                    if (_locCountStatus > 0)
                    {
                        if (_locCALines.Count() == _locCountStatus)
                        {
                            _locActivationPosting = true;
                        }
                    }

                    _locCashAdvanceXPO.Status = Status.Posted;
                    _locActivationPosting = true;
                    _locCashAdvanceXPO.StatusDate = now;
                    _locCashAdvanceXPO.Save();
                    _locCashAdvanceXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CashAdvance " + ex.ToString());
            }
        }

        #endregion ProcessForCashAdvance

        #region GlobalMethod

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion GlobalMethod

    }
}
