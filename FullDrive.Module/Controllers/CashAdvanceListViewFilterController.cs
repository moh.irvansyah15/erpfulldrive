﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;
using FullDrive.Module.BusinessObjects;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CashAdvanceListViewFilterController : ViewController
    {
        #region Default

        public CashAdvanceListViewFilterController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.

            RegisterActions(components);
            TargetViewId = "CashAdvance_ListView";
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.

            var User = SecuritySystem.CurrentUserName;
            string _beginString1 = null;
            string _endString1 = null;
            string _fullString1 = null;
            string _beginString2 = null;
            string _endString2 = null;
            string _fullString2 = null;
            string _beginString3 = null;
            string _endString3 = null;
            string _fullString3 = null;
            string _beginString4 = null;
            string _endString4 = null;
            string _fullString4 = null;
            string _fullString = null;
            var ListView = View as ListView;
            Session currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
            UserAccess _locUserAccess = currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));
            if (_locUserAccess != null)
            {
                if (_locUserAccess.Employee != null)
                {
                    List<string> _stringCompany = new List<string>();
                    List<string> _stringDivision = new List<string>();
                    List<string> _stringDepartment = new List<string>();
                    List<string> _stringSection = new List<string>();

                    XPCollection<OrganizationSetupDetail> _locOrganizationSetupDetails = new XPCollection<OrganizationSetupDetail>
                                                                                         (currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                          new BinaryOperator("UserAccess", _locUserAccess),
                                                                                          new BinaryOperator("ObjectList", CustomProcess.ObjectList.CashAdvance),
                                                                                          new BinaryOperator("PageType", CustomProcess.PageType.ListView),
                                                                                          new BinaryOperator("Active", true)));

                    if (_locOrganizationSetupDetails != null && _locOrganizationSetupDetails.Count > 0)
                    {
                        foreach (OrganizationSetupDetail _locOrganizationSetupDetail in _locOrganizationSetupDetails)
                        {
                            if (_locOrganizationSetupDetail.Company != null)
                            {
                                _stringCompany.Add(_locOrganizationSetupDetail.Company.Code);
                            }

                            if (_locOrganizationSetupDetail.Division != null)
                            {
                                _stringDivision.Add(_locOrganizationSetupDetail.Division.Code);
                            }

                            if (_locOrganizationSetupDetail.Department != null)
                            {
                                _stringDepartment.Add(_locOrganizationSetupDetail.Department.Code);
                            }

                            if (_locOrganizationSetupDetail.Section != null)
                            {
                                _stringSection.Add(_locOrganizationSetupDetail.Section.Code);
                            }
                        }
                    }

                    #region Company

                    IEnumerable<string> _stringArrayCompanyDistinct = _stringCompany.Distinct();
                    string[] _stringArrayCompanyList = _stringArrayCompanyDistinct.ToArray();
                    if (_stringArrayCompanyList.Length == 1)
                    {
                        for (int i = 0; i < _stringArrayCompanyList.Length; i++)
                        {
                            Company _locCompany = currentSession.FindObject<Company>(new BinaryOperator("Code", _stringArrayCompanyList[i]));
                            if (_locCompany != null)
                            {
                                if (i == 0)
                                {
                                    _beginString1 = "[Company.Code]=='" + _locCompany.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArrayCompanyList.Length > 1)
                    {
                        for (int i = 0; i < _stringArrayCompanyList.Length; i++)
                        {
                            Company _locCompany = currentSession.FindObject<Company>(new BinaryOperator("Code", _stringArrayCompanyList[i]));
                            if (_locCompany != null)
                            {
                                if (i == 0)
                                {
                                    _beginString1 = "[Company.Code]=='" + _locCompany.Code + "'";
                                }
                                else
                                {
                                    _endString1 = " OR [Company.Code]=='" + _locCompany.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString1 = _beginString1 + _endString1;

                    #endregion Company

                    #region Division

                    IEnumerable<string> _stringArrayDivisionDistinct = _stringDivision.Distinct();
                    string[] _stringArrayDivisionList = _stringArrayDivisionDistinct.ToArray();
                    if (_stringArrayDivisionList.Length == 1)
                    {
                        for (int i = 0; i < _stringArrayDivisionList.Length; i++)
                        {
                            Division _locDivision = currentSession.FindObject<Division>(new BinaryOperator("Code", _stringArrayDivisionList[i]));
                            if (_locDivision != null)
                            {
                                if (i == 0)
                                {
                                    _beginString2 = "[Division.Code]=='" + _locDivision.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArrayDivisionList.Length > 1)
                    {
                        for (int i = 0; i < _stringArrayDivisionList.Length; i++)
                        {
                            Division _locDivision = currentSession.FindObject<Division>(new BinaryOperator("Code", _stringArrayDivisionList[i]));
                            if (_locDivision != null)
                            {
                                if (i == 0)
                                {
                                    _beginString2 = "[Division.Code]=='" + _locDivision.Code + "'";
                                }
                                else
                                {
                                    _endString2 = " OR [Division.Code]=='" + _locDivision.Code + "'";
                                }
                            }
                        }
                    }
                    _fullString2 = _beginString2 + _endString2;

                    #endregion Division

                    #region Department

                    IEnumerable<string> _stringArrayDepartmentDistinct = _stringDepartment.Distinct();
                    string[] _stringArrayDepartmentList = _stringArrayDepartmentDistinct.ToArray();
                    if (_stringArrayDepartmentList.Length == 1)
                    {
                        for (int i = 0; i < _stringArrayDepartmentList.Length; i++)
                        {
                            Department _locDepartment = currentSession.FindObject<Department>(new BinaryOperator("Code", _stringArrayDepartmentList[i]));
                            if (_locDepartment != null)
                            {
                                if (i == 0)
                                {
                                    _beginString3 = "[Department.Code]=='" + _locDepartment.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArrayDepartmentList.Length > 1)
                    {
                        for (int i = 0; i < _stringArrayDepartmentList.Length; i++)
                        {
                            Department _locDepartment = currentSession.FindObject<Department>(new BinaryOperator("Code", _stringArrayDepartmentList[i]));
                            if (_locDepartment != null)
                            {
                                if (i == 0)
                                {
                                    _beginString3 = "[Department.Code]=='" + _locDepartment.Code + "'";
                                }
                            }
                            else
                            {
                                _endString3 = " OR [Department.Code]=='" + _locDepartment.Code + "'";
                            }
                        }
                    }
                    _fullString3 = _beginString3 + _endString3;

                    #endregion Department

                    #region Section

                    IEnumerable<string> _stringArraySectionDistinct = _stringSection.Distinct();
                    string[] _stringArraySectionList = _stringArraySectionDistinct.ToArray();
                    if (_stringArraySectionList.Length == 1)
                    {
                        for (int i = 0; i < _stringArraySectionList.Length; i++)
                        {
                            Section _locSection = currentSession.FindObject<Section>(new BinaryOperator("Code", _stringArraySectionList[i]));
                            if (_locSection != null)
                            {
                                if (i == 0)
                                {
                                    _beginString4 = "[Section.Code]=='" + _locSection.Code + "'";
                                }
                            }
                        }
                    }
                    else if (_stringArraySectionList.Length > 1)
                    {
                        for (int i = 0; i < _stringArraySectionList.Length; i++)
                        {
                            Section _locSection = currentSession.FindObject<Section>(new BinaryOperator("Code", _stringArraySectionList[i]));
                            if (_locSection != null)
                            {
                                if (i == 0)
                                {
                                    _beginString4 = "[Section.Code]=='" + _locSection.Code + "'";
                                }
                            }
                            else
                            {
                                _endString4 = " OR [Section.Code]=='" + _locSection.Code + "'";
                            }
                        }
                    }
                    _fullString4 = _beginString4 + _endString4;

                    #endregion Section

                    #region FullString

                    if (_stringArrayCompanyList.Length == 1 && _stringArrayDivisionList.Length == 1 && _stringArrayDepartmentList.Length == 1 && _stringArraySectionList.Length == 1)
                    {
                        _fullString = _fullString1 + " AND " + _fullString2 + " AND " + _fullString3 + " AND " + _fullString4;
                    }
                    else if (_stringArrayCompanyList.Length == 1 && _stringArrayDivisionList.Length == 1 && _stringArrayDepartmentList.Length == 1 && _stringArraySectionList.Length == 0)
                    {
                        _fullString = _fullString1 + " AND " + _fullString2 + " AND " + _fullString3;
                    }
                    else if (_stringArrayCompanyList.Length == 1 && _stringArrayDivisionList.Length == 1 && _stringArrayDepartmentList.Length == 0 && _stringArraySectionList.Length == 0)
                    {
                        _fullString = _fullString1 + " AND " + _fullString2;
                    }
                    else if (_stringArrayCompanyList.Length == 1 && _stringArrayDivisionList.Length == 0 && _stringArrayDepartmentList.Length == 0 && _stringArraySectionList.Length == 0)
                    {
                        _fullString = _fullString1;
                    }
                    else if (_stringArrayCompanyList.Length > 1 && _stringArrayDivisionList.Length > 1 && _stringArrayDepartmentList.Length > 1 && _stringArraySectionList.Length > 1)
                    {
                        _fullString = _fullString1 + " OR " + _fullString2 + " OR " + _fullString3 + " OR " + _fullString4;
                    }
                    else if (_stringArrayCompanyList.Length > 1 && _stringArrayDivisionList.Length > 1 && _stringArrayDepartmentList.Length > 1 && _stringArraySectionList.Length == 0)
                    {
                        _fullString = _fullString1 + " OR " + _fullString2 + " OR " + _fullString3;
                    }
                    else if (_stringArrayCompanyList.Length > 1 && _stringArrayDivisionList.Length > 1 && _stringArrayDepartmentList.Length == 0 && _stringArraySectionList.Length == 0)
                    {
                        _fullString = _fullString1 + " OR " + _fullString2;
                    }
                    else if (_stringArrayCompanyList.Length > 1 && _stringArrayDivisionList.Length == 0 && _stringArrayDepartmentList.Length == 0 && _stringArraySectionList.Length == 0)
                    {
                        _fullString = _fullString1;
                    }
                    else if (_stringArrayCompanyList.Length == 1 && _stringArrayDivisionList.Length > 1 && _stringArrayDepartmentList.Length > 1 && _stringArraySectionList.Length > 1)
                    {
                        _fullString = _fullString1 + " OR " + _fullString2 + " OR " + _fullString3 + " OR " + _fullString4;
                    }
                    else if (_stringArrayCompanyList.Length == 1 && _stringArrayDivisionList.Length == 1 && _stringArrayDepartmentList.Length > 1 && _stringArraySectionList.Length > 1)
                    {
                        _fullString = _fullString1 + " OR " + _fullString2 + " OR " + _fullString3 + " OR " + _fullString4;
                    }
                    else if (_stringArrayCompanyList.Length == 1 && _stringArrayDivisionList.Length == 1 && _stringArrayDepartmentList.Length == 1 && _stringArraySectionList.Length > 1)
                    {
                        _fullString = _fullString1 + " OR " + _fullString2 + " OR " + _fullString3 + " OR " + _fullString4;
                    }
                    else if (_stringArrayCompanyList.Length == 1 && _stringArrayDivisionList.Length > 1 && _stringArrayDepartmentList.Length > 1 && _stringArraySectionList.Length == 0)
                    {
                        _fullString = _fullString1 + " OR " + _fullString2 + " OR " + _fullString3;
                    }
                    else if (_stringArrayCompanyList.Length == 1 && _stringArrayDivisionList.Length > 1 && _stringArrayDepartmentList.Length == 0 && _stringArraySectionList.Length == 0)
                    {
                        _fullString = _fullString1 + " OR " + _fullString2;
                    }
                    else if (_stringArrayCompanyList.Length == 0 && _stringArrayDivisionList.Length == 1 && _stringArrayDepartmentList.Length == 1 && _stringArraySectionList.Length == 1)
                    {
                        _fullString = _fullString2 + " AND " + _fullString3 + " AND " + _fullString4;
                    }
                    else if (_stringArrayCompanyList.Length == 0 && _stringArrayDivisionList.Length == 1 && _stringArrayDepartmentList.Length == 1 && _stringArraySectionList.Length == 0)
                    {
                        _fullString = _fullString2 + " AND " + _fullString3;
                    }
                    else if (_stringArrayCompanyList.Length == 0 && _stringArrayDivisionList.Length == 1 && _stringArrayDepartmentList.Length == 0 && _stringArraySectionList.Length == 0)
                    {
                        _fullString = _fullString2;
                    }
                    else if (_stringArrayCompanyList.Length == 0 && _stringArrayDivisionList.Length > 1 && _stringArrayDepartmentList.Length > 1 && _stringArraySectionList.Length > 1)
                    {
                        _fullString = _fullString2 + " OR " + _fullString3 + " OR " + _fullString4;
                    }
                    else if (_stringArrayCompanyList.Length == 0 && _stringArrayDivisionList.Length > 1 && _stringArrayDepartmentList.Length > 1 && _stringArraySectionList.Length > 1)
                    {
                        _fullString = _fullString2 + " OR " + _fullString3 + " OR " + _fullString4;
                    }
                    else if (_stringArrayCompanyList.Length == 0 && _stringArrayDivisionList.Length > 1 && _stringArrayDepartmentList.Length > 1 && _stringArraySectionList.Length == 0)
                    {
                        _fullString = _fullString2 + " OR " + _fullString3;
                    }
                    else if (_stringArrayCompanyList.Length == 0 && _stringArrayDivisionList.Length > 1 && _stringArrayDepartmentList.Length == 0 && _stringArraySectionList.Length == 0)
                    {
                        _fullString = _fullString2;
                    }

                    #endregion FullString

                    ListView.CollectionSource.Criteria["CashAdvanceLineFilter"] = CriteriaOperator.Parse(_fullString);
                }
            }
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

    }
}
