﻿namespace FullDrive.Module.Controllers
{
    partial class TransferOrderApprovalDetailController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TransferOrderApprovalDetailAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // TransferOrderApprovalDetailAction
            // 
            this.TransferOrderApprovalDetailAction.Caption = "Approval";
            this.TransferOrderApprovalDetailAction.ConfirmationMessage = null;
            this.TransferOrderApprovalDetailAction.Id = "TransferOrderApprovalDetailActionId";
            this.TransferOrderApprovalDetailAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.TransferOrderApprovalDetailAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.TransferOrder);
            this.TransferOrderApprovalDetailAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.TransferOrderApprovalDetailAction.ToolTip = null;
            this.TransferOrderApprovalDetailAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.TransferOrderApprovalDetailAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.TransferOrderApprovalDetailAction_Execute);
            // 
            // TransferOrderApprovalDetailController
            // 
            this.Actions.Add(this.TransferOrderApprovalDetailAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction TransferOrderApprovalDetailAction;
    }
}
