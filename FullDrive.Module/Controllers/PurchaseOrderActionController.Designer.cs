﻿namespace FullDrive.Module.Controllers
{
    partial class PurchaseOrderActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PurchaseOrderProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchaseOrderPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchaseOrderGetPRAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchaseOrderListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.PurchaseOrderPurchaseReturnAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchaseOrderListviewFilterApprovalSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // PurchaseOrderProgressAction
            // 
            this.PurchaseOrderProgressAction.Caption = "Progress";
            this.PurchaseOrderProgressAction.ConfirmationMessage = null;
            this.PurchaseOrderProgressAction.Id = "PurchaseOrderProgressActionId";
            this.PurchaseOrderProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseOrder);
            this.PurchaseOrderProgressAction.ToolTip = null;
            this.PurchaseOrderProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseOrderProgressAction_Execute);
            // 
            // PurchaseOrderPostingAction
            // 
            this.PurchaseOrderPostingAction.Caption = "Posting";
            this.PurchaseOrderPostingAction.ConfirmationMessage = null;
            this.PurchaseOrderPostingAction.Id = "PurchaseOrderPostingActionId";
            this.PurchaseOrderPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseOrder);
            this.PurchaseOrderPostingAction.ToolTip = null;
            this.PurchaseOrderPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseOrderPostingAction_Execute);
            // 
            // PurchaseOrderGetPRAction
            // 
            this.PurchaseOrderGetPRAction.Caption = "Get PR";
            this.PurchaseOrderGetPRAction.ConfirmationMessage = null;
            this.PurchaseOrderGetPRAction.Id = "PurchaseOrderGetPRActionId";
            this.PurchaseOrderGetPRAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseOrder);
            this.PurchaseOrderGetPRAction.ToolTip = null;
            this.PurchaseOrderGetPRAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseOrderGetPRAction_Execute);
            // 
            // PurchaseOrderListviewFilterSelectionAction
            // 
            this.PurchaseOrderListviewFilterSelectionAction.Caption = "Filter";
            this.PurchaseOrderListviewFilterSelectionAction.ConfirmationMessage = null;
            this.PurchaseOrderListviewFilterSelectionAction.Id = "PurchaseOrderListviewFilterSelectionActionId";
            this.PurchaseOrderListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.PurchaseOrderListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseOrder);
            this.PurchaseOrderListviewFilterSelectionAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.PurchaseOrderListviewFilterSelectionAction.ToolTip = null;
            this.PurchaseOrderListviewFilterSelectionAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.PurchaseOrderListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.PurchaseOrderListviewFilterSelectionAction_Execute);
            // 
            // PurchaseOrderPurchaseReturnAction
            // 
            this.PurchaseOrderPurchaseReturnAction.Caption = "Purchase Return";
            this.PurchaseOrderPurchaseReturnAction.ConfirmationMessage = null;
            this.PurchaseOrderPurchaseReturnAction.Id = "PurchaseOrderPurchaseReturnActionId";
            this.PurchaseOrderPurchaseReturnAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseOrder);
            this.PurchaseOrderPurchaseReturnAction.ToolTip = null;
            this.PurchaseOrderPurchaseReturnAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseOrderPurchaseReturnAction_Execute);
            // 
            // PurchaseOrderListviewFilterApprovalSelectionAction
            // 
            this.PurchaseOrderListviewFilterApprovalSelectionAction.Caption = "Filter Approval";
            this.PurchaseOrderListviewFilterApprovalSelectionAction.ConfirmationMessage = null;
            this.PurchaseOrderListviewFilterApprovalSelectionAction.Id = "PurchaseOrderListviewFilterApprovalSelectionActionId";
            this.PurchaseOrderListviewFilterApprovalSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.PurchaseOrderListviewFilterApprovalSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseOrder);
            this.PurchaseOrderListviewFilterApprovalSelectionAction.ToolTip = null;
            this.PurchaseOrderListviewFilterApprovalSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.PurchaseOrderListviewFilterApprovalSelectionAction_Execute);
            // 
            // PurchaseOrderActionController
            // 
            this.Actions.Add(this.PurchaseOrderProgressAction);
            this.Actions.Add(this.PurchaseOrderPostingAction);
            this.Actions.Add(this.PurchaseOrderGetPRAction);
            this.Actions.Add(this.PurchaseOrderListviewFilterSelectionAction);
            this.Actions.Add(this.PurchaseOrderPurchaseReturnAction);
            this.Actions.Add(this.PurchaseOrderListviewFilterApprovalSelectionAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseOrderProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseOrderPostingAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseOrderGetPRAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction PurchaseOrderListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseOrderPurchaseReturnAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction PurchaseOrderListviewFilterApprovalSelectionAction;
    }
}
