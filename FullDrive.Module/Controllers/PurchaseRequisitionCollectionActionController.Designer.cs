﻿namespace FullDrive.Module.Controllers
{
    partial class PurchaseRequisitionCollectionActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PurchaseRequisitionCollectionShowPRLActionController = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // PurchaseRequisitionCollectionShowPRLActionController
            // 
            this.PurchaseRequisitionCollectionShowPRLActionController.Caption = "Show PRL";
            this.PurchaseRequisitionCollectionShowPRLActionController.ConfirmationMessage = null;
            this.PurchaseRequisitionCollectionShowPRLActionController.Id = "PurchaseRequisitionCollectionShowPRLActionControllerId";
            this.PurchaseRequisitionCollectionShowPRLActionController.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseRequisitionCollection);
            this.PurchaseRequisitionCollectionShowPRLActionController.ToolTip = null;
            this.PurchaseRequisitionCollectionShowPRLActionController.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseRequisitionCollectionShowPRLActionController_Execute);
            // 
            // PurchaseRequisitionCollectionActionController
            // 
            this.Actions.Add(this.PurchaseRequisitionCollectionShowPRLActionController);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseRequisitionCollectionShowPRLActionController;
    }
}
