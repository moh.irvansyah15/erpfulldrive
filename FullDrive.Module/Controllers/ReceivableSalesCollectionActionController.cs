﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ReceivableSalesCollectionActionController : ViewController
    {
        public ReceivableSalesCollectionActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void ReceivableSalesCollectionShowSIAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
            var ListView = View as ListView;
            IObjectSpace os = Application.CreateObjectSpace();
            string listViewId = Application.FindListViewId(typeof(SalesInvoiceMonitoring));
            CollectionSourceBase cs = Application.CreateCollectionSource(os, typeof(SalesInvoiceMonitoring), listViewId);
            //Tinggal buat 
            ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
            string _stringSIM = null;
            if (_objectsToProcess != null)
            {
                foreach (Object obj in _objectsToProcess)
                {
                    ReceivableSalesCollection _locReceivableSalesCollection = (ReceivableSalesCollection)_objectSpace.GetObject(obj);
                    if (_locReceivableSalesCollection != null)
                    {
                        if (_locReceivableSalesCollection.SalesInvoice != null)
                        {
                            if (_stringSIM == null)
                            {
                                if (_locReceivableSalesCollection.SalesInvoice.Code != null)
                                {
                                    _stringSIM = "( [SalesInvoice.Code]=='" + _locReceivableSalesCollection.SalesInvoice.Code + "' AND [Status] != 4 )";
                                }

                            }
                            else
                            {
                                if (_locReceivableSalesCollection.SalesInvoice.Code != null)
                                {
                                    _stringSIM = _stringSIM + " OR ( [SalesInvoice.Code]=='" + _locReceivableSalesCollection.SalesInvoice.Code + "' AND [Status] != 4 )";
                                }
                            }
                        }
                    }
                }
            }
            if (_stringSIM != null)
            {
                cs.Criteria["ReceivableSalesCollectionFilterSIM"] = CriteriaOperator.Parse(_stringSIM);
            }
            e.ShowViewParameters.CreatedView = Application.CreateListView(
                listViewId,
                cs,
                true
            );
            e.ShowViewParameters.Context = TemplateContext.View;
            e.ShowViewParameters.TargetWindow = TargetWindow.NewModalWindow;
            DialogController dc = Application.CreateController<DialogController>();
            dc.Accepting += new EventHandler<DialogControllerAcceptingEventArgs>(dc_Accepting);
            e.ShowViewParameters.Controllers.Add(dc);
        }

        void dc_Accepting(object sender, DialogControllerAcceptingEventArgs e)
        {
            try
            {
                ReceivableTransaction _locReceivableTransaction = null;
                View popupView = ((Controller)sender).Frame.View;
                ArrayList _objectsToProcess = new ArrayList(View.SelectedObjects);
                if (_objectsToProcess != null)
                {
                    foreach (Object _obj in _objectsToProcess)
                    {
                        ReceivableSalesCollection _locReceiSalesCollection = (ReceivableSalesCollection)popupView.ObjectSpace.GetObject(_obj);
                        if (_locReceiSalesCollection != null)
                        {
                            if (_locReceiSalesCollection.ReceivableTransaction != null)
                            {
                                _locReceivableTransaction = _locReceiSalesCollection.ReceivableTransaction;
                            }
                        }
                    }
                }
                if (_locReceivableTransaction != null)
                {
                    ArrayList _objectsToProcess2 = new ArrayList(popupView.SelectedObjects);
                    if (_objectsToProcess2 != null)
                    {
                        foreach (Object _obj2 in _objectsToProcess2)
                        {
                            SalesInvoiceMonitoring _locSalesInvMonitoring = (SalesInvoiceMonitoring)popupView.ObjectSpace.GetObject(_obj2);
                            if (_locSalesInvMonitoring != null && _locSalesInvMonitoring.Select == true && _locSalesInvMonitoring.Status != Status.Close)
                            {
                                _locSalesInvMonitoring.ReceivableTransaction = _locReceivableTransaction;
                            }
                        }
                    }
                }
                popupView.ObjectSpace.CommitChanges();
            }
            catch (ValidationException)
            {
            }
        }

        private void ReceivableSalesCollectionShowSPIAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
            var ListView = View as ListView;
            IObjectSpace os = Application.CreateObjectSpace();
            string listViewId = Application.FindListViewId(typeof(SalesPrePaymentInvoice));
            CollectionSourceBase cs = Application.CreateCollectionSource(os, typeof(SalesPrePaymentInvoice), listViewId);
            //Tinggal buat 
            ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
            string _stringSPI = null;
            if (_objectsToProcess != null)
            {
                foreach (Object obj in _objectsToProcess)
                {
                    ReceivableSalesCollection _locReceivableSalesCollection = (ReceivableSalesCollection)_objectSpace.GetObject(obj);
                    if (_locReceivableSalesCollection != null)
                    {
                        if (_locReceivableSalesCollection.SalesPrePaymentInvoice != null)
                        {
                            if (_stringSPI == null)
                            {
                                if (_locReceivableSalesCollection.SalesPrePaymentInvoice.Code != null)
                                {
                                    _stringSPI = "( [Code]=='" + _locReceivableSalesCollection.SalesPrePaymentInvoice.Code + "' AND [Status] != 4 )";
                                }

                            }
                            else
                            {
                                if (_locReceivableSalesCollection.SalesPrePaymentInvoice.Code != null)
                                {
                                    _stringSPI = _stringSPI + " OR ( [Code]=='" + _locReceivableSalesCollection.SalesPrePaymentInvoice.Code + "' AND [Status] != 4 )";
                                }
                            }
                        }
                    }
                }
            }
            if (_stringSPI != null)
            {
                cs.Criteria["ReceivableSalesCollectionFilterSPI"] = CriteriaOperator.Parse(_stringSPI);
            }
            e.ShowViewParameters.CreatedView = Application.CreateListView(
                listViewId,
                cs,
                true
            );
            e.ShowViewParameters.Context = TemplateContext.View;
            e.ShowViewParameters.TargetWindow = TargetWindow.NewModalWindow;
            DialogController dc = Application.CreateController<DialogController>();
            dc.Accepting += new EventHandler<DialogControllerAcceptingEventArgs>(dc_Accepting2);
            e.ShowViewParameters.Controllers.Add(dc);
        }

        void dc_Accepting2(object sender, DialogControllerAcceptingEventArgs e)
        {
            try
            {
                ReceivableTransaction _locReceivableTransaction = null;
                View popupView = ((Controller)sender).Frame.View;
                ArrayList _objectsToProcess = new ArrayList(View.SelectedObjects);
                if (_objectsToProcess != null)
                {
                    foreach (Object _obj in _objectsToProcess)
                    {
                        ReceivableSalesCollection _locReceiSalesCollection = (ReceivableSalesCollection)popupView.ObjectSpace.GetObject(_obj);
                        if (_locReceiSalesCollection != null)
                        {
                            if (_locReceiSalesCollection.ReceivableTransaction != null)
                            {
                                _locReceivableTransaction = _locReceiSalesCollection.ReceivableTransaction;
                            }
                        }
                    }
                }
                if (_locReceivableTransaction != null)
                {
                    ArrayList _objectsToProcess2 = new ArrayList(popupView.SelectedObjects);
                    if (_objectsToProcess2 != null)
                    {
                        foreach (Object _obj2 in _objectsToProcess2)
                        {
                            SalesPrePaymentInvoice _locSalesPrePayment = (SalesPrePaymentInvoice)popupView.ObjectSpace.GetObject(_obj2);
                            if (_locSalesPrePayment != null && _locSalesPrePayment.Select == true && _locSalesPrePayment.Status != Status.Close)
                            {
                                _locSalesPrePayment.ReceivableTransaction = _locReceivableTransaction;
                            }
                        }
                    }
                }
                popupView.ObjectSpace.CommitChanges();
            }
            catch (ValidationException)
            {
            }
        }
    }
}
