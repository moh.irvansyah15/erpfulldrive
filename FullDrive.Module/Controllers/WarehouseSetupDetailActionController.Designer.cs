﻿namespace FullDrive.Module.Controllers
{
    partial class WarehouseSetupDetailActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.WarehouseSetupDetailGetPreviousAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // WarehouseSetupDetailGetPreviousAction
            // 
            this.WarehouseSetupDetailGetPreviousAction.Caption = "Get Previous Data";
            this.WarehouseSetupDetailGetPreviousAction.ConfirmationMessage = null;
            this.WarehouseSetupDetailGetPreviousAction.Id = "WarehouseSetupDetailGetPreviousActionId";
            this.WarehouseSetupDetailGetPreviousAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.WarehouseSetupDetail);
            this.WarehouseSetupDetailGetPreviousAction.ToolTip = null;
            this.WarehouseSetupDetailGetPreviousAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.WarehouseSetupDetailGetPreviousAction_Execute);
            // 
            // WarehouseSetupDetailActionController
            // 
            this.Actions.Add(this.WarehouseSetupDetailGetPreviousAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction WarehouseSetupDetailGetPreviousAction;
    }
}
