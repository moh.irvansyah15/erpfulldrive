﻿namespace FullDrive.Module.Controllers
{
    partial class ReceivableTransactionActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ReceivableTransactionProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ReceivableTransactionPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ReceivableTransactionListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.ReceivableTransactionGetSIAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ReceivableTransactionGetSPIAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // ReceivableTransactionProgressAction
            // 
            this.ReceivableTransactionProgressAction.Caption = "Progress";
            this.ReceivableTransactionProgressAction.ConfirmationMessage = null;
            this.ReceivableTransactionProgressAction.Id = "ReceivableTransactionProgressActionId";
            this.ReceivableTransactionProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ReceivableTransaction);
            this.ReceivableTransactionProgressAction.ToolTip = null;
            this.ReceivableTransactionProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ReceivableTransactionProgressAction_Execute);
            // 
            // ReceivableTransactionPostingAction
            // 
            this.ReceivableTransactionPostingAction.Caption = "Posting";
            this.ReceivableTransactionPostingAction.ConfirmationMessage = null;
            this.ReceivableTransactionPostingAction.Id = "ReceivableTransactionPostingActionId";
            this.ReceivableTransactionPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ReceivableTransaction);
            this.ReceivableTransactionPostingAction.ToolTip = null;
            this.ReceivableTransactionPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ReceivableTransactionPostingAction_Execute);
            // 
            // ReceivableTransactionListviewFilterSelectionAction
            // 
            this.ReceivableTransactionListviewFilterSelectionAction.Caption = "Filter";
            this.ReceivableTransactionListviewFilterSelectionAction.ConfirmationMessage = null;
            this.ReceivableTransactionListviewFilterSelectionAction.Id = "ReceivableTransactionListviewFilterSelectionActionId";
            this.ReceivableTransactionListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.ReceivableTransactionListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ReceivableTransaction);
            this.ReceivableTransactionListviewFilterSelectionAction.ToolTip = null;
            this.ReceivableTransactionListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.ReceivableTransactionListviewFilterSelectionAction_Execute);
            // 
            // ReceivableTransactionGetSIAction
            // 
            this.ReceivableTransactionGetSIAction.Caption = "Get SI";
            this.ReceivableTransactionGetSIAction.ConfirmationMessage = null;
            this.ReceivableTransactionGetSIAction.Id = "ReceivableTransactionGetSIActionId";
            this.ReceivableTransactionGetSIAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ReceivableTransaction);
            this.ReceivableTransactionGetSIAction.ToolTip = null;
            this.ReceivableTransactionGetSIAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ReceivableTransactionGetSIAction_Execute);
            // 
            // ReceivableTransactionGetSPIAction
            // 
            this.ReceivableTransactionGetSPIAction.Caption = "Get SPI";
            this.ReceivableTransactionGetSPIAction.ConfirmationMessage = null;
            this.ReceivableTransactionGetSPIAction.Id = "ReceivableTransactionGetSPIActionId";
            this.ReceivableTransactionGetSPIAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ReceivableTransaction);
            this.ReceivableTransactionGetSPIAction.ToolTip = null;
            this.ReceivableTransactionGetSPIAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ReceivableTransactionGetSPIAction_Execute);
            // 
            // ReceivableTransactionActionController
            // 
            this.Actions.Add(this.ReceivableTransactionProgressAction);
            this.Actions.Add(this.ReceivableTransactionPostingAction);
            this.Actions.Add(this.ReceivableTransactionListviewFilterSelectionAction);
            this.Actions.Add(this.ReceivableTransactionGetSIAction);
            this.Actions.Add(this.ReceivableTransactionGetSPIAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction ReceivableTransactionProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction ReceivableTransactionPostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction ReceivableTransactionListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SimpleAction ReceivableTransactionGetSIAction;
        private DevExpress.ExpressApp.Actions.SimpleAction ReceivableTransactionGetSPIAction;
    }
}
