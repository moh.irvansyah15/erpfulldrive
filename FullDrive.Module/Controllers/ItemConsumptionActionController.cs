﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ItemConsumptionActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;
        public ItemConsumptionActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.

            #region FilterStatus
            ItemConsumptionListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                ItemConsumptionListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterStatus

            #region FilterApproval
            ItemConsumptionListviewFilterApprovalSelectionAction.Items.Clear();
            foreach (object _currApprovalFilter in Enum.GetValues(typeof(CustomProcess.ApprovalFilter)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalFilter));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApprovalFilter), _currApprovalFilter);
                ItemConsumptionListviewFilterApprovalSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterApproval
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void ItemConsumptionProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        ItemConsumption _locItemConsumptionOS = (ItemConsumption)_objectSpace.GetObject(obj);

                        if (_locItemConsumptionOS != null)
                        {
                            if (_locItemConsumptionOS.Code != null)
                            {
                                _currObjectId = _locItemConsumptionOS.Code;

                                ItemConsumption _locItemConsumptionXPO = _currSession.FindObject<ItemConsumption>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locItemConsumptionXPO != null)
                                {
                                    if (_locItemConsumptionXPO.Status == Status.Open)
                                    {
                                        _locItemConsumptionXPO.Status = Status.Progress;
                                        _locItemConsumptionXPO.StatusDate = now;
                                        _locItemConsumptionXPO.Save();
                                        _locItemConsumptionXPO.Session.CommitTransaction();

                                        XPCollection<ItemConsumptionLine> _locItemConsumptionLines = new XPCollection<ItemConsumptionLine>
                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("ItemConsumption", _locItemConsumptionXPO)));

                                        if (_locItemConsumptionLines != null && _locItemConsumptionLines.Count > 0)
                                        {
                                            foreach (ItemConsumptionLine _locItemConsumptionLine in _locItemConsumptionLines)
                                            {
                                                if (_locItemConsumptionLine.Status == Status.Open)
                                                {
                                                    _locItemConsumptionLine.Status = Status.Progress;
                                                    _locItemConsumptionLine.StatusDate = now;
                                                    _locItemConsumptionLine.Save();
                                                    _locItemConsumptionLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        SuccessMessageShow(_locItemConsumptionXPO.Code + " has been change successfully to Progress");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Item Consumption Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data Item Consumption Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ItemConsumption " + ex.ToString());
            }
        }

        private void ItemConsumptionPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        ItemConsumption _locItemConsumptionOS = (ItemConsumption)_objectSpace.GetObject(obj);

                        if (_locItemConsumptionOS != null)
                        {
                            if (_locItemConsumptionOS.Code != null)
                            {
                                _currObjectId = _locItemConsumptionOS.Code;

                                ItemConsumption _locItemConsumptionXPO = _currSession.FindObject<ItemConsumption>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locItemConsumptionXPO != null)
                                {
                                    //Get Approval dulu
                                    if (_locItemConsumptionXPO.Status == Status.Progress || _locItemConsumptionXPO.Status == Status.Posted)
                                    {
                                        ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("EndApproval", true),
                                                                     new BinaryOperator("ItemConsumption", _locItemConsumptionXPO)));

                                        if (_locApprovalLineXPO != null)
                                        {
                                            SetBeginingInventory(_currSession, _locItemConsumptionXPO);
                                            SetInventoryJournal(_currSession, _locItemConsumptionXPO);
                                            SetStatusItemConsumptionLine(_currSession, _locItemConsumptionXPO);
                                            SetStatusItemConsumption(_currSession, _locItemConsumptionXPO);
                                        }
                                    }

                                    SuccessMessageShow("Item Consumption has been successfully post");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Item Consumption Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Item Consumption Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ItemConsumption " + ex.ToString());
            }
        }

        private void ItemConsumptionListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(ItemConsumption)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ItemConsumption " + ex.ToString());
            }
        }

        private void ItemConsumptionListviewFilterApprovalSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(ItemConsumption)))
                {
                    if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.Approval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal)));
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.EndApproval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal);
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.AllData)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved1", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal));
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ItemConsumption " + ex.ToString());
            }
        }

        //==== Code Only ====

        #region ItemConsumption Method

        private void SetBeginingInventory(Session _currSession, ItemConsumption _itemConsumption)
        {
            try
            {
                XPCollection<ItemConsumptionLine> _locItmConsumpLines = new XPCollection<ItemConsumptionLine>(_currSession,
                                                                        new BinaryOperator("ItemConsumption", _itemConsumption));

                if (_locItmConsumpLines != null && _locItmConsumpLines.Count() > 0)
                {
                    GlobalFunction _globFunc = new GlobalFunction();
                    double _locItmConLineTotal = 0;
                    XPCollection<BeginingInventoryLine> _locBegInventoryLines = null;
                    ItemUnitOfMeasure _locItemUOM = null;
                    string _fullString = null;
                    string _locItemParse = null;
                    string _locLocationParse = null;
                    string _locBinLocationParse = null;
                    string _locDUOMParse = null;
                    string _locStockTypeParse = null;
                    string _locProjectHeader = null;
                    string _locActiveParse = null;
                    string _locSignCode = null;

                    foreach (ItemConsumptionLine _locItmConsumpLine in _locItmConsumpLines)
                    {
                        if (_locItmConsumpLine.Status == Status.Progress || _locItmConsumpLine.Status == Status.Posted)
                        {
                            if (_locItmConsumpLine.DQty > 0 || _locItmConsumpLine.Qty > 0)
                            {
                                if (_locItmConsumpLine.Item != null && _locItmConsumpLine.UOM != null && _locItmConsumpLine.DUOM != null)
                                {
                                    //for conversion
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", _locItmConsumpLine.Item),
                                                         new BinaryOperator("UOM", _locItmConsumpLine.UOM),
                                                         new BinaryOperator("DefaultUOM", _locItmConsumpLine.DUOM),
                                                         new BinaryOperator("Active", true)));
                                }

                                //for query
                                BeginingInventory _locBeginingInventory = _currSession.FindObject<BeginingInventory>(new GroupOperator(GroupOperatorType.And,
                                                                          new BinaryOperator("Item", _locItmConsumpLine.Item)));

                                #region UpdateBeginingInventory
                                if (_locBeginingInventory != null)
                                {
                                    #region code
                                    if (_locItmConsumpLine.Item != null) { _locItemParse = "[Item.Code]=='" + _locItmConsumpLine.Item.Code + "'"; }
                                    else { _locItemParse = ""; }

                                    if (_locItmConsumpLine.Location != null && (_locItmConsumpLine.Item != null))
                                    { _locLocationParse = " AND [Location.Code]=='" + _locItmConsumpLine.Location.Code + "'"; }
                                    else if (_locItmConsumpLine.Location != null && _locItmConsumpLine.Item == null)
                                    { _locLocationParse = " [Location.Code]=='" + _locItmConsumpLine.Location.Code + "'"; }
                                    else { _locLocationParse = ""; }

                                    if (_locItmConsumpLine.BinLocation != null && (_locItmConsumpLine.Item != null || _locItmConsumpLine.Location != null))
                                    { _locBinLocationParse = " AND [BinLocation.Code]=='" + _locItmConsumpLine.BinLocation.Code + "'"; }
                                    else if (_locItmConsumpLine.BinLocation != null && _locItmConsumpLine.Item == null && _locItmConsumpLine.Location == null)
                                    { _locBinLocationParse = " [BinLocation.Code]=='" + _locItmConsumpLine.BinLocation.Code + "'"; }
                                    else { _locBinLocationParse = ""; }

                                    if (_locItmConsumpLine.DUOM != null && (_locItmConsumpLine.Item != null || _locItmConsumpLine.Location != null || _locItmConsumpLine.BinLocation != null))
                                    { _locDUOMParse = " AND [DefaultUOM.Code]=='" + _locItmConsumpLine.DUOM.Code + "'"; }
                                    else if (_locItmConsumpLine.DUOM != null && _locItmConsumpLine.Item == null && _locItmConsumpLine.Location == null && _locItmConsumpLine.BinLocation == null)
                                    { _locDUOMParse = " [DefaultUOM.Code]=='" + _locItmConsumpLine.DUOM.Code + "'"; }
                                    else { _locDUOMParse = ""; }

                                    if (_locItmConsumpLine.StockType != StockType.None && (_locItmConsumpLine.Item != null || _locItmConsumpLine.Location != null || _locItmConsumpLine.BinLocation != null
                                        || _locItmConsumpLine.DUOM != null))
                                    { _locStockTypeParse = " AND [StockType]=='" + GetStockType(_locItmConsumpLine.StockType).ToString() + "'"; }
                                    else if (_locItmConsumpLine.StockType != StockType.None && _locItmConsumpLine.Item == null && _locItmConsumpLine.Location == null && _locItmConsumpLine.BinLocation == null
                                        && _locItmConsumpLine.DUOM == null)
                                    { _locStockTypeParse = " [StockType]=='" + GetStockType(_locItmConsumpLine.StockType).ToString() + "'"; }
                                    else { _locStockTypeParse = ""; }

                                    if (_locItmConsumpLine.Item == null && _locItmConsumpLine.Location == null && _locItmConsumpLine.BinLocation == null
                                        && _locItmConsumpLine.DUOM == null && _locItmConsumpLine.StockType != StockType.None)
                                    { _locActiveParse = " [Active]=='" + GetActive(true).ToString() + "'"; }
                                    else { _locActiveParse = " AND [Active]=='" + GetActive(true).ToString() + "'"; }

                                    if (_itemConsumption.ProjectHeader != null)
                                    { _locProjectHeader = " AND [ProjectHeader.Code]=='" + _itemConsumption.ProjectHeader.Code + "'"; }
                                    else
                                    { _locProjectHeader = ""; }

                                    if (_locItemParse != null || _locLocationParse != null || _locBinLocationParse != null || _locDUOMParse != null || _locStockTypeParse != null || _locProjectHeader != null)
                                    {
                                        _fullString = _locItemParse + _locLocationParse + _locBinLocationParse + _locDUOMParse + _locStockTypeParse + _locActiveParse + _locProjectHeader;
                                    }
                                    else
                                    {
                                        _fullString = _locActiveParse;
                                    }
                                    #endregion code

                                    _locBegInventoryLines = new XPCollection<BeginingInventoryLine>(_currSession, CriteriaOperator.Parse(_fullString));

                                    #region UpdateBeginingInventoryLine
                                    if (_locBegInventoryLines != null && _locBegInventoryLines.Count() > 0)
                                    {

                                        foreach (BeginingInventoryLine _locBegInventoryLine in _locBegInventoryLines)
                                        {
                                            if (_locItemUOM != null)
                                            {
                                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                {
                                                    _locItmConLineTotal = _locItmConsumpLine.Qty * _locItemUOM.DefaultConversion + _locItmConsumpLine.DQty;
                                                }
                                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                {
                                                    _locItmConLineTotal = _locItmConsumpLine.Qty / _locItemUOM.Conversion + _locItmConsumpLine.DQty;
                                                }
                                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                {
                                                    _locItmConLineTotal = _locItmConsumpLine.Qty + _locItmConsumpLine.DQty;
                                                }
                                            }

                                            else
                                            {
                                                _locItmConLineTotal = _locItmConsumpLine.Qty + _locItmConsumpLine.DQty;
                                            }

                                            _locBegInventoryLine.QtySale = _locItmConLineTotal - _locBegInventoryLine.QtySale;
                                            _locBegInventoryLine.Save();
                                            _locBegInventoryLine.Session.CommitTransaction();

                                            _locBeginingInventory.QtySale = _locItmConLineTotal - _locBeginingInventory.QtySale;
                                            _locBeginingInventory.Save();
                                            _locBeginingInventory.Session.CommitTransaction();
                                        }
                                    }
                                    #endregion UpdateBeginingInventoryLine

                                    #region NewBeginingInventoryLine
                                    else
                                    {
                                        if (_locItemUOM != null)
                                        {
                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                            {
                                                _locItmConLineTotal = _locItmConsumpLine.Qty * _locItemUOM.DefaultConversion + _locItmConsumpLine.DQty;
                                            }
                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                            {
                                                _locItmConLineTotal = _locItmConsumpLine.Qty / _locItemUOM.Conversion + _locItmConsumpLine.DQty;
                                            }
                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                            {
                                                _locItmConLineTotal = _locItmConsumpLine.Qty + _locItmConsumpLine.DQty;
                                            }
                                        }
                                        else
                                        {
                                            _locItmConLineTotal = _locItmConsumpLine.Qty + _locItmConsumpLine.DQty;
                                        }

                                        BeginingInventoryLine _locSaveDataBeginingInventory = new BeginingInventoryLine(_currSession)
                                        {
                                            Item = _locItmConsumpLine.Item,
                                            Location = _locItmConsumpLine.Location,
                                            BinLocation = _locItmConsumpLine.BinLocation,
                                            QtySale = _locItmConLineTotal,
                                            Company = _locItmConsumpLine.Company,
                                            DefaultUOM = _locItmConsumpLine.DUOM,
                                            StockType = _locItmConsumpLine.StockType,
                                            Active = true,
                                            BeginingInventory = _locBeginingInventory,
                                        };
                                        _locSaveDataBeginingInventory.Save();
                                        _locSaveDataBeginingInventory.Session.CommitTransaction();
                                    }
                                    #endregion NewBeginingInventoryLine
                                }
                                #endregion UpdateBeginingInventory

                                #region CreateNewBeginingInventory
                                else
                                {
                                    _locSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.BeginingInventory);

                                    if (_locSignCode != null)
                                    {
                                        if (_locItemUOM != null)
                                        {
                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                            {
                                                _locItmConLineTotal = _locItmConsumpLine.Qty * _locItemUOM.DefaultConversion + _locItmConsumpLine.DQty;
                                            }
                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                            {
                                                _locItmConLineTotal = _locItmConsumpLine.Qty / _locItemUOM.Conversion + _locItmConsumpLine.DQty;
                                            }
                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                            {
                                                _locItmConLineTotal = _locItmConsumpLine.Qty + _locItmConsumpLine.DQty;
                                            }
                                        }

                                        else
                                        {
                                            _locItmConLineTotal = _locItmConsumpLine.Qty + _locItmConsumpLine.DQty;
                                        }

                                        BeginingInventory _saveDataBegInv = new BeginingInventory(_currSession)
                                        {
                                            Item = _locItmConsumpLine.Item,
                                            QtySale = _locItmConLineTotal,
                                            DefaultUOM = _locItmConsumpLine.DUOM,
                                            Active = true,
                                            SignCode = _locSignCode,
                                        };
                                        _saveDataBegInv.Save();
                                        _saveDataBegInv.Session.CommitTransaction();

                                        BeginingInventory _locBeginingInventory2 = _currSession.FindObject<BeginingInventory>(new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Item", _locItmConsumpLine.Item),
                                                                    new BinaryOperator("SignCode", _locSignCode)));

                                        if (_locBeginingInventory2 != null)
                                        {
                                            BeginingInventoryLine _saveDataBegInvLine = new BeginingInventoryLine(_currSession)
                                            {
                                                Item = _locItmConsumpLine.Item,
                                                Location = _locItmConsumpLine.Location,
                                                BinLocation = _locItmConsumpLine.BinLocation,
                                                QtySale = _locBeginingInventory2.QtySale,
                                                DefaultUOM = _locBeginingInventory2.DefaultUOM,
                                                StockType = _locItmConsumpLine.StockType,
                                                ProjectHeader = _itemConsumption.ProjectHeader,
                                                Active = true,
                                                BeginingInventory = _locBeginingInventory2,
                                            };
                                            _saveDataBegInvLine.Save();
                                            _saveDataBegInvLine.Session.CommitTransaction();
                                        }
                                    }
                                }
                                #endregion CreateNewBeginingInventory
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ItemConsumption ", ex.ToString());
            }
        }

        private void SetInventoryJournal(Session _currSession, ItemConsumption _itemConsumption)
        {
            try
            {
                XPCollection<ItemConsumptionLine> _locItmConsLines = new XPCollection<ItemConsumptionLine>(_currSession,
                                                                        new BinaryOperator("ItemConsumption", _itemConsumption));

                if (_locItmConsLines != null && _locItmConsLines.Count > 0)
                {
                    double _locItmConLineTotal = 0;
                    DateTime now = DateTime.Now;

                    foreach (ItemConsumptionLine _locInvTransLine in _locItmConsLines)
                    {
                        if (_locInvTransLine.Status == Status.Progress || _locInvTransLine.Status == Status.Posted)
                        {
                            if (_locInvTransLine.DQty > 0 || _locInvTransLine.Qty > 0)
                            {
                                if (_locInvTransLine.Item != null && _locInvTransLine.UOM != null && _locInvTransLine.DUOM != null)
                                {
                                    ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("Item", _locInvTransLine.Item),
                                                         new BinaryOperator("UOM", _locInvTransLine.UOM),
                                                         new BinaryOperator("DefaultUOM", _locInvTransLine.DUOM),
                                                         new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        #region Code
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locItmConLineTotal = _locInvTransLine.Qty * _locItemUOM.DefaultConversion + _locInvTransLine.DQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locItmConLineTotal = _locInvTransLine.Qty / _locItemUOM.Conversion + _locInvTransLine.DQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locItmConLineTotal = _locInvTransLine.Qty + _locInvTransLine.DQty;
                                        }
                                        #endregion Code

                                        InventoryJournal _locNegatifInventoryJournal = new InventoryJournal(_currSession)
                                        {
                                            DocumentType = _itemConsumption.DocumentType,
                                            DocNo = _itemConsumption.DocNo,
                                            Location = _locInvTransLine.Location,
                                            BinLocation = _locInvTransLine.BinLocation,
                                            Item = _locInvTransLine.Item,
                                            Company = _locInvTransLine.Company,
                                            QtyPos = 0,
                                            QtyNeg = _locItmConLineTotal,
                                            DUOM = _locInvTransLine.DUOM,
                                            JournalDate = now,
                                            ProjectHeader = _itemConsumption.ProjectHeader
                                        };
                                        _locNegatifInventoryJournal.Save();
                                        _locNegatifInventoryJournal.Session.CommitTransaction();

                                    }
                                }
                                else
                                {
                                    _locItmConLineTotal = _locInvTransLine.Qty + _locInvTransLine.DQty;

                                    InventoryJournal _locNegatifInventoryJournal = new InventoryJournal(_currSession)
                                    {
                                        DocumentType = _itemConsumption.DocumentType,
                                        DocNo = _itemConsumption.DocNo,
                                        Location = _locInvTransLine.Location,
                                        BinLocation = _locInvTransLine.BinLocation,
                                        Item = _locInvTransLine.Item,
                                        Company = _locInvTransLine.Company,
                                        QtyPos = 0,
                                        QtyNeg = _locItmConLineTotal,
                                        DUOM = _locInvTransLine.DUOM,
                                        JournalDate = now,
                                        ProjectHeader = _itemConsumption.ProjectHeader
                                    };
                                    _locNegatifInventoryJournal.Save();
                                    _locNegatifInventoryJournal.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = ItemConsumption ", ex.ToString());
            }
        }

        private void SetStatusItemConsumptionLine(Session _currSession, ItemConsumption _itemConsumption)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_itemConsumption != null)
                {
                    XPCollection<ItemConsumptionLine> _locItemConsumptionLines = new XPCollection<ItemConsumptionLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("ItemConsumption", _itemConsumption)));

                    if (_locItemConsumptionLines != null && _locItemConsumptionLines.Count > 0)
                    {

                        foreach (ItemConsumptionLine _locItemConsumptionLine in _locItemConsumptionLines)
                        {
                            if (_locItemConsumptionLine.Status == Status.Progress || _locItemConsumptionLine.Status == Status.Posted)
                            {
                                if (_locItemConsumptionLine.DQty > 0 || _locItemConsumptionLine.Qty > 0)
                                {
                                    if (_locItemConsumptionLine.RmDQty == 0 && _locItemConsumptionLine.RmQty == 0 && _locItemConsumptionLine.RmTQty == 0)
                                    {
                                        _locItemConsumptionLine.Status = Status.Close;
                                        _locItemConsumptionLine.ActivationPosting = true;
                                        _locItemConsumptionLine.StatusDate = now;
                                    }
                                    else
                                    {
                                        _locItemConsumptionLine.Status = Status.Posted;
                                        _locItemConsumptionLine.StatusDate = now;
                                    }
                                    _locItemConsumptionLine.Save();
                                    _locItemConsumptionLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ItemConsumption " + ex.ToString());
            }
        }

        private void SetStatusItemConsumption(Session _currSession, ItemConsumption _itemConsumption)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locCount = 0;

                if (_itemConsumption != null)
                {
                    XPCollection<ItemConsumptionLine> _locItemConsumptionLines = new XPCollection<ItemConsumptionLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("ItemConsumption", _itemConsumption)));

                    if (_locItemConsumptionLines != null && _locItemConsumptionLines.Count() > 0)
                    {
                        foreach (ItemConsumptionLine _locItemConsumptionLine in _locItemConsumptionLines)
                        {
                            if (_locItemConsumptionLine.Status == Status.Close)
                            {
                                _locCount = _locCount + 1;
                            }
                        }

                        if (_locCount == _locItemConsumptionLines.Count())
                        {
                            _itemConsumption.Status = Status.Close;
                            _itemConsumption.StatusDate = now;
                            _itemConsumption.Save();
                            _itemConsumption.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = ItemConsumption " + ex.ToString());
            }
        }

        #endregion ItemConsumption Method

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private int GetStockType(StockType objectName)
        {
            int _result = 0;
            try
            {
                if (objectName == StockType.Good)
                {
                    _result = 1;
                }
                else if (objectName == StockType.Bad)
                {
                    _result = 2;
                }
                else
                {
                    _result = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = ItemConsumptionAction " + ex.ToString());
            }
            return _result;
        }

        private int GetActive(bool objectName)
        {
            int _result = 0;
            try
            {
                if (objectName == true)
                {
                    _result = 1;
                }
                else
                {
                    _result = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = ItemConsumptionAction " + ex.ToString());
            }
            return _result;
        }

        #endregion Global Method

    }
}
