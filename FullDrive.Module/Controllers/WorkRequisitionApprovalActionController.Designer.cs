﻿namespace FullDrive.Module.Controllers
{
    partial class WorkRequisitionApprovalActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.WorkRequisitionApprovalAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // WorkRequisitionApprovalAction
            // 
            this.WorkRequisitionApprovalAction.Caption = "Approval";
            this.WorkRequisitionApprovalAction.ConfirmationMessage = null;
            this.WorkRequisitionApprovalAction.Id = "WorkRequisitionApprovalActionId";
            this.WorkRequisitionApprovalAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.WorkRequisitionApprovalAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.WorkRequisition);
            this.WorkRequisitionApprovalAction.ToolTip = null;
            this.WorkRequisitionApprovalAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.WorkRequisitionApprovalAction_Execute);
            // 
            // WorkRequisitionApprovalActionController
            // 
            this.Actions.Add(this.WorkRequisitionApprovalAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction WorkRequisitionApprovalAction;
    }
}
