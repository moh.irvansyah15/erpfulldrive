﻿namespace FullDrive.Module.Controllers
{
    partial class InventoryTransferInLineActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.InventoryTransferInLineListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // InventoryTransferInLineListviewFilterSelectionAction
            // 
            this.InventoryTransferInLineListviewFilterSelectionAction.Caption = "Filter";
            this.InventoryTransferInLineListviewFilterSelectionAction.ConfirmationMessage = null;
            this.InventoryTransferInLineListviewFilterSelectionAction.Id = "InventoryTransferInLineListviewFilterSelectionActionId";
            this.InventoryTransferInLineListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.InventoryTransferInLineListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryTransferInLine);
            this.InventoryTransferInLineListviewFilterSelectionAction.ToolTip = null;
            this.InventoryTransferInLineListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.InventoryTransferInLineListviewFilterSelectionAction_Execute);
            // 
            // InventoryTransferInLineActionController
            // 
            this.Actions.Add(this.InventoryTransferInLineListviewFilterSelectionAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction InventoryTransferInLineListviewFilterSelectionAction;
    }
}
