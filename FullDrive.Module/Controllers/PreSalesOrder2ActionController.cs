﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using FullDrive.Module.BusinessObjects;
using DevExpress.ExpressApp.Xpo;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PreSalesOrder2ActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;

        public PreSalesOrder2ActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            PreSalesOrder2ListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                PreSalesOrder2ListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void PreSalesOrder2ProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PreSalesOrder2 _locPresalesOrder2OS = (PreSalesOrder2)_objectSpace.GetObject(obj);

                        if (_locPresalesOrder2OS != null)
                        {
                            _currObjectId = _locPresalesOrder2OS.Code;

                            PreSalesOrder2 _locPreSalesOrder2XPO = _currSession.FindObject<PreSalesOrder2>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _currObjectId)));

                            if (_locPreSalesOrder2XPO != null)
                            {
                                if (_locPreSalesOrder2XPO.Status == Status.Open)
                                {
                                    if (_locPreSalesOrder2XPO.ProjectHeader != null)
                                    {
                                        ProjectHeader _locProjectHeaderXPO = _currSession.FindObject<ProjectHeader>
                                                                             (new GroupOperator(GroupOperatorType.And,
                                                                              new BinaryOperator("Code", _locPreSalesOrder2XPO.ProjectHeader.Code)));

                                        if (_locProjectHeaderXPO != null)
                                        {
                                            XPCollection<PreSalesOrder2> _locPreSalesOrder2s = new XPCollection<PreSalesOrder2>(_currSession,
                                                                                               new GroupOperator(GroupOperatorType.And,
                                                                                               new BinaryOperator("ProjectHeader", _locProjectHeaderXPO),
                                                                                               new BinaryOperator("Status", Status.Open)));

                                            if (_locPreSalesOrder2s != null && _locPreSalesOrder2s.Count() > 0)
                                            {
                                                foreach (PreSalesOrder2 _locPreSalesOrder2 in _locPreSalesOrder2s)
                                                {
                                                    _locPreSalesOrder2.Status = Status.Progress;
                                                    _locPreSalesOrder2.StatusDate = now;
                                                    _locPreSalesOrder2.Save();
                                                    _locPreSalesOrder2.Session.CommitTransaction();
                                                }
                                            }
                                            SuccessMessageShow("Pre Sales Order2 has been successfully updated to progress");
                                        }
                                    }
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Pre Sales Order2 data not available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Pre Sales Order2 data not available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }

                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PreSalesOrder2" + ex.ToString());
            }
        }

        private void PreSalesOrder2PostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                string _currSignCode = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PreSalesOrder2 _locPreSalesOrder2OS = (PreSalesOrder2)_objectSpace.GetObject(obj);

                        if (_locPreSalesOrder2OS != null)
                        {
                            if (_locPreSalesOrder2OS.Code != null)
                            {
                                _currObjectId = _locPreSalesOrder2OS.Code;

                                PreSalesOrder2 _locPreSalesOrder2XPO = _currSession.FindObject<PreSalesOrder2>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));
                                #region PreSalesOrderXPO
                                if (_locPreSalesOrder2XPO != null)
                                {
                                    XPQuery<PreSalesOrder2> _preSalesOrder2sQuery = new XPQuery<PreSalesOrder2>(_currSession);

                                    var _preSalesOrder2s = from ppo in _preSalesOrder2sQuery
                                                           where (ppo.Choose == true && (ppo.Status == Status.Progress || ppo.Status == Status.Posted))
                                                           group ppo by ppo.ProjectHeader into g
                                                           select new { ProjectHeader = g.Key };

                                    if (_preSalesOrder2s != null && _preSalesOrder2s.Count() > 0)
                                    {
                                        foreach (var _preSalesOrder2 in _preSalesOrder2s)
                                        {
                                            _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.SalesOrder);
                                            if (_currSignCode != null)
                                            {
                                                SalesOrder _saveDataSo = new SalesOrder(_currSession)
                                                {
                                                    TransferType = DirectionType.External,
                                                    SignCode = _currSignCode,
                                                    ProjectHeader = _preSalesOrder2.ProjectHeader,
                                                };
                                                _saveDataSo.Save();
                                                _saveDataSo.Session.CommitTransaction();

                                                SalesOrder _locSalesOrder = _currSession.FindObject<SalesOrder>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("SignCode", _currSignCode)));

                                                XPCollection<PreSalesOrder2> _locPreSalesOrder2s = new XPCollection<PreSalesOrder2>(_currSession,
                                                                                                        new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("ProjectHeader", _preSalesOrder2.ProjectHeader),
                                                                                                        new BinaryOperator("Choose", true),
                                                                                                        new BinaryOperator("Status", Status.Progress)));
                                                if (_locSalesOrder != null)
                                                {
                                                    if (_locPreSalesOrder2s != null && _locPreSalesOrder2s.Count > 0)
                                                    {

                                                        foreach (PreSalesOrder2 _locPreSalesOrder2 in _locPreSalesOrder2s)
                                                        {
                                                            if (_locPreSalesOrder2.Status == Status.Progress)
                                                            {
                                                                SalesOrderLine _saveDataSol = new SalesOrderLine(_currSession)
                                                                {
                                                                    SalesType = OrderType.Service,
                                                                    Name = _locPreSalesOrder2.Name,
                                                                    MxDQty = _locPreSalesOrder2.Qty,
                                                                    MxDUOM = _locPreSalesOrder2.UOM,
                                                                    MxUAmount = _locPreSalesOrder2.UnitAmount,
                                                                    MxTUAmount = _locPreSalesOrder2.TotalUnitAmount,
                                                                    DQty = _locPreSalesOrder2.Qty,
                                                                    DUOM = _locPreSalesOrder2.UOM,
                                                                    TQty = _locPreSalesOrder2.Qty,
                                                                    SalesOrder = _locSalesOrder,
                                                                };
                                                                _saveDataSol.Save();
                                                                _saveDataSol.Session.CommitTransaction();
                                                            }

                                                            _locPreSalesOrder2.Status = Status.Lock;
                                                            _locPreSalesOrder2.StatusDate = now;
                                                            _locPreSalesOrder2.ActivationPosting = true;
                                                            _locPreSalesOrder2.PostedCount = _locPreSalesOrder2.PostedCount + 1;
                                                            _locPreSalesOrder2.Choose = false;
                                                            _locPreSalesOrder2.Qty = 0;
                                                            _locPreSalesOrder2.UnitAmount = 0;
                                                            _locPreSalesOrder2.TotalUnitAmount = 0;
                                                            _locPreSalesOrder2.Save();
                                                            _locPreSalesOrder2.Session.CommitTransaction();
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        SuccessMessageShow("PreSales Order Successfully Posted");
                                    }

                                }
                                #endregion PreSalesOrderXPO
                            }
                            ErrorMessageShow("Pre Sales Order 2 Data Not Available");
                        }
                        ErrorMessageShow("Pre Sales Order 2 Data Not Available");
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Controller = ProjectHeaderAction" + ex.ToString());
            }
        }

        private void PreSalesOrder2ListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(PreSalesOrder2)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PreSalesOrder2" + ex.ToString());
            }
        }

        #region Global

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global

    }
}
