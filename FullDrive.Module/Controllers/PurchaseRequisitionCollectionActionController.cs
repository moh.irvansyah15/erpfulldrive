﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PurchaseRequisitionCollectionActionController : ViewController
    {
        public PurchaseRequisitionCollectionActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void PurchaseRequisitionCollectionShowPRLActionController_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            
            IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
            var ListView = View as ListView;
            IObjectSpace os = Application.CreateObjectSpace();
            string listViewId = Application.FindListViewId(typeof(PurchaseRequisitionLine));
            CollectionSourceBase cs = Application.CreateCollectionSource(os, typeof(PurchaseRequisitionLine), listViewId);
            //Tinggal buat 
            ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
            string _stringPR = null;
            if (_objectsToProcess != null)
            {
                foreach (Object obj in _objectsToProcess)
                {
                    PurchaseRequisitionCollection _locPurchRequisitionCollection = (PurchaseRequisitionCollection)_objectSpace.GetObject(obj);
                    if (_locPurchRequisitionCollection != null)
                    {
                        if (_locPurchRequisitionCollection.PurchaseRequisition != null)
                        {
                            if (_stringPR == null)
                            {
                                if (_locPurchRequisitionCollection.PurchaseRequisition.Code != null)
                                {
                                    _stringPR = "[PurchaseRequisition.Code]=='" + _locPurchRequisitionCollection.PurchaseRequisition.Code + "'";
                                }

                            }
                            else
                            {
                                if (_locPurchRequisitionCollection.PurchaseRequisition.Code != null)
                                {
                                    _stringPR = _stringPR + " OR [PurchaseRequisition.Code]=='" + _locPurchRequisitionCollection.PurchaseRequisition.Code + "'";
                                }
                            }
                        }
                    }
                }
            }
            if (_stringPR != null)
            {
                cs.Criteria["PurchaseRequisitionCollectionFilter"] = CriteriaOperator.Parse(_stringPR);
            }
            e.ShowViewParameters.CreatedView = Application.CreateListView(
                listViewId,
                cs,
                true
            );
            e.ShowViewParameters.Context = TemplateContext.View;
            e.ShowViewParameters.TargetWindow = TargetWindow.NewModalWindow;
        }
    }
}
