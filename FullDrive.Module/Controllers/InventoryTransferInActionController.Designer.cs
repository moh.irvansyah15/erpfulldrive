﻿namespace FullDrive.Module.Controllers
{
    partial class InventoryTransferInActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.InventoryTransferInProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.InventoryTransferInPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.InventoryTransferInListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // InventoryTransferInProgressAction
            // 
            this.InventoryTransferInProgressAction.Caption = "Progress";
            this.InventoryTransferInProgressAction.ConfirmationMessage = null;
            this.InventoryTransferInProgressAction.Id = "InventoryTransferInProgressActionId";
            this.InventoryTransferInProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryTransferIn);
            this.InventoryTransferInProgressAction.ToolTip = null;
            this.InventoryTransferInProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InventoryTransferInProgressAction_Execute);
            // 
            // InventoryTransferInPostingAction
            // 
            this.InventoryTransferInPostingAction.Caption = "Posting";
            this.InventoryTransferInPostingAction.ConfirmationMessage = null;
            this.InventoryTransferInPostingAction.Id = "InventoryTransferInPostingActionId";
            this.InventoryTransferInPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryTransferIn);
            this.InventoryTransferInPostingAction.ToolTip = null;
            this.InventoryTransferInPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.InventoryTransferInPostingAction_Execute);
            // 
            // InventoryTransferInListviewFilterSelectionAction
            // 
            this.InventoryTransferInListviewFilterSelectionAction.Caption = "Filter";
            this.InventoryTransferInListviewFilterSelectionAction.ConfirmationMessage = null;
            this.InventoryTransferInListviewFilterSelectionAction.Id = "InventoryTransferInListviewFilterSelectionActionId";
            this.InventoryTransferInListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.InventoryTransferInListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.InventoryTransferIn);
            this.InventoryTransferInListviewFilterSelectionAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.InventoryTransferInListviewFilterSelectionAction.ToolTip = null;
            this.InventoryTransferInListviewFilterSelectionAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.InventoryTransferInListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.InventoryTransferInListviewFilterSelectionAction_Execute);
            // 
            // InventoryTransferInActionController
            // 
            this.Actions.Add(this.InventoryTransferInProgressAction);
            this.Actions.Add(this.InventoryTransferInPostingAction);
            this.Actions.Add(this.InventoryTransferInListviewFilterSelectionAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction InventoryTransferInProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction InventoryTransferInPostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction InventoryTransferInListviewFilterSelectionAction;
    }
}
