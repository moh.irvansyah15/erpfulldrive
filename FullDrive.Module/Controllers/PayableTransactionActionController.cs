﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PayableTransactionActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;

        public PayableTransactionActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            PayableTransactionListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                PayableTransactionListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void PayableTransactionProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PayableTransaction _locPayableTransactionOS = (PayableTransaction)_objectSpace.GetObject(obj);

                        if (_locPayableTransactionOS != null)
                        {
                            if (_locPayableTransactionOS.Code != null)
                            {
                                _currObjectId = _locPayableTransactionOS.Code;

                                PayableTransaction _locPayableTransactionXPO = _currSession.FindObject<PayableTransaction>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locPayableTransactionXPO != null)
                                {
                                    if (_locPayableTransactionXPO.Status == Status.Open)
                                    {
                                        _locPayableTransactionXPO.Status = Status.Progress;
                                        _locPayableTransactionXPO.StatusDate = now;
                                        _locPayableTransactionXPO.Save();
                                        _locPayableTransactionXPO.Session.CommitTransaction();

                                        XPCollection<PayableTransactionLine> _locPayableTransactionLines = new XPCollection<PayableTransactionLine>
                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("PayableTransaction", _locPayableTransactionXPO)));

                                        if (_locPayableTransactionLines != null && _locPayableTransactionLines.Count > 0)
                                        {
                                            foreach (PayableTransactionLine _locPayableTransactionLine in _locPayableTransactionLines)
                                            {
                                                if (_locPayableTransactionLine.Status == Status.Open)
                                                {
                                                    _locPayableTransactionLine.Status = Status.Progress;
                                                    _locPayableTransactionLine.StatusDate = now;
                                                    _locPayableTransactionLine.Save();
                                                    _locPayableTransactionLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        SuccessMessageShow(_locPayableTransactionXPO.Code + " has been change successfully to Progress");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Payable Transaction Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data Payable Transaction Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PayableTransaction " + ex.ToString());
            }
        }

        private void PayableTransactionPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PayableTransaction _locPayableTransactionOS = (PayableTransaction)_objectSpace.GetObject(obj);

                        if (_locPayableTransactionOS != null)
                        {
                            if (_locPayableTransactionOS.Code != null)
                            {
                                _currObjectId = _locPayableTransactionOS.Code;

                                PayableTransaction _locPayableTransactionXPO = _currSession.FindObject<PayableTransaction>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locPayableTransactionXPO != null)
                                {
                                    if (_locPayableTransactionXPO.Status == Status.Progress || _locPayableTransactionXPO.Status == Status.Posted)
                                    {
                                        SetPaymentJournal(_currSession, _locPayableTransactionXPO);
                                        SetFinalStatusPayableTransaction(_currSession, _locPayableTransactionXPO);
                                        SetPaymentOutPlan(_currSession, _locPayableTransactionXPO);
                                        SetCloseAllPurchaseProcess(_currSession, _locPayableTransactionXPO);
                                        SuccessMessageShow(_locPayableTransactionXPO.Code + " has been change successfully to Progress");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Payable Transaction Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data Payable Transaction Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PayableTransaction " + ex.ToString());
            }
        }

        private void PayableTransactionListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(PayableTransaction)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        #region PostingMethod

        private void SetPaymentJournal(Session _currSession, PayableTransaction _locPayableTransactionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locPayableTransactionXPO != null)
                {
                    #region JournalPaymentByBusinessPartner

                    XPQuery<PayableTransactionLine> _payableTransactionLineQueryByVendor = new XPQuery<PayableTransactionLine>(_currSession);

                    var _payableTransactionLineByVendors = from ptl in _payableTransactionLineQueryByVendor
                                                           where (ptl.PayableTransaction == _locPayableTransactionXPO
                                                           && ptl.OpenVendor == true
                                                           && (ptl.Status == Status.Progress || ptl.Status == Status.Posted))
                                                           group ptl by ptl.Vendor.BusinessPartnerAccountGroup into g
                                                           select new { BusinessPartnerAccountGroup = g.Key };

                    if (_payableTransactionLineByVendors != null && _payableTransactionLineByVendors.Count() > 0)
                    {
                        double _locTotDebit = 0;
                        double _locTotCredit = 0;
                        foreach (var _payableTransactionLineByVendor in _payableTransactionLineByVendors)
                        {
                            XPCollection<PayableTransactionLine> _locPayableTransactionLines = new XPCollection<PayableTransactionLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("PayableTransaction", _locPayableTransactionXPO),
                                                                        new BinaryOperator("OpenVendor", true),
                                                                        new BinaryOperator("Vendor.BusinessPartnerAccountGroup", _payableTransactionLineByVendor.BusinessPartnerAccountGroup)));

                            if (_locPayableTransactionLines != null && _locPayableTransactionLines.Count() > 0)
                            {
                                //Menghitung Total Debi dan total Credit
                                foreach (PayableTransactionLine _locPayableTransactionLine in _locPayableTransactionLines)
                                {
                                    _locTotDebit = _locTotDebit + _locPayableTransactionLine.Debit;
                                    _locTotCredit = _locTotCredit + _locPayableTransactionLine.Credit;
                                    _locPayableTransactionLine.Status = Status.Close;
                                    _locPayableTransactionLine.StatusDate = now;
                                    _locPayableTransactionLine.Save();
                                    _locPayableTransactionLine.Session.CommitTransaction();
                                }

                                #region JournalMapBankAccountGroup
                                XPCollection<JournalMap> _locJournalMaps = new XPCollection<JournalMap>
                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                           new BinaryOperator("BusinessPartnerAccountGroup", _payableTransactionLineByVendor.BusinessPartnerAccountGroup)));

                                if (_locJournalMaps != null && _locJournalMaps.Count() > 0)
                                {
                                    foreach (JournalMap _locJournalMap in _locJournalMaps)
                                    {
                                        XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("JournalMap", _locJournalMap)));

                                        if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                        {
                                            foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                            {
                                                AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                             (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                             new BinaryOperator("PostingType", PostingType.Purchase),
                                                                             new BinaryOperator("OrderType", OrderType.Item),
                                                                             new BinaryOperator("PostingMethod", PostingMethod.Payment),
                                                                             new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                if (_locAccountMap != null)
                                                {
                                                    XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("AccountMap", _locAccountMap),
                                                                                    new BinaryOperator("Active", true)));

                                                    if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                    {
                                                        double _locTotalAmountDebit = 0;
                                                        double _locTotalAmountCredit = 0;
                                                        double _locTotalBalance = 0;

                                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                        {
                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                            {
                                                                _locTotalAmountDebit = _locTotDebit;
                                                            }
                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                            {
                                                                _locTotalAmountCredit = _locTotCredit;
                                                            }

                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                            {
                                                                PostingDate = now,
                                                                PostingType = PostingType.Purchase,
                                                                PostingMethod = PostingMethod.Payment,
                                                                Account = _locAccountMapLine.Account,
                                                                Debit = _locTotalAmountDebit,
                                                                Credit = _locTotalAmountCredit,
                                                                PurchaseOrder = _locPayableTransactionXPO.PurchaseOrder,
                                                                PayableTransaction = _locPayableTransactionXPO,
                                                                Company = _locPayableTransactionXPO.CompanyDefault,
                                                            };
                                                            _saveGeneralJournal.Save();
                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                            if (_locAccountMapLine.Account.Code != null)
                                                            {
                                                                ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                if (_locCOA != null)
                                                                {
                                                                    if (_locCOA.BalanceType == BalanceType.Change)
                                                                    {
                                                                        if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            if (_locTotalAmountDebit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                            }
                                                                            if (_locTotalAmountCredit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                            }
                                                                        }
                                                                        if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            if (_locTotalAmountDebit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                            }
                                                                            if (_locTotalAmountCredit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                            }
                                                                        }
                                                                    }

                                                                    _locCOA.Balance = _locTotalBalance;
                                                                    _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                    _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                    _locCOA.Save();
                                                                    _locCOA.Session.CommitTransaction();
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion JournalMapBankAccountGroup
                            }
                        }
                    }

                    #endregion JournalPaymentByBusinessPartner

                    #region JournalPaymentBankAccountByCompany
                    XPQuery<PayableTransactionLine> _payableTransactionLineQueryByCompany = new XPQuery<PayableTransactionLine>(_currSession);

                    var _payableTransactionLineByCompanys = from ptl in _payableTransactionLineQueryByCompany
                                                            where (ptl.PayableTransaction == _locPayableTransactionXPO
                                                            && ptl.OpenCompany == true
                                                            && (ptl.Status == Status.Progress || ptl.Status == Status.Posted))
                                                            group ptl by ptl.Company into g
                                                            select new { Company = g.Key };

                    if (_payableTransactionLineByCompanys != null && _payableTransactionLineByCompanys.Count() > 0)
                    {
                        foreach (var _payableTransactionLineByCompany in _payableTransactionLineByCompanys)
                        {
                            XPQuery<PayableTransactionLine> _payableTransactionLineQuery = new XPQuery<PayableTransactionLine>(_currSession);

                            var _payableTransactionLines = from ptl in _payableTransactionLineQuery
                                                           where (ptl.PayableTransaction == _locPayableTransactionXPO
                                                           && ptl.Company == _payableTransactionLineByCompany.Company
                                                           && ptl.OpenCompany == true
                                                           && (ptl.Status == Status.Progress || ptl.Status == Status.Posted))
                                                           group ptl by ptl.BankAccount.BankAccountGroup into g
                                                           select new { BankAccountGroup = g.Key };

                            if (_payableTransactionLines != null && _payableTransactionLines.Count() > 0)
                            {
                                double _locTotDebit = 0;
                                double _locTotCredit = 0;
                                foreach (var _payableTransactionLine in _payableTransactionLines)
                                {
                                    #region PurchasePayment
                                    XPCollection<PayableTransactionLine> _locPayableTransactionLines = new XPCollection<PayableTransactionLine>(_currSession,
                                                                                new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("PayableTransaction", _locPayableTransactionXPO),
                                                                                new BinaryOperator("OpenCompany", true),
                                                                                new BinaryOperator("Company", _payableTransactionLineByCompany.Company),
                                                                                new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                new BinaryOperator("PostingMethod", PostingMethod.Payment),
                                                                                new BinaryOperator("BankAccount.BankAccountGroup", _payableTransactionLine.BankAccountGroup)));

                                    if (_locPayableTransactionLines != null && _locPayableTransactionLines.Count() > 0)
                                    {
                                        //Menghitung Total Debi dan total Credit
                                        foreach (PayableTransactionLine _locPayableTransactionLine in _locPayableTransactionLines)
                                        {
                                            if (_locPayableTransactionLine.PostingType == PostingType.Purchase && _locPayableTransactionLine.PostingMethod == PostingMethod.Payment)
                                            {
                                                _locTotDebit = _locTotDebit + _locPayableTransactionLine.Debit;
                                                _locTotCredit = _locTotCredit + _locPayableTransactionLine.Credit;
                                                _locPayableTransactionLine.Status = Status.Close;
                                                _locPayableTransactionLine.StatusDate = now;
                                                _locPayableTransactionLine.Save();
                                                _locPayableTransactionLine.Session.CommitTransaction();
                                            }
                                        }

                                        #region JournalMapBankAccountGroup
                                        XPCollection<JournalMap> _locJournalMaps = new XPCollection<JournalMap>
                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("BankAccountGroup", _payableTransactionLine.BankAccountGroup)));

                                        if (_locJournalMaps != null && _locJournalMaps.Count() > 0)
                                        {
                                            foreach (JournalMap _locJournalMap in _locJournalMaps)
                                            {
                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("JournalMap", _locJournalMap)));

                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                {
                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                    {
                                                        AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                     new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                     new BinaryOperator("OrderType", OrderType.Item),
                                                                                     new BinaryOperator("PostingMethod", PostingMethod.Payment),
                                                                                     new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                        if (_locAccountMap != null)
                                                        {
                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("AccountMap", _locAccountMap),
                                                                                            new BinaryOperator("Active", true)));

                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                            {
                                                                double _locTotalAmountDebit = 0;
                                                                double _locTotalAmountCredit = 0;
                                                                double _locTotalBalance = 0;

                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                {
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                    {
                                                                        _locTotalAmountDebit = _locTotDebit;
                                                                    }
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                    {
                                                                        _locTotalAmountCredit = _locTotCredit;
                                                                    }

                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                    {
                                                                        PostingDate = now,
                                                                        PostingType = PostingType.Purchase,
                                                                        PostingMethod = PostingMethod.Payment,
                                                                        Account = _locAccountMapLine.Account,
                                                                        Debit = _locTotalAmountDebit,
                                                                        Credit = _locTotalAmountCredit,
                                                                        PurchaseOrder = _locPayableTransactionXPO.PurchaseOrder,
                                                                        PayableTransaction = _locPayableTransactionXPO,
                                                                        Company = _locPayableTransactionXPO.CompanyDefault,
                                                                    };
                                                                    _saveGeneralJournal.Save();
                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                    {
                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                        if (_locCOA != null)
                                                                        {
                                                                            if (_locCOA.BalanceType == BalanceType.Change)
                                                                            {
                                                                                if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                {
                                                                                    if (_locTotalAmountDebit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                    }
                                                                                    if (_locTotalAmountCredit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                    }
                                                                                }
                                                                                if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                {
                                                                                    if (_locTotalAmountDebit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                    }
                                                                                    if (_locTotalAmountCredit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                    }
                                                                                }
                                                                            }

                                                                            _locCOA.Balance = _locTotalBalance;
                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                            _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                            _locCOA.Save();
                                                                            _locCOA.Session.CommitTransaction();
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion JournalMapBankAccountGroup
                                    }
                                    #endregion PurchasePayment                                   
                                }
                            }

                            #region PurchaseAdvancePayment
                            XPCollection<PayableTransactionLine> _locPayableTransactionLine2s = new XPCollection<PayableTransactionLine>(_currSession,
                                                                    new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("PayableTransaction", _locPayableTransactionXPO),
                                                                    new BinaryOperator("OpenCompany", true),
                                                                    new BinaryOperator("Company", _payableTransactionLineByCompany.Company),
                                                                    new BinaryOperator("PostingType", PostingType.Purchase),
                                                                    new BinaryOperator("PostingMethod", PostingMethod.AdvancePayment)));
                            if (_locPayableTransactionLine2s != null && _locPayableTransactionLine2s.Count() > 0)
                            {

                                foreach (PayableTransactionLine _locPayableTransactionLine2 in _locPayableTransactionLine2s)
                                {
                                    #region DebitAdv
                                    if (_locPayableTransactionLine2.Debit > 0 && _locPayableTransactionLine2.Account != null)
                                    {
                                        double _locTotalBalance = 0;
                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                        {
                                            PostingDate = now,
                                            PostingType = PostingType.Purchase,
                                            PostingMethod = PostingMethod.Payment,
                                            Account = _locPayableTransactionLine2.Account,
                                            Debit = _locPayableTransactionLine2.Debit,
                                            Credit = 0,
                                            PurchaseOrder = _locPayableTransactionXPO.PurchaseOrder,
                                            PayableTransaction = _locPayableTransactionXPO,
                                            Company = _locPayableTransactionXPO.CompanyDefault,
                                        };
                                        _saveGeneralJournal.Save();
                                        _saveGeneralJournal.Session.CommitTransaction();

                                        if (_locPayableTransactionLine2.Account.Code != null)
                                        {
                                            ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Code", _locPayableTransactionLine2.Account.Code)));
                                            if (_locCOA != null)
                                            {
                                                if (_locCOA.BalanceType == BalanceType.Change)
                                                {
                                                    if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                    {
                                                        if (_locPayableTransactionLine2.Debit > 0)
                                                        {
                                                            _locTotalBalance = _locCOA.Balance + _locPayableTransactionLine2.Debit;
                                                        }
                                                    }
                                                    if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                    {
                                                        if (_locPayableTransactionLine2.Debit > 0)
                                                        {
                                                            _locTotalBalance = _locCOA.Balance - _locPayableTransactionLine2.Debit;
                                                        }
                                                    }
                                                }

                                                _locCOA.Balance = _locTotalBalance;
                                                _locCOA.Debit = _locCOA.Debit + _locPayableTransactionLine2.Debit;
                                                _locCOA.Save();
                                                _locCOA.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                    #endregion DebitAdv
                                    #region CreditAdv
                                    else if (_locPayableTransactionLine2.Credit > 0)
                                    {
                                        if (_locPayableTransactionLine2.BankAccount != null)
                                        {
                                            if (_locPayableTransactionLine2.BankAccount.BankAccountGroup != null)
                                            {
                                                #region JournalMapBankAccountGroup
                                                XPCollection<JournalMap> _locJournalMaps = new XPCollection<JournalMap>
                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                           new BinaryOperator("BankAccountGroup", _locPayableTransactionLine2.BankAccount.BankAccountGroup)));

                                                if (_locJournalMaps != null && _locJournalMaps.Count() > 0)
                                                {
                                                    foreach (JournalMap _locJournalMap in _locJournalMaps)
                                                    {
                                                        XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("JournalMap", _locJournalMap)));

                                                        if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                        {
                                                            foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                            {
                                                                AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                             (new GroupOperator(GroupOperatorType.And,
                                                                                             new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                             new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                             new BinaryOperator("OrderType", OrderType.Item),
                                                                                             new BinaryOperator("PostingMethod", PostingMethod.AdvancePayment),
                                                                                             new BinaryOperator("PostingMethodType", PostingMethodType.Advances)));

                                                                if (_locAccountMap != null)
                                                                {
                                                                    XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                    new BinaryOperator("Active", true)));

                                                                    if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                    {
                                                                        double _locTotalAmountCredit = 0;
                                                                        double _locTotalBalance = 0;

                                                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                        {

                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                            {
                                                                                _locTotalAmountCredit = _locPayableTransactionLine2.Credit;
                                                                            }

                                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                            {
                                                                                PostingDate = now,
                                                                                PostingType = PostingType.Purchase,
                                                                                PostingMethod = PostingMethod.AdvancePayment,
                                                                                Account = _locAccountMapLine.Account,
                                                                                Debit = 0,
                                                                                Credit = _locTotalAmountCredit,
                                                                                PurchaseOrder = _locPayableTransactionXPO.PurchaseOrder,
                                                                                PayableTransaction = _locPayableTransactionXPO,
                                                                                Company = _locPayableTransactionXPO.CompanyDefault,
                                                                            };
                                                                            _saveGeneralJournal.Save();
                                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                                            if (_locAccountMapLine.Account.Code != null)
                                                                            {
                                                                                ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                                if (_locCOA != null)
                                                                                {
                                                                                    if (_locCOA.BalanceType == BalanceType.Change)
                                                                                    {
                                                                                        if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                        {

                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                        if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                        {
                                                                                            if (_locTotalAmountCredit > 0)
                                                                                            {
                                                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                            }
                                                                                        }
                                                                                    }

                                                                                    _locCOA.Balance = _locTotalBalance;
                                                                                    _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                    _locCOA.Save();
                                                                                    _locCOA.Session.CommitTransaction();
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                #endregion JournalMapBankAccountGroup
                                            }
                                        }
                                    }
                                    #endregion CreditAdv
                                }
                            }
                            #endregion PurchaseAdvancePayment
                        }
                    }
                    #endregion JournalPaymentBankAccountByCompany

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseInvoice ", ex.ToString());
            }
        }

        private void SetFinalStatusPayableTransaction(Session _currSession, PayableTransaction _locPayableTransactionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locCount = 0;
                if (_locPayableTransactionXPO != null)
                {
                    XPCollection<PayableTransactionLine> _locPayableTransactionLines = new XPCollection<PayableTransactionLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PayableTransaction", _locPayableTransactionXPO)));

                    if (_locPayableTransactionLines != null && _locPayableTransactionLines.Count() > 0)
                    {

                        foreach (PayableTransactionLine _locPayableTransactionLine in _locPayableTransactionLines)
                        {
                            if (_locPayableTransactionLine.Status == Status.Close)
                            {
                                _locCount = _locCount + 1;
                            }
                        }

                        if (_locCount == _locPayableTransactionLines.Count())
                        {
                            _locPayableTransactionXPO.ActivationPosting = true;
                            _locPayableTransactionXPO.Status = Status.Close;
                            _locPayableTransactionXPO.StatusDate = now;
                            _locPayableTransactionXPO.Save();
                            _locPayableTransactionXPO.Session.CommitTransaction();
                        }
                        else
                        {
                            _locPayableTransactionXPO.Status = Status.Posted;
                            _locPayableTransactionXPO.StatusDate = now;
                            _locPayableTransactionXPO.Save();
                            _locPayableTransactionXPO.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PayableTransaction " + ex.ToString());
            }
        }

        private void SetPaymentOutPlan(Session _currSession, PayableTransaction _locPayableTransactionXPO)
        {
            try
            {
                //Mencek Payment Out yg ada
                PurchaseOrder _locPurchOrder = null;
                PurchaseInvoice _locPurchInvoice = null;
                XPCollection<PaymentOutPlan> _locPaymentOutPlans = null;

                if (_locPayableTransactionXPO.PurchaseOrder != null)
                {
                    _locPurchOrder = _locPayableTransactionXPO.PurchaseOrder;
                }

                if (_locPayableTransactionXPO.PurchaseInvoice != null)
                {
                    _locPurchInvoice = _locPayableTransactionXPO.PurchaseInvoice;
                }

                if(_locPayableTransactionXPO.PurchasePrePaymentInvoice != null)
                {
                    _locPaymentOutPlans = new XPCollection<PaymentOutPlan>(_currSession,
                                                new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("PurchaseOrder", _locPurchOrder),
                                                new BinaryOperator("PurchasePrePaymentInvoice", _locPayableTransactionXPO.PurchasePrePaymentInvoice),
                                                new BinaryOperator("PayableTransaction", _locPayableTransactionXPO)));
                }
                else
                {
                    _locPaymentOutPlans = new XPCollection<PaymentOutPlan>(_currSession,
                                                new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("PurchaseOrder", _locPurchOrder),
                                                new BinaryOperator("PurchaseInvoice", _locPurchInvoice),
                                                new BinaryOperator("PayableTransaction", _locPayableTransactionXPO)));
                }
                

                if (_locPaymentOutPlans != null && _locPaymentOutPlans.Count > 0)
                {
                    //Cari Final Amount di PayableTransactionLine cari yang debit
                    double _locCredit = 0;

                    XPCollection<PayableTransactionLine> _locPayableTransactionLines = new XPCollection<PayableTransactionLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PayableTransaction", _locPayableTransactionXPO)));

                    if (_locPayableTransactionLines != null && _locPayableTransactionLines.Count > 0)
                    {
                        foreach (PayableTransactionLine _locPayableTransactionLine in _locPayableTransactionLines)
                        {
                            if (_locPayableTransactionLine.Credit > 0)
                            {
                                _locCredit = _locCredit + _locPayableTransactionLine.Credit;
                            }
                        }
                    }


                    foreach (PaymentOutPlan _locPaymentOutPlan in _locPaymentOutPlans)
                    {
                        _locPaymentOutPlan.Outstanding = _locPaymentOutPlan.Outstanding - _locCredit;
                        _locPaymentOutPlan.Paid = _locCredit;
                        _locPaymentOutPlan.Save();
                        _locPaymentOutPlan.Session.CommitTransaction();
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PayableTransaction " + ex.ToString());
            }
        }

        private void SetCloseAllPurchaseProcess(Session _currSession, PayableTransaction _locPayableTransactionXPO)
        {
            //GroupByPaymentOutPlanBasedOnPurchaseInvoice


            DateTime now = DateTime.Now;
            if (_locPayableTransactionXPO != null)
            {
                #region ClosePurchaseInvoice
                if (_locPayableTransactionXPO.PurchaseInvoice != null)
                {
                    double _locPayPaid = 0;

                    XPCollection<PaymentOutPlan> _locPaymentOutPlans = new XPCollection<PaymentOutPlan>(_currSession,
                                                                new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("PurchaseInvoice", _locPayableTransactionXPO.PurchaseInvoice)));

                    if (_locPaymentOutPlans != null && _locPaymentOutPlans.Count > 0)
                    {
                        foreach (PaymentOutPlan _locPaymentOutPlan in _locPaymentOutPlans)
                        {
                            _locPayPaid = _locPayPaid + _locPaymentOutPlan.Paid;
                        }

                        if (_locPayableTransactionXPO.PurchaseInvoice.MaxPay > 0)
                        {
                            if (_locPayableTransactionXPO.PurchaseInvoice.MaxPay == _locPayPaid)
                            {
                                if (_locPayableTransactionXPO.PurchaseOrder != null)
                                {
                                    if (_locPayableTransactionXPO.PurchaseOrder.Code != null)
                                    {
                                        PurchaseOrder _locPurchaseOrder = _currSession.FindObject<PurchaseOrder>(new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Code", _locPayableTransactionXPO.PurchaseOrder.Code)));
                                        if (_locPurchaseOrder != null)
                                        {
                                            _locPurchaseOrder.Status = Status.Close;
                                            _locPurchaseOrder.StatusDate = now;
                                            _locPurchaseOrder.Save();
                                            _locPurchaseOrder.Session.CommitTransaction();
                                        }
                                    }
                                }

                                if (_locPayableTransactionXPO.PurchaseInvoice.Code != null)
                                {
                                    PurchaseInvoice _locPurchaseInvoice = _currSession.FindObject<PurchaseInvoice>(new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Code", _locPayableTransactionXPO.PurchaseInvoice.Code)));
                                    if (_locPurchaseInvoice != null)
                                    {
                                        _locPurchaseInvoice.Status = Status.Close;
                                        _locPurchaseInvoice.StatusDate = now;
                                        _locPurchaseInvoice.Save();
                                        _locPurchaseInvoice.Session.CommitTransaction();
                                    }
                                }

                                foreach (PaymentOutPlan _locPaymentOutPlan in _locPaymentOutPlans)
                                {
                                    _locPaymentOutPlan.Status = Status.Close;
                                    _locPaymentOutPlan.StatusDate = now;
                                    _locPaymentOutPlan.Outstanding = 0;
                                    _locPaymentOutPlan.Save();
                                    _locPaymentOutPlan.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }
                #endregion ClosePurchaseInvoice

                #region ClosePurchasePrePaymentInvoice
                if (_locPayableTransactionXPO.PurchasePrePaymentInvoice != null)
                {
                    double _locPayPaid = 0;

                    XPCollection<PaymentOutPlan> _locPaymentOutPlans = new XPCollection<PaymentOutPlan>(_currSession,
                                                                new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("PurchasePrePaymentInvoice", _locPayableTransactionXPO.PurchasePrePaymentInvoice)));

                    if (_locPaymentOutPlans != null && _locPaymentOutPlans.Count > 0)
                    {
                        foreach (PaymentOutPlan _locPaymentOutPlan in _locPaymentOutPlans)
                        {
                            _locPayPaid = _locPayPaid + _locPaymentOutPlan.Paid;
                        }

                        if (_locPayableTransactionXPO.PurchasePrePaymentInvoice.MaxPay > 0)
                        {
                            if (_locPayableTransactionXPO.PurchasePrePaymentInvoice.MaxPay == _locPayPaid)
                            {
                                if (_locPayableTransactionXPO.PurchaseOrder != null)
                                {
                                    if (_locPayableTransactionXPO.PurchaseOrder.Code != null)
                                    {
                                        PurchaseOrder _locPurchaseOrder = _currSession.FindObject<PurchaseOrder>(new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Code", _locPayableTransactionXPO.PurchaseOrder.Code)));
                                        if (_locPurchaseOrder != null)
                                        {
                                            _locPurchaseOrder.Status = Status.Close;
                                            _locPurchaseOrder.StatusDate = now;
                                            _locPurchaseOrder.Save();
                                            _locPurchaseOrder.Session.CommitTransaction();
                                        }
                                    }
                                }

                                if (_locPayableTransactionXPO.PurchasePrePaymentInvoice.Code != null)
                                {
                                    PurchasePrePaymentInvoice _locPurchasePrePaymentInvoice = _currSession.FindObject<PurchasePrePaymentInvoice>(new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Code", _locPayableTransactionXPO.PurchasePrePaymentInvoice.Code)));
                                    if (_locPurchasePrePaymentInvoice != null)
                                    {
                                        _locPurchasePrePaymentInvoice.Status = Status.Close;
                                        _locPurchasePrePaymentInvoice.StatusDate = now;
                                        _locPurchasePrePaymentInvoice.Save();
                                        _locPurchasePrePaymentInvoice.Session.CommitTransaction();
                                    }
                                }

                                foreach (PaymentOutPlan _locPaymentOutPlan in _locPaymentOutPlans)
                                {
                                    _locPaymentOutPlan.Status = Status.Close;
                                    _locPaymentOutPlan.StatusDate = now;
                                    _locPaymentOutPlan.Outstanding = 0;
                                    _locPaymentOutPlan.Save();
                                    _locPaymentOutPlan.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }
                #endregion ClosePurchasePrePaymentInvoice

                #region ClosePurchaseInvoiceCollectionOrPurchasePrePaymentInvoice
                if (_locPayableTransactionXPO.PurchaseInvoice != null || _locPayableTransactionXPO.PurchasePrePaymentInvoice != null)
                {
                    if (_locPayableTransactionXPO.PurchaseOrder != null)
                    {
                        double _locPayPaid = 0;
                        double _locMaxPay = 0;

                        PurchaseInvoiceCollection _locPurchaseInvoiceCollection = _currSession.FindObject<PurchaseInvoiceCollection>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("PurchaseOrder", _locPayableTransactionXPO.PurchaseOrder)));
                        if (_locPurchaseInvoiceCollection != null)
                        {
                            XPCollection<PaymentOutPlan> _locPaymentOutPlans = new XPCollection<PaymentOutPlan>(_currSession,
                                                                new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("PurchaseInvoiceCollection", _locPurchaseInvoiceCollection)));

                            if (_locPaymentOutPlans != null && _locPaymentOutPlans.Count > 0)
                            {
                                foreach (PaymentOutPlan _locPaymentOutPlan in _locPaymentOutPlans)
                                {
                                    _locPayPaid = _locPayPaid + _locPaymentOutPlan.Paid;
                                }

                                _locMaxPay = _locPurchaseInvoiceCollection.MaxPay - _locPurchaseInvoiceCollection.CM_Amount;

                                if (_locMaxPay == _locPayPaid)
                                {
                                    #region ClosePurchaseOrderAndPurchasePrePaymentInvoice
                                    if (_locPayableTransactionXPO.PurchaseOrder.Code != null)
                                    {
                                        PurchaseOrder _locPurchaseOrder = _currSession.FindObject<PurchaseOrder>(new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("Code", _locPayableTransactionXPO.PurchaseOrder.Code)));
                                        if (_locPurchaseOrder != null)
                                        {
                                            _locPurchaseOrder.Status = Status.Close;
                                            _locPurchaseOrder.StatusDate = now;
                                            _locPurchaseOrder.Save();
                                            _locPurchaseOrder.Session.CommitTransaction();

                                            PurchasePrePaymentInvoice _locPurchasePrePaymentInvoice = _currSession.FindObject<PurchasePrePaymentInvoice>
                                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                                     new BinaryOperator("PurchaseOrder", _locPurchaseOrder)));
                                            if(_locPurchasePrePaymentInvoice != null)
                                            {
                                                _locPurchasePrePaymentInvoice.Status = Status.Close;
                                                _locPurchasePrePaymentInvoice.StatusDate = now;
                                                _locPurchasePrePaymentInvoice.Save();
                                                _locPurchasePrePaymentInvoice.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                    #endregion ClosePurchaseOrder

                                    #region ClosePurchaseInvoice
                                    XPQuery<PaymentOutPlan> _paymentOutPlanQuery = new XPQuery<PaymentOutPlan>(_currSession);

                                    var _paymentOutPlans = from pop in _paymentOutPlanQuery
                                                          where (pop.PurchaseInvoiceCollection == _locPurchaseInvoiceCollection)
                                                          group pop by pop.PurchaseInvoice into g
                                                          select new { PurchaseInvoice = g.Key };

                                    if (_paymentOutPlans != null && _paymentOutPlans.Count() > 0)
                                    {
                                        foreach (var _paymentOutPlan in _paymentOutPlans)
                                        {
                                            if(_paymentOutPlan.PurchaseInvoice != null)
                                            {
                                                if (_paymentOutPlan.PurchaseInvoice.Code != null)
                                                {
                                                    PurchaseInvoice _locPurchaseInvoice = _currSession.FindObject<PurchaseInvoice>(new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Code", _paymentOutPlan.PurchaseInvoice.Code)));
                                                    if (_locPurchaseInvoice != null)
                                                    {
                                                        _locPurchaseInvoice.Status = Status.Close;
                                                        _locPurchaseInvoice.StatusDate = now;
                                                        _locPurchaseInvoice.Save();
                                                        _locPurchaseInvoice.Session.CommitTransaction();
                                                    }
                                                }
                                            }   
                                        }
                                    }
                                    #endregion ClosePurchaseInvoice

                                    #region ClosePaymentOutPlan
                                    foreach (PaymentOutPlan _locPaymentOutPlan in _locPaymentOutPlans)
                                    {
                                        _locPaymentOutPlan.Status = Status.Close;
                                        _locPaymentOutPlan.StatusDate = now;
                                        _locPaymentOutPlan.Save();
                                        _locPaymentOutPlan.Session.CommitTransaction();
                                    }
                                    #endregion ClosePaymentOutPlan

                                    #region ClosePurchaseInvoiceCollection
                                    _locPurchaseInvoiceCollection.Status = Status.Close;
                                    _locPurchaseInvoiceCollection.StatusDate = now;
                                    _locPurchaseInvoiceCollection.Save();
                                    _locPurchaseInvoiceCollection.Session.CommitTransaction();
                                    #endregion ClosePurchaseInvoiceCollection
                                }
                            }

                        }
                    }
                }
                #endregion ClosePurchaseInvoiceCollectionOrPurchasePrePaymentInvoice
            }
        }

        #endregion PostingMethod

        #region PostingMethodOld

        private void SetCloseAllPurchaseProcessOld(Session _currSession, PayableTransaction _locPayableTransactionXPO)
        {
            //GroupByPaymentOutPlanBasedOnPurchaseInvoice
            double _locPaid = 0;
            DateTime now = DateTime.Now;
            if (_locPayableTransactionXPO.PurchaseInvoice != null)
            {
                XPCollection<PaymentOutPlan> _locPaymentOutPlans = new XPCollection<PaymentOutPlan>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseInvoice", _locPayableTransactionXPO.PurchaseInvoice)));

                if (_locPaymentOutPlans != null && _locPaymentOutPlans.Count > 0)
                {
                    foreach (PaymentOutPlan _locPaymentOutPlan in _locPaymentOutPlans)
                    {
                        _locPaid = _locPaid + _locPaymentOutPlan.Paid;
                    }

                    if (_locPayableTransactionXPO.PurchaseInvoice.MaxPay > 0)
                    {
                        if (_locPayableTransactionXPO.PurchaseInvoice.MaxPay == _locPaid)
                        {
                            if (_locPayableTransactionXPO.PurchaseOrder != null)
                            {
                                if (_locPayableTransactionXPO.PurchaseOrder.Code != null)
                                {
                                    PurchaseOrder _locPurchaseOrder = _currSession.FindObject<PurchaseOrder>(new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _locPayableTransactionXPO.PurchaseOrder.Code)));
                                    if (_locPurchaseOrder != null)
                                    {
                                        _locPurchaseOrder.Status = Status.Close;
                                        _locPurchaseOrder.StatusDate = now;
                                        _locPurchaseOrder.Save();
                                        _locPurchaseOrder.Session.CommitTransaction();
                                    }
                                }

                            }

                            if (_locPayableTransactionXPO.PurchaseInvoice.Code != null)
                            {
                                PurchaseInvoice _locPurchaseInvoice = _currSession.FindObject<PurchaseInvoice>(new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("Code", _locPayableTransactionXPO.PurchaseInvoice.Code)));
                                if (_locPurchaseInvoice != null)
                                {
                                    _locPurchaseInvoice.Status = Status.Close;
                                    _locPurchaseInvoice.StatusDate = now;
                                    _locPurchaseInvoice.Save();
                                    _locPurchaseInvoice.Session.CommitTransaction();
                                }
                            }

                            foreach (PaymentOutPlan _locPaymentOutPlan in _locPaymentOutPlans)
                            {
                                _locPaymentOutPlan.Status = Status.Close;
                                _locPaymentOutPlan.StatusDate = now;
                                _locPaymentOutPlan.Save();
                                _locPaymentOutPlan.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
        }

        #endregion PostingMethodOld

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

    }
}
