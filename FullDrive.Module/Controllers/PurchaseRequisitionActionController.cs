﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

using System.Web;
using System.IO;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PurchaseRequisitionActionController : ViewController
    {
        #region Default

        private ChoiceActionItem _selectionListviewFilter;
        private ChoiceActionItem _selectionApprovalListviewFilter;
        private ChoiceActionItem _setApprovalLevel;

        public PurchaseRequisitionActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region FilterStatus
            PurchaseRequisitionListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {

                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                if(_ed.GetCaption(_currApproval) != "Approved")
                {
                    _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                }
                PurchaseRequisitionListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterStatus

            #region FilterApproval
            PurchaseRequisitionListviewFilterApprovalSelectionAction.Items.Clear();
            foreach(object _currApprovalFilter in Enum.GetValues(typeof(CustomProcess.ApprovalFilter)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalFilter));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApprovalFilter), _currApprovalFilter);
                PurchaseRequisitionListviewFilterApprovalSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterApproval
        }

        protected override void OnActivated()
        {
            TypeOfView = typeof(ObjectView);
            base.OnActivated();
            // Perform various tasks depending on the target View.
            #region Approval
            var _locUser = SecuritySystem.CurrentUserName;
            GlobalFunction _globalFunction = new GlobalFunction();
            Session _currentSession = null;
            if (this.ObjectSpace != null)
            {
                _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
            }

            if (_currentSession != null)
            {
                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", _locUser));

                if (_locUserAccess != null)
                {
                    PurchaseRequisitionApprovalAction.Items.Clear();

                    XPCollection<ApplicationSetupDetail> _locAppSetupDetails = new XPCollection<ApplicationSetupDetail>
                                                                                 (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("UserAccess", _locUserAccess),
                                                                                  new BinaryOperator("ObjectList", CustomProcess.ObjectList.PurchaseRequisition),
                                                                                  new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                  new BinaryOperator("Active", true)));

                    if (_locAppSetupDetails != null && _locAppSetupDetails.Count > 0)
                    {

                        foreach (ApplicationSetupDetail _locAppSetupDetail in _locAppSetupDetails)
                        {
                            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.ApprovalLevel)))
                            {
                                if (_locAppSetupDetail.ApprovalLevel != CustomProcess.ApprovalLevel.None)
                                {
                                    EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalLevel));
                                    _setApprovalLevel = new ChoiceActionItem(_ed.GetCaption(_locAppSetupDetail.ApprovalLevel), _locAppSetupDetail.ApprovalLevel);
                                }
                            }
                            PurchaseRequisitionApprovalAction.Items.Add(_setApprovalLevel);
                        }
                    }
                }
            }
            #endregion Approval
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        private void PurchaseRequisitionProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseRequisition _locPurchaseRequisitionOS = (PurchaseRequisition)_objectSpace.GetObject(obj);

                        if (_locPurchaseRequisitionOS != null)
                        {
                            if (_locPurchaseRequisitionOS.Code != null)
                            {
                                _currObjectId = _locPurchaseRequisitionOS.Code;

                                PurchaseRequisition _locPurchaseRequisitionXPO = _currSession.FindObject<PurchaseRequisition>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locPurchaseRequisitionXPO != null)
                                {
                                    #region Status
                                    if (_locPurchaseRequisitionXPO.Status == Status.Open || _locPurchaseRequisitionXPO.Status == Status.Progress)
                                    {
                                        if (_locPurchaseRequisitionXPO.Status == Status.Open)
                                        {
                                            _locPurchaseRequisitionXPO.Status = Status.Progress;
                                            _locPurchaseRequisitionXPO.StatusDate = now;
                                            _locPurchaseRequisitionXPO.Save();
                                            _locPurchaseRequisitionXPO.Session.CommitTransaction();
                                        }

                                        XPCollection<PurchaseRequisitionLine> _locPurchaseRequisitionLines = new XPCollection<PurchaseRequisitionLine>
                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Open),
                                                            new BinaryOperator("Status", Status.Progress))));

                                        if (_locPurchaseRequisitionLines != null && _locPurchaseRequisitionLines.Count > 0)
                                        {
                                            foreach (PurchaseRequisitionLine _locPurchaseRequisitionLine in _locPurchaseRequisitionLines)
                                            {
                                                _locPurchaseRequisitionLine.Status = Status.Progress;
                                                _locPurchaseRequisitionLine.StatusDate = now;
                                                _locPurchaseRequisitionLine.Save();
                                                _locPurchaseRequisitionLine.Session.CommitTransaction();
                                            }
                                        }

                                        XPCollection<PurchaseRequisitionMonitoring> _locPurchaseRequisitionMonitorings = new XPCollection<PurchaseRequisitionMonitoring>
                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("Status", Status.Open)));
                                        if (_locPurchaseRequisitionMonitorings != null && _locPurchaseRequisitionMonitorings.Count() > 0)
                                        {
                                            foreach (PurchaseRequisitionMonitoring _locPurchaseRequisitionMonitoring in _locPurchaseRequisitionMonitorings)
                                            {
                                                _locPurchaseRequisitionMonitoring.Status = Status.Open;
                                                _locPurchaseRequisitionMonitoring.StatusDate = now;
                                                _locPurchaseRequisitionMonitoring.Save();
                                                _locPurchaseRequisitionMonitoring.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                    #endregion Status

                                    SuccessMessageShow("PR has successfully updated to progress");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Order Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Purchase Order Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseRequisition " + ex.ToString());
            }
        }

        private void PurchaseRequisitionPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseRequisition _locPurchaseRequisitionOS = (PurchaseRequisition)_objectSpace.GetObject(obj);

                        if (_locPurchaseRequisitionOS != null)
                        {
                            if (_locPurchaseRequisitionOS.Code != null)
                            {
                                _currObjectId = _locPurchaseRequisitionOS.Code;

                                PurchaseRequisition _locPurchaseRequisitionXPO = _currSession.FindObject<PurchaseRequisition>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locPurchaseRequisitionXPO != null)
                                {
                                    if (_locPurchaseRequisitionXPO.Status == Status.Progress || _locPurchaseRequisitionXPO.Status == Status.Posted)
                                    {

                                        ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("EndApproval", true),
                                                                    new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO)));

                                        if (_locApprovalLineXPO != null)
                                        {
                                            SetPurchaseRequisitionMonitoring(_currSession, _locPurchaseRequisitionXPO);
                                            SetRemainQty(_currSession, _locPurchaseRequisitionXPO);
                                            SetPostingQty(_currSession, _locPurchaseRequisitionXPO);
                                            SetProcessCount(_currSession, _locPurchaseRequisitionXPO);
                                            SetStatusPurchaseRequisitionLine(_currSession, _locPurchaseRequisitionXPO);
                                            SetNormalQuantity(_currSession, _locPurchaseRequisitionXPO);
                                            SetFinalStatusPurchaseRequisition(_currSession, _locPurchaseRequisitionXPO);
                                            SuccessMessageShow("PR has successfully posted into Purchase Requisition Monitoring");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Order Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Purchase Order Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseRequisition " + ex.ToString());
            }
        }

        private void PurchaseRequisitionListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(PurchaseRequisition)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseRequisition " + ex.ToString());
            }
        }

        private void PurchaseRequisitionListviewFilterApprovalSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(PurchaseRequisition)))
                {
                    if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.Approval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal)));
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.EndApproval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal);
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.AllData)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved1", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal));
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseRequisition " + ex.ToString());
            }
        }

        private void PurchaseRequisitionApprovalAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                ApplicationSetupDetail _locAppSetDetail = null;
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                var User = SecuritySystem.CurrentUserName;
                Session _currentSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));

                foreach (Object obj in _objectsToProcess)
                {
                    PurchaseRequisition _objInNewObjectSpace = (PurchaseRequisition)_objectSpace.GetObject(obj);

                    if (_objInNewObjectSpace != null)
                    {
                        if (_objInNewObjectSpace.Code != null)
                        {
                            _currObjectId = _objInNewObjectSpace.Code;
                        }
                    }

                    if (_currObjectId != null)
                    {
                        PurchaseRequisition _locPurchaseRequisitionXPO = _currentSession.FindObject<PurchaseRequisition>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId)));

                        if (_locPurchaseRequisitionXPO != null)
                        {
                            if (_locPurchaseRequisitionXPO.Status == Status.Progress)
                            {
                                ApprovalLine _locApprovalLine = _currentSession.FindObject<ApprovalLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO),
                                                                             new BinaryOperator("EndApproval", true)));
                                if (_locApprovalLine == null)
                                {
                                    #region Approval Level 1
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level1)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level1, ObjectList.PurchaseRequisition);
                                        if (_locAppSetDetail != null)
                                        {
                                            //Buat bs input langsung ke approvalline
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level1)));
                                            if (_locApprovalLineXPO == null)
                                            {

                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        EndApproval = true,
                                                        PurchaseRequisition = _locPurchaseRequisitionXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locPurchaseRequisitionXPO.ActivationPosting = true;
                                                    _locPurchaseRequisitionXPO.ActiveApproved1 = false;
                                                    _locPurchaseRequisitionXPO.ActiveApproved2 = false;
                                                    _locPurchaseRequisitionXPO.ActiveApproved3 = true;
                                                    _locPurchaseRequisitionXPO.Save();
                                                    _locPurchaseRequisitionXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        PurchaseRequisition = _locPurchaseRequisitionXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locPurchaseRequisitionXPO.ActiveApproved1 = true;
                                                    _locPurchaseRequisitionXPO.ActiveApproved2 = false;
                                                    _locPurchaseRequisitionXPO.ActiveApproved3 = false;
                                                    _locPurchaseRequisitionXPO.Save();
                                                    _locPurchaseRequisitionXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO)));
                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locPurchaseRequisitionXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("Purchase Requisition has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 1

                                    #region Approval Level 2
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level2)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level2, ObjectList.PurchaseRequisition);

                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level2)));

                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        EndApproval = true,
                                                        PurchaseRequisition = _locPurchaseRequisitionXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locPurchaseRequisitionXPO.ActivationPosting = true;
                                                    _locPurchaseRequisitionXPO.ActiveApproved1 = false;
                                                    _locPurchaseRequisitionXPO.ActiveApproved2 = false;
                                                    _locPurchaseRequisitionXPO.ActiveApproved3 = true;
                                                    _locPurchaseRequisitionXPO.Save();
                                                    _locPurchaseRequisitionXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        PurchaseRequisition = _locPurchaseRequisitionXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locPurchaseRequisitionXPO, ApprovalLevel.Level1);

                                                    _locPurchaseRequisitionXPO.ActiveApproved1 = false;
                                                    _locPurchaseRequisitionXPO.ActiveApproved2 = true;
                                                    _locPurchaseRequisitionXPO.ActiveApproved3 = false;
                                                    _locPurchaseRequisitionXPO.Save();
                                                    _locPurchaseRequisitionXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO)));
                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locPurchaseRequisitionXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("Purchase Requisition has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 2

                                    #region Approval Level 3
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level3)
                                    {
                                        _locAppSetDetail = _globFunc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level3, ObjectList.PurchaseRequisition);

                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level3)));

                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        EndApproval = true,
                                                        PurchaseRequisition = _locPurchaseRequisitionXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    _locPurchaseRequisitionXPO.ActivationPosting = true;
                                                    _locPurchaseRequisitionXPO.ActiveApproved1 = false;
                                                    _locPurchaseRequisitionXPO.ActiveApproved2 = false;
                                                    _locPurchaseRequisitionXPO.ActiveApproved3 = true;
                                                    _locPurchaseRequisitionXPO.Save();
                                                    _locPurchaseRequisitionXPO.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        PurchaseRequisition = _locPurchaseRequisitionXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locPurchaseRequisitionXPO, ApprovalLevel.Level2);
                                                    SetApprovalLine(_currentSession, _locPurchaseRequisitionXPO, ApprovalLevel.Level1);

                                                    _locPurchaseRequisitionXPO.ActiveApproved1 = false;
                                                    _locPurchaseRequisitionXPO.ActiveApproved2 = false;
                                                    _locPurchaseRequisitionXPO.ActiveApproved3 = true;
                                                    _locPurchaseRequisitionXPO.Save();
                                                    _locPurchaseRequisitionXPO.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO)));
                                                if (_locApprovalLineXPO2 != null)
                                                {
                                                    SendEmail(_currentSession, _locPurchaseRequisitionXPO, Status.Progress, _locApprovalLineXPO2);
                                                }

                                                SuccessMessageShow("Purchase Requisition has successfully Approve");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 3
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Purchase Requisition Status Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Purchase Requisition Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseRequisition " + ex.ToString());
            }
        }

        //==== Code Only ====

        #region Posting

        private void SetPurchaseRequisitionMonitoring(Session _currSession, PurchaseRequisition _locPurchaseRequisitionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locPurchaseRequisitionXPO != null)
                {
                    XPCollection<PurchaseRequisitionLine> _locPurchaseRequisitionLines = new XPCollection<PurchaseRequisitionLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locPurchaseRequisitionLines != null && _locPurchaseRequisitionLines.Count() > 0)
                    {

                        foreach (PurchaseRequisitionLine _locPurchaseRequisitionLine in _locPurchaseRequisitionLines)
                        {
                            if (_locPurchaseRequisitionLine.Status == Status.Progress || _locPurchaseRequisitionLine.Status == Status.Posted)
                            {
                                PurchaseRequisitionMonitoring _saveDataPurchaseRequisitionMonitoring = new PurchaseRequisitionMonitoring(_currSession)
                                {
                                    PurchaseRequisition = _locPurchaseRequisitionXPO,
                                    PurchaseRequisitionLine = _locPurchaseRequisitionLine,
                                    Item = _locPurchaseRequisitionLine.Item,
                                    DQty = _locPurchaseRequisitionLine.DQty,
                                    DUOM = _locPurchaseRequisitionLine.DUOM,
                                    Qty = _locPurchaseRequisitionLine.Qty,
                                    TQty = _locPurchaseRequisitionLine.TQty,
                                    UOM = _locPurchaseRequisitionLine.UOM,
                                };
                                _saveDataPurchaseRequisitionMonitoring.Save();
                                _saveDataPurchaseRequisitionMonitoring.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseRequisition " + ex.ToString());
            }
        }

        #endregion Posting

        #region Approval Process

        private void SetApprovalLine(Session _currentSession, PurchaseRequisition _locPurchaseRequisitionXPO, ApprovalLevel _locApprovalLevel)
        {
            try
            {
                DateTime now = DateTime.Now;

                ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO),
                                                                         new BinaryOperator("ApprovalLevel", _locApprovalLevel)));

                if (_locApprovalLineXPO == null)
                {
                    ApprovalLine _saveDataAL2 = new ApprovalLine(_currentSession)
                    {
                        ApprovalDate = now,
                        ApprovalStatus = Status.Approved,
                        ApprovalLevel = _locApprovalLevel,
                        PurchaseRequisition = _locPurchaseRequisitionXPO,
                    };
                    _saveDataAL2.Save();
                    _saveDataAL2.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseRequisition " + ex.ToString());
            }
        }

        #endregion Approval Process

        #region Email

        private string BackgroundBody(Session _currentSession, PurchaseRequisition _locPurchaseRequisitionXPO, MailSetupDetail _locMailSetDetail)
        {
            string body = string.Empty;

            PurchaseRequisition _locPurchaseRequisitions = _currentSession.FindObject<PurchaseRequisition>
                                           (new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Code", _locPurchaseRequisitionXPO.Code)));

            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                               (new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitions)));

            string Status = _locApprovalLineXPO.ApprovalStatus.ToString();

            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Email/EmailTemplate.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{Code}", _locPurchaseRequisitionXPO.Code);

            #region Level
            if (_locPurchaseRequisitions.ActiveApproved1 == true)
            {
                string Active = "Level 1";
                body = body.Replace("{Level}", Active);
            }
            else if (_locPurchaseRequisitions.ActiveApproved2 == true)
            {
                string Active = "Level 2";
                body = body.Replace("{Level}", Active);
            }
            else if (_locPurchaseRequisitions.ActiveApproved3 == true)
            {
                string Active = "Level 3";
                body = body.Replace("{Level}", Active);
            }
            else
            {
                string Active = "Not Available";
                body = body.Replace("{Level}", Active);
            }
            #endregion Level

            body = body.Replace("{Status}", Status);
            body = body.Replace("{Message}", _locMailSetDetail.MessageBody);
            return body;
        }

        protected void SendEmail(Session _currentSession, PurchaseRequisition _locPurchaseRequisitionXPO, Status _locStatus, ApprovalLine _locApprovalLineXPO2)
        {
            DateTime now = DateTime.Now;
            var User = SecuritySystem.CurrentUserName;
            GlobalFunction _globFunc = new GlobalFunction();
            UserAccess _numLineUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));
            string _locMessageBody = null;

            try
            {
                if (_locPurchaseRequisitionXPO != null)
                {
                    XPCollection<MailSetupDetail> _locMailSetDetails = new XPCollection<MailSetupDetail>
                                                                       (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ObjectList", ObjectList.PurchaseRequisition),
                                                                        new BinaryOperator("UserAccess", _numLineUserAccess),
                                                                        new BinaryOperator("Active", true)));

                    if (_locMailSetDetails != null && _locMailSetDetails.Count > 0)
                    {
                        foreach (MailSetupDetail _locMailSetDetail in _locMailSetDetails)
                        {
                            if (_locMailSetDetail.MessageBody != null)
                            {
                                if (_locApprovalLineXPO2.EndApproval == true)
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locPurchaseRequisitionXPO, _locMailSetDetail);
                                }
                                else
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locPurchaseRequisitionXPO, _locMailSetDetail);
                                }
                            }

                            _globFunc.SetAndSendMail(_locMailSetDetail.SmtpHost, _locMailSetDetail.SmtpPort,
                            _locMailSetDetail.MailFrom, _locMailSetDetail.MailFromPassword, _locMailSetDetail.MailTo, _locMailSetDetail.MailSubject, _locMessageBody);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseRequisition " + ex.ToString());
            }
        }

        #endregion Email

        #region PostingOld

        private void SetRemainQuantityOld(Session _currSession, PurchaseRequisitionLine _locPurchaseRequisitionLine)
        {
            try
            {
                DateTime now = DateTime.Now;

                #region Remain Qty  with PostedCount != 0
                if (_locPurchaseRequisitionLine.PostedCount > 0)
                {
                    double _locRmDqty = 0;
                    double _locRmQty = 0;
                    double _locInvLineTotal = 0;
                    double _locRmTQty = 0;
                    bool _locActivationPosting = false;
                    Status _locStatus = Status.Posted;

                    if (_locPurchaseRequisitionLine.RmDQty > 0 || _locPurchaseRequisitionLine.RmQty > 0 && _locPurchaseRequisitionLine.MxTQty != _locPurchaseRequisitionLine.RmTQty)
                    {
                        _locRmDqty = _locPurchaseRequisitionLine.RmDQty - _locPurchaseRequisitionLine.DQty;
                        _locRmQty = _locPurchaseRequisitionLine.RmQty - _locPurchaseRequisitionLine.Qty;

                        if (_locPurchaseRequisitionLine.Item != null && _locPurchaseRequisitionLine.MxUOM != null && _locPurchaseRequisitionLine.MxDUOM != null)
                        {
                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("Item", _locPurchaseRequisitionLine.Item),
                                                     new BinaryOperator("UOM", _locPurchaseRequisitionLine.MxUOM),
                                                     new BinaryOperator("DefaultUOM", _locPurchaseRequisitionLine.MxDUOM),
                                                     new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDqty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDqty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDqty;
                                }
                            }
                        }
                        else
                        {
                            _locInvLineTotal = _locRmQty + _locRmDqty;
                        }

                        if (_locRmDqty > 0 || _locRmQty > 0)
                        {
                            _locRmTQty = _locPurchaseRequisitionLine.RmTQty - _locPurchaseRequisitionLine.TQty;
                        }
                        else
                        {
                            _locRmTQty = _locPurchaseRequisitionLine.RmTQty - _locPurchaseRequisitionLine.TQty;
                            _locActivationPosting = true;
                            _locStatus = Status.Close;
                        }


                        _locPurchaseRequisitionLine.ActivationPosting = _locActivationPosting;
                        _locPurchaseRequisitionLine.Select = false;
                        _locPurchaseRequisitionLine.RmDQty = _locRmDqty;
                        _locPurchaseRequisitionLine.RmQty = _locRmQty;
                        _locPurchaseRequisitionLine.RmTQty = _locRmTQty;
                        _locPurchaseRequisitionLine.PostedCount = _locPurchaseRequisitionLine.PostedCount + 1;
                        _locPurchaseRequisitionLine.DQty = 0;
                        _locPurchaseRequisitionLine.Qty = 0;
                        _locPurchaseRequisitionLine.TQty = 0;
                        _locPurchaseRequisitionLine.Status = _locStatus;
                        _locPurchaseRequisitionLine.StatusDate = now;
                        _locPurchaseRequisitionLine.Save();
                        _locPurchaseRequisitionLine.Session.CommitTransaction();
                    }
                }
                #endregion Remain Qty  with PostedCount != 0

                #region Remain Qty with PostedCount == 0
                else
                {
                    double _locRmDqty = 0;
                    double _locRmQty = 0;
                    double _locInvLineTotal = 0;
                    double _locRmTQty = 0;
                    _locRmDqty = _locPurchaseRequisitionLine.MxDQty - _locPurchaseRequisitionLine.DQty;
                    _locRmQty = _locPurchaseRequisitionLine.MxQty - _locPurchaseRequisitionLine.Qty;

                    #region Remain > 0
                    if (_locRmDqty > 0 || _locRmQty > 0)
                    {
                        if (_locPurchaseRequisitionLine.Item != null && _locPurchaseRequisitionLine.MxUOM != null && _locPurchaseRequisitionLine.MxDUOM != null)
                        {
                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("Item", _locPurchaseRequisitionLine.Item),
                                                     new BinaryOperator("UOM", _locPurchaseRequisitionLine.MxUOM),
                                                     new BinaryOperator("DefaultUOM", _locPurchaseRequisitionLine.MxDUOM),
                                                     new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDqty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDqty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDqty;
                                }
                            }
                        }
                        else
                        {
                            _locInvLineTotal = _locRmQty + _locRmDqty;
                        }

                        if (_locInvLineTotal > 0)
                        {
                            if (_locPurchaseRequisitionLine.RmTQty > 0)
                            {
                                _locRmTQty = _locPurchaseRequisitionLine.RmTQty - _locInvLineTotal;
                            }
                            else
                            {
                                _locRmTQty = _locInvLineTotal;
                            }

                        }

                        _locPurchaseRequisitionLine.RmDQty = _locRmDqty;
                        _locPurchaseRequisitionLine.RmQty = _locRmQty;
                        _locPurchaseRequisitionLine.RmTQty = _locRmTQty;
                        _locPurchaseRequisitionLine.PostedCount = _locPurchaseRequisitionLine.PostedCount + 1;
                        _locPurchaseRequisitionLine.Select = false;
                        _locPurchaseRequisitionLine.DQty = 0;
                        _locPurchaseRequisitionLine.Qty = 0;
                        _locPurchaseRequisitionLine.TQty = 0;
                        _locPurchaseRequisitionLine.Status = Status.Posted;
                        _locPurchaseRequisitionLine.StatusDate = now;
                        _locPurchaseRequisitionLine.Save();
                        _locPurchaseRequisitionLine.Session.CommitTransaction();
                    }
                    #endregion Remain > 0

                    #region Remain <= 0
                    if (_locRmDqty <= 0 && _locRmQty <= 0)
                    {
                        _locPurchaseRequisitionLine.ActivationPosting = true;
                        _locPurchaseRequisitionLine.RmDQty = 0;
                        _locPurchaseRequisitionLine.RmQty = 0;
                        _locPurchaseRequisitionLine.RmTQty = 0;
                        _locPurchaseRequisitionLine.PostedCount = _locPurchaseRequisitionLine.PostedCount + 1;
                        _locPurchaseRequisitionLine.Select = false;
                        _locPurchaseRequisitionLine.DQty = 0;
                        _locPurchaseRequisitionLine.Qty = 0;
                        _locPurchaseRequisitionLine.TQty = 0;
                        _locPurchaseRequisitionLine.Status = Status.Close;
                        _locPurchaseRequisitionLine.StatusDate = now;
                        _locPurchaseRequisitionLine.Save();
                        _locPurchaseRequisitionLine.Session.CommitTransaction();
                    }
                    #endregion Remain < 0
                }
                #endregion Remain Qty with PostedCount == 0

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PrePurchaseOrder " + ex.ToString());
            }
        }

        private void SetFinalStatusOld(Session _currSession, PurchaseRequisition _locPurchaseRequisitionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                Status _locStatus = Status.Posted;
                int _locCountStatus = 0;
                bool _locActivationPosting = false;

                if (_locPurchaseRequisitionXPO != null)
                {
                    XPCollection<PurchaseRequisitionLine> _locPurchaseRequisitionLines = new XPCollection<PurchaseRequisitionLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO)));
                    if (_locPurchaseRequisitionLines != null && _locPurchaseRequisitionLines.Count() > 0)
                    {
                        foreach (PurchaseRequisitionLine _locPurchaseRequisitionLine in _locPurchaseRequisitionLines)
                        {
                            if (_locPurchaseRequisitionLine.Status == Status.Close)
                            {
                                _locCountStatus = _locCountStatus + 1;
                            }
                        }
                    }

                    if (_locCountStatus > 0)
                    {
                        if (_locPurchaseRequisitionLines.Count() == _locCountStatus)
                        {
                            _locActivationPosting = true;
                            _locStatus = Status.Close;
                        }
                    }

                    _locPurchaseRequisitionXPO.Status = _locStatus;
                    _locPurchaseRequisitionXPO.StatusDate = now;
                    _locPurchaseRequisitionXPO.ActivationPosting = _locActivationPosting;
                    _locPurchaseRequisitionXPO.Save();
                    _locPurchaseRequisitionXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseRequisition " + ex.ToString());
            }
        }

        private void SetPostingPOQtyOld(Session _currSession, PurchaseRequisition _locPurchaseRequisitionXPO)
        {
            try
            {
                if (_locPurchaseRequisitionXPO != null)
                {
                    XPCollection<PurchaseRequisitionLine> _locPurchaseRequisitionLines = new XPCollection<PurchaseRequisitionLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locPurchaseRequisitionLines != null && _locPurchaseRequisitionLines.Count >= 0)
                    {
                        double _locPDQty = 0;
                        double _locPQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (PurchaseRequisitionLine _locPurchaseRequisitionLine in _locPurchaseRequisitionLines)
                        {
                            #region PostedCount=0
                            //if (_locPurchaseRequisitionLine.PostedCount == 0)
                            //{
                            #region Max Quantity
                            if (_locPurchaseRequisitionLine.TQty >= 0)
                            {
                                if (_locPurchaseRequisitionLine.DQty > 0 && _locPurchaseRequisitionLine.DQty <= _locPurchaseRequisitionLine.TQty)
                                {
                                    _locPDQty = _locPurchaseRequisitionLine.DQty;
                                }

                                if (_locPurchaseRequisitionLine.Qty > 0 && _locPurchaseRequisitionLine.Qty <= _locPurchaseRequisitionLine.TQty)
                                {
                                    _locPQty = _locPurchaseRequisitionLine.Qty;
                                }

                                _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                           new BinaryOperator("Item", _locPurchaseRequisitionLine.Item),
                                                           new BinaryOperator("UOM", _locPurchaseRequisitionLine.MxUOM),
                                                           new BinaryOperator("DefaultUOM", _locPurchaseRequisitionLine.MxDUOM),
                                                           new BinaryOperator("Active", true)));
                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                //}
                                #endregion Max Quantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locPurchaseRequisitionLine.DQty > 0)
                                    {
                                        _locPDQty = _locPurchaseRequisitionLine.DQty;
                                    }

                                    if (_locPurchaseRequisitionLine.Qty > 0)
                                    {
                                        _locPQty = _locPurchaseRequisitionLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locPurchaseRequisitionLine.Item),
                                                                new BinaryOperator("UOM", _locPurchaseRequisitionLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locPurchaseRequisitionLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion PostedCount=0

                            #region PostedCount>0
                            //if (_locPurchaseRequisitionLine.PostedCount > 0)
                            //{
                            //    if (_locPurchaseRequisitionLine.PDQty > 0)
                            //    {
                            //        _locPDQty = _locPurchaseRequisitionLine.PDQty + _locPurchaseRequisitionLine.DQty;
                            //    }

                            //    if (_locPurchaseRequisitionLine.PQty > 0)
                            //    {
                            //        _locPQty = _locPurchaseRequisitionLine.PQty + _locPurchaseRequisitionLine.Qty;
                            //    }

                            //    if (_locPurchaseRequisitionLine.MxDQty > 0 || _locPurchaseRequisitionLine.MxQty > 0)
                            //    {
                            //        _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                            //                               (new GroupOperator(GroupOperatorType.And,
                            //                                new BinaryOperator("Item", _locPurchaseRequisitionLine.Item),
                            //                                new BinaryOperator("UOM", _locPurchaseRequisitionLine.MxUOM),
                            //                                new BinaryOperator("DefaultUOM", _locPurchaseRequisitionLine.MxDUOM),
                            //                                new BinaryOperator("Active", true)));
                            //    }
                            //    else
                            //    {
                            //        _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                            //                               (new GroupOperator(GroupOperatorType.And,
                            //                                new BinaryOperator("Item", _locPurchaseRequisitionLine.Item),
                            //                                new BinaryOperator("UOM", _locPurchaseRequisitionLine.UOM),
                            //                                new BinaryOperator("DefaultUOM", _locPurchaseRequisitionLine.DUOM),
                            //                                new BinaryOperator("Active", true)));
                            //    }

                            //    if (_locItemUOM != null)
                            //    {
                            //        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                            //        {
                            //            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                            //        }
                            //        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                            //        {
                            //            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                            //        }
                            //        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                            //        {
                            //            _locInvLineTotal = _locPQty + _locPDQty;
                            //        }
                            //    }
                            //    else
                            //    {
                            //        _locInvLineTotal = _locPQty + _locPDQty;
                            //    }

                            ////}
                            #endregion PostedCount>0

                            _locPurchaseRequisitionLine.PDQty = _locPDQty;
                            _locPurchaseRequisitionLine.PDUOM = _locPurchaseRequisitionLine.DUOM;
                            _locPurchaseRequisitionLine.PQty = _locPQty;
                            _locPurchaseRequisitionLine.PUOM = _locPurchaseRequisitionLine.UOM;
                            _locPurchaseRequisitionLine.PTQty = _locInvLineTotal;
                            _locPurchaseRequisitionLine.Save();
                            _locPurchaseRequisitionLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseRequisition", ex.ToString());
            }
        }

        private void PurchaseRequisitionPostingAction_ExecuteOld(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                string _currSignCode = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseRequisition _locPurchaseRequisitionOS = (PurchaseRequisition)_objectSpace.GetObject(obj);

                        if (_locPurchaseRequisitionOS != null)
                        {
                            if (_locPurchaseRequisitionOS.Code != null)
                            {
                                _currObjectId = _locPurchaseRequisitionOS.Code;
                                PurchaseOrder _locPurchaseOrderByPR = null;
                                PurchaseOrder _locPurchaseOrderByPRandPH = null;

                                PurchaseRequisition _locPurchaseRequisitionXPO = _currSession.FindObject<PurchaseRequisition>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locPurchaseRequisitionXPO != null)
                                {
                                    if (_locPurchaseRequisitionXPO.Status == Status.Progress || _locPurchaseRequisitionXPO.Status == Status.Posted)
                                    {

                                        ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("EndApproval", true),
                                                                    new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO)));

                                        if (_locApprovalLineXPO != null)
                                        {
                                            _locPurchaseOrderByPR = _currSession.FindObject<PurchaseOrder>(new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO)));

                                            if (_locPurchaseRequisitionXPO.ProjectHeader != null)
                                            {
                                                _locPurchaseOrderByPRandPH = _currSession.FindObject<PurchaseOrder>(new GroupOperator(GroupOperatorType.And,
                                                                           new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO),
                                                                           new BinaryOperator("ProjectHeader", _locPurchaseRequisitionXPO.ProjectHeader)));
                                            }

                                            if (_locPurchaseOrderByPR == null && _locPurchaseOrderByPRandPH == null)
                                            {
                                                _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.PurchaseOrder);

                                                if (_currSignCode != null)
                                                {
                                                    PurchaseOrder _saveDataPO = new PurchaseOrder(_currSession)
                                                    {
                                                        TransferType = _locPurchaseRequisitionXPO.TransferType,
                                                        SignCode = _currSignCode,
                                                        ProjectHeader = _locPurchaseRequisitionXPO.ProjectHeader,
                                                        Company = _locPurchaseRequisitionXPO.Company,
                                                        PurchaseRequisition = _locPurchaseRequisitionXPO,
                                                    };
                                                    _saveDataPO.Save();
                                                    _saveDataPO.Session.CommitTransaction();

                                                    XPCollection<PurchaseRequisitionLine> _numLinePurchaseRequisitionLines = new XPCollection<PurchaseRequisitionLine>(_currSession,
                                                                                                            new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO),
                                                                                                            new BinaryOperator("Select", true)));

                                                    PurchaseOrder _locPurchaseOrder2 = _currSession.FindObject<PurchaseOrder>(new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("SignCode", _currSignCode)));
                                                    if (_locPurchaseOrder2 != null)
                                                    {
                                                        if (_numLinePurchaseRequisitionLines != null && _numLinePurchaseRequisitionLines.Count > 0)
                                                        {
                                                            foreach (PurchaseRequisitionLine _numLinePurchaseRequisitionLine in _numLinePurchaseRequisitionLines)
                                                            {
                                                                if (_numLinePurchaseRequisitionLine.Status == Status.Progress || _numLinePurchaseRequisitionLine.Status == Status.Posted)
                                                                {
                                                                    PurchaseOrderLine _saveDataPurchaseOrderLine = new PurchaseOrderLine(_currSession)
                                                                    {
                                                                        PurchaseType = _numLinePurchaseRequisitionLine.PurchaseType,
                                                                        Item = _numLinePurchaseRequisitionLine.Item,
                                                                        Description = _numLinePurchaseRequisitionLine.Description,
                                                                        MxDQty = _numLinePurchaseRequisitionLine.DQty,
                                                                        MxDUOM = _numLinePurchaseRequisitionLine.DUOM,
                                                                        MxQty = _numLinePurchaseRequisitionLine.Qty,
                                                                        MxUOM = _numLinePurchaseRequisitionLine.UOM,
                                                                        //wahab
                                                                        MxTQty = _numLinePurchaseRequisitionLine.TQty,
                                                                        MxUAmount = _numLinePurchaseRequisitionLine.MxUAmount,
                                                                        MxTUAmount = _numLinePurchaseRequisitionLine.TQty * _numLinePurchaseRequisitionLine.MxUAmount,
                                                                        DQty = _numLinePurchaseRequisitionLine.DQty,
                                                                        DUOM = _numLinePurchaseRequisitionLine.DUOM,
                                                                        Qty = _numLinePurchaseRequisitionLine.Qty,
                                                                        UOM = _numLinePurchaseRequisitionLine.UOM,
                                                                        TQty = _numLinePurchaseRequisitionLine.TQty,
                                                                        Company = _numLinePurchaseRequisitionLine.Company,
                                                                        PurchaseOrder = _locPurchaseOrder2,
                                                                    };
                                                                    _saveDataPurchaseOrderLine.Save();
                                                                    _saveDataPurchaseOrderLine.Session.CommitTransaction();

                                                                    //SetPostingPOQty(_currSession, _locPurchaseRequisitionXPO);
                                                                    //SetRemainQuantity(_currSession, _numLinePurchaseRequisitionLine);

                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            //SetFinalStatus(_currSession, _locPurchaseRequisitionXPO);
                                            //SuccessMessageShow("PO has successfully created");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Order Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Purchase Order Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseRequisition " + ex.ToString());
            }
        }

        private void SetPurchaseOrder(Session _currSession, PurchaseRequisition _locPurchaseRequisitionXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                string _currSignCode = null;
                DateTime now = DateTime.Now;

                if (_locPurchaseRequisitionXPO != null)
                {
                    _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.PurchaseOrder);

                    if (_currSignCode != null)
                    {
                        PurchaseOrder _saveDataPO = new PurchaseOrder(_currSession)
                        {
                            TransferType = _locPurchaseRequisitionXPO.TransferType,
                            SignCode = _currSignCode,
                            ProjectHeader = _locPurchaseRequisitionXPO.ProjectHeader,
                            Company = _locPurchaseRequisitionXPO.Company,
                            PurchaseRequisition = _locPurchaseRequisitionXPO,
                        };
                        _saveDataPO.Save();
                        _saveDataPO.Session.CommitTransaction();

                        XPCollection<PurchaseRequisitionLine> _numLinePurchaseRequisitionLines = new XPCollection<PurchaseRequisitionLine>(_currSession,
                                                                                new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO),
                                                                                new BinaryOperator("Select", true)));

                        PurchaseOrder _locPurchaseOrder2 = _currSession.FindObject<PurchaseOrder>(new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SignCode", _currSignCode)));
                        if (_locPurchaseOrder2 != null)
                        {
                            if (_numLinePurchaseRequisitionLines != null && _numLinePurchaseRequisitionLines.Count > 0)
                            {
                                foreach (PurchaseRequisitionLine _numLinePurchaseRequisitionLine in _numLinePurchaseRequisitionLines)
                                {
                                    if (_numLinePurchaseRequisitionLine.Status == Status.Progress || _numLinePurchaseRequisitionLine.Status == Status.Posted)
                                    {
                                        PurchaseOrderLine _saveDataPurchaseOrderLine = new PurchaseOrderLine(_currSession)
                                        {
                                            PurchaseType = _numLinePurchaseRequisitionLine.PurchaseType,
                                            Item = _numLinePurchaseRequisitionLine.Item,
                                            Description = _numLinePurchaseRequisitionLine.Description,
                                            MxDQty = _numLinePurchaseRequisitionLine.DQty,
                                            MxDUOM = _numLinePurchaseRequisitionLine.DUOM,
                                            MxQty = _numLinePurchaseRequisitionLine.Qty,
                                            MxUOM = _numLinePurchaseRequisitionLine.UOM,
                                            //wahab
                                            MxTQty = _numLinePurchaseRequisitionLine.TQty,
                                            MxUAmount = _numLinePurchaseRequisitionLine.MxUAmount,
                                            MxTUAmount = _numLinePurchaseRequisitionLine.TQty * _numLinePurchaseRequisitionLine.MxUAmount,
                                            DQty = _numLinePurchaseRequisitionLine.DQty,
                                            DUOM = _numLinePurchaseRequisitionLine.DUOM,
                                            Qty = _numLinePurchaseRequisitionLine.Qty,
                                            UOM = _numLinePurchaseRequisitionLine.UOM,
                                            TQty = _numLinePurchaseRequisitionLine.TQty,
                                            Company = _numLinePurchaseRequisitionLine.Company,
                                            PurchaseOrder = _locPurchaseOrder2,
                                        };
                                        _saveDataPurchaseOrderLine.Save();
                                        _saveDataPurchaseOrderLine.Session.CommitTransaction();

                                    }
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseRequisition ", ex.ToString());
            }
        }

        private void SetRemainQty(Session _currSession, PurchaseRequisition _locPurchaseRequisitionXPO)
        {
            try
            {
                if (_locPurchaseRequisitionXPO != null)
                {
                    XPCollection<PurchaseRequisitionLine> _locPurchaseRequisitionLines = new XPCollection<PurchaseRequisitionLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locPurchaseRequisitionLines != null && _locPurchaseRequisitionLines.Count > 0)
                    {
                        double _locRmDQty = 0;
                        double _locRmQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (PurchaseRequisitionLine _locPurchaseRequisitionLine in _locPurchaseRequisitionLines)
                        {
                            #region ProcessCount=0
                            if (_locPurchaseRequisitionLine.PostedCount == 0)
                            {
                                #region MaxQuantity
                                if (_locPurchaseRequisitionLine.MxDQty > 0)
                                {
                                    if (_locPurchaseRequisitionLine.DQty > 0 && _locPurchaseRequisitionLine.DQty <= _locPurchaseRequisitionLine.MxDQty)
                                    {
                                        _locRmDQty = _locPurchaseRequisitionLine.MxDQty - _locPurchaseRequisitionLine.DQty;
                                    }

                                    if (_locPurchaseRequisitionLine.Qty > 0 && _locPurchaseRequisitionLine.Qty <= _locPurchaseRequisitionLine.MxQty)
                                    {
                                        _locRmQty = _locPurchaseRequisitionLine.MxQty - _locPurchaseRequisitionLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locPurchaseRequisitionLine.Item),
                                                                new BinaryOperator("UOM", _locPurchaseRequisitionLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locPurchaseRequisitionLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locPurchaseRequisitionLine.DQty > 0)
                                    {
                                        _locRmDQty = 0;
                                    }

                                    if (_locPurchaseRequisitionLine.Qty > 0)
                                    {
                                        _locRmQty = 0;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locPurchaseRequisitionLine.Item),
                                                                new BinaryOperator("UOM", _locPurchaseRequisitionLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locPurchaseRequisitionLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locPurchaseRequisitionLine.PostedCount > 0)
                            {
                                if (_locPurchaseRequisitionLine.RmDQty > 0)
                                {
                                    _locRmDQty = _locPurchaseRequisitionLine.RmDQty - _locPurchaseRequisitionLine.DQty;
                                }

                                if (_locPurchaseRequisitionLine.RmQty > 0)
                                {
                                    _locRmQty = _locPurchaseRequisitionLine.RmQty - _locPurchaseRequisitionLine.Qty;
                                }

                                if (_locPurchaseRequisitionLine.MxDQty > 0 || _locPurchaseRequisitionLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locPurchaseRequisitionLine.Item),
                                                                new BinaryOperator("UOM", _locPurchaseRequisitionLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locPurchaseRequisitionLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locPurchaseRequisitionLine.Item),
                                                                new BinaryOperator("UOM", _locPurchaseRequisitionLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locPurchaseRequisitionLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                }


                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locPurchaseRequisitionLine.RmDQty = _locRmDQty;
                            _locPurchaseRequisitionLine.RmQty = _locRmQty;
                            _locPurchaseRequisitionLine.RmTQty = _locInvLineTotal;
                            _locPurchaseRequisitionLine.Save();
                            _locPurchaseRequisitionLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseRequisition " + ex.ToString());
            }
        }

        private void SetPostingQty(Session _currSession, PurchaseRequisition _locPurchaseRequisitionXPO)
        {
            try
            {
                if (_locPurchaseRequisitionXPO != null)
                {
                    XPCollection<PurchaseRequisitionLine> _locPurchaseRequisitionLines = new XPCollection<PurchaseRequisitionLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locPurchaseRequisitionLines != null && _locPurchaseRequisitionLines.Count > 0)
                    {
                        double _locPDQty = 0;
                        double _locPQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (PurchaseRequisitionLine _locPurchaseRequisitionLine in _locPurchaseRequisitionLines)
                        {
                            #region ProcessCount=0
                            if (_locPurchaseRequisitionLine.PostedCount == 0)
                            {
                                #region MaxQuantity
                                if (_locPurchaseRequisitionLine.MxDQty > 0)
                                {
                                    if (_locPurchaseRequisitionLine.DQty > 0 && _locPurchaseRequisitionLine.DQty <= _locPurchaseRequisitionLine.MxDQty)
                                    {
                                        _locPDQty = _locPurchaseRequisitionLine.DQty;
                                    }

                                    if (_locPurchaseRequisitionLine.Qty > 0 && _locPurchaseRequisitionLine.Qty <= _locPurchaseRequisitionLine.MxQty)
                                    {
                                        _locPQty = _locPurchaseRequisitionLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locPurchaseRequisitionLine.Item),
                                                                new BinaryOperator("UOM", _locPurchaseRequisitionLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locPurchaseRequisitionLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locPurchaseRequisitionLine.DQty > 0)
                                    {
                                        _locPDQty = _locPurchaseRequisitionLine.DQty;
                                    }

                                    if (_locPurchaseRequisitionLine.Qty > 0)
                                    {
                                        _locPQty = _locPurchaseRequisitionLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locPurchaseRequisitionLine.Item),
                                                                new BinaryOperator("UOM", _locPurchaseRequisitionLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locPurchaseRequisitionLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locPurchaseRequisitionLine.PostedCount > 0)
                            {
                                if (_locPurchaseRequisitionLine.PDQty > 0)
                                {
                                    _locPDQty = _locPurchaseRequisitionLine.PDQty + _locPurchaseRequisitionLine.DQty;
                                }

                                if (_locPurchaseRequisitionLine.PQty > 0)
                                {
                                    _locPQty = _locPurchaseRequisitionLine.PQty + _locPurchaseRequisitionLine.Qty;
                                }

                                if (_locPurchaseRequisitionLine.MxDQty > 0 || _locPurchaseRequisitionLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locPurchaseRequisitionLine.Item),
                                                            new BinaryOperator("UOM", _locPurchaseRequisitionLine.MxUOM),
                                                            new BinaryOperator("DefaultUOM", _locPurchaseRequisitionLine.MxDUOM),
                                                            new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locPurchaseRequisitionLine.Item),
                                                            new BinaryOperator("UOM", _locPurchaseRequisitionLine.UOM),
                                                            new BinaryOperator("DefaultUOM", _locPurchaseRequisitionLine.DUOM),
                                                            new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locPurchaseRequisitionLine.PDQty = _locPDQty;
                            _locPurchaseRequisitionLine.PDUOM = _locPurchaseRequisitionLine.DUOM;
                            _locPurchaseRequisitionLine.PQty = _locPQty;
                            _locPurchaseRequisitionLine.PUOM = _locPurchaseRequisitionLine.UOM;
                            _locPurchaseRequisitionLine.PTQty = _locInvLineTotal;
                            _locPurchaseRequisitionLine.Save();
                            _locPurchaseRequisitionLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseRequisition " + ex.ToString());
            }

        }

        private void SetProcessCount(Session _currSession, PurchaseRequisition _locPurchaseRequisitionXPO)
        {
            try
            {
                if (_locPurchaseRequisitionXPO != null)
                {
                    XPCollection<PurchaseRequisitionLine> _locPurchaseRequisitionLines = new XPCollection<PurchaseRequisitionLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locPurchaseRequisitionLines != null && _locPurchaseRequisitionLines.Count > 0)
                    {

                        foreach (PurchaseRequisitionLine _locPurchaseRequisitionLine in _locPurchaseRequisitionLines)
                        {
                            if (_locPurchaseRequisitionLine.Status == Status.Progress || _locPurchaseRequisitionLine.Status == Status.Posted)
                            {
                                _locPurchaseRequisitionLine.PostedCount = _locPurchaseRequisitionLine.PostedCount + 1;
                                _locPurchaseRequisitionLine.Save();
                                _locPurchaseRequisitionLine.Session.CommitTransaction();
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseRequisition " + ex.ToString());
            }
        }

        private void SetStatusPurchaseRequisitionLine(Session _currSession, PurchaseRequisition _locPurchaseRequisitionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locPurchaseRequisitionXPO != null)
                {
                    XPCollection<PurchaseRequisitionLine> _locPurchaseRequisitionLines = new XPCollection<PurchaseRequisitionLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locPurchaseRequisitionLines != null && _locPurchaseRequisitionLines.Count > 0)
                    {

                        foreach (PurchaseRequisitionLine _locPurchaseRequisitionLine in _locPurchaseRequisitionLines)
                        {
                            if (_locPurchaseRequisitionLine.Status == Status.Progress || _locPurchaseRequisitionLine.Status == Status.Posted)
                            {
                                if (_locPurchaseRequisitionLine.RmDQty == 0 && _locPurchaseRequisitionLine.RmQty == 0 && _locPurchaseRequisitionLine.RmTQty == 0)
                                {
                                    _locPurchaseRequisitionLine.Status = Status.Close;
                                    _locPurchaseRequisitionLine.ActivationPosting = true;
                                    _locPurchaseRequisitionLine.ActivationQuantity = true;
                                    _locPurchaseRequisitionLine.SelectEnabled = true;
                                    _locPurchaseRequisitionLine.StatusDate = now;
                                }
                                else
                                {
                                    _locPurchaseRequisitionLine.Status = Status.Posted;
                                    _locPurchaseRequisitionLine.StatusDate = now;
                                }
                                _locPurchaseRequisitionLine.Save();
                                _locPurchaseRequisitionLine.Session.CommitTransaction();
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseRequisition " + ex.ToString());
            }
        }

        private void SetNormalQuantity(Session _currSession, PurchaseRequisition _locPurchaseRequisitionXPO)
        {
            try
            {
                if (_locPurchaseRequisitionXPO != null)
                {
                    XPCollection<PurchaseRequisitionLine> _locPurchaseRequisitionLines = new XPCollection<PurchaseRequisitionLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO),
                                                                                     new BinaryOperator("Select", true)));

                    if (_locPurchaseRequisitionLines != null && _locPurchaseRequisitionLines.Count > 0)
                    {
                        foreach (PurchaseRequisitionLine _locSalesQuotationLine in _locPurchaseRequisitionLines)
                        {
                            if (_locSalesQuotationLine.Status == Status.Progress || _locSalesQuotationLine.Status == Status.Posted || _locSalesQuotationLine.Status == Status.Close)
                            {
                                if (_locSalesQuotationLine.DQty > 0 || _locSalesQuotationLine.Qty > 0)
                                {
                                    _locSalesQuotationLine.Select = false;
                                    _locSalesQuotationLine.DQty = 0;
                                    _locSalesQuotationLine.Qty = 0;
                                    _locSalesQuotationLine.Save();
                                    _locSalesQuotationLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseRequisition " + ex.ToString());
            }
        }

        private void SetFinalStatusPurchaseRequisition(Session _currSession, PurchaseRequisition _locPurchaseRequisitionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locPurchaseRequisitionLineCount = 0;

                if (_locPurchaseRequisitionXPO != null)
                {

                    XPCollection<PurchaseRequisitionLine> _locPurchaseRequisitionLines = new XPCollection<PurchaseRequisitionLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO)));

                    if (_locPurchaseRequisitionLines != null && _locPurchaseRequisitionLines.Count() > 0)
                    {
                        _locPurchaseRequisitionLineCount = _locPurchaseRequisitionLines.Count();

                        foreach (PurchaseRequisitionLine _locPurchaseRequisitionLine in _locPurchaseRequisitionLines)
                        {
                            if (_locPurchaseRequisitionLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }
                    }

                    if (_locStatusCount == _locPurchaseRequisitionLineCount)
                    {
                        _locPurchaseRequisitionXPO.ActivationPosting = true;
                        _locPurchaseRequisitionXPO.Status = Status.Close;
                        _locPurchaseRequisitionXPO.StatusDate = now;
                        _locPurchaseRequisitionXPO.Save();
                        _locPurchaseRequisitionXPO.Session.CommitTransaction();
                    }
                    else
                    {
                        _locPurchaseRequisitionXPO.Status = Status.Posted;
                        _locPurchaseRequisitionXPO.StatusDate = now;
                        _locPurchaseRequisitionXPO.Save();
                        _locPurchaseRequisitionXPO.Session.CommitTransaction();
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseRequisition " + ex.ToString());
            }
        }

        #endregion PostingOld

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }


        #endregion Global Method

        
    }
}
