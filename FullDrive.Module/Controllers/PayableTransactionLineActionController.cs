﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PayableTransactionLineActionController : ViewController
    {
        #region Default

        private ChoiceActionItem _selectionListviewFilter;
        public PayableTransactionLineActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.

            #region FilterStatus
            PayableTransactionLineListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                if (_ed.GetCaption(_currApproval) != "Approved" && _ed.GetCaption(_currApproval) != "Lock")
                {
                    _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                }
                PayableTransactionLineListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterStatus
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        private void PayableTransactionLineExchangeRatesAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PayableTransactionLine _locPayableTransactionLineOS = (PayableTransactionLine)_objectSpace.GetObject(obj);

                        if (_locPayableTransactionLineOS != null)
                        {
                            if (_locPayableTransactionLineOS.Debit != 0 && _locPayableTransactionLineOS.Credit != 0)
                            {
                                if (_locPayableTransactionLineOS.Code != null)
                                {
                                    _currObjectId = _locPayableTransactionLineOS.Code;

                                    PayableTransactionLine _locPayableTransactionLineXPO = _currSession.FindObject<PayableTransactionLine>
                                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("Code", _currObjectId)));

                                    if (_locPayableTransactionLineXPO != null)
                                    {
                                        GetChangeRates(_currSession, _locPayableTransactionLineOS);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PayableTransactionLine" + ex.ToString());
            }
        }

        private void PayableTransactionLineListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(PayableTransactionLine)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PayableTransactionLine " + ex.ToString());
            }
        }

        //===== Code Only ====

        #region Method

        private void GetChangeRates(Session _currSession, PayableTransactionLine _PayableTransactionLine)
        {
            try
            {
                XPCollection<ExchangeRate> _locExchangeRates = new XPCollection<ExchangeRate>(_currSession,
                                                               new BinaryOperator("PayableTransactionLine", _PayableTransactionLine));

                if (_locExchangeRates.Count() > 0)
                {
                    foreach (ExchangeRate _locExchangeRate in _locExchangeRates)
                    {
                        if (_locExchangeRate.Rate > 0 && _locExchangeRate.BillDebit > 0 || _locExchangeRate.BillCredit > 0)
                        {
                            PayableTransactionLine _locPayableTransactionLine = _currSession.FindObject<PayableTransactionLine>
                                                                                      (new GroupOperator(GroupOperatorType.And,
                                                                                       new BinaryOperator("Code", _PayableTransactionLine.Code)));

                            if (_PayableTransactionLine != null)
                            {
                                _PayableTransactionLine.Debit = _locExchangeRate.ForeignAmountDebit;
                                _PayableTransactionLine.Credit = _locExchangeRate.ForeignAmountCredit;
                                _PayableTransactionLine.Save();
                                _PayableTransactionLine.Session.CommitTransaction();
                            }
                        }
                        else
                        {
                            PayableTransactionLine _locPayableTransactionLine = _currSession.FindObject<PayableTransactionLine>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                 new BinaryOperator("Code", _PayableTransactionLine.Code)));

                            _PayableTransactionLine.Debit = 0;
                            _PayableTransactionLine.Credit = 0;
                            _PayableTransactionLine.Save();
                            _PayableTransactionLine.Session.CommitTransaction();
                        }
                    }
                }
                else
                {
                    PayableTransactionLine _locPayableTransactionLine = _currSession.FindObject<PayableTransactionLine>
                                                                              (new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("Code", _PayableTransactionLine.Code)));

                    _PayableTransactionLine.Debit = _PayableTransactionLine.Debit;
                    _PayableTransactionLine.Credit = _PayableTransactionLine.Credit;
                    _PayableTransactionLine.Save();
                    _PayableTransactionLine.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PayableTransactionLine" + ex.ToString());
            }
        }

        #endregion Method
        
    }
}
