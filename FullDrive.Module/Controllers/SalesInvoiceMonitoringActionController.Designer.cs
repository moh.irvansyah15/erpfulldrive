﻿namespace FullDrive.Module.Controllers
{
    partial class SalesInvoiceMonitoringActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SalesInvoiceMonitoringSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesInvoiceMonitoringUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // SalesInvoiceMonitoringSelectAction
            // 
            this.SalesInvoiceMonitoringSelectAction.Caption = "Select";
            this.SalesInvoiceMonitoringSelectAction.ConfirmationMessage = null;
            this.SalesInvoiceMonitoringSelectAction.Id = "SalesInvoiceMonitoringSelectActionId";
            this.SalesInvoiceMonitoringSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesInvoiceMonitoring);
            this.SalesInvoiceMonitoringSelectAction.ToolTip = null;
            this.SalesInvoiceMonitoringSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesInvoiceMonitoringSelectAction_Execute);
            // 
            // SalesInvoiceMonitoringUnselectAction
            // 
            this.SalesInvoiceMonitoringUnselectAction.Caption = "Unselect";
            this.SalesInvoiceMonitoringUnselectAction.ConfirmationMessage = null;
            this.SalesInvoiceMonitoringUnselectAction.Id = "SalesInvoiceMonitoringUnselectActionId";
            this.SalesInvoiceMonitoringUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesInvoiceMonitoring);
            this.SalesInvoiceMonitoringUnselectAction.ToolTip = null;
            this.SalesInvoiceMonitoringUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesInvoiceMonitoringUnselectAction_Execute);
            // 
            // SalesInvoiceMonitoringActionController
            // 
            this.Actions.Add(this.SalesInvoiceMonitoringSelectAction);
            this.Actions.Add(this.SalesInvoiceMonitoringUnselectAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction SalesInvoiceMonitoringSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesInvoiceMonitoringUnselectAction;
    }
}
