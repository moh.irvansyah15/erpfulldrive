﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PurchaseInvoiceActionController : ViewController
    {
        #region Default

        private ChoiceActionItem _selectionListviewFilter;
        public PurchaseInvoiceActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            PurchaseInvoiceListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                PurchaseInvoiceListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        private void PurchaseInvoiceProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Status _locStatus = Status.Open;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseInvoice _locPurchaseInvoiceOS = (PurchaseInvoice)_objectSpace.GetObject(obj);

                        if (_locPurchaseInvoiceOS != null)
                        {
                            if (_locPurchaseInvoiceOS.Code != null)
                            {
                                _currObjectId = _locPurchaseInvoiceOS.Code;

                                PurchaseInvoice _locPurchaseInvoiceXPO = _currSession.FindObject<PurchaseInvoice>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locPurchaseInvoiceXPO != null)
                                {
                                    if (_locPurchaseInvoiceXPO.Status == Status.Open || _locPurchaseInvoiceXPO.Status == Status.Progress || _locPurchaseInvoiceXPO.Status == Status.Posted)
                                    {
                                        if(_locPurchaseInvoiceXPO.Status == Status.Open || _locPurchaseInvoiceXPO.Status == Status.Progress)
                                        {
                                            _locStatus = Status.Progress;

                                        }else if(_locPurchaseInvoiceXPO.Status == Status.Posted)
                                        {
                                            _locStatus = Status.Posted;
                                        }

                                        _locPurchaseInvoiceXPO.Status = _locStatus;
                                        _locPurchaseInvoiceXPO.StatusDate = now;
                                        _locPurchaseInvoiceXPO.Save();
                                        _locPurchaseInvoiceXPO.Session.CommitTransaction();

                                        XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>
                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO)));

                                        if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count > 0)
                                        {
                                            foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                                            {
                                                if (_locPurchaseInvoiceLine.Status == Status.Open)
                                                {
                                                    _locPurchaseInvoiceLine.Status = Status.Progress;
                                                    _locPurchaseInvoiceLine.StatusDate = now;
                                                    _locPurchaseInvoiceLine.Save();
                                                    _locPurchaseInvoiceLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        SuccessMessageShow(_locPurchaseInvoiceXPO.Code + " has been change successfully to Progress");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Invoice Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data PurchaseInvoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        private void PurchaseInvoiceGetPayAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                double _locMaxPay = 0;
                double _locTotalAmount = 0;
                double _locPay = 0;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseInvoice _locPurchaseInvoiceOS = (PurchaseInvoice)_objectSpace.GetObject(obj);

                        if (_locPurchaseInvoiceOS != null)
                        {
                            if (_locPurchaseInvoiceOS.Code != null)
                            {
                                _currObjectId = _locPurchaseInvoiceOS.Code;

                                PurchaseInvoice _locPurchaseInvoiceXPO = _currSession.FindObject<PurchaseInvoice>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locPurchaseInvoiceXPO != null)
                                {
                                    _locMaxPay = _locPurchaseInvoiceXPO.Outstanding;

                                    if (_locPurchaseInvoiceXPO.Status == Status.Progress || _locPurchaseInvoiceXPO.Status == Status.Posted)
                                    {
                                        XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>
                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                                new BinaryOperator("Select", true)));

                                        if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count > 0)
                                        {
                                            foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                                            {
                                                if (_locPurchaseInvoiceLine.Status == Status.Progress || _locPurchaseInvoiceLine.Status == Status.Posted)
                                                {
                                                    _locTotalAmount = _locTotalAmount + _locPurchaseInvoiceLine.TAmount;
                                                }
                                            }
                                        }
                                    }

                                    if(_locPurchaseInvoiceXPO.PostedCount > 0)
                                    {
                                        if (_locTotalAmount <= _locMaxPay)
                                        {
                                            _locPay = _locTotalAmount;
                                        }
                                        else
                                        {
                                            _locPay = _locMaxPay;
                                        }
                                    }
                                    else
                                    {
                                        _locPay = _locTotalAmount;
                                    }
                                    
                                    _locPurchaseInvoiceXPO.Pay = _locPay;
                                    _locPurchaseInvoiceXPO.Save();
                                    _locPurchaseInvoiceXPO.Session.CommitTransaction();

                                    SuccessMessageShow(_locPurchaseInvoiceXPO.Code + " Has been change successfully to Get Total Pay");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Invoice Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data Purchase Invoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        private void PurchaseInvoicePostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                //Mengirim informasi ke Payable Transaction --HutangDagang Dan --BankAccount Perusahaan yg aktif default
                //Meng-akumulasi semua hal perhitungan lokal Purchase Invoice termasuk close status

                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseInvoice _locPurchaseInvoiceOS = (PurchaseInvoice)_objectSpace.GetObject(obj);

                        if (_locPurchaseInvoiceOS != null)
                        {
                            if (_locPurchaseInvoiceOS.Code != null)
                            {
                                _currObjectId = _locPurchaseInvoiceOS.Code;

                                PurchaseInvoice _locPurchaseInvoiceXPO = _currSession.FindObject<PurchaseInvoice>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locPurchaseInvoiceXPO != null)
                                {
                                    if (_locPurchaseInvoiceXPO.Status == Status.Progress || _locPurchaseInvoiceXPO.Status == Status.Posted)
                                    {
                                        if (_locPurchaseInvoiceXPO.Pay > 0)
                                        {
                                            if(GetAvailableGeneralJournal(_currSession, _locPurchaseInvoiceXPO) == false )
                                            {
                                                SetReverseGoodsReceiptJournal(_currSession, _locPurchaseInvoiceXPO);
                                                SetInvoiceAPJournal(_currSession, _locPurchaseInvoiceXPO);
                                            }
                                            
                                            SetPayableTransaction(_currSession, _locPurchaseInvoiceXPO);
                                            SetRemainAmount(_currSession, _locPurchaseInvoiceXPO);
                                            SetRemainAmountSplitInvoiceForPIC(_currSession, _locPurchaseInvoiceXPO);
                                            SetAfterPostingReport(_currSession, _locPurchaseInvoiceXPO);
                                            SetRemainPayableQty(_currSession, _locPurchaseInvoiceXPO);
                                            SetPostingPayableQty(_currSession, _locPurchaseInvoiceXPO);
                                            SetProcessPayableCount(_currSession, _locPurchaseInvoiceXPO);
                                            SetStatusPayablePurchaseInvoiceLine(_currSession, _locPurchaseInvoiceXPO);
                                            SetNormalPay(_currSession, _locPurchaseInvoiceXPO);
                                            SetNormalPayableQuantity(_currSession, _locPurchaseInvoiceXPO);
                                            SetFinalPayablePurchaseInvoice(_currSession, _locPurchaseInvoiceXPO);
                                            SuccessMessageShow(_locPurchaseInvoiceXPO.Code + " has been change successfully to Posted");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Invoice Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data PurchaseInvoice Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        private void PurchaseInvoiceCMCalculationAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseInvoice _locPurchaseInvoiceOS = (PurchaseInvoice)_objectSpace.GetObject(obj);

                        if (_locPurchaseInvoiceOS != null)
                        {
                            //if (_locPurchaseInvoiceOS.TQty != 0)
                            //{
                            if (_locPurchaseInvoiceOS.Code != null)
                            {
                                _currObjectId = _locPurchaseInvoiceOS.Code;

                                PurchaseInvoice _locPurchaseInvoiceXPO = _currSession.FindObject<PurchaseInvoice>
                                                                         (new GroupOperator(GroupOperatorType.And,
                                                                          new BinaryOperator("Code", _currObjectId)));

                                if (_locPurchaseInvoiceXPO != null)
                                {
                                    GetSumTotalCM(_currSession, _locPurchaseInvoiceOS);
                                }
                            }
                            //}
                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseInvoice" + ex.ToString());
            }
        }

        private void PurchaseInvoiceListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(PurchaseInvoice)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        #region CLM

        private void GetSumTotalCM(Session _currSession, PurchaseInvoice _purchaseInvoice)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                DateTime now = DateTime.Now;
                double _locTotalCMCL = 0;
                double _locOutstanding = 0;

                if (_purchaseInvoice != null)
                {
                    XPCollection<CreditMemoCollection> _locCMCs = new XPCollection<CreditMemoCollection>(_currSession,
                                                              new BinaryOperator("PurchaseInvoice", _purchaseInvoice));

                    if (_locCMCs != null && _locCMCs.Count() > 0)
                    {
                        foreach (CreditMemoCollection _locCMC in _locCMCs)
                        {
                            if (_locCMC.CM_Amount >= 0)
                            {
                                _locTotalCMCL = _locCMC.CM_Amount;
                            }
                        }
                        if (_purchaseInvoice.PostedCount == 0)
                        {
                            _purchaseInvoice.CM_Amount = _locTotalCMCL;
                            _purchaseInvoice.Save();
                            _purchaseInvoice.Session.CommitTransaction();
                        }
                        if (_purchaseInvoice.PostedCount > 0)
                        {
                            if (_purchaseInvoice.CM_Amount != _locTotalCMCL)
                            {
                                if (_purchaseInvoice.CM_Amount < _locTotalCMCL)
                                {
                                    if (_locOutstanding - (_locTotalCMCL - _purchaseInvoice.CM_Amount) > 0)
                                    {
                                        _locOutstanding = _locOutstanding - (_locTotalCMCL - _purchaseInvoice.CM_Amount);
                                    }
                                    else
                                    {
                                        _locOutstanding = _purchaseInvoice.MaxPay - (_purchaseInvoice.Pay  + _purchaseInvoice.CM_Amount);
                                        _locTotalCMCL = _purchaseInvoice.CM_Amount;
                                    }

                                    _purchaseInvoice.Outstanding = _locOutstanding;
                                    _purchaseInvoice.CM_Amount = _locTotalCMCL;
                                    _purchaseInvoice.Save();
                                    _purchaseInvoice.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                    else
                    {
                        _purchaseInvoice.CM_Amount = 0;
                        _purchaseInvoice.Save();
                        _purchaseInvoice.Session.CommitTransaction();
                    }
                }


            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PurchaseInvoice" + ex.ToString());
            }
        }

        #endregion CLM

        #region PostingMethod

        private bool GetAvailableGeneralJournal(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            bool _result = false;
            try
            {
                XPCollection<GeneralJournal> _locGeneralJournals = new XPCollection<GeneralJournal>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO)));

                if (_locGeneralJournals != null && _locGeneralJournals.Count() > 0)
                {
                    _result = true;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseInvoice ", ex.ToString());
            }

            return _result;
        }

        private void SetReverseGoodsReceiptJournal(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locTotalUnitPrice = 0;

                //Bikin Group by  dan Totalkan jumlah uang pergroup
                if (_locPurchaseInvoiceXPO != null)
                {
                    if (_locPurchaseInvoiceXPO.PurchaseOrder != null)
                    {
                        XPQuery<PurchaseOrderLine> _purchaseOrderLinesQuery = new XPQuery<PurchaseOrderLine>(_currSession);

                        var _purchaseOrderLines = from pil in _purchaseOrderLinesQuery
                                                  where (pil.PurchaseOrder == _locPurchaseInvoiceXPO.PurchaseOrder
                                                  && (pil.Status == Status.Posted || pil.Status == Status.Close))
                                                  group pil by pil.Item.ItemAccountGroup into g
                                                  select new { ItemAccountGroup = g.Key };

                        if (_purchaseOrderLines != null && _purchaseOrderLines.Count() > 0)
                        {
                            foreach (var _purchaseOrderLine in _purchaseOrderLines)
                            {
                                //Menjumlahkan total uang pergroupnya
                                XPCollection<PurchaseOrderLine> _locPurOrderLines = new XPCollection<PurchaseOrderLine>(_currSession,
                                                                                new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("PurchaseOrder", _locPurchaseInvoiceXPO.PurchaseOrder),
                                                                                new BinaryOperator("Item.ItemAccountGroup", _purchaseOrderLine.ItemAccountGroup)));

                                if (_locPurOrderLines != null && _locPurOrderLines.Count > 0)
                                {
                                    #region MenghitungTotalUnitPriceBasedOnItemAccountGroup
                                    foreach (PurchaseOrderLine _locPurOrderLine in _locPurOrderLines)
                                    {
                                        if (_locPurOrderLine.PurchaseType == OrderType.Item)
                                        {
                                            _locTotalUnitPrice = _locTotalUnitPrice + _locPurOrderLine.TUAmount;
                                        }
                                    }
                                    #endregion MenghitungTotalUnitPriceBasedOnItemAccountGroup

                                    #region CreateReverseGoodsReceiptJournal

                                    #region CreateReverseGoodsReceiptJournalByItems
                                    XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("ItemAccountGroup", _purchaseOrderLine.ItemAccountGroup)));

                                    if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                    {
                                        foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                        {
                                            XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("JournalMap", _locJournalMapItem)));

                                            if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                            {
                                                foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                {
                                                    AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                 new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                 new BinaryOperator("OrderType", OrderType.Item),
                                                                                 new BinaryOperator("PostingMethod", PostingMethod.Receipt),
                                                                                 new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                    if (_locAccountMap != null)
                                                    {
                                                        XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("AccountMap", _locAccountMap),
                                                                                        new BinaryOperator("Active", true)));

                                                        if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                        {
                                                            double _locTotalAmountDebit = 0;
                                                            double _locTotalAmountCredit = 0;
                                                            double _locTotalBalance = 0;

                                                            foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                            {
                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                {
                                                                    _locTotalAmountCredit = _locTotalUnitPrice;
                                                                }
                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                {
                                                                    _locTotalAmountDebit = _locTotalUnitPrice;
                                                                }

                                                                GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                {
                                                                    PostingDate = now,
                                                                    PostingType = PostingType.Purchase,
                                                                    PostingMethod = PostingMethod.InvoiceAP,
                                                                    Account = _locAccountMapLine.Account,
                                                                    Debit = _locTotalAmountDebit,
                                                                    Credit = _locTotalAmountCredit,
                                                                    PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                                                                    PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                    Company = _locPurchaseInvoiceXPO.Company,
                                                                };
                                                                _saveGeneralJournal.Save();
                                                                _saveGeneralJournal.Session.CommitTransaction();

                                                                if (_locAccountMapLine.Account.Code != null)
                                                                {
                                                                    ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                    if (_locCOA != null)
                                                                    {
                                                                        if (_locCOA.BalanceType == BalanceType.Change)
                                                                        {
                                                                            if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                            {
                                                                                if (_locTotalAmountDebit > 0)
                                                                                {
                                                                                    _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                }
                                                                                if (_locTotalAmountCredit > 0)
                                                                                {
                                                                                    _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                }
                                                                            }
                                                                            if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                            {
                                                                                if (_locTotalAmountDebit > 0)
                                                                                {
                                                                                    _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                }
                                                                                if (_locTotalAmountCredit > 0)
                                                                                {
                                                                                    _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                }
                                                                            }
                                                                        }

                                                                        _locCOA.Balance = _locTotalBalance;
                                                                        _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                        _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                        _locCOA.Save();
                                                                        _locCOA.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion CreateReverseGoodsReceiptJournalByItems

                                    #region CreateReverseGoodsReceiptJournalByBusinessPartner
                                    if (_locPurchaseInvoiceXPO.BuyFromVendor != null)
                                    {
                                        if (_locPurchaseInvoiceXPO.BuyFromVendor.BusinessPartnerAccountGroup != null)
                                        {
                                            XPCollection<JournalMap> _locJournalMapBusinessPartners = new XPCollection<JournalMap>
                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("BusinessPartnerAccountGroup", _locPurchaseInvoiceXPO.BuyFromVendor.BusinessPartnerAccountGroup)));

                                            if (_locJournalMapBusinessPartners != null && _locJournalMapBusinessPartners.Count() > 0)
                                            {
                                                foreach (JournalMap _locJournalMapBusinessPartner in _locJournalMapBusinessPartners)
                                                {
                                                    XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("JournalMap", _locJournalMapBusinessPartner)));

                                                    if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                    {
                                                        foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                        {
                                                            AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                         (new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                         new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                         new BinaryOperator("OrderType", OrderType.Item),
                                                                                         new BinaryOperator("PostingMethod", PostingMethod.Receipt),
                                                                                         new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                            if (_locAccountMap != null)
                                                            {
                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                new BinaryOperator("Active", true)));

                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                {
                                                                    double _locTotalAmountDebit = 0;
                                                                    double _locTotalAmountCredit = 0;
                                                                    double _locTotalBalance = 0;

                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                    {
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            _locTotalAmountCredit = _locTotalUnitPrice;
                                                                        }
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            _locTotalAmountDebit = _locTotalUnitPrice;
                                                                        }

                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                        {
                                                                            PostingDate = now,
                                                                            PostingType = PostingType.Purchase,
                                                                            PostingMethod = PostingMethod.InvoiceAP,
                                                                            Account = _locAccountMapLine.Account,
                                                                            Debit = _locTotalAmountDebit,
                                                                            Credit = _locTotalAmountCredit,
                                                                            PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                                                                            PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                            Company = _locPurchaseInvoiceXPO.Company,
                                                                        };
                                                                        _saveGeneralJournal.Save();
                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                        {
                                                                            ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                            if (_locCOA != null)
                                                                            {
                                                                                if (_locCOA.BalanceType == BalanceType.Change)
                                                                                {
                                                                                    if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                    if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                }

                                                                                _locCOA.Balance = _locTotalBalance;
                                                                                _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                _locCOA.Save();
                                                                                _locCOA.Session.CommitTransaction();
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion CreateReverseGoodsReceiptJournalByBusinessPartner

                                    #endregion CreateReverseGoodsReceiptJournal

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseInvoice ", ex.ToString());
            }
        }

        private void SetInvoiceAPJournal(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locPayTUAmount = 0;
                double _locTUAmount = 0;
                double _locTxOfAmount = 0;
                double _locDiscOfAmount = 0;

                if (_locPurchaseInvoiceXPO != null)
                {
                    if (_locPurchaseInvoiceXPO.PurchaseOrder != null)
                    {
                        #region CreateInvoiceAPJournal

                        #region CreateInvoiceAPJournalNormal
                        XPQuery<PurchaseOrderLine> _PurchaseOrderLinesQueryBasedNormal = new XPQuery<PurchaseOrderLine>(_currSession);

                        var _PurchaseOrderLineBasedNormals = from pil in _PurchaseOrderLinesQueryBasedNormal
                                                             where (pil.PurchaseOrder == _locPurchaseInvoiceXPO.PurchaseOrder
                                                             && (pil.Status == Status.Posted || pil.Status == Status.Close))
                                                             group pil by pil.Item.ItemAccountGroup into g
                                                             select new { ItemAccountGroup = g.Key };

                        if (_PurchaseOrderLineBasedNormals != null && _PurchaseOrderLineBasedNormals.Count() > 0)
                        {
                            foreach (var _PurchaseOrderLineBasedNormal in _PurchaseOrderLineBasedNormals)
                            {
                                XPCollection<PurchaseOrderLine> _locPurchaseOrderLines = new XPCollection<PurchaseOrderLine>(_currSession,
                                                                                                new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("PurchaseOrder", _locPurchaseInvoiceXPO.PurchaseOrder),
                                                                                                new BinaryOperator("Item.ItemAccountGroup", _PurchaseOrderLineBasedNormal.ItemAccountGroup)));

                                if (_locPurchaseOrderLines != null && _locPurchaseOrderLines.Count() > 0)
                                {
                                    foreach (PurchaseOrderLine _locPurchaseOrderLine in _locPurchaseOrderLines)
                                    {
                                        _locTUAmount = _locTUAmount + _locPurchaseOrderLine.TUAmount;
                                    }
                                }

                                #region PerhitunganPay

                                _locPayTUAmount = _locTUAmount;

                                #endregion PerhitunganPay

                                if (_locPayTUAmount > 0)
                                {
                                    #region JournalMapItemAccountGroup
                                    XPCollection<JournalMap> _locJournalMaps = new XPCollection<JournalMap>
                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("ItemAccountGroup", _PurchaseOrderLineBasedNormal.ItemAccountGroup)));

                                    if (_locJournalMaps != null && _locJournalMaps.Count() > 0)
                                    {
                                        foreach (JournalMap _locJournalMap in _locJournalMaps)
                                        {
                                            XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("JournalMap", _locJournalMap)));

                                            if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                            {
                                                foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                {
                                                    AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                 new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                 new BinaryOperator("OrderType", OrderType.Item),
                                                                                 new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                                                                                 new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                    if (_locAccountMap != null)
                                                    {
                                                        XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                            new BinaryOperator("Active", true)));

                                                        if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                        {
                                                            double _locTotalAmountDebit = 0;
                                                            double _locTotalAmountCredit = 0;
                                                            double _locTotalBalance = 0;

                                                            foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                            {
                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                {
                                                                    _locTotalAmountDebit = _locPayTUAmount;
                                                                }
                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                {
                                                                    _locTotalAmountCredit = _locPayTUAmount;
                                                                }

                                                                GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                {
                                                                    PostingDate = now,
                                                                    PostingType = PostingType.Purchase,
                                                                    PostingMethod = PostingMethod.InvoiceAP,
                                                                    Account = _locAccountMapLine.Account,
                                                                    Debit = _locTotalAmountDebit,
                                                                    Credit = _locTotalAmountCredit,
                                                                    PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                                                                    PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                    Company = _locPurchaseInvoiceXPO.Company,
                                                                };
                                                                _saveGeneralJournal.Save();
                                                                _saveGeneralJournal.Session.CommitTransaction();

                                                                if (_locAccountMapLine.Account.Code != null)
                                                                {
                                                                    ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                    if (_locCOA != null)
                                                                    {
                                                                        if (_locCOA.BalanceType == BalanceType.Change)
                                                                        {
                                                                            if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                            {
                                                                                if (_locTotalAmountDebit > 0)
                                                                                {
                                                                                    _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                }
                                                                                if (_locTotalAmountCredit > 0)
                                                                                {
                                                                                    _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                }
                                                                            }
                                                                            if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                            {
                                                                                if (_locTotalAmountDebit > 0)
                                                                                {
                                                                                    _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                }
                                                                                if (_locTotalAmountCredit > 0)
                                                                                {
                                                                                    _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                }
                                                                            }
                                                                        }

                                                                        _locCOA.Balance = _locTotalBalance;
                                                                        _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                        _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                        _locCOA.Save();
                                                                        _locCOA.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion JournalMapItemAccountGroup

                                    #region JournalMapBusinessPartnerAccountGroup
                                    if (_locPurchaseInvoiceXPO.PayToVendor != null)
                                    {
                                        if (_locPurchaseInvoiceXPO.PayToVendor.BusinessPartnerAccountGroup != null)
                                        {
                                            XPCollection<JournalMap> _locJournalMapByBusinessPartners = new XPCollection<JournalMap>
                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                         new BinaryOperator("BusinessPartnerAccountGroup", _locPurchaseInvoiceXPO.PayToVendor.BusinessPartnerAccountGroup)));

                                            if (_locJournalMapByBusinessPartners != null && _locJournalMapByBusinessPartners.Count() > 0)
                                            {
                                                foreach (JournalMap _locJournalMapByBusinessPartner in _locJournalMapByBusinessPartners)
                                                {
                                                    XPCollection<JournalMapLine> _locJournalMapLineByBusinessPartners = new XPCollection<JournalMapLine>
                                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                         new BinaryOperator("JournalMap", _locJournalMapByBusinessPartner)));

                                                    if (_locJournalMapLineByBusinessPartners != null && _locJournalMapLineByBusinessPartners.Count() > 0)
                                                    {
                                                        foreach (JournalMapLine _locJournalMapLineByBusinessPartner in _locJournalMapLineByBusinessPartners)
                                                        {
                                                            AccountMap _locAccountMapByBusinessPartner = _currSession.FindObject<AccountMap>
                                                                                                         (new GroupOperator(GroupOperatorType.And,
                                                                                                          new BinaryOperator("Code", _locJournalMapLineByBusinessPartner.AccountMap.Code),
                                                                                                          new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                                          new BinaryOperator("OrderType", OrderType.Item),
                                                                                                          new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                                                                                                          new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                            if (_locAccountMapByBusinessPartner != null)
                                                            {
                                                                XPCollection<AccountMapLine> _locAccountMapLineByBusinessPartners = new XPCollection<AccountMapLine>
                                                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                     new BinaryOperator("AccountMap", _locAccountMapByBusinessPartner),
                                                                                                                                     new BinaryOperator("Active", true)));

                                                                if (_locAccountMapLineByBusinessPartners != null && _locAccountMapLineByBusinessPartners.Count() > 0)
                                                                {
                                                                    double _locTotalAmountDebit = 0;
                                                                    double _locTotalAmountCredit = 0;
                                                                    double _locTotalBalance = 0;

                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLineByBusinessPartners)
                                                                    {
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            _locTotalAmountDebit = _locPayTUAmount;
                                                                        }
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            _locTotalAmountCredit = _locPayTUAmount;
                                                                        }

                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                        {
                                                                            PostingDate = now,
                                                                            PostingType = PostingType.Purchase,
                                                                            PostingMethod = PostingMethod.InvoiceAP,
                                                                            Account = _locAccountMapLine.Account,
                                                                            Debit = _locTotalAmountDebit,
                                                                            Credit = _locTotalAmountCredit,
                                                                            PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                                                                            PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                            Company = _locPurchaseInvoiceXPO.Company,
                                                                        };
                                                                        _saveGeneralJournal.Save();
                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                        {
                                                                            ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                            if (_locCOA != null)
                                                                            {
                                                                                if (_locCOA.BalanceType == BalanceType.Change)
                                                                                {
                                                                                    if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                    if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                }

                                                                                _locCOA.Balance = _locTotalBalance;
                                                                                _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                _locCOA.Save();
                                                                                _locCOA.Session.CommitTransaction();
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion JournalMapBusinessPartnerAccountGroup

                                }
                            }
                        }
                        #endregion CreateInvoiceAPJournalNormal

                        #region CreateInvoiceAPWithTax
                        XPQuery<PurchaseOrderLine> _PurchaseOrderLinesQueryBasedTax = new XPQuery<PurchaseOrderLine>(_currSession);

                        var _PurchaseOrderLineBasedTaxs = from pil in _PurchaseOrderLinesQueryBasedTax
                                                          where (pil.PurchaseOrder == _locPurchaseInvoiceXPO.PurchaseOrder
                                                          && (pil.Status == Status.Posted || pil.Status == Status.Close))
                                                          group pil by pil.Tax.TaxAccountGroup into g
                                                          select new { TaxAccountGroup = g.Key };

                        if (_PurchaseOrderLineBasedTaxs != null && _PurchaseOrderLineBasedTaxs.Count() > 0)
                        {
                            foreach (var _PurchaseOrderLineBasedTax in _PurchaseOrderLineBasedTaxs)
                            {
                                XPCollection<PurchaseOrderLine> _locPurchaseOrderLines = new XPCollection<PurchaseOrderLine>(_currSession,
                                                                                                new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("PurchaseOrder", _locPurchaseInvoiceXPO.PurchaseOrder),
                                                                                                new BinaryOperator("Tax.TaxAccountGroup", _PurchaseOrderLineBasedTax.TaxAccountGroup)));

                                if (_locPurchaseOrderLines != null && _locPurchaseOrderLines.Count() > 0)
                                {
                                    foreach (PurchaseOrderLine _locPurchaseOrderLine in _locPurchaseOrderLines)
                                    {
                                        _locTxOfAmount = _locTxOfAmount + _locPurchaseOrderLine.TxAmount;
                                    }
                                }

                                if (_locTxOfAmount > 0)
                                {
                                    #region JournalMapTaxAccountGroup
                                    XPCollection<JournalMap> _locJournalMapByTaxs = new XPCollection<JournalMap>
                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("TaxAccountGroup", _PurchaseOrderLineBasedTax.TaxAccountGroup)));

                                    if (_locJournalMapByTaxs != null && _locJournalMapByTaxs.Count() > 0)
                                    {
                                        foreach (JournalMap _locJournalMapByTax in _locJournalMapByTaxs)
                                        {
                                            XPCollection<JournalMapLine> _locJournalMapLineByTaxs = new XPCollection<JournalMapLine>
                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                     new BinaryOperator("JournalMap", _locJournalMapByTax)));

                                            if (_locJournalMapLineByTaxs != null && _locJournalMapLineByTaxs.Count() > 0)
                                            {
                                                foreach (JournalMapLine _locJournalMapLineByTax in _locJournalMapLineByTaxs)
                                                {
                                                    AccountMap _locAccountMapByTax = _currSession.FindObject<AccountMap>
                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                      new BinaryOperator("Code", _locJournalMapLineByTax.AccountMap.Code),
                                                                                      new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                      new BinaryOperator("OrderType", OrderType.Item),
                                                                                      new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                                                                                      new BinaryOperator("PostingMethodType", PostingMethodType.Tax)));

                                                    if (_locAccountMapByTax != null)
                                                    {
                                                        XPCollection<AccountMapLine> _locAccountMapLineByTaxs = new XPCollection<AccountMapLine>
                                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                 new BinaryOperator("AccountMap", _locAccountMapByTax),
                                                                                                                 new BinaryOperator("Active", true)));

                                                        if (_locAccountMapLineByTaxs != null && _locAccountMapLineByTaxs.Count() > 0)
                                                        {
                                                            double _locTotalAmountDebit = 0;
                                                            double _locTotalAmountCredit = 0;
                                                            double _locTotalBalance = 0;

                                                            foreach (AccountMapLine _locAccountMapLineByTax in _locAccountMapLineByTaxs)
                                                            {
                                                                if (_locAccountMapLineByTax.AccountCharge == AccountCharge.Debit)
                                                                {
                                                                    _locTotalAmountDebit = _locTxOfAmount;
                                                                }
                                                                if (_locAccountMapLineByTax.AccountCharge == AccountCharge.Credit)
                                                                {
                                                                    _locTotalAmountCredit = _locTxOfAmount;
                                                                }

                                                                GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                {
                                                                    PostingDate = now,
                                                                    PostingType = PostingType.Purchase,
                                                                    PostingMethod = PostingMethod.InvoiceAP,
                                                                    Account = _locAccountMapLineByTax.Account,
                                                                    Debit = _locTotalAmountDebit,
                                                                    Credit = _locTotalAmountCredit,
                                                                    PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                                                                    PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                    Company = _locPurchaseInvoiceXPO.Company,
                                                                };
                                                                _saveGeneralJournal.Save();
                                                                _saveGeneralJournal.Session.CommitTransaction();

                                                                if (_locAccountMapLineByTax.Account.Code != null)
                                                                {
                                                                    ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                             (new GroupOperator(GroupOperatorType.And,
                                                                                              new BinaryOperator("Code", _locAccountMapLineByTax.Account.Code)));
                                                                    if (_locCOA != null)
                                                                    {
                                                                        if (_locCOA.BalanceType == BalanceType.Change)
                                                                        {
                                                                            if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                            {
                                                                                if (_locTotalAmountDebit > 0)
                                                                                {
                                                                                    _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                }
                                                                                if (_locTotalAmountCredit > 0)
                                                                                {
                                                                                    _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                }
                                                                            }
                                                                            if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                            {
                                                                                if (_locTotalAmountDebit > 0)
                                                                                {
                                                                                    _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                }
                                                                                if (_locTotalAmountCredit > 0)
                                                                                {
                                                                                    _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                }
                                                                            }
                                                                        }

                                                                        _locCOA.Balance = _locTotalBalance;
                                                                        _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                        _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                        _locCOA.Save();
                                                                        _locCOA.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion JournalMapTaxAccountGroup

                                    #region JournalMapBusinessPartnerAccountGroup
                                    if (_locPurchaseInvoiceXPO.PayToVendor != null)
                                    {
                                        if (_locPurchaseInvoiceXPO.PayToVendor.BusinessPartnerAccountGroup != null)
                                        {
                                            XPCollection<JournalMap> _locJournalMapByBusinessPartners = new XPCollection<JournalMap>
                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                         new BinaryOperator("BusinessPartnerAccountGroup", _locPurchaseInvoiceXPO.PayToVendor.BusinessPartnerAccountGroup)));

                                            if (_locJournalMapByBusinessPartners != null && _locJournalMapByBusinessPartners.Count() > 0)
                                            {
                                                foreach (JournalMap _locJournalMapByBusinessPartner in _locJournalMapByBusinessPartners)
                                                {
                                                    XPCollection<JournalMapLine> _locJournalMapLineByBusinessPartners = new XPCollection<JournalMapLine>
                                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                         new BinaryOperator("JournalMap", _locJournalMapByBusinessPartner)));

                                                    if (_locJournalMapLineByBusinessPartners != null && _locJournalMapLineByBusinessPartners.Count() > 0)
                                                    {
                                                        foreach (JournalMapLine _locJournalMapLineByBusinessPartner in _locJournalMapLineByBusinessPartners)
                                                        {
                                                            AccountMap _locAccountMapByBusinessPartner = _currSession.FindObject<AccountMap>
                                                                                                         (new GroupOperator(GroupOperatorType.And,
                                                                                                          new BinaryOperator("Code", _locJournalMapLineByBusinessPartner.AccountMap.Code),
                                                                                                          new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                                          new BinaryOperator("OrderType", OrderType.Item),
                                                                                                          new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                                                                                                          new BinaryOperator("PostingMethodType", PostingMethodType.Tax)));

                                                            if (_locAccountMapByBusinessPartner != null)
                                                            {
                                                                XPCollection<AccountMapLine> _locAccountMapLineByBusinessPartners = new XPCollection<AccountMapLine>
                                                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                     new BinaryOperator("AccountMap", _locAccountMapByBusinessPartner),
                                                                                                                                     new BinaryOperator("Active", true)));

                                                                if (_locAccountMapLineByBusinessPartners != null && _locAccountMapLineByBusinessPartners.Count() > 0)
                                                                {
                                                                    double _locTotalAmountDebit = 0;
                                                                    double _locTotalAmountCredit = 0;
                                                                    double _locTotalBalance = 0;

                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLineByBusinessPartners)
                                                                    {
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            _locTotalAmountDebit = _locTxOfAmount;
                                                                        }
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            _locTotalAmountCredit = _locTxOfAmount;
                                                                        }

                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                        {
                                                                            PostingDate = now,
                                                                            PostingType = PostingType.Purchase,
                                                                            PostingMethod = PostingMethod.InvoiceAP,
                                                                            Account = _locAccountMapLine.Account,
                                                                            Debit = _locTotalAmountDebit,
                                                                            Credit = _locTotalAmountCredit,
                                                                            PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                                                                            PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                            Company = _locPurchaseInvoiceXPO.Company,
                                                                        };
                                                                        _saveGeneralJournal.Save();
                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                        {
                                                                            ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                            if (_locCOA != null)
                                                                            {
                                                                                if (_locCOA.BalanceType == BalanceType.Change)
                                                                                {
                                                                                    if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                    if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                }

                                                                                _locCOA.Balance = _locTotalBalance;
                                                                                _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                _locCOA.Save();
                                                                                _locCOA.Session.CommitTransaction();
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion JournalMapBusinessPartnerAccountGroup
                                }
                            }
                        }
                        #endregion CreateInvoiceAPWithTax

                        #region CreateInvoiceAPWithDiscount
                        XPQuery<PurchaseOrderLine> _PurchaseOrderLinesQueryBasedDiscount = new XPQuery<PurchaseOrderLine>(_currSession);

                        var _PurchaseOrderLineBasedDiscounts = from pil in _PurchaseOrderLinesQueryBasedDiscount
                                                               where (pil.PurchaseOrder == _locPurchaseInvoiceXPO.PurchaseOrder
                                                               && (pil.Status == Status.Posted || pil.Status == Status.Close))
                                                               group pil by pil.Discount.DiscountAccountGroup into g
                                                               select new { DiscountAccountGroup = g.Key };

                        if (_PurchaseOrderLineBasedDiscounts != null && _PurchaseOrderLineBasedDiscounts.Count() > 0)
                        {
                            foreach (var _PurchaseOrderLineBasedDiscount in _PurchaseOrderLineBasedDiscounts)
                            {
                                XPCollection<PurchaseOrderLine> _locPurchaseOrderLines = new XPCollection<PurchaseOrderLine>(_currSession,
                                                                                                new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("PurchaseOrder", _locPurchaseInvoiceXPO.PurchaseOrder),
                                                                                                new BinaryOperator("Discount.DiscountAccountGroup", _PurchaseOrderLineBasedDiscount.DiscountAccountGroup)));

                                if (_locPurchaseOrderLines != null && _locPurchaseOrderLines.Count() > 0)
                                {
                                    foreach (PurchaseOrderLine _locPurchaseOrderLine in _locPurchaseOrderLines)
                                    {
                                        _locDiscOfAmount = _locDiscOfAmount + _locPurchaseOrderLine.DiscAmount;
                                    }
                                }

                                if (_locDiscOfAmount > 0)
                                {
                                    #region JournalMapDiscountAccountGroup
                                    XPCollection<JournalMap> _locJournalMapByDiscounts = new XPCollection<JournalMap>
                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("DiscountAccountGroup", _PurchaseOrderLineBasedDiscount.DiscountAccountGroup)));

                                    if (_locJournalMapByDiscounts != null && _locJournalMapByDiscounts.Count() > 0)
                                    {
                                        foreach (JournalMap _locJournalMapByDiscount in _locJournalMapByDiscounts)
                                        {
                                            XPCollection<JournalMapLine> _locJournalMapLineByDiscounts = new XPCollection<JournalMapLine>
                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                     new BinaryOperator("JournalMap", _locJournalMapByDiscount)));

                                            if (_locJournalMapLineByDiscounts != null && _locJournalMapLineByDiscounts.Count() > 0)
                                            {
                                                foreach (JournalMapLine _locJournalMapLineByDiscount in _locJournalMapLineByDiscounts)
                                                {
                                                    AccountMap _locAccountMapByDiscount = _currSession.FindObject<AccountMap>
                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                      new BinaryOperator("Code", _locJournalMapLineByDiscount.AccountMap.Code),
                                                                                      new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                      new BinaryOperator("OrderType", OrderType.Item),
                                                                                      new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                                                                                      new BinaryOperator("PostingMethodType", PostingMethodType.Discount)));

                                                    if (_locAccountMapByDiscount != null)
                                                    {
                                                        XPCollection<AccountMapLine> _locAccountMapLineByDiscounts = new XPCollection<AccountMapLine>
                                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                 new BinaryOperator("AccountMap", _locAccountMapByDiscount),
                                                                                                                 new BinaryOperator("Active", true)));

                                                        if (_locAccountMapLineByDiscounts != null && _locAccountMapLineByDiscounts.Count() > 0)
                                                        {
                                                            double _locTotalPriceDebit = 0;
                                                            double _locTotalPriceCredit = 0;

                                                            foreach (AccountMapLine _locAccountMapLineByDiscount in _locAccountMapLineByDiscounts)
                                                            {
                                                                if (_locAccountMapLineByDiscount.AccountCharge == AccountCharge.Debit)
                                                                {
                                                                    _locTotalPriceDebit = _locDiscOfAmount;
                                                                }
                                                                if (_locAccountMapLineByDiscount.AccountCharge == AccountCharge.Credit)
                                                                {
                                                                    _locTotalPriceCredit = _locDiscOfAmount;
                                                                }

                                                                GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                {
                                                                    PostingDate = now,
                                                                    PostingType = PostingType.Purchase,
                                                                    PostingMethod = PostingMethod.InvoiceAP,
                                                                    Account = _locAccountMapLineByDiscount.Account,
                                                                    Debit = _locTotalPriceDebit,
                                                                    Credit = _locTotalPriceCredit,
                                                                    PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                                                                    PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                    Company = _locPurchaseInvoiceXPO.Company,
                                                                };
                                                                _saveGeneralJournal.Save();
                                                                _saveGeneralJournal.Session.CommitTransaction();

                                                                if (_locAccountMapLineByDiscount.Account.Code != null)
                                                                {
                                                                    ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                             (new GroupOperator(GroupOperatorType.And,
                                                                                              new BinaryOperator("Code", _locAccountMapLineByDiscount.Account.Code)));
                                                                    if (_locCOA != null)
                                                                    {
                                                                        _locCOA.Debit = _locCOA.Debit + _locTotalPriceDebit;
                                                                        _locCOA.Credit = _locCOA.Credit + _locTotalPriceCredit;
                                                                        _locCOA.Save();
                                                                        _locCOA.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion JournalMapDiscountAccountGroup

                                    #region JournalMapBusinessPartnerAccountGroup
                                    if (_locPurchaseInvoiceXPO.PayToVendor != null)
                                    {
                                        if (_locPurchaseInvoiceXPO.PayToVendor.BusinessPartnerAccountGroup != null)
                                        {
                                            XPCollection<JournalMap> _locJournalMapByBusinessPartners = new XPCollection<JournalMap>
                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                         new BinaryOperator("BusinessPartnerAccountGroup", _locPurchaseInvoiceXPO.PayToVendor.BusinessPartnerAccountGroup)));

                                            if (_locJournalMapByBusinessPartners != null && _locJournalMapByBusinessPartners.Count() > 0)
                                            {
                                                foreach (JournalMap _locJournalMapByBusinessPartner in _locJournalMapByBusinessPartners)
                                                {
                                                    XPCollection<JournalMapLine> _locJournalMapLineByBusinessPartners = new XPCollection<JournalMapLine>
                                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                         new BinaryOperator("JournalMap", _locJournalMapByBusinessPartner)));

                                                    if (_locJournalMapLineByBusinessPartners != null && _locJournalMapLineByBusinessPartners.Count() > 0)
                                                    {
                                                        foreach (JournalMapLine _locJournalMapLineByBusinessPartner in _locJournalMapLineByBusinessPartners)
                                                        {
                                                            AccountMap _locAccountMapByBusinessPartner = _currSession.FindObject<AccountMap>
                                                                                                         (new GroupOperator(GroupOperatorType.And,
                                                                                                          new BinaryOperator("Code", _locJournalMapLineByBusinessPartner.AccountMap.Code),
                                                                                                          new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                                          new BinaryOperator("OrderType", OrderType.Item),
                                                                                                          new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                                                                                                          new BinaryOperator("PostingMethodType", PostingMethodType.Discount)));

                                                            if (_locAccountMapByBusinessPartner != null)
                                                            {
                                                                XPCollection<AccountMapLine> _locAccountMapLineByBusinessPartners = new XPCollection<AccountMapLine>
                                                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                     new BinaryOperator("AccountMap", _locAccountMapByBusinessPartner),
                                                                                                                                     new BinaryOperator("Active", true)));

                                                                if (_locAccountMapLineByBusinessPartners != null && _locAccountMapLineByBusinessPartners.Count() > 0)
                                                                {
                                                                    double _locTotalPriceDebit = 0;
                                                                    double _locTotalPriceCredit = 0;

                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLineByBusinessPartners)
                                                                    {
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            _locTotalPriceDebit = _locDiscOfAmount;
                                                                        }
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            _locTotalPriceCredit = _locDiscOfAmount;
                                                                        }

                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                        {
                                                                            PostingDate = now,
                                                                            PostingType = PostingType.Purchase,
                                                                            PostingMethod = PostingMethod.InvoiceAP,
                                                                            Account = _locAccountMapLine.Account,
                                                                            Debit = _locTotalPriceDebit,
                                                                            Credit = _locTotalPriceCredit,
                                                                            PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                                                                            PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                            Company = _locPurchaseInvoiceXPO.Company,
                                                                        };
                                                                        _saveGeneralJournal.Save();
                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                        {
                                                                            ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                            if (_locCOA != null)
                                                                            {
                                                                                _locCOA.Debit = _locCOA.Debit + _locTotalPriceDebit;
                                                                                _locCOA.Credit = _locCOA.Credit + _locTotalPriceCredit;
                                                                                _locCOA.Save();
                                                                                _locCOA.Session.CommitTransaction();
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion JournalMapBusinessPartnerAccountGroup

                                }
                            }
                        }
                        #endregion CreateInvoiceAPWithDiscount

                        #endregion CreateInvoiceAPJournal
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseInvoice ", ex.ToString());
            }
        }

        private void SetPayableTransaction(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _locSignCode = null;
                double _locTotalUnitPrice = _locPurchaseInvoiceXPO.Pay;
                if (_locPurchaseInvoiceXPO != null)
                {
                    if (_locPurchaseInvoiceXPO.PayToVendor != null)
                    {
                        if (_locPurchaseInvoiceXPO.PayToVendor.Code != null)
                        {
                            BusinessPartner _locBusinessPartner = _currSession.FindObject<BusinessPartner>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Code", _locPurchaseInvoiceXPO.PayToVendor.Code)));


                            if (_locBusinessPartner != null)
                            {
                                _locSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.PayableTransaction);
                                if (_locSignCode != null)
                                {
                                    PayableTransaction _savePayableTransaction = new PayableTransaction(_currSession)
                                    {
                                        PostingType = PostingType.Purchase,
                                        PostingMethod = PostingMethod.Payment,
                                        PaymentMethod = _locPurchaseInvoiceXPO.PaymentMethod,
                                        EstimatedDate = _locPurchaseInvoiceXPO.EstimatedDate,
                                        SignCode = _locSignCode,
                                        PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                                        PurchaseInvoice = _locPurchaseInvoiceXPO,
                                        CompanyDefault = _locPurchaseInvoiceXPO.Company,
                                    };
                                    _savePayableTransaction.Save();
                                    _savePayableTransaction.Session.CommitTransaction();

                                    PayableTransaction _locPayableTransaction = _currSession.FindObject<PayableTransaction>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("SignCode", _locSignCode)));
                                    if (_locPayableTransaction != null)
                                    {
                                        #region BusinessPartner
                                        if (_locBusinessPartner.BusinessPartnerAccountGroup != null)
                                        {
                                            PayableTransactionLine _savePayableTransactionLine = new PayableTransactionLine(_currSession)
                                            {
                                                PostingType = PostingType.Purchase,
                                                PostingMethod = PostingMethod.Payment,
                                                OpenVendor = true,
                                                Vendor = _locBusinessPartner,
                                                BankAccount = _locPurchaseInvoiceXPO.BankAccount,
                                                PayableTransaction = _locPayableTransaction,
                                                CloseCredit = true,
                                                Debit = _locPurchaseInvoiceXPO.Pay,
                                                CompanyDefault = _locPayableTransaction.CompanyDefault,
                                            };
                                            _savePayableTransactionLine.Save();
                                            _savePayableTransactionLine.Session.CommitTransaction();
                                        }
                                        #endregion BusinessPartner

                                        #region Company
                                        #region Bank
                                        if (_locPayableTransaction.CompanyDefault != null)
                                        {
                                            if(_locPurchaseInvoiceXPO.BankAccountCompany != null)
                                            {
                                                PayableTransactionLine _savePayableTransactionLine = new PayableTransactionLine(_currSession)
                                                {
                                                    PostingType = PostingType.Purchase,
                                                    PostingMethod = PostingMethod.Payment,
                                                    OpenCompany = true,
                                                    Company = _locPayableTransaction.CompanyDefault,
                                                    BankAccount = _locPurchaseInvoiceXPO.BankAccountCompany,
                                                    PayableTransaction = _locPayableTransaction,
                                                    CloseDebit = true,
                                                    Credit = _locPurchaseInvoiceXPO.Pay,
                                                    CompanyDefault = _locPayableTransaction.CompanyDefault,
                                                };
                                                _savePayableTransactionLine.Save();
                                                _savePayableTransactionLine.Session.CommitTransaction();
                                            }
                                            else
                                            {
                                                BankAccount _locBankAccount = _currSession.FindObject<BankAccount>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", _locPayableTransaction.CompanyDefault),
                                                                            new BinaryOperator("Default", true),
                                                                            new BinaryOperator("Active", true)));
                                                if (_locBankAccount != null)
                                                {
                                                    PayableTransactionLine _savePayableTransactionLine = new PayableTransactionLine(_currSession)
                                                    {
                                                        PostingType = PostingType.Purchase,
                                                        PostingMethod = PostingMethod.Payment,
                                                        OpenCompany = true,
                                                        Company = _locPayableTransaction.CompanyDefault,
                                                        BankAccount = _locBankAccount,
                                                        PayableTransaction = _locPayableTransaction,
                                                        CloseDebit = true,
                                                        Credit = _locPurchaseInvoiceXPO.Pay,
                                                        CompanyDefault = _locPayableTransaction.CompanyDefault,
                                                    };
                                                    _savePayableTransactionLine.Save();
                                                    _savePayableTransactionLine.Session.CommitTransaction();
                                                }
                                            }
                                            

                                        }
                                        #endregion Bank
                                        #region AdvanceFee
                                        if (_locPurchaseInvoiceXPO.AdvFee > 0)
                                        {
                                            if (_locPayableTransaction.CompanyDefault != null)
                                            {
                                                if (_locPurchaseInvoiceXPO.BankAccountCompany != null)
                                                {
                                                    PayableTransactionLine _savePayableTransactionBank1Line = new PayableTransactionLine(_currSession)
                                                    {
                                                        PostingType = PostingType.Purchase,
                                                        PostingMethod = PostingMethod.AdvancePayment,
                                                        OpenCompany = true,
                                                        Company = _locPayableTransaction.CompanyDefault,
                                                        BankAccount = _locPurchaseInvoiceXPO.BankAccountCompany,
                                                        PayableTransaction = _locPayableTransaction,
                                                        CloseDebit = true,
                                                        Credit = _locPurchaseInvoiceXPO.AdvFee,
                                                        CompanyDefault = _locPayableTransaction.CompanyDefault,
                                                    };
                                                    _savePayableTransactionBank1Line.Save();
                                                    _savePayableTransactionBank1Line.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    BankAccount _locBankAccount = _currSession.FindObject<BankAccount>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Company", _locPayableTransaction.CompanyDefault),
                                                                                new BinaryOperator("Default", true),
                                                                                new BinaryOperator("Active", true)));
                                                    if (_locBankAccount != null)
                                                    {
                                                        PayableTransactionLine _savePayableTransactionBank2Line = new PayableTransactionLine(_currSession)
                                                        {
                                                            PostingType = PostingType.Purchase,
                                                            PostingMethod = PostingMethod.AdvancePayment,
                                                            OpenCompany = true,
                                                            Company = _locPayableTransaction.CompanyDefault,
                                                            BankAccount = _locBankAccount,
                                                            PayableTransaction = _locPayableTransaction,
                                                            CloseDebit = true,
                                                            Credit = _locPurchaseInvoiceXPO.AdvFee,
                                                            CompanyDefault = _locPayableTransaction.CompanyDefault,
                                                        };
                                                        _savePayableTransactionBank2Line.Save();
                                                        _savePayableTransactionBank2Line.Session.CommitTransaction();
                                                    }
                                                }

                                                PayableTransactionLine _savePayableTransactionLine = new PayableTransactionLine(_currSession)
                                                {
                                                    PostingType = PostingType.Purchase,
                                                    PostingMethod = PostingMethod.AdvancePayment,
                                                    OpenCompany = true,
                                                    Company = _locPayableTransaction.CompanyDefault,
                                                    PayableTransaction = _locPayableTransaction,
                                                    CloseCredit = true,
                                                    Debit = _locPurchaseInvoiceXPO.AdvFee,
                                                    CompanyDefault = _locPayableTransaction.CompanyDefault,
                                                };
                                                _savePayableTransactionLine.Save();
                                                _savePayableTransactionLine.Session.CommitTransaction();
                                            }

                                            _locPurchaseInvoiceXPO.PostAdvFee = _locPurchaseInvoiceXPO.PostAdvFee + _locPurchaseInvoiceXPO.AdvFee;
                                            _locPurchaseInvoiceXPO.Save();
                                            _locPurchaseInvoiceXPO.Session.CommitTransaction();
                                        }
                                        #endregion AdvanceFee
                                        #endregion Company

                                        //Tambahkan PaymentOutPlan
                                        SetPaymentOutPlan(_currSession, _locPurchaseInvoiceXPO, _locPayableTransaction);
                                    }
                                }
                            }
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseInvoice ", ex.ToString());
            }
        }

        private void SetRemainAmount(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locPurchaseInvoiceXPO != null)
                {
                    double _locPlan = 0;
                    double _locOutstanding = 0;
                    double _locMaxPay = 0;

                    bool _locActivationPosting = false;
                    string _locNotification = null;
                    Status _locStatus = Status.Posted;

                    #region RemainAmountWithPostedCount!=0

                    if (_locPurchaseInvoiceXPO.PostedCount > 0)
                    {
                        _locMaxPay = _locPurchaseInvoiceXPO.MaxPay - _locPurchaseInvoiceXPO.CM_Amount;
                        if (_locPurchaseInvoiceXPO.Outstanding > 0 && _locMaxPay != _locPurchaseInvoiceXPO.Plan)
                        {
                            if (_locPurchaseInvoiceXPO.Pay > 0)
                            {

                                _locPlan = _locPurchaseInvoiceXPO.Plan + _locPurchaseInvoiceXPO.Pay;
                                _locOutstanding = _locPurchaseInvoiceXPO.Outstanding - _locPurchaseInvoiceXPO.Pay;

                                if (_locOutstanding == 0 && _locPlan == _locMaxPay)
                                {
                                    _locActivationPosting = true;
                                    _locStatus = Status.Posted;
                                }

                                _locPurchaseInvoiceXPO.Plan = _locPlan;
                                _locPurchaseInvoiceXPO.Outstanding = _locOutstanding;
                                _locPurchaseInvoiceXPO.Status = _locStatus;
                                _locPurchaseInvoiceXPO.StatusDate = now;
                                _locPurchaseInvoiceXPO.ActivationPosting = _locActivationPosting;
                                _locPurchaseInvoiceXPO.PostedCount = _locPurchaseInvoiceXPO.PostedCount + 1;
                                _locPurchaseInvoiceXPO.Save();
                                _locPurchaseInvoiceXPO.Session.CommitTransaction();
                            }
                        }
                    }
                    #endregion RemainAmountWithPostedCount!=0

                    #region RemainAmountWithPostedCount=0
                    else
                    {
                        if (_locPurchaseInvoiceXPO.Pay > 0)
                        {

                            _locPlan = _locPurchaseInvoiceXPO.Pay;
                            _locMaxPay = _locPurchaseInvoiceXPO.MaxPay - _locPurchaseInvoiceXPO.CM_Amount;
                            _locOutstanding = _locMaxPay - _locPlan;


                            if (_locPlan == _locMaxPay && _locOutstanding == 0)
                            {
                                _locStatus = Status.Posted;
                                _locActivationPosting = true;
                            }

                            _locPurchaseInvoiceXPO.Plan = _locPlan;
                            _locPurchaseInvoiceXPO.Outstanding = _locOutstanding;
                            _locPurchaseInvoiceXPO.Status = _locStatus;
                            _locPurchaseInvoiceXPO.StatusDate = now;
                            _locPurchaseInvoiceXPO.Notification = _locNotification;
                            _locPurchaseInvoiceXPO.ActivationPosting = _locActivationPosting;
                            _locPurchaseInvoiceXPO.PostedCount = _locPurchaseInvoiceXPO.PostedCount + 1;
                            _locPurchaseInvoiceXPO.Save();
                            _locPurchaseInvoiceXPO.Session.CommitTransaction();

                        }
                    }
                    #endregion RemainAmountWithPostedCount=0
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseInvoice ", ex.ToString());
            }
        }

        private void SetRemainAmountSplitInvoiceForPIC(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                double _locPlan = 0;
                double _locOutstanding = 0;
                double _locMaxPay = 0;
                double _locDP_Amount = 0;
                bool _locDP_Posting = false;
                bool _locActivationPosting = false;
                Status _locStatus = Status.Posted;

                if (_locPurchaseInvoiceXPO != null)
                {
                    if (_locPurchaseInvoiceXPO.PurchaseOrder != null)
                    {
                        PurchaseInvoiceCollection _locPurchaseInvoiceCollection = _currSession.FindObject<PurchaseInvoiceCollection>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("PurchaseOrder", _locPurchaseInvoiceXPO.PurchaseOrder)));
                        if (_locPurchaseInvoiceCollection != null)
                        {
                            #region RemainAmountWithPostedCount!=0

                            if (_locPurchaseInvoiceCollection.PostedCount > 0)
                            {
                                _locMaxPay = _locPurchaseInvoiceCollection.MaxPay - _locPurchaseInvoiceCollection.CM_Amount;
                                if (_locPurchaseInvoiceCollection.Outstanding > 0 && _locPurchaseInvoiceCollection.MaxPay != _locPurchaseInvoiceCollection.Plan)
                                {
                                    if (_locPurchaseInvoiceXPO.Pay > 0)
                                    {

                                        _locPlan = _locPurchaseInvoiceCollection.Plan + _locPurchaseInvoiceXPO.Pay;
                                        _locOutstanding = _locPurchaseInvoiceCollection.Outstanding - _locPurchaseInvoiceXPO.Pay;

                                        if (_locOutstanding == 0 && _locPlan == _locMaxPay)
                                        {
                                            _locActivationPosting = true;
                                            _locStatus = Status.Posted;
                                        }

                                        _locPurchaseInvoiceCollection.DP_Posting = _locDP_Posting;
                                        _locPurchaseInvoiceCollection.DP_Amount = _locDP_Amount;
                                        _locPurchaseInvoiceCollection.Plan = _locPlan;
                                        _locPurchaseInvoiceCollection.Outstanding = _locOutstanding;
                                        _locPurchaseInvoiceCollection.Status = _locStatus;
                                        _locPurchaseInvoiceCollection.StatusDate = now;
                                        _locPurchaseInvoiceCollection.ActivationPosting = _locActivationPosting;
                                        _locPurchaseInvoiceCollection.PostedCount = _locPurchaseInvoiceCollection.PostedCount + 1;
                                        _locPurchaseInvoiceCollection.Save();
                                        _locPurchaseInvoiceCollection.Session.CommitTransaction();

                                        PurchaseInvoiceCollectionLine _locPurchaseInvoiceCollectionPC = _currSession.FindObject<PurchaseInvoiceCollectionLine>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                     new BinaryOperator("PurchaseInvoiceCollection", _locPurchaseInvoiceCollection),
                                                                                                     new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO)));
                                        if (_locPurchaseInvoiceCollectionPC == null)
                                        {
                                            PurchaseInvoiceCollectionLine _saveDataPurchaseInvoiceCollectionLine = new PurchaseInvoiceCollectionLine(_currSession)
                                            {
                                                PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                PurchaseInvoiceCollection = _locPurchaseInvoiceCollection,
                                            };
                                            _saveDataPurchaseInvoiceCollectionLine.Save();
                                            _saveDataPurchaseInvoiceCollectionLine.Session.CommitTransaction();
                                        }
                                    }
                                }
                            }
                            #endregion RemainAmountWithPostedCount!=0

                            #region RemainAmountWithPostedCount=0
                            else
                            {
                                if (_locPurchaseInvoiceXPO.Pay > 0)
                                {

                                    _locPlan = _locPurchaseInvoiceXPO.Pay;
                                    _locMaxPay = _locPurchaseInvoiceCollection.MaxPay - _locPurchaseInvoiceCollection.CM_Amount;
                                    _locOutstanding = _locMaxPay - _locPurchaseInvoiceXPO.Pay;

                                    if (_locPlan == _locMaxPay && _locOutstanding == 0)
                                    {
                                        _locStatus = Status.Posted;
                                        _locActivationPosting = true;
                                    }

                                    _locPurchaseInvoiceCollection.DP_Posting = _locDP_Posting;
                                    _locPurchaseInvoiceCollection.DP_Amount = _locDP_Amount;
                                    _locPurchaseInvoiceCollection.Plan = _locPlan;
                                    _locPurchaseInvoiceCollection.Outstanding = _locOutstanding;
                                    _locPurchaseInvoiceCollection.Status = _locStatus;
                                    _locPurchaseInvoiceCollection.StatusDate = now;
                                    _locPurchaseInvoiceCollection.ActivationPosting = _locActivationPosting;
                                    _locPurchaseInvoiceCollection.PostedCount = _locPurchaseInvoiceCollection.PostedCount + 1;
                                    _locPurchaseInvoiceCollection.Save();
                                    _locPurchaseInvoiceCollection.Session.CommitTransaction();

                                    PurchaseInvoiceCollectionLine _locPurchaseInvoiceCollectionPC = _currSession.FindObject<PurchaseInvoiceCollectionLine>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                     new BinaryOperator("PurchaseInvoiceCollection", _locPurchaseInvoiceCollection),
                                                                                                     new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO)));
                                    if (_locPurchaseInvoiceCollectionPC == null)
                                    {
                                        PurchaseInvoiceCollectionLine _saveDataPurchaseInvoiceCollectionLine = new PurchaseInvoiceCollectionLine(_currSession)
                                        {
                                            PurchaseInvoice = _locPurchaseInvoiceXPO,
                                            PurchaseInvoiceCollection = _locPurchaseInvoiceCollection,
                                        };
                                        _saveDataPurchaseInvoiceCollectionLine.Save();
                                        _saveDataPurchaseInvoiceCollectionLine.Session.CommitTransaction();
                                    }
                                }
                            }
                            #endregion RemainAmountWithPostedCount=0
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesInvoice ", ex.ToString());
            }
        }

        private void SetPaymentOutPlan(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO, PayableTransaction _PayableTransaction)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                double _locTotalUnitPrice = _locPurchaseInvoiceXPO.Pay;
                PurchaseInvoiceCollection _locPurchaseInvoiceCollection = null;

                if (_locPurchaseInvoiceXPO != null && _PayableTransaction != null)
                {
                    if (_locPurchaseInvoiceXPO.PurchaseOrder != null)
                    {
                        _locPurchaseInvoiceCollection = _currSession.FindObject<PurchaseInvoiceCollection>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("PurchaseOrder", _locPurchaseInvoiceXPO.PurchaseOrder)));

                        if (_locPurchaseInvoiceCollection != null)
                        {
                            PaymentOutPlan _savePaymentOutPlan = new PaymentOutPlan(_currSession)
                            {
                                PaymentMethod = _locPurchaseInvoiceXPO.PaymentMethod,
                                EstimatedDate = _locPurchaseInvoiceXPO.EstimatedDate,
                                Plan = _locPurchaseInvoiceXPO.Pay,
                                Outstanding = _locPurchaseInvoiceXPO.Pay,
                                PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                                PurchaseInvoice = _locPurchaseInvoiceXPO,
                                PayableTransaction = _PayableTransaction,
                                PurchaseInvoiceCollection = _locPurchaseInvoiceCollection,
                                Company = _locPurchaseInvoiceXPO.Company,
                            };
                            _savePaymentOutPlan.Save();
                            _savePaymentOutPlan.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseInvoice ", ex.ToString());
            }
        }

        private void SetNormalPay(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locPurchaseInvoiceXPO != null)
                {
                    _locPurchaseInvoiceXPO.Pay = 0;
                    _locPurchaseInvoiceXPO.Save();
                    _locPurchaseInvoiceXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseInvoice ", ex.ToString());
            }
        }

        private void SetRemainPayableQty(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                if (_locPurchaseInvoiceXPO != null)
                {
                    XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count > 0)
                    {
                        double _locRmDQty = 0;
                        double _locRmQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                        {
                            #region ProcessCount=0
                            if (_locPurchaseInvoiceLine.ProcessCount == 0)
                            {
                                #region MaxQuantity
                                if (_locPurchaseInvoiceLine.MxDQty > 0)
                                {
                                    if (_locPurchaseInvoiceLine.DQty > 0 && _locPurchaseInvoiceLine.DQty <= _locPurchaseInvoiceLine.MxDQty)
                                    {
                                        _locRmDQty = _locPurchaseInvoiceLine.MxDQty - _locPurchaseInvoiceLine.DQty;
                                    }

                                    if (_locPurchaseInvoiceLine.Qty > 0 && _locPurchaseInvoiceLine.Qty <= _locPurchaseInvoiceLine.MxQty)
                                    {
                                        _locRmQty = _locPurchaseInvoiceLine.MxQty - _locPurchaseInvoiceLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locPurchaseInvoiceLine.Item),
                                                                new BinaryOperator("UOM", _locPurchaseInvoiceLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locPurchaseInvoiceLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locPurchaseInvoiceLine.DQty > 0)
                                    {
                                        _locRmDQty = 0;
                                    }

                                    if (_locPurchaseInvoiceLine.Qty > 0)
                                    {
                                        _locRmQty = 0;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locPurchaseInvoiceLine.Item),
                                                                new BinaryOperator("UOM", _locPurchaseInvoiceLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locPurchaseInvoiceLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locPurchaseInvoiceLine.ProcessCount > 0)
                            {
                                if (_locPurchaseInvoiceLine.RmDQty > 0)
                                {
                                    _locRmDQty = _locPurchaseInvoiceLine.RmDQty - _locPurchaseInvoiceLine.DQty;
                                }

                                if (_locPurchaseInvoiceLine.RmQty > 0)
                                {
                                    _locRmQty = _locPurchaseInvoiceLine.RmQty - _locPurchaseInvoiceLine.Qty;
                                }

                                if (_locPurchaseInvoiceLine.MxDQty > 0 || _locPurchaseInvoiceLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locPurchaseInvoiceLine.Item),
                                                                new BinaryOperator("UOM", _locPurchaseInvoiceLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locPurchaseInvoiceLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locPurchaseInvoiceLine.Item),
                                                                new BinaryOperator("UOM", _locPurchaseInvoiceLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locPurchaseInvoiceLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                }


                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locPurchaseInvoiceLine.RmDQty = _locRmDQty;
                            _locPurchaseInvoiceLine.RmQty = _locRmQty;
                            _locPurchaseInvoiceLine.RmTQty = _locInvLineTotal;
                            _locPurchaseInvoiceLine.Save();
                            _locPurchaseInvoiceLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        private void SetPostingPayableQty(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                if (_locPurchaseInvoiceXPO != null)
                {
                    XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count > 0)
                    {
                        double _locPDQty = 0;
                        double _locPQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                        {
                            #region ProcessCount=0
                            if (_locPurchaseInvoiceLine.ProcessCount == 0)
                            {
                                #region MaxQuantity
                                if (_locPurchaseInvoiceLine.MxDQty > 0)
                                {
                                    if (_locPurchaseInvoiceLine.DQty > 0 && _locPurchaseInvoiceLine.DQty <= _locPurchaseInvoiceLine.MxDQty)
                                    {
                                        _locPDQty = _locPurchaseInvoiceLine.DQty;
                                    }

                                    if (_locPurchaseInvoiceLine.Qty > 0 && _locPurchaseInvoiceLine.Qty <= _locPurchaseInvoiceLine.MxQty)
                                    {
                                        _locPQty = _locPurchaseInvoiceLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locPurchaseInvoiceLine.Item),
                                                                new BinaryOperator("UOM", _locPurchaseInvoiceLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locPurchaseInvoiceLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locPurchaseInvoiceLine.DQty > 0)
                                    {
                                        _locPDQty = _locPurchaseInvoiceLine.DQty;
                                    }

                                    if (_locPurchaseInvoiceLine.Qty > 0)
                                    {
                                        _locPQty = _locPurchaseInvoiceLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locPurchaseInvoiceLine.Item),
                                                                new BinaryOperator("UOM", _locPurchaseInvoiceLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locPurchaseInvoiceLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locPurchaseInvoiceLine.ProcessCount > 0)
                            {
                                if (_locPurchaseInvoiceLine.PDQty > 0)
                                {
                                    _locPDQty = _locPurchaseInvoiceLine.PDQty + _locPurchaseInvoiceLine.DQty;
                                }

                                if (_locPurchaseInvoiceLine.PQty > 0)
                                {
                                    _locPQty = _locPurchaseInvoiceLine.PQty + _locPurchaseInvoiceLine.Qty;
                                }

                                if (_locPurchaseInvoiceLine.MxDQty > 0 || _locPurchaseInvoiceLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locPurchaseInvoiceLine.Item),
                                                            new BinaryOperator("UOM", _locPurchaseInvoiceLine.MxUOM),
                                                            new BinaryOperator("DefaultUOM", _locPurchaseInvoiceLine.MxDUOM),
                                                            new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locPurchaseInvoiceLine.Item),
                                                            new BinaryOperator("UOM", _locPurchaseInvoiceLine.UOM),
                                                            new BinaryOperator("DefaultUOM", _locPurchaseInvoiceLine.DUOM),
                                                            new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locPurchaseInvoiceLine.PDQty = _locPDQty;
                            _locPurchaseInvoiceLine.PDUOM = _locPurchaseInvoiceLine.DUOM;
                            _locPurchaseInvoiceLine.PQty = _locPQty;
                            _locPurchaseInvoiceLine.PUOM = _locPurchaseInvoiceLine.UOM;
                            _locPurchaseInvoiceLine.PTQty = _locInvLineTotal;
                            _locPurchaseInvoiceLine.PTotalPIL = _locPurchaseInvoiceLine.TotalPIl;
                            _locPurchaseInvoiceLine.PTAmountPIL = _locPurchaseInvoiceLine.TAmountPIL;
                            _locPurchaseInvoiceLine.PTPIL = _locPurchaseInvoiceLine.ReportTPIl;
                            _locPurchaseInvoiceLine.PTax = _locPurchaseInvoiceLine.ReportTaxPIL;
                            _locPurchaseInvoiceLine.PDisc = _locPurchaseInvoiceLine.ReportDisc;
                            _locPurchaseInvoiceLine.Save();
                            _locPurchaseInvoiceLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }

        }

        private void SetProcessPayableCount(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                if (_locPurchaseInvoiceXPO != null)
                {
                    XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count > 0)
                    {

                        foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                        {
                            if (_locPurchaseInvoiceLine.Status == Status.Progress || _locPurchaseInvoiceLine.Status == Status.Posted)
                            {
                                _locPurchaseInvoiceLine.ProcessCount = _locPurchaseInvoiceLine.ProcessCount + 1;
                                _locPurchaseInvoiceLine.Save();
                                _locPurchaseInvoiceLine.Session.CommitTransaction();
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        private void SetStatusPayablePurchaseInvoiceLine(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locPurchaseInvoiceXPO != null)
                {
                    XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count > 0)
                    {

                        foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                        {
                            if (_locPurchaseInvoiceLine.Status == Status.Progress || _locPurchaseInvoiceLine.Status == Status.Posted)
                            {
                                if (_locPurchaseInvoiceLine.RmDQty == 0 && _locPurchaseInvoiceLine.RmQty == 0 && _locPurchaseInvoiceLine.RmTQty == 0)
                                {
                                    _locPurchaseInvoiceLine.Status = Status.Close;
                                    _locPurchaseInvoiceLine.ActivationPosting = true;
                                    _locPurchaseInvoiceLine.StatusDate = now;
                                }
                                else
                                {
                                    _locPurchaseInvoiceLine.Status = Status.Posted;
                                    _locPurchaseInvoiceLine.StatusDate = now;
                                }
                                _locPurchaseInvoiceLine.Save();
                                _locPurchaseInvoiceLine.Session.CommitTransaction();
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        private void SetNormalPayableQuantity(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                if (_locPurchaseInvoiceXPO != null)
                {
                    XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                                                                                     new BinaryOperator("Select", true)));

                    if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count > 0)
                    {
                        foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                        {
                            if (_locPurchaseInvoiceLine.Status == Status.Progress || _locPurchaseInvoiceLine.Status == Status.Posted || _locPurchaseInvoiceLine.Status == Status.Close)
                            {
                                if (_locPurchaseInvoiceLine.DQty > 0 || _locPurchaseInvoiceLine.Qty > 0)
                                {
                                    _locPurchaseInvoiceLine.Select = false;
                                    _locPurchaseInvoiceLine.DQty = 0;
                                    _locPurchaseInvoiceLine.Qty = 0;
                                    _locPurchaseInvoiceLine.Save();
                                    _locPurchaseInvoiceLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        private void SetFinalPayablePurchaseInvoice(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locPurchInvLineCount = 0;
                double _locMaxPay = 0;
                bool _locActivationPosting = false;

                if (_locPurchaseInvoiceXPO != null)
                {

                    _locMaxPay = _locPurchaseInvoiceXPO.MaxPay - _locPurchaseInvoiceXPO.CM_Amount;

                    XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO)));

                    if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count() > 0)
                    {
                        _locPurchInvLineCount = _locPurchaseInvoiceLines.Count();

                        foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                        {
                            if (_locPurchaseInvoiceLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }
                    }

                    if (_locStatusCount == _locPurchInvLineCount || (_locMaxPay == _locPurchaseInvoiceXPO.Plan && _locPurchaseInvoiceXPO.Outstanding == 0))
                    {
                        if(_locMaxPay == _locPurchaseInvoiceXPO.Plan && _locPurchaseInvoiceXPO.Outstanding == 0)
                        {
                            _locActivationPosting = true;
                        }
                        _locPurchaseInvoiceXPO.ActivationPosting = _locActivationPosting;
                        _locPurchaseInvoiceXPO.Status = Status.Posted;
                        _locPurchaseInvoiceXPO.StatusDate = now;
                        _locPurchaseInvoiceXPO.Save();
                        _locPurchaseInvoiceXPO.Session.CommitTransaction();
                    }
                    else
                    {
                        _locPurchaseInvoiceXPO.Status = Status.Posted;
                        _locPurchaseInvoiceXPO.StatusDate = now;
                        _locPurchaseInvoiceXPO.Save();
                        _locPurchaseInvoiceXPO.Session.CommitTransaction();
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        private void SetAfterPostingReport(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            //Memindahkan Nilai TotalPIL, TUAmount, ReportTPIL, ReportTDisc, ReportTaxPIL ke field Posting Untuk Report After Posting
            try
            {
                DateTime now = DateTime.Now;

                if (_locPurchaseInvoiceXPO != null)
                {
                    XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO)));

                    if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count() > 0)
                    {
                        foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                        {
                            _locPurchaseInvoiceLine.PTotalPIL = _locPurchaseInvoiceLine.TotalPIl;
                            _locPurchaseInvoiceLine.PTUAmount = _locPurchaseInvoiceLine.TUAmount;
                            _locPurchaseInvoiceLine.PTAmountPIL = _locPurchaseInvoiceLine.TAmountPIL;
                            _locPurchaseInvoiceLine.PTPIL = _locPurchaseInvoiceLine.ReportTPIl;
                            _locPurchaseInvoiceLine.PDisc = _locPurchaseInvoiceLine.ReportDisc;
                            _locPurchaseInvoiceLine.PTax = _locPurchaseInvoiceLine.ReportTaxPIL;
                            _locPurchaseInvoiceLine.Save();
                            _locPurchaseInvoiceLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseInvoice " + ex.ToString());
            }
        }

        #endregion PostingMethod

        #region PostingMethodOld
        //24 Jan 2020

        //24 Jan 2020
        

        private void SetInvoiceAPJournalOld(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                #region CreateInvoiceAPJournal

                //Check Nullable TaxAccountGroup
                if (GetNullPurchaseInvoiceLineTaxOld(_currSession, _locPurchaseInvoiceXPO) == false)
                {
                    #region CreateInvoiceAPJournalByItemsWithTax
                    XPQuery<PurchaseInvoiceLine> _purchaseInvoiceLinesQueryBasedTax = new XPQuery<PurchaseInvoiceLine>(_currSession);

                    var _purchaseInvoiceLineBasedTaxs = from pil in _purchaseInvoiceLinesQueryBasedTax
                                                        where (pil.PurchaseInvoice == _locPurchaseInvoiceXPO
                                                        && (pil.Status == Status.Progress || pil.Status == Status.Posted)
                                                        && pil.Select == true)
                                                        group pil by pil.Tax.TaxAccountGroup into g
                                                        select new { TaxAccountGroup = g.Key };

                    if (_purchaseInvoiceLineBasedTaxs != null && _purchaseInvoiceLineBasedTaxs.Count() > 0)
                    {
                        foreach (var _purchaseInvoiceLineBasedTax in _purchaseInvoiceLineBasedTaxs)
                        {
                            #region CreateInvoiceAPJournal
                            //Kalau discountnya pergroup yang berbeda maka tinggal buat Discount.DiscountAccountGroup di bawah ItemAccountGroup
                            XPQuery<PurchaseInvoiceLine> _purchaseInvoiceLinesQuery = new XPQuery<PurchaseInvoiceLine>(_currSession);

                            var _purchaseInvoiceLines = from pil in _purchaseInvoiceLinesQuery
                                                        where (pil.PurchaseInvoice == _locPurchaseInvoiceXPO
                                                        && pil.Tax.TaxAccountGroup == _purchaseInvoiceLineBasedTax.TaxAccountGroup
                                                        && (pil.Status == Status.Progress || pil.Status == Status.Posted))
                                                        group pil by pil.Item.ItemAccountGroup into g
                                                        select new { ItemAccountGroup = g.Key };

                            if (_purchaseInvoiceLines != null && _purchaseInvoiceLines.Count() > 0)
                            {
                                foreach (var _purchaseInvoiceLine in _purchaseInvoiceLines)
                                {
                                    //Membuat Perhitungan jumlah TUPrice dan Total TxOfPrice

                                    double _locTUAmount = 0;
                                    double _locTxOfAmount = 0;
                                    double _locDiscOfAmount = 0;
                                    XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>(_currSession,
                                                                                            new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("Item.ItemAccountGroup", _purchaseInvoiceLine.ItemAccountGroup),
                                                                                            new BinaryOperator("Tax.TaxAccountGroup", _purchaseInvoiceLineBasedTax.TaxAccountGroup),
                                                                                            new BinaryOperator("Select", true)));

                                    if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count() > 0)
                                    {
                                        foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                                        {
                                            _locTUAmount = _locTUAmount + _locPurchaseInvoiceLine.TUAmount;
                                            //Membuat TaxLine
                                            _locTxOfAmount = _locTxOfAmount + _locPurchaseInvoiceLine.TxAmount;
                                            _locDiscOfAmount = _locDiscOfAmount + _locPurchaseInvoiceLine.DiscAmount;
                                        }
                                    }

                                    if (_locTUAmount > 0)
                                    {
                                        #region JournalMapItemAccountGroup
                                        XPCollection<JournalMap> _locJournalMaps = new XPCollection<JournalMap>
                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("ItemAccountGroup", _purchaseInvoiceLine.ItemAccountGroup)));

                                        if (_locJournalMaps != null && _locJournalMaps.Count() > 0)
                                        {
                                            foreach (JournalMap _locJournalMap in _locJournalMaps)
                                            {
                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("JournalMap", _locJournalMap)));

                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                {
                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                    {
                                                        AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                     new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                     new BinaryOperator("OrderType", OrderType.Item),
                                                                                     new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                                                                                     new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                        if (_locAccountMap != null)
                                                        {
                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("AccountMap", _locAccountMap),
                                                                                            new BinaryOperator("Active", true)));

                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                            {
                                                                double _locTotalPriceDebit = 0;
                                                                double _locTotalPriceCredit = 0;

                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                {
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                    {
                                                                        _locTotalPriceDebit = _locTUAmount;
                                                                    }
                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                    {
                                                                        _locTotalPriceCredit = _locTUAmount;
                                                                    }

                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                    {
                                                                        PostingDate = now,
                                                                        PostingType = PostingType.Purchase,
                                                                        PostingMethod = PostingMethod.InvoiceAP,
                                                                        Account = _locAccountMapLine.Account,
                                                                        Debit = _locTotalPriceDebit,
                                                                        Credit = _locTotalPriceCredit,
                                                                        PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                                                                        PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                        Company = _locPurchaseInvoiceXPO.Company,
                                                                    };
                                                                    _saveGeneralJournal.Save();
                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                    {
                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                        if (_locCOA != null)
                                                                        {
                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalPriceDebit;
                                                                            _locCOA.Credit = _locCOA.Credit + _locTotalPriceCredit;
                                                                            _locCOA.Save();
                                                                            _locCOA.Session.CommitTransaction();
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion JournalMapItemAccountGroup

                                        #region JournalMapBusinessPartnerAccountGroup
                                        if (_locPurchaseInvoiceXPO.PayToVendor != null)
                                        {
                                            if (_locPurchaseInvoiceXPO.PayToVendor.BusinessPartnerAccountGroup != null)
                                            {
                                                XPCollection<JournalMap> _locJournalMapByBusinessPartners = new XPCollection<JournalMap>
                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                           new BinaryOperator("BusinessPartnerAccountGroup", _locPurchaseInvoiceXPO.PayToVendor.BusinessPartnerAccountGroup)));

                                                if (_locJournalMapByBusinessPartners != null && _locJournalMapByBusinessPartners.Count() > 0)
                                                {
                                                    foreach (JournalMap _locJournalMapByBusinessPartner in _locJournalMapByBusinessPartners)
                                                    {
                                                        XPCollection<JournalMapLine> _locJournalMapLineByBusinessPartners = new XPCollection<JournalMapLine>
                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("JournalMap", _locJournalMapByBusinessPartner)));

                                                        if (_locJournalMapLineByBusinessPartners != null && _locJournalMapLineByBusinessPartners.Count() > 0)
                                                        {
                                                            foreach (JournalMapLine _locJournalMapLineByBusinessPartner in _locJournalMapLineByBusinessPartners)
                                                            {
                                                                AccountMap _locAccountMapByBusinessPartner = _currSession.FindObject<AccountMap>
                                                                                             (new GroupOperator(GroupOperatorType.And,
                                                                                             new BinaryOperator("Code", _locJournalMapLineByBusinessPartner.AccountMap.Code),
                                                                                             new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                             new BinaryOperator("OrderType", OrderType.Item),
                                                                                             new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                                                                                             new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                                if (_locAccountMapByBusinessPartner != null)
                                                                {
                                                                    XPCollection<AccountMapLine> _locAccountMapLineByBusinessPartners = new XPCollection<AccountMapLine>
                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("AccountMap", _locAccountMapByBusinessPartner),
                                                                                                    new BinaryOperator("Active", true)));

                                                                    if (_locAccountMapLineByBusinessPartners != null && _locAccountMapLineByBusinessPartners.Count() > 0)
                                                                    {
                                                                        double _locTotalPriceDebit = 0;
                                                                        double _locTotalPriceCredit = 0;

                                                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLineByBusinessPartners)
                                                                        {
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                            {
                                                                                _locTotalPriceDebit = _locTUAmount;
                                                                            }
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                            {
                                                                                _locTotalPriceCredit = _locTUAmount;
                                                                            }

                                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                            {
                                                                                PostingDate = now,
                                                                                PostingType = PostingType.Purchase,
                                                                                PostingMethod = PostingMethod.InvoiceAP,
                                                                                Account = _locAccountMapLine.Account,
                                                                                Debit = _locTotalPriceDebit,
                                                                                Credit = _locTotalPriceCredit,
                                                                                PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                                                                                PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                                Company = _locPurchaseInvoiceXPO.Company,
                                                                            };
                                                                            _saveGeneralJournal.Save();
                                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                                            if (_locAccountMapLine.Account.Code != null)
                                                                            {
                                                                                ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                                if (_locCOA != null)
                                                                                {
                                                                                    _locCOA.Debit = _locCOA.Debit + _locTotalPriceDebit;
                                                                                    _locCOA.Credit = _locCOA.Credit + _locTotalPriceCredit;
                                                                                    _locCOA.Save();
                                                                                    _locCOA.Session.CommitTransaction();
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion JournalMapBusinessPartnerAccountGroup
                                    }

                                    if (_locTxOfAmount > 0)
                                    {
                                        #region JournalMapTaxAccountGroup
                                        XPCollection<JournalMap> _locJournalMapByTaxs = new XPCollection<JournalMap>
                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("TaxAccountGroup", _purchaseInvoiceLineBasedTax.TaxAccountGroup)));

                                        if (_locJournalMapByTaxs != null && _locJournalMapByTaxs.Count() > 0)
                                        {
                                            foreach (JournalMap _locJournalMapByTax in _locJournalMapByTaxs)
                                            {
                                                XPCollection<JournalMapLine> _locJournalMapLineByTaxs = new XPCollection<JournalMapLine>
                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("JournalMap", _locJournalMapByTax)));

                                                if (_locJournalMapLineByTaxs != null && _locJournalMapLineByTaxs.Count() > 0)
                                                {
                                                    foreach (JournalMapLine _locJournalMapLineByTax in _locJournalMapLineByTaxs)
                                                    {
                                                        AccountMap _locAccountMapByTax = _currSession.FindObject<AccountMap>
                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Code", _locJournalMapLineByTax.AccountMap.Code),
                                                                                     new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                     new BinaryOperator("OrderType", OrderType.Item),
                                                                                     new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                                                                                     new BinaryOperator("PostingMethodType", PostingMethodType.Tax)));

                                                        if (_locAccountMapByTax != null)
                                                        {
                                                            XPCollection<AccountMapLine> _locAccountMapLineByTaxs = new XPCollection<AccountMapLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("AccountMap", _locAccountMapByTax),
                                                                                            new BinaryOperator("Active", true)));

                                                            if (_locAccountMapLineByTaxs != null && _locAccountMapLineByTaxs.Count() > 0)
                                                            {
                                                                double _locTotalPriceDebit = 0;
                                                                double _locTotalPriceCredit = 0;

                                                                foreach (AccountMapLine _locAccountMapLineByTax in _locAccountMapLineByTaxs)
                                                                {
                                                                    if (_locAccountMapLineByTax.AccountCharge == AccountCharge.Debit)
                                                                    {
                                                                        _locTotalPriceDebit = _locTxOfAmount;
                                                                    }
                                                                    if (_locAccountMapLineByTax.AccountCharge == AccountCharge.Credit)
                                                                    {
                                                                        _locTotalPriceCredit = _locTxOfAmount;
                                                                    }

                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                    {
                                                                        PostingDate = now,
                                                                        PostingType = PostingType.Purchase,
                                                                        PostingMethod = PostingMethod.InvoiceAP,
                                                                        Account = _locAccountMapLineByTax.Account,
                                                                        Debit = _locTotalPriceDebit,
                                                                        Credit = _locTotalPriceCredit,
                                                                        PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                                                                        PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                        Company = _locPurchaseInvoiceXPO.Company,
                                                                    };
                                                                    _saveGeneralJournal.Save();
                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                    if (_locAccountMapLineByTax.Account.Code != null)
                                                                    {
                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Code", _locAccountMapLineByTax.Account.Code)));
                                                                        if (_locCOA != null)
                                                                        {
                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalPriceDebit;
                                                                            _locCOA.Credit = _locCOA.Credit + _locTotalPriceCredit;
                                                                            _locCOA.Save();
                                                                            _locCOA.Session.CommitTransaction();
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion JournalMapTaxAccountGroup

                                        #region JournalMapBusinessPartnerAccountGroup
                                        if (_locPurchaseInvoiceXPO.PayToVendor != null)
                                        {
                                            if (_locPurchaseInvoiceXPO.PayToVendor.BusinessPartnerAccountGroup != null)
                                            {
                                                XPCollection<JournalMap> _locJournalMapByBusinessPartners = new XPCollection<JournalMap>
                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                           new BinaryOperator("BusinessPartnerAccountGroup", _locPurchaseInvoiceXPO.PayToVendor.BusinessPartnerAccountGroup)));

                                                if (_locJournalMapByBusinessPartners != null && _locJournalMapByBusinessPartners.Count() > 0)
                                                {
                                                    foreach (JournalMap _locJournalMapByBusinessPartner in _locJournalMapByBusinessPartners)
                                                    {
                                                        XPCollection<JournalMapLine> _locJournalMapLineByBusinessPartners = new XPCollection<JournalMapLine>
                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("JournalMap", _locJournalMapByBusinessPartner)));

                                                        if (_locJournalMapLineByBusinessPartners != null && _locJournalMapLineByBusinessPartners.Count() > 0)
                                                        {
                                                            foreach (JournalMapLine _locJournalMapLineByBusinessPartner in _locJournalMapLineByBusinessPartners)
                                                            {
                                                                AccountMap _locAccountMapByBusinessPartner = _currSession.FindObject<AccountMap>
                                                                                             (new GroupOperator(GroupOperatorType.And,
                                                                                             new BinaryOperator("Code", _locJournalMapLineByBusinessPartner.AccountMap.Code),
                                                                                             new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                             new BinaryOperator("OrderType", OrderType.Item),
                                                                                             new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                                                                                             new BinaryOperator("PostingMethodType", PostingMethodType.Tax)));

                                                                if (_locAccountMapByBusinessPartner != null)
                                                                {
                                                                    XPCollection<AccountMapLine> _locAccountMapLineByBusinessPartners = new XPCollection<AccountMapLine>
                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("AccountMap", _locAccountMapByBusinessPartner),
                                                                                                    new BinaryOperator("Active", true)));

                                                                    if (_locAccountMapLineByBusinessPartners != null && _locAccountMapLineByBusinessPartners.Count() > 0)
                                                                    {
                                                                        double _locTotalPriceDebit = 0;
                                                                        double _locTotalPriceCredit = 0;

                                                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLineByBusinessPartners)
                                                                        {
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                            {
                                                                                _locTotalPriceDebit = _locTxOfAmount;
                                                                            }
                                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                            {
                                                                                _locTotalPriceCredit = _locTxOfAmount;
                                                                            }

                                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                            {
                                                                                PostingDate = now,
                                                                                PostingType = PostingType.Purchase,
                                                                                PostingMethod = PostingMethod.InvoiceAP,
                                                                                Account = _locAccountMapLine.Account,
                                                                                Debit = _locTotalPriceDebit,
                                                                                Credit = _locTotalPriceCredit,
                                                                                PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                                                                                PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                                Company = _locPurchaseInvoiceXPO.Company,
                                                                            };
                                                                            _saveGeneralJournal.Save();
                                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                                            if (_locAccountMapLine.Account.Code != null)
                                                                            {
                                                                                ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                                if (_locCOA != null)
                                                                                {
                                                                                    _locCOA.Debit = _locCOA.Debit + _locTotalPriceDebit;
                                                                                    _locCOA.Credit = _locCOA.Credit + _locTotalPriceCredit;
                                                                                    _locCOA.Save();
                                                                                    _locCOA.Session.CommitTransaction();
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion JournalMapBusinessPartnerAccountGroup
                                    }

                                    if (_locDiscOfAmount > 0)
                                    {
                                        #region JournalMapDefaultDiscount
                                        XPQuery<PurchaseInvoiceLine> _purchaseInvoiceLinesQueryBasedDiscount = new XPQuery<PurchaseInvoiceLine>(_currSession);

                                        var _purchaseInvoiceLinesBasedDiscounts = from pil in _purchaseInvoiceLinesQueryBasedDiscount
                                                                                  where (pil.PurchaseInvoice == _locPurchaseInvoiceXPO && (pil.Status == Status.Progress || pil.Status == Status.Posted))
                                                                                  group pil by pil.Discount.DiscountAccountGroup into g
                                                                                  select new { DiscountAccountGroup = g.Key };

                                        if (_purchaseInvoiceLinesBasedDiscounts != null && _purchaseInvoiceLinesBasedDiscounts.Count() > 0)
                                        {
                                            foreach (var _purchaseInvoiceLinesBasedDiscount in _purchaseInvoiceLinesBasedDiscounts)
                                            {
                                                XPCollection<JournalMap> _locJournalMapByDiscounts = new XPCollection<JournalMap>
                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                       new BinaryOperator("BusinessPartnerAccountGroup", _locPurchaseInvoiceXPO.PayToVendor.BusinessPartnerAccountGroup)));

                                                if (_locJournalMapByDiscounts != null && _locJournalMapByDiscounts.Count() > 0)
                                                {
                                                    foreach (JournalMap _locJournalMapByBusinessPartner in _locJournalMapByDiscounts)
                                                    {
                                                        XPCollection<JournalMapLine> _locJournalMapLineByDiscounts = new XPCollection<JournalMapLine>
                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("JournalMap", _locJournalMapByBusinessPartner)));

                                                        if (_locJournalMapLineByDiscounts != null && _locJournalMapLineByDiscounts.Count() > 0)
                                                        {
                                                            foreach (JournalMapLine _locJournalMapLineByDiscount in _locJournalMapLineByDiscounts)
                                                            {
                                                                AccountMap _locAccountMapByDiscount = _currSession.FindObject<AccountMap>
                                                                                             (new GroupOperator(GroupOperatorType.And,
                                                                                             new BinaryOperator("Code", _locJournalMapLineByDiscount.AccountMap.Code),
                                                                                             new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                             new BinaryOperator("OrderType", OrderType.Item),
                                                                                             new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                                                                                             new BinaryOperator("PostingMethodType", PostingMethodType.Discount)));

                                                                if (_locAccountMapByDiscount != null)
                                                                {
                                                                    XPCollection<AccountMapLine> _locAccountMapLineByDiscounts = new XPCollection<AccountMapLine>
                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("AccountMap", _locAccountMapByDiscount),
                                                                                                    new BinaryOperator("Active", true)));

                                                                    if (_locAccountMapLineByDiscounts != null && _locAccountMapLineByDiscounts.Count() > 0)
                                                                    {
                                                                        double _locTotalPriceDebit = 0;
                                                                        double _locTotalPriceCredit = 0;

                                                                        foreach (AccountMapLine _locAccountMapLineByDiscount in _locAccountMapLineByDiscounts)
                                                                        {
                                                                            if (_locAccountMapLineByDiscount.AccountCharge == AccountCharge.Debit)
                                                                            {
                                                                                _locTotalPriceDebit = _locDiscOfAmount;
                                                                            }
                                                                            if (_locAccountMapLineByDiscount.AccountCharge == AccountCharge.Credit)
                                                                            {
                                                                                _locTotalPriceCredit = _locDiscOfAmount;
                                                                            }

                                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                            {
                                                                                PostingDate = now,
                                                                                PostingType = PostingType.Purchase,
                                                                                PostingMethod = PostingMethod.InvoiceAP,
                                                                                Account = _locAccountMapLineByDiscount.Account,
                                                                                Debit = _locTotalPriceDebit,
                                                                                Credit = _locTotalPriceCredit,
                                                                                PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                                                                                PurchaseInvoice = _locPurchaseInvoiceXPO,
                                                                                Company = _locPurchaseInvoiceXPO.Company,
                                                                            };
                                                                            _saveGeneralJournal.Save();
                                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                                            if (_locAccountMapLineByDiscount.Account.Code != null)
                                                                            {
                                                                                ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("Code", _locAccountMapLineByDiscount.Account.Code)));
                                                                                if (_locCOA != null)
                                                                                {
                                                                                    _locCOA.Debit = _locCOA.Debit + _locTotalPriceDebit;
                                                                                    _locCOA.Credit = _locCOA.Credit + _locTotalPriceCredit;
                                                                                    _locCOA.Save();
                                                                                    _locCOA.Session.CommitTransaction();
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        #endregion JournalMapDefaultDiscount
                                    }
                                }
                            }
                            #endregion CreateInvoiceAPJournal

                        }
                    }
                    #endregion CreateInvoiceAPJournalByItemsWithTax
                }

                #endregion CreateInvoiceAPJournal
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseInvoice ", ex.ToString());
            }
        }

        private void SetPaymentOutPlanOld(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO, PayableTransaction _payableTransaction)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                double _locTotalUnitPrice = _locPurchaseInvoiceXPO.Pay;
                if (_locPurchaseInvoiceXPO != null && _payableTransaction != null)
                {
                    //Membuat PaymentOutBasedOn PO dan PI
                    if (_locPurchaseInvoiceXPO.Outstanding > 0)
                    {
                        PaymentOutPlan _savePaymentOutPlan = new PaymentOutPlan(_currSession)
                        {
                            PaymentMethod = _locPurchaseInvoiceXPO.PaymentMethod,
                            EstimatedDate = _locPurchaseInvoiceXPO.EstimatedDate,
                            Plan = _locPurchaseInvoiceXPO.Pay,
                            Outstanding = _locPurchaseInvoiceXPO.Outstanding - _locPurchaseInvoiceXPO.Pay,
                            PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                            PurchaseInvoice = _locPurchaseInvoiceXPO,
                            PayableTransaction = _payableTransaction,
                            Company = _locPurchaseInvoiceXPO.Company,
                        };
                        _savePaymentOutPlan.Save();
                        _savePaymentOutPlan.Session.CommitTransaction();
                    }
                    else
                    {
                        PaymentOutPlan _savePaymentOutPlan = new PaymentOutPlan(_currSession)
                        {
                            PaymentMethod = _locPurchaseInvoiceXPO.PaymentMethod,
                            EstimatedDate = _locPurchaseInvoiceXPO.EstimatedDate,
                            Plan = _locPurchaseInvoiceXPO.Pay,
                            Outstanding = _locPurchaseInvoiceXPO.MaxPay - _locPurchaseInvoiceXPO.Pay,
                            PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                            PurchaseInvoice = _locPurchaseInvoiceXPO,
                            PayableTransaction = _payableTransaction,
                            Company = _locPurchaseInvoiceXPO.Company,
                        };
                        _savePaymentOutPlan.Save();
                        _savePaymentOutPlan.Session.CommitTransaction();
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseInvoice ", ex.ToString());
            }
        }

        private void SetRemainAmountOld(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locPurchaseInvoiceXPO != null)
                {
                    double _locPlan = 0;
                    double _locOutstanding = 0;
                    double _locMaxPay = 0;
                    bool _locActivationPosting = false;
                    Status _locStatus = Status.Posted;

                    //#region RemainAmountWithPostedCount!=0

                    //if (_locPurchaseInvoiceXPO.PostedCount > 0)
                    //{
                    //    _locMaxPay = _locPurchaseInvoiceXPO.MaxPay - (_locPurchaseInvoiceXPO.DP_Amount + _locPurchaseInvoiceXPO.CM_Amount);
                    //    if (_locPurchaseInvoiceXPO.Outstanding > 0 && _locMaxPay != _locPurchaseInvoiceXPO.Plan)
                    //    {
                    //        if (_locPurchaseInvoiceXPO.Pay > 0)
                    //        {
                    //            _locPlan = _locPurchaseInvoiceXPO.Plan + _locPurchaseInvoiceXPO.Pay;
                    //            _locOutstanding = _locPurchaseInvoiceXPO.Outstanding - _locPurchaseInvoiceXPO.Pay;

                    //            if (_locOutstanding == 0 && _locPlan == _locMaxPay)
                    //            {
                    //                _locActivationPosting = true;
                    //                _locStatus = Status.Posted;
                    //            }

                    //            _locPurchaseInvoiceXPO.Plan = _locPlan;
                    //            _locPurchaseInvoiceXPO.Outstanding = _locOutstanding;
                    //            _locPurchaseInvoiceXPO.Status = _locStatus;
                    //            _locPurchaseInvoiceXPO.StatusDate = now;
                    //            _locPurchaseInvoiceXPO.ActivationPosting = _locActivationPosting;
                    //            _locPurchaseInvoiceXPO.PostedCount = _locPurchaseInvoiceXPO.PostedCount + 1;
                    //            _locPurchaseInvoiceXPO.Save();
                    //            _locPurchaseInvoiceXPO.Session.CommitTransaction();

                    //            if (_locPurchaseInvoiceXPO.Code != null)
                    //            {
                    //                PurchaseInvoice _locPurchaseInvoice = _currSession.FindObject<PurchaseInvoice>
                    //                                                (new GroupOperator(GroupOperatorType.And,
                    //                                                new BinaryOperator("Code", _locPurchaseInvoiceXPO.Code)));
                    //                if (_locPurchaseInvoice != null)
                    //                {
                    //                    if (_locPurchaseInvoice.Status == Status.Posted && _locPurchaseInvoice.ActivationPosting == true)
                    //                    {
                    //                        XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>
                    //                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                    //                                                                new BinaryOperator("PurchaseInvoice", _locPurchaseInvoice),
                    //                                                                new BinaryOperator("Select", true)));

                    //                        if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count() > 0)
                    //                        {
                    //                            foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                    //                            {
                    //                                _locPurchaseInvoiceLine.ActivationPosting = true;
                    //                                _locPurchaseInvoiceLine.Status = Status.Close;
                    //                                _locPurchaseInvoiceLine.Save();
                    //                                _locPurchaseInvoiceLine.Session.CommitTransaction();
                    //                            }
                    //                        }
                    //                    }
                    //                    else
                    //                    {
                    //                        XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>
                    //                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                    //                                                                new BinaryOperator("PurchaseInvoice", _locPurchaseInvoice),
                    //                                                                new BinaryOperator("Select", true)));

                    //                        if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count() > 0)
                    //                        {
                    //                            foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                    //                            {
                    //                                _locPurchaseInvoiceLine.ActivationPosting = true;
                    //                                _locPurchaseInvoiceLine.Status = Status.Posted;
                    //                                _locPurchaseInvoiceLine.Save();
                    //                                _locPurchaseInvoiceLine.Session.CommitTransaction();
                    //                            }
                    //                        }
                    //                    }
                    //                }
                    //            }
                    //        }
                    //    }
                    //}
                    //#endregion RemainAmountWithPostedCount!=0

                    //#region RemainAmountWithPostedCount=0
                    //else
                    //{
                    //    if (_locPurchaseInvoiceXPO.Pay > 0)
                    //    {
                    //        _locPlan = _locPurchaseInvoiceXPO.Pay;
                    //        _locMaxPay = _locPurchaseInvoiceXPO.MaxPay - (_locPurchaseInvoiceXPO.DP_Amount + _locPurchaseInvoiceXPO.CM_Amount);
                    //        _locOutstanding = _locMaxPay - _locPlan;

                    //        if (_locPlan == _locMaxPay && _locOutstanding == 0)
                    //        {
                    //            _locStatus = Status.Posted;
                    //            _locActivationPosting = true;
                    //        }
                    //        //_locPurchaseInvoiceXPO.DP_Posting = true;
                    //        _locPurchaseInvoiceXPO.Plan = _locPlan;
                    //        _locPurchaseInvoiceXPO.Outstanding = _locOutstanding;
                    //        _locPurchaseInvoiceXPO.Status = _locStatus;
                    //        _locPurchaseInvoiceXPO.StatusDate = now;
                    //        _locPurchaseInvoiceXPO.ActivationPosting = _locActivationPosting;
                    //        _locPurchaseInvoiceXPO.PostedCount = _locPurchaseInvoiceXPO.PostedCount + 1;
                    //        _locPurchaseInvoiceXPO.Save();
                    //        _locPurchaseInvoiceXPO.Session.CommitTransaction();

                    //        if (_locPurchaseInvoiceXPO.Code != null)
                    //        {
                    //            PurchaseInvoice _locPurchaseInvoice = _currSession.FindObject<PurchaseInvoice>
                    //                                            (new GroupOperator(GroupOperatorType.And,
                    //                                            new BinaryOperator("Code", _locPurchaseInvoiceXPO.Code)));
                    //            if (_locPurchaseInvoice != null)
                    //            {
                    //                if (_locPurchaseInvoice.Status == Status.Posted && _locPurchaseInvoice.ActivationPosting == true)
                    //                {
                    //                    XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>
                    //                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                    //                                                            new BinaryOperator("PurchaseInvoice", _locPurchaseInvoice),
                    //                                                            new BinaryOperator("Select", true)));

                    //                    if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count() > 0)
                    //                    {
                    //                        foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                    //                        {
                    //                            _locPurchaseInvoiceLine.ActivationPosting = true;
                    //                            _locPurchaseInvoiceLine.Status = Status.Close;
                    //                            _locPurchaseInvoiceLine.Save();
                    //                            _locPurchaseInvoiceLine.Session.CommitTransaction();
                    //                        }
                    //                    }
                    //                }
                    //                else
                    //                {
                    //                    XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>
                    //                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                    //                                                            new BinaryOperator("PurchaseInvoice", _locPurchaseInvoice),
                    //                                                            new BinaryOperator("Select", true)));

                    //                    if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count() > 0)
                    //                    {
                    //                        foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                    //                        {
                    //                            _locPurchaseInvoiceLine.ActivationPosting = false;
                    //                            _locPurchaseInvoiceLine.Status = Status.Posted;
                    //                            _locPurchaseInvoiceLine.Save();
                    //                            _locPurchaseInvoiceLine.Session.CommitTransaction();
                    //                        }
                    //                    }
                    //                }
                    //            }
                    //        }
                    //    }

                    //}
                    //#endregion RemainAmountWithPostedCount=0
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseInvoice ", ex.ToString());
            }
        }

        private bool GetNullPurchaseInvoiceLineTaxOld(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            bool _result = false;
            try
            {
                double _currCount = 0;
                XPCollection<PurchaseInvoiceLine> _locPurchInvLines = new XPCollection<PurchaseInvoiceLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO)));

                if (_locPurchInvLines != null && _locPurchInvLines.Count() > 0)
                {
                    foreach (PurchaseInvoiceLine _locPurchInvLine in _locPurchInvLines)
                    {
                        if (_locPurchInvLine.Tax == null)
                        {
                            _currCount = _currCount + 1;

                        }
                        else
                        {
                            if (_locPurchInvLine.Tax != null)
                            {
                                if (_locPurchInvLine.Tax.TaxAccountGroup == null)
                                {
                                    _currCount = _currCount + 1;
                                }
                            }
                        }
                    }

                    if (_currCount > 0)
                    {
                        _result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseInvoice ", ex.ToString());
            }

            return _result;
        }

        private void SetInvoiceAPJournal20Feb2020(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locPayTUAmount = 0;
                double _locTUAmount = 0;
                double _locTxOfAmount = 0;
                double _locDiscOfAmount = 0;
                double _locDP = 0;
                PaymentOutPlan _locPaymentPlan = null;

                #region CreateInvoiceAPJournal

                //if (_locPurchaseInvoiceXPO.DP_Posting == true && _locPurchaseInvoiceXPO.DP_Amount > 0)
                //{

                //    #region CreateInvoiceAPJournalDownPayment

                //    if (_locPurchaseInvoiceXPO.Pay > 0)
                //    {

                //        if ((_locPurchaseInvoiceXPO.Pay <= _locPurchaseInvoiceXPO.DP_Amount))
                //        {
                //            _locDP = _locPurchaseInvoiceXPO.Pay;
                //        }
                //        else
                //        {
                //            _locDP = _locPurchaseInvoiceXPO.Pay - _locPurchaseInvoiceXPO.DP_Amount;
                //        }

                //        #region JournalMapCompanyAccountGroup
                //        if (_locPurchaseInvoiceXPO.Company != null)
                //        {
                //            if (_locPurchaseInvoiceXPO.Company.CompanyAccountGroup != null)
                //            {
                //                XPCollection<JournalMap> _locJournalMapByCompanys = new XPCollection<JournalMap>
                //                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                             new BinaryOperator("CompanyAccountGroup", _locPurchaseInvoiceXPO.Company.CompanyAccountGroup)));

                //                if (_locJournalMapByCompanys != null && _locJournalMapByCompanys.Count() > 0)
                //                {
                //                    foreach (JournalMap _locJournalMapByCompany in _locJournalMapByCompanys)
                //                    {
                //                        XPCollection<JournalMapLine> _locJournalMapLineByCompanys = new XPCollection<JournalMapLine>
                //                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                                             new BinaryOperator("JournalMap", _locJournalMapByCompany)));

                //                        if (_locJournalMapLineByCompanys != null && _locJournalMapLineByCompanys.Count() > 0)
                //                        {
                //                            foreach (JournalMapLine _locJournalMapLineByCompany in _locJournalMapLineByCompanys)
                //                            {
                //                                AccountMap _locAccountMapByBusinessPartner = _currSession.FindObject<AccountMap>
                //                                                                             (new GroupOperator(GroupOperatorType.And,
                //                                                                              new BinaryOperator("Code", _locJournalMapLineByCompany.AccountMap.Code),
                //                                                                              new BinaryOperator("PostingType", PostingType.Purchase),
                //                                                                              new BinaryOperator("OrderType", OrderType.Item),
                //                                                                              new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                //                                                                              new BinaryOperator("PostingMethodType", PostingMethodType.DownPayment)));

                //                                if (_locAccountMapByBusinessPartner != null)
                //                                {
                //                                    XPCollection<AccountMapLine> _locAccountMapLineByBusinessPartners = new XPCollection<AccountMapLine>
                //                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                                                         new BinaryOperator("AccountMap", _locAccountMapByBusinessPartner),
                //                                                                                                         new BinaryOperator("Active", true)));

                //                                    if (_locAccountMapLineByBusinessPartners != null && _locAccountMapLineByBusinessPartners.Count() > 0)
                //                                    {
                //                                        double _locTotalAmountDebit = 0;
                //                                        double _locTotalAmountCredit = 0;
                //                                        double _locTotalBalance = 0;

                //                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLineByBusinessPartners)
                //                                        {
                //                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                //                                            {
                //                                                _locTotalAmountDebit = _locDP;
                //                                            }
                //                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                //                                            {
                //                                                _locTotalAmountCredit = _locDP;
                //                                            }

                //                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                //                                            {
                //                                                PostingDate = now,
                //                                                PostingType = PostingType.Purchase,
                //                                                PostingMethod = PostingMethod.InvoiceAP,
                //                                                Account = _locAccountMapLine.Account,
                //                                                Debit = _locTotalAmountDebit,
                //                                                Credit = _locTotalAmountCredit,
                //                                                PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                //                                                PurchaseInvoice = _locPurchaseInvoiceXPO,
                //                                                Company = _locPurchaseInvoiceXPO.Company,
                //                                            };
                //                                            _saveGeneralJournal.Save();
                //                                            _saveGeneralJournal.Session.CommitTransaction();

                //                                            if (_locAccountMapLine.Account.Code != null)
                //                                            {
                //                                                ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                //                                                                         (new GroupOperator(GroupOperatorType.And,
                //                                                                          new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                //                                                if (_locCOA != null)
                //                                                {
                //                                                    if (_locCOA.BalanceType == BalanceType.Change)
                //                                                    {
                //                                                        if (_locCOA.AccountCharge == AccountCharge.Debit)
                //                                                        {
                //                                                            if (_locTotalAmountDebit > 0)
                //                                                            {
                //                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                //                                                            }
                //                                                            if (_locTotalAmountCredit > 0)
                //                                                            {
                //                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                //                                                            }
                //                                                        }
                //                                                        if (_locCOA.AccountCharge == AccountCharge.Credit)
                //                                                        {
                //                                                            if (_locTotalAmountDebit > 0)
                //                                                            {
                //                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                //                                                            }
                //                                                            if (_locTotalAmountCredit > 0)
                //                                                            {
                //                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                //                                                            }
                //                                                        }
                //                                                    }

                //                                                    _locCOA.Balance = _locTotalBalance;
                //                                                    _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                //                                                    _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                //                                                    _locCOA.Save();
                //                                                    _locCOA.Session.CommitTransaction();
                //                                                }
                //                                            }
                //                                        }
                //                                    }
                //                                }
                //                            }
                //                        }
                //                    }
                //                }
                //            }
                //        }
                //        #endregion JournalMapCompanyAccountGroup
                //        #region JournalMapBusinessPartnerAccountGroup
                //        if (_locPurchaseInvoiceXPO.PayToVendor != null)
                //        {
                //            if (_locPurchaseInvoiceXPO.PayToVendor.BusinessPartnerAccountGroup != null)
                //            {
                //                XPCollection<JournalMap> _locJournalMapByBusinessPartners = new XPCollection<JournalMap>
                //                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                             new BinaryOperator("BusinessPartnerAccountGroup", _locPurchaseInvoiceXPO.PayToVendor.BusinessPartnerAccountGroup)));

                //                if (_locJournalMapByBusinessPartners != null && _locJournalMapByBusinessPartners.Count() > 0)
                //                {
                //                    foreach (JournalMap _locJournalMapByBusinessPartner in _locJournalMapByBusinessPartners)
                //                    {
                //                        XPCollection<JournalMapLine> _locJournalMapLineByBusinessPartners = new XPCollection<JournalMapLine>
                //                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                                             new BinaryOperator("JournalMap", _locJournalMapByBusinessPartner)));

                //                        if (_locJournalMapLineByBusinessPartners != null && _locJournalMapLineByBusinessPartners.Count() > 0)
                //                        {
                //                            foreach (JournalMapLine _locJournalMapLineByBusinessPartner in _locJournalMapLineByBusinessPartners)
                //                            {
                //                                AccountMap _locAccountMapByBusinessPartner = _currSession.FindObject<AccountMap>
                //                                                                             (new GroupOperator(GroupOperatorType.And,
                //                                                                              new BinaryOperator("Code", _locJournalMapLineByBusinessPartner.AccountMap.Code),
                //                                                                              new BinaryOperator("PostingType", PostingType.Purchase),
                //                                                                              new BinaryOperator("OrderType", OrderType.Item),
                //                                                                              new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                //                                                                              new BinaryOperator("PostingMethodType", PostingMethodType.DownPayment)));

                //                                if (_locAccountMapByBusinessPartner != null)
                //                                {
                //                                    XPCollection<AccountMapLine> _locAccountMapLineByBusinessPartners = new XPCollection<AccountMapLine>
                //                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                                                         new BinaryOperator("AccountMap", _locAccountMapByBusinessPartner),
                //                                                                                                         new BinaryOperator("Active", true)));

                //                                    if (_locAccountMapLineByBusinessPartners != null && _locAccountMapLineByBusinessPartners.Count() > 0)
                //                                    {
                //                                        double _locTotalAmountDebit = 0;
                //                                        double _locTotalAmountCredit = 0;
                //                                        double _locTotalBalance = 0;

                //                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLineByBusinessPartners)
                //                                        {
                //                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                //                                            {
                //                                                _locTotalAmountDebit = _locDP;
                //                                            }
                //                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                //                                            {
                //                                                _locTotalAmountCredit = _locDP;
                //                                            }

                //                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                //                                            {
                //                                                PostingDate = now,
                //                                                PostingType = PostingType.Purchase,
                //                                                PostingMethod = PostingMethod.InvoiceAP,
                //                                                Account = _locAccountMapLine.Account,
                //                                                Debit = _locTotalAmountDebit,
                //                                                Credit = _locTotalAmountCredit,
                //                                                PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                //                                                PurchaseInvoice = _locPurchaseInvoiceXPO,
                //                                                Company = _locPurchaseInvoiceXPO.Company,
                //                                            };
                //                                            _saveGeneralJournal.Save();
                //                                            _saveGeneralJournal.Session.CommitTransaction();

                //                                            if (_locAccountMapLine.Account.Code != null)
                //                                            {
                //                                                ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                //                                                                         (new GroupOperator(GroupOperatorType.And,
                //                                                                          new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                //                                                if (_locCOA != null)
                //                                                {
                //                                                    if (_locCOA.BalanceType == BalanceType.Change)
                //                                                    {
                //                                                        if (_locCOA.AccountCharge == AccountCharge.Debit)
                //                                                        {
                //                                                            if (_locTotalAmountDebit > 0)
                //                                                            {
                //                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                //                                                            }
                //                                                            if (_locTotalAmountCredit > 0)
                //                                                            {
                //                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                //                                                            }
                //                                                        }
                //                                                        if (_locCOA.AccountCharge == AccountCharge.Credit)
                //                                                        {
                //                                                            if (_locTotalAmountDebit > 0)
                //                                                            {
                //                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                //                                                            }
                //                                                            if (_locTotalAmountCredit > 0)
                //                                                            {
                //                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                //                                                            }
                //                                                        }
                //                                                    }

                //                                                    _locCOA.Balance = _locTotalBalance;
                //                                                    _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                //                                                    _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                //                                                    _locCOA.Save();
                //                                                    _locCOA.Session.CommitTransaction();
                //                                                }
                //                                            }
                //                                        }
                //                                    }
                //                                }
                //                            }
                //                        }
                //                    }
                //                }
                //            }
                //        }
                //        #endregion JournalMapBusinessPartnerAccountGroup
                //    }

                //    #endregion CreateInvoiceAPJournalDownPayment
                //}
                //else
                //{
                //    #region CreateInvoiceAPJournalNormal
                //    XPQuery<PurchaseInvoiceLine> _PurchaseInvoiceLinesQueryBasedNormal = new XPQuery<PurchaseInvoiceLine>(_currSession);

                //    var _PurchaseInvoiceLineBasedNormals = from pil in _PurchaseInvoiceLinesQueryBasedNormal
                //                                           where (pil.PurchaseInvoice == _locPurchaseInvoiceXPO
                //                                           && (pil.Status == Status.Progress || pil.Status == Status.Posted)
                //                                           && pil.Select == true)
                //                                           group pil by pil.Item.ItemAccountGroup into g
                //                                           select new { ItemAccountGroup = g.Key };

                //    if (_PurchaseInvoiceLineBasedNormals != null && _PurchaseInvoiceLineBasedNormals.Count() > 0)
                //    {
                //        foreach (var _PurchaseInvoiceLineBasedNormal in _PurchaseInvoiceLineBasedNormals)
                //        {
                //            XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>(_currSession,
                //                                                                            new GroupOperator(GroupOperatorType.And,
                //                                                                            new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                //                                                                            new BinaryOperator("Item.ItemAccountGroup", _PurchaseInvoiceLineBasedNormal.ItemAccountGroup),
                //                                                                            new BinaryOperator("Select", true)));

                //            if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count() > 0)
                //            {
                //                foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                //                {
                //                    _locTUAmount = _locTUAmount + _locPurchaseInvoiceLine.TUAmount;
                //                }
                //            }

                //            #region PerhitunganPay
                //            if (_locPurchaseInvoiceXPO.PurchaseOrder != null)
                //            {
                //                if (_locPurchaseInvoiceXPO.PurchaseOrder.SplitInvoice == true)
                //                {

                //                    _locPaymentPlan = _currSession.FindObject<PaymentOutPlan>
                //                                                    (new GroupOperator(GroupOperatorType.And,
                //                                                     new BinaryOperator("PurchaseOrder", _locPurchaseInvoiceXPO.PurchaseOrder),
                //                                                     new BinaryOperator("DownPayment", true),
                //                                                     new BinaryOperator("Deduction", false)));
                //                    if (_locPaymentPlan != null)
                //                    {
                //                        if (_locPurchaseInvoiceXPO.PostedCount == 0)
                //                        {
                //                            if (_locTUAmount > _locPaymentPlan.Plan)
                //                            {
                //                                _locPayTUAmount = _locTUAmount - _locPaymentPlan.Plan;
                //                            }
                //                            else
                //                            {

                //                            }
                //                            //Buat else sisa 50% dari plan di kurangkan ke LocTUAmount

                //                        }
                //                        else
                //                        {
                //                            if (_locTUAmount <= _locPurchaseInvoiceXPO.Outstanding)
                //                            {
                //                                if (_locTUAmount > _locPaymentPlan.Plan)
                //                                {
                //                                    _locPayTUAmount = _locTUAmount - _locPaymentPlan.Plan;
                //                                }
                //                            }
                //                            else
                //                            {
                //                                _locPayTUAmount = _locPurchaseInvoiceXPO.Outstanding - _locPaymentPlan.Plan;
                //                            }
                //                        }

                //                    }
                //                    else
                //                    {
                //                        _locPayTUAmount = _locTUAmount;
                //                    }

                //                }
                //                else
                //                {
                //                    if (_locTUAmount > _locPurchaseInvoiceXPO.Outstanding)
                //                    {
                //                        _locPayTUAmount = _locTUAmount;
                //                    }
                //                    else
                //                    {
                //                        _locPayTUAmount = _locPurchaseInvoiceXPO.Outstanding;

                //                    }
                //                }
                //            }
                //            #endregion PerhitunganPay

                //            if (_locPayTUAmount > 0)
                //            {
                //                #region JournalMapItemAccountGroup
                //                XPCollection<JournalMap> _locJournalMaps = new XPCollection<JournalMap>
                //                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                           new BinaryOperator("ItemAccountGroup", _PurchaseInvoiceLineBasedNormal.ItemAccountGroup)));

                //                if (_locJournalMaps != null && _locJournalMaps.Count() > 0)
                //                {
                //                    foreach (JournalMap _locJournalMap in _locJournalMaps)
                //                    {
                //                        XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                //                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                        new BinaryOperator("JournalMap", _locJournalMap)));

                //                        if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                //                        {
                //                            foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                //                            {
                //                                AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                //                                                            (new GroupOperator(GroupOperatorType.And,
                //                                                             new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                //                                                             new BinaryOperator("PostingType", PostingType.Purchase),
                //                                                             new BinaryOperator("OrderType", OrderType.Item),
                //                                                             new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                //                                                             new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                //                                if (_locAccountMap != null)
                //                                {
                //                                    XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                //                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                                        new BinaryOperator("AccountMap", _locAccountMap),
                //                                                                                        new BinaryOperator("Active", true)));

                //                                    if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                //                                    {
                //                                        double _locTotalAmountDebit = 0;
                //                                        double _locTotalAmountCredit = 0;
                //                                        double _locTotalBalance = 0;

                //                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                //                                        {
                //                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                //                                            {
                //                                                _locTotalAmountDebit = _locPayTUAmount;
                //                                            }
                //                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                //                                            {
                //                                                _locTotalAmountCredit = _locPayTUAmount;
                //                                            }

                //                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                //                                            {
                //                                                PostingDate = now,
                //                                                PostingType = PostingType.Purchase,
                //                                                PostingMethod = PostingMethod.InvoiceAP,
                //                                                Account = _locAccountMapLine.Account,
                //                                                Debit = _locTotalAmountDebit,
                //                                                Credit = _locTotalAmountCredit,
                //                                                PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                //                                                PurchaseInvoice = _locPurchaseInvoiceXPO,
                //                                                Company = _locPurchaseInvoiceXPO.Company,
                //                                            };
                //                                            _saveGeneralJournal.Save();
                //                                            _saveGeneralJournal.Session.CommitTransaction();

                //                                            if (_locAccountMapLine.Account.Code != null)
                //                                            {
                //                                                ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                //                                                                (new GroupOperator(GroupOperatorType.And,
                //                                                                new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                //                                                if (_locCOA != null)
                //                                                {
                //                                                    if (_locCOA.BalanceType == BalanceType.Change)
                //                                                    {
                //                                                        if (_locCOA.AccountCharge == AccountCharge.Debit)
                //                                                        {
                //                                                            if (_locTotalAmountDebit > 0)
                //                                                            {
                //                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                //                                                            }
                //                                                            if (_locTotalAmountCredit > 0)
                //                                                            {
                //                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                //                                                            }
                //                                                        }
                //                                                        if (_locCOA.AccountCharge == AccountCharge.Credit)
                //                                                        {
                //                                                            if (_locTotalAmountDebit > 0)
                //                                                            {
                //                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                //                                                            }
                //                                                            if (_locTotalAmountCredit > 0)
                //                                                            {
                //                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                //                                                            }
                //                                                        }
                //                                                    }

                //                                                    _locCOA.Balance = _locTotalBalance;
                //                                                    _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                //                                                    _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                //                                                    _locCOA.Save();
                //                                                    _locCOA.Session.CommitTransaction();
                //                                                }
                //                                            }
                //                                        }
                //                                    }
                //                                }
                //                            }
                //                        }
                //                    }
                //                }
                //                #endregion JournalMapItemAccountGroup

                //                #region JournalMapBusinessPartnerAccountGroup
                //                if (_locPurchaseInvoiceXPO.PayToVendor != null)
                //                {
                //                    if (_locPurchaseInvoiceXPO.PayToVendor.BusinessPartnerAccountGroup != null)
                //                    {
                //                        XPCollection<JournalMap> _locJournalMapByBusinessPartners = new XPCollection<JournalMap>
                //                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                                     new BinaryOperator("BusinessPartnerAccountGroup", _locPurchaseInvoiceXPO.PayToVendor.BusinessPartnerAccountGroup)));

                //                        if (_locJournalMapByBusinessPartners != null && _locJournalMapByBusinessPartners.Count() > 0)
                //                        {
                //                            foreach (JournalMap _locJournalMapByBusinessPartner in _locJournalMapByBusinessPartners)
                //                            {
                //                                XPCollection<JournalMapLine> _locJournalMapLineByBusinessPartners = new XPCollection<JournalMapLine>
                //                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                                                     new BinaryOperator("JournalMap", _locJournalMapByBusinessPartner)));

                //                                if (_locJournalMapLineByBusinessPartners != null && _locJournalMapLineByBusinessPartners.Count() > 0)
                //                                {
                //                                    foreach (JournalMapLine _locJournalMapLineByBusinessPartner in _locJournalMapLineByBusinessPartners)
                //                                    {
                //                                        AccountMap _locAccountMapByBusinessPartner = _currSession.FindObject<AccountMap>
                //                                                                                     (new GroupOperator(GroupOperatorType.And,
                //                                                                                      new BinaryOperator("Code", _locJournalMapLineByBusinessPartner.AccountMap.Code),
                //                                                                                      new BinaryOperator("PostingType", PostingType.Purchase),
                //                                                                                      new BinaryOperator("OrderType", OrderType.Item),
                //                                                                                      new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                //                                                                                      new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                //                                        if (_locAccountMapByBusinessPartner != null)
                //                                        {
                //                                            XPCollection<AccountMapLine> _locAccountMapLineByBusinessPartners = new XPCollection<AccountMapLine>
                //                                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                                                                 new BinaryOperator("AccountMap", _locAccountMapByBusinessPartner),
                //                                                                                                                 new BinaryOperator("Active", true)));

                //                                            if (_locAccountMapLineByBusinessPartners != null && _locAccountMapLineByBusinessPartners.Count() > 0)
                //                                            {
                //                                                double _locTotalAmountDebit = 0;
                //                                                double _locTotalAmountCredit = 0;
                //                                                double _locTotalBalance = 0;

                //                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLineByBusinessPartners)
                //                                                {
                //                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                //                                                    {
                //                                                        _locTotalAmountDebit = _locPayTUAmount;
                //                                                    }
                //                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                //                                                    {
                //                                                        _locTotalAmountCredit = _locPayTUAmount;
                //                                                    }

                //                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                //                                                    {
                //                                                        PostingDate = now,
                //                                                        PostingType = PostingType.Purchase,
                //                                                        PostingMethod = PostingMethod.InvoiceAP,
                //                                                        Account = _locAccountMapLine.Account,
                //                                                        Debit = _locTotalAmountDebit,
                //                                                        Credit = _locTotalAmountCredit,
                //                                                        PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                //                                                        PurchaseInvoice = _locPurchaseInvoiceXPO,
                //                                                        Company = _locPurchaseInvoiceXPO.Company,
                //                                                    };
                //                                                    _saveGeneralJournal.Save();
                //                                                    _saveGeneralJournal.Session.CommitTransaction();

                //                                                    if (_locAccountMapLine.Account.Code != null)
                //                                                    {
                //                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                //                                                                                 (new GroupOperator(GroupOperatorType.And,
                //                                                                                  new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                //                                                        if (_locCOA != null)
                //                                                        {
                //                                                            if (_locCOA.BalanceType == BalanceType.Change)
                //                                                            {
                //                                                                if (_locCOA.AccountCharge == AccountCharge.Debit)
                //                                                                {
                //                                                                    if (_locTotalAmountDebit > 0)
                //                                                                    {
                //                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                //                                                                    }
                //                                                                    if (_locTotalAmountCredit > 0)
                //                                                                    {
                //                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                //                                                                    }
                //                                                                }
                //                                                                if (_locCOA.AccountCharge == AccountCharge.Credit)
                //                                                                {
                //                                                                    if (_locTotalAmountDebit > 0)
                //                                                                    {
                //                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                //                                                                    }
                //                                                                    if (_locTotalAmountCredit > 0)
                //                                                                    {
                //                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                //                                                                    }
                //                                                                }
                //                                                            }

                //                                                            _locCOA.Balance = _locTotalBalance;
                //                                                            _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                //                                                            _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                //                                                            _locCOA.Save();
                //                                                            _locCOA.Session.CommitTransaction();
                //                                                        }
                //                                                    }
                //                                                }
                //                                            }
                //                                        }
                //                                    }
                //                                }
                //                            }
                //                        }
                //                    }
                //                }
                //                #endregion JournalMapBusinessPartnerAccountGroup


                //            }
                //        }
                //    }
                //    #endregion CreateInvoiceAPJournalNormal

                //    #region CreateInvoiceARWithTax
                //    XPQuery<PurchaseInvoiceLine> _PurchaseInvoiceLinesQueryBasedTax = new XPQuery<PurchaseInvoiceLine>(_currSession);

                //    var _PurchaseInvoiceLineBasedTaxs = from pil in _PurchaseInvoiceLinesQueryBasedNormal
                //                                        where (pil.PurchaseInvoice == _locPurchaseInvoiceXPO
                //                                        && (pil.Status == Status.Progress || pil.Status == Status.Posted)
                //                                        && pil.Select == true)
                //                                        group pil by pil.Tax.TaxAccountGroup into g
                //                                        select new { TaxAccountGroup = g.Key };

                //    if (_PurchaseInvoiceLineBasedNormals != null && _PurchaseInvoiceLineBasedNormals.Count() > 0)
                //    {
                //        foreach (var _PurchaseInvoiceLineBasedTax in _PurchaseInvoiceLineBasedTaxs)
                //        {
                //            XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>(_currSession,
                //                                                                            new GroupOperator(GroupOperatorType.And,
                //                                                                            new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                //                                                                            new BinaryOperator("Tax.TaxAccountGroup", _PurchaseInvoiceLineBasedTax.TaxAccountGroup),
                //                                                                            new BinaryOperator("Select", true)));

                //            if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count() > 0)
                //            {
                //                foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                //                {
                //                    _locTxOfAmount = _locTxOfAmount + _locPurchaseInvoiceLine.TxAmount;
                //                }
                //            }

                //            if (_locTxOfAmount > 0)
                //            {
                //                #region JournalMapTaxAccountGroup
                //                XPCollection<JournalMap> _locJournalMapByTaxs = new XPCollection<JournalMap>
                //                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                 new BinaryOperator("TaxAccountGroup", _PurchaseInvoiceLineBasedTax.TaxAccountGroup)));

                //                if (_locJournalMapByTaxs != null && _locJournalMapByTaxs.Count() > 0)
                //                {
                //                    foreach (JournalMap _locJournalMapByTax in _locJournalMapByTaxs)
                //                    {
                //                        XPCollection<JournalMapLine> _locJournalMapLineByTaxs = new XPCollection<JournalMapLine>
                //                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                                 new BinaryOperator("JournalMap", _locJournalMapByTax)));

                //                        if (_locJournalMapLineByTaxs != null && _locJournalMapLineByTaxs.Count() > 0)
                //                        {
                //                            foreach (JournalMapLine _locJournalMapLineByTax in _locJournalMapLineByTaxs)
                //                            {
                //                                AccountMap _locAccountMapByTax = _currSession.FindObject<AccountMap>
                //                                                                 (new GroupOperator(GroupOperatorType.And,
                //                                                                  new BinaryOperator("Code", _locJournalMapLineByTax.AccountMap.Code),
                //                                                                  new BinaryOperator("PostingType", PostingType.Purchase),
                //                                                                  new BinaryOperator("OrderType", OrderType.Item),
                //                                                                  new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                //                                                                  new BinaryOperator("PostingMethodType", PostingMethodType.Tax)));

                //                                if (_locAccountMapByTax != null)
                //                                {
                //                                    XPCollection<AccountMapLine> _locAccountMapLineByTaxs = new XPCollection<AccountMapLine>
                //                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                                             new BinaryOperator("AccountMap", _locAccountMapByTax),
                //                                                                                             new BinaryOperator("Active", true)));

                //                                    if (_locAccountMapLineByTaxs != null && _locAccountMapLineByTaxs.Count() > 0)
                //                                    {
                //                                        double _locTotalPriceDebit = 0;
                //                                        double _locTotalPriceCredit = 0;

                //                                        foreach (AccountMapLine _locAccountMapLineByTax in _locAccountMapLineByTaxs)
                //                                        {
                //                                            if (_locAccountMapLineByTax.AccountCharge == AccountCharge.Debit)
                //                                            {
                //                                                _locTotalPriceDebit = _locTxOfAmount;
                //                                            }
                //                                            if (_locAccountMapLineByTax.AccountCharge == AccountCharge.Credit)
                //                                            {
                //                                                _locTotalPriceCredit = _locTxOfAmount;
                //                                            }

                //                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                //                                            {
                //                                                PostingDate = now,
                //                                                PostingType = PostingType.Purchase,
                //                                                PostingMethod = PostingMethod.InvoiceAP,
                //                                                Account = _locAccountMapLineByTax.Account,
                //                                                Debit = _locTotalPriceDebit,
                //                                                Credit = _locTotalPriceCredit,
                //                                                PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                //                                                PurchaseInvoice = _locPurchaseInvoiceXPO,
                //                                                Company = _locPurchaseInvoiceXPO.Company,
                //                                            };
                //                                            _saveGeneralJournal.Save();
                //                                            _saveGeneralJournal.Session.CommitTransaction();

                //                                            if (_locAccountMapLineByTax.Account.Code != null)
                //                                            {
                //                                                ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                //                                                                         (new GroupOperator(GroupOperatorType.And,
                //                                                                          new BinaryOperator("Code", _locAccountMapLineByTax.Account.Code)));
                //                                                if (_locCOA != null)
                //                                                {
                //                                                    _locCOA.Debit = _locCOA.Debit + _locTotalPriceDebit;
                //                                                    _locCOA.Credit = _locCOA.Credit + _locTotalPriceCredit;
                //                                                    _locCOA.Save();
                //                                                    _locCOA.Session.CommitTransaction();
                //                                                }
                //                                            }
                //                                        }
                //                                    }
                //                                }
                //                            }
                //                        }
                //                    }
                //                }
                //                #endregion JournalMapTaxAccountGroup

                //                #region JournalMapBusinessPartnerAccountGroup
                //                if (_locPurchaseInvoiceXPO.PayToVendor != null)
                //                {
                //                    if (_locPurchaseInvoiceXPO.PayToVendor.BusinessPartnerAccountGroup != null)
                //                    {
                //                        XPCollection<JournalMap> _locJournalMapByBusinessPartners = new XPCollection<JournalMap>
                //                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                                     new BinaryOperator("BusinessPartnerAccountGroup", _locPurchaseInvoiceXPO.PayToVendor.BusinessPartnerAccountGroup)));

                //                        if (_locJournalMapByBusinessPartners != null && _locJournalMapByBusinessPartners.Count() > 0)
                //                        {
                //                            foreach (JournalMap _locJournalMapByBusinessPartner in _locJournalMapByBusinessPartners)
                //                            {
                //                                XPCollection<JournalMapLine> _locJournalMapLineByBusinessPartners = new XPCollection<JournalMapLine>
                //                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                                                     new BinaryOperator("JournalMap", _locJournalMapByBusinessPartner)));

                //                                if (_locJournalMapLineByBusinessPartners != null && _locJournalMapLineByBusinessPartners.Count() > 0)
                //                                {
                //                                    foreach (JournalMapLine _locJournalMapLineByBusinessPartner in _locJournalMapLineByBusinessPartners)
                //                                    {
                //                                        AccountMap _locAccountMapByBusinessPartner = _currSession.FindObject<AccountMap>
                //                                                                                     (new GroupOperator(GroupOperatorType.And,
                //                                                                                      new BinaryOperator("Code", _locJournalMapLineByBusinessPartner.AccountMap.Code),
                //                                                                                      new BinaryOperator("PostingType", PostingType.Purchase),
                //                                                                                      new BinaryOperator("OrderType", OrderType.Item),
                //                                                                                      new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                //                                                                                      new BinaryOperator("PostingMethodType", PostingMethodType.Tax)));

                //                                        if (_locAccountMapByBusinessPartner != null)
                //                                        {
                //                                            XPCollection<AccountMapLine> _locAccountMapLineByBusinessPartners = new XPCollection<AccountMapLine>
                //                                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                                                                 new BinaryOperator("AccountMap", _locAccountMapByBusinessPartner),
                //                                                                                                                 new BinaryOperator("Active", true)));

                //                                            if (_locAccountMapLineByBusinessPartners != null && _locAccountMapLineByBusinessPartners.Count() > 0)
                //                                            {
                //                                                double _locTotalPriceDebit = 0;
                //                                                double _locTotalPriceCredit = 0;

                //                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLineByBusinessPartners)
                //                                                {
                //                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                //                                                    {
                //                                                        _locTotalPriceDebit = _locTxOfAmount;
                //                                                    }
                //                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                //                                                    {
                //                                                        _locTotalPriceCredit = _locTxOfAmount;
                //                                                    }

                //                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                //                                                    {
                //                                                        PostingDate = now,
                //                                                        PostingType = PostingType.Purchase,
                //                                                        PostingMethod = PostingMethod.InvoiceAP,
                //                                                        Account = _locAccountMapLine.Account,
                //                                                        Debit = _locTotalPriceDebit,
                //                                                        Credit = _locTotalPriceCredit,
                //                                                        PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                //                                                        PurchaseInvoice = _locPurchaseInvoiceXPO,
                //                                                        Company = _locPurchaseInvoiceXPO.Company,
                //                                                    };
                //                                                    _saveGeneralJournal.Save();
                //                                                    _saveGeneralJournal.Session.CommitTransaction();

                //                                                    if (_locAccountMapLine.Account.Code != null)
                //                                                    {
                //                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                //                                                                        (new GroupOperator(GroupOperatorType.And,
                //                                                                        new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                //                                                        if (_locCOA != null)
                //                                                        {
                //                                                            _locCOA.Debit = _locCOA.Debit + _locTotalPriceDebit;
                //                                                            _locCOA.Credit = _locCOA.Credit + _locTotalPriceCredit;
                //                                                            _locCOA.Save();
                //                                                            _locCOA.Session.CommitTransaction();
                //                                                        }
                //                                                    }
                //                                                }
                //                                            }
                //                                        }
                //                                    }
                //                                }
                //                            }
                //                        }
                //                    }
                //                }
                //                #endregion JournalMapBusinessPartnerAccountGroup
                //            }
                //        }
                //    }
                //    #endregion CreateInvoiceARWithTax

                //    #region CreateInvoiceARWithDiscount
                //    XPQuery<PurchaseInvoiceLine> _PurchaseInvoiceLinesQueryBasedDiscount = new XPQuery<PurchaseInvoiceLine>(_currSession);

                //    var _PurchaseInvoiceLineBasedDiscounts = from pil in _PurchaseInvoiceLinesQueryBasedTax
                //                                             where (pil.PurchaseInvoice == _locPurchaseInvoiceXPO
                //                                             && (pil.Status == Status.Progress || pil.Status == Status.Posted)
                //                                             && pil.Select == true)
                //                                             group pil by pil.Discount.DiscountAccountGroup into g
                //                                             select new { DiscountAccountGroup = g.Key };

                //    if (_PurchaseInvoiceLineBasedDiscounts != null && _PurchaseInvoiceLineBasedDiscounts.Count() > 0)
                //    {
                //        foreach (var _PurchaseInvoiceLineBasedDiscount in _PurchaseInvoiceLineBasedDiscounts)
                //        {
                //            XPCollection<PurchaseInvoiceLine> _locPurchaseInvoiceLines = new XPCollection<PurchaseInvoiceLine>(_currSession,
                //                                                                            new GroupOperator(GroupOperatorType.And,
                //                                                                            new BinaryOperator("PurchaseInvoice", _locPurchaseInvoiceXPO),
                //                                                                            new BinaryOperator("Discount.DiscountAccountGroup", _PurchaseInvoiceLineBasedDiscount.DiscountAccountGroup),
                //                                                                            new BinaryOperator("Select", true)));

                //            if (_locPurchaseInvoiceLines != null && _locPurchaseInvoiceLines.Count() > 0)
                //            {
                //                foreach (PurchaseInvoiceLine _locPurchaseInvoiceLine in _locPurchaseInvoiceLines)
                //                {
                //                    _locDiscOfAmount = _locDiscOfAmount + _locPurchaseInvoiceLine.DiscAmount;
                //                }
                //            }

                //            if (_locDiscOfAmount > 0)
                //            {
                //                #region JournalMapDiscountAccountGroup
                //                XPCollection<JournalMap> _locJournalMapByDiscounts = new XPCollection<JournalMap>
                //                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                 new BinaryOperator("DiscountAccountGroup", _PurchaseInvoiceLineBasedDiscount.DiscountAccountGroup)));

                //                if (_locJournalMapByDiscounts != null && _locJournalMapByDiscounts.Count() > 0)
                //                {
                //                    foreach (JournalMap _locJournalMapByDiscount in _locJournalMapByDiscounts)
                //                    {
                //                        XPCollection<JournalMapLine> _locJournalMapLineByDiscounts = new XPCollection<JournalMapLine>
                //                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                                 new BinaryOperator("JournalMap", _locJournalMapByDiscount)));

                //                        if (_locJournalMapLineByDiscounts != null && _locJournalMapLineByDiscounts.Count() > 0)
                //                        {
                //                            foreach (JournalMapLine _locJournalMapLineByDiscount in _locJournalMapLineByDiscounts)
                //                            {
                //                                AccountMap _locAccountMapByDiscount = _currSession.FindObject<AccountMap>
                //                                                                 (new GroupOperator(GroupOperatorType.And,
                //                                                                  new BinaryOperator("Code", _locJournalMapLineByDiscount.AccountMap.Code),
                //                                                                  new BinaryOperator("PostingType", PostingType.Purchase),
                //                                                                  new BinaryOperator("OrderType", OrderType.Item),
                //                                                                  new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                //                                                                  new BinaryOperator("PostingMethodType", PostingMethodType.Discount)));

                //                                if (_locAccountMapByDiscount != null)
                //                                {
                //                                    XPCollection<AccountMapLine> _locAccountMapLineByDiscounts = new XPCollection<AccountMapLine>
                //                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                                             new BinaryOperator("AccountMap", _locAccountMapByDiscount),
                //                                                                                             new BinaryOperator("Active", true)));

                //                                    if (_locAccountMapLineByDiscounts != null && _locAccountMapLineByDiscounts.Count() > 0)
                //                                    {
                //                                        double _locTotalPriceDebit = 0;
                //                                        double _locTotalPriceCredit = 0;

                //                                        foreach (AccountMapLine _locAccountMapLineByDiscount in _locAccountMapLineByDiscounts)
                //                                        {
                //                                            if (_locAccountMapLineByDiscount.AccountCharge == AccountCharge.Debit)
                //                                            {
                //                                                _locTotalPriceDebit = _locDiscOfAmount;
                //                                            }
                //                                            if (_locAccountMapLineByDiscount.AccountCharge == AccountCharge.Credit)
                //                                            {
                //                                                _locTotalPriceCredit = _locDiscOfAmount;
                //                                            }

                //                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                //                                            {
                //                                                PostingDate = now,
                //                                                PostingType = PostingType.Purchase,
                //                                                PostingMethod = PostingMethod.InvoiceAP,
                //                                                Account = _locAccountMapLineByDiscount.Account,
                //                                                Debit = _locTotalPriceDebit,
                //                                                Credit = _locTotalPriceCredit,
                //                                                PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                //                                                PurchaseInvoice = _locPurchaseInvoiceXPO,
                //                                                Company = _locPurchaseInvoiceXPO.Company,
                //                                            };
                //                                            _saveGeneralJournal.Save();
                //                                            _saveGeneralJournal.Session.CommitTransaction();

                //                                            if (_locAccountMapLineByDiscount.Account.Code != null)
                //                                            {
                //                                                ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                //                                                                         (new GroupOperator(GroupOperatorType.And,
                //                                                                          new BinaryOperator("Code", _locAccountMapLineByDiscount.Account.Code)));
                //                                                if (_locCOA != null)
                //                                                {
                //                                                    _locCOA.Debit = _locCOA.Debit + _locTotalPriceDebit;
                //                                                    _locCOA.Credit = _locCOA.Credit + _locTotalPriceCredit;
                //                                                    _locCOA.Save();
                //                                                    _locCOA.Session.CommitTransaction();
                //                                                }
                //                                            }
                //                                        }
                //                                    }
                //                                }
                //                            }
                //                        }
                //                    }
                //                }
                //                #endregion JournalMapDiscountAccountGroup

                //                #region JournalMapBusinessPartnerAccountGroup
                //                if (_locPurchaseInvoiceXPO.PayToVendor != null)
                //                {
                //                    if (_locPurchaseInvoiceXPO.PayToVendor.BusinessPartnerAccountGroup != null)
                //                    {
                //                        XPCollection<JournalMap> _locJournalMapByBusinessPartners = new XPCollection<JournalMap>
                //                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                                     new BinaryOperator("BusinessPartnerAccountGroup", _locPurchaseInvoiceXPO.PayToVendor.BusinessPartnerAccountGroup)));

                //                        if (_locJournalMapByBusinessPartners != null && _locJournalMapByBusinessPartners.Count() > 0)
                //                        {
                //                            foreach (JournalMap _locJournalMapByBusinessPartner in _locJournalMapByBusinessPartners)
                //                            {
                //                                XPCollection<JournalMapLine> _locJournalMapLineByBusinessPartners = new XPCollection<JournalMapLine>
                //                                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                                                     new BinaryOperator("JournalMap", _locJournalMapByBusinessPartner)));

                //                                if (_locJournalMapLineByBusinessPartners != null && _locJournalMapLineByBusinessPartners.Count() > 0)
                //                                {
                //                                    foreach (JournalMapLine _locJournalMapLineByBusinessPartner in _locJournalMapLineByBusinessPartners)
                //                                    {
                //                                        AccountMap _locAccountMapByBusinessPartner = _currSession.FindObject<AccountMap>
                //                                                                                     (new GroupOperator(GroupOperatorType.And,
                //                                                                                      new BinaryOperator("Code", _locJournalMapLineByBusinessPartner.AccountMap.Code),
                //                                                                                      new BinaryOperator("PostingType", PostingType.Purchase),
                //                                                                                      new BinaryOperator("OrderType", OrderType.Item),
                //                                                                                      new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                //                                                                                      new BinaryOperator("PostingMethodType", PostingMethodType.Discount)));

                //                                        if (_locAccountMapByBusinessPartner != null)
                //                                        {
                //                                            XPCollection<AccountMapLine> _locAccountMapLineByBusinessPartners = new XPCollection<AccountMapLine>
                //                                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                //                                                                                                                 new BinaryOperator("AccountMap", _locAccountMapByBusinessPartner),
                //                                                                                                                 new BinaryOperator("Active", true)));

                //                                            if (_locAccountMapLineByBusinessPartners != null && _locAccountMapLineByBusinessPartners.Count() > 0)
                //                                            {
                //                                                double _locTotalPriceDebit = 0;
                //                                                double _locTotalPriceCredit = 0;

                //                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLineByBusinessPartners)
                //                                                {
                //                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                //                                                    {
                //                                                        _locTotalPriceDebit = _locDiscOfAmount;
                //                                                    }
                //                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                //                                                    {
                //                                                        _locTotalPriceCredit = _locDiscOfAmount;
                //                                                    }

                //                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                //                                                    {
                //                                                        PostingDate = now,
                //                                                        PostingType = PostingType.Purchase,
                //                                                        PostingMethod = PostingMethod.InvoiceAP,
                //                                                        Account = _locAccountMapLine.Account,
                //                                                        Debit = _locTotalPriceDebit,
                //                                                        Credit = _locTotalPriceCredit,
                //                                                        PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                //                                                        PurchaseInvoice = _locPurchaseInvoiceXPO,
                //                                                        Company = _locPurchaseInvoiceXPO.Company,
                //                                                    };
                //                                                    _saveGeneralJournal.Save();
                //                                                    _saveGeneralJournal.Session.CommitTransaction();

                //                                                    if (_locAccountMapLine.Account.Code != null)
                //                                                    {
                //                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                //                                                                                 (new GroupOperator(GroupOperatorType.And,
                //                                                                                  new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                //                                                        if (_locCOA != null)
                //                                                        {
                //                                                            _locCOA.Debit = _locCOA.Debit + _locTotalPriceDebit;
                //                                                            _locCOA.Credit = _locCOA.Credit + _locTotalPriceCredit;
                //                                                            _locCOA.Save();
                //                                                            _locCOA.Session.CommitTransaction();
                //                                                        }
                //                                                    }
                //                                                }
                //                                            }
                //                                        }
                //                                    }
                //                                }
                //                            }
                //                        }
                //                    }
                //                }
                //                #endregion JournalMapBusinessPartnerAccountGroup

                //            }
                //        }
                //    }
                //    #endregion CreateInvoiceARWithDiscount
                //}

                #endregion CreateInvoiceAPJournal
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseInvoice ", ex.ToString());
            }
        }

        private void SetPayableTransaction20Feb2020(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _locSignCode = null;
                double _locTotalUnitPrice = _locPurchaseInvoiceXPO.Pay;
                if (_locPurchaseInvoiceXPO != null)
                {
                    if (_locPurchaseInvoiceXPO.PayToVendor != null)
                    {
                        if (_locPurchaseInvoiceXPO.PayToVendor.Code != null)
                        {
                            BusinessPartner _locBusinessPartner = _currSession.FindObject<BusinessPartner>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Code", _locPurchaseInvoiceXPO.PayToVendor.Code)));


                            if (_locBusinessPartner != null)
                            {
                                _locSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.PayableTransaction);
                                if (_locSignCode != null)
                                {
                                    PayableTransaction _savePayableTransaction = new PayableTransaction(_currSession)
                                    {
                                        PostingType = PostingType.Purchase,
                                        PostingMethod = PostingMethod.Payment,
                                        PaymentMethod = _locPurchaseInvoiceXPO.PaymentMethod,
                                        EstimatedDate = _locPurchaseInvoiceXPO.EstimatedDate,
                                        SignCode = _locSignCode,
                                        PurchaseOrder = _locPurchaseInvoiceXPO.PurchaseOrder,
                                        PurchaseInvoice = _locPurchaseInvoiceXPO,
                                        CompanyDefault = _locPurchaseInvoiceXPO.Company,
                                    };
                                    _savePayableTransaction.Save();
                                    _savePayableTransaction.Session.CommitTransaction();

                                    PayableTransaction _locPayableTransaction = _currSession.FindObject<PayableTransaction>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("SignCode", _locSignCode)));
                                    if (_locPayableTransaction != null)
                                    {
                                        //Create Mapping for BusinessPartner
                                        if (_locBusinessPartner.BusinessPartnerAccountGroup != null)
                                        {
                                            PayableTransactionLine _savePayableTransactionLine = new PayableTransactionLine(_currSession)
                                            {
                                                PostingType = PostingType.Purchase,
                                                PostingMethod = PostingMethod.Payment,
                                                OpenVendor = true,
                                                Vendor = _locBusinessPartner,
                                                BankAccount = _locPurchaseInvoiceXPO.BankAccount,
                                                PayableTransaction = _locPayableTransaction,
                                                CloseCredit = true,
                                                Debit = _locPurchaseInvoiceXPO.Pay,
                                                CompanyDefault = _locPayableTransaction.CompanyDefault,
                                            };
                                            _savePayableTransactionLine.Save();
                                            _savePayableTransactionLine.Session.CommitTransaction();
                                        }

                                        //Create Mapping For CompanyDefault
                                        if (_locPayableTransaction.CompanyDefault != null)
                                        {
                                            BankAccount _locBankAccount = _currSession.FindObject<BankAccount>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("Company", _locPayableTransaction.CompanyDefault),
                                                                            new BinaryOperator("Default", true),
                                                                            new BinaryOperator("Active", true)));
                                            if (_locBankAccount != null)
                                            {
                                                PayableTransactionLine _savePayableTransactionLine = new PayableTransactionLine(_currSession)
                                                {
                                                    PostingType = PostingType.Purchase,
                                                    PostingMethod = PostingMethod.Payment,
                                                    OpenCompany = true,
                                                    Company = _locPayableTransaction.CompanyDefault,
                                                    BankAccount = _locBankAccount,
                                                    PayableTransaction = _locPayableTransaction,
                                                    CloseDebit = true,
                                                    Credit = _locPurchaseInvoiceXPO.Pay,
                                                    CompanyDefault = _locPayableTransaction.CompanyDefault,
                                                };
                                                _savePayableTransactionLine.Save();
                                                _savePayableTransactionLine.Session.CommitTransaction();
                                            }

                                        }

                                        //Tambahkan PaymentOutPlan
                                        SetPaymentOutPlan(_currSession, _locPurchaseInvoiceXPO, _locPayableTransaction);
                                    }
                                }
                            }
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseInvoice ", ex.ToString());
            }
        }

        private void SetRemainAmount20Feb2020(Session _currSession, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locPurchaseInvoiceXPO != null)
                {
                    double _locPlan = 0;
                    double _locOutstanding = 0;
                    double _locMaxPay = 0;
                    double _locDP_Amount = 0;
                    bool _locDP_Posting = false;
                    bool _locActivationPosting = false;
                    string _locNotification = null;
                    Status _locStatus = Status.Posted;

                    //#region RemainAmountWithPostedCount!=0

                    //if (_locPurchaseInvoiceXPO.PostedCount > 0)
                    //{
                    //    _locMaxPay = _locPurchaseInvoiceXPO.MaxPay - _locPurchaseInvoiceXPO.CM_Amount;
                    //    if (_locPurchaseInvoiceXPO.Outstanding > 0 && _locMaxPay != _locPurchaseInvoiceXPO.Plan)
                    //    {
                    //        if (_locPurchaseInvoiceXPO.Pay > 0)
                    //        {
                    //            if (_locPurchaseInvoiceXPO.PurchaseOrder != null)
                    //            {
                    //                if (_locPurchaseInvoiceXPO.PurchaseOrder.SplitInvoice == true)
                    //                {
                    //                    double _locPay = 0;

                    //                    if (_locPurchaseInvoiceXPO.DP_Posting == true && _locPurchaseInvoiceXPO.DP_Amount > 0)
                    //                    {
                    //                        if (_locPurchaseInvoiceXPO.Pay <= _locPurchaseInvoiceXPO.DP_Amount)
                    //                        {
                    //                            _locDP_Amount = _locPurchaseInvoiceXPO.DP_Amount - _locPurchaseInvoiceXPO.Pay;
                    //                        }
                    //                        else
                    //                        {
                    //                            _locDP_Amount = 0;
                    //                        }

                    //                        if (_locDP_Amount > 0)
                    //                        {
                    //                            _locDP_Posting = true;
                    //                        }
                    //                    }

                    //                    PaymentOutPlan _locPaymentPlan = _currSession.FindObject<PaymentOutPlan>
                    //                                                (new GroupOperator(GroupOperatorType.And,
                    //                                                 new BinaryOperator("PurchaseOrder", _locPurchaseInvoiceXPO.PurchaseOrder),
                    //                                                 new BinaryOperator("DownPayment", true),
                    //                                                 new BinaryOperator("Deduction", false)));
                    //                    if (_locPaymentPlan != null)
                    //                    {
                    //                        if (_locPurchaseInvoiceXPO.Pay > _locPaymentPlan.Plan)
                    //                        {
                    //                            _locPay = _locPurchaseInvoiceXPO.Pay - _locPaymentPlan.Plan;
                    //                        }
                    //                        _locPlan = _locPurchaseInvoiceXPO.Plan + _locPay;
                    //                        _locOutstanding = _locPurchaseInvoiceXPO.Outstanding - _locPay;
                    //                    }
                    //                    else
                    //                    {
                    //                        _locPlan = _locPurchaseInvoiceXPO.Plan + _locPurchaseInvoiceXPO.Pay;
                    //                        _locOutstanding = _locPurchaseInvoiceXPO.Outstanding - _locPurchaseInvoiceXPO.Pay;
                    //                    }
                    //                }
                    //                else
                    //                {
                    //                    if (_locPurchaseInvoiceXPO.DP_Posting == true && _locPurchaseInvoiceXPO.DP_Amount > 0)
                    //                    {
                    //                        if (_locPurchaseInvoiceXPO.Pay <= _locPurchaseInvoiceXPO.DP_Amount)
                    //                        {
                    //                            _locDP_Amount = _locPurchaseInvoiceXPO.DP_Amount - _locPurchaseInvoiceXPO.Pay;
                    //                        }
                    //                        else
                    //                        {
                    //                            _locDP_Amount = 0;
                    //                        }

                    //                        if (_locDP_Amount > 0)
                    //                        {
                    //                            _locDP_Posting = true;
                    //                        }
                    //                    }

                    //                    _locPlan = _locPurchaseInvoiceXPO.Plan + _locPurchaseInvoiceXPO.Pay;
                    //                    _locOutstanding = _locPurchaseInvoiceXPO.Outstanding - _locPurchaseInvoiceXPO.Pay;
                    //                }
                    //            }


                    //            if (_locOutstanding == 0 && _locPlan == _locMaxPay)
                    //            {
                    //                _locActivationPosting = true;
                    //                _locStatus = Status.Posted;
                    //            }

                    //            _locPurchaseInvoiceXPO.DP_Posting = _locDP_Posting;
                    //            _locPurchaseInvoiceXPO.DP_Amount = _locDP_Amount;
                    //            _locPurchaseInvoiceXPO.Plan = _locPlan;
                    //            _locPurchaseInvoiceXPO.Outstanding = _locOutstanding;
                    //            _locPurchaseInvoiceXPO.Status = _locStatus;
                    //            _locPurchaseInvoiceXPO.StatusDate = now;
                    //            _locPurchaseInvoiceXPO.ActivationPosting = _locActivationPosting;
                    //            _locPurchaseInvoiceXPO.PostedCount = _locPurchaseInvoiceXPO.PostedCount + 1;
                    //            _locPurchaseInvoiceXPO.Save();
                    //            _locPurchaseInvoiceXPO.Session.CommitTransaction();
                    //        }
                    //    }
                    //}
                    //#endregion RemainAmountWithPostedCount!=0

                    //#region RemainAmountWithPostedCount=0
                    //else
                    //{
                    //    if (_locPurchaseInvoiceXPO.Pay > 0)
                    //    {
                    //        if (_locPurchaseInvoiceXPO.PurchaseOrder != null)
                    //        {
                    //            if (_locPurchaseInvoiceXPO.PurchaseOrder.SplitInvoice == true)
                    //            {
                    //                double _locPay = 0;

                    //                if (_locPurchaseInvoiceXPO.DP_Posting == true && _locPurchaseInvoiceXPO.DP_Amount > 0)
                    //                {
                    //                    if (_locPurchaseInvoiceXPO.Pay <= _locPurchaseInvoiceXPO.DP_Amount)
                    //                    {
                    //                        _locDP_Amount = _locPurchaseInvoiceXPO.DP_Amount - _locPurchaseInvoiceXPO.Pay;
                    //                    }
                    //                    else
                    //                    {
                    //                        _locDP_Amount = 0;
                    //                    }

                    //                    if (_locDP_Amount > 0)
                    //                    {
                    //                        _locDP_Posting = true;
                    //                    }
                    //                }

                    //                PaymentOutPlan _locPaymentPlan = _currSession.FindObject<PaymentOutPlan>
                    //                                                (new GroupOperator(GroupOperatorType.And,
                    //                                                 new BinaryOperator("PurchaseOrder", _locPurchaseInvoiceXPO.PurchaseOrder),
                    //                                                 new BinaryOperator("DownPayment", true),
                    //                                                 new BinaryOperator("Deduction", false)));

                    //                if (_locPaymentPlan != null)
                    //                {
                    //                    if (_locPurchaseInvoiceXPO.Pay > _locPaymentPlan.Plan)
                    //                    {
                    //                        _locPay = _locPurchaseInvoiceXPO.Pay - _locPaymentPlan.Plan;
                    //                    }

                    //                    _locPlan = _locPay;
                    //                    _locMaxPay = _locPurchaseInvoiceXPO.MaxPay - (_locPurchaseInvoiceXPO.CM_Amount + _locPaymentPlan.Plan);
                    //                    _locOutstanding = _locMaxPay - _locPlan;
                    //                    _locNotification = "Deduction By DownPayment";
                    //                }
                    //                else
                    //                {
                    //                    _locPlan = _locPurchaseInvoiceXPO.Pay;
                    //                    _locMaxPay = _locPurchaseInvoiceXPO.MaxPay - _locPurchaseInvoiceXPO.CM_Amount;
                    //                    _locOutstanding = _locMaxPay - _locPlan;
                    //                }

                    //            }
                    //            else
                    //            {
                    //                if (_locPurchaseInvoiceXPO.DP_Posting == true && _locPurchaseInvoiceXPO.DP_Amount > 0)
                    //                {
                    //                    if (_locPurchaseInvoiceXPO.Pay <= _locPurchaseInvoiceXPO.DP_Amount)
                    //                    {
                    //                        _locDP_Amount = _locPurchaseInvoiceXPO.DP_Amount - _locPurchaseInvoiceXPO.Pay;
                    //                    }
                    //                    else
                    //                    {
                    //                        _locDP_Amount = 0;
                    //                    }

                    //                    if (_locDP_Amount > 0)
                    //                    {
                    //                        _locDP_Posting = true;
                    //                    }
                    //                }

                    //                _locPlan = _locPurchaseInvoiceXPO.Pay;
                    //                _locMaxPay = _locPurchaseInvoiceXPO.MaxPay - _locPurchaseInvoiceXPO.CM_Amount;
                    //                _locOutstanding = _locMaxPay - _locPlan;
                    //            }
                    //        }


                    //        if (_locPlan == _locMaxPay && _locOutstanding == 0)
                    //        {
                    //            _locStatus = Status.Posted;
                    //            _locActivationPosting = true;
                    //        }

                    //        _locPurchaseInvoiceXPO.DP_Posting = _locDP_Posting;
                    //        _locPurchaseInvoiceXPO.DP_Amount = _locDP_Amount;
                    //        _locPurchaseInvoiceXPO.Plan = _locPlan;
                    //        _locPurchaseInvoiceXPO.Outstanding = _locOutstanding;
                    //        _locPurchaseInvoiceXPO.Status = _locStatus;
                    //        _locPurchaseInvoiceXPO.StatusDate = now;
                    //        _locPurchaseInvoiceXPO.Notification = _locNotification;
                    //        _locPurchaseInvoiceXPO.ActivationPosting = _locActivationPosting;
                    //        _locPurchaseInvoiceXPO.PostedCount = _locPurchaseInvoiceXPO.PostedCount + 1;
                    //        _locPurchaseInvoiceXPO.Save();
                    //        _locPurchaseInvoiceXPO.Session.CommitTransaction();

                    //    }
                    //}
                    //#endregion RemainAmountWithPostedCount=0
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseInvoice ", ex.ToString());
            }
        }

        #endregion PostingMethodOld

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }


        #endregion Global Method

    }
}
