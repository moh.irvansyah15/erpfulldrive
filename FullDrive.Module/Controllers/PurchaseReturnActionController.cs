﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using System.Collections;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PurchaseReturnActionController : ViewController
    {
        private ChoiceActionItem _selectionListviewFilter;

        public PurchaseReturnActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            PurchaseReturnListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {

                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                PurchaseReturnListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void PurchaseReturnProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseReturn _locPurchaseReturnOS = (PurchaseReturn)_objectSpace.GetObject(obj);

                        if (_locPurchaseReturnOS != null)
                        {
                            if (_locPurchaseReturnOS.Code != null)
                            {
                                _currObjectId = _locPurchaseReturnOS.Code;

                                PurchaseReturn _locPurchaseReturnXPO = _currSession.FindObject<PurchaseReturn>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locPurchaseReturnXPO != null)
                                {
                                    if (_locPurchaseReturnXPO.Status == Status.Open || _locPurchaseReturnXPO.Status == Status.Progress)
                                    {
                                        if (_locPurchaseReturnXPO.Status == Status.Open)
                                        {
                                            _locPurchaseReturnXPO.Status = Status.Progress;
                                            _locPurchaseReturnXPO.StatusDate = now;
                                            _locPurchaseReturnXPO.Save();
                                            _locPurchaseReturnXPO.Session.CommitTransaction();
                                        }

                                        XPCollection<PurchaseReturnLine> _locPurchaseReturnLines = new XPCollection<PurchaseReturnLine>
                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO),
                                                                new BinaryOperator("Status", Status.Open)));

                                        if (_locPurchaseReturnLines != null && _locPurchaseReturnLines.Count > 0)
                                        {
                                            foreach (PurchaseReturnLine _locPurchaseReturnLine in _locPurchaseReturnLines)
                                            {
                                                _locPurchaseReturnLine.Status = Status.Progress;
                                                _locPurchaseReturnLine.StatusDate = now;
                                                _locPurchaseReturnLine.Save();
                                                _locPurchaseReturnLine.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                    SuccessMessageShow("Purchase Return has been successfully updated to progress");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Return Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data Purchase Return Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturn " + ex.ToString());
            }
        }

        private void PurchaseReturnPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseReturn _locPurchaseReturnOS = (PurchaseReturn)_objectSpace.GetObject(obj);

                        if (_locPurchaseReturnOS != null)
                        {
                            if (_locPurchaseReturnOS.Code != null)
                            {
                                _currObjectId = _locPurchaseReturnOS.Code;

                                PurchaseReturn _locPurchaseReturnXPO = _currSession.FindObject<PurchaseReturn>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locPurchaseReturnXPO != null)
                                {
                                    if (_locPurchaseReturnXPO.Status == Status.Progress || _locPurchaseReturnXPO.Status == Status.Posted)
                                    {
                                        SetInventoryTransferReplacedFullAndCreditMemo(_currSession, _locPurchaseReturnXPO);
                                        SetCreditMemo(_currSession, _locPurchaseReturnXPO);
                                        SetChangeStatusPurchaseReturnLine(_currSession, _locPurchaseReturnXPO);
                                        SetFinalStatus(_currSession, _locPurchaseReturnXPO);
                                    }

                                    SuccessMessageShow("Purchase Return has been successfully Posted");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Return Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data Purchase Return Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturn " + ex.ToString());
            }
        }

        private void PurchaseReturnListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(PurchaseReturn)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturn " + ex.ToString());
            }
        }

        #region Posting Method

        private void SetInventoryTransferReplacedFullAndCreditMemo(Session _currSession, PurchaseReturn _locPurchaseReturnXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _locDocCode = null;
                InventoryTransferOut _locInventoryTransferOut = null;

                XPCollection<PurchaseReturnLine> _locPurchaseReturnLines = new XPCollection<PurchaseReturnLine>(_currSession,
                                                                new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO)));

                if (_locPurchaseReturnLines != null && _locPurchaseReturnLines.Count() > 0)
                {
                    if (_locPurchaseReturnXPO.PurchaseOrder != null && _locPurchaseReturnXPO != null)
                    {
                        _locInventoryTransferOut = _currSession.FindObject<InventoryTransferOut>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO),
                                                    new BinaryOperator("PurchaseOrder", _locPurchaseReturnXPO.PurchaseOrder)));
                    }
                    else
                    {
                        _locInventoryTransferOut = _currSession.FindObject<InventoryTransferOut>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO)));
                    }


                    if (_locInventoryTransferOut != null)
                    {
                        #region UpdateInventoryTransfer

                        foreach (PurchaseReturnLine _locPurchaseReturnLine in _locPurchaseReturnLines)
                        {
                            if (_locPurchaseReturnLine.Status == Status.Progress || _locPurchaseReturnLine.Status == Status.Posted)
                            {
                                if (_locPurchaseReturnLine.Select == true)
                                {
                                    InventoryTransferOutLine _locInventoryTransferOutLine = _currSession.FindObject<InventoryTransferOutLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("InventoryTransferOut", _locInventoryTransferOut),
                                                                            new BinaryOperator("Item", _locPurchaseReturnLine.Item),
                                                                            new BinaryOperator("Location", _locPurchaseReturnLine.Location),
                                                                            new BinaryOperator("BinLocation", _locPurchaseReturnLine.BinLocation)));
                                    if (_locInventoryTransferOutLine == null)
                                    {
                                        InventoryTransferOutLine _locSaveDataInventoryTransferOutLine = new InventoryTransferOutLine(_currSession)
                                        {
                                            Item = _locPurchaseReturnLine.Item,
                                            Description = _locPurchaseReturnLine.Description,
                                            MxDQty = _locPurchaseReturnLine.DQty,
                                            MxDUOM = _locPurchaseReturnLine.DUOM,
                                            MxQty = _locPurchaseReturnLine.Qty,
                                            MxUOM = _locPurchaseReturnLine.UOM,
                                            ReturnType = _locPurchaseReturnLine.ReturnType,
                                            Location = _locPurchaseReturnLine.Location,
                                            BinLocation = _locPurchaseReturnLine.BinLocation,
                                            StockType = _locPurchaseReturnLine.StockType,
                                            EstimatedDate = _locPurchaseReturnLine.EstimatedDate,
                                            InventoryTransferOut = _locInventoryTransferOut,
                                        };
                                        _locSaveDataInventoryTransferOutLine.Save();
                                        _locSaveDataInventoryTransferOutLine.Session.CommitTransaction();
                                    }
                                }
                            }
                        }

                        #endregion UpdateInventoryTransfer
                    }
                    else
                    {
                        #region CreateNewInventoryTransfer

                        DocumentType _locDocumentTypeXPO = _currSession.FindObject<DocumentType>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("DirectionType", DirectionType.External),
                                                                     new BinaryOperator("InventoryMovingType", InventoryMovingType.Return),
                                                                     new BinaryOperator("ObjectList", ObjectList.InventoryTransferOut),
                                                                     new BinaryOperator("DocumentRule", DocumentRule.Vendor),
                                                                     new BinaryOperator("Active", true),
                                                                     new BinaryOperator("Default", true)));

                        if (_locDocumentTypeXPO != null)
                        {
                            _locDocCode = _globFunc.GetDocumentNumberingUnlockOptimisticRecord(_currSession.DataLayer, _locDocumentTypeXPO);

                            if (_locDocCode != null)
                            {
                                InventoryTransferOut _locSaveDataInventoryOutTransfer = new InventoryTransferOut(_currSession)
                                {
                                    TransferType = _locDocumentTypeXPO.TransferType,
                                    InventoryMovingType = _locDocumentTypeXPO.InventoryMovingType,
                                    ObjectList = _locDocumentTypeXPO.ObjectList,
                                    DocumentType = _locDocumentTypeXPO,
                                    DocNo = _locDocCode,
                                    BusinessPartner = _locPurchaseReturnXPO.BuyFromVendor,
                                    BusinessPartnerCountry = _locPurchaseReturnXPO.BuyFromVendor.Country,
                                    BusinessPartnerCity = _locPurchaseReturnXPO.BuyFromVendor.City,
                                    BusinessPartnerAddress = _locPurchaseReturnXPO.BuyFromAddress,
                                    EstimatedDate = _locPurchaseReturnXPO.EstimatedDate,
                                    PurchaseReturn = _locPurchaseReturnXPO,
                                    PurchaseOrder = _locPurchaseReturnXPO.PurchaseOrder,

                                };
                                _locSaveDataInventoryOutTransfer.Save();
                                _locSaveDataInventoryOutTransfer.Session.CommitTransaction();

                                InventoryTransferOut _locInventoryTransferOut2x = _currSession.FindObject<InventoryTransferOut>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("DocNo", _locDocCode)));

                                if (_locInventoryTransferOut2x != null)
                                {
                                    foreach (PurchaseReturnLine _locPurchaseReturnLine in _locPurchaseReturnLines)
                                    {
                                        if (_locPurchaseReturnLine.Status == Status.Progress || _locPurchaseReturnLine.Status == Status.Posted)
                                        {
                                            if (_locPurchaseReturnLine.Select == true)
                                            {
                                                InventoryTransferOutLine _locSaveDataInventoryTransferOutLine = new InventoryTransferOutLine(_currSession)
                                                {
                                                    Item = _locPurchaseReturnLine.Item,
                                                    Description = _locPurchaseReturnLine.Description,
                                                    MxDQty = _locPurchaseReturnLine.DQty,
                                                    MxDUOM = _locPurchaseReturnLine.DUOM,
                                                    MxQty = _locPurchaseReturnLine.Qty,
                                                    MxUOM = _locPurchaseReturnLine.UOM,
                                                    ReturnType = _locPurchaseReturnLine.ReturnType,
                                                    Location = _locPurchaseReturnLine.Location,
                                                    BinLocation = _locPurchaseReturnLine.BinLocation,
                                                    StockType = _locPurchaseReturnLine.StockType,
                                                    EstimatedDate = _locPurchaseReturnLine.EstimatedDate,
                                                    InventoryTransferOut = _locInventoryTransferOut2x,
                                                };
                                                _locSaveDataInventoryTransferOutLine.Save();
                                                _locSaveDataInventoryTransferOutLine.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        #endregion CreateNewInventoryTransfer
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturn " + ex.ToString());
            }
        }

        private void SetCreditMemo(Session _currSession, PurchaseReturn _locPurchaseReturnXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _locSignCode = null;
                CreditMemo _locCreditMemo = null;
                double _locUnitPrice = 0;

                //Grouping based on ReturnType

                XPQuery<PurchaseReturnLine> _purchReturnLineQuery = new XPQuery<PurchaseReturnLine>(_currSession);


                var _purchReturnLines = from prlQ in _purchReturnLineQuery
                                        where (prlQ.Select == true && prlQ.Status == Status.Progress
                                        && prlQ.ReturnType == ReturnType.CreditMemo
                                        && prlQ.PurchaseReturn == _locPurchaseReturnXPO)
                                        group prlQ by prlQ.ReturnType into g
                                        select new { ReturnType = g.Key };

                if (_purchReturnLines != null && _purchReturnLines.Count() > 0)
                {
                    foreach (var _purchReturnLine in _purchReturnLines)
                    {
                        XPCollection<PurchaseReturnLine> _locPurchaseReturnLines = new XPCollection<PurchaseReturnLine>(_currSession,
                                                                new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Select", true),
                                                                new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO),
                                                                new BinaryOperator("ReturnType", _purchReturnLine.ReturnType)));

                        if (_locPurchaseReturnLines != null && _locPurchaseReturnLines.Count() > 0)
                        {
                            if (_locPurchaseReturnXPO.PurchaseOrder != null && _locPurchaseReturnXPO != null)
                            {
                                _locCreditMemo = _currSession.FindObject<CreditMemo>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO),
                                                            new BinaryOperator("PurchaseOrder", _locPurchaseReturnXPO.PurchaseOrder)));
                            }
                            else
                            {
                                _locCreditMemo = _currSession.FindObject<CreditMemo>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO)));
                            }


                            if (_locCreditMemo != null)
                            {
                                #region UpdateCreditMemo

                                foreach (PurchaseReturnLine _locPurchaseReturnLine in _locPurchaseReturnLines)
                                {
                                    if (_locPurchaseReturnLine.Status == Status.Progress || _locPurchaseReturnLine.Status == Status.Posted)
                                    {
                                        if (_locPurchaseReturnLine.Select == true)
                                        {
                                            CreditMemoLine _locCreditMemoLine = _currSession.FindObject<CreditMemoLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("CreditMemo", _locCreditMemo),
                                                                                    new BinaryOperator("Item", _locPurchaseReturnLine.Item),
                                                                                    new BinaryOperator("Location", _locPurchaseReturnLine.Location),
                                                                                    new BinaryOperator("BinLocation", _locPurchaseReturnLine.BinLocation)));
                                            if (_locCreditMemoLine == null)
                                            {
                                                if (_locPurchaseReturnXPO.PurchaseOrder != null && _locPurchaseReturnLine.Item != null)
                                                {
                                                    _locUnitPrice = GetUnitPriceByPurchaseOrder(_currSession, _locPurchaseReturnXPO.PurchaseOrder, _locPurchaseReturnLine.Item);
                                                }

                                                CreditMemoLine _locSaveDataCreditMemoLine = new CreditMemoLine(_currSession)
                                                {
                                                    Item = _locPurchaseReturnLine.Item,
                                                    Description = _locPurchaseReturnLine.Description,
                                                    MxDQty = _locPurchaseReturnLine.DQty,
                                                    MxDUOM = _locPurchaseReturnLine.DUOM,
                                                    MxQty = _locPurchaseReturnLine.Qty,
                                                    MxUOM = _locPurchaseReturnLine.UOM,
                                                    DQty = _locPurchaseReturnLine.DQty,
                                                    DUOM = _locPurchaseReturnLine.DUOM,
                                                    Qty = _locPurchaseReturnLine.Qty,
                                                    UOM = _locPurchaseReturnLine.UOM,
                                                    TQty = _locPurchaseReturnLine.TQty,
                                                    UAmount = _locUnitPrice,
                                                    TUAmount = _locUnitPrice * _locPurchaseReturnLine.TQty,
                                                    Location = _locPurchaseReturnLine.Location,
                                                    BinLocation = _locPurchaseReturnLine.BinLocation,
                                                    StockType = _locPurchaseReturnLine.StockType,
                                                    CreditMemo = _locCreditMemo,
                                                };
                                                _locSaveDataCreditMemoLine.Save();
                                                _locSaveDataCreditMemoLine.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                }

                                #endregion UpdateCreditMemo
                            }
                            else
                            {
                                #region CreateNewCreditMemo

                                _locSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.CreditMemo);

                                if (_locSignCode != null)
                                {
                                    CreditMemo _locSaveDataCreditMemo = new CreditMemo(_currSession)
                                    {
                                        BuyFromVendor = _locPurchaseReturnXPO.BuyFromVendor,
                                        BuyFromAddress = _locPurchaseReturnXPO.BuyFromAddress,
                                        BuyFromCity = _locPurchaseReturnXPO.BuyFromCity,
                                        BuyFromContact = _locPurchaseReturnXPO.BuyFromContact,
                                        BuyFromCountry = _locPurchaseReturnXPO.BuyFromCountry,
                                        TOP = _locPurchaseReturnXPO.PurchaseOrder.TOP,
                                        SignCode = _locSignCode,
                                        PurchaseReturn = _locPurchaseReturnXPO,
                                        PurchaseOrder = _locPurchaseReturnXPO.PurchaseOrder,

                                    };
                                    _locSaveDataCreditMemo.Save();
                                    _locSaveDataCreditMemo.Session.CommitTransaction();

                                    CreditMemo _locCreditMemo2x = _currSession.FindObject<CreditMemo>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("SignCode", _locSignCode)));

                                    if (_locCreditMemo2x != null)
                                    {
                                        foreach (PurchaseReturnLine _locPurchaseReturnLine in _locPurchaseReturnLines)
                                        {
                                            if (_locPurchaseReturnLine.Status == Status.Progress || _locPurchaseReturnLine.Status == Status.Posted)
                                            {
                                                if (_locPurchaseReturnLine.Select == true)
                                                {
                                                    if (_locPurchaseReturnXPO.PurchaseOrder != null && _locPurchaseReturnLine.Item != null)
                                                    {
                                                        _locUnitPrice = GetUnitPriceByPurchaseOrder(_currSession, _locPurchaseReturnXPO.PurchaseOrder, _locPurchaseReturnLine.Item);
                                                    }

                                                    CreditMemoLine _locSaveDataCreditMemoLine = new CreditMemoLine(_currSession)
                                                    {
                                                        Item = _locPurchaseReturnLine.Item,
                                                        Description = _locPurchaseReturnLine.Description,
                                                        MxDQty = _locPurchaseReturnLine.DQty,
                                                        MxDUOM = _locPurchaseReturnLine.DUOM,
                                                        MxQty = _locPurchaseReturnLine.Qty,
                                                        MxUOM = _locPurchaseReturnLine.UOM,
                                                        DQty = _locPurchaseReturnLine.DQty,
                                                        DUOM = _locPurchaseReturnLine.DUOM,
                                                        Qty = _locPurchaseReturnLine.Qty,
                                                        UOM = _locPurchaseReturnLine.UOM,
                                                        TQty = _locPurchaseReturnLine.TQty,
                                                        UAmount = _locUnitPrice,
                                                        TUAmount = _locUnitPrice * _locPurchaseReturnLine.TQty,
                                                        Location = _locPurchaseReturnLine.Location,
                                                        BinLocation = _locPurchaseReturnLine.BinLocation,
                                                        StockType = _locPurchaseReturnLine.StockType,
                                                        CreditMemo = _locCreditMemo2x,
                                                    };
                                                    _locSaveDataCreditMemoLine.Save();
                                                    _locSaveDataCreditMemoLine.Session.CommitTransaction();

                                                }
                                            }
                                        }
                                    }
                                }

                                #endregion CreateNewInventoryTransfer
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturn " + ex.ToString());
            }
        }

        private void SetChangeStatusPurchaseReturnLine(Session _currSession, PurchaseReturn _locPurchaseReturnXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locPurchaseReturnXPO != null)
                {
                    XPCollection<PurchaseRequisitionLine> _locPurchaseReturnLines = new XPCollection<PurchaseRequisitionLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO)));
                    if (_locPurchaseReturnLines != null && _locPurchaseReturnLines.Count() > 0)
                    {
                        foreach (PurchaseRequisitionLine _locPurchaseReturnLine in _locPurchaseReturnLines)
                        {
                            if (_locPurchaseReturnLine.Select == true)
                            {
                                if (_locPurchaseReturnLine.Status == Status.Progress || _locPurchaseReturnLine.Status == Status.Posted)
                                {
                                    _locPurchaseReturnLine.ActivationPosting = true;
                                    _locPurchaseReturnLine.Status = Status.Close;
                                    _locPurchaseReturnLine.StatusDate = now;
                                    _locPurchaseReturnLine.Save();
                                    _locPurchaseReturnLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturn " + ex.ToString());
            }
        }

        private void SetFinalStatus(Session _currSession, PurchaseReturn _locPurchaseReturnXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locCountStatus = 0;

                if (_locPurchaseReturnXPO != null)
                {
                    XPCollection<PurchaseRequisitionLine> _locPurchaseReturnLines = new XPCollection<PurchaseRequisitionLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseReturn", _locPurchaseReturnXPO)));
                    if (_locPurchaseReturnLines != null && _locPurchaseReturnLines.Count() > 0)
                    {
                        foreach (PurchaseRequisitionLine _locPurchaseReturnLine in _locPurchaseReturnLines)
                        {
                            if (_locPurchaseReturnLine.Status != Status.Close)
                            {
                                _locCountStatus = _locCountStatus + 1;
                            }
                        }
                    }

                    if (_locCountStatus == 0)
                    {
                        _locPurchaseReturnXPO.Status = Status.Close;
                        _locPurchaseReturnXPO.StatusDate = now;
                        _locPurchaseReturnXPO.ActivationPosting = true;
                        _locPurchaseReturnXPO.Save();
                        _locPurchaseReturnXPO.Session.CommitTransaction();
                    }
                    else
                    {
                        _locPurchaseReturnXPO.Status = Status.Posted;
                        _locPurchaseReturnXPO.StatusDate = now;
                        _locPurchaseReturnXPO.Save();
                        _locPurchaseReturnXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturn " + ex.ToString());
            }
        }

        private double GetUnitPriceByPurchaseOrder(Session _currSession, PurchaseOrder _locPurchaseOrder, Item _locItem)
        {
            double _result = 0;
            try
            {
                if (_locPurchaseOrder != null)
                {
                    PurchaseOrderLine _locPurchaseOrderLine = _currSession.FindObject<PurchaseOrderLine>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseOrder", _locPurchaseOrder),
                                                            new BinaryOperator("Item", _locItem)));
                    if (_locPurchaseOrderLine != null)
                    {
                        _result = _locPurchaseOrderLine.UAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturn " + ex.ToString());
            }
            return _result;
        }

        #endregion Posting Method

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

    }
}
