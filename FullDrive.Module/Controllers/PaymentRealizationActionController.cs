﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.ConditionalAppearance;
using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PaymentRealizationActionController : ViewController
    {
        #region Default

        private ChoiceActionItem _selectionListviewFilter;
        public PaymentRealizationActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.

            #region FilterApproval
            PaymentRealizationListviewFilterApprovalSelectionAction.Items.Clear();
            foreach (object _currApprovalFilter in Enum.GetValues(typeof(CustomProcess.ApprovalFilter)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalFilter));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApprovalFilter), _currApprovalFilter);
                PaymentRealizationListviewFilterApprovalSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterApproval
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        private void PaymentRealizationProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectToProcess != null)
                {
                    foreach (Object obj in _objectToProcess)
                    {
                        PaymentRealization _locPaymentRealizationOS = (PaymentRealization)_objectSpace.GetObject(obj);

                        if (_locPaymentRealizationOS != null)
                        {
                            if (_locPaymentRealizationOS.Code != null)
                            {
                                _currObjectId = _locPaymentRealizationOS.Code;

                                PaymentRealization _locPaymentRealizationXPO = _currSession.FindObject<PaymentRealization>
                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                  new BinaryOperator("Code", _currObjectId)));

                                if (_locPaymentRealizationXPO != null)
                                {
                                    if (_locPaymentRealizationXPO.Status == Status.Open || _locPaymentRealizationXPO.Status == Status.Progress)
                                    {
                                        if (_locPaymentRealizationXPO.Status == Status.Open)
                                        {
                                            _locPaymentRealizationXPO.Status = Status.Progress;
                                            _locPaymentRealizationXPO.StatusDate = now;
                                            _locPaymentRealizationXPO.Save();
                                            _locPaymentRealizationXPO.Session.CommitTransaction();
                                        }

                                        XPCollection<PaymentRealizationLine> _locPaymentRealizationLines = new XPCollection<PaymentRealizationLine>
                                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                            new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO),
                                                                                                            new BinaryOperator("Status", Status.Open)));

                                        if (_locPaymentRealizationLines != null && _locPaymentRealizationLines.Count > 0)
                                        {
                                            foreach (PaymentRealizationLine _locPaymentRealizationLine in _locPaymentRealizationLines)
                                            {
                                                _locPaymentRealizationLine.Status = Status.Progress;
                                                _locPaymentRealizationLine.StatusDate = now;
                                                _locPaymentRealizationLine.Save();
                                                _locPaymentRealizationLine.Session.CommitTransaction();
                                            }
                                        }
                                    }

                                    SuccessMessageShow("PaymentRealization has successfully updated to progress");
                                }
                                else
                                {
                                    ErrorMessageShow("PaymentRealization Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("PaymentRealization Not Available");
                            }
                        }
                    }
                }
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PaymentRealization" + ex.ToString());
            }
        }

        private void PaymentRealizationPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PaymentRealization _locPaymentRealizationOS = (PaymentRealization)_objectSpace.GetObject(obj);

                        if (_locPaymentRealizationOS != null)
                        {
                            if (_locPaymentRealizationOS.Code != null)
                            {
                                _currObjectId = _locPaymentRealizationOS.Code;

                                PaymentRealization _locPaymentRealizationXPO = _currSession.FindObject<PaymentRealization>
                                                                               (new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Code", _currObjectId)));

                                if (_locPaymentRealizationXPO != null)
                                {
                                    if (_locPaymentRealizationXPO.Status == Status.Progress || _locPaymentRealizationXPO.Status == Status.Posted)
                                    {
                                        ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("EndApproval", true),
                                                                            new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO)));

                                        if (_locApprovalLineXPO != null)
                                        {
                                            SetJournalRealization(_currSession, _locPaymentRealizationXPO);
                                            SetStatusPaymentRealizationLine(_currSession, _locPaymentRealizationXPO);
                                            SetProcessCount(_currSession, _locPaymentRealizationXPO);
                                            SetStatusPaymentRealization(_currSession, _locPaymentRealizationXPO);
                                        }
                                        else
                                        {
                                            ErrorMessageShow("Data Approval Not Available");
                                        }
                                    }
                                    else
                                    {
                                        ErrorMessageShow("Data PaymentRealization Not Available");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data PaymentRealization Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data PaymentRealization Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Data PaymentRealization Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("BusinessObject = PaymentRealization" + ex.ToString());
            }
        }

        private void PaymentRealizationListviewFilterApprovalSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(PaymentRealization)))
                {
                    if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.Approval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal)));
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.EndApproval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal);
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.AllData)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved1", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal));
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PaymentRealization " + ex.ToString());
            }
        }

        //==== Code Only ====

        #region PostingForPaymentRealization

        private void SetStatusPaymentRealizationLine(Session _currSession, PaymentRealization _locPaymentRealizationXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locPaymentRealizationXPO != null)
                {
                    XPCollection<PaymentRealizationLine> _locPaymentRealizationLines = new XPCollection<PaymentRealizationLine>(_currSession,
                                                                         new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO)));

                    if (_locPaymentRealizationLines != null && _locPaymentRealizationLines.Count > 0)
                    {
                        foreach (PaymentRealizationLine _locPaymentRealizationLine in _locPaymentRealizationLines)
                        {
                            if (_locPaymentRealizationLine.Status == Status.Progress)
                            {
                                _locPaymentRealizationLine.Status = Status.Close;
                                _locPaymentRealizationLine.ActivationPosting = true;
                                _locPaymentRealizationLine.StatusDate = now;
                                _locPaymentRealizationLine.Save();
                                _locPaymentRealizationLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PaymentRealization " + ex.ToString());
            }
        }

        private void SetProcessCount(Session _currSession, PaymentRealization _locPaymentRealizationXPO)
        {
            try
            {
                if (_locPaymentRealizationXPO != null)
                {
                    XPCollection<PaymentRealizationLine> _locPaymentRealizationLines = new XPCollection<PaymentRealizationLine>(_currSession,
                                                                         new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO)));

                    if (_locPaymentRealizationLines != null && _locPaymentRealizationLines.Count > 0)
                    {
                        foreach (PaymentRealizationLine _locPaymentRealizationLine in _locPaymentRealizationLines)
                        {
                            if (_locPaymentRealizationLine.Status == Status.Close)
                            {
                                if (_locPaymentRealizationLine.RealizationAmount > 0)
                                {
                                    _locPaymentRealizationLine.ProcessCount = _locPaymentRealizationLine.ProcessCount + 1;
                                    _locPaymentRealizationLine.Save();
                                    _locPaymentRealizationLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PaymentRealization " + ex.ToString());
            }
        }

        private void SetStatusPaymentRealization(Session _currSession, PaymentRealization _locPaymentRealizationXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locCount = 0;

                if (_locPaymentRealizationXPO != null)
                {
                    if (_locPaymentRealizationXPO.Status == Status.Close)
                    {
                        SetCloseAllCAProcess(_currSession, _locPaymentRealizationXPO);
                        SuccessMessageShow(_locPaymentRealizationXPO.Code + " has been change successfully to Close");
                    }
                    else
                    {
                        XPCollection<PaymentRealizationLine> _locPaymentRealizationLines = new XPCollection<PaymentRealizationLine>(_currSession,
                                                                         new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO)));

                        if (_locPaymentRealizationLines != null && _locPaymentRealizationLines.Count() > 0)
                        {
                            foreach (PaymentRealizationLine _locPaymentRealizationLine in _locPaymentRealizationLines)
                            {
                                if (_locPaymentRealizationLine.Status == Status.Close)
                                {
                                    _locCount = _locCount + 1;
                                }
                            }

                            if (_locCount == _locPaymentRealizationLines.Count())
                            {
                                _locPaymentRealizationXPO.ActivationPosting = true;
                                _locPaymentRealizationXPO.Status = Status.Close;
                                _locPaymentRealizationXPO.StatusDate = now;
                                _locPaymentRealizationXPO.Save();
                                _locPaymentRealizationXPO.Session.CommitTransaction();
                            }
                            else
                            {
                                _locPaymentRealizationXPO.Status = Status.Posted;
                                _locPaymentRealizationXPO.StatusDate = now;
                                _locPaymentRealizationXPO.Save();
                                _locPaymentRealizationXPO.Session.CommitTransaction();
                            }

                            SetCloseAllCAProcess(_currSession, _locPaymentRealizationXPO);
                            SuccessMessageShow(_locPaymentRealizationXPO.Code + " has been change successfully to Close");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PaymentRealization " + ex.ToString());
            }
        }

        private void SetJournalRealization(Session _currSession, PaymentRealization _locPaymentRealizationXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locPRApproved = 0;
                double _locSetPRApproved = 0;
                double _locPRAmount = 0;
                double _locSetPRAmount = 0;

                if (_locPaymentRealizationXPO != null)
                {
                    #region JournalCashBonByCompany
                    XPQuery<PaymentRealizationLine> _PaymentRealizationLinesQueryBasedCompany = new XPQuery<PaymentRealizationLine>(_currSession);

                    var _PaymentRealizationLineByCompanys = from prl in _PaymentRealizationLinesQueryBasedCompany
                                                            where (prl.PaymentRealization == _locPaymentRealizationXPO
                                                            && prl.Company == _locPaymentRealizationXPO.Company
                                                            && (prl.Status == Status.Progress || prl.Status == Status.Posted))
                                                            group prl by prl.Company into g
                                                            select new { Company = g.Key };

                    if (_PaymentRealizationLineByCompanys != null)
                    {
                        foreach (var _PaymentRealizationLineByCompany in _PaymentRealizationLineByCompanys)
                        {
                            #region SetAmount
                            XPCollection<PaymentRealizationLine> _locPaymentRealizationLines = new XPCollection<PaymentRealizationLine>(_currSession,
                                                                                               new GroupOperator(GroupOperatorType.And,
                                                                                               new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO)));

                            if (_locPaymentRealizationLines != null && _locPaymentRealizationLines.Count() > 0)
                            {
                                foreach (PaymentRealizationLine _locPaymentRealizationLine in _locPaymentRealizationLines)
                                {
                                    _locPRApproved = _locPRApproved + _locPaymentRealizationLine.RealizationAmount;
                                }
                            }

                            _locSetPRApproved = _locPRApproved;
                            #endregion SetAmount

                            if (_locSetPRApproved > 0)
                            {
                                #region JournalMap
                                XPCollection<JournalMap> _locJournalMaps = new XPCollection<JournalMap>
                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("CompanyAccountGroup", _PaymentRealizationLineByCompany.Company.CompanyAccountGroup)));

                                if (_locJournalMaps != null && _locJournalMaps.Count() > 0)
                                {
                                    foreach (JournalMap _locJournalMap in _locJournalMaps)
                                    {
                                        XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("JournalMap", _locJournalMap)));

                                        if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                        {
                                            foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                            {
                                                AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                             new BinaryOperator("PostingType", PostingType.Advances),
                                                                             new BinaryOperator("OrderType", OrderType.Account),
                                                                             new BinaryOperator("PostingMethod", PostingMethod.CashRealization),
                                                                             new BinaryOperator("PostingMethodType", PostingMethodType.Advances)));

                                                if (_locAccountMap != null)
                                                {
                                                    XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                        new BinaryOperator("Active", true)));

                                                    if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                    {
                                                        double _locTotalAmountDebit = 0;
                                                        double _locTotalAmountCredit = 0;
                                                        double _locTotalBalance = 0;

                                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                        {
                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                            {
                                                                _locTotalAmountDebit = _locSetPRApproved;
                                                            }
                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                            {
                                                                _locTotalAmountCredit = _locSetPRApproved;
                                                            }

                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                            {
                                                                PostingDate = now,
                                                                PostingType = PostingType.Advances,
                                                                PostingMethod = PostingMethod.CashRealization,
                                                                Account = _locAccountMapLine.Account,
                                                                Debit = _locTotalAmountDebit,
                                                                Credit = _locTotalAmountCredit,
                                                                Company = _locPaymentRealizationXPO.Company,
                                                                PaymentRealization = _locPaymentRealizationXPO,
                                                            };
                                                            _saveGeneralJournal.Save();
                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                            if (_locAccountMapLine.Account.Code != null)
                                                            {
                                                                #region COA
                                                                ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Code", _locAccountMapLine.Account.Code)));

                                                                if (_locCOA != null)
                                                                {
                                                                    if (_locCOA.BalanceType == BalanceType.Change)
                                                                    {
                                                                        if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            if (_locTotalAmountDebit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                            }
                                                                            if (_locTotalAmountCredit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                            }
                                                                        }
                                                                        if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            if (_locTotalAmountDebit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                            }
                                                                            if (_locTotalAmountCredit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                            }
                                                                        }
                                                                    }

                                                                    _locCOA.Balance = _locTotalBalance;
                                                                    _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                    _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                    _locCOA.Save();
                                                                    _locCOA.Session.CommitTransaction();
                                                                }
                                                                #endregion COA
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion JournalMap
                            }
                        }
                    }
                    #endregion JournalCashBonByCompany

                    #region JournalCashBonByDepartement
                    XPQuery<PaymentRealizationLine> _PaymentRealizationLinesQueryBasedDepartment = new XPQuery<PaymentRealizationLine>(_currSession);

                    var _PaymentRealizationLineByDepartments = from cal in _PaymentRealizationLinesQueryBasedDepartment
                                                               where (cal.PaymentRealization == _locPaymentRealizationXPO
                                                               && cal.Department == _locPaymentRealizationXPO.Department
                                                               && (cal.Status == Status.Progress || cal.Status == Status.Posted))
                                                               group cal by cal.Department into g
                                                               select new { Department = g.Key };

                    if (_PaymentRealizationLineByDepartments != null)
                    {
                        foreach (var _PaymentRealizationLineByDepartment in _PaymentRealizationLineByDepartments)
                        {
                            #region SetAmount
                            XPCollection<PaymentRealizationLine> _locPaymentRealizationLines = new XPCollection<PaymentRealizationLine>(_currSession,
                                                                                 new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("PaymentRealization", _locPaymentRealizationXPO)));

                            if (_locPaymentRealizationLines != null && _locPaymentRealizationLines.Count() > 0)
                            {
                                foreach (PaymentRealizationLine _locPaymentRealizationLine in _locPaymentRealizationLines)
                                {
                                    _locPRAmount = _locPRAmount + _locPaymentRealizationLine.RealizationAmount;
                                }
                            }

                            _locSetPRAmount = _locPRAmount;
                            #endregion SetAmount

                            if (_locSetPRAmount > 0)
                            {
                                #region JournalMap
                                XPCollection<JournalMap> _locJournalMaps = new XPCollection<JournalMap>
                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("DepartmentAccountGroup", _PaymentRealizationLineByDepartment.Department.DepartmentAccountGroup)));

                                if (_locJournalMaps != null && _locJournalMaps.Count() > 0)
                                {
                                    foreach (JournalMap _locJournalMap in _locJournalMaps)
                                    {
                                        XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("JournalMap", _locJournalMap)));

                                        if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                        {
                                            foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                            {
                                                AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                             new BinaryOperator("PostingType", PostingType.Advances),
                                                                             new BinaryOperator("OrderType", OrderType.Account),
                                                                             new BinaryOperator("PostingMethod", PostingMethod.CashRealization),
                                                                             new BinaryOperator("PostingMethodType", PostingMethodType.Advances)));

                                                if (_locAccountMap != null)
                                                {
                                                    XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                        new BinaryOperator("Active", true)));

                                                    if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                    {
                                                        double _locTotalAmountDebit = 0;
                                                        double _locTotalAmountCredit = 0;
                                                        double _locTotalBalance = 0;

                                                        foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                        {
                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                            {
                                                                _locTotalAmountDebit = _locSetPRAmount;
                                                            }
                                                            if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                            {
                                                                _locTotalAmountCredit = _locSetPRAmount;
                                                            }

                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                            {
                                                                PostingDate = now,
                                                                PostingType = PostingType.Advances,
                                                                PostingMethod = PostingMethod.CashRealization,
                                                                Account = _locAccountMapLine.Account,
                                                                Debit = _locTotalAmountDebit,
                                                                Credit = _locTotalAmountCredit,
                                                                Company = _locPaymentRealizationXPO.Company,
                                                                PaymentRealization = _locPaymentRealizationXPO
                                                            };
                                                            _saveGeneralJournal.Save();
                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                            if (_locAccountMapLine.Account.Code != null)
                                                            {
                                                                #region COA
                                                                ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Code", _locAccountMapLine.Account.Code)));

                                                                if (_locCOA != null)
                                                                {
                                                                    if (_locCOA.BalanceType == BalanceType.Change)
                                                                    {
                                                                        if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            if (_locTotalAmountDebit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                            }
                                                                            if (_locTotalAmountCredit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                            }
                                                                        }
                                                                        if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            if (_locTotalAmountDebit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                            }
                                                                            if (_locTotalAmountCredit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                            }
                                                                        }
                                                                    }

                                                                    _locCOA.Balance = _locTotalBalance;
                                                                    _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                    _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                    _locCOA.Save();
                                                                    _locCOA.Session.CommitTransaction();
                                                                }
                                                                #endregion COA
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion JournalMap
                            }
                        }
                    }
                    #endregion JournalCashBonByDepartement
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PaymentRealization ", ex.ToString());
            }
        }

        private void SetCloseAllCAProcess(Session _currSession, PaymentRealization _locPaymentRealizationXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locPaymentRealizationXPO.CashAdvance != null)
                {
                    XPCollection<PaymentRealization> _locPaymentSlips = new XPCollection<PaymentRealization>(_currSession,
                                                                 new GroupOperator(GroupOperatorType.And,
                                                                 new BinaryOperator("CashAdvance", _locPaymentRealizationXPO.CashAdvance)));

                    if (_locPaymentSlips != null && _locPaymentSlips.Count > 0)
                    {
                        if (_locPaymentRealizationXPO.CashAdvance != null)
                        {
                            if (_locPaymentRealizationXPO.CashAdvance.Code != null)
                            {
                                CashAdvance _locCashAdvance = _currSession.FindObject<CashAdvance>
                                                              (new GroupOperator(GroupOperatorType.And,
                                                               new BinaryOperator("Code", _locPaymentRealizationXPO.CashAdvance.Code)));
                                if (_locCashAdvance != null)
                                {
                                    _locCashAdvance.Status = Status.Close;
                                    _locCashAdvance.StatusDate = now;
                                    _locCashAdvance.Save();
                                    _locCashAdvance.Session.CommitTransaction();
                                }

                                XPCollection<CashAdvanceLine> _locCashAdvanceLines = new XPCollection<CashAdvanceLine>
                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                      new BinaryOperator("CashAdvance", _locCashAdvance),
                                                                                      new BinaryOperator("Status", Status.Posted)));

                                if (_locCashAdvanceLines != null && _locCashAdvanceLines.Count > 0)
                                {
                                    foreach (CashAdvanceLine _locCashAdvanceLine in _locCashAdvanceLines)
                                    {
                                        _locCashAdvanceLine.Status = Status.Close;
                                        _locCashAdvanceLine.StatusDate = now;
                                        _locCashAdvanceLine.Save();
                                        _locCashAdvanceLine.Session.CommitTransaction();
                                    }
                                }


                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PaymentRealization " + ex.ToString());
            }
        }

        #endregion PostingForPaymentRealization

        #region GlobalMethod

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion GlobalMethod

    }
}
