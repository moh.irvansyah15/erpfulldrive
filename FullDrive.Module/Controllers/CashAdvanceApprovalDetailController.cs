﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using DevExpress.ExpressApp.ConditionalAppearance;
using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Model;
using System.Web.UI.WebControls;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

#region Email

using System.Net;
using System.Web;
using System.IO;
using System.Data;
using System.Web.UI;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.Configuration;

#endregion Email

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CashAdvanceApprovalDetailController : ViewController
    {
        #region Default

        private ChoiceActionItem _setApprovalLevel;
        public CashAdvanceApprovalDetailController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.

            TargetViewId = "CashAdvance_DetailView";
        }
        protected override void OnActivated()
        {
            TypeOfView = typeof(ObjectView);
            base.OnActivated();
            var _locUser = SecuritySystem.CurrentUserName;
            GlobalFunction _globalFunction = new GlobalFunction();
            Session _currentSession = null;

            if (this.ObjectSpace != null)
            {
                _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
            }

            if (_currentSession != null)
            {
                UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", _locUser));

                if (_locUserAccess != null)
                {
                    CashAdvanceApprovalDetailAction.Items.Clear();

                    XPCollection<ApplicationSetupDetail> _locAppSetupDetails = new XPCollection<ApplicationSetupDetail>
                                                                               (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("UserAccess", _locUserAccess),
                                                                                new BinaryOperator("ObjectList", CustomProcess.ObjectList.CashAdvance),
                                                                                new BinaryOperator("FunctionList", CustomProcess.FunctionList.Approval),
                                                                                new BinaryOperator("Active", true)));

                    if (_locAppSetupDetails != null && _locAppSetupDetails.Count > 0)
                    {
                        foreach (ApplicationSetupDetail _locAppSetupDetail in _locAppSetupDetails)
                        {
                            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.ApprovalLevel)))
                            {
                                if (_locAppSetupDetail.ApprovalLevel != CustomProcess.ApprovalLevel.None)
                                {
                                    EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalLevel));
                                    _setApprovalLevel = new ChoiceActionItem(_ed.GetCaption(_locAppSetupDetail.ApprovalLevel), _locAppSetupDetail.ApprovalLevel);
                                }
                            }

                            CashAdvanceApprovalDetailAction.Items.Add(_setApprovalLevel);
                        }
                    }
                }
            }
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        private void CashAdvanceApprovalDetailAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            GlobalFunction _globFUnc = new GlobalFunction();
            ApplicationSetupDetail _locAppSetDetail = null;
            IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
            ArrayList _objectToProcess = new ArrayList(e.SelectedObjects);
            DateTime now = DateTime.Now;
            var User = SecuritySystem.CurrentUserName;
            Session _currentSession = null;
            string _currObjectId = null;

            if (this.ObjectSpace != null)
            {
                _currentSession = ((XPObjectSpace)this.ObjectSpace).Session;
            }

            UserAccess _locUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));

            foreach (Object obj in _objectToProcess)
            {
                CashAdvance _objInNewObjectSpace = (CashAdvance)_objectSpace.GetObject(obj);

                if (_objInNewObjectSpace != null)
                {
                    if (_objInNewObjectSpace.Code != null)
                    {
                        _currObjectId = _objInNewObjectSpace.Code;
                    }
                }

                if (_currObjectId != null)
                {
                    CashAdvance _locCashAdvanceXPO = _currentSession.FindObject<CashAdvance>
                                                     (new GroupOperator(GroupOperatorType.And,
                                                      new BinaryOperator("Code", _currObjectId)));

                    if (_locCashAdvanceXPO != null)
                    {
                        if (_locCashAdvanceXPO.CashAdvanceTotalApprovedAmount > 0)
                        {
                            if (_locCashAdvanceXPO.CashAdvanceTotalAmount >= _locCashAdvanceXPO.CashAdvanceTotalApprovedAmount)
                            {
                                if (_locCashAdvanceXPO.Status == Status.Progress || _locCashAdvanceXPO.Status == Status.Approved && _locCashAdvanceXPO.CashAdvanceTotalApprovedAmount > 0)
                                {
                                    #region Approval Level 1
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level1)
                                    {
                                        _locAppSetDetail = _globFUnc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level1, ObjectList.CashAdvance);
                                        if (_locAppSetDetail != null)
                                        {
                                            //Approval Line direct input 
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("CashAdvance", _locCashAdvanceXPO),
                                                                                new BinaryOperator("ApprovalLevel", ApprovalLevel.Level1)));
                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        EndApproval = true,
                                                        CashAdvance = _locCashAdvanceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAl = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level1,
                                                        CashAdvance = _locCashAdvanceXPO,
                                                    };
                                                    _saveDataAl.Save();
                                                    _saveDataAl.Session.CommitTransaction();
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("CashAdvance", _locCashAdvanceXPO)));

                                                SetApprovalCashAdvance(_currentSession, _locCashAdvanceXPO, Status.Progress);
                                                SendEmail(_currentSession, _locCashAdvanceXPO, Status.Progress, _locApprovalLineXPO2);
                                                SuccessMessageShow("Cash Advance has successfully Approve - Level 1");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 1

                                    #region Approval Level 2
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level2) ;
                                    {
                                        _locAppSetDetail = _globFUnc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level2, ObjectList.CashAdvance);
                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("CashAdvance", _locCashAdvanceXPO),
                                                                                new BinaryOperator("ApprovalLevel", ApprovalLevel.Level2)));
                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDateAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        EndApproval = true,
                                                        CashAdvance = _locCashAdvanceXPO,

                                                    };
                                                    _saveDateAL.Save();
                                                    _saveDateAL.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDateAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level2,
                                                        CashAdvance = _locCashAdvanceXPO,
                                                    };
                                                    _saveDateAL.Save();
                                                    _saveDateAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locCashAdvanceXPO, ApprovalLevel.Level1);
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("CashAdvance", _locCashAdvanceXPO)));

                                                SetApprovalCashAdvance(_currentSession, _locCashAdvanceXPO, Status.Progress);
                                                SendEmail(_currentSession, _locCashAdvanceXPO, Status.Progress, _locApprovalLineXPO2);
                                                SuccessMessageShow("Cash Advance Has Successfully Approve - Level 2");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 2

                                    #region Approval Level 3
                                    if ((ApprovalLevel)e.SelectedChoiceActionItem.Data == ApprovalLevel.Level3)
                                    {
                                        _locAppSetDetail = _globFUnc.GetApplicationSetupDetailByApprovalLevel(_currentSession, _locUserAccess, ApprovalLevel.Level3, ObjectList.CashAdvance);

                                        if (_locAppSetDetail != null)
                                        {
                                            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("CashAdvance", _locCashAdvanceXPO),
                                                                                 new BinaryOperator("ApprovalLevel", ApprovalLevel.Level3)));

                                            if (_locApprovalLineXPO == null)
                                            {
                                                if (_locAppSetDetail.EndApproval == true)
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        EndApproval = true,
                                                        CashAdvance = _locCashAdvanceXPO,

                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();
                                                }
                                                else
                                                {
                                                    ApprovalLine _saveDataAL = new ApprovalLine(_currentSession)
                                                    {
                                                        ApprovalDate = now,
                                                        ApprovalStatus = Status.Approved,
                                                        ApprovalLevel = ApprovalLevel.Level3,
                                                        CashAdvance = _locCashAdvanceXPO,
                                                    };
                                                    _saveDataAL.Save();
                                                    _saveDataAL.Session.CommitTransaction();

                                                    SetApprovalLine(_currentSession, _locCashAdvanceXPO, ApprovalLevel.Level2);
                                                    SetApprovalLine(_currentSession, _locCashAdvanceXPO, ApprovalLevel.Level1);
                                                }

                                                //Send Email
                                                ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("CashAdvance", _locCashAdvanceXPO)));

                                                SetApprovalCashAdvance(_currentSession, _locCashAdvanceXPO, Status.Progress);
                                                SendEmail(_currentSession, _locCashAdvanceXPO, Status.Progress, _locApprovalLineXPO2);
                                                SuccessMessageShow("Cash Advance has successfully Approve - Level 3");
                                            }
                                        }
                                    }
                                    #endregion Approval Level 3 
                                }
                                else
                                {
                                    ErrorMessageShow("CashAdvance Status Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Total ApprovedAmount Cannot Exceed Total Cash Advance Amount");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Total ApprovedAmount Must Be Filled");
                        }
                    }
                    else
                    {
                        ErrorMessageShow("Data CashAdvance Not Available");
                    }
                }
            }

            if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
            {
                _objectSpace.CommitChanges();
            }
            if (View is ListView)
            {
                _objectSpace.CommitChanges();
                View.ObjectSpace.Refresh();
            }
        }

        //==== Code Only =====

        #region ApprovalProcess

        private void SetApprovalLine(Session _currentSession, CashAdvance _locCashAdvanceXPO, ApprovalLevel _locApprovalLevel)
        {
            try
            {
                DateTime now = DateTime.Now;

                ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                                   (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("CashAdvance", _locCashAdvanceXPO),
                                                    new BinaryOperator("ApprovalLevel", _locApprovalLevel)));

                if (_locApprovalLineXPO == null)
                {
                    ApprovalLine _saveDataAL2 = new ApprovalLine(_currentSession)
                    {
                        ApprovalDate = now,
                        ApprovalStatus = Status.Approved,
                        ApprovalLevel = _locApprovalLevel,
                        CashAdvance = _locCashAdvanceXPO,
                    };
                    _saveDataAL2.Save();
                    _saveDataAL2.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CashAdvance " + ex.ToString());
            }
        }

        private void SetApprovalCashAdvance(Session _currentSession, CashAdvance _locCashAdvanceXPO, Status _locStatus)
        {
            DateTime now = DateTime.Now;

            try
            {
                CashAdvance _locCashAdvances = _currentSession.FindObject<CashAdvance>
                                               (new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("Code", _locCashAdvanceXPO.Code),
                                                new BinaryOperator("Status", _locStatus)));

                if (_locCashAdvances != null)
                {
                    ApprovalLine _locApprovalLineXPO2 = _currentSession.FindObject<ApprovalLine>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("EndApproval", true),
                                                         new BinaryOperator("CashAdvance", _locCashAdvances)));

                    if (_locApprovalLineXPO2 != null)
                    {
                        #region True
                        if (_locCashAdvances != null)
                        {
                            _locCashAdvances.ActivationPosting = true;
                            _locCashAdvances.ActiveApproved1 = false;
                            _locCashAdvances.ActiveApproved2 = false;
                            _locCashAdvances.ActiveApproved3 = true;
                            _locCashAdvances.StatusDate = now;
                            _locCashAdvances.Save();
                            _locCashAdvances.Session.CommitTransaction();

                            XPCollection<CashAdvanceLine> _locCashAdvanceLines = new XPCollection<CashAdvanceLine>
                                                                                    (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("CashAdvance", _locCashAdvanceXPO),
                                                                                    new BinaryOperator("Status", Status.Progress)));
                            if (_locCashAdvanceLines != null)
                            {
                                foreach (CashAdvanceLine _locCashAdvanceLine in _locCashAdvanceLines)
                                {
                                    _locCashAdvanceLine.StatusDate = now;
                                    _locCashAdvanceLine.ActiveApprovedAmount = true;
                                    _locCashAdvanceLine.ActivationPosting = true;
                                    _locCashAdvanceLine.Save();
                                    _locCashAdvanceLine.Session.CommitTransaction();
                                }
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Data CashAdvance Not Available");
                        }
                        #endregion True
                    }
                    else if (_locApprovalLineXPO2 == null)
                    {
                        #region False
                        if (_locCashAdvances.ActiveApproved1 == false && _locCashAdvances.ActiveApproved2 == false && _locCashAdvances.ActiveApproved3 == false)
                        {
                            #region Lv1
                            if (_locCashAdvances != null)
                            {
                                _locCashAdvances.ActivationPosting = true;
                                _locCashAdvances.ActiveApproved1 = true;
                                _locCashAdvances.ActiveApproved2 = false;
                                _locCashAdvances.ActiveApproved3 = false;
                                _locCashAdvances.StatusDate = now;
                                _locCashAdvances.Save();
                                _locCashAdvances.Session.CommitTransaction();

                                XPCollection<CashAdvanceLine> _locCashAdvanceLines = new XPCollection<CashAdvanceLine>
                                                                                     (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                      new BinaryOperator("CashAdvance", _locCashAdvanceXPO),
                                                                                      new BinaryOperator("Status", Status.Progress)));
                                if (_locCashAdvanceLines != null)
                                {
                                    foreach (CashAdvanceLine _locCashAdvanceLine in _locCashAdvanceLines)
                                    {
                                        _locCashAdvanceLine.StatusDate = now;
                                        _locCashAdvanceLine.ActiveApprovedAmount = true;
                                        _locCashAdvanceLine.ActivationPosting = true;
                                        _locCashAdvanceLine.Save();
                                        _locCashAdvanceLine.Session.CommitTransaction();
                                    }
                                }
                            }
                            #endregion Lv1
                        }
                        else if (_locCashAdvances.ActiveApproved1 == true && _locCashAdvances.ActiveApproved2 == false && _locCashAdvances.ActiveApproved3 == false)
                        {
                            #region Lv2
                            if (_locCashAdvances != null)
                            {
                                _locCashAdvances.ActivationPosting = true;
                                _locCashAdvances.ActiveApproved2 = true;
                                _locCashAdvances.ActiveApproved1 = false;
                                _locCashAdvances.ActiveApproved3 = false;
                                _locCashAdvances.StatusDate = now;
                                _locCashAdvances.Save();
                                _locCashAdvances.Session.CommitTransaction();

                                XPCollection<CashAdvanceLine> _locCashAdvanceLines = new XPCollection<CashAdvanceLine>
                                                                                     (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                      new BinaryOperator("CashAdvance", _locCashAdvanceXPO),
                                                                                      new BinaryOperator("Status", Status.Progress)));
                                if (_locCashAdvanceLines != null)
                                {
                                    foreach (CashAdvanceLine _locCashAdvanceLine in _locCashAdvanceLines)
                                    {
                                        _locCashAdvanceLine.StatusDate = now;
                                        _locCashAdvanceLine.ActiveApprovedAmount = true;
                                        _locCashAdvanceLine.ActivationPosting = true;
                                        _locCashAdvanceLine.Save();
                                        _locCashAdvanceLine.Session.CommitTransaction();
                                    }
                                }
                            }
                            #endregion Lv2
                        }
                        else if (_locCashAdvances.ActiveApproved1 == false && _locCashAdvances.ActiveApproved2 == true && _locCashAdvances.ActiveApproved3 == false)
                        {
                            #region Lv3
                            if (_locCashAdvances != null)
                            {
                                _locCashAdvances.ActivationPosting = true;
                                _locCashAdvances.ActiveApproved3 = true;
                                _locCashAdvances.ActiveApproved2 = false;
                                _locCashAdvances.ActiveApproved1 = false;
                                _locCashAdvances.StatusDate = now;
                                _locCashAdvances.Save();
                                _locCashAdvances.Session.CommitTransaction();

                                XPCollection<CashAdvanceLine> _locCashAdvanceLines = new XPCollection<CashAdvanceLine>
                                                                                     (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                                      new BinaryOperator("CashAdvance", _locCashAdvanceXPO),
                                                                                      new BinaryOperator("Status", Status.Progress)));
                                if (_locCashAdvanceLines != null)
                                {
                                    foreach (CashAdvanceLine _locCashAdvanceLine in _locCashAdvanceLines)
                                    {
                                        _locCashAdvanceLine.StatusDate = now;
                                        _locCashAdvanceLine.ActiveApprovedAmount = true;
                                        _locCashAdvanceLine.ActivationPosting = true;
                                        _locCashAdvanceLine.Save();
                                        _locCashAdvanceLine.Session.CommitTransaction();
                                    }
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data CashAdvance Not Available");
                            }
                            #endregion Lv3
                        }
                        else
                        {
                            ErrorMessageShow("Data Approval For CashAdvance Not Available");
                        }
                        #endregion False
                    }
                    else
                    {
                        ErrorMessageShow("Data Approval For CashAdvance Not Available");
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CashAdvance " + ex.ToString());
            }
        }

        #endregion ApprovalProcess

        #region Email

        private string BackgroundBody(Session _currentSession, CashAdvance _locCashAdvanceXPO, MailSetupDetail _locMailSetDetail)
        {
            string body = string.Empty;

            CashAdvance _locCashAdvances = _currentSession.FindObject<CashAdvance>
                                           (new GroupOperator(GroupOperatorType.And,
                                            new BinaryOperator("Code", _locCashAdvanceXPO.Code)));

            ApprovalLine _locApprovalLineXPO = _currentSession.FindObject<ApprovalLine>
                                               (new GroupOperator(GroupOperatorType.And,
                                                new BinaryOperator("CashAdvance", _locCashAdvanceXPO)));

            string Status = _locApprovalLineXPO.ApprovalStatus.ToString();

            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Email/EmailTemplate.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{Code}", _locCashAdvanceXPO.Code);

            #region Level
            if (_locCashAdvances.ActiveApproved1 == true)
            {
                string Active = "Level 1";
                body = body.Replace("{Level}", Active);
            }
            else if (_locCashAdvances.ActiveApproved2 == true)
            {
                string Active = "Level 2";
                body = body.Replace("{Level}", Active);
            }
            else if (_locCashAdvances.ActiveApproved3 == true)
            {
                string Active = "Level 3";
                body = body.Replace("{Level}", Active);
            }
            else
            {
                string Active = "Not Available";
                body = body.Replace("{Level}", Active);
            }
            #endregion Level

            body = body.Replace("{Status}", Status);
            body = body.Replace("{Message}", _locMailSetDetail.MessageBody);
            return body;
        }

        protected void SendEmail(Session _currentSession, CashAdvance _locCashAdvanceXPO, Status _locStatus, ApprovalLine _locApprovalLineXPO2)
        {
            DateTime now = DateTime.Now;
            var User = SecuritySystem.CurrentUserName;
            GlobalFunction _globFunc = new GlobalFunction();
            UserAccess _numLineUserAccess = _currentSession.FindObject<UserAccess>(new BinaryOperator("UserName", User));
            string _locMessageBody = null;

            try
            {
                if (_locCashAdvanceXPO != null)
                {
                    XPCollection<MailSetupDetail> _locMailSetDetails = new XPCollection<MailSetupDetail>
                                                                       (_currentSession, new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("ObjectList", ObjectList.CashAdvance),
                                                                        new BinaryOperator("UserAccess", _numLineUserAccess),
                                                                        new BinaryOperator("Active", true)));

                    if (_locMailSetDetails != null && _locMailSetDetails.Count > 0)
                    {
                        foreach (MailSetupDetail _locMailSetDetail in _locMailSetDetails)
                        {
                            if (_locMailSetDetail.MessageBody != null)
                            {
                                if (_locApprovalLineXPO2.EndApproval == true)
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locCashAdvanceXPO, _locMailSetDetail);
                                }
                                else
                                {
                                    _locMessageBody = this.BackgroundBody(_currentSession, _locCashAdvanceXPO, _locMailSetDetail);
                                }
                            }

                            _globFunc.SetAndSendMail(_locMailSetDetail.SmtpHost, _locMailSetDetail.SmtpPort,
                            _locMailSetDetail.MailFrom, _locMailSetDetail.MailFromPassword, _locMailSetDetail.MailTo, _locMailSetDetail.MailSubject, _locMessageBody);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CashAdvance " + ex.ToString());
            }
        }

        #endregion Email

        #region GlobalMethod

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion GlobalMethod

    }
}
