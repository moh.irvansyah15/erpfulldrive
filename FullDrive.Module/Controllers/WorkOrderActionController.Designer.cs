﻿namespace FullDrive.Module.Controllers
{
    partial class WorkOrderActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.WorkOrderProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.WorkOrderListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.WorkOrderGetWRAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.WorkOrderPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.WorkOrderGetMaterialAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.WorkOrderPostingMaterialAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.WorkOrderListviewFilterApprovalSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // WorkOrderProgressAction
            // 
            this.WorkOrderProgressAction.Caption = "Progress";
            this.WorkOrderProgressAction.ConfirmationMessage = null;
            this.WorkOrderProgressAction.Id = "WorkOrderProgressActionId";
            this.WorkOrderProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.WorkOrder);
            this.WorkOrderProgressAction.ToolTip = null;
            this.WorkOrderProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.WorkOrderProgressAction_Execute);
            // 
            // WorkOrderListviewFilterSelectionAction
            // 
            this.WorkOrderListviewFilterSelectionAction.Caption = "Filter";
            this.WorkOrderListviewFilterSelectionAction.ConfirmationMessage = null;
            this.WorkOrderListviewFilterSelectionAction.Id = "WorkOrderListviewFilterSelectionActionId";
            this.WorkOrderListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.WorkOrderListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.WorkOrder);
            this.WorkOrderListviewFilterSelectionAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.WorkOrderListviewFilterSelectionAction.ToolTip = null;
            this.WorkOrderListviewFilterSelectionAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.WorkOrderListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.WorkOrderListviewFilterSelectionAction_Execute);
            // 
            // WorkOrderGetWRAction
            // 
            this.WorkOrderGetWRAction.Caption = "Get WR";
            this.WorkOrderGetWRAction.ConfirmationMessage = null;
            this.WorkOrderGetWRAction.Id = "WorkOrderGetWRActionId";
            this.WorkOrderGetWRAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.WorkOrder);
            this.WorkOrderGetWRAction.ToolTip = null;
            this.WorkOrderGetWRAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.WorkOrderGetWRAction_Execute);
            // 
            // WorkOrderPostingAction
            // 
            this.WorkOrderPostingAction.Caption = "Posting";
            this.WorkOrderPostingAction.ConfirmationMessage = null;
            this.WorkOrderPostingAction.Id = "WorkOrderPostingActionId";
            this.WorkOrderPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.WorkOrder);
            this.WorkOrderPostingAction.ToolTip = null;
            this.WorkOrderPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.WorkOrderPostingAction_Execute);
            // 
            // WorkOrderGetMaterialAction
            // 
            this.WorkOrderGetMaterialAction.Caption = "Get Material";
            this.WorkOrderGetMaterialAction.ConfirmationMessage = null;
            this.WorkOrderGetMaterialAction.Id = "WorkOrderGetMaterialActionId";
            this.WorkOrderGetMaterialAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.WorkOrder);
            this.WorkOrderGetMaterialAction.ToolTip = null;
            this.WorkOrderGetMaterialAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.WorkOrderGetMaterialAction_Execute);
            // 
            // WorkOrderPostingMaterialAction
            // 
            this.WorkOrderPostingMaterialAction.Caption = "Posting Material";
            this.WorkOrderPostingMaterialAction.ConfirmationMessage = null;
            this.WorkOrderPostingMaterialAction.Id = "WorkOrderPostingMaterialActionId";
            this.WorkOrderPostingMaterialAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.WorkOrder);
            this.WorkOrderPostingMaterialAction.ToolTip = null;
            this.WorkOrderPostingMaterialAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.WorkOrderPostingMaterialAction_Execute);
            // 
            // WorkOrderListviewFilterApprovalSelectionAction
            // 
            this.WorkOrderListviewFilterApprovalSelectionAction.Caption = "Filter Approval";
            this.WorkOrderListviewFilterApprovalSelectionAction.ConfirmationMessage = null;
            this.WorkOrderListviewFilterApprovalSelectionAction.Id = "WorkOrderListviewFilterApprovalSelectionActionId";
            this.WorkOrderListviewFilterApprovalSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.WorkOrderListviewFilterApprovalSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.WorkOrder);
            this.WorkOrderListviewFilterApprovalSelectionAction.ToolTip = null;
            this.WorkOrderListviewFilterApprovalSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.WorkOrderListviewFilterApprovalSelectionAction_Execute);
            // 
            // WorkOrderActionController
            // 
            this.Actions.Add(this.WorkOrderProgressAction);
            this.Actions.Add(this.WorkOrderListviewFilterSelectionAction);
            this.Actions.Add(this.WorkOrderGetWRAction);
            this.Actions.Add(this.WorkOrderPostingAction);
            this.Actions.Add(this.WorkOrderGetMaterialAction);
            this.Actions.Add(this.WorkOrderPostingMaterialAction);
            this.Actions.Add(this.WorkOrderListviewFilterApprovalSelectionAction);
            this.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.WorkOrder);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction WorkOrderProgressAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction WorkOrderListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SimpleAction WorkOrderGetWRAction;
        private DevExpress.ExpressApp.Actions.SimpleAction WorkOrderPostingAction;
        private DevExpress.ExpressApp.Actions.SimpleAction WorkOrderGetMaterialAction;
        private DevExpress.ExpressApp.Actions.SimpleAction WorkOrderPostingMaterialAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction WorkOrderListviewFilterApprovalSelectionAction;
    }
}
