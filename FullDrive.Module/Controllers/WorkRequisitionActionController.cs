﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class WorkRequisitionActionController : ViewController
    {
        #region Default

        private ChoiceActionItem _selectionListviewFilter;
        public WorkRequisitionActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.

            #region FilterStatus
            WorkRequisitionListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                WorkRequisitionListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterStatus

            #region FilterApproval
            WorkRequisitionListviewFilterApprovalSelectionAction.Items.Clear();
            foreach (object _currApprovalFilter in Enum.GetValues(typeof(CustomProcess.ApprovalFilter)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalFilter));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApprovalFilter), _currApprovalFilter);
                WorkRequisitionListviewFilterApprovalSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterApproval
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        private void WorkRequisitionProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        WorkRequisition _locWorkRequisitionOS = (WorkRequisition)_objectSpace.GetObject(obj);

                        if (_locWorkRequisitionOS != null)
                        {
                            if (_locWorkRequisitionOS.Code != null)
                            {
                                _currObjectId = _locWorkRequisitionOS.Code;

                                WorkRequisition _locWorkRequisitionXPO = _currSession.FindObject<WorkRequisition>
                                                                         (new GroupOperator(GroupOperatorType.And,
                                                                          new BinaryOperator("Code", _currObjectId)));

                                if (_locWorkRequisitionXPO != null)
                                {
                                    if (_locWorkRequisitionXPO.Status == Status.Open || _locWorkRequisitionXPO.Status == Status.Progress)
                                    {
                                        if (_locWorkRequisitionXPO.Status == Status.Open)
                                        {
                                            _locWorkRequisitionXPO.Status = Status.Progress;
                                            _locWorkRequisitionXPO.StatusDate = now;
                                            _locWorkRequisitionXPO.Save();
                                            _locWorkRequisitionXPO.Session.CommitTransaction();
                                        }

                                        XPCollection<WorkRequisitionLine> _locWorkRequisitionLines = new XPCollection<WorkRequisitionLine>
                                                                                                     (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("WorkRequisition", _locWorkRequisitionXPO),
                                                                                                      new BinaryOperator("Status", Status.Open)));

                                        if (_locWorkRequisitionLines != null && _locWorkRequisitionLines.Count > 0)
                                        {
                                            foreach (WorkRequisitionLine _locWorkRequisitionLine in _locWorkRequisitionLines)
                                            {
                                                _locWorkRequisitionLine.Status = Status.Progress;
                                                _locWorkRequisitionLine.StatusDate = now;
                                                _locWorkRequisitionLine.Save();
                                                _locWorkRequisitionLine.Session.CommitTransaction();
                                            }
                                        }
                                    }

                                    SuccessMessageShow("WorkRequisition has successfully updated to progress");
                                }
                                else
                                {
                                    ErrorMessageShow("Data WorkRequisition Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data WorkRequisition Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkRequisition " + ex.ToString());
            }
        }

        private void WorkRequisitionPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        WorkRequisition _locWorkRequisitionOS = (WorkRequisition)_objectSpace.GetObject(obj);

                        if (_locWorkRequisitionOS != null)
                        {
                            if (_locWorkRequisitionOS.Code != null)
                            {
                                _currObjectId = _locWorkRequisitionOS.Code;

                                WorkRequisition _locWorkRequisitionXPO = _currSession.FindObject<WorkRequisition>
                                                                         (new GroupOperator(GroupOperatorType.And,
                                                                          new BinaryOperator("Code", _currObjectId)));

                                if (_locWorkRequisitionXPO != null)
                                {
                                    if (_locWorkRequisitionXPO.Status == Status.Progress || _locWorkRequisitionXPO.Status == Status.Posted)
                                    {
                                        ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("EndApproval", true),
                                                                            new BinaryOperator("WorkRequisition", _locWorkRequisitionXPO)));

                                        if (_locApprovalLineXPO != null)
                                        {
                                            SetWorkOrder(_currSession, _locWorkRequisitionXPO);
                                            SetRemainQty(_currSession, _locWorkRequisitionXPO);
                                            SetPostingQty(_currSession, _locWorkRequisitionXPO);
                                            SetPostedCount(_currSession, _locWorkRequisitionXPO);
                                            SetStatusWorkRequisitionLine(_currSession, _locWorkRequisitionXPO);
                                            SetNormalQuantity(_currSession, _locWorkRequisitionXPO);
                                            SetFinalStatusWorkRequisition(_currSession, _locWorkRequisitionXPO);
                                            SuccessMessageShow("WR has successfully posted");
                                        }
                                        else
                                        {
                                            ErrorMessageShow("Data Approval Not Available");
                                        }
                                    }
                                    else
                                    {
                                        ErrorMessageShow("Data WorkRequisition Not Available");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data WorkRequisition Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data WorkRequisition Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Data WorkRequisition Not Available");
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkRequisition " + ex.ToString());
            }
        }

        private void WorkRequisitionListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(WorkRequisition)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkRequisition " + ex.ToString());
            }
        }

        private void WorkRequisitionListviewFilterApprovalSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(WorkRequisition)))
                {
                    if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.Approval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal)));
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.EndApproval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal);
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.AllData)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved1", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal));
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkRequisition " + ex.ToString());
            }
        }

        //==== Code Only ====

        #region PostingforWorkOrder

        //Membuat Work Order
        private void SetWorkOrder(Session _currSession, WorkRequisition _locWorkRequisitionXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;

                if (_locWorkRequisitionXPO != null)
                {
                    if (_locWorkRequisitionXPO.Status == Status.Progress)
                    {
                        _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.WorkOrder);

                        if (_currSignCode != null)
                        {
                            WorkOrder _saveDataWO = new WorkOrder(_currSession)
                            {
                                SignCode = _currSignCode,
                                Name = _locWorkRequisitionXPO.Name,
                                Description = _locWorkRequisitionXPO.Description,
                                StartDate = _locWorkRequisitionXPO.StartDate,
                                EndDate = _locWorkRequisitionXPO.EndDate,
                                Company = _locWorkRequisitionXPO.Company,
                                WorkRequisition = _locWorkRequisitionXPO,
                            };
                            _saveDataWO.Save();
                            _saveDataWO.Session.CommitTransaction();
                        }
                    }

                    XPCollection<WorkRequisitionLine> _numLineWorkRequisitionLines = new XPCollection<WorkRequisitionLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Select", true),
                                                                                     new BinaryOperator("WorkRequisition", _locWorkRequisitionXPO)));

                    if (_numLineWorkRequisitionLines != null && _numLineWorkRequisitionLines.Count > 0)
                    {
                        WorkOrder _locWorkOrder2 = _currSession.FindObject<WorkOrder>
                                                   (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("WorkRequisition", _locWorkRequisitionXPO)));

                        if (_locWorkOrder2 != null)
                        {
                            if (_numLineWorkRequisitionLines != null && _numLineWorkRequisitionLines.Count > 0)
                            {
                                foreach (WorkRequisitionLine _numLineWorkRequisitionLine in _numLineWorkRequisitionLines)
                                {
                                    if (_numLineWorkRequisitionLine.Status == Status.Progress || _numLineWorkRequisitionLine.Status == Status.Posted)
                                    {
                                        WorkOrderLine _saveDataWorkOrderLine = new WorkOrderLine(_currSession)
                                        {
                                            Name = _numLineWorkRequisitionLine.Name,
                                            Item = _numLineWorkRequisitionLine.Item,
                                            Description = _numLineWorkRequisitionLine.Description,
                                            MxDQty = _numLineWorkRequisitionLine.DQty,
                                            MxDUOM = _numLineWorkRequisitionLine.DUOM,
                                            MxQty = _numLineWorkRequisitionLine.Qty,
                                            MxUOM = _numLineWorkRequisitionLine.UOM,
                                            MxTQty = _numLineWorkRequisitionLine.DQty,
                                            DQty = _numLineWorkRequisitionLine.DQty,
                                            DUOM = _numLineWorkRequisitionLine.DUOM,
                                            Qty = _numLineWorkRequisitionLine.Qty,
                                            UOM = _numLineWorkRequisitionLine.UOM,
                                            TQty = _numLineWorkRequisitionLine.DQty,
                                            Company = _numLineWorkRequisitionLine.Company,
                                            WorkOrder = _locWorkOrder2,
                                        };
                                        _saveDataWorkOrderLine.Save();
                                        _saveDataWorkOrderLine.Session.CommitTransaction();
                                    }
                                    else
                                    {
                                        ErrorMessageShow("Data Status WorkRequisition Not Available");
                                    }
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data WorkRequisitionLine Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Data WorkOrder Not Available");
                        }
                    }
                    else
                    {
                        ErrorMessageShow("Data WorkRequisitionLine Not Available");
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = WorkRequisition ", ex.ToString());
            }
        }

        //Menentukan sisa dari transaksi
        private void SetRemainQty(Session _currSession, WorkRequisition _locWorkRequisitionXPO)
        {
            try
            {
                if (_locWorkRequisitionXPO != null)
                {
                    XPCollection<WorkRequisitionLine> _locWorkRequisitionLines = new XPCollection<WorkRequisitionLine>(_currSession,
                                                                                 new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("WorkRequisition", _locWorkRequisitionXPO),
                                                                                 new BinaryOperator("Select", true)));

                    if (_locWorkRequisitionLines != null && _locWorkRequisitionLines.Count > 0)
                    {
                        double _locRmDQty = 0;
                        double _locRmQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (WorkRequisitionLine _locWorkRequisitionLine in _locWorkRequisitionLines)
                        {
                            #region PostedCount=0
                            if (_locWorkRequisitionLine.PostedCount == 0)
                            {
                                #region MaxQuantity
                                if (_locWorkRequisitionLine.MxDQty > 0)
                                {
                                    if (_locWorkRequisitionLine.DQty > 0 && _locWorkRequisitionLine.DQty <= _locWorkRequisitionLine.MxDQty)
                                    {
                                        _locRmDQty = _locWorkRequisitionLine.MxDQty - _locWorkRequisitionLine.DQty;
                                    }

                                    if (_locWorkRequisitionLine.Qty > 0 && _locWorkRequisitionLine.Qty <= _locWorkRequisitionLine.MxQty)
                                    {
                                        _locRmQty = _locWorkRequisitionLine.MxQty - _locWorkRequisitionLine.Qty;
                                    }

                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locWorkRequisitionLine.Item),
                                                   new BinaryOperator("UOM", _locWorkRequisitionLine.MxUOM),
                                                   new BinaryOperator("DefaultUOM", _locWorkRequisitionLine.MxDUOM),
                                                   new BinaryOperator("Active", true)));

                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion MaxQuantity

                                #region NonMaxQuantity
                                else
                                {
                                    if (_locWorkRequisitionLine.DQty > 0)
                                    {
                                        _locRmDQty = 0;
                                    }

                                    if (_locWorkRequisitionLine.Qty > 0)
                                    {
                                        _locRmQty = 0;
                                    }

                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locWorkRequisitionLine.Item),
                                                   new BinaryOperator("UOM", _locWorkRequisitionLine.UOM),
                                                   new BinaryOperator("DefaultUOM", _locWorkRequisitionLine.DUOM),
                                                   new BinaryOperator("Active", true)));

                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion PostedCount=0

                            #region PostedCount>0
                            if (_locWorkRequisitionLine.PostedCount > 0)
                            {
                                if (_locWorkRequisitionLine.RmDQty > 0)
                                {
                                    _locRmDQty = _locWorkRequisitionLine.RmDQty - _locWorkRequisitionLine.DQty;
                                }

                                if (_locWorkRequisitionLine.RmQty > 0)
                                {
                                    _locRmQty = _locWorkRequisitionLine.RmQty - _locWorkRequisitionLine.Qty;
                                }

                                if (_locWorkRequisitionLine.MxDQty > 0 || _locWorkRequisitionLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locWorkRequisitionLine.Item),
                                                   new BinaryOperator("UOM", _locWorkRequisitionLine.MxUOM),
                                                   new BinaryOperator("DefaultUOM", _locWorkRequisitionLine.MxDUOM),
                                                   new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locWorkRequisitionLine.Item),
                                                   new BinaryOperator("UOM", _locWorkRequisitionLine.UOM),
                                                   new BinaryOperator("DefaultUOM", _locWorkRequisitionLine.DUOM),
                                                   new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }

                            }
                            #endregion PostedCount>0

                            _locWorkRequisitionLine.RmDQty = _locRmDQty;
                            _locWorkRequisitionLine.RmQty = _locRmQty;
                            _locWorkRequisitionLine.RmTQty = _locInvLineTotal;
                            _locWorkRequisitionLine.Save();
                            _locWorkRequisitionLine.Session.CommitTransaction();
                        }
                    }
                    else
                    {
                        ErrorMessageShow("Data WorkRequisitionLine Not Available");
                    }
                }
                else
                {
                    ErrorMessageShow("Data WorkRequisition Not Available");
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkRequisition " + ex.ToString());
            }
        }

        //Menentukan jumlah quantity yang di posting
        private void SetPostingQty(Session _currSession, WorkRequisition _locWorkRequisitionXPO)
        {
            try
            {
                if (_locWorkRequisitionXPO != null)
                {
                    XPCollection<WorkRequisitionLine> _locWorkRequisitionLines = new XPCollection<WorkRequisitionLine>(_currSession,
                                                                                 new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("WorkRequisition", _locWorkRequisitionXPO),
                                                                                 new BinaryOperator("Select", true)));

                    if (_locWorkRequisitionLines != null && _locWorkRequisitionLines.Count > 0)
                    {
                        double _locPDQty = 0;
                        double _locPQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (WorkRequisitionLine _locWorkRequisitionLine in _locWorkRequisitionLines)
                        {
                            #region PostedCount=0
                            if (_locWorkRequisitionLine.PostedCount == 0)
                            {
                                #region MaxQuantity
                                if (_locWorkRequisitionLine.MxDQty > 0)
                                {
                                    if (_locWorkRequisitionLine.DQty > 0 && _locWorkRequisitionLine.DQty <= _locWorkRequisitionLine.MxDQty)
                                    {
                                        _locPDQty = _locWorkRequisitionLine.DQty;
                                    }

                                    if (_locWorkRequisitionLine.Qty > 0 && _locWorkRequisitionLine.Qty <= _locWorkRequisitionLine.MxQty)
                                    {
                                        _locPQty = _locWorkRequisitionLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locWorkRequisitionLine.Item),
                                                   new BinaryOperator("UOM", _locWorkRequisitionLine.MxUOM),
                                                   new BinaryOperator("DefaultUOM", _locWorkRequisitionLine.MxDUOM),
                                                   new BinaryOperator("Active", true)));

                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }

                                }
                                #endregion MaxQuantity

                                #region NonMaxQuantity
                                else
                                {
                                    if (_locWorkRequisitionLine.DQty > 0)
                                    {
                                        _locPDQty = _locWorkRequisitionLine.DQty;
                                    }

                                    if (_locWorkRequisitionLine.Qty > 0)
                                    {
                                        _locPQty = _locWorkRequisitionLine.Qty;
                                    }

                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locWorkRequisitionLine.Item),
                                                   new BinaryOperator("UOM", _locWorkRequisitionLine.UOM),
                                                   new BinaryOperator("DefaultUOM", _locWorkRequisitionLine.DUOM),
                                                   new BinaryOperator("Active", true)));

                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion PostedCount=0

                            #region PostedCount>0
                            if (_locWorkRequisitionLine.PostedCount > 0)
                            {
                                if (_locWorkRequisitionLine.PDQty > 0)
                                {
                                    _locPDQty = _locWorkRequisitionLine.PDQty + _locWorkRequisitionLine.DQty;
                                }

                                if (_locWorkRequisitionLine.PQty > 0)
                                {
                                    _locPQty = _locWorkRequisitionLine.PQty + _locWorkRequisitionLine.Qty;
                                }

                                if (_locWorkRequisitionLine.MxDQty > 0 || _locWorkRequisitionLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locWorkRequisitionLine.Item),
                                                   new BinaryOperator("UOM", _locWorkRequisitionLine.MxUOM),
                                                   new BinaryOperator("DefaultUOM", _locWorkRequisitionLine.MxDUOM),
                                                   new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("Item", _locWorkRequisitionLine.Item),
                                                   new BinaryOperator("UOM", _locWorkRequisitionLine.UOM),
                                                   new BinaryOperator("DefaultUOM", _locWorkRequisitionLine.DUOM),
                                                   new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }

                            }
                            #endregion PostedCount>0

                            _locWorkRequisitionLine.PDQty = _locPDQty;
                            _locWorkRequisitionLine.PDUOM = _locWorkRequisitionLine.DUOM;
                            _locWorkRequisitionLine.PQty = _locPQty;
                            _locWorkRequisitionLine.PUOM = _locWorkRequisitionLine.UOM;
                            _locWorkRequisitionLine.PTQty = _locInvLineTotal;
                            _locWorkRequisitionLine.Save();
                            _locWorkRequisitionLine.Session.CommitTransaction();
                        }
                    }
                    else
                    {
                        ErrorMessageShow("Data WorkrequisitionLine Not Available");
                    }
                }
                else
                {
                    ErrorMessageShow("Data WorkRequisition Not Available");
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkRequisition " + ex.ToString());
            }
        }

        //Menentukan Banyak process issue
        private void SetPostedCount(Session _currSession, WorkRequisition _locWorkRequisitionXPO)
        {
            try
            {
                if (_locWorkRequisitionXPO != null)
                {
                    XPCollection<WorkRequisitionLine> _locWorkRequisitionLines = new XPCollection<WorkRequisitionLine>(_currSession,
                                                                                 new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("WorkRequisition", _locWorkRequisitionXPO),
                                                                                 new BinaryOperator("Select", true)));

                    if (_locWorkRequisitionLines != null && _locWorkRequisitionLines.Count > 0)
                    {
                        foreach (WorkRequisitionLine _locWorkRequisitionLine in _locWorkRequisitionLines)
                        {
                            if (_locWorkRequisitionLine.Status == Status.Progress || _locWorkRequisitionLine.Status == Status.Posted)
                            {
                                if (_locWorkRequisitionLine.DQty > 0 || _locWorkRequisitionLine.Qty > 0)
                                {
                                    _locWorkRequisitionLine.PostedCount = _locWorkRequisitionLine.PostedCount + 1;
                                    _locWorkRequisitionLine.Save();
                                    _locWorkRequisitionLine.Session.CommitTransaction();
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Status WorkRequisitionLine Not Available");
                            }
                        }
                    }
                    else
                    {
                        ErrorMessageShow("Data WorkRequisitionLine Not Available");
                    }
                }
                else
                {
                    ErrorMessageShow("Data WorkRequisition Not Available");
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkRequisition " + ex.ToString());
            }
        }

        //Menentukan Status Issue Pada Inventory Transfer Line
        private void SetStatusWorkRequisitionLine(Session _currSession, WorkRequisition _locWorkRequisitionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locWorkRequisitionXPO != null)
                {
                    XPCollection<WorkRequisitionLine> _locWorkRequisitionLines = new XPCollection<WorkRequisitionLine>(_currSession,
                                                                                 new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("WorkRequisition", _locWorkRequisitionXPO),
                                                                                 new BinaryOperator("Select", true)));

                    if (_locWorkRequisitionLines != null && _locWorkRequisitionLines.Count > 0)
                    {
                        foreach (WorkRequisitionLine _locWorkRequisitionLine in _locWorkRequisitionLines)
                        {
                            if (_locWorkRequisitionLine.Status == Status.Progress || _locWorkRequisitionLine.Status == Status.Posted)
                            {
                                if (_locWorkRequisitionLine.RmDQty == 0 && _locWorkRequisitionLine.RmQty == 0 && _locWorkRequisitionLine.RmTQty == 0)
                                {
                                    _locWorkRequisitionLine.Status = Status.Close;
                                    _locWorkRequisitionLine.ActivationPosting = true;
                                    _locWorkRequisitionLine.StatusDate = now;
                                }
                                else
                                {
                                    _locWorkRequisitionLine.Status = Status.Posted;
                                    _locWorkRequisitionLine.StatusDate = now;
                                }
                                _locWorkRequisitionLine.Save();
                                _locWorkRequisitionLine.Session.CommitTransaction();
                            }
                            else
                            {
                                ErrorMessageShow("Data Status WorkRequisitionLine Not Available");
                            }
                        }
                    }
                    else
                    {
                        ErrorMessageShow("Data WorkRequisitionLine Not Available");
                    }
                }
                else
                {
                    ErrorMessageShow("Data Status WorkRequisition Not Available");
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkRequisition " + ex.ToString());
            }
        }

        //Menormalkan Quantity
        private void SetNormalQuantity(Session _currSession, WorkRequisition _locWorkRequisitionXPO)
        {
            try
            {
                if (_locWorkRequisitionXPO != null)
                {
                    XPCollection<WorkRequisitionLine> _locWorkRequisitionLines = new XPCollection<WorkRequisitionLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Select", true),
                                                                                     new BinaryOperator("WorkRequisition", _locWorkRequisitionXPO)));

                    if (_locWorkRequisitionLines != null && _locWorkRequisitionLines.Count > 0)
                    {
                        foreach (WorkRequisitionLine _locWorkRequisitionLine in _locWorkRequisitionLines)
                        {
                            if (_locWorkRequisitionLine.Status == Status.Progress || _locWorkRequisitionLine.Status == Status.Posted || _locWorkRequisitionLine.Status == Status.Close)
                            {
                                if (_locWorkRequisitionLine.DQty > 0 || _locWorkRequisitionLine.Qty > 0)
                                {
                                    _locWorkRequisitionLine.Select = false;
                                    _locWorkRequisitionLine.DQty = 0;
                                    _locWorkRequisitionLine.Qty = 0;
                                    _locWorkRequisitionLine.Save();
                                    _locWorkRequisitionLine.Session.CommitTransaction();
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Status WorkRequisitionLine Not Available");
                            }
                        }
                    }
                    else
                    {
                        ErrorMessageShow("Data WorkRequisitionLine Not Available");
                    }
                }
                else
                {
                    ErrorMessageShow("Data WorkRequisition Not Available");
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkRequisition " + ex.ToString());
            }
        }

        //Menentukan Status Issue Pada Inventory Transfer
        private void SetFinalStatusWorkRequisition(Session _currSession, WorkRequisition _locWorkRequisitionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locWorkRequisitionLineCount = 0;

                if (_locWorkRequisitionXPO != null)
                {
                    XPCollection<WorkRequisitionLine> _locWorkRequisitionLines = new XPCollection<WorkRequisitionLine>(_currSession,
                                                                                 new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("WorkRequisition", _locWorkRequisitionXPO)));

                    if (_locWorkRequisitionLines != null && _locWorkRequisitionLines.Count() > 0)
                    {
                        _locWorkRequisitionLineCount = _locWorkRequisitionLines.Count();

                        foreach (WorkRequisitionLine _locWorkRequisitionLine in _locWorkRequisitionLines)
                        {
                            if (_locWorkRequisitionLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }
                    }

                    if (_locStatusCount == _locWorkRequisitionLineCount)
                    {
                        _locWorkRequisitionXPO.ActivationPosting = true;
                        _locWorkRequisitionXPO.Status = Status.Close;
                        _locWorkRequisitionXPO.StatusDate = now;
                        _locWorkRequisitionXPO.Save();
                        _locWorkRequisitionXPO.Session.CommitTransaction();
                    }
                    else
                    {
                        _locWorkRequisitionXPO.Status = Status.Posted;
                        _locWorkRequisitionXPO.StatusDate = now;
                        _locWorkRequisitionXPO.Save();
                        _locWorkRequisitionXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = WorkRequisition " + ex.ToString());
            }
        }

        #endregion PostingforWorkOrder

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method
        
    }
}
