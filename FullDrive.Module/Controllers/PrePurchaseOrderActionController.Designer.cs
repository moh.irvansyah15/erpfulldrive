﻿namespace FullDrive.Module.Controllers
{
    partial class PrePurchaseOrderActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PrePurchaseOrderProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PrePurchaseOrderPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PrePurchaseOrderListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // PrePurchaseOrderProgressAction
            // 
            this.PrePurchaseOrderProgressAction.Caption = "Progress";
            this.PrePurchaseOrderProgressAction.ConfirmationMessage = null;
            this.PrePurchaseOrderProgressAction.Id = "PrePurchaseOrderProgressActionId";
            this.PrePurchaseOrderProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PrePurchaseOrder);
            this.PrePurchaseOrderProgressAction.ToolTip = null;
            this.PrePurchaseOrderProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PrePurchaseOrderProgressAction_Execute);
            // 
            // PrePurchaseOrderPostingAction
            // 
            this.PrePurchaseOrderPostingAction.Caption = "Posting";
            this.PrePurchaseOrderPostingAction.ConfirmationMessage = null;
            this.PrePurchaseOrderPostingAction.Id = "PrePurchaseOrderPostingActionId";
            this.PrePurchaseOrderPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PrePurchaseOrder);
            this.PrePurchaseOrderPostingAction.ToolTip = null;
            this.PrePurchaseOrderPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PrePurchaseOrderPostingAction_Execute);
            // 
            // PrePurchaseOrderListviewFilterSelectionAction
            // 
            this.PrePurchaseOrderListviewFilterSelectionAction.Caption = "Filter";
            this.PrePurchaseOrderListviewFilterSelectionAction.ConfirmationMessage = null;
            this.PrePurchaseOrderListviewFilterSelectionAction.Id = "PrePurchaseOrderListviewFilterSelectionActionId";
            this.PrePurchaseOrderListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.PrePurchaseOrderListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PrePurchaseOrder);
            this.PrePurchaseOrderListviewFilterSelectionAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.PrePurchaseOrderListviewFilterSelectionAction.ToolTip = null;
            this.PrePurchaseOrderListviewFilterSelectionAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.PrePurchaseOrderListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.PrePurchaseOrderListviewFilterSelectionAction_Execute);
            // 
            // PrePurchaseOrderActionController
            // 
            this.Actions.Add(this.PrePurchaseOrderProgressAction);
            this.Actions.Add(this.PrePurchaseOrderPostingAction);
            this.Actions.Add(this.PrePurchaseOrderListviewFilterSelectionAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction PrePurchaseOrderProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PrePurchaseOrderPostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction PrePurchaseOrderListviewFilterSelectionAction;
    }
}
