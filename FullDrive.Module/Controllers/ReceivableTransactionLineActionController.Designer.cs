﻿namespace FullDrive.Module.Controllers
{
    partial class ReceivableTransactionLineActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ReceivableTransactionLineExchangeRateAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // ReceivableTransactionLineExchangeRateAction
            // 
            this.ReceivableTransactionLineExchangeRateAction.Caption = "Change Rates";
            this.ReceivableTransactionLineExchangeRateAction.ConfirmationMessage = null;
            this.ReceivableTransactionLineExchangeRateAction.Id = "ReceivableTransactionLineExchangeRateActionId";
            this.ReceivableTransactionLineExchangeRateAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ReceivableTransactionLine);
            this.ReceivableTransactionLineExchangeRateAction.ToolTip = null;
            this.ReceivableTransactionLineExchangeRateAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ReceivableTransactionLineExchangeRateAction_Execute);
            // 
            // ReceivableTransactionLineActionController
            // 
            this.Actions.Add(this.ReceivableTransactionLineExchangeRateAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction ReceivableTransactionLineExchangeRateAction;
    }
}
