﻿namespace FullDrive.Module.Controllers
{
    partial class MaterialRequisitionApprovalActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.MaterialRequisitionApprovalAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // MaterialRequisitionApprovalAction
            // 
            this.MaterialRequisitionApprovalAction.Caption = "Approval";
            this.MaterialRequisitionApprovalAction.ConfirmationMessage = null;
            this.MaterialRequisitionApprovalAction.Id = "MaterialRequisitionApprovalActionId";
            this.MaterialRequisitionApprovalAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.MaterialRequisitionApprovalAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.MaterialRequisition);
            this.MaterialRequisitionApprovalAction.ToolTip = null;
            this.MaterialRequisitionApprovalAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.MaterialRequisitionApprovalAction_Execute);
            // 
            // MaterialRequisitionApprovalActionController
            // 
            this.Actions.Add(this.MaterialRequisitionApprovalAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction MaterialRequisitionApprovalAction;
    }
}
