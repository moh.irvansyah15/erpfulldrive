﻿namespace FullDrive.Module.Controllers
{
    partial class PurchaseOrderApprovalDetailController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PurchaseOrderApprovalDetailAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // PurchaseOrderApprovalDetailAction
            // 
            this.PurchaseOrderApprovalDetailAction.Caption = "Approval";
            this.PurchaseOrderApprovalDetailAction.ConfirmationMessage = null;
            this.PurchaseOrderApprovalDetailAction.Id = "PurchaseOrderApprovalDetailActionId";
            this.PurchaseOrderApprovalDetailAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.PurchaseOrderApprovalDetailAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseOrder);
            this.PurchaseOrderApprovalDetailAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.PurchaseOrderApprovalDetailAction.ToolTip = null;
            this.PurchaseOrderApprovalDetailAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.PurchaseOrderApprovalDetailAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.PurchaseOrderApprovalDetailAction_Execute);
            // 
            // PurchaseOrderApprovalDetailController
            // 
            this.Actions.Add(this.PurchaseOrderApprovalDetailAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction PurchaseOrderApprovalDetailAction;
    }
}
