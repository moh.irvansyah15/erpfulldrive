﻿namespace FullDrive.Module.Controllers
{
    partial class SalesQuotationLineActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SalesQuotationLineTaxCalculationAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesQuotationLineDiscCalculationActionController = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesQuotationLineListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.SalesQuotationLineSelectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SalesQuotationLineUnselectAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // SalesQuotationLineTaxCalculationAction
            // 
            this.SalesQuotationLineTaxCalculationAction.Caption = "Tax Calculation";
            this.SalesQuotationLineTaxCalculationAction.ConfirmationMessage = null;
            this.SalesQuotationLineTaxCalculationAction.Id = "SalesQuotationLineTaxCalculationActionId";
            this.SalesQuotationLineTaxCalculationAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesQuotationLine);
            this.SalesQuotationLineTaxCalculationAction.ToolTip = null;
            this.SalesQuotationLineTaxCalculationAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesQuotationLineTaxCalculationAction_Execute);
            // 
            // SalesQuotationLineDiscCalculationActionController
            // 
            this.SalesQuotationLineDiscCalculationActionController.Caption = "Disc Calculation";
            this.SalesQuotationLineDiscCalculationActionController.ConfirmationMessage = null;
            this.SalesQuotationLineDiscCalculationActionController.Id = "SalesQuotationLineDiscCalculationActionControllerId";
            this.SalesQuotationLineDiscCalculationActionController.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesQuotationLine);
            this.SalesQuotationLineDiscCalculationActionController.ToolTip = null;
            this.SalesQuotationLineDiscCalculationActionController.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesQuotationLineDiscCalculationActionController_Execute);
            // 
            // SalesQuotationLineListviewFilterSelectionAction
            // 
            this.SalesQuotationLineListviewFilterSelectionAction.Caption = "Filter";
            this.SalesQuotationLineListviewFilterSelectionAction.ConfirmationMessage = null;
            this.SalesQuotationLineListviewFilterSelectionAction.Id = "SalesQuotationLineListviewFilterSelectionActionId";
            this.SalesQuotationLineListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.SalesQuotationLineListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesQuotationLine);
            this.SalesQuotationLineListviewFilterSelectionAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.SalesQuotationLineListviewFilterSelectionAction.ToolTip = null;
            this.SalesQuotationLineListviewFilterSelectionAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.SalesQuotationLineListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.SalesQuotationLineListviewFilterSelectionAction_Execute);
            // 
            // SalesQuotationLineSelectAction
            // 
            this.SalesQuotationLineSelectAction.Caption = "Select";
            this.SalesQuotationLineSelectAction.ConfirmationMessage = null;
            this.SalesQuotationLineSelectAction.Id = "SalesQuotationLineSelectActionId";
            this.SalesQuotationLineSelectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesQuotationLine);
            this.SalesQuotationLineSelectAction.ToolTip = null;
            this.SalesQuotationLineSelectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesQuotationLineSelectAction_Execute);
            // 
            // SalesQuotationLineUnselectAction
            // 
            this.SalesQuotationLineUnselectAction.Caption = "Unselect";
            this.SalesQuotationLineUnselectAction.ConfirmationMessage = null;
            this.SalesQuotationLineUnselectAction.Id = "SalesQuotationLineUnselectActionId";
            this.SalesQuotationLineUnselectAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.SalesQuotationLine);
            this.SalesQuotationLineUnselectAction.ToolTip = null;
            this.SalesQuotationLineUnselectAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SalesQuotationLineUnselectAction_Execute);
            // 
            // SalesQuotationLineActionController
            // 
            this.Actions.Add(this.SalesQuotationLineTaxCalculationAction);
            this.Actions.Add(this.SalesQuotationLineDiscCalculationActionController);
            this.Actions.Add(this.SalesQuotationLineListviewFilterSelectionAction);
            this.Actions.Add(this.SalesQuotationLineSelectAction);
            this.Actions.Add(this.SalesQuotationLineUnselectAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction SalesQuotationLineTaxCalculationAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesQuotationLineDiscCalculationActionController;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction SalesQuotationLineListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesQuotationLineSelectAction;
        private DevExpress.ExpressApp.Actions.SimpleAction SalesQuotationLineUnselectAction;
    }
}
