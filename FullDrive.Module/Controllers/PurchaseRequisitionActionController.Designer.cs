﻿namespace FullDrive.Module.Controllers
{
    partial class PurchaseRequisitionActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PurchaseRequisitionProgressAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchaseRequisitionPostingAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.PurchaseRequisitionListviewFilterSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.PurchaseRequisitionListviewFilterApprovalSelectionAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.PurchaseRequisitionApprovalAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // PurchaseRequisitionProgressAction
            // 
            this.PurchaseRequisitionProgressAction.Caption = "Progress";
            this.PurchaseRequisitionProgressAction.ConfirmationMessage = null;
            this.PurchaseRequisitionProgressAction.Id = "PurchaseRequisitionProgressActionId";
            this.PurchaseRequisitionProgressAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseRequisition);
            this.PurchaseRequisitionProgressAction.ToolTip = null;
            this.PurchaseRequisitionProgressAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseRequisitionProgressAction_Execute);
            // 
            // PurchaseRequisitionPostingAction
            // 
            this.PurchaseRequisitionPostingAction.Caption = "Posting";
            this.PurchaseRequisitionPostingAction.ConfirmationMessage = null;
            this.PurchaseRequisitionPostingAction.Id = "PurchaseRequisitionPostingActionId";
            this.PurchaseRequisitionPostingAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseRequisition);
            this.PurchaseRequisitionPostingAction.ToolTip = null;
            this.PurchaseRequisitionPostingAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.PurchaseRequisitionPostingAction_Execute);
            // 
            // PurchaseRequisitionListviewFilterSelectionAction
            // 
            this.PurchaseRequisitionListviewFilterSelectionAction.Caption = "Filter";
            this.PurchaseRequisitionListviewFilterSelectionAction.ConfirmationMessage = null;
            this.PurchaseRequisitionListviewFilterSelectionAction.Id = "PurchaseRequisitionListviewFilterSelectionActionId";
            this.PurchaseRequisitionListviewFilterSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.PurchaseRequisitionListviewFilterSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseRequisition);
            this.PurchaseRequisitionListviewFilterSelectionAction.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.PurchaseRequisitionListviewFilterSelectionAction.ToolTip = null;
            this.PurchaseRequisitionListviewFilterSelectionAction.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.PurchaseRequisitionListviewFilterSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.PurchaseRequisitionListviewFilterSelectionAction_Execute);
            // 
            // PurchaseRequisitionListviewFilterApprovalSelectionAction
            // 
            this.PurchaseRequisitionListviewFilterApprovalSelectionAction.Caption = "Filter Approval";
            this.PurchaseRequisitionListviewFilterApprovalSelectionAction.ConfirmationMessage = null;
            this.PurchaseRequisitionListviewFilterApprovalSelectionAction.Id = "PurchaseRequisitionListviewFilterApprovalSelectionActionId";
            this.PurchaseRequisitionListviewFilterApprovalSelectionAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.PurchaseRequisitionListviewFilterApprovalSelectionAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseRequisition);
            this.PurchaseRequisitionListviewFilterApprovalSelectionAction.ToolTip = null;
            this.PurchaseRequisitionListviewFilterApprovalSelectionAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.PurchaseRequisitionListviewFilterApprovalSelectionAction_Execute);
            // 
            // PurchaseRequisitionApprovalAction
            // 
            this.PurchaseRequisitionApprovalAction.Caption = "Approval";
            this.PurchaseRequisitionApprovalAction.ConfirmationMessage = null;
            this.PurchaseRequisitionApprovalAction.Id = "PurchaseRequisitionApprovalActionId";
            this.PurchaseRequisitionApprovalAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.PurchaseRequisitionApprovalAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseRequisition);
            this.PurchaseRequisitionApprovalAction.ToolTip = null;
            this.PurchaseRequisitionApprovalAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.PurchaseRequisitionApprovalAction_Execute);
            // 
            // PurchaseRequisitionActionController
            // 
            this.Actions.Add(this.PurchaseRequisitionProgressAction);
            this.Actions.Add(this.PurchaseRequisitionPostingAction);
            this.Actions.Add(this.PurchaseRequisitionListviewFilterSelectionAction);
            this.Actions.Add(this.PurchaseRequisitionListviewFilterApprovalSelectionAction);
            this.Actions.Add(this.PurchaseRequisitionApprovalAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseRequisitionProgressAction;
        private DevExpress.ExpressApp.Actions.SimpleAction PurchaseRequisitionPostingAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction PurchaseRequisitionListviewFilterSelectionAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction PurchaseRequisitionListviewFilterApprovalSelectionAction;
        private DevExpress.ExpressApp.Actions.SingleChoiceAction PurchaseRequisitionApprovalAction;
    }
}
