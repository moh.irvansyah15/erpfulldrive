﻿namespace FullDrive.Module.Controllers
{
    partial class PurchaseRequisitionApprovalDetailController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.PurchaseRequisitionApprovalDetailAction = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            // 
            // PurchaseRequisitionApprovalDetailAction
            // 
            this.PurchaseRequisitionApprovalDetailAction.Caption = "Approval";
            this.PurchaseRequisitionApprovalDetailAction.ConfirmationMessage = null;
            this.PurchaseRequisitionApprovalDetailAction.Id = "PurchaseRequisitionApprovalDetailActionId";
            this.PurchaseRequisitionApprovalDetailAction.ItemType = DevExpress.ExpressApp.Actions.SingleChoiceActionItemType.ItemIsOperation;
            this.PurchaseRequisitionApprovalDetailAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.PurchaseRequisition);
            this.PurchaseRequisitionApprovalDetailAction.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.PurchaseRequisitionApprovalDetailAction.ToolTip = null;
            this.PurchaseRequisitionApprovalDetailAction.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.PurchaseRequisitionApprovalDetailAction.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.PurchaseRequisitionApprovalDetailAction_Execute);
            // 
            // PurchaseRequisitionApprovalDetailController
            // 
            this.Actions.Add(this.PurchaseRequisitionApprovalDetailAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction PurchaseRequisitionApprovalDetailAction;
    }
}
