﻿    #region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class PurchaseOrderActionController : ViewController
    {
        #region Default

        private ChoiceActionItem _selectionListviewFilter;
        public PurchaseOrderActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            #region FilterStatus
            PurchaseOrderListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                PurchaseOrderListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterStatus

            #region FilterApproval
            PurchaseOrderListviewFilterApprovalSelectionAction.Items.Clear();
            foreach (object _currApprovalFilter in Enum.GetValues(typeof(CustomProcess.ApprovalFilter)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.ApprovalFilter));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApprovalFilter), _currApprovalFilter);
                PurchaseOrderListviewFilterApprovalSelectionAction.Items.Add(_selectionListviewFilter);
            }
            #endregion FilterApproval
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        private void PurchaseOrderProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseOrder _locPurchaseOrderOS = (PurchaseOrder)_objectSpace.GetObject(obj);

                        if (_locPurchaseOrderOS != null)
                        {
                            if (_locPurchaseOrderOS.Code != null)
                            {
                                _currObjectId = _locPurchaseOrderOS.Code;

                                PurchaseOrder _locPurchaseOrderXPO = _currSession.FindObject<PurchaseOrder>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locPurchaseOrderXPO != null)
                                {
                                    if (_locPurchaseOrderXPO.Status == Status.Open || _locPurchaseOrderXPO.Status == Status.Progress)
                                    {
                                        if (_locPurchaseOrderXPO.Status == Status.Open)
                                        {
                                            _locPurchaseOrderXPO.Status = Status.Progress;
                                            _locPurchaseOrderXPO.StatusDate = now;
                                            _locPurchaseOrderXPO.Save();
                                            _locPurchaseOrderXPO.Session.CommitTransaction();
                                        }

                                        XPCollection<PurchaseOrderLine> _locPurchaseOrderLines = new XPCollection<PurchaseOrderLine>
                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                                                new BinaryOperator("Status", Status.Open)));

                                        if (_locPurchaseOrderLines != null && _locPurchaseOrderLines.Count > 0)
                                        {
                                            foreach (PurchaseOrderLine _locPurchaseOrderLine in _locPurchaseOrderLines)
                                            {
                                                _locPurchaseOrderLine.Status = Status.Progress;
                                                _locPurchaseOrderLine.StatusDate = now;
                                                _locPurchaseOrderLine.Save();
                                                _locPurchaseOrderLine.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                    SuccessMessageShow("Purchase Order has been successfully updated to progress");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Order Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data Purchase Order Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void PurchaseOrderPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseOrder _locPurchaseOrderOS = (PurchaseOrder)_objectSpace.GetObject(obj);

                        if (_locPurchaseOrderOS != null)
                        {
                            if (_locPurchaseOrderOS.Code != null)
                            {
                                _currObjectId = _locPurchaseOrderOS.Code;

                                PurchaseOrder _locPurchaseOrderXPO = _currSession.FindObject<PurchaseOrder>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locPurchaseOrderXPO != null)
                                {
                                    //Get Approval dulu
                                    if ((_locPurchaseOrderXPO.Status == Status.Progress || _locPurchaseOrderXPO.Status == Status.Posted) 
                                        && GetStatusClosePurchaseOrderLine(_currSession, _locPurchaseOrderXPO) == false)
                                    {
                                        ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("EndApproval", true),
                                                                     new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO)));

                                        if (_locApprovalLineXPO != null)
                                        {
                                            //For WMS
                                            //SetInventoryTransferOrder(_currSession, _locPurchaseOrderXPO);
                                            if(_locPurchaseOrderXPO.DownPayment == true)
                                            {
                                                SetPurchasePrePaymentInvoice(_currSession, _locPurchaseOrderXPO);
                                                         
                                            }
                                            //Non Split Invoice
                                            if(_locPurchaseOrderXPO.CBD == true)
                                            {
                                                SetPurchaseInvoice(_currSession, _locPurchaseOrderXPO);
                                            }
                                            SetInventoryTransferIn(_currSession, _locPurchaseOrderXPO);
                                            SetFinalStatusPurchaseOrder(_currSession, _locPurchaseOrderXPO);

                                        }
                                    }

                                    SuccessMessageShow("Purchase Order has been successfully post");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Order Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Purchase Order Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void PurchaseOrderGetPRAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseOrder _locPurchaseOrderOS = (PurchaseOrder)_objectSpace.GetObject(obj);

                        if (_locPurchaseOrderOS != null)
                        {
                            if (_locPurchaseOrderOS.Code != null)
                            {
                                _currObjectId = _locPurchaseOrderOS.Code;

                                PurchaseOrder _locPurchaseOrderXPO = _currSession.FindObject<PurchaseOrder>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locPurchaseOrderXPO != null)
                                {
                                    if (_locPurchaseOrderXPO.Status == Status.Open || _locPurchaseOrderXPO.Status == Status.Progress)
                                    {
                                        if (_locPurchaseOrderXPO.Collection == true)
                                        {
                                            XPCollection<PurchaseRequisitionCollection> _locPurchaseRequisitionCollections = new XPCollection<PurchaseRequisitionCollection>
                                                              (_currSession, new GroupOperator(GroupOperatorType.And,
                                                               new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO)));

                                            if (_locPurchaseRequisitionCollections != null && _locPurchaseRequisitionCollections.Count() > 0)
                                            {
                                                foreach (PurchaseRequisitionCollection _locPurchaseRequisitionCollection in _locPurchaseRequisitionCollections)
                                                {
                                                    if (_locPurchaseRequisitionCollection.PurchaseRequisition != null &&
                                                        (_locPurchaseRequisitionCollection.Status == Status.Open || _locPurchaseRequisitionCollection.Status == Status.Progress))
                                                    {
                                                        ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                   (new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("EndApproval", true),
                                                                    new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionCollection.PurchaseRequisition)));

                                                        if (_locApprovalLineXPO != null)
                                                        {
                                                            SetPurchaseOrder(_currSession, _locPurchaseRequisitionCollection.PurchaseRequisition, _locPurchaseOrderXPO);
                                                            SetRemainQty(_currSession, _locPurchaseRequisitionCollection.PurchaseRequisition);
                                                            SetPostingQty(_currSession, _locPurchaseRequisitionCollection.PurchaseRequisition);
                                                            SetProcessCount(_currSession, _locPurchaseRequisitionCollection.PurchaseRequisition);
                                                            SetStatusPurchaseRequisitionLine(_currSession, _locPurchaseRequisitionCollection.PurchaseRequisition);
                                                            SetNormalQuantity(_currSession, _locPurchaseRequisitionCollection.PurchaseRequisition);
                                                            SetFinalStatusPurchaseRequisition(_currSession, _locPurchaseRequisitionCollection.PurchaseRequisition);
                                                        }          
                                                    }
                                                }
                                                SuccessMessageShow("Purchase Requisition Has Been Successfully Getting into Purchase Order");
                                            }
                                        }
                                    }    
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Order Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Purchase Order Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void PurchaseOrderPurchaseReturnAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                string _currSignCode = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseOrder _locPurchaseOrderOS = (PurchaseOrder)_objectSpace.GetObject(obj);

                        if (_locPurchaseOrderOS != null)
                        {
                            if (_locPurchaseOrderOS.Code != null)
                            {
                                _currObjectId = _locPurchaseOrderOS.Code;
                                PurchaseReturn _locPurchaseReturnByPO = null;

                                PurchaseOrder _locPurchaseOrderXPO = _currSession.FindObject<PurchaseOrder>
                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                      new BinaryOperator("Code", _currObjectId)));
                                if (_locPurchaseOrderXPO != null)
                                {
                                    if (_locPurchaseOrderXPO.Status == Status.Progress || _locPurchaseOrderXPO.Status == Status.Posted)
                                    {
                                        _locPurchaseReturnByPO = _currSession.FindObject<PurchaseReturn>(new GroupOperator(GroupOperatorType.And,
                                                                 new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO)));

                                        if (_locPurchaseReturnByPO == null)
                                        {
                                            _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.PurchaseReturn);

                                            if (_currSignCode != null)
                                            {
                                                PurchaseReturn _saveDataPReturn = new PurchaseReturn(_currSession)
                                                {
                                                    BuyFromVendor = _locPurchaseOrderXPO.BuyFromVendor,
                                                    BuyFromContact = _locPurchaseOrderXPO.BuyFromContact,
                                                    BuyFromCountry = _locPurchaseOrderXPO.BuyFromCountry,
                                                    BuyFromCity = _locPurchaseOrderXPO.BuyFromCity,
                                                    BuyFromAddress = _locPurchaseOrderXPO.BuyFromAddress,
                                                    ProjectHeader = _locPurchaseOrderXPO.ProjectHeader,
                                                    SignCode = _locPurchaseOrderXPO.Code,
                                                    PurchaseOrder = _locPurchaseOrderXPO,
                                                };
                                                _saveDataPReturn.Save();
                                                _saveDataPReturn.Session.CommitTransaction();

                                                XPCollection<PurchaseOrderLine> _numLinePurchaseOrderLines = new XPCollection<PurchaseOrderLine>(_currSession,
                                                                                                             new GroupOperator(GroupOperatorType.And,
                                                                                                             new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO)));

                                                PurchaseReturn _locPurchaseReturn2 = _currSession.FindObject<PurchaseReturn>
                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                      new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO)));
                                                if (_locPurchaseReturn2 != null)
                                                {
                                                    if (_numLinePurchaseOrderLines != null && _numLinePurchaseOrderLines.Count > 0)
                                                    {
                                                        foreach (PurchaseOrderLine _numLinePurchaseOrderLine in _numLinePurchaseOrderLines)
                                                        {
                                                            if (_numLinePurchaseOrderLine.Status == Status.Progress || _numLinePurchaseOrderLine.Status == Status.Posted)
                                                            {
                                                                PurchaseReturnLine _saveDataPurchaseReturnLine = new PurchaseReturnLine(_currSession)
                                                                {
                                                                    Item = _numLinePurchaseOrderLine.Item,
                                                                    MxDQty = _numLinePurchaseOrderLine.MxDQty,
                                                                    MxDUOM = _numLinePurchaseOrderLine.MxDUOM,
                                                                    MxTQty = _numLinePurchaseOrderLine.MxTQty,
                                                                    MxUOM = _numLinePurchaseOrderLine.MxUOM,
                                                                    DQty = _numLinePurchaseOrderLine.DQty,
                                                                    DUOM = _numLinePurchaseOrderLine.DUOM,
                                                                    Qty = _numLinePurchaseOrderLine.Qty,
                                                                    UOM = _numLinePurchaseOrderLine.UOM,
                                                                    TQty = _numLinePurchaseOrderLine.TQty,
                                                                    UAmount = _numLinePurchaseOrderLine.UAmount,
                                                                    TUAmount = _numLinePurchaseOrderLine.TUAmount,
                                                                    EstimatedDate = _numLinePurchaseOrderLine.EstimatedDate,
                                                                    StatusDate = _numLinePurchaseOrderLine.StatusDate,
                                                                    PurchaseReturn = _locPurchaseReturn2,
                                                                };
                                                                _saveDataPurchaseReturnLine.Save();
                                                                _saveDataPurchaseReturnLine.Session.CommitTransaction();
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ErrorMessageShow("Data Purchase Return Not Available");
                                                    }
                                                }
                                                else
                                                {
                                                    ErrorMessageShow("Data Purchase Return Not Available");
                                                }
                                            }
                                        }
                                        SuccessMessageShow("Purchase Return has been successfully post");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Return Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Purchase Return Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseReturn " + ex.ToString());
            }
        }

        private void PurchaseOrderListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(PurchaseOrder)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void PurchaseOrderListviewFilterApprovalSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(PurchaseOrder)))
                {
                    if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.Approval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal)));
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.EndApproval)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal);
                    }
                    else if ((ApprovalFilter)e.SelectedChoiceActionItem.Data == ApprovalFilter.AllData)
                    {
                        ((ListView)View).CollectionSource.Criteria["FilterApproval1"] = new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("ActiveApproved1", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", true, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved1", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved2", false, BinaryOperatorType.Equal),
                                                                                        new BinaryOperator("ActiveApproved3", false, BinaryOperatorType.Equal));
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        #region PostingMethod

        //Change SetInventoryTransferIn Into SetInventoryTransferOrder

        private void SetInventoryTransferOrder(Session _currSession, PurchaseOrder _locPurchaseOrderXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _locDocCode = null;
                ProjectHeader _locProjectHeader = null;

                if (_locPurchaseOrderXPO.ProjectHeader != null)
                {
                    _locProjectHeader = _locPurchaseOrderXPO.ProjectHeader;
                }

                DocumentType _locDocumentTypeXPO = _currSession.FindObject<DocumentType>
                                                (new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("TransferType", DirectionType.External),
                                                 new BinaryOperator("InventoryMovingType", InventoryMovingType.Receive),
                                                 new BinaryOperator("ObjectList", ObjectList.InventoryTransferOrder),
                                                 new BinaryOperator("DocumentRule", DocumentRule.Vendor),
                                                 new BinaryOperator("Active", true),
                                                 new BinaryOperator("Default", true)));

                if (_locDocumentTypeXPO != null)
                {
                    _locDocCode = _globFunc.GetDocumentNumberingUnlockOptimisticRecord(_currSession.DataLayer, _locDocumentTypeXPO);

                    if (_locDocCode != null)
                    {

                        InventoryTransferOrder _saveDataITOr = new InventoryTransferOrder(_currSession)
                        {
                            TransferType = _locDocumentTypeXPO.TransferType,
                            InventoryMovingType = _locDocumentTypeXPO.InventoryMovingType,
                            ObjectList = _locDocumentTypeXPO.ObjectList,
                            DocumentRule = _locDocumentTypeXPO.DocumentRule,
                            DocumentType = _locDocumentTypeXPO,
                            DocNo = _locDocCode,
                            BusinessPartner = _locPurchaseOrderXPO.BuyFromVendor,
                            BusinessPartnerCountry = _locPurchaseOrderXPO.BuyFromCountry,
                            BusinessPartnerCity = _locPurchaseOrderXPO.BuyFromCity,
                            BusinessPartnerAddress = _locPurchaseOrderXPO.BuyFromAddress,
                            EstimatedDate = _locPurchaseOrderXPO.EstimatedDate,
                            PurchaseOrder = _locPurchaseOrderXPO,
                            ProjectHeader = _locProjectHeader,
                            Company = _locPurchaseOrderXPO.Company,
                        };
                        _saveDataITOr.Save();
                        _saveDataITOr.Session.CommitTransaction();

                        InventoryTransferOrder _locInventoryTransferOrder = _currSession.FindObject<InventoryTransferOrder>
                                                (new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("DocNo", _locDocCode),
                                                 new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO)));

                        if (_locInventoryTransferOrder != null)
                        {

                            XPCollection<PurchaseOrderLine> _locPurchaseOrderLines = new XPCollection<PurchaseOrderLine>
                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                        new BinaryOperator("Status", Status.Progress)));

                            if (_locPurchaseOrderLines != null && _locPurchaseOrderLines.Count > 0)
                            {
                                foreach (PurchaseOrderLine _locPurchaseOrderLine in _locPurchaseOrderLines)
                                {
                                    #region SetMaxValue
                                    double _locMxDQty = _locPurchaseOrderLine.DQty;
                                    UnitOfMeasure _locMxDUOM = _locPurchaseOrderLine.DUOM;
                                    double _locMxQty = _locPurchaseOrderLine.MxQty;
                                    UnitOfMeasure _locMxUOM = _locPurchaseOrderLine.UOM;
                                    double _locMxTQty = _locPurchaseOrderLine.TQty;


                                    if (_locPurchaseOrderLine.MxDQty > 0)
                                    {
                                        _locMxDQty = _locPurchaseOrderLine.MxDQty;
                                    }

                                    if (_locPurchaseOrderLine.MxDUOM != null)
                                    {
                                        _locMxDUOM = _locPurchaseOrderLine.MxDUOM;
                                    }

                                    if (_locPurchaseOrderLine.MxQty > 0)
                                    {
                                        _locMxQty = _locPurchaseOrderLine.MxQty;
                                    }

                                    if (_locPurchaseOrderLine.MxUOM != null)
                                    {
                                        _locMxUOM = _locPurchaseOrderLine.MxUOM;
                                    }

                                    if (_locPurchaseOrderLine.MxTQty > 0)
                                    {
                                        _locMxTQty = _locPurchaseOrderLine.MxTQty;
                                    }
                                    #endregion SetMaxValue

                                    InventoryTransferOrderLine _saveDataInventoryTransferOrderLine = new InventoryTransferOrderLine(_currSession)
                                    {
                                        Select = true,
                                        Item = _locPurchaseOrderLine.Item,
                                        MxDQty = _locMxDQty,
                                        MxDUOM = _locMxDUOM,
                                        MxQty = _locMxQty,
                                        MxUOM = _locMxUOM,
                                        MxTQty = _locMxTQty,
                                        DQty = _locPurchaseOrderLine.DQty,
                                        DUOM = _locPurchaseOrderLine.DUOM,
                                        Qty = _locPurchaseOrderLine.Qty,
                                        UOM = _locPurchaseOrderLine.UOM,
                                        TQty = _locPurchaseOrderLine.TQty,
                                        EstimatedDate = _locPurchaseOrderLine.EstimatedDate,
                                        Company = _locInventoryTransferOrder.Company,
                                        InventoryTransferOrder = _locInventoryTransferOrder
                                    };
                                    _saveDataInventoryTransferOrderLine.Save();
                                    _saveDataInventoryTransferOrderLine.Session.CommitTransaction();

                                    //Change Status POL From Progress To Posted
                                    _locPurchaseOrderLine.Status = Status.Close;
                                    _locPurchaseOrderLine.StatusDate = now;
                                    _locPurchaseOrderLine.ActivationPosting = true;
                                    _locPurchaseOrderLine.Save();
                                    _locPurchaseOrderLine.Session.CommitTransaction();

                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void SetInventoryTransferIn(Session _currSession, PurchaseOrder _locPurchaseOrderXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _locDocCode = null;
                double _locMaxPay = 0;
                ProjectHeader _locProjectHeader = null;

                if (_locPurchaseOrderXPO.ProjectHeader != null)
                {
                    _locProjectHeader = _locPurchaseOrderXPO.ProjectHeader;
                }

                if(_locPurchaseOrderXPO.DownPayment == true)
                {
                    _locMaxPay = GetMaxPay(_currSession, _locPurchaseOrderXPO) - _locPurchaseOrderXPO.DP_Amount;
                }else
                {
                    if(_locPurchaseOrderXPO.CBD == true)
                    {
                        _locMaxPay = 0;
                    }
                    else
                    {
                        _locMaxPay = GetMaxPay(_currSession, _locPurchaseOrderXPO);
                    }
                    
                }
                
                DocumentType _locDocumentTypeXPO = _currSession.FindObject<DocumentType>
                                                (new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("TransferType", DirectionType.External),
                                                 new BinaryOperator("InventoryMovingType", InventoryMovingType.Receive),
                                                 new BinaryOperator("ObjectList", ObjectList.InventoryTransferIn),
                                                 new BinaryOperator("DocumentRule", DocumentRule.Vendor),
                                                 new BinaryOperator("Active", true),
                                                 new BinaryOperator("Default", true)));

                if (_locDocumentTypeXPO != null)
                {
                    _locDocCode = _globFunc.GetDocumentNumberingUnlockOptimisticRecord(_currSession.DataLayer, _locDocumentTypeXPO);

                    if (_locDocCode != null)
                    {

                        InventoryTransferIn _saveDataITIn = new InventoryTransferIn(_currSession)
                        {
                            TransferType = _locDocumentTypeXPO.TransferType,
                            InventoryMovingType = _locDocumentTypeXPO.InventoryMovingType,
                            ObjectList = _locDocumentTypeXPO.ObjectList,
                            DocumentRule = _locDocumentTypeXPO.DocumentRule,
                            DocumentType = _locDocumentTypeXPO,
                            DocNo = _locDocCode,
                            BusinessPartner = _locPurchaseOrderXPO.BuyFromVendor,
                            BusinessPartnerCountry = _locPurchaseOrderXPO.BuyFromCountry,
                            BusinessPartnerCity = _locPurchaseOrderXPO.BuyFromCity,
                            BusinessPartnerAddress = _locPurchaseOrderXPO.BuyFromAddress,
                            EstimatedDate = _locPurchaseOrderXPO.EstimatedDate,
                            PurchaseOrder = _locPurchaseOrderXPO,
                            ProjectHeader = _locProjectHeader,
                            MaxPay = _locMaxPay,
                            Company = _locPurchaseOrderXPO.Company,
                        };
                        _saveDataITIn.Save();
                        _saveDataITIn.Session.CommitTransaction();

                        InventoryTransferIn _locInventoryTransferIn = _currSession.FindObject<InventoryTransferIn>
                                                (new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("DocNo", _locDocCode),
                                                 new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO)));

                        if (_locInventoryTransferIn != null)
                        {

                            XPCollection<PurchaseOrderLine> _locPurchaseOrderLines = new XPCollection<PurchaseOrderLine>
                                       (_currSession, new GroupOperator(GroupOperatorType.And,
                                        new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                        new BinaryOperator("Status", Status.Progress)));

                            if (_locPurchaseOrderLines != null && _locPurchaseOrderLines.Count > 0)
                            {
                                foreach (PurchaseOrderLine _locPurchaseOrderLine in _locPurchaseOrderLines)
                                {
                                    #region SetMaxValueQuantity
                                    double _locMxDQty = _locPurchaseOrderLine.DQty;
                                    UnitOfMeasure _locMxDUOM = _locPurchaseOrderLine.DUOM;
                                    double _locMxQty = _locPurchaseOrderLine.MxQty;
                                    UnitOfMeasure _locMxUOM = _locPurchaseOrderLine.UOM;
                                    double _locMxTQty = _locPurchaseOrderLine.TQty;


                                    if (_locPurchaseOrderLine.MxDQty > 0)
                                    {
                                        _locMxDQty = _locPurchaseOrderLine.MxDQty;
                                    }

                                    if (_locPurchaseOrderLine.MxDUOM != null)
                                    {
                                        _locMxDUOM = _locPurchaseOrderLine.MxDUOM;
                                    }

                                    if (_locPurchaseOrderLine.MxQty > 0)
                                    {
                                        _locMxQty = _locPurchaseOrderLine.MxQty;
                                    }

                                    if (_locPurchaseOrderLine.MxUOM != null)
                                    {
                                        _locMxUOM = _locPurchaseOrderLine.MxUOM;
                                    }

                                    if (_locPurchaseOrderLine.MxTQty > 0)
                                    {
                                        _locMxTQty = _locPurchaseOrderLine.MxTQty;
                                    }
                                    #endregion SetMaxValueQuantity

                                    InventoryTransferInLine _saveDataInventoryTransferInLine = new InventoryTransferInLine(_currSession)
                                    {
                                        Select = true,
                                        Item = _locPurchaseOrderLine.Item,
                                        Description = _locPurchaseOrderLine.Description,
                                        MxDQty = _locMxDQty,
                                        MxDUOM = _locMxDUOM,
                                        MxQty = _locMxQty,
                                        MxUOM = _locMxUOM,
                                        MxTQty = _locMxTQty,
                                        DQty = _locPurchaseOrderLine.DQty,
                                        DUOM = _locPurchaseOrderLine.DUOM,
                                        Qty = _locPurchaseOrderLine.Qty,
                                        UOM = _locPurchaseOrderLine.UOM,
                                        TQty = _locPurchaseOrderLine.TQty,
                                        EstimatedDate = _locPurchaseOrderLine.EstimatedDate,
                                        Company = _locInventoryTransferIn.Company,
                                        InventoryTransferIn = _locInventoryTransferIn
                                    };
                                    _saveDataInventoryTransferInLine.Save();
                                    _saveDataInventoryTransferInLine.Session.CommitTransaction();

                                    //Change Status POL From Progress To Posted
                                    _locPurchaseOrderLine.Status = Status.Close;
                                    _locPurchaseOrderLine.StatusDate = now;
                                    _locPurchaseOrderLine.ActivationPosting = true;
                                    _locPurchaseOrderLine.Save();
                                    _locPurchaseOrderLine.Session.CommitTransaction();

                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void SetPurchaseInvoice(Session _currSession, PurchaseOrder _locPurchaseOrderXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;
                ProjectHeader _locProjectHeader = null;

                if (_locPurchaseOrderXPO != null)
                {
                    if (_locPurchaseOrderXPO.ProjectHeader != null)
                    {
                        _locProjectHeader = _locPurchaseOrderXPO.ProjectHeader;
                    }
                    _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.PurchaseInvoice);

                    if (_currSignCode != null)
                    {
                        PurchaseInvoice _saveDataPurchaseInvoice = new PurchaseInvoice(_currSession)
                        {
                            SignCode = _currSignCode,
                            BuyFromVendor = _locPurchaseOrderXPO.BuyFromVendor,
                            BuyFromContact = _locPurchaseOrderXPO.BuyFromContact,
                            BuyFromCountry = _locPurchaseOrderXPO.BuyFromCountry,
                            BuyFromCity = _locPurchaseOrderXPO.BuyFromCity,
                            BuyFromAddress = _locPurchaseOrderXPO.BuyFromAddress,
                            PayToVendor = _locPurchaseOrderXPO.BuyFromVendor,
                            PayToContact = _locPurchaseOrderXPO.BuyFromContact,
                            PayToCountry = _locPurchaseOrderXPO.BuyFromCountry,
                            PayToCity = _locPurchaseOrderXPO.BuyFromCity,
                            PayToAddress = _locPurchaseOrderXPO.BuyFromAddress,
                            TaxNo = _locPurchaseOrderXPO.TaxNo,
                            TOP = _locPurchaseOrderXPO.TOP,
                            MaxPay = GetMaxPay(_currSession, _locPurchaseOrderXPO),
                            Pay = GetMaxPay(_currSession, _locPurchaseOrderXPO),
                            PurchaseOrder = _locPurchaseOrderXPO,
                            ProjectHeader = _locProjectHeader,
                            PaymentMethod = _locPurchaseOrderXPO.PaymentMethod,
                            Company = _locPurchaseOrderXPO.Company,
                        };
                        _saveDataPurchaseInvoice.Save();
                        _saveDataPurchaseInvoice.Session.CommitTransaction();

                        PurchaseInvoice _locPurchaseInvoice2 = _currSession.FindObject<PurchaseInvoice>(new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("SignCode", _currSignCode)));

                        if (_locPurchaseInvoice2 != null)
                        {
                            SetPurchaseInvoiceCollection(_currSession, _locPurchaseOrderXPO, _locPurchaseInvoice2);
                        }

                    }
                }
                     

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void SetPurchasePrePaymentInvoice(Session _currSession, PurchaseOrder _locPurchaseOrderXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;
                ProjectHeader _locProjectHeader = null;

                if (_locPurchaseOrderXPO != null)
                {
                    if (_locPurchaseOrderXPO.ProjectHeader != null)
                    {
                        _locProjectHeader = _locPurchaseOrderXPO.ProjectHeader;
                    }
                    _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.PurchasePrePaymentInvoice);

                    if (_currSignCode != null)
                    {
                        PurchasePrePaymentInvoice _saveDataPurchasePrePaymentInvoice = new PurchasePrePaymentInvoice(_currSession)
                        {
                            SignCode = _currSignCode,
                            BuyFromVendor = _locPurchaseOrderXPO.BuyFromVendor,
                            BuyFromContact = _locPurchaseOrderXPO.BuyFromContact,
                            BuyFromCountry = _locPurchaseOrderXPO.BuyFromCountry,
                            BuyFromCity = _locPurchaseOrderXPO.BuyFromCity,
                            BuyFromAddress = _locPurchaseOrderXPO.BuyFromAddress,
                            PayToVendor = _locPurchaseOrderXPO.BuyFromVendor,
                            PayToContact = _locPurchaseOrderXPO.BuyFromContact,
                            PayToCountry = _locPurchaseOrderXPO.BuyFromCountry,
                            PayToCity = _locPurchaseOrderXPO.BuyFromCity,
                            PayToAddress = _locPurchaseOrderXPO.BuyFromAddress,
                            TaxNo = _locPurchaseOrderXPO.TaxNo,
                            TOP = _locPurchaseOrderXPO.TOP,
                            PaymentMethod = _locPurchaseOrderXPO.PaymentMethod,
                            MaxPay = _locPurchaseOrderXPO.DP_Amount,
                            Pay = _locPurchaseOrderXPO.DP_Amount,
                            PurchaseOrder = _locPurchaseOrderXPO,
                            ProjectHeader = _locProjectHeader,
                            Company = _locPurchaseOrderXPO.Company,
                        };
                        _saveDataPurchasePrePaymentInvoice.Save();
                        _saveDataPurchasePrePaymentInvoice.Session.CommitTransaction();



                        PurchasePrePaymentInvoice _locPurchasePrePaymentInvoice2 = _currSession.FindObject<PurchasePrePaymentInvoice>(new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("SignCode", _currSignCode)));

                        if (_locPurchasePrePaymentInvoice2 != null)
                        {
                            SetPurchaseInvoiceCollectionPrePayment(_currSession, _locPurchaseOrderXPO, _locPurchasePrePaymentInvoice2);
                        }

                    }
                }


            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void SetPurchaseInvoiceCollection(Session _currSession, PurchaseOrder _locPurchaseOrderXPO, PurchaseInvoice _locPurchaseInvoiceXPO)
        {
            try
            {
                //SalesInvoiceCollection based On Sales Order
                GlobalFunction _globFunc = new GlobalFunction();
                string _currSignCode = null;
                ProjectHeader _locProjectHeader = null;

                if (_locPurchaseOrderXPO != null)
                {
                    if (_locPurchaseOrderXPO.ProjectHeader != null)
                    {
                        _locProjectHeader = _locPurchaseOrderXPO.ProjectHeader;
                    }
                }

                PurchaseInvoiceCollection _locPurchaseInvoiceCollection = _currSession.FindObject<PurchaseInvoiceCollection>(new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO)));
                if (_locPurchaseInvoiceCollection != null)
                {
                    PurchaseInvoiceCollectionLine _saveDataPurchaseInvoiceCollectionLine = new PurchaseInvoiceCollectionLine(_currSession)
                    {
                        PurchaseInvoice = _locPurchaseInvoiceXPO,
                        PurchaseInvoiceCollection = _locPurchaseInvoiceCollection,
                    };
                    _saveDataPurchaseInvoiceCollectionLine.Save();
                    _saveDataPurchaseInvoiceCollectionLine.Session.CommitTransaction();
                }
                if (_locPurchaseInvoiceCollection == null)
                {
                    _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.PurchaseInvoiceCollection);

                    if (_currSignCode != null)
                    {
                        PurchaseInvoiceCollection _saveDataPurchaseInvoiceCollection = new PurchaseInvoiceCollection(_currSession)
                        {
                            SignCode = _currSignCode,
                            BuyFromVendor = _locPurchaseOrderXPO.BuyFromVendor,
                            BuyFromContact = _locPurchaseOrderXPO.BuyFromContact,
                            BuyFromCountry = _locPurchaseOrderXPO.BuyFromCountry,
                            BuyFromCity = _locPurchaseOrderXPO.BuyFromCity,
                            BuyFromAddress = _locPurchaseOrderXPO.BuyFromAddress,
                            TaxNo = _locPurchaseOrderXPO.TaxNo,
                            TOP = _locPurchaseOrderXPO.TOP,
                            MaxPay = GetMaxPay(_currSession, _locPurchaseOrderXPO),
                            DP_Amount = _locPurchaseOrderXPO.DP_Amount,
                            PurchaseOrder = _locPurchaseOrderXPO,
                            ProjectHeader = _locProjectHeader,
                            Company = _locPurchaseOrderXPO.Company,
                            
                        };
                        _saveDataPurchaseInvoiceCollection.Save();
                        _saveDataPurchaseInvoiceCollection.Session.CommitTransaction();

                        PurchaseInvoiceCollection _locPurchaseInvoiceCollection2 = _currSession.FindObject<PurchaseInvoiceCollection>(new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("SignCode", _currSignCode)));

                        if (_locPurchaseInvoiceCollection2 != null)
                        {
                            PurchaseInvoiceCollectionLine _saveDataPurchaseInvoiceCollectionLine = new PurchaseInvoiceCollectionLine(_currSession)
                            {
                                PurchaseInvoice = _locPurchaseInvoiceXPO,
                                PurchaseInvoiceCollection = _locPurchaseInvoiceCollection2,
                            };
                            _saveDataPurchaseInvoiceCollectionLine.Save();
                            _saveDataPurchaseInvoiceCollectionLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseOrder ", ex.ToString());
            }
        }

        private void SetPurchaseInvoiceCollectionPrePayment(Session _currSession, PurchaseOrder _locPurchaseOrderXPO, PurchasePrePaymentInvoice _locPurchasePrePaymentInvoiceXPO)
        {
            try
            {
                //SalesInvoiceCollection based On Sales Order
                GlobalFunction _globFunc = new GlobalFunction();
                string _currSignCode = null;
                ProjectHeader _locProjectHeader = null;

                if (_locPurchaseOrderXPO != null)
                {
                    if (_locPurchaseOrderXPO.ProjectHeader != null)
                    {
                        _locProjectHeader = _locPurchaseOrderXPO.ProjectHeader;
                    }
                }

                PurchaseInvoiceCollection _locPurchaseInvoiceCollection = _currSession.FindObject<PurchaseInvoiceCollection>(new GroupOperator(GroupOperatorType.And,
                                                                    new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO)));
                if (_locPurchaseInvoiceCollection != null)
                {
                    PurchaseInvoiceCollectionLine _saveDataPurchaseInvoiceCollectionLine = new PurchaseInvoiceCollectionLine(_currSession)
                    {
                        PurchasePrePaymentInvoice = _locPurchasePrePaymentInvoiceXPO,
                        PurchaseInvoiceCollection = _locPurchaseInvoiceCollection,
                    };
                    _saveDataPurchaseInvoiceCollectionLine.Save();
                    _saveDataPurchaseInvoiceCollectionLine.Session.CommitTransaction();
                }
                if (_locPurchaseInvoiceCollection == null)
                {
                    _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.PurchaseInvoiceCollection);

                    if (_currSignCode != null)
                    {
                        PurchaseInvoiceCollection _saveDataPurchaseInvoiceCollection = new PurchaseInvoiceCollection(_currSession)
                        {
                            SignCode = _currSignCode,
                            BuyFromVendor = _locPurchaseOrderXPO.BuyFromVendor,
                            BuyFromContact = _locPurchaseOrderXPO.BuyFromContact,
                            BuyFromCountry = _locPurchaseOrderXPO.BuyFromCountry,
                            BuyFromCity = _locPurchaseOrderXPO.BuyFromCity,
                            BuyFromAddress = _locPurchaseOrderXPO.BuyFromAddress,
                            TaxNo = _locPurchaseOrderXPO.TaxNo,
                            TOP = _locPurchaseOrderXPO.TOP,
                            MaxPay = GetMaxPay(_currSession, _locPurchaseOrderXPO),
                            DP_Amount = _locPurchaseOrderXPO.DP_Amount,
                            PurchaseOrder = _locPurchaseOrderXPO,
                            ProjectHeader = _locProjectHeader,
                            Company = _locPurchaseOrderXPO.Company,

                        };
                        _saveDataPurchaseInvoiceCollection.Save();
                        _saveDataPurchaseInvoiceCollection.Session.CommitTransaction();

                        PurchaseInvoiceCollection _locPurchaseInvoiceCollection2 = _currSession.FindObject<PurchaseInvoiceCollection>(new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("SignCode", _currSignCode)));

                        if (_locPurchaseInvoiceCollection2 != null)
                        {
                            PurchaseInvoiceCollectionLine _saveDataPurchaseInvoiceCollectionLine = new PurchaseInvoiceCollectionLine(_currSession)
                            {
                                PurchasePrePaymentInvoice = _locPurchasePrePaymentInvoiceXPO,
                                PurchaseInvoiceCollection = _locPurchaseInvoiceCollection2,
                            };
                            _saveDataPurchaseInvoiceCollectionLine.Save();
                            _saveDataPurchaseInvoiceCollectionLine.Session.CommitTransaction();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseOrder ", ex.ToString());
            }
        }

        private void SetFinalStatusPurchaseOrder(Session _currSession, PurchaseOrder _locPurchaseOrderXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locPurchaseOrderXPO != null)
                {
                    XPCollection<PurchaseOrderLine> _locPurchaseOrderLines = new XPCollection<PurchaseOrderLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO)));

                    if (_locPurchaseOrderLines != null && _locPurchaseOrderLines.Count() > 0)
                    {
                        foreach (PurchaseOrderLine _locPurchaseOrderLine in _locPurchaseOrderLines)
                        {
                            if(_locPurchaseOrderLine.Status == Status.Progress || _locPurchaseOrderLine.Status == Status.Posted)
                            {
                                _locPurchaseOrderLine.ActivationPosting = true;
                                _locPurchaseOrderLine.Status = Status.Close;
                                _locPurchaseOrderLine.StatusDate = now;
                                _locPurchaseOrderLine.Save();
                                _locPurchaseOrderLine.Session.CommitTransaction();
                            }
                        }
                    }

                    _locPurchaseOrderXPO.Status = Status.Posted;
                    _locPurchaseOrderXPO.StatusDate = now;
                    _locPurchaseOrderXPO.ActivationPosting = true;
                    _locPurchaseOrderXPO.Save();
                    _locPurchaseOrderXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private double GetMaxPay(Session _currSession, PurchaseOrder _locPurchaseOrderXPO)
        {
            double _result = 0;
            try
            {
                double _locTotAmount = 0;

                if (_locPurchaseOrderXPO != null)
                {
                    XPCollection<PurchaseOrderLine> _locPurchaseOrderLines = new XPCollection<PurchaseOrderLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO)));

                    if (_locPurchaseOrderLines != null && _locPurchaseOrderLines.Count() > 0)
                    {
                        foreach (PurchaseOrderLine _locPurchaseOrderLine in _locPurchaseOrderLines)
                        {
                            _locTotAmount = _locTotAmount + _locPurchaseOrderLine.TAmount;
                        }

                        _result = _locTotAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
            return _result;
        }

        private bool GetStatusClosePurchaseOrderLine(Session _currSession, PurchaseOrder _locPurchaseOrderXPO)
        {
            bool _result = false;
            try
            {
                DateTime now = DateTime.Now;
                int _locCountClose = 0;

                if (_locPurchaseOrderXPO != null)
                {
                    XPCollection<PurchaseOrderLine> _locPurchaseOrderLines = new XPCollection<PurchaseOrderLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO)));

                    if (_locPurchaseOrderLines != null && _locPurchaseOrderLines.Count() > 0)
                    {
                        foreach (PurchaseOrderLine _locPurchaseOrderLine in _locPurchaseOrderLines)
                        {
                            if (_locPurchaseOrderLine.Status == Status.Close)
                            {
                                _locCountClose = _locCountClose + 1;
                            }
                        }

                        if (_locCountClose == _locPurchaseOrderLines.Count())
                        {
                            _result = true;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
            return _result;
        }

        #endregion PostingMethod

        #region GetPR

        private void SetPurchaseOrder(Session _currSession, PurchaseRequisition _locPurchaseRequisitionXPO, PurchaseOrder _locPurchaseOrderXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locPurchaseRequisitionXPO != null && _locPurchaseOrderXPO != null)
                {
                    if (_locPurchaseRequisitionXPO.Code != null)
                    {
                        PurchaseRequisition _locPurchaseRequisition = _currSession.FindObject<PurchaseRequisition>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Code", _locPurchaseRequisitionXPO.Code)));

                        if (_locPurchaseRequisition != null)
                        {
                            if(_locPurchaseRequisition.Status == Status.Progress || _locPurchaseRequisition.Status == Status.Posted)
                            {
                                XPCollection<PurchaseRequisitionLine> _locPurchaseRequisitionLines = new XPCollection<PurchaseRequisitionLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseRequisition", _locPurchaseRequisition),
                                                            new BinaryOperator("Select", true)));

                                if (_locPurchaseRequisitionLines != null && _locPurchaseRequisitionLines.Count() > 0)
                                {
                                    foreach (PurchaseRequisitionLine _locPurchaseRequisitionLine in _locPurchaseRequisitionLines)
                                    {
                                        if (_locPurchaseRequisitionLine.PurchaseType == OrderType.Item
                                            && (_locPurchaseRequisitionLine.Status == Status.Progress || _locPurchaseRequisitionLine.Status == Status.Posted))
                                        {
                                            PurchaseOrderLine _locPurchaseOrderLine = _currSession.FindObject<PurchaseOrderLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                                                             new BinaryOperator("Item", _locPurchaseRequisitionLine.Item)));

                                            if (_locPurchaseOrderLine != null)
                                            {
                                                _locPurchaseOrderLine.MxDQty = _locPurchaseOrderLine.MxDQty + _locPurchaseRequisitionLine.DQty;
                                                _locPurchaseOrderLine.MxQty = _locPurchaseOrderLine.MxQty + _locPurchaseRequisitionLine.Qty;
                                                _locPurchaseOrderLine.MxTQty = _locPurchaseOrderLine.MxTQty + _locPurchaseRequisitionLine.TQty;
                                                _locPurchaseOrderLine.MxTUAmount = _locPurchaseOrderLine.MxTUAmount + (_locPurchaseOrderLine.MxUAmount * _locPurchaseRequisitionLine.TQty);
                                                _locPurchaseOrderLine.DQty = _locPurchaseOrderLine.DQty + _locPurchaseRequisitionLine.DQty;
                                                _locPurchaseOrderLine.Qty = _locPurchaseOrderLine.Qty + _locPurchaseRequisitionLine.Qty;
                                                _locPurchaseOrderLine.TQty = _locPurchaseOrderLine.TQty + _locPurchaseRequisitionLine.TQty;
                                                _locPurchaseOrderLine.TUAmount = _locPurchaseOrderLine.TUAmount + (_locPurchaseOrderLine.UAmount * _locPurchaseRequisitionLine.TQty);
                                                _locPurchaseOrderLine.Save();
                                                _locPurchaseOrderLine.Session.CommitTransaction();
                                            }
                                            else
                                            {
                                                PurchaseOrderLine _saveDataPurchaseOrderLine = new PurchaseOrderLine(_currSession)
                                                {
                                                    PurchaseType = _locPurchaseRequisitionLine.PurchaseType,
                                                    Item = _locPurchaseRequisitionLine.Item,
                                                    Description = _locPurchaseRequisitionLine.Description,
                                                    MxDQty = _locPurchaseRequisitionLine.DQty,
                                                    MxDUOM = _locPurchaseRequisitionLine.DUOM,
                                                    MxQty = _locPurchaseRequisitionLine.Qty,
                                                    MxUOM = _locPurchaseRequisitionLine.UOM,
                                                    MxTQty = _locPurchaseRequisitionLine.TQty,
                                                    MxUAmount = _locPurchaseRequisitionLine.MxUAmount,
                                                    MxTUAmount = _locPurchaseRequisitionLine.TQty * _locPurchaseRequisitionLine.MxUAmount,
                                                    DQty = _locPurchaseRequisitionLine.DQty,
                                                    DUOM = _locPurchaseRequisitionLine.DUOM,
                                                    Qty = _locPurchaseRequisitionLine.Qty,
                                                    UOM = _locPurchaseRequisitionLine.UOM,
                                                    TQty = _locPurchaseRequisitionLine.TQty,
                                                    UAmount = _locPurchaseRequisitionLine.MxUAmount,
                                                    TUAmount = _locPurchaseRequisitionLine.TQty * _locPurchaseRequisitionLine.MxUAmount,
                                                    PurchaseOrder = _locPurchaseOrderXPO,
                                                };
                                                _saveDataPurchaseOrderLine.Save();
                                                _saveDataPurchaseOrderLine.Session.CommitTransaction();


                                            }
                                        }
                                    }
                                } 
                            }                       
                        }
                    }    
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = PurchaseOrder ", ex.ToString());
            }
        }

        private void SetRemainQty(Session _currSession, PurchaseRequisition _locPurchaseRequisitionXPO)
        {
            try
            {
                if (_locPurchaseRequisitionXPO != null)
                {
                    XPCollection<PurchaseRequisitionLine> _locPurchaseRequisitionLines = new XPCollection<PurchaseRequisitionLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locPurchaseRequisitionLines != null && _locPurchaseRequisitionLines.Count > 0)
                    {
                        double _locRmDQty = 0;
                        double _locRmQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (PurchaseRequisitionLine _locPurchaseRequisitionLine in _locPurchaseRequisitionLines)
                        {
                            #region ProcessCount=0
                            if (_locPurchaseRequisitionLine.PostedCount == 0)
                            {
                                #region MaxQuantity
                                if (_locPurchaseRequisitionLine.MxDQty > 0)
                                {
                                    if (_locPurchaseRequisitionLine.DQty > 0 && _locPurchaseRequisitionLine.DQty <= _locPurchaseRequisitionLine.MxDQty)
                                    {
                                        _locRmDQty = _locPurchaseRequisitionLine.MxDQty - _locPurchaseRequisitionLine.DQty;
                                    }

                                    if (_locPurchaseRequisitionLine.Qty > 0 && _locPurchaseRequisitionLine.Qty <= _locPurchaseRequisitionLine.MxQty)
                                    {
                                        _locRmQty = _locPurchaseRequisitionLine.MxQty - _locPurchaseRequisitionLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locPurchaseRequisitionLine.Item),
                                                                new BinaryOperator("UOM", _locPurchaseRequisitionLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locPurchaseRequisitionLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locPurchaseRequisitionLine.DQty > 0)
                                    {
                                        _locRmDQty = 0;
                                    }

                                    if (_locPurchaseRequisitionLine.Qty > 0)
                                    {
                                        _locRmQty = 0;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locPurchaseRequisitionLine.Item),
                                                                new BinaryOperator("UOM", _locPurchaseRequisitionLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locPurchaseRequisitionLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locPurchaseRequisitionLine.PostedCount > 0)
                            {
                                if (_locPurchaseRequisitionLine.RmDQty > 0)
                                {
                                    _locRmDQty = _locPurchaseRequisitionLine.RmDQty - _locPurchaseRequisitionLine.DQty;
                                }

                                if (_locPurchaseRequisitionLine.RmQty > 0)
                                {
                                    _locRmQty = _locPurchaseRequisitionLine.RmQty - _locPurchaseRequisitionLine.Qty;
                                }

                                if (_locPurchaseRequisitionLine.MxDQty > 0 || _locPurchaseRequisitionLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locPurchaseRequisitionLine.Item),
                                                                new BinaryOperator("UOM", _locPurchaseRequisitionLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locPurchaseRequisitionLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locPurchaseRequisitionLine.Item),
                                                                new BinaryOperator("UOM", _locPurchaseRequisitionLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locPurchaseRequisitionLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                }


                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locPurchaseRequisitionLine.RmDQty = _locRmDQty;
                            _locPurchaseRequisitionLine.RmQty = _locRmQty;
                            _locPurchaseRequisitionLine.RmTQty = _locInvLineTotal;
                            _locPurchaseRequisitionLine.Save();
                            _locPurchaseRequisitionLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void SetPostingQty(Session _currSession, PurchaseRequisition _locPurchaseRequisitionXPO)
        {
            try
            {
                if (_locPurchaseRequisitionXPO != null)
                {
                    XPCollection<PurchaseRequisitionLine> _locPurchaseRequisitionLines = new XPCollection<PurchaseRequisitionLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locPurchaseRequisitionLines != null && _locPurchaseRequisitionLines.Count > 0)
                    {
                        double _locPDQty = 0;
                        double _locPQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (PurchaseRequisitionLine _locPurchaseRequisitionLine in _locPurchaseRequisitionLines)
                        {
                            #region ProcessCount=0
                            if (_locPurchaseRequisitionLine.PostedCount == 0)
                            {
                                #region MaxQuantity
                                if (_locPurchaseRequisitionLine.MxDQty > 0)
                                {
                                    if (_locPurchaseRequisitionLine.DQty > 0 && _locPurchaseRequisitionLine.DQty <= _locPurchaseRequisitionLine.MxDQty)
                                    {
                                        _locPDQty = _locPurchaseRequisitionLine.DQty;
                                    }

                                    if (_locPurchaseRequisitionLine.Qty > 0 && _locPurchaseRequisitionLine.Qty <= _locPurchaseRequisitionLine.MxQty)
                                    {
                                        _locPQty = _locPurchaseRequisitionLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locPurchaseRequisitionLine.Item),
                                                                new BinaryOperator("UOM", _locPurchaseRequisitionLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locPurchaseRequisitionLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locPurchaseRequisitionLine.DQty > 0)
                                    {
                                        _locPDQty = _locPurchaseRequisitionLine.DQty;
                                    }

                                    if (_locPurchaseRequisitionLine.Qty > 0)
                                    {
                                        _locPQty = _locPurchaseRequisitionLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locPurchaseRequisitionLine.Item),
                                                                new BinaryOperator("UOM", _locPurchaseRequisitionLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locPurchaseRequisitionLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locPurchaseRequisitionLine.PostedCount > 0)
                            {
                                if (_locPurchaseRequisitionLine.PDQty > 0)
                                {
                                    _locPDQty = _locPurchaseRequisitionLine.PDQty + _locPurchaseRequisitionLine.DQty;
                                }

                                if (_locPurchaseRequisitionLine.PQty > 0)
                                {
                                    _locPQty = _locPurchaseRequisitionLine.PQty + _locPurchaseRequisitionLine.Qty;
                                }

                                if (_locPurchaseRequisitionLine.MxDQty > 0 || _locPurchaseRequisitionLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locPurchaseRequisitionLine.Item),
                                                            new BinaryOperator("UOM", _locPurchaseRequisitionLine.MxUOM),
                                                            new BinaryOperator("DefaultUOM", _locPurchaseRequisitionLine.MxDUOM),
                                                            new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locPurchaseRequisitionLine.Item),
                                                            new BinaryOperator("UOM", _locPurchaseRequisitionLine.UOM),
                                                            new BinaryOperator("DefaultUOM", _locPurchaseRequisitionLine.DUOM),
                                                            new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locPurchaseRequisitionLine.PDQty = _locPDQty;
                            _locPurchaseRequisitionLine.PDUOM = _locPurchaseRequisitionLine.DUOM;
                            _locPurchaseRequisitionLine.PQty = _locPQty;
                            _locPurchaseRequisitionLine.PUOM = _locPurchaseRequisitionLine.UOM;
                            _locPurchaseRequisitionLine.PTQty = _locInvLineTotal;
                            _locPurchaseRequisitionLine.Save();
                            _locPurchaseRequisitionLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }

        }

        private void SetProcessCount(Session _currSession, PurchaseRequisition _locPurchaseRequisitionXPO)
        {
            try
            {
                if (_locPurchaseRequisitionXPO != null)
                {
                    XPCollection<PurchaseRequisitionLine> _locPurchaseRequisitionLines = new XPCollection<PurchaseRequisitionLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locPurchaseRequisitionLines != null && _locPurchaseRequisitionLines.Count > 0)
                    {

                        foreach (PurchaseRequisitionLine _locPurchaseRequisitionLine in _locPurchaseRequisitionLines)
                        {
                            if (_locPurchaseRequisitionLine.Status == Status.Progress || _locPurchaseRequisitionLine.Status == Status.Posted)
                            {
                                _locPurchaseRequisitionLine.PostedCount = _locPurchaseRequisitionLine.PostedCount + 1;
                                _locPurchaseRequisitionLine.Save();
                                _locPurchaseRequisitionLine.Session.CommitTransaction();
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void SetStatusPurchaseRequisitionLine(Session _currSession, PurchaseRequisition _locPurchaseRequisitionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locPurchaseRequisitionXPO != null)
                {
                    XPCollection<PurchaseRequisitionLine> _locPurchaseRequisitionLines = new XPCollection<PurchaseRequisitionLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locPurchaseRequisitionLines != null && _locPurchaseRequisitionLines.Count > 0)
                    {

                        foreach (PurchaseRequisitionLine _locPurchaseRequisitionLine in _locPurchaseRequisitionLines)
                        {
                            if (_locPurchaseRequisitionLine.Status == Status.Progress || _locPurchaseRequisitionLine.Status == Status.Posted)
                            {
                                if (_locPurchaseRequisitionLine.RmDQty == 0 && _locPurchaseRequisitionLine.RmQty == 0 && _locPurchaseRequisitionLine.RmTQty == 0)
                                {
                                    _locPurchaseRequisitionLine.Status = Status.Close;
                                    _locPurchaseRequisitionLine.ActivationQuantity = true;
                                    _locPurchaseRequisitionLine.ActivationPosting = true;
                                    _locPurchaseRequisitionLine.StatusDate = now;
                                }
                                else
                                {
                                    _locPurchaseRequisitionLine.Status = Status.Posted;
                                    _locPurchaseRequisitionLine.StatusDate = now;
                                }
                                _locPurchaseRequisitionLine.Save();
                                _locPurchaseRequisitionLine.Session.CommitTransaction();
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void SetNormalQuantity(Session _currSession, PurchaseRequisition _locPurchaseRequisitionXPO)
        {
            try
            {
                if (_locPurchaseRequisitionXPO != null)
                {
                    XPCollection<PurchaseRequisitionLine> _locPurchaseRequisitionLines = new XPCollection<PurchaseRequisitionLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO),
                                                                                     new BinaryOperator("Select", true)));

                    if (_locPurchaseRequisitionLines != null && _locPurchaseRequisitionLines.Count > 0)
                    {
                        foreach (PurchaseRequisitionLine _locSalesQuotationLine in _locPurchaseRequisitionLines)
                        {
                            if (_locSalesQuotationLine.Status == Status.Progress || _locSalesQuotationLine.Status == Status.Posted || _locSalesQuotationLine.Status == Status.Close)
                            {
                                if (_locSalesQuotationLine.DQty > 0 || _locSalesQuotationLine.Qty > 0)
                                {
                                    _locSalesQuotationLine.Select = false;
                                    _locSalesQuotationLine.DQty = 0;
                                    _locSalesQuotationLine.Qty = 0;
                                    _locSalesQuotationLine.Save();
                                    _locSalesQuotationLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void SetFinalStatusPurchaseRequisition(Session _currSession, PurchaseRequisition _locPurchaseRequisitionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locPurchaseRequisitionLineCount = 0;

                if (_locPurchaseRequisitionXPO != null)
                {

                    XPCollection<PurchaseRequisitionLine> _locPurchaseRequisitionLines = new XPCollection<PurchaseRequisitionLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO)));

                    if (_locPurchaseRequisitionLines != null && _locPurchaseRequisitionLines.Count() > 0)
                    {
                        _locPurchaseRequisitionLineCount = _locPurchaseRequisitionLines.Count();

                        foreach (PurchaseRequisitionLine _locPurchaseRequisitionLine in _locPurchaseRequisitionLines)
                        {
                            if (_locPurchaseRequisitionLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }
                    }

                    if (_locStatusCount == _locPurchaseRequisitionLineCount)
                    {
                        _locPurchaseRequisitionXPO.ActivationPosting = true;
                        _locPurchaseRequisitionXPO.Status = Status.Close;
                        _locPurchaseRequisitionXPO.StatusDate = now;
                        _locPurchaseRequisitionXPO.Save();
                        _locPurchaseRequisitionXPO.Session.CommitTransaction();
                    }
                    else
                    {
                        _locPurchaseRequisitionXPO.Status = Status.Posted;
                        _locPurchaseRequisitionXPO.StatusDate = now;
                        _locPurchaseRequisitionXPO.Save();
                        _locPurchaseRequisitionXPO.Session.CommitTransaction();
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        #endregion GetPR

        #region GetPROld

        private void SetRemainQuantityOld(Session _currSession, PurchaseRequisitionLine _locPurchaseRequisitionLine)
        {
            try
            {
                DateTime now = DateTime.Now;

                #region Remain Qty  with PostedCount != 0
                if (_locPurchaseRequisitionLine.PostedCount > 0)
                {
                    double _locRmDqty = 0;
                    double _locRmQty = 0;
                    double _locInvLineTotal = 0;
                    double _locRmTQty = 0;
                    bool _locActivationPosting = false;
                    Status _locStatus = Status.Posted;

                    if (_locPurchaseRequisitionLine.RmDQty > 0 || _locPurchaseRequisitionLine.RmQty > 0 && _locPurchaseRequisitionLine.MxTQty != _locPurchaseRequisitionLine.RmTQty)
                    {
                        _locRmDqty = _locPurchaseRequisitionLine.RmDQty - _locPurchaseRequisitionLine.DQty;
                        _locRmQty = _locPurchaseRequisitionLine.RmQty - _locPurchaseRequisitionLine.Qty;

                        if (_locPurchaseRequisitionLine.Item != null && _locPurchaseRequisitionLine.MxUOM != null && _locPurchaseRequisitionLine.MxDUOM != null)
                        {
                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("Item", _locPurchaseRequisitionLine.Item),
                                                     new BinaryOperator("UOM", _locPurchaseRequisitionLine.MxUOM),
                                                     new BinaryOperator("DefaultUOM", _locPurchaseRequisitionLine.MxDUOM),
                                                     new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDqty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDqty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDqty;
                                }
                            }
                        }
                        else
                        {
                            _locInvLineTotal = _locRmQty + _locRmDqty;
                        }

                        if (_locRmDqty > 0 || _locRmQty > 0)
                        {
                            _locRmTQty = _locPurchaseRequisitionLine.RmTQty - _locPurchaseRequisitionLine.TQty;
                        }
                        else
                        {
                            _locRmTQty = _locPurchaseRequisitionLine.RmTQty - _locPurchaseRequisitionLine.TQty;
                            _locActivationPosting = true;
                            _locStatus = Status.Close;
                        }


                        _locPurchaseRequisitionLine.ActivationPosting = _locActivationPosting;
                        _locPurchaseRequisitionLine.Select = false;
                        _locPurchaseRequisitionLine.RmDQty = _locRmDqty;
                        _locPurchaseRequisitionLine.RmQty = _locRmQty;
                        _locPurchaseRequisitionLine.RmTQty = _locRmTQty;
                        _locPurchaseRequisitionLine.PostedCount = _locPurchaseRequisitionLine.PostedCount + 1;
                        _locPurchaseRequisitionLine.DQty = 0;
                        _locPurchaseRequisitionLine.Qty = 0;
                        _locPurchaseRequisitionLine.TQty = 0;
                        _locPurchaseRequisitionLine.Status = _locStatus;
                        _locPurchaseRequisitionLine.StatusDate = now;
                        _locPurchaseRequisitionLine.Save();
                        _locPurchaseRequisitionLine.Session.CommitTransaction();
                    }
                }
                #endregion Remain Qty  with PostedCount != 0

                #region Remain Qty with PostedCount == 0
                else
                {
                    double _locRmDqty = 0;
                    double _locRmQty = 0;
                    double _locInvLineTotal = 0;
                    double _locRmTQty = 0;
                    _locRmDqty = _locPurchaseRequisitionLine.MxDQty - _locPurchaseRequisitionLine.DQty;
                    _locRmQty = _locPurchaseRequisitionLine.MxQty - _locPurchaseRequisitionLine.Qty;

                    #region Remain > 0
                    if (_locRmDqty > 0 || _locRmQty > 0)
                    {
                        if (_locPurchaseRequisitionLine.Item != null && _locPurchaseRequisitionLine.MxUOM != null && _locPurchaseRequisitionLine.MxDUOM != null)
                        {
                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("Item", _locPurchaseRequisitionLine.Item),
                                                     new BinaryOperator("UOM", _locPurchaseRequisitionLine.MxUOM),
                                                     new BinaryOperator("DefaultUOM", _locPurchaseRequisitionLine.MxDUOM),
                                                     new BinaryOperator("Active", true)));
                            if (_locItemUOM != null)
                            {
                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDqty;
                                }
                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDqty;
                                }
                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDqty;
                                }
                            }
                        }
                        else
                        {
                            _locInvLineTotal = _locRmQty + _locRmDqty;
                        }

                        if (_locInvLineTotal > 0)
                        {
                            if (_locPurchaseRequisitionLine.RmTQty > 0)
                            {
                                _locRmTQty = _locPurchaseRequisitionLine.RmTQty - _locInvLineTotal;
                            }
                            else
                            {
                                _locRmTQty = _locInvLineTotal;
                            }

                        }

                        _locPurchaseRequisitionLine.RmDQty = _locRmDqty;
                        _locPurchaseRequisitionLine.RmQty = _locRmQty;
                        _locPurchaseRequisitionLine.RmTQty = _locRmTQty;
                        _locPurchaseRequisitionLine.PostedCount = _locPurchaseRequisitionLine.PostedCount + 1;
                        _locPurchaseRequisitionLine.Select = false;
                        _locPurchaseRequisitionLine.DQty = 0;
                        _locPurchaseRequisitionLine.Qty = 0;
                        _locPurchaseRequisitionLine.TQty = 0;
                        _locPurchaseRequisitionLine.Status = Status.Posted;
                        _locPurchaseRequisitionLine.StatusDate = now;
                        _locPurchaseRequisitionLine.Save();
                        _locPurchaseRequisitionLine.Session.CommitTransaction();
                    }
                    #endregion Remain > 0

                    #region Remain <= 0
                    if (_locRmDqty <= 0 && _locRmQty <= 0)
                    {
                        _locPurchaseRequisitionLine.ActivationPosting = true;
                        _locPurchaseRequisitionLine.RmDQty = 0;
                        _locPurchaseRequisitionLine.RmQty = 0;
                        _locPurchaseRequisitionLine.RmTQty = 0;
                        _locPurchaseRequisitionLine.PostedCount = _locPurchaseRequisitionLine.PostedCount + 1;
                        _locPurchaseRequisitionLine.Select = false;
                        _locPurchaseRequisitionLine.DQty = 0;
                        _locPurchaseRequisitionLine.Qty = 0;
                        _locPurchaseRequisitionLine.TQty = 0;
                        _locPurchaseRequisitionLine.Status = Status.Lock;
                        _locPurchaseRequisitionLine.StatusDate = now;
                        _locPurchaseRequisitionLine.Save();
                        _locPurchaseRequisitionLine.Session.CommitTransaction();
                    }
                    #endregion Remain < 0
                }
                #endregion Remain Qty with PostedCount == 0

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PrePurchaseOrder " + ex.ToString());
            }
        }

        private void SetFinalStatusPurchaseRequisitionOld(Session _currSession, PurchaseRequisition _locPurchaseRequisitionXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locCountStatus = 0;
                bool _locActivationPosting = false;

                if (_locPurchaseRequisitionXPO != null)
                {
                    XPCollection<PurchaseRequisitionLine> _locPurchaseRequisitionLines = new XPCollection<PurchaseRequisitionLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseRequisition", _locPurchaseRequisitionXPO)));
                    if (_locPurchaseRequisitionLines != null && _locPurchaseRequisitionLines.Count() > 0)
                    {
                        foreach (PurchaseRequisitionLine _locPurchaseRequisitionLine in _locPurchaseRequisitionLines)
                        {
                            if (_locPurchaseRequisitionLine.Status != Status.Close)
                            {
                                _locCountStatus = _locCountStatus + 1;
                            }
                        }
                    }

                    if (_locCountStatus > 0)
                    {
                        if (_locPurchaseRequisitionLines.Count() == _locCountStatus)
                        {
                            _locActivationPosting = true;
                        }
                    }

                    _locPurchaseRequisitionXPO.Status = Status.Posted;
                    _locPurchaseRequisitionXPO.StatusDate = now;
                    _locPurchaseRequisitionXPO.ActivationPosting = _locActivationPosting;
                    _locPurchaseRequisitionXPO.Save();
                    _locPurchaseRequisitionXPO.Session.CommitTransaction();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        private void PurchaseOrderGetPRAction_ExecuteOld(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        PurchaseOrder _locPurchaseOrderOS = (PurchaseOrder)_objectSpace.GetObject(obj);

                        if (_locPurchaseOrderOS != null)
                        {
                            if (_locPurchaseOrderOS.Code != null)
                            {
                                _currObjectId = _locPurchaseOrderOS.Code;

                                PurchaseOrder _locPurchaseOrderXPO = _currSession.FindObject<PurchaseOrder>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locPurchaseOrderXPO != null)
                                {
                                    if (_locPurchaseOrderXPO.Status == Status.Open || _locPurchaseOrderXPO.Status == Status.Progress)
                                    {
                                        if (_locPurchaseOrderXPO.Collection == true)
                                        {
                                            XPCollection<PurchaseRequisitionCollection> _locPurchaseRequisitionCollections = new XPCollection<PurchaseRequisitionCollection>
                                                              (_currSession, new GroupOperator(GroupOperatorType.And,
                                                               new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO)));

                                            if (_locPurchaseRequisitionCollections != null && _locPurchaseRequisitionCollections.Count() > 0)
                                            {
                                                foreach (PurchaseRequisitionCollection _locPurchaseRequisitionCollection in _locPurchaseRequisitionCollections)
                                                {
                                                    if (_locPurchaseRequisitionCollection.PurchaseRequisition != null &&
                                                        (_locPurchaseRequisitionCollection.Status == Status.Open || _locPurchaseRequisitionCollection.Status == Status.Progress))
                                                    {
                                                        if (_locPurchaseRequisitionCollection.PurchaseRequisition.Code != null)
                                                        {
                                                            PurchaseRequisition _locPurchaseRequisition = _currSession.FindObject<PurchaseRequisition>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                    new BinaryOperator("Code", _locPurchaseRequisitionCollection.PurchaseRequisition.Code)));

                                                            if (_locPurchaseRequisition != null)
                                                            {
                                                                XPCollection<PurchaseRequisitionLine> _locPurchaseRequisitionLines = new XPCollection<PurchaseRequisitionLine>
                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("PurchaseRequisition", _locPurchaseRequisition),
                                                                                                new BinaryOperator("Select", true)));

                                                                if (_locPurchaseRequisitionLines != null && _locPurchaseRequisitionLines.Count() > 0)
                                                                {
                                                                    foreach (PurchaseRequisitionLine _locPurchaseRequisitionLine in _locPurchaseRequisitionLines)
                                                                    {
                                                                        if (_locPurchaseRequisitionLine.PurchaseType == OrderType.Item
                                                                            && (_locPurchaseRequisitionLine.Status == Status.Progress || _locPurchaseRequisitionLine.Status == Status.Posted))
                                                                        {
                                                                            PurchaseOrderLine _locPurchaseOrderLine = _currSession.FindObject<PurchaseOrderLine>
                                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                                             new BinaryOperator("PurchaseOrder", _locPurchaseOrderXPO),
                                                                                                             new BinaryOperator("Item", _locPurchaseRequisitionLine.Item)));

                                                                            if (_locPurchaseOrderLine != null)
                                                                            {

                                                                                _locPurchaseOrderLine.MxDQty = _locPurchaseOrderLine.MxDQty + _locPurchaseRequisitionLine.DQty;
                                                                                _locPurchaseOrderLine.MxQty = _locPurchaseOrderLine.MxQty + _locPurchaseRequisitionLine.Qty;
                                                                                _locPurchaseOrderLine.MxTQty = _locPurchaseOrderLine.MxTQty + _locPurchaseRequisitionLine.TQty;
                                                                                _locPurchaseOrderLine.MxTUAmount = _locPurchaseOrderLine.MxTUAmount + (_locPurchaseOrderLine.MxUAmount * _locPurchaseRequisitionLine.TQty);
                                                                                _locPurchaseOrderLine.DQty = _locPurchaseOrderLine.DQty + _locPurchaseRequisitionLine.DQty;
                                                                                _locPurchaseOrderLine.Qty = _locPurchaseOrderLine.Qty + _locPurchaseRequisitionLine.Qty;
                                                                                _locPurchaseOrderLine.TQty = _locPurchaseOrderLine.TQty + _locPurchaseRequisitionLine.TQty;
                                                                                _locPurchaseOrderLine.TUAmount = _locPurchaseOrderLine.TUAmount + (_locPurchaseOrderLine.UAmount * _locPurchaseRequisitionLine.TQty);
                                                                                _locPurchaseOrderLine.Save();
                                                                                _locPurchaseOrderLine.Session.CommitTransaction();

                                                                                //SetRemainQuantity(_currSession, _locPurchaseRequisitionLine);
                                                                            }
                                                                            else
                                                                            {
                                                                                PurchaseOrderLine _saveDataPurchaseOrderLine = new PurchaseOrderLine(_currSession)
                                                                                {
                                                                                    PurchaseType = _locPurchaseRequisitionLine.PurchaseType,
                                                                                    Item = _locPurchaseRequisitionLine.Item,
                                                                                    Description = _locPurchaseRequisitionLine.Description,
                                                                                    MxDQty = _locPurchaseRequisitionLine.DQty,
                                                                                    MxDUOM = _locPurchaseRequisitionLine.DUOM,
                                                                                    MxQty = _locPurchaseRequisitionLine.Qty,
                                                                                    MxUOM = _locPurchaseRequisitionLine.UOM,
                                                                                    MxTQty = _locPurchaseRequisitionLine.TQty,
                                                                                    MxUAmount = _locPurchaseRequisitionLine.MxUAmount,
                                                                                    MxTUAmount = _locPurchaseRequisitionLine.TQty * _locPurchaseRequisitionLine.MxUAmount,
                                                                                    DQty = _locPurchaseRequisitionLine.DQty,
                                                                                    DUOM = _locPurchaseRequisitionLine.DUOM,
                                                                                    Qty = _locPurchaseRequisitionLine.Qty,
                                                                                    UOM = _locPurchaseRequisitionLine.UOM,
                                                                                    TQty = _locPurchaseRequisitionLine.TQty,
                                                                                    UAmount = _locPurchaseRequisitionLine.MxUAmount,
                                                                                    TUAmount = _locPurchaseRequisitionLine.TQty * _locPurchaseRequisitionLine.MxUAmount,
                                                                                    PurchaseOrder = _locPurchaseOrderXPO,
                                                                                };
                                                                                _saveDataPurchaseOrderLine.Save();
                                                                                _saveDataPurchaseOrderLine.Session.CommitTransaction();

                                                                                //SetRemainQuantity(_currSession, _locPurchaseRequisitionLine);
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                //SetFinalStatusPurchaseRequisition(_currSession, _locPurchaseRequisition);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    SuccessMessageShow("Purchase Requisition Has Been Successfully Getting into Purchase Order");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Purchase Order Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data Purchase Order Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = PurchaseOrder " + ex.ToString());
            }
        }

        #endregion GetPROld

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

    }
}
