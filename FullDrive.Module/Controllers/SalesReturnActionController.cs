﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class SalesReturnActionController : ViewController
    {
        #region Default

        private ChoiceActionItem _selectionListviewFilter;
        public SalesReturnActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            SalesReturnFilterAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {
                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                SalesReturnFilterAction.Items.Add(_selectionListviewFilter);
            }
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        private void SalesReturnProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        SalesReturn _locSalesReturnOS = (SalesReturn)_objectSpace.GetObject(obj);

                        if (_locSalesReturnOS != null)
                        {
                            if (_locSalesReturnOS.Code != null)
                            {
                                _currObjectId = _locSalesReturnOS.Code;

                                SalesReturn _locSalesReturnXPO = _currSession.FindObject<SalesReturn>
                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                  new BinaryOperator("Code", _currObjectId)));

                                if (_locSalesReturnXPO != null)
                                {
                                    if (_locSalesReturnXPO.Status == Status.Open || _locSalesReturnXPO.Status == Status.Progress)
                                    {
                                        if (_locSalesReturnXPO.Status == Status.Open)
                                        {
                                            _locSalesReturnXPO.Status = Status.Progress;
                                            _locSalesReturnXPO.StatusDate = now;
                                            _locSalesReturnXPO.Save();
                                            _locSalesReturnXPO.Session.CommitTransaction();
                                        }

                                        XPCollection<SalesReturnLine> _locSalesReturnLines = new XPCollection<SalesReturnLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                             new BinaryOperator("SalesReturn", _locSalesReturnXPO),
                                                                                             new BinaryOperator("Status", Status.Open)));

                                        if (_locSalesReturnLines != null && _locSalesReturnLines.Count > 0)
                                        {
                                            foreach (SalesReturnLine _locSalesReturnLine in _locSalesReturnLines)
                                            {
                                                _locSalesReturnLine.Status = Status.Progress;
                                                _locSalesReturnLine.StatusDate = now;
                                                _locSalesReturnLine.Save();
                                                _locSalesReturnLine.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                    SuccessMessageShow("Sales Return has been successfully updated to progress");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Sales Return Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data Sales Return Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesReturn " + ex.ToString());
            }
        }

        private void SalesReturnPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        SalesReturn _locSalesReturnOS = (SalesReturn)_objectSpace.GetObject(obj);

                        if (_locSalesReturnOS != null)
                        {
                            if (_locSalesReturnOS.Code != null)
                            {
                                _currObjectId = _locSalesReturnOS.Code;

                                SalesReturn _locSalesReturnXPO = _currSession.FindObject<SalesReturn>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locSalesReturnXPO != null)
                                {
                                    if (_locSalesReturnXPO.Status == Status.Progress || _locSalesReturnXPO.Status == Status.Posted)
                                    {
                                        SetInventoryTransferReplacedFullAndDebitMemo(_currSession, _locSalesReturnXPO);
                                        SetDebitMemo(_currSession, _locSalesReturnXPO);
                                        SetFinalStatus(_currSession, _locSalesReturnXPO);
                                        SetChangeStatusSalesReturnLine(_currSession, _locSalesReturnXPO);
                                    }

                                    SuccessMessageShow("Sales Return has been successfully Posted");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Sales Return Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data Sales Return Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesReturn " + ex.ToString());
            }
        }

        private void SalesReturnFilterAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(SalesReturn)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesReturn " + ex.ToString());
            }
        }

        #region Posting

        private void SetInventoryTransferReplacedFullAndDebitMemo(Session _currSession, SalesReturn _locSalesReturnXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _locDocCode = null;
                InventoryTransferIn _locInventoryTransferIn = null;

                XPCollection<SalesReturnLine> _locSalesReturnLines = new XPCollection<SalesReturnLine>(_currSession,
                                                                     new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("SalesReturn", _locSalesReturnXPO)));

                if (_locSalesReturnLines != null && _locSalesReturnLines.Count() > 0)
                {
                    if (_locSalesReturnXPO.SalesOrder != null && _locSalesReturnXPO != null)
                    {
                        _locInventoryTransferIn = _currSession.FindObject<InventoryTransferIn>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("SalesReturn", _locSalesReturnXPO),
                                                   new BinaryOperator("SalesOrder", _locSalesReturnXPO.SalesOrder)));
                    }
                    else
                    {
                        _locInventoryTransferIn = _currSession.FindObject<InventoryTransferIn>
                                                  (new GroupOperator(GroupOperatorType.And,
                                                   new BinaryOperator("SalesReturn", _locSalesReturnXPO)));
                    }


                    if (_locInventoryTransferIn != null)
                    {
                        #region UpdateInventoryTransfer

                        foreach (SalesReturnLine _locSalesReturnLine in _locSalesReturnLines)
                        {
                            if (_locSalesReturnLine.Status == Status.Progress || _locSalesReturnLine.Status == Status.Posted)
                            {
                                if (_locSalesReturnLine.Select == true)
                                {
                                    InventoryTransferInLine _locInventoryTransferInLine = _currSession.FindObject<InventoryTransferInLine>
                                                                                          (new GroupOperator(GroupOperatorType.And,
                                                                                           new BinaryOperator("InventoryTransferIn", _locInventoryTransferIn),
                                                                                           new BinaryOperator("Item", _locSalesReturnLine.Item),
                                                                                           new BinaryOperator("Location", _locSalesReturnLine.Location),
                                                                                           new BinaryOperator("BinLocation", _locSalesReturnLine.BinLocation)));

                                    if (_locInventoryTransferInLine == null)
                                    {
                                        InventoryTransferInLine _locSaveDataInventoryTransferInLine = new InventoryTransferInLine(_currSession)
                                        {
                                            Item = _locSalesReturnLine.Item,
                                            Description = _locSalesReturnLine.Description,
                                            MxDQty = _locSalesReturnLine.DQty,
                                            MxDUOM = _locSalesReturnLine.DUOM,
                                            MxQty = _locSalesReturnLine.Qty,
                                            MxUOM = _locSalesReturnLine.UOM,
                                            ReturnType = _locSalesReturnLine.ReturnType,
                                            Location = _locSalesReturnLine.Location,
                                            BinLocation = _locSalesReturnLine.BinLocation,
                                            StockType = _locSalesReturnLine.StockType,
                                            EstimatedDate = _locSalesReturnLine.EstimatedDate,
                                            InventoryTransferIn = _locInventoryTransferIn,
                                        };
                                        _locSaveDataInventoryTransferInLine.Save();
                                        _locSaveDataInventoryTransferInLine.Session.CommitTransaction();
                                    }
                                }
                            }
                        }

                        #endregion UpdateInventoryTransfer
                    }
                    else
                    {
                        #region CreateNewInventoryTransfer

                        DocumentType _locDocumentTypeXPO = _currSession.FindObject<DocumentType>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("TransferType", DirectionType.External),
                                                            new BinaryOperator("InventoryMovingType", InventoryMovingType.Return),
                                                            new BinaryOperator("ObjectList", ObjectList.InventoryTransferIn),
                                                            new BinaryOperator("DocumentRule", DocumentRule.Customer),
                                                            new BinaryOperator("Active", true),
                                                            new BinaryOperator("Default", true)));

                        if (_locDocumentTypeXPO != null)
                        {
                            _locDocCode = _globFunc.GetDocumentNumberingUnlockOptimisticRecord(_currSession.DataLayer, _locDocumentTypeXPO);

                            if (_locDocCode != null)
                            {
                                InventoryTransferIn _locSaveDataInventoryInTransfer = new InventoryTransferIn(_currSession)
                                {
                                    TransferType = _locDocumentTypeXPO.TransferType,
                                    InventoryMovingType = _locDocumentTypeXPO.InventoryMovingType,
                                    ObjectList = _locDocumentTypeXPO.ObjectList,
                                    DocumentType = _locDocumentTypeXPO,
                                    DocNo = _locDocCode,
                                    BusinessPartner = _locSalesReturnXPO.BillToCustomer,
                                    BusinessPartnerCountry = _locSalesReturnXPO.BillToCustomer.Country,
                                    BusinessPartnerCity = _locSalesReturnXPO.BillToCustomer.City,
                                    BusinessPartnerAddress = _locSalesReturnXPO.BillToAddress,
                                    EstimatedDate = _locSalesReturnXPO.EstimatedDate,
                                    SalesReturn = _locSalesReturnXPO,
                                    SalesOrder = _locSalesReturnXPO.SalesOrder,

                                };
                                _locSaveDataInventoryInTransfer.Save();
                                _locSaveDataInventoryInTransfer.Session.CommitTransaction();

                                InventoryTransferIn _locInventoryTransferIn2x = _currSession.FindObject<InventoryTransferIn>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("DocNo", _locDocCode)));

                                if (_locInventoryTransferIn2x != null)
                                {
                                    foreach (SalesReturnLine _locSalesReturnLine in _locSalesReturnLines)
                                    {
                                        if (_locSalesReturnLine.Status == Status.Progress || _locSalesReturnLine.Status == Status.Posted)
                                        {
                                            if (_locSalesReturnLine.Select == true)
                                            {
                                                InventoryTransferInLine _locSaveDataInventoryTransferInLine = new InventoryTransferInLine(_currSession)
                                                {
                                                    Item = _locSalesReturnLine.Item,
                                                    Description = _locSalesReturnLine.Description,
                                                    MxDQty = _locSalesReturnLine.DQty,
                                                    MxDUOM = _locSalesReturnLine.DUOM,
                                                    MxQty = _locSalesReturnLine.Qty,
                                                    MxUOM = _locSalesReturnLine.UOM,
                                                    ReturnType = _locSalesReturnLine.ReturnType,
                                                    Location = _locSalesReturnLine.Location,
                                                    BinLocation = _locSalesReturnLine.BinLocation,
                                                    StockType = _locSalesReturnLine.StockType,
                                                    EstimatedDate = _locSalesReturnLine.EstimatedDate,
                                                    InventoryTransferIn = _locInventoryTransferIn2x,
                                                };
                                                _locSaveDataInventoryTransferInLine.Save();
                                                _locSaveDataInventoryTransferInLine.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        #endregion CreateNewInventoryTransfer
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesReturn " + ex.ToString());
            }
        }

        private void SetDebitMemo(Session _currSession, SalesReturn _locSalesReturnXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _locSignCode = null;
                DebitMemo _locDebitMemo = null;
                double _locUnitPrice = 0;

                //Grouping based on ReturnType
                XPQuery<SalesReturnLine> _salsReturnLineQuery = new XPQuery<SalesReturnLine>(_currSession);

                var _slsReturnLines = from srlQ in _salsReturnLineQuery
                                      where (srlQ.Select == true && srlQ.Status == Status.Progress
                                      && srlQ.ReturnType == ReturnType.DebitMemo
                                      && srlQ.SalesReturn == _locSalesReturnXPO)
                                      group srlQ by srlQ.ReturnType into g
                                      select new { ReturnType = g.Key };

                if (_slsReturnLines != null && _slsReturnLines.Count() > 0)
                {
                    foreach (var _slsReturnLine in _slsReturnLines)
                    {
                        XPCollection<SalesReturnLine> _locSalesReturnLines = new XPCollection<SalesReturnLine>(_currSession,
                                                                             new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("Select", true),
                                                                             new BinaryOperator("SalesReturn", _locSalesReturnXPO),
                                                                             new BinaryOperator("ReturnType", _slsReturnLine.ReturnType)));

                        if (_locSalesReturnLines != null && _locSalesReturnLines.Count() > 0)
                        {
                            if (_locSalesReturnXPO.SalesOrder != null && _locSalesReturnXPO != null)
                            {
                                _locDebitMemo = _currSession.FindObject<DebitMemo>
                                                (new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("SalesReturn", _locSalesReturnXPO),
                                                 new BinaryOperator("SalesOrder", _locSalesReturnXPO.SalesOrder)));
                            }
                            else
                            {
                                _locDebitMemo = _currSession.FindObject<DebitMemo>
                                                (new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("SalesReturn", _locSalesReturnXPO)));
                            }

                            if (_locDebitMemo != null)
                            {
                                #region UpdateDebitMemo

                                foreach (SalesReturnLine _locSalesReturnLine in _locSalesReturnLines)
                                {
                                    if (_locSalesReturnLine.Status == Status.Progress || _locSalesReturnLine.Status == Status.Posted)
                                    {
                                        if (_locSalesReturnLine.Select == true)
                                        {
                                            DebitMemoLine _locDebitMemoLine = _currSession.FindObject<DebitMemoLine>
                                                                              (new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("DebitMemo", _locDebitMemo),
                                                                               new BinaryOperator("Item", _locSalesReturnLine.Item),
                                                                               new BinaryOperator("Location", _locSalesReturnLine.Location),
                                                                               new BinaryOperator("BinLocation", _locSalesReturnLine.BinLocation)));

                                            if (_locDebitMemoLine == null)
                                            {
                                                if (_locSalesReturnXPO.SalesOrder != null && _locSalesReturnLine.Item != null)
                                                {
                                                    _locUnitPrice = GetUnitPriceBySalesOrder(_currSession, _locSalesReturnXPO.SalesOrder, _locSalesReturnLine.Item);
                                                }

                                                DebitMemoLine _locSaveDataDebitMemoLine = new DebitMemoLine(_currSession)
                                                {
                                                    Item = _locSalesReturnLine.Item,
                                                    Description = _locSalesReturnLine.Description,
                                                    MxDQty = _locSalesReturnLine.DQty,
                                                    MxDUOM = _locSalesReturnLine.DUOM,
                                                    MxQty = _locSalesReturnLine.Qty,
                                                    MxUOM = _locSalesReturnLine.UOM,
                                                    DQty = _locSalesReturnLine.DQty,
                                                    DUOM = _locSalesReturnLine.DUOM,
                                                    Qty = _locSalesReturnLine.Qty,
                                                    UOM = _locSalesReturnLine.UOM,
                                                    TQty = _locSalesReturnLine.TQty,
                                                    UAmount = _locUnitPrice,
                                                    TUAmount = _locUnitPrice * _locSalesReturnLine.TQty,
                                                    Location = _locSalesReturnLine.Location,
                                                    BinLocation = _locSalesReturnLine.BinLocation,
                                                    StockType = _locSalesReturnLine.StockType,
                                                    DebitMemo = _locDebitMemo,
                                                };
                                                _locSaveDataDebitMemoLine.Save();
                                                _locSaveDataDebitMemoLine.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                }

                                #endregion UpdateDebitMemo
                            }
                            else
                            {
                                #region CreateNewDebitMemo

                                _locSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.DebitMemo);

                                if (_locSignCode != null)
                                {
                                    DebitMemo _locSaveDataDebitMemo = new DebitMemo(_currSession)
                                    {
                                        BillToCustomer = _locSalesReturnXPO.BillToCustomer,
                                        BillToAddress = _locSalesReturnXPO.BillToAddress,
                                        BillToCity = _locSalesReturnXPO.BillToCity,
                                        BillToContact = _locSalesReturnXPO.BillToContact,
                                        BillToCountry = _locSalesReturnXPO.BillToCountry,
                                        TOP = _locSalesReturnXPO.SalesOrder.TOP,
                                        SignCode = _locSignCode,
                                        SalesReturn = _locSalesReturnXPO,
                                        SalesOrder = _locSalesReturnXPO.SalesOrder,

                                    };
                                    _locSaveDataDebitMemo.Save();
                                    _locSaveDataDebitMemo.Session.CommitTransaction();

                                    DebitMemo _locDebitMemo2x = _currSession.FindObject<DebitMemo>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                 new BinaryOperator("SignCode", _locSignCode)));

                                    if (_locDebitMemo2x != null)
                                    {
                                        foreach (SalesReturnLine _locSalesReturnLine in _locSalesReturnLines)
                                        {
                                            if (_locSalesReturnLine.Status == Status.Progress || _locSalesReturnLine.Status == Status.Posted)
                                            {
                                                if (_locSalesReturnLine.Select == true)
                                                {
                                                    if (_locSalesReturnXPO.SalesOrder != null && _locSalesReturnLine.Item != null)
                                                    {
                                                        _locUnitPrice = GetUnitPriceBySalesOrder(_currSession, _locSalesReturnXPO.SalesOrder, _locSalesReturnLine.Item);
                                                    }

                                                    DebitMemoLine _locSaveDataDebitMemoLine = new DebitMemoLine(_currSession)
                                                    {
                                                        Item = _locSalesReturnLine.Item,
                                                        Description = _locSalesReturnLine.Description,
                                                        MxDQty = _locSalesReturnLine.DQty,
                                                        MxDUOM = _locSalesReturnLine.DUOM,
                                                        MxQty = _locSalesReturnLine.Qty,
                                                        MxUOM = _locSalesReturnLine.UOM,
                                                        DQty = _locSalesReturnLine.DQty,
                                                        DUOM = _locSalesReturnLine.DUOM,
                                                        Qty = _locSalesReturnLine.Qty,
                                                        UOM = _locSalesReturnLine.UOM,
                                                        TQty = _locSalesReturnLine.TQty,
                                                        UAmount = _locUnitPrice,
                                                        TUAmount = _locUnitPrice * _locSalesReturnLine.TQty,
                                                        Location = _locSalesReturnLine.Location,
                                                        BinLocation = _locSalesReturnLine.BinLocation,
                                                        StockType = _locSalesReturnLine.StockType,
                                                        DebitMemo = _locDebitMemo2x,
                                                    };
                                                    _locSaveDataDebitMemoLine.Save();
                                                    _locSaveDataDebitMemoLine.Session.CommitTransaction();

                                                }
                                            }
                                        }
                                    }
                                }

                                #endregion CreateNewDebitMemo
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesReturn " + ex.ToString());
            }
        }

        private double GetUnitPriceBySalesOrder(Session _currSession, SalesOrder _locSalesOrder, Item _locItem)
        {
            double _result = 0;
            try
            {
                if (_locSalesOrder != null)
                {
                    SalesOrderLine _locSalesOrderLine = _currSession.FindObject<SalesOrderLine>
                                                        (new GroupOperator(GroupOperatorType.And,
                                                         new BinaryOperator("SalesOrder", _locSalesOrder),
                                                         new BinaryOperator("Item", _locItem)));
                    if (_locSalesOrderLine != null)
                    {
                        _result = _locSalesOrderLine.UAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesReturn " + ex.ToString());
            }
            return _result;
        }

        private void SetFinalStatus(Session _currSession, SalesReturn _locSalesReturnXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locCountStatus = 0;

                if (_locSalesReturnXPO != null)
                {
                    XPCollection<SalesReturnLine> _locSalesReturnLines = new XPCollection<SalesReturnLine>
                                                                         (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                          new BinaryOperator("SalesReturn", _locSalesReturnXPO)));

                    if (_locSalesReturnLines != null && _locSalesReturnLines.Count() > 0)
                    {
                        foreach (SalesReturnLine _locSalesReturnLine in _locSalesReturnLines)
                        {
                            if (_locSalesReturnLine.Status != Status.Close)
                            {
                                _locCountStatus = _locCountStatus + 1;
                            }
                        }
                    }

                    if (_locCountStatus == 0)
                    {
                        _locSalesReturnXPO.Status = Status.Close;
                        _locSalesReturnXPO.StatusDate = now;
                        _locSalesReturnXPO.ActivationPosting = true;
                        _locSalesReturnXPO.Save();
                        _locSalesReturnXPO.Session.CommitTransaction();
                    }
                    else
                    {
                        _locSalesReturnXPO.Status = Status.Posted;
                        _locSalesReturnXPO.StatusDate = now;
                        _locSalesReturnXPO.Save();
                        _locSalesReturnXPO.Session.CommitTransaction();
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesReturn " + ex.ToString());
            }
        }

        private void SetChangeStatusSalesReturnLine(Session _currSession, SalesReturn _locSalesReturnXPO)
        {
            try
            {
                DateTime now = DateTime.Now;

                if (_locSalesReturnXPO != null)
                {
                    XPCollection<SalesReturnLine> _locSalesReturnLines = new XPCollection<SalesReturnLine>
                                                                         (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                          new BinaryOperator("SalesReturn", _locSalesReturnXPO)));

                    if (_locSalesReturnLines != null && _locSalesReturnLines.Count() > 0)
                    {
                        foreach (SalesReturnLine _locSalesReturnLine in _locSalesReturnLines)
                        {
                            if (_locSalesReturnLine.Select == true)
                            {
                                if (_locSalesReturnLine.Status == Status.Progress || _locSalesReturnLine.Status == Status.Posted)
                                {
                                    _locSalesReturnLine.ActivationPosting = true;
                                    _locSalesReturnLine.Status = Status.Close;
                                    _locSalesReturnLine.StatusDate = now;
                                    _locSalesReturnLine.Save();
                                    _locSalesReturnLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = SalesReturn " + ex.ToString());
            }
        }

        #endregion Posting

        #region Global Method

        private void SuccessMessageShow(string _locActionName)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format("{0} !", _locActionName);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

    }
}
