﻿namespace FullDrive.Module.Controllers
{
    partial class ReceivableSalesCollectionActionController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ReceivableSalesCollectionShowSIAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ReceivableSalesCollectionShowSPIAction = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // ReceivableSalesCollectionShowSIAction
            // 
            this.ReceivableSalesCollectionShowSIAction.Caption = "Show SI";
            this.ReceivableSalesCollectionShowSIAction.ConfirmationMessage = null;
            this.ReceivableSalesCollectionShowSIAction.Id = "ReceivableSalesCollectionShowSIActionId";
            this.ReceivableSalesCollectionShowSIAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ReceivableSalesCollection);
            this.ReceivableSalesCollectionShowSIAction.ToolTip = null;
            this.ReceivableSalesCollectionShowSIAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ReceivableSalesCollectionShowSIAction_Execute);
            // 
            // ReceivableSalesCollectionShowSPIAction
            // 
            this.ReceivableSalesCollectionShowSPIAction.Caption = "Show SPI";
            this.ReceivableSalesCollectionShowSPIAction.ConfirmationMessage = null;
            this.ReceivableSalesCollectionShowSPIAction.Id = "ReceivableSalesCollectionShowSPIActionId";
            this.ReceivableSalesCollectionShowSPIAction.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.ReceivableSalesCollection);
            this.ReceivableSalesCollectionShowSPIAction.ToolTip = null;
            this.ReceivableSalesCollectionShowSPIAction.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ReceivableSalesCollectionShowSPIAction_Execute);
            // 
            // ReceivableSalesCollectionActionController
            // 
            this.Actions.Add(this.ReceivableSalesCollectionShowSIAction);
            this.Actions.Add(this.ReceivableSalesCollectionShowSPIAction);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction ReceivableSalesCollectionShowSIAction;
        private DevExpress.ExpressApp.Actions.SimpleAction ReceivableSalesCollectionShowSPIAction;
    }
}
