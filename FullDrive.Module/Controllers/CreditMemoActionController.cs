﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CreditMemoActionController : ViewController
    {
        #region Default

        private ChoiceActionItem _selectionListviewFilter;
        public CreditMemoActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            CreditMemoListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {

                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                CreditMemoListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        private void CreditMemoProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        CreditMemo _locCreditMemoOS = (CreditMemo)_objectSpace.GetObject(obj);

                        if (_locCreditMemoOS != null)
                        {
                            if (_locCreditMemoOS.Code != null)
                            {
                                _currObjectId = _locCreditMemoOS.Code;

                                CreditMemo _locCreditMemoXPO = _currSession.FindObject<CreditMemo>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locCreditMemoXPO != null)
                                {
                                    if (_locCreditMemoXPO.Status == Status.Open)
                                    {
                                        _locCreditMemoXPO.Status = Status.Progress;
                                        _locCreditMemoXPO.StatusDate = now;
                                        _locCreditMemoXPO.Save();
                                        _locCreditMemoXPO.Session.CommitTransaction();

                                        XPCollection<CreditMemoLine> _locCreditMemoLines = new XPCollection<CreditMemoLine>
                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("CreditMemo", _locCreditMemoXPO)));

                                        if (_locCreditMemoLines != null && _locCreditMemoLines.Count > 0)
                                        {
                                            foreach (CreditMemoLine _locCreditMemoLine in _locCreditMemoLines)
                                            {
                                                if (_locCreditMemoLine.Status == Status.Open)
                                                {
                                                    _locCreditMemoLine.Status = Status.Progress;
                                                    _locCreditMemoLine.StatusDate = now;
                                                    _locCreditMemoLine.Save();
                                                    _locCreditMemoLine.Session.CommitTransaction();
                                                }
                                            }
                                        }
                                        SuccessMessageShow(_locCreditMemoXPO.Code + " has been change successfully to Progress");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Inventory Transfer Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data Inventory Transfer Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemo " + ex.ToString());
            }
        }

        private void CreditMemoPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        CreditMemo _locCreditMemoOS = (CreditMemo)_objectSpace.GetObject(obj);

                        if (_locCreditMemoOS != null)
                        {
                            if (_locCreditMemoOS.Code != null)
                            {
                                _currObjectId = _locCreditMemoOS.Code;

                                CreditMemo _locCreditMemoXPO = _currSession.FindObject<CreditMemo>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Code", _currObjectId)));

                                if (_locCreditMemoXPO != null)
                                {
                                    if(_locCreditMemoXPO.PurchaseOrder != null)
                                    {
                                        SetCreditMemoCollectionInSICAndSI(_currSession, _locCreditMemoXPO);
                                    }
                                    
                                    SetStatusPostingCreditMemoLine(_currSession, _locCreditMemoXPO);
                                    SetInvoiceAPReturnJournal(_currSession, _locCreditMemoXPO);
                                    SetReverseGoodsReturnJournal(_currSession, _locCreditMemoXPO);
                                    SetFinalStatusReturnCreditMemo(_currSession, _locCreditMemoXPO);
                                    SuccessMessageShow(_locCreditMemoXPO.Code + " has been change successfully to Posted");
                                }
                                else
                                {
                                    ErrorMessageShow("Data Inventory Transfer Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data Inventory Transfer Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemo " + ex.ToString());
            }
        }

        private void CreditMemoListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(CreditMemo)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemo " + ex.ToString());
            }
        }

        #region Posting

        private void SetReverseGoodsReturnJournal(Session _currSession, CreditMemo _locCreditMemoXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locTotalUnitPrice = 0;
                double _currCountPosted = 0;
                //Bikin Group by  dan Totalkan jumlah uang pergroup
                if (_locCreditMemoXPO != null)
                {
                    XPQuery<CreditMemoLine> _creditMemoLinesQuery = new XPQuery<CreditMemoLine>(_currSession);

                    var _creditMemoLines = from cml in _creditMemoLinesQuery
                                           where (cml.CreditMemo == _locCreditMemoXPO && (cml.Status == Status.Progress || cml.Status == Status.Posted))
                                           group cml by cml.Item.ItemAccountGroup into g
                                           select new { ItemAccountGroup = g.Key };

                    if (_creditMemoLines != null && _creditMemoLines.Count() > 0)
                    {
                        foreach (var _creditMemoLine in _creditMemoLines)
                        {
                            XPCollection<CreditMemoLine> _locCreMemLines = new XPCollection<CreditMemoLine>(_currSession,
                                                                            new GroupOperator(GroupOperatorType.And,
                                                                            new BinaryOperator("CreditMemo", _locCreditMemoXPO),
                                                                            new BinaryOperator("Item.ItemAccountGroup", _creditMemoLine.ItemAccountGroup)));

                            if (_locCreMemLines != null && _locCreMemLines.Count > 0)
                            {
                                foreach (CreditMemoLine _locCreMemLine in _locCreMemLines)
                                {
                                    if (_locCreMemLine.Status == Status.Posted || _locCreMemLine.Status == Status.Close)
                                    {
                                        _currCountPosted = _currCountPosted + 1;
                                    }
                                }

                                if (_currCountPosted == 0)
                                {
                                    #region MenghitungTotalUnitPriceBasedOnItemAccountGroup
                                    foreach (CreditMemoLine _locCreMemLine in _locCreMemLines)
                                    {
                                        if (_locCreMemLine.Status == Status.Progress || _locCreMemLine.Status == Status.Posted)
                                        {
                                            if (_locCreditMemoXPO.PurchaseReturn != null)
                                            {
                                                PurchaseReturnLine _locPurchaseReturnLine = _currSession.FindObject<PurchaseReturnLine>
                                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("PurchaseReturn", _locCreditMemoXPO.PurchaseReturn),
                                                                                 new BinaryOperator("Item", _locCreMemLine.Item)));

                                                if (_locPurchaseReturnLine != null)
                                                {
                                                    _locTotalUnitPrice = _locTotalUnitPrice + _locPurchaseReturnLine.TUAmount;
                                                }
                                            }
                                        }
                                    }
                                    #endregion MenghitungTotalUnitPriceBasedOnItemAccountGroup

                                    #region CreateReverseGoodsReceiptJournal

                                    #region CreateReverseGoodsReceiptJournalByItems
                                    XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("ItemAccountGroup", _creditMemoLine.ItemAccountGroup)));

                                    if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                    {
                                        foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                        {
                                            XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("JournalMap", _locJournalMapItem)));

                                            if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                            {
                                                foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                {
                                                    AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                 new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                 new BinaryOperator("OrderType", OrderType.Item),
                                                                                 new BinaryOperator("PostingMethod", PostingMethod.Return),
                                                                                 new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                    if (_locAccountMap != null)
                                                    {
                                                        XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("AccountMap", _locAccountMap),
                                                                                        new BinaryOperator("Active", true)));

                                                        if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                        {
                                                            double _locTotalAmountDebit = 0;
                                                            double _locTotalAmountCredit = 0;
                                                            double _locTotalBalance = 0;

                                                            foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                            {
                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                {
                                                                    _locTotalAmountCredit = _locTotalUnitPrice;
                                                                }
                                                                if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                {
                                                                    _locTotalAmountDebit = _locTotalUnitPrice;
                                                                }

                                                                GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                {
                                                                    PostingDate = now,
                                                                    PostingType = PostingType.Purchase,
                                                                    PostingMethod = PostingMethod.CreditMemo,
                                                                    Account = _locAccountMapLine.Account,
                                                                    Debit = _locTotalAmountDebit,
                                                                    Credit = _locTotalAmountCredit,
                                                                    PurchaseOrder = _locCreditMemoXPO.PurchaseOrder,
                                                                    PurchaseReturn = _locCreditMemoXPO.PurchaseReturn,
                                                                    CreditMemo = _locCreditMemoXPO,
                                                                };
                                                                _saveGeneralJournal.Save();
                                                                _saveGeneralJournal.Session.CommitTransaction();

                                                                if (_locAccountMapLine.Account.Code != null)
                                                                {
                                                                    ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                    if (_locCOA != null)
                                                                    {
                                                                        if (_locCOA.BalanceType == BalanceType.Change)
                                                                        {
                                                                            if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                            {
                                                                                if (_locTotalAmountDebit > 0)
                                                                                {
                                                                                    _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                }
                                                                                if (_locTotalAmountCredit > 0)
                                                                                {
                                                                                    _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                }
                                                                            }
                                                                            if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                            {
                                                                                if (_locTotalAmountDebit > 0)
                                                                                {
                                                                                    _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                }
                                                                                if (_locTotalAmountCredit > 0)
                                                                                {
                                                                                    _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                }
                                                                            }
                                                                        }

                                                                        _locCOA.Balance = _locTotalBalance;
                                                                        _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                        _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                        _locCOA.Save();
                                                                        _locCOA.Session.CommitTransaction();
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion CreateReverseGoodsReceiptJournalByItems

                                    #region CreateReverseGoodsReceiptJournalByBusinessPartner
                                    if (_locCreditMemoXPO.BuyFromVendor != null)
                                    {
                                        if (_locCreditMemoXPO.BuyFromVendor.BusinessPartnerAccountGroup != null)
                                        {
                                            XPCollection<JournalMap> _locJournalMapBusinessPartners = new XPCollection<JournalMap>
                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                               new BinaryOperator("BusinessPartnerAccountGroup", _locCreditMemoXPO.BuyFromVendor.BusinessPartnerAccountGroup)));

                                            if (_locJournalMapBusinessPartners != null && _locJournalMapBusinessPartners.Count() > 0)
                                            {
                                                foreach (JournalMap _locJournalMapBusinessPartner in _locJournalMapBusinessPartners)
                                                {
                                                    XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("JournalMap", _locJournalMapBusinessPartner)));

                                                    if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                    {
                                                        foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                        {
                                                            AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                         (new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                         new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                         new BinaryOperator("OrderType", OrderType.Item),
                                                                                         new BinaryOperator("PostingMethod", PostingMethod.Return),
                                                                                         new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                            if (_locAccountMap != null)
                                                            {
                                                                XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                new BinaryOperator("Active", true)));

                                                                if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                {
                                                                    double _locTotalAmountDebit = 0;
                                                                    double _locTotalAmountCredit = 0;
                                                                    double _locTotalBalance = 0;

                                                                    foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                    {
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            _locTotalAmountCredit = _locTotalUnitPrice;
                                                                        }
                                                                        if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            _locTotalAmountDebit = _locTotalUnitPrice;
                                                                        }

                                                                        GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                        {
                                                                            PostingDate = now,
                                                                            PostingType = PostingType.Purchase,
                                                                            PostingMethod = PostingMethod.CreditMemo,
                                                                            Account = _locAccountMapLine.Account,
                                                                            Debit = _locTotalAmountDebit,
                                                                            Credit = _locTotalAmountCredit,
                                                                            PurchaseOrder = _locCreditMemoXPO.PurchaseOrder,
                                                                            PurchaseReturn = _locCreditMemoXPO.PurchaseReturn,
                                                                            CreditMemo = _locCreditMemoXPO,
                                                                        };
                                                                        _saveGeneralJournal.Save();
                                                                        _saveGeneralJournal.Session.CommitTransaction();

                                                                        if (_locAccountMapLine.Account.Code != null)
                                                                        {
                                                                            ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                            if (_locCOA != null)
                                                                            {
                                                                                if (_locCOA.BalanceType == BalanceType.Change)
                                                                                {
                                                                                    if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                    if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        if (_locTotalAmountDebit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                        }
                                                                                        if (_locTotalAmountCredit > 0)
                                                                                        {
                                                                                            _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                        }
                                                                                    }
                                                                                }

                                                                                _locCOA.Balance = _locTotalBalance;
                                                                                _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                _locCOA.Save();
                                                                                _locCOA.Session.CommitTransaction();
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    #endregion CreateReverseGoodsReceiptJournalByBusinessPartner

                                    #endregion CreateReverseGoodsReceiptJournal
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CreditMemo ", ex.ToString());
            }
        }

        private void SetInvoiceAPReturnJournal(Session _currSession, CreditMemo _locCreditMemoXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locTotalUnitPrice = 0;

                #region CreateInvoiceAPReturnJournal

                if (_locCreditMemoXPO != null)
                {
                    XPQuery<CreditMemoLine> _creditMemoLinesQuery = new XPQuery<CreditMemoLine>(_currSession);

                    var _creditMemoLines = from cml in _creditMemoLinesQuery
                                           where (cml.CreditMemo == _locCreditMemoXPO && (cml.Status == Status.Progress || cml.Status == Status.Posted))
                                           group cml by cml.Item.ItemAccountGroup into g
                                           select new { ItemAccountGroup = g.Key };

                    if (_creditMemoLines != null && _creditMemoLines.Count() > 0)
                    {
                        foreach (var _creditMemoLine in _creditMemoLines)
                        {
                            XPCollection<CreditMemoLine> _locCreMemLines = new XPCollection<CreditMemoLine>(_currSession,
                                                                           new GroupOperator(GroupOperatorType.And,
                                                                           new BinaryOperator("CreditMemo", _locCreditMemoXPO),
                                                                           new BinaryOperator("Item.ItemAccountGroup", _creditMemoLine.ItemAccountGroup)));
                            if (_locCreMemLines != null && _locCreMemLines.Count() > 0)
                            {
                                #region MenghitungTotalUnitPriceBasedOnItemAccountGroup
                                foreach (CreditMemoLine _locCreMemLine in _locCreMemLines)
                                {
                                    if (_locCreMemLine.Status == Status.Progress || _locCreMemLine.Status == Status.Posted)
                                    {
                                        _locTotalUnitPrice = _locTotalUnitPrice + _locCreMemLine.TUAmount;
                                    }
                                }
                                #endregion MenghitungTotalUnitPriceBasedOnItemAccountGroup

                                #region CreateInvoiceAPReturnJournalByItems

                                XPCollection<JournalMap> _locJournalMapByItems = new XPCollection<JournalMap>
                                                                                 (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                  new BinaryOperator("ItemAccountGroup", _creditMemoLine.ItemAccountGroup)));

                                if (_locJournalMapByItems != null && _locJournalMapByItems.Count() > 0)
                                {
                                    foreach (JournalMap _locJournalMapByItem in _locJournalMapByItems)
                                    {
                                        XPCollection<JournalMapLine> _locJournalMapLineByItems = new XPCollection<JournalMapLine>
                                                                                                 (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                  new BinaryOperator("JournalMap", _locJournalMapByItem)));

                                        if (_locJournalMapLineByItems != null && _locJournalMapLineByItems.Count() > 0)
                                        {
                                            foreach (JournalMapLine _locJournalMapLineByItem in _locJournalMapLineByItems)
                                            {
                                                AccountMap _locAccountMapByItem = _currSession.FindObject<AccountMap>
                                                                                  (new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("Code", _locJournalMapLineByItem.AccountMap.Code),
                                                                                   new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                   new BinaryOperator("OrderType", OrderType.Item),
                                                                                   new BinaryOperator("PostingMethod", PostingMethod.CreditMemo),
                                                                                   new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                if (_locAccountMapByItem != null)
                                                {
                                                    XPCollection<AccountMapLine> _locAccountMapLineByItems = new XPCollection<AccountMapLine>
                                                                                    (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                    new BinaryOperator("AccountMap", _locAccountMapByItem),
                                                                                    new BinaryOperator("Active", true)));

                                                    if (_locAccountMapLineByItems != null && _locAccountMapLineByItems.Count() > 0)
                                                    {
                                                        double _locTotalAmountDebit = 0;
                                                        double _locTotalAmountCredit = 0;
                                                        double _locTotalBalance = 0;

                                                        foreach (AccountMapLine _locAccountMapLineByItem in _locAccountMapLineByItems)
                                                        {
                                                            if (_locAccountMapLineByItem.AccountCharge == AccountCharge.Debit)
                                                            {
                                                                _locTotalAmountDebit = _locTotalUnitPrice;
                                                            }
                                                            if (_locAccountMapLineByItem.AccountCharge == AccountCharge.Credit)
                                                            {
                                                                _locTotalAmountCredit = _locTotalUnitPrice;
                                                            }

                                                            GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                            {
                                                                PostingDate = now,
                                                                PostingType = PostingType.Purchase,
                                                                PostingMethod = PostingMethod.InvoiceAP,
                                                                Account = _locAccountMapLineByItem.Account,
                                                                Debit = _locTotalAmountDebit,
                                                                Credit = _locTotalAmountCredit,
                                                                PurchaseOrder = _locCreditMemoXPO.PurchaseOrder,
                                                                CreditMemo = _locCreditMemoXPO,
                                                            };
                                                            _saveGeneralJournal.Save();
                                                            _saveGeneralJournal.Session.CommitTransaction();

                                                            if (_locAccountMapLineByItem.Account.Code != null)
                                                            {
                                                                ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("Code", _locAccountMapLineByItem.Account.Code)));
                                                                if (_locCOA != null)
                                                                {
                                                                    if (_locCOA.BalanceType == BalanceType.Change)
                                                                    {
                                                                        if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                        {
                                                                            if (_locTotalAmountDebit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                            }
                                                                            if (_locTotalAmountCredit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                            }
                                                                        }
                                                                        if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                        {
                                                                            if (_locTotalAmountDebit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                            }
                                                                            if (_locTotalAmountCredit > 0)
                                                                            {
                                                                                _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                            }
                                                                        }
                                                                    }

                                                                    _locCOA.Balance = _locTotalBalance;
                                                                    _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                    _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                    _locCOA.Save();
                                                                    _locCOA.Session.CommitTransaction();
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                #endregion CreateInvoiceAPReturnJournalByItems

                                #region CreateInvoiceAPJournalByBusinessPartner
                                if (_locCreditMemoXPO.BuyFromVendor != null)
                                {
                                    if (_locCreditMemoXPO.BuyFromVendor.BusinessPartnerAccountGroup != null)
                                    {
                                        XPCollection<JournalMap> _locJournalMapByBusinessPartners = new XPCollection<JournalMap>
                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                   new BinaryOperator("BusinessPartnerAccountGroup", _locCreditMemoXPO.BuyFromVendor.BusinessPartnerAccountGroup)));

                                        if (_locJournalMapByBusinessPartners != null && _locJournalMapByBusinessPartners.Count() > 0)
                                        {
                                            foreach (JournalMap _locJournalMapByBusinessPartner in _locJournalMapByBusinessPartners)
                                            {
                                                XPCollection<JournalMapLine> _locJournalMapLineByBusinessPartners = new XPCollection<JournalMapLine>
                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("JournalMap", _locJournalMapByBusinessPartner)));

                                                if (_locJournalMapLineByBusinessPartners != null && _locJournalMapLineByBusinessPartners.Count() > 0)
                                                {
                                                    foreach (JournalMapLine _locJournalMapLineByBusinessPartner in _locJournalMapLineByBusinessPartners)
                                                    {
                                                        AccountMap _locAccountMapByBusinessPartner = _currSession.FindObject<AccountMap>
                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Code", _locJournalMapLineByBusinessPartner.AccountMap.Code),
                                                                                     new BinaryOperator("PostingType", PostingType.Purchase),
                                                                                     new BinaryOperator("OrderType", OrderType.Item),
                                                                                     new BinaryOperator("PostingMethod", PostingMethod.InvoiceAP),
                                                                                     new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                        if (_locAccountMapByBusinessPartner != null)
                                                        {
                                                            XPCollection<AccountMapLine> _locAccountMapLineByBusinessPartners = new XPCollection<AccountMapLine>
                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                            new BinaryOperator("AccountMap", _locAccountMapByBusinessPartner),
                                                                                            new BinaryOperator("Active", true)));

                                                            if (_locAccountMapLineByBusinessPartners != null && _locAccountMapLineByBusinessPartners.Count() > 0)
                                                            {
                                                                double _locTotalAmountDebit = 0;
                                                                double _locTotalAmountCredit = 0;
                                                                double _locTotalBalance = 0;

                                                                foreach (AccountMapLine _locAccountMapLineByBusinessPartner in _locAccountMapLineByBusinessPartners)
                                                                {
                                                                    if (_locAccountMapLineByBusinessPartner.AccountCharge == AccountCharge.Debit)
                                                                    {
                                                                        _locTotalAmountDebit = _locTotalUnitPrice;
                                                                    }
                                                                    if (_locAccountMapLineByBusinessPartner.AccountCharge == AccountCharge.Credit)
                                                                    {
                                                                        _locTotalAmountCredit = _locTotalUnitPrice;
                                                                    }

                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                    {
                                                                        PostingDate = now,
                                                                        PostingType = PostingType.Purchase,
                                                                        PostingMethod = PostingMethod.InvoiceAP,
                                                                        Account = _locAccountMapLineByBusinessPartner.Account,
                                                                        Debit = _locTotalAmountDebit,
                                                                        Credit = _locTotalAmountCredit,
                                                                        PurchaseOrder = _locCreditMemoXPO.PurchaseOrder,
                                                                        CreditMemo = _locCreditMemoXPO,
                                                                    };
                                                                    _saveGeneralJournal.Save();
                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                    if (_locAccountMapLineByBusinessPartner.Account.Code != null)
                                                                    {
                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Code", _locAccountMapLineByBusinessPartner.Account.Code)));
                                                                        if (_locCOA != null)
                                                                        {
                                                                            if (_locCOA.BalanceType == BalanceType.Change)
                                                                            {
                                                                                if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                {
                                                                                    if (_locTotalAmountDebit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                    }
                                                                                    if (_locTotalAmountCredit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                    }
                                                                                }
                                                                                if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                {
                                                                                    if (_locTotalAmountDebit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                    }
                                                                                    if (_locTotalAmountCredit > 0)
                                                                                    {
                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                    }
                                                                                }
                                                                            }

                                                                            _locCOA.Balance = _locTotalBalance;
                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                            _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                            _locCOA.Save();
                                                                            _locCOA.Session.CommitTransaction();
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                #endregion CreateInvoiceAPJournalByBusinessPartner
                            }
                        }
                    }
                }

                #endregion CreateInvoiceAPReturnJournal
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = CreditMemo ", ex.ToString());
            }
        }

        private void SetCreditMemoCollectionInSICAndSI(Session _currSession, CreditMemo _locCreditMemoXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _locSignCode = null;
                double _locTUAmount = 0;
                double _locCM_Amount = 0;

                if (_locCreditMemoXPO != null)
                {
                    if (_locCreditMemoXPO.PurchaseOrder != null)
                    {
                        PurchaseInvoice _locPurchaseInvoice = _currSession.FindObject<PurchaseInvoice>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                     new BinaryOperator("PurchaseOrder", _locCreditMemoXPO.PurchaseOrder)));

                        PurchaseInvoiceCollection _locPurchaseInvoiceCollection = _currSession.FindObject<PurchaseInvoiceCollection>
                                                (new GroupOperator(GroupOperatorType.And,
                                                 new BinaryOperator("PurchaseOrder", _locCreditMemoXPO.PurchaseOrder)));

                        if (_locPurchaseInvoice != null && _locPurchaseInvoiceCollection != null)
                        {
                            XPCollection<CreditMemoLine> _locCreditMemoLines = new XPCollection<CreditMemoLine>(_currSession,
                                                                             new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("PurchaseReturn", _locCreditMemoXPO.PurchaseReturn),
                                                                             new BinaryOperator("CreditMemo", _locCreditMemoXPO)));

                            if (_locCreditMemoLines != null && _locCreditMemoLines.Count() > 0)
                            {
                                _locSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.CreditMemoCollection);

                                if (_locSignCode != null)
                                {
                                    foreach (CreditMemoLine _locCreditMemoLine in _locCreditMemoLines)
                                    {
                                        _locTUAmount = _locTUAmount + _locCreditMemoLine.TUAmount;
                                    }

                                    CreditMemoCollection _locSaveDataCMC = new CreditMemoCollection(_currSession)
                                    {
                                        BuyFromVendor = _locCreditMemoXPO.BuyFromVendor,
                                        BuyFromAddress = _locCreditMemoXPO.BuyFromAddress,
                                        BuyFromCity = _locCreditMemoXPO.BuyFromCity,
                                        BuyFromContact = _locCreditMemoXPO.BuyFromContact,
                                        BuyFromCountry = _locCreditMemoXPO.BuyFromCountry,
                                        CM_Amount = _locTUAmount,
                                        TOP = _locCreditMemoXPO.PurchaseOrder.TOP,
                                        SignCode = _locSignCode,
                                        PurchaseReturn = _locCreditMemoXPO.PurchaseReturn,
                                        PurchaseOrder = _locCreditMemoXPO.PurchaseOrder,
                                        PurchaseInvoice = _locPurchaseInvoice,
                                        PurchaseInvoiceCollection = _locPurchaseInvoiceCollection,
                                        CreditMemo = _locCreditMemoXPO,
                                    };

                                    _locSaveDataCMC.Save();
                                    _locSaveDataCMC.Session.CommitTransaction();

                                    //Menghitung Total Jumlah DebitMemoCollection based on SalesInvoiceCollection
                                    XPCollection<CreditMemoCollection> _locCreditMemoCollections = new XPCollection<CreditMemoCollection>
                                                                                                (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                 new BinaryOperator("PurchaseInvoice", _locPurchaseInvoice),
                                                                                                 new BinaryOperator("PurchaseInvoiceCollection", _locPurchaseInvoiceCollection)));
                                    if (_locCreditMemoCollections != null && _locCreditMemoCollections.Count() > 0)
                                    {
                                        foreach (CreditMemoCollection _locCreditMemoCollection in _locCreditMemoCollections)
                                        {
                                            _locCM_Amount = _locCM_Amount + _locCreditMemoCollection.CM_Amount;
                                        }

                                        _locPurchaseInvoiceCollection.CM_Amount = _locCM_Amount;
                                        _locPurchaseInvoiceCollection.Save();
                                        _locPurchaseInvoiceCollection.Session.CommitTransaction();

                                        _locPurchaseInvoice.CM_Amount = _locCM_Amount;
                                        _locPurchaseInvoice.Save();
                                        _locPurchaseInvoice.Session.CommitTransaction();
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data DebitMemoLine Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data DebitMemoLine Not Available");
                            }
                        }  
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemo" + ex.ToString());
            }
        }

        private void SetStatusPostingCreditMemoLine(Session _currSession, CreditMemo _creditMemoXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_creditMemoXPO != null)
                {
                    XPCollection<CreditMemoLine> _locCreditMemoLines = new XPCollection<CreditMemoLine>(_currSession,
                                                                       new GroupOperator(GroupOperatorType.And,
                                                                       new BinaryOperator("CreditMemo", _creditMemoXPO)));

                    if (_locCreditMemoLines != null && _locCreditMemoLines.Count > 0)
                    {

                        foreach (CreditMemoLine _locCreditMemoLine in _locCreditMemoLines)
                        {
                            if (_locCreditMemoLine.Status == Status.Progress)
                            {
                                _locCreditMemoLine.Status = Status.Posted;
                                _locCreditMemoLine.StatusDate = now;
                                _locCreditMemoLine.Save();
                                _locCreditMemoLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemo " + ex.ToString());
            }
        }

        private void SetFinalStatusReturnCreditMemo(Session _currSession, CreditMemo _locCreditMemoXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locCount = 0;
                if (_locCreditMemoXPO != null)
                {
                    XPCollection<CreditMemoLine> _locCreditMemoLines = new XPCollection<CreditMemoLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("CreditMemo", _locCreditMemoXPO)));

                    if (_locCreditMemoLines != null && _locCreditMemoLines.Count() > 0)
                    {

                        foreach (CreditMemoLine _locCreditMemoLine in _locCreditMemoLines)
                        {
                            if (_locCreditMemoLine.Status == Status.Close)
                            {
                                _locCount = _locCount + 1;
                            }
                        }

                        if (_locCount == _locCreditMemoLines.Count())
                        {
                            _locCreditMemoXPO.ActivationPosting = true;
                            _locCreditMemoXPO.Status = Status.Close;
                            _locCreditMemoXPO.StatusDate = now;
                            _locCreditMemoXPO.Save();
                            _locCreditMemoXPO.Session.CommitTransaction();
                        }
                        else
                        {
                            _locCreditMemoXPO.Status = Status.Posted;
                            _locCreditMemoXPO.StatusDate = now;
                            _locCreditMemoXPO.Save();
                            _locCreditMemoXPO.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemo " + ex.ToString());
            }
        }

        #endregion Posting

        #region PostingOld

        private void SetCreditMemoCollectionsInPurchaseInvoice(Session _currSession, CreditMemo _locCreditMemoXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _locSignCode = null;

                if (_locCreditMemoXPO != null)
                {
                    PurchaseInvoice _locPurchaseInvoice = _currSession.FindObject<PurchaseInvoice>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseOrder", _locCreditMemoXPO.PurchaseOrder)));

                    if (_locPurchaseInvoice != null)
                    {
                        XPCollection<CreditMemoLine> _locCreditMemoLines = new XPCollection<CreditMemoLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("PurchaseReturn", _locCreditMemoXPO.PurchaseReturn),
                                                                        new BinaryOperator("CreditMemoLine", _locCreditMemoXPO)));

                        if (_locCreditMemoLines != null)
                        {
                            _locSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.CreditMemoCollection);

                            if (_locSignCode != null)
                            {
                                CreditMemoCollection _locSaveDataCMC = new CreditMemoCollection(_currSession)
                                {
                                    BuyFromVendor = _locCreditMemoXPO.BuyFromVendor,
                                    BuyFromAddress = _locCreditMemoXPO.BuyFromAddress,
                                    BuyFromCity = _locCreditMemoXPO.BuyFromCity,
                                    BuyFromContact = _locCreditMemoXPO.BuyFromContact,
                                    BuyFromCountry = _locCreditMemoXPO.BuyFromCountry,
                                    CM_Amount = _locCreditMemoXPO.GrandTotalCm,
                                    TOP = _locCreditMemoXPO.PurchaseOrder.TOP,
                                    SignCode = _locSignCode,
                                    PurchaseReturn = _locCreditMemoXPO.PurchaseReturn,
                                    PurchaseOrder = _locCreditMemoXPO.PurchaseOrder,
                                    PurchaseInvoice = _locPurchaseInvoice,
                                    CreditMemo = _locCreditMemoXPO,
                                };

                                _locSaveDataCMC.Save();
                                _locSaveDataCMC.Session.CommitTransaction();
                            }
                            else
                            {
                                ErrorMessageShow("Data LocSign Not Available");
                            }
                        }
                        else
                        {
                            ErrorMessageShow("Data Credit Memo Line Not Available");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemo" + ex.ToString());
            }
        }

        private void SetCreditMemoCollectionInSICAndSIOld(Session _currSession, CreditMemo _locCreditMemoXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _locSignCode = null;
                double _locTUAmount = 0;
                double _locCM_Amount = 0;

                if (_locCreditMemoXPO != null)
                {
                    if (_locCreditMemoXPO.PurchaseOrder != null)
                    {
                        //#region SplitInvoice
                        //if (_locCreditMemoXPO.PurchaseOrder.SplitInvoice == false)
                        //{
                        //    PurchaseInvoice _locPurchaseInvoice = _currSession.FindObject<PurchaseInvoice>
                        //                            (new GroupOperator(GroupOperatorType.And,
                        //                             new BinaryOperator("PurchaseOrder", _locCreditMemoXPO.PurchaseOrder)));

                        //    PurchaseInvoiceCollection _locPurchaseInvoiceCollection = _currSession.FindObject<PurchaseInvoiceCollection>
                        //                            (new GroupOperator(GroupOperatorType.And,
                        //                             new BinaryOperator("PurchaseOrder", _locCreditMemoXPO.PurchaseOrder)));

                        //    if (_locPurchaseInvoice != null && _locPurchaseInvoiceCollection != null)
                        //    {
                        //        XPCollection<CreditMemoLine> _locCreditMemoLines = new XPCollection<CreditMemoLine>(_currSession,
                        //                                                         new GroupOperator(GroupOperatorType.And,
                        //                                                         new BinaryOperator("PurchaseReturn", _locCreditMemoXPO.PurchaseReturn),
                        //                                                         new BinaryOperator("CreditMemo", _locCreditMemoXPO)));

                        //        if (_locCreditMemoLines != null && _locCreditMemoLines.Count() > 0)
                        //        {
                        //            _locSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.CreditMemoCollection);

                        //            if (_locSignCode != null)
                        //            {
                        //                foreach (CreditMemoLine _locCreditMemoLine in _locCreditMemoLines)
                        //                {
                        //                    _locTUAmount = _locTUAmount + _locCreditMemoLine.TUAmount;
                        //                }

                        //                CreditMemoCollection _locSaveDataCMC = new CreditMemoCollection(_currSession)
                        //                {
                        //                    BuyFromVendor = _locCreditMemoXPO.BuyFromVendor,
                        //                    BuyFromAddress = _locCreditMemoXPO.BuyFromAddress,
                        //                    BuyFromCity = _locCreditMemoXPO.BuyFromCity,
                        //                    BuyFromContact = _locCreditMemoXPO.BuyFromContact,
                        //                    BuyFromCountry = _locCreditMemoXPO.BuyFromCountry,
                        //                    CM_Amount = _locTUAmount,
                        //                    TOP = _locCreditMemoXPO.PurchaseOrder.TOP,
                        //                    SignCode = _locSignCode,
                        //                    PurchaseReturn = _locCreditMemoXPO.PurchaseReturn,
                        //                    PurchaseOrder = _locCreditMemoXPO.PurchaseOrder,
                        //                    PurchaseInvoice = _locPurchaseInvoice,
                        //                    PurchaseInvoiceCollection = _locPurchaseInvoiceCollection,
                        //                    CreditMemo = _locCreditMemoXPO,
                        //                };

                        //                _locSaveDataCMC.Save();
                        //                _locSaveDataCMC.Session.CommitTransaction();

                        //                //Menghitung Total Jumlah DebitMemoCollection based on SalesInvoiceCollection
                        //                XPCollection<CreditMemoCollection> _locCreditMemoCollections = new XPCollection<CreditMemoCollection>
                        //                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                        //                                                                             new BinaryOperator("PurchaseInvoice", _locPurchaseInvoice),
                        //                                                                             new BinaryOperator("PurchaseInvoiceCollection", _locPurchaseInvoiceCollection)));
                        //                if (_locCreditMemoCollections != null && _locCreditMemoCollections.Count() > 0)
                        //                {
                        //                    foreach (CreditMemoCollection _locCreditMemoCollection in _locCreditMemoCollections)
                        //                    {
                        //                        _locCM_Amount = _locCM_Amount + _locCreditMemoCollection.CM_Amount;
                        //                    }

                        //                    _locPurchaseInvoiceCollection.CM_Amount = _locCM_Amount;
                        //                    _locPurchaseInvoiceCollection.Save();
                        //                    _locPurchaseInvoiceCollection.Session.CommitTransaction();

                        //                    _locPurchaseInvoice.CM_Amount = _locCM_Amount;
                        //                    _locPurchaseInvoice.Save();
                        //                    _locPurchaseInvoice.Session.CommitTransaction();
                        //                }
                        //            }
                        //            else
                        //            {
                        //                ErrorMessageShow("Data DebitMemoLine Not Available");
                        //            }
                        //        }
                        //        else
                        //        {
                        //            ErrorMessageShow("Data DebitMemoLine Not Available");
                        //        }
                        //    }
                        //}
                        //#endregion SplitInvoice
                        //#region NonSplitInvoice
                        //else
                        //{
                        //    PurchaseInvoiceCollection _locPurchaseInvoiceCollection = _currSession.FindObject<PurchaseInvoiceCollection>
                        //                            (new GroupOperator(GroupOperatorType.And,
                        //                             new BinaryOperator("PurchaseOrder", _locCreditMemoXPO.PurchaseOrder)));

                        //    if (_locPurchaseInvoiceCollection != null)
                        //    {
                        //        XPCollection<CreditMemoLine> _locCreditMemoLines = new XPCollection<CreditMemoLine>(_currSession,
                        //                                                         new GroupOperator(GroupOperatorType.And,
                        //                                                         new BinaryOperator("PurchaseReturn", _locCreditMemoXPO.PurchaseReturn),
                        //                                                         new BinaryOperator("CreditMemo", _locCreditMemoXPO)));

                        //        if (_locCreditMemoLines != null && _locCreditMemoLines.Count() > 0)
                        //        {
                        //            _locSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.DebitMemoCollection);

                        //            if (_locSignCode != null)
                        //            {
                        //                //Menghitung Total di DebitMemoLine
                        //                foreach (CreditMemoLine _locCreditMemoLine in _locCreditMemoLines)
                        //                {
                        //                    _locTUAmount = _locTUAmount + _locCreditMemoLine.TUAmount;
                        //                }

                        //                CreditMemoCollection _locSaveDataDMC = new CreditMemoCollection(_currSession)
                        //                {
                        //                    BuyFromVendor = _locCreditMemoXPO.BuyFromVendor,
                        //                    BuyFromAddress = _locCreditMemoXPO.BuyFromAddress,
                        //                    BuyFromCity = _locCreditMemoXPO.BuyFromCity,
                        //                    BuyFromContact = _locCreditMemoXPO.BuyFromContact,
                        //                    BuyFromCountry = _locCreditMemoXPO.BuyFromCountry,
                        //                    CM_Amount = _locTUAmount,
                        //                    TOP = _locCreditMemoXPO.PurchaseOrder.TOP,
                        //                    SignCode = _locSignCode,
                        //                    PurchaseReturn = _locCreditMemoXPO.PurchaseReturn,
                        //                    PurchaseOrder = _locCreditMemoXPO.PurchaseOrder,
                        //                    PurchaseInvoiceCollection = _locPurchaseInvoiceCollection,
                        //                    CreditMemo = _locCreditMemoXPO,
                        //                };

                        //                _locSaveDataDMC.Save();
                        //                _locSaveDataDMC.Session.CommitTransaction();

                        //                //Menghitung Total Jumlah DebitMemoCollection based on SalesInvoiceCollection
                        //                XPCollection<CreditMemoCollection> _locCreditMemoCollections = new XPCollection<CreditMemoCollection>
                        //                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                        //                                                                             new BinaryOperator("PurchaseInvoiceCollection", _locPurchaseInvoiceCollection)));
                        //                if (_locCreditMemoCollections != null && _locCreditMemoCollections.Count() > 0)
                        //                {
                        //                    foreach (CreditMemoCollection _locCreditMemoLine in _locCreditMemoCollections)
                        //                    {
                        //                        _locCM_Amount = _locCM_Amount + _locCreditMemoLine.CM_Amount;
                        //                    }

                        //                    _locPurchaseInvoiceCollection.CM_Amount = _locCM_Amount;
                        //                    _locPurchaseInvoiceCollection.Save();
                        //                    _locPurchaseInvoiceCollection.Session.CommitTransaction();
                        //                }
                        //            }
                        //            else
                        //            {
                        //                ErrorMessageShow("Data DebitMemoLine Not Available");
                        //            }
                        //        }
                        //        else
                        //        {
                        //            ErrorMessageShow("Data DebitMemoLine Not Available");
                        //        }
                        //    }
                        //}
                        //#endregion NonSplitInvoice
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = CreditMemo" + ex.ToString());
            }
        }

        #endregion PostingOld

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        #endregion Global Method

    }
}
