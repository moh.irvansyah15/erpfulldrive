﻿#region Default

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;

#endregion Default

using FullDrive.Module.BusinessObjects;
using FullDrive.Module.CustomProcess;
using DevExpress.ExpressApp.Xpo;
using System.Collections;
using DevExpress.Xpo;

namespace FullDrive.Module.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class InventoryTransferOutActionController : ViewController
    {
        #region Default

        private ChoiceActionItem _selectionListviewFilter;
        public InventoryTransferOutActionController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            InventoryTransferOutListviewFilterSelectionAction.Items.Clear();
            foreach (object _currApproval in Enum.GetValues(typeof(CustomProcess.Status)))
            {

                EnumDescriptor _ed = new EnumDescriptor(typeof(CustomProcess.Status));
                _selectionListviewFilter = new ChoiceActionItem(_ed.GetCaption(_currApproval), _currApproval);
                InventoryTransferOutListviewFilterSelectionAction.Items.Add(_selectionListviewFilter);
            }
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        #endregion Default

        private void InventoryTransferOutProgressAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;
                Status _locStatus = Status.None;
                DateTime _locNow;
                Status _locStatus2 = Status.None;
                DateTime _locNow2;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        InventoryTransferOut _locInventoryTransferOutOS = (InventoryTransferOut)_objectSpace.GetObject(obj);

                        if (_locInventoryTransferOutOS != null)
                        {
                            if (_locInventoryTransferOutOS.Code != null)
                            {
                                _currObjectId = _locInventoryTransferOutOS.Code;

                                InventoryTransferOut _locInventoryTransferOutXPO = _currSession.FindObject<InventoryTransferOut>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locInventoryTransferOutXPO != null)
                                {
                                    if (_locInventoryTransferOutXPO.Status == Status.Open || _locInventoryTransferOutXPO.Status == Status.Progress || _locInventoryTransferOutXPO.Status == Status.Posted)
                                    {
                                        if(_locInventoryTransferOutXPO.Status == Status.Open)
                                        {
                                            _locStatus = Status.Progress;
                                            _locNow = now;
                                        }
                                        else
                                        {
                                            _locStatus = _locInventoryTransferOutXPO.Status;
                                            _locNow = _locInventoryTransferOutXPO.StatusDate;
                                        }
                                        _locInventoryTransferOutXPO.Status = _locStatus;
                                        _locInventoryTransferOutXPO.StatusDate = _locNow;
                                        _locInventoryTransferOutXPO.Save();
                                        _locInventoryTransferOutXPO.Session.CommitTransaction();

                                        #region InventoryTransferOutLine
                                        XPCollection<InventoryTransferOutLine> _locInventoryTransferOutLines = new XPCollection<InventoryTransferOutLine>
                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("InventoryTransferOut", _locInventoryTransferOutXPO)));

                                        if (_locInventoryTransferOutLines != null && _locInventoryTransferOutLines.Count > 0)
                                        {
                                            foreach (InventoryTransferOutLine _locInventoryTransferOutLine in _locInventoryTransferOutLines)
                                            {
                                                if (_locInventoryTransferOutLine.Status == Status.Open || _locInventoryTransferOutLine.Status == Status.Progress || _locInventoryTransferOutLine.Status == Status.Posted)
                                                {
                                                    if (_locInventoryTransferOutLine.Status == Status.Open)
                                                    {
                                                        _locStatus2 = Status.Progress;
                                                        _locNow2 = now;
                                                    }
                                                    else
                                                    {
                                                        _locStatus2 = _locInventoryTransferOutLine.Status;
                                                        _locNow2 = _locInventoryTransferOutLine.StatusDate;
                                                    }

                                                    _locInventoryTransferOutLine.Status = _locStatus2;
                                                    _locInventoryTransferOutLine.StatusDate = _locNow2;
                                                    _locInventoryTransferOutLine.Save();
                                                    _locInventoryTransferOutLine.Session.CommitTransaction();
                                                }

                                                XPCollection<InventoryTransferOutLot> _locInventoryTransferOutLots = new XPCollection<InventoryTransferOutLot>
                                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                        new BinaryOperator("InventoryTransferOutLine", _locInventoryTransferOutLine),
                                                                                                        new BinaryOperator("Status", Status.Open)));

                                                if (_locInventoryTransferOutLots != null && _locInventoryTransferOutLots.Count() > 0)
                                                {
                                                    foreach (InventoryTransferOutLot _locInventoryTransferOutLot in _locInventoryTransferOutLots)
                                                    {
                                                        _locInventoryTransferOutLot.Status = Status.Progress;
                                                        _locInventoryTransferOutLot.StatusDate = now;
                                                        _locInventoryTransferOutLot.Save();
                                                        _locInventoryTransferOutLot.Session.CommitTransaction();
                                                    }
                                                }
                                            }
                                        }
                                        #endregion InventoryTransferOutLine
                                        #region InventorySalesCollection
                                        XPCollection<InventorySalesCollection> _locInventorySalesCollections = new XPCollection<InventorySalesCollection>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Status", Status.Open)));
                                        if(_locInventorySalesCollections != null && _locInventorySalesCollections.Count() > 0)
                                        {
                                            foreach(InventorySalesCollection _locInventorySalesCollection in _locInventorySalesCollections)
                                            {
                                                _locInventorySalesCollection.Status = Status.Progress;
                                                _locInventorySalesCollection.StatusDate = now;
                                                _locInventorySalesCollection.Save();
                                                _locInventorySalesCollection.Session.CommitTransaction();
                                            }
                                        }
                                        #endregion InventorySalesCollection
                                        SuccessMessageShow(_locInventoryTransferOutXPO.Code + " has been change successfully to Progress");
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Inventory Transfer Not Available");
                                }

                            }
                            else
                            {
                                ErrorMessageShow("Data Inventory Transfer Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOut " + ex.ToString());
            }
        }

        private void InventoryTransferOutPostingAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;

                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        InventoryTransferOut _locInventoryTransferOutOS = (InventoryTransferOut)_objectSpace.GetObject(obj);

                        if (_locInventoryTransferOutOS != null)
                        {
                            if (_locInventoryTransferOutOS.Status == Status.Progress || _locInventoryTransferOutOS.Status == Status.Posted)
                            {
                                if (_locInventoryTransferOutOS.Code != null)
                                {
                                    _currObjectId = _locInventoryTransferOutOS.Code;

                                    InventoryTransferOut _locInventoryTransferOutXPO = _currSession.FindObject<InventoryTransferOut>
                                                                        (new GroupOperator(GroupOperatorType.And,
                                                                         new BinaryOperator("Code", _currObjectId),
                                                                         new BinaryOperator("InventoryMovingType", InventoryMovingType.Deliver)));

                                    if (_locInventoryTransferOutXPO != null)
                                    {
                                        if (CheckAvailableStock(_currSession, _locInventoryTransferOutXPO) == true)
                                        {
                                            #region Normal
                                            SetInventoryTransferOutMonitoring(_currSession, _locInventoryTransferOutXPO);
                                            SetDeliverBeginingInventory(_currSession, _locInventoryTransferOutXPO);
                                            SetDeliverInventoryJournal(_currSession, _locInventoryTransferOutXPO);
                                            SetRemainDeliverQty(_currSession, _locInventoryTransferOutXPO);
                                            SetPostingDeliverQty(_currSession, _locInventoryTransferOutXPO);
                                            SetProcessDeliverCount(_currSession, _locInventoryTransferOutXPO);

                                            if (_locInventoryTransferOutXPO.TransferType == DirectionType.External)
                                            {
                                                SetGoodsDeliverJournal(_currSession, _locInventoryTransferOutXPO);
                                            }

                                            SetStatusDeliverInventoryTransferOutLine(_currSession, _locInventoryTransferOutXPO);
                                            SetNormalDeliverQuantity(_currSession, _locInventoryTransferOutXPO);
                                            SetFinalStatusDeliverInventoryTransferOut(_currSession, _locInventoryTransferOutXPO);
                                            
                                            #endregion Normal
                                            SuccessMessageShow(_locInventoryTransferOutXPO.Code + " has been change successfully to Deliver");
                                        }
                                        else
                                        {
                                            ErrorMessageShow("Please check stock quantity");
                                        }

                                    }
                                    else
                                    {
                                        ErrorMessageShow("InventoryTransferOut Data Not Available");
                                    }
                                }
                            }
                            else
                            {
                                ErrorMessageShow("InventoryTransferOut Data Not Available");
                            }
                        }
                    }
                }

                //auto refresh
                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOut " + ex.ToString());
            }
        }

        private void InventoryTransferOutGenerateLotAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            //try
            //{
            //    GlobalFunction _globFunc = new GlobalFunction();
            //    IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
            //    ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
            //    DateTime now = DateTime.Now;
            //    Session _currSession = null;
            //    string _currObjectId = null;

            //    if (this.ObjectSpace != null)
            //    {
            //        _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
            //    }

            //    if (_objectsToProcess != null)
            //    {
            //        foreach (Object obj in _objectsToProcess)
            //        {
            //            InventoryTransferOut _locInventoryTransferOutOS = (InventoryTransferOut)_objectSpace.GetObject(obj);

            //            if (_locInventoryTransferOutOS != null)
            //            {
            //                if (_locInventoryTransferOutOS.Code != null)
            //                {
            //                    _currObjectId = _locInventoryTransferOutOS.Code;

            //                    InventoryTransferOut _locInventoryTransferOutXPO = _currSession.FindObject<InventoryTransferOut>
            //                                                                       (new GroupOperator(GroupOperatorType.And,
            //                                                                        new BinaryOperator("Code", _currObjectId)));

            //                    if (_locInventoryTransferOutXPO != null)
            //                    {
            //                        XPCollection<InventoryTransferOutLine> _locInventoryTransferOutLines = new XPCollection<InventoryTransferOutLine>
            //                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
            //                                                                                                new BinaryOperator("InventoryTransferOut", _locInventoryTransferOutXPO)));

            //                        if (_locInventoryTransferOutLines != null && _locInventoryTransferOutLines.Count > 0)
            //                        {
            //                            foreach (InventoryTransferOutLine _locInventoryTransferOutLine in _locInventoryTransferOutLines)
            //                            {
            //                                if (_locInventoryTransferOutLine.Count > 0 && _locInventoryTransferOutLine.QtyPackage > 0)
            //                                {
            //                                    if (_locInventoryTransferOutLine.Status == Status.Open || _locInventoryTransferOutLine.Status == Status.Progress)
            //                                    {
            //                                        SetInventoryTransferOutLotLooping(_currSession, _locInventoryTransferOutXPO);
            //                                        SetInventoryTransferOutLinePostedLotCount(_currSession, _locInventoryTransferOutXPO);
            //                                        SuccessMessageShow(_locInventoryTransferOutXPO.Code + " has been change successfully to Posted");
            //                                    }
            //                                    else
            //                                    {
            //                                        ErrorMessageShow("Data InventoryTransferOutLine Status Not Available");
            //                                    }
            //                                }
            //                                else
            //                                {
            //                                    ErrorMessageShow("Data InventoryTransferOutLine Count & QtyPackage Not Available");
            //                                }
            //                            }
            //                        }
            //                        else
            //                        {
            //                            ErrorMessageShow("Data Status InventoryTransferOutLine Not Available");
            //                        }
            //                    }
            //                    else
            //                    {
            //                        ErrorMessageShow("Data InventoryTransferOut Not Available");
            //                    }
            //                }
            //                else
            //                {
            //                    ErrorMessageShow("Data InventoryTransferOut Not Available");
            //                }
            //            }
            //            else
            //            {
            //                ErrorMessageShow("Data InventoryTransferOut Not Available");
            //            }
            //        }
            //    }

            //    if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
            //    {
            //        _objectSpace.CommitChanges();
            //        _objectSpace.Refresh();
            //    }
            //    if (View is ListView)
            //    {
            //        _objectSpace.CommitChanges();
            //        View.ObjectSpace.Refresh();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOut " + ex.ToString());
            //}
        }

        private void InventoryTransferOutListviewFilterSelectionAction_Execute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            try
            {
                if ((View is ListView) & (View.ObjectTypeInfo.Type == typeof(InventoryTransferOut)))
                {
                    if ((Status)e.SelectedChoiceActionItem.Data == Status.None)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.None, BinaryOperatorType.NotEqual);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Open)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Open, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Progress)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Progress, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Posted)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Posted, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Lock)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Lock, BinaryOperatorType.Equal);
                    }
                    else if ((Status)e.SelectedChoiceActionItem.Data == Status.Close)
                    {
                        ((ListView)View).CollectionSource.Criteria["Filter1"] = new BinaryOperator("Status", Status.Close, BinaryOperatorType.Equal);
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOut " + ex.ToString());
            }
        }

        private void InventoryTransferOutGetSOAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        InventoryTransferOut _locInventoryTransferOutOS = (InventoryTransferOut)_objectSpace.GetObject(obj);

                        if (_locInventoryTransferOutOS != null)
                        {
                            if (_locInventoryTransferOutOS.Code != null)
                            {
                                _currObjectId = _locInventoryTransferOutOS.Code;

                                InventoryTransferOut _locInventoryTransferOutXPO = _currSession.FindObject<InventoryTransferOut>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locInventoryTransferOutXPO != null)
                                {
                                    if (_locInventoryTransferOutXPO.Status == Status.Open || _locInventoryTransferOutXPO.Status == Status.Progress)
                                    {
                                        XPCollection<InventorySalesCollection> _locInventorySalesCollections = new XPCollection<InventorySalesCollection>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("InventoryTransferOut", _locInventoryTransferOutXPO),
                                                                                        new GroupOperator(GroupOperatorType.Or,
                                                                                        new BinaryOperator("Status", Status.Open),
                                                                                        new BinaryOperator("Status", Status.Progress))));

                                        if (_locInventorySalesCollections != null && _locInventorySalesCollections.Count() > 0)
                                        {
                                            foreach (InventorySalesCollection _locInventorySalesCollection in _locInventorySalesCollections)
                                            {
                                                if (_locInventorySalesCollection.SalesOrder != null)
                                                {
                                                    ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                new BinaryOperator("EndApproval", true),
                                                                                new BinaryOperator("SalesOrder", _locInventorySalesCollection.SalesOrder)));

                                                    if (_locApprovalLineXPO != null)
                                                    {
                                                        GetSalesOrderMonitoring(_currSession, _locInventorySalesCollection.SalesOrder, _locInventoryTransferOutXPO);
                                                    }
                                                }
                                            }
                                            SuccessMessageShow("SalesOrder Has Been Successfully Getting into InventoryTransferOut");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data SalesOrder Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data SalesOrder Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOut " + ex.ToString());
            }
        }

        private void InventoryTransferOutGetITOAction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                IObjectSpace _objectSpace = View is ListView ? Application.CreateObjectSpace() : View.ObjectSpace;
                ArrayList _objectsToProcess = new ArrayList(e.SelectedObjects);
                DateTime now = DateTime.Now;
                Session _currSession = null;
                string _currObjectId = null;


                if (this.ObjectSpace != null)
                {
                    _currSession = ((XPObjectSpace)this.ObjectSpace).Session;
                }

                if (_objectsToProcess != null)
                {
                    foreach (Object obj in _objectsToProcess)
                    {
                        InventoryTransferOut _locInventoryTransferOutOS = (InventoryTransferOut)_objectSpace.GetObject(obj);

                        if (_locInventoryTransferOutOS != null)
                        {
                            if (_locInventoryTransferOutOS.Code != null)
                            {
                                _currObjectId = _locInventoryTransferOutOS.Code;

                                InventoryTransferOut _locInventoryTransferOutXPO = _currSession.FindObject<InventoryTransferOut>
                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                     new BinaryOperator("Code", _currObjectId)));

                                if (_locInventoryTransferOutXPO != null)
                                {
                                    if (_locInventoryTransferOutXPO.Status == Status.Open || _locInventoryTransferOutXPO.Status == Status.Progress)
                                    {
                                        XPCollection<InventorySalesCollection> _locInventorySalesCollections = new XPCollection<InventorySalesCollection>
                                                              (_currSession, new GroupOperator(GroupOperatorType.And,
                                                               new BinaryOperator("InventoryTransferOut", _locInventoryTransferOutXPO)));

                                        if (_locInventorySalesCollections != null && _locInventorySalesCollections.Count() > 0)
                                        {
                                            foreach (InventorySalesCollection _locInventorySalesCollection in _locInventorySalesCollections)
                                            {
                                                if (_locInventorySalesCollection.InventoryTransferOrder != null &&
                                                    (_locInventorySalesCollection.Status == Status.Open || _locInventorySalesCollection.Status == Status.Progress))
                                                {
                                                    ApprovalLine _locApprovalLineXPO = _currSession.FindObject<ApprovalLine>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("EndApproval", true),
                                                                new BinaryOperator("InventoryTransferOrder", _locInventorySalesCollection.InventoryTransferOrder)));

                                                    if (_locApprovalLineXPO != null)
                                                    {
                                                        SetInventoryTransferOrder(_currSession, _locInventorySalesCollection.InventoryTransferOrder, _locInventoryTransferOutXPO);
                                                        SetRemainQtyITOL(_currSession, _locInventorySalesCollection.InventoryTransferOrder);
                                                        SetPostingQtyITOL(_currSession, _locInventorySalesCollection.InventoryTransferOrder);
                                                        SetProcessCountITOL(_currSession, _locInventorySalesCollection.InventoryTransferOrder);
                                                        SetStatusInventoryTransferOrderLine(_currSession, _locInventorySalesCollection.InventoryTransferOrder);
                                                        SetNormalQuantityITOL(_currSession, _locInventorySalesCollection.InventoryTransferOrder);
                                                        SetFinalStatusInventoryTransferOrderInGet(_currSession, _locInventorySalesCollection.InventoryTransferOrder);
                                                    }
                                                }
                                            }
                                            SuccessMessageShow("Inventory Transfer Order Has Been Successfully Getting into Inventory Transfer Out");
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessageShow("Data Sales Order Not Available");
                                }
                            }
                            else
                            {
                                ErrorMessageShow("Data Sales Order Not Available");
                            }
                        }
                    }
                }

                if (View is DetailView && ((DetailView)View).ViewEditMode == ViewEditMode.View)
                {
                    _objectSpace.CommitChanges();
                    _objectSpace.Refresh();
                }
                if (View is ListView)
                {
                    _objectSpace.CommitChanges();
                    View.ObjectSpace.Refresh();
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOut " + ex.ToString());
            }
        }

        //=========================================== Code Only =====================================================

        #region PostingLot

        private void SetInventoryTransferOutLotLooping(Session _currSession, InventoryTransferOut _locInventoryTransferOutXPO)
        {
            //try
            //{
            //    if (_locInventoryTransferOutXPO != null)
            //    {
            //        XPCollection<InventoryTransferOutLine> _locInventoryTransferOutLines = new XPCollection<InventoryTransferOutLine>
            //                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
            //                                                                                new BinaryOperator("InventoryTransferOut", _locInventoryTransferOutXPO)));

            //        if (_locInventoryTransferOutLines != null && _locInventoryTransferOutLines.Count > 0)
            //        {
            //            foreach (InventoryTransferOutLine _locInventoryTransferOutLine in _locInventoryTransferOutLines)
            //            {
            //                if (_locInventoryTransferOutLine.Status == Status.Open || _locInventoryTransferOutLine.Status == Status.Progress)
            //                {
            //                    if (_locInventoryTransferOutLine.ProcessLotCount < 1)
            //                    {
            //                        if (_locInventoryTransferOutLine.Count > 0 && _locInventoryTransferOutLine.QtyPackage > 0)
            //                        {
            //                            InventoryTransferOutLot _locInventoryTransferOutLots = _currSession.FindObject<InventoryTransferOutLot>
            //                                                                                   (new GroupOperator(GroupOperatorType.And,
            //                                                                                    new BinaryOperator("InventoryTransferOutLine", _locInventoryTransferOutLine)));

            //                            if (_locInventoryTransferOutLots == null)
            //                            {
            //                                for (int i = 1; i <= (int)_locInventoryTransferOutLine.Count; i++)
            //                                {
            //                                    InventoryTransferOutLot _saveData = new InventoryTransferOutLot(_currSession)
            //                                    {
            //                                        QtyLot = _locInventoryTransferOutLine.QtyPackage,
            //                                        DQty = _locInventoryTransferOutLine.DQty,
            //                                        Status = _locInventoryTransferOutLine.Status,
            //                                        DUOM = _locInventoryTransferOutLine.UnitPackage,
            //                                        InventoryTransferOutLine = _locInventoryTransferOutLine,
            //                                    };
            //                                    _saveData.Save();
            //                                    _saveData.Session.CommitTransaction();
            //                                    SuccessMessageShow("InventorytransferOutline has successfully posted");
            //                                }
            //                            }
            //                        }
            //                    }
            //                }
            //            }
            //        }
            //        else
            //        {
            //            ErrorMessageShow("Data InventorytransferOutLine has been Sent");
            //        }
            //    }
            //    else
            //    {
            //        ErrorMessageShow("Data InventoryTransferout Not Available");
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOut " + ex.ToString());
            //}
        }

        private void SetInventoryTransferOutLinePostedLotCount(Session _currSession, InventoryTransferOut _locInventoryTransferOutXPO)
        {
            //try
            //{
            //    if (_locInventoryTransferOutXPO != null)
            //    {
            //        if (_locInventoryTransferOutXPO.PostedLotCount < 1)
            //        {
            //            _locInventoryTransferOutXPO.PostedLotCount = _locInventoryTransferOutXPO.PostedLotCount + 1;
            //            _locInventoryTransferOutXPO.Save();
            //            _locInventoryTransferOutXPO.Session.CommitTransaction();

            //            XPCollection<InventoryTransferOutLine> _locInventoryTransferOutLines = new XPCollection<InventoryTransferOutLine>(_currSession,
            //                                                                                   new GroupOperator(GroupOperatorType.And,
            //                                                                                   new BinaryOperator("InventoryTransferOut", _locInventoryTransferOutXPO)));

            //            if (_locInventoryTransferOutLines != null && _locInventoryTransferOutLines.Count > 0)
            //            {
            //                foreach (InventoryTransferOutLine _locInventoryTransferOutLine in _locInventoryTransferOutLines)
            //                {
            //                    if (_locInventoryTransferOutLine.Status == Status.Open || _locInventoryTransferOutLine.Status == Status.Progress)
            //                    {
            //                        _locInventoryTransferOutLine.ProcessLotCount = _locInventoryTransferOutLine.ProcessLotCount + 1;
            //                        _locInventoryTransferOutLine.Save();
            //                        _locInventoryTransferOutLine.Session.CommitTransaction();
            //                    }
            //                }
            //            }
            //            else
            //            {
            //                ErrorMessageShow("Data InventoryTransferoutLine Not Available");
            //            }
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOut " + ex.ToString());
            //}
        }

        #endregion PostingLot

        #region GetSO

        private void GetSalesOrderMonitoring(Session _currSession, SalesOrder _locSalesOrderXPO, InventoryTransferOut _locInventoryTransferOutXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;

                if (_locSalesOrderXPO != null && _locInventoryTransferOutXPO != null)
                {
                    //Hanya memindahkan SalesOrderMonitoring ke InventoryTransferOutLine

                    XPCollection<SalesOrderMonitoring> _locSalesOrderMonitorings = new XPCollection<SalesOrderMonitoring>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("SalesOrder", _locSalesOrderXPO),
                                                                                        new BinaryOperator("InventoryTransferOut", _locInventoryTransferOutXPO),
                                                                                        new BinaryOperator("Select", true),
                                                                                        new BinaryOperator("PostedCount", 0)));

                    if(_locSalesOrderMonitorings != null && _locSalesOrderMonitorings.Count() > 0)
                    {
                        foreach(SalesOrderMonitoring _locSalesOrderMonitoring in _locSalesOrderMonitorings)
                        {
                            if(_locSalesOrderMonitoring.SalesOrderLine != null)
                            {
                                _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.InventoryTransferOutLine);

                                if(_currSignCode != null)
                                {
                                    InventoryTransferOutLine _saveDataInventoryTransferOutLine = new InventoryTransferOutLine(_currSession)
                                    {
                                        SignCode = _currSignCode,
                                        SalesType = _locSalesOrderMonitoring.SalesType,
                                        Item = _locSalesOrderMonitoring.Item,
                                        Description = _locSalesOrderMonitoring.SalesOrderLine.Description,
                                        MxDQty = _locSalesOrderMonitoring.DQty,
                                        MxDUOM = _locSalesOrderMonitoring.DUOM,
                                        MxQty = _locSalesOrderMonitoring.Qty,
                                        MxUOM = _locSalesOrderMonitoring.UOM,
                                        MxTQty = _locSalesOrderMonitoring.TQty,
                                        DQty = _locSalesOrderMonitoring.DQty,
                                        DUOM = _locSalesOrderMonitoring.DUOM,
                                        Qty = _locSalesOrderMonitoring.Qty,
                                        UOM = _locSalesOrderMonitoring.UOM,
                                        TQty = _locSalesOrderMonitoring.TQty,
                                        InventoryTransferOut = _locInventoryTransferOutXPO,
                                        SalesOrderMonitoring = _locSalesOrderMonitoring,
                                    };
                                    _saveDataInventoryTransferOutLine.Save();
                                    _saveDataInventoryTransferOutLine.Session.CommitTransaction();

                                    InventoryTransferOutLine _locInvTransOutLine = _currSession.FindObject<InventoryTransferOutLine>
                                                                                 (new GroupOperator(GroupOperatorType.And,
                                                                                 new BinaryOperator("SignCode", _currSignCode)));
                                    if (_locInvTransOutLine != null)
                                    {
                                        _locSalesOrderMonitoring.InventoryTransferOutLine = _locInvTransOutLine;
                                        _locSalesOrderMonitoring.Status = Status.Close;
                                        _locSalesOrderMonitoring.StatusDate = now;
                                        _locSalesOrderMonitoring.PostedCount = _locSalesOrderMonitoring.PostedCount + 1;
                                        _locSalesOrderMonitoring.Save();
                                        _locSalesOrderMonitoring.Session.CommitTransaction();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferOut ", ex.ToString());
            }
        }

        #endregion GetSO

        #region GetITO

        private void SetInventoryTransferOrder(Session _currSession, InventoryTransferOrder _locInventoryTransferOrderXPO, InventoryTransferOut _locInventoryTransferOutXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;
                string _currSignCode = null;

                if (_locInventoryTransferOrderXPO != null && _locInventoryTransferOutXPO != null)
                {
                    if (_locInventoryTransferOrderXPO.Code != null)
                    {
                        InventoryTransferOrder _locInventoryTransferOrder = _currSession.FindObject<InventoryTransferOrder>
                                                                (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Code", _locInventoryTransferOrderXPO.Code)));

                        if (_locInventoryTransferOrder != null)
                        {
                            if (_locInventoryTransferOrder.Status == Status.Progress || _locInventoryTransferOrder.Status == Status.Posted)
                            {
                                XPCollection<InventoryTransferOrderLine> _locInventoryTransferOrderLines = new XPCollection<InventoryTransferOrderLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("InventoryTransferOrder", _locInventoryTransferOrder),
                                                            new BinaryOperator("Select", true)));

                                if (_locInventoryTransferOrderLines != null && _locInventoryTransferOrderLines.Count() > 0)
                                {
                                    foreach (InventoryTransferOrderLine _locInventoryTransferOrderLine in _locInventoryTransferOrderLines)
                                    {
                                        if (_locInventoryTransferOrderLine.Status == Status.Progress || _locInventoryTransferOrderLine.Status == Status.Posted)
                                        {
                                            InventoryTransferOutLine _locInventoryTransferOutLine = _currSession.FindObject<InventoryTransferOutLine>
                                                                            (new GroupOperator(GroupOperatorType.And,
                                                                             new BinaryOperator("InventoryTransferOut", _locInventoryTransferOutXPO),
                                                                             new BinaryOperator("Item", _locInventoryTransferOrderLine.Item)));

                                            if (_locInventoryTransferOutLine != null)
                                            {
                                                _locInventoryTransferOutLine.MxDQty = _locInventoryTransferOutLine.MxDQty + _locInventoryTransferOrderLine.DQty;
                                                _locInventoryTransferOutLine.MxQty = _locInventoryTransferOutLine.MxQty + _locInventoryTransferOrderLine.Qty;
                                                _locInventoryTransferOutLine.MxTQty = _locInventoryTransferOutLine.MxTQty + _locInventoryTransferOrderLine.TQty;
                                                _locInventoryTransferOutLine.DQty = _locInventoryTransferOutLine.DQty + _locInventoryTransferOrderLine.DQty;
                                                _locInventoryTransferOutLine.Qty = _locInventoryTransferOutLine.Qty + _locInventoryTransferOrderLine.Qty;
                                                _locInventoryTransferOutLine.TQty = _locInventoryTransferOutLine.TQty + _locInventoryTransferOrderLine.TQty;
                                                _locInventoryTransferOutLine.Save();
                                                _locInventoryTransferOutLine.Session.CommitTransaction();
                                                //Tambahkan ke Object SalesOrderMonitoring

                                                InventoryTransferOrderMonitoring _saveDataInventoryTransferOrderMonitoring = new InventoryTransferOrderMonitoring(_currSession)
                                                {
                                                    InventoryTransferOrder = _locInventoryTransferOrder,
                                                    InventoryTransferOrderLine = _locInventoryTransferOrderLine,
                                                    InventoryTransferOut = _locInventoryTransferOutXPO,
                                                    InventoryTransferOutLine = _locInventoryTransferOutLine,
                                                    Item = _locInventoryTransferOrderLine.Item,
                                                    DQty = _locInventoryTransferOrderLine.DQty,
                                                    DUOM = _locInventoryTransferOrderLine.DUOM,
                                                    Qty = _locInventoryTransferOrderLine.Qty,
                                                    UOM = _locInventoryTransferOrderLine.UOM,
                                                    TQty = _locInventoryTransferOrderLine.TQty,
                                                };
                                                _saveDataInventoryTransferOrderMonitoring.Save();
                                                _saveDataInventoryTransferOrderMonitoring.Session.CommitTransaction();
                                            }
                                            else
                                            {
                                                _currSignCode = _globFunc.GetNumberingSignUnlockOptimisticRecord(_currSession.DataLayer, ObjectList.InventoryTransferOutLine);

                                                if (_currSignCode != null)
                                                {
                                                    InventoryTransferOutLine _saveDataInventoryTransferOutLine = new InventoryTransferOutLine(_currSession)
                                                    {
                                                        SignCode = _currSignCode,
                                                        Item = _locInventoryTransferOrderLine.Item,
                                                        Description = _locInventoryTransferOrderLine.Description,
                                                        MxDQty = _locInventoryTransferOrderLine.DQty,
                                                        MxDUOM = _locInventoryTransferOrderLine.DUOM,
                                                        MxQty = _locInventoryTransferOrderLine.Qty,
                                                        MxUOM = _locInventoryTransferOrderLine.UOM,
                                                        MxTQty = _locInventoryTransferOrderLine.TQty,
                                                        DQty = _locInventoryTransferOrderLine.DQty,
                                                        DUOM = _locInventoryTransferOrderLine.DUOM,
                                                        Qty = _locInventoryTransferOrderLine.Qty,
                                                        UOM = _locInventoryTransferOrderLine.UOM,
                                                        TQty = _locInventoryTransferOrderLine.TQty,
                                                        InventoryTransferOut = _locInventoryTransferOutXPO,
                                                    };
                                                    _saveDataInventoryTransferOutLine.Save();
                                                    _saveDataInventoryTransferOutLine.Session.CommitTransaction();
                                                    //Tambahkan ke object SalesOrderMonitoring

                                                    InventoryTransferOutLine _locInvTransOutLine = _currSession.FindObject<InventoryTransferOutLine>
                                                                                                                           (new GroupOperator(GroupOperatorType.And,
                                                                                                                            new BinaryOperator("SignCode", _currSignCode)));
                                                    if (_locInvTransOutLine != null)
                                                    {
                                                        InventoryTransferOrderMonitoring _saveDataInventoryTransferOrderMonitoring = new InventoryTransferOrderMonitoring(_currSession)
                                                        {
                                                            InventoryTransferOrder = _locInventoryTransferOrder,
                                                            InventoryTransferOrderLine = _locInventoryTransferOrderLine,
                                                            InventoryTransferOut = _locInventoryTransferOutXPO,
                                                            InventoryTransferOutLine = _locInventoryTransferOutLine,
                                                            Item = _locInventoryTransferOrderLine.Item,
                                                            DQty = _locInventoryTransferOrderLine.DQty,
                                                            DUOM = _locInventoryTransferOrderLine.DUOM,
                                                            Qty = _locInventoryTransferOrderLine.Qty,
                                                            UOM = _locInventoryTransferOrderLine.UOM,
                                                            TQty = _locInventoryTransferOrderLine.TQty,
                                                        };
                                                        _saveDataInventoryTransferOrderMonitoring.Save();
                                                        _saveDataInventoryTransferOrderMonitoring.Session.CommitTransaction();

                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferOut ", ex.ToString());
            }
        }

        private void SetRemainQtyITOL(Session _currSession, InventoryTransferOrder _locInventoryTransferOrderXPO)
        {
            try
            {
                if (_locInventoryTransferOrderXPO != null)
                {
                    XPCollection<InventoryTransferOrderLine> _locInventoryTransferOrderLines = new XPCollection<InventoryTransferOrderLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("InventoryTransferOrder", _locInventoryTransferOrderXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locInventoryTransferOrderLines != null && _locInventoryTransferOrderLines.Count > 0)
                    {
                        double _locRmDQty = 0;
                        double _locRmQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (InventoryTransferOrderLine _locInventoryTransferOrderLine in _locInventoryTransferOrderLines)
                        {
                            #region ProcessCount=0
                            if (_locInventoryTransferOrderLine.ProcessCount == 0)
                            {
                                #region MaxQuantity
                                if (_locInventoryTransferOrderLine.MxDQty > 0)
                                {
                                    if (_locInventoryTransferOrderLine.DQty > 0 && _locInventoryTransferOrderLine.DQty <= _locInventoryTransferOrderLine.MxDQty)
                                    {
                                        _locRmDQty = _locInventoryTransferOrderLine.MxDQty - _locInventoryTransferOrderLine.DQty;
                                    }

                                    if (_locInventoryTransferOrderLine.Qty > 0 && _locInventoryTransferOrderLine.Qty <= _locInventoryTransferOrderLine.MxQty)
                                    {
                                        _locRmQty = _locInventoryTransferOrderLine.MxQty - _locInventoryTransferOrderLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locInventoryTransferOrderLine.Item),
                                                                new BinaryOperator("UOM", _locInventoryTransferOrderLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locInventoryTransferOrderLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locInventoryTransferOrderLine.DQty > 0)
                                    {
                                        _locRmDQty = 0;
                                    }

                                    if (_locInventoryTransferOrderLine.Qty > 0)
                                    {
                                        _locRmQty = 0;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locInventoryTransferOrderLine.Item),
                                                                new BinaryOperator("UOM", _locInventoryTransferOrderLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locInventoryTransferOrderLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locInventoryTransferOrderLine.ProcessCount > 0)
                            {
                                if (_locInventoryTransferOrderLine.RmDQty > 0)
                                {
                                    _locRmDQty = _locInventoryTransferOrderLine.RmDQty - _locInventoryTransferOrderLine.DQty;
                                }

                                if (_locInventoryTransferOrderLine.RmQty > 0)
                                {
                                    _locRmQty = _locInventoryTransferOrderLine.RmQty - _locInventoryTransferOrderLine.Qty;
                                }

                                if (_locInventoryTransferOrderLine.MxDQty > 0 || _locInventoryTransferOrderLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locInventoryTransferOrderLine.Item),
                                                                new BinaryOperator("UOM", _locInventoryTransferOrderLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locInventoryTransferOrderLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locInventoryTransferOrderLine.Item),
                                                                new BinaryOperator("UOM", _locInventoryTransferOrderLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locInventoryTransferOrderLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                }


                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locInventoryTransferOrderLine.RmDQty = _locRmDQty;
                            _locInventoryTransferOrderLine.RmQty = _locRmQty;
                            _locInventoryTransferOrderLine.RmTQty = _locInvLineTotal;
                            _locInventoryTransferOrderLine.Save();
                            _locInventoryTransferOrderLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOut " + ex.ToString());
            }
        }

        private void SetPostingQtyITOL(Session _currSession, InventoryTransferOrder _locInventoryTransferOrderXPO)
        {
            try
            {
                if (_locInventoryTransferOrderXPO != null)
                {
                    XPCollection<InventoryTransferOrderLine> _locInventoryOrderLines = new XPCollection<InventoryTransferOrderLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("InventoryTransferOrder", _locInventoryTransferOrderXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locInventoryOrderLines != null && _locInventoryOrderLines.Count > 0)
                    {
                        double _locPDQty = 0;
                        double _locPQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (InventoryTransferOrderLine _locInventoryOrderLine in _locInventoryOrderLines)
                        {
                            #region ProcessCount=0
                            if (_locInventoryOrderLine.ProcessCount == 0)
                            {
                                #region MaxQuantity
                                if (_locInventoryOrderLine.MxDQty > 0)
                                {
                                    if (_locInventoryOrderLine.DQty > 0 && _locInventoryOrderLine.DQty <= _locInventoryOrderLine.MxDQty)
                                    {
                                        _locPDQty = _locInventoryOrderLine.DQty;
                                    }

                                    if (_locInventoryOrderLine.Qty > 0 && _locInventoryOrderLine.Qty <= _locInventoryOrderLine.MxQty)
                                    {
                                        _locPQty = _locInventoryOrderLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locInventoryOrderLine.Item),
                                                                new BinaryOperator("UOM", _locInventoryOrderLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locInventoryOrderLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locInventoryOrderLine.DQty > 0)
                                    {
                                        _locPDQty = _locInventoryOrderLine.DQty;
                                    }

                                    if (_locInventoryOrderLine.Qty > 0)
                                    {
                                        _locPQty = _locInventoryOrderLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locInventoryOrderLine.Item),
                                                                new BinaryOperator("UOM", _locInventoryOrderLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locInventoryOrderLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locInventoryOrderLine.ProcessCount > 0)
                            {
                                if (_locInventoryOrderLine.PDQty > 0)
                                {
                                    _locPDQty = _locInventoryOrderLine.PDQty + _locInventoryOrderLine.DQty;
                                }

                                if (_locInventoryOrderLine.PQty > 0)
                                {
                                    _locPQty = _locInventoryOrderLine.PQty + _locInventoryOrderLine.Qty;
                                }

                                if (_locInventoryOrderLine.MxDQty > 0 || _locInventoryOrderLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locInventoryOrderLine.Item),
                                                            new BinaryOperator("UOM", _locInventoryOrderLine.MxUOM),
                                                            new BinaryOperator("DefaultUOM", _locInventoryOrderLine.MxDUOM),
                                                            new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locInventoryOrderLine.Item),
                                                            new BinaryOperator("UOM", _locInventoryOrderLine.UOM),
                                                            new BinaryOperator("DefaultUOM", _locInventoryOrderLine.DUOM),
                                                            new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locInventoryOrderLine.PDQty = _locPDQty;
                            _locInventoryOrderLine.PDUOM = _locInventoryOrderLine.DUOM;
                            _locInventoryOrderLine.PQty = _locPQty;
                            _locInventoryOrderLine.PUOM = _locInventoryOrderLine.UOM;
                            _locInventoryOrderLine.PTQty = _locInvLineTotal;
                            _locInventoryOrderLine.Save();
                            _locInventoryOrderLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOut " + ex.ToString());
            }

        }

        private void SetProcessCountITOL(Session _currSession, InventoryTransferOrder _locInventoryTransferOrderXPO)
        {
            try
            {
                if (_locInventoryTransferOrderXPO != null)
                {
                    XPCollection<InventoryTransferOrderLine> _locInventoryTransferOrderLines = new XPCollection<InventoryTransferOrderLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("InventoryTransferOrder", _locInventoryTransferOrderXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locInventoryTransferOrderLines != null && _locInventoryTransferOrderLines.Count > 0)
                    {

                        foreach (InventoryTransferOrderLine _locInventoryTransferOrderLine in _locInventoryTransferOrderLines)
                        {
                            if (_locInventoryTransferOrderLine.Status == Status.Progress || _locInventoryTransferOrderLine.Status == Status.Posted)
                            {
                                _locInventoryTransferOrderLine.ProcessCount = _locInventoryTransferOrderLine.ProcessCount + 1;
                                _locInventoryTransferOrderLine.Save();
                                _locInventoryTransferOrderLine.Session.CommitTransaction();
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOut " + ex.ToString());
            }
        }

        private void SetStatusInventoryTransferOrderLine(Session _currSession, InventoryTransferOrder _locInventoryTransferOrderXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locInventoryTransferOrderXPO != null)
                {
                    XPCollection<InventoryTransferOrderLine> _locInventoryTransferOrderLines = new XPCollection<InventoryTransferOrderLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("SalesOrder", _locInventoryTransferOrderXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locInventoryTransferOrderLines != null && _locInventoryTransferOrderLines.Count > 0)
                    {

                        foreach (InventoryTransferOrderLine _locInventoryTransferOrderLine in _locInventoryTransferOrderLines)
                        {
                            if (_locInventoryTransferOrderLine.Status == Status.Progress || _locInventoryTransferOrderLine.Status == Status.Posted)
                            {
                                if (_locInventoryTransferOrderLine.RmDQty == 0 && _locInventoryTransferOrderLine.RmQty == 0 && _locInventoryTransferOrderLine.RmTQty == 0)
                                {
                                    _locInventoryTransferOrderLine.Status = Status.Close;
                                    _locInventoryTransferOrderLine.ActivationQuantity = true;
                                    _locInventoryTransferOrderLine.ActivationPosting = true;
                                    _locInventoryTransferOrderLine.StatusDate = now;
                                }
                                else
                                {
                                    _locInventoryTransferOrderLine.Status = Status.Posted;
                                    _locInventoryTransferOrderLine.StatusDate = now;
                                }
                                _locInventoryTransferOrderLine.Save();
                                _locInventoryTransferOrderLine.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOut " + ex.ToString());
            }
        }

        private void SetNormalQuantityITOL(Session _currSession, InventoryTransferOrder _locInventoryTransferOrderXPO)
        {
            try
            {
                if (_locInventoryTransferOrderXPO != null)
                {
                    XPCollection<InventoryTransferOrderLine> _locInventoryTransferOrderLines = new XPCollection<InventoryTransferOrderLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("InventoryTransferOrder", _locInventoryTransferOrderXPO),
                                                                                     new BinaryOperator("Select", true)));

                    if (_locInventoryTransferOrderLines != null && _locInventoryTransferOrderLines.Count > 0)
                    {
                        foreach (InventoryTransferOrderLine _locInventoryTransferOrderLine in _locInventoryTransferOrderLines)
                        {
                            if (_locInventoryTransferOrderLine.Status == Status.Progress || _locInventoryTransferOrderLine.Status == Status.Posted || _locInventoryTransferOrderLine.Status == Status.Close)
                            {
                                if (_locInventoryTransferOrderLine.DQty > 0 || _locInventoryTransferOrderLine.Qty > 0)
                                {
                                    _locInventoryTransferOrderLine.Select = false;
                                    _locInventoryTransferOrderLine.DQty = 0;
                                    _locInventoryTransferOrderLine.Qty = 0;
                                    _locInventoryTransferOrderLine.Save();
                                    _locInventoryTransferOrderLine.Session.CommitTransaction();
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOut " + ex.ToString());
            }
        }

        private void SetFinalStatusInventoryTransferOrderInGet(Session _currSession, InventoryTransferOrder _locInventoryTransferOrderXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locStatusCount = 0;
                int _locPurchaseRequisitionLineCount = 0;

                if (_locInventoryTransferOrderXPO != null)
                {

                    XPCollection<InventoryTransferOrderLine> _locInventoryTransferOrderLines = new XPCollection<InventoryTransferOrderLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("InventoryTransferOrder", _locInventoryTransferOrderXPO)));

                    if (_locInventoryTransferOrderLines != null && _locInventoryTransferOrderLines.Count() > 0)
                    {
                        _locPurchaseRequisitionLineCount = _locInventoryTransferOrderLines.Count();

                        foreach (InventoryTransferOrderLine _locInventoryTransferOrderLine in _locInventoryTransferOrderLines)
                        {
                            if (_locInventoryTransferOrderLine.Status == Status.Close)
                            {
                                _locStatusCount = _locStatusCount + 1;
                            }
                        }
                    }

                    if (_locStatusCount == _locPurchaseRequisitionLineCount)
                    {
                        _locInventoryTransferOrderXPO.ActivationPosting = true;
                        _locInventoryTransferOrderXPO.Status = Status.Close;
                        _locInventoryTransferOrderXPO.StatusDate = now;
                        _locInventoryTransferOrderXPO.Save();
                        _locInventoryTransferOrderXPO.Session.CommitTransaction();
                    }
                    else
                    {
                        _locInventoryTransferOrderXPO.Status = Status.Posted;
                        _locInventoryTransferOrderXPO.StatusDate = now;
                        _locInventoryTransferOrderXPO.Save();
                        _locInventoryTransferOrderXPO.Session.CommitTransaction();
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOut " + ex.ToString());
            }
        }

        #endregion GetITO

        #region Posting

        #region InventoryNormalDeliver

        private bool CheckAvailableStock(Session _currSession, InventoryTransferOut _locInventoryTransferOutXPO)
        {
            bool _result = true;
            try
            {
                XPCollection<InventoryTransferOutLine> _locInventoryTransOutLines = new XPCollection<InventoryTransferOutLine>(_currSession,
                                                                        new BinaryOperator("InventoryTransferOut", _locInventoryTransferOutXPO));
                
                if (_locInventoryTransOutLines != null && _locInventoryTransOutLines.Count > 0)
                {
                    double _locTransOutLineTotal = 0;
                    BeginingInventory _locBegInventory = null;
                    XPCollection<BeginingInventoryLine> _locBegInvLines = null;
                    foreach (InventoryTransferOutLine _locInventoryTransOutLine in _locInventoryTransOutLines)
                    {

                        XPCollection<InventoryTransferOutLot> _locInventoryTransferOutLots = new XPCollection<InventoryTransferOutLot>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("InventoryTransferOutLine", _locInventoryTransOutLine)));

                        #region LotNumber
                        if(_locInventoryTransferOutLots != null && _locInventoryTransferOutLots.Count() > 0)
                        {
                            foreach(InventoryTransferOutLot _locInventoryTransferOutLot in _locInventoryTransferOutLots)
                            {
                                _locBegInventory = _currSession.FindObject<BeginingInventory>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Item", _locInventoryTransferOutLot.Item)));
                                if(_locBegInventory != null)
                                {
                                    if(_locInventoryTransferOutLot.BegInvLine != null)
                                    {
                                        if(_locInventoryTransferOutLot.BegInvLine.Code != null)
                                        {
                                            BeginingInventoryLine _locBegInvLine = _currSession.FindObject<BeginingInventoryLine>(
                                                                                        new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("Code", _locInventoryTransferOutLot.BegInvLine.Code)));
                                            if(_locBegInvLine != null)
                                            {
                                                if(_locInventoryTransferOutLot.TQty > _locBegInvLine.QtyAvailable)
                                                {
                                                    _result = false;
                                                }
                                            }
                                            else
                                            {
                                                _result = false;
                                            }
                                        }
                                    }
                                    
                                }
                            }
                        }
                        #endregion LotNumber
                        #region NonLotNumber
                        else
                        {
                            if (_locInventoryTransOutLine.Location != null || _locInventoryTransOutLine.BinLocation != null)
                            {
                                _locBegInventory = _currSession.FindObject<BeginingInventory>
                                                    (new GroupOperator(GroupOperatorType.And,
                                                    new BinaryOperator("Item", _locInventoryTransOutLine.Item)));

                                if (_locBegInventory != null)
                                {
                                    if(_locInventoryTransOutLine.Location != null && _locInventoryTransOutLine.BinLocation != null)
                                    {
                                        _locBegInvLines = new XPCollection<BeginingInventoryLine>(_currSession,
                                                                                        new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("BeginingInventory", _locBegInventory),
                                                                                        new BinaryOperator("Item", _locInventoryTransOutLine.Item),
                                                                                        new BinaryOperator("Location", _locInventoryTransOutLine.Location),
                                                                                        new BinaryOperator("BinLocation", _locInventoryTransOutLine.BinLocation),
                                                                                        new BinaryOperator("StockType", _locInventoryTransOutLine.StockType)));

                                    }
                                    else if(_locInventoryTransOutLine.Location != null && _locInventoryTransOutLine.BinLocation == null)
                                    {
                                        _locBegInvLines = new XPCollection<BeginingInventoryLine>(_currSession,
                                                                                        new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("BeginingInventory", _locBegInventory),
                                                                                        new BinaryOperator("Item", _locInventoryTransOutLine.Item),
                                                                                        new BinaryOperator("Location", _locInventoryTransOutLine.Location),
                                                                                        new BinaryOperator("StockType", _locInventoryTransOutLine.StockType)));
                                    }
                                    

                                    if (_locBegInvLines != null && _locBegInvLines.Count() > 0)
                                    {
                                        foreach (BeginingInventoryLine _locBegInvLine in _locBegInvLines)
                                        {
                                            ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                             new BinaryOperator("Item", _locInventoryTransOutLine.Item),
                                                             new BinaryOperator("UOM", _locInventoryTransOutLine.UOM),
                                                             new BinaryOperator("DefaultUOM", _locInventoryTransOutLine.DUOM),
                                                             new BinaryOperator("Active", true)));

                                            if (_locItemUOM != null)
                                            {
                                                if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                {
                                                    _locTransOutLineTotal = _locInventoryTransOutLine.Qty * _locItemUOM.DefaultConversion + _locInventoryTransOutLine.DQty;
                                                }
                                                else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                {
                                                    _locTransOutLineTotal = _locInventoryTransOutLine.Qty / _locItemUOM.Conversion + _locInventoryTransOutLine.DQty;
                                                }
                                                else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                {
                                                    _locTransOutLineTotal = _locInventoryTransOutLine.Qty + _locInventoryTransOutLine.DQty;
                                                }

                                                if (_locTransOutLineTotal > _locBegInventory.QtyAvailable)
                                                {
                                                    _result = false;
                                                }
                                            }
                                            else
                                            {
                                                _locTransOutLineTotal = _locInventoryTransOutLine.Qty + _locInventoryTransOutLine.DQty;

                                                if (_locTransOutLineTotal > _locBegInventory.QtyAvailable)
                                                {
                                                    _result = false;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        #endregion NonLotNumber
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferOut ", ex.ToString());
            }
            return _result;
        }

        private void SetInventoryTransferOutMonitoring(Session _currSession, InventoryTransferOut _locInventoryTransferOutXPO)
        {
            try
            {
                GlobalFunction _globFunc = new GlobalFunction();
                DateTime now = DateTime.Now;

                if (_locInventoryTransferOutXPO != null)
                {
                    if (_locInventoryTransferOutXPO.Status == Status.Progress || _locInventoryTransferOutXPO.Status == Status.Posted)
                    {
                        XPCollection<InventoryTransferOutLine> _locInventoryTransferOutLines = new XPCollection<InventoryTransferOutLine>
                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("InventoryTransferOut", _locInventoryTransferOutXPO),
                                                            new BinaryOperator("Select", true),
                                                            new GroupOperator(GroupOperatorType.Or,
                                                            new BinaryOperator("Status", Status.Progress),
                                                            new BinaryOperator("Status", Status.Posted))));

                        if (_locInventoryTransferOutLines != null && _locInventoryTransferOutLines.Count() > 0)
                        {
                            foreach (InventoryTransferOutLine _locInventoryTransferOutLine in _locInventoryTransferOutLines)
                            {
                                InventoryTransferOutMonitoring _saveDataInventoryTransferOutMonitoring = new InventoryTransferOutMonitoring(_currSession)
                                {
                                    InventoryTransferOut  = _locInventoryTransferOutXPO,
                                    InventoryTransferOutLine = _locInventoryTransferOutLine,
                                    SalesType = _locInventoryTransferOutLine.SalesType,
                                    Item = _locInventoryTransferOutLine.Item,
                                    DQty = _locInventoryTransferOutLine.DQty,
                                    DUOM = _locInventoryTransferOutLine.DUOM,
                                    Qty = _locInventoryTransferOutLine.Qty,
                                    UOM = _locInventoryTransferOutLine.UOM,
                                    TQty = _locInventoryTransferOutLine.TQty,
                                    SalesOrderMonitoring = _locInventoryTransferOutLine.SalesOrderMonitoring,
                                };
                                _saveDataInventoryTransferOutMonitoring.Save();
                                _saveDataInventoryTransferOutMonitoring.Session.CommitTransaction();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = SalesOrder ", ex.ToString());
            }
        }

        private void SetDeliverBeginingInventory(Session _currSession, InventoryTransferOut _locInventoryTransferOutXPO)
        {
            try
            {
                XPCollection<InventoryTransferOutLine> _locInvTransOutLines = new XPCollection<InventoryTransferOutLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("InventoryTransferOut", _locInventoryTransferOutXPO),
                                                                        new BinaryOperator("Select", true)));

                if (_locInvTransOutLines != null && _locInvTransOutLines.Count() > 0)
                {
                    GlobalFunction _globFunc = new GlobalFunction();
                    double _locInvLineTotal = 0;
                    XPCollection<BeginingInventoryLine> _locBegInventoryLines = null;
                    ItemUnitOfMeasure _locItemUOM = null;
                    string _fullString = null;
                    string _locItemParse = null;
                    string _locLocationParse = null;
                    string _locBinLocationParse = null;
                    string _locDUOMParse = null;
                    string _locStockTypeParse = null;
                    string _locProjectHeader = null;
                    string _locActiveParse = null;

                    foreach (InventoryTransferOutLine _locInvTransOutLine in _locInvTransOutLines)
                    {
                        if (_locInvTransOutLine.Status == Status.Progress || _locInvTransOutLine.Status == Status.Posted)
                        {
                            if (_locInvTransOutLine.DQty > 0 || _locInvTransOutLine.Qty > 0)
                            {
                                XPCollection<InventoryTransferOutLot> _locInventoryTransferOutLots = new XPCollection<InventoryTransferOutLot>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("InventoryTransferOutLine", _locInvTransOutLine)));
                                #region LotNumber
                                if(_locInventoryTransferOutLots != null && _locInventoryTransferOutLots.Count() > 0)
                                {
                                    foreach(InventoryTransferOutLot _locInventoryTransferOutLot in _locInventoryTransferOutLots)
                                    {
                                        if(_locInventoryTransferOutLot.Select == true)
                                        {
                                            if (_locInventoryTransferOutLot.BegInvLine != null)
                                            {
                                                if (_locInventoryTransferOutLot.BegInvLine.Code != null)
                                                {
                                                    BeginingInventoryLine _locBegInvLine = _currSession.FindObject<BeginingInventoryLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Code", _locInventoryTransferOutLot.BegInvLine.Code)));
                                                    if (_locBegInvLine != null)
                                                    {
                                                        if (_locBegInvLine.QtyAvailable >= _locInventoryTransferOutLot.TQty)
                                                        {
                                                            _locBegInvLine.QtyAvailable = _locBegInvLine.QtyAvailable - _locInventoryTransferOutLot.TQty;
                                                            _locBegInvLine.QtySale = _locBegInvLine.QtySale + _locInventoryTransferOutLot.TQty;
                                                            _locBegInvLine.Save();
                                                            _locBegInvLine.Session.CommitTransaction();
                                                        }

                                                        if (_locBegInvLine.BeginingInventory != null)
                                                        {
                                                            if (_locBegInvLine.BeginingInventory.Code != null)
                                                            {
                                                                BeginingInventory _locBegInv = _currSession.FindObject<BeginingInventory>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Code", _locBegInvLine.BeginingInventory.Code)));
                                                                if (_locBegInv != null)
                                                                {
                                                                    _locBegInv.QtyAvailable = _locBegInv.QtyAvailable - _locInventoryTransferOutLot.TQty;
                                                                    _locBegInv.QtySale = _locBegInv.QtySale + _locInventoryTransferOutLot.TQty;
                                                                    _locBegInv.Save();
                                                                    _locBegInv.Session.CommitTransaction();
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }  
                                    }
                                }
                                #endregion LotNumber
                                #region NonLotNumber
                                else
                                {
                                    if (_locInvTransOutLine.Item != null && _locInvTransOutLine.UOM != null && _locInvTransOutLine.DUOM != null)
                                    {
                                        //for conversion
                                        _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                             new BinaryOperator("Item", _locInvTransOutLine.Item),
                                                             new BinaryOperator("UOM", _locInvTransOutLine.UOM),
                                                             new BinaryOperator("DefaultUOM", _locInvTransOutLine.DUOM),
                                                             new BinaryOperator("Active", true)));
                                    }

                                    //for query
                                    BeginingInventory _locBeginingInventory = _currSession.FindObject<BeginingInventory>(new GroupOperator(GroupOperatorType.And,
                                                                              new BinaryOperator("Item", _locInvTransOutLine.Item)));

                                    #region UpdateBeginingInventory
                                    if (_locBeginingInventory != null)
                                    {
                                        #region code
                                        if (_locInvTransOutLine.Item != null) { _locItemParse = "[Item.Code]=='" + _locInvTransOutLine.Item.Code + "'"; }
                                        else { _locItemParse = ""; }

                                        if (_locInvTransOutLine.Location != null && (_locInvTransOutLine.Item != null))
                                        { _locLocationParse = " AND [Location.Code]=='" + _locInvTransOutLine.Location.Code + "'"; }
                                        else if (_locInvTransOutLine.Location != null && _locInvTransOutLine.Item == null)
                                        { _locLocationParse = " [Location.Code]=='" + _locInvTransOutLine.Location.Code + "'"; }
                                        else { _locLocationParse = ""; }

                                        if (_locInvTransOutLine.BinLocation != null && (_locInvTransOutLine.Item != null || _locInvTransOutLine.Location != null))
                                        { _locBinLocationParse = " AND [BinLocation.Code]=='" + _locInvTransOutLine.BinLocation.Code + "'"; }
                                        else if (_locInvTransOutLine.BinLocation != null && _locInvTransOutLine.Item == null && _locInvTransOutLine.Location == null)
                                        { _locBinLocationParse = " [BinLocation.Code]=='" + _locInvTransOutLine.BinLocation.Code + "'"; }
                                        else { _locBinLocationParse = ""; }

                                        if (_locInvTransOutLine.DUOM != null && (_locInvTransOutLine.Item != null || _locInvTransOutLine.Location != null || _locInvTransOutLine.BinLocation != null))
                                        { _locDUOMParse = " AND [DefaultUOM.Code]=='" + _locInvTransOutLine.DUOM.Code + "'"; }
                                        else if (_locInvTransOutLine.DUOM != null && _locInvTransOutLine.Item == null && _locInvTransOutLine.Location == null && _locInvTransOutLine.BinLocation == null)
                                        { _locDUOMParse = " [DefaultUOM.Code]=='" + _locInvTransOutLine.DUOM.Code + "'"; }
                                        else { _locDUOMParse = ""; }

                                        if (_locInvTransOutLine.StockType != StockType.None && (_locInvTransOutLine.Item != null || _locInvTransOutLine.Location != null || _locInvTransOutLine.BinLocation != null
                                            || _locInvTransOutLine.DUOM != null))
                                        { _locStockTypeParse = " AND [StockType]=='" + GetStockType(_locInvTransOutLine.StockType).ToString() + "'"; }
                                        else if (_locInvTransOutLine.StockType != StockType.None && _locInvTransOutLine.Item == null && _locInvTransOutLine.Location == null && _locInvTransOutLine.BinLocation == null
                                            && _locInvTransOutLine.DUOM == null)
                                        { _locStockTypeParse = " [StockType]=='" + GetStockType(_locInvTransOutLine.StockType).ToString() + "'"; }
                                        else { _locStockTypeParse = ""; }

                                        if (_locInvTransOutLine.Item == null && _locInvTransOutLine.Location == null && _locInvTransOutLine.BinLocation == null
                                            && _locInvTransOutLine.DUOM == null && _locInvTransOutLine.StockType != StockType.None)
                                        { _locActiveParse = " [Active]=='" + GetActive(true).ToString() + "'"; }
                                        else { _locActiveParse = " AND [Active]=='" + GetActive(true).ToString() + "'"; }

                                        if (_locInventoryTransferOutXPO.ProjectHeader != null)
                                        { _locProjectHeader = " AND [ProjectHeader.Code]=='" + _locInventoryTransferOutXPO.ProjectHeader.Code + "'"; }
                                        else
                                        { _locProjectHeader = ""; }

                                        if (_locItemParse != null || _locLocationParse != null || _locBinLocationParse != null || _locDUOMParse != null || _locStockTypeParse != null || _locProjectHeader != null)
                                        {
                                            _fullString = _locItemParse + _locLocationParse + _locBinLocationParse + _locDUOMParse + _locStockTypeParse + _locActiveParse + _locProjectHeader;
                                        }
                                        else
                                        {
                                            _fullString = _locActiveParse;
                                        }
                                        #endregion code

                                        _locBegInventoryLines = new XPCollection<BeginingInventoryLine>(_currSession, CriteriaOperator.Parse(_fullString));

                                        #region UpdateBeginingInventoryLine
                                        if (_locBegInventoryLines != null && _locBegInventoryLines.Count() > 0)
                                        {

                                            foreach (BeginingInventoryLine _locBegInventoryLine in _locBegInventoryLines)
                                            {
                                                if (_locItemUOM != null)
                                                {
                                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                                    {
                                                        _locInvLineTotal = _locInvTransOutLine.Qty * _locItemUOM.DefaultConversion + _locInvTransOutLine.DQty;
                                                    }
                                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                                    {
                                                        _locInvLineTotal = _locInvTransOutLine.Qty / _locItemUOM.Conversion + _locInvTransOutLine.DQty;
                                                    }
                                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                                    {
                                                        _locInvLineTotal = _locInvTransOutLine.Qty + _locInvTransOutLine.DQty;
                                                    }
                                                }

                                                else
                                                {
                                                    _locInvLineTotal = _locInvTransOutLine.Qty + _locInvTransOutLine.DQty;
                                                }

                                                _locBegInventoryLine.QtyAvailable = _locBegInventoryLine.QtyAvailable - _locInvLineTotal;
                                                _locBegInventoryLine.QtySale = _locBegInventoryLine.QtySale + _locInvLineTotal;
                                                _locBegInventoryLine.Save();
                                                _locBegInventoryLine.Session.CommitTransaction();

                                                _locBeginingInventory.QtyAvailable = _locBeginingInventory.QtyAvailable - _locInvLineTotal;
                                                _locBeginingInventory.QtySale = _locBeginingInventory.QtySale + _locInvLineTotal;
                                                _locBeginingInventory.Save();
                                                _locBeginingInventory.Session.CommitTransaction();
                                            }
                                        }
                                        #endregion UpdateBeginingInventoryLine

                                    }
                                    #endregion UpdateBeginingInventory
                                }
                                #endregion NonLotNumber

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferOut ", ex.ToString());
            }
        }

        private void SetDeliverInventoryJournal(Session _currSession, InventoryTransferOut _locInventoryTransferOutXPO)
        {
            try
            {
                XPCollection<InventoryTransferOutLine> _locInvTransOutLines = new XPCollection<InventoryTransferOutLine>(_currSession,
                                                                        new GroupOperator(GroupOperatorType.And,
                                                                        new BinaryOperator("InventoryTransferOut", _locInventoryTransferOutXPO),
                                                                        new BinaryOperator("Select", true)));

                if (_locInvTransOutLines != null && _locInvTransOutLines.Count > 0)
                {
                    double _locInvLineTotal = 0;
                    DateTime now = DateTime.Now;

                    foreach (InventoryTransferOutLine _locInvTransOutLine in _locInvTransOutLines)
                    {
                        if (_locInvTransOutLine.Status == Status.Progress || _locInvTransOutLine.Status == Status.Posted)
                        {
                            if (_locInvTransOutLine.DQty > 0 || _locInvTransOutLine.Qty > 0)
                            {
                                XPCollection<InventoryTransferOutLot> _locInventoryTransferOutLots = new XPCollection<InventoryTransferOutLot>
                                                                                        (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                        new BinaryOperator("InventoryTransferOutLine", _locInvTransOutLine)));

                                #region LotNumber
                                if(_locInventoryTransferOutLots != null && _locInventoryTransferOutLots.Count() > 0)
                                {
                                    foreach (InventoryTransferOutLot _locInventoryTransferOutLot in _locInventoryTransferOutLots)
                                    {
                                        if(_locInventoryTransferOutLot.Select == true)
                                        {
                                            if (_locInventoryTransferOutLot.BegInvLine != null)
                                            {
                                                if (_locInventoryTransferOutLot.BegInvLine.Code != null)
                                                {
                                                    BeginingInventoryLine _locBegInvLine = _currSession.FindObject<BeginingInventoryLine>
                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("Code", _locInventoryTransferOutLot.BegInvLine.Code)));
                                                    if (_locBegInvLine != null)
                                                    {
                                                        InventoryJournal _locNegatifInventoryJournal = new InventoryJournal(_currSession)
                                                        {
                                                            DocumentType = _locInventoryTransferOutXPO.DocumentType,
                                                            DocNo = _locInventoryTransferOutXPO.DocNo,
                                                            Location = _locInventoryTransferOutLot.Location,
                                                            BinLocation = _locInventoryTransferOutLot.BinLocation,
                                                            StockType = _locInventoryTransferOutLot.StockType,
                                                            Item = _locInventoryTransferOutLot.Item,
                                                            Company = _locInventoryTransferOutLot.Company,
                                                            QtyPos = 0,
                                                            QtyNeg = _locInventoryTransferOutLot.TQty,
                                                            DUOM = _locInventoryTransferOutLot.DUOM,
                                                            JournalDate = now,
                                                            LotNumber = _locInventoryTransferOutLot.LotNumber,
                                                            ProjectHeader = _locInventoryTransferOutXPO.ProjectHeader,
                                                            InventoryTransferOut = _locInventoryTransferOutXPO,
                                                        };
                                                        _locNegatifInventoryJournal.Save();
                                                        _locNegatifInventoryJournal.Session.CommitTransaction();
                                                    }
                                                }
                                            }
                                        }  
                                    }
                                }
                                #endregion LotNumber
                                #region NonLotNumber
                                else
                                {
                                    if (_locInvTransOutLine.Item != null && _locInvTransOutLine.UOM != null && _locInvTransOutLine.DUOM != null)
                                    {
                                        ItemUnitOfMeasure _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                             new BinaryOperator("Item", _locInvTransOutLine.Item),
                                                             new BinaryOperator("UOM", _locInvTransOutLine.UOM),
                                                             new BinaryOperator("DefaultUOM", _locInvTransOutLine.DUOM),
                                                             new BinaryOperator("Active", true)));
                                        if (_locItemUOM != null)
                                        {
                                            #region Code
                                            if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                            {
                                                _locInvLineTotal = _locInvTransOutLine.Qty * _locItemUOM.DefaultConversion + _locInvTransOutLine.DQty;
                                            }
                                            else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                            {
                                                _locInvLineTotal = _locInvTransOutLine.Qty / _locItemUOM.Conversion + _locInvTransOutLine.DQty;
                                            }
                                            else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                            {
                                                _locInvLineTotal = _locInvTransOutLine.Qty + _locInvTransOutLine.DQty;
                                            }
                                            #endregion Code

                                            InventoryJournal _locNegatifInventoryJournal = new InventoryJournal(_currSession)
                                            {
                                                DocumentType = _locInventoryTransferOutXPO.DocumentType,
                                                DocNo = _locInventoryTransferOutXPO.DocNo,
                                                Location = _locInvTransOutLine.Location,
                                                BinLocation = _locInvTransOutLine.BinLocation,
                                                StockType = _locInvTransOutLine.StockType,
                                                Item = _locInvTransOutLine.Item,
                                                Company = _locInvTransOutLine.Company,
                                                QtyPos = 0,
                                                QtyNeg = _locInvLineTotal,
                                                DUOM = _locInvTransOutLine.DUOM,
                                                JournalDate = now,
                                                ProjectHeader = _locInventoryTransferOutXPO.ProjectHeader,
                                                InventoryTransferOut = _locInventoryTransferOutXPO,
                                            };
                                            _locNegatifInventoryJournal.Save();
                                            _locNegatifInventoryJournal.Session.CommitTransaction();

                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locInvTransOutLine.Qty + _locInvTransOutLine.DQty;

                                        InventoryJournal _locNegatifInventoryJournal = new InventoryJournal(_currSession)
                                        {
                                            DocumentType = _locInventoryTransferOutXPO.DocumentType,
                                            DocNo = _locInventoryTransferOutXPO.DocNo,
                                            Location = _locInvTransOutLine.Location,
                                            BinLocation = _locInvTransOutLine.BinLocation,
                                            StockType = _locInvTransOutLine.StockType,
                                            Item = _locInvTransOutLine.Item,
                                            Company = _locInvTransOutLine.Company,
                                            QtyPos = 0,
                                            QtyNeg = _locInvLineTotal,
                                            DUOM = _locInvTransOutLine.DUOM,
                                            JournalDate = now,
                                            ProjectHeader = _locInventoryTransferOutXPO.ProjectHeader,
                                            InventoryTransferOut = _locInventoryTransferOutXPO,
                                        };
                                        _locNegatifInventoryJournal.Save();
                                        _locNegatifInventoryJournal.Session.CommitTransaction();
                                    }
                                }
                                #endregion NonLotNumber
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferOut ", ex.ToString());
            }
        }

        private void SetRemainDeliverQty(Session _currSession, InventoryTransferOut _locInventoryTransferOutXPO)
        {
            try
            {
                if (_locInventoryTransferOutXPO != null)
                {
                    XPCollection<InventoryTransferOutLine> _locInventoryTransferOutLines = new XPCollection<InventoryTransferOutLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("InventoryTransferOut", _locInventoryTransferOutXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locInventoryTransferOutLines != null && _locInventoryTransferOutLines.Count > 0)
                    {
                        double _locRmDQty = 0;
                        double _locRmQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (InventoryTransferOutLine _locInventoryTransferOutLine in _locInventoryTransferOutLines)
                        {
                            #region ProcessCount=0
                            if (_locInventoryTransferOutLine.ProcessCount == 0)
                            {
                                #region MaxQuantity
                                if (_locInventoryTransferOutLine.MxDQty > 0)
                                {
                                    if (_locInventoryTransferOutLine.DQty > 0 && _locInventoryTransferOutLine.DQty <= _locInventoryTransferOutLine.MxDQty)
                                    {
                                        _locRmDQty = _locInventoryTransferOutLine.MxDQty - _locInventoryTransferOutLine.DQty;
                                    }

                                    if (_locInventoryTransferOutLine.Qty > 0 && _locInventoryTransferOutLine.Qty <= _locInventoryTransferOutLine.MxQty)
                                    {
                                        _locRmQty = _locInventoryTransferOutLine.MxQty - _locInventoryTransferOutLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locInventoryTransferOutLine.Item),
                                                                new BinaryOperator("UOM", _locInventoryTransferOutLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locInventoryTransferOutLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locInventoryTransferOutLine.DQty > 0)
                                    {
                                        _locRmDQty = 0;
                                    }

                                    if (_locInventoryTransferOutLine.Qty > 0)
                                    {
                                        _locRmQty = 0;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locInventoryTransferOutLine.Item),
                                                                new BinaryOperator("UOM", _locInventoryTransferOutLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locInventoryTransferOutLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locRmQty + _locRmDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locInventoryTransferOutLine.ProcessCount > 0)
                            {
                                if (_locInventoryTransferOutLine.RmDQty > 0)
                                {
                                    _locRmDQty = _locInventoryTransferOutLine.RmDQty - _locInventoryTransferOutLine.DQty;
                                }

                                if (_locInventoryTransferOutLine.RmQty > 0)
                                {
                                    _locRmQty = _locInventoryTransferOutLine.RmQty - _locInventoryTransferOutLine.Qty;
                                }

                                if (_locInventoryTransferOutLine.MxDQty > 0 || _locInventoryTransferOutLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locInventoryTransferOutLine.Item),
                                                                new BinaryOperator("UOM", _locInventoryTransferOutLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locInventoryTransferOutLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locInventoryTransferOutLine.Item),
                                                                new BinaryOperator("UOM", _locInventoryTransferOutLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locInventoryTransferOutLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                }


                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty * _locItemUOM.DefaultConversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty / _locItemUOM.Conversion + _locRmDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locRmQty + _locRmDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locRmQty + _locRmDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locInventoryTransferOutLine.RmDQty = _locRmDQty;
                            _locInventoryTransferOutLine.RmQty = _locRmQty;
                            _locInventoryTransferOutLine.RmTQty = _locInvLineTotal;
                            _locInventoryTransferOutLine.Save();
                            _locInventoryTransferOutLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOut " + ex.ToString());
            }
        }

        private void SetPostingDeliverQty(Session _currSession, InventoryTransferOut _locInventoryTransferOutXPO)
        {
            try
            {
                if (_locInventoryTransferOutXPO != null)
                {
                    XPCollection<InventoryTransferOutLine> _locInvTransOutLines = new XPCollection<InventoryTransferOutLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("InventoryTransferOut", _locInventoryTransferOutXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locInvTransOutLines != null && _locInvTransOutLines.Count > 0)
                    {
                        double _locPDQty = 0;
                        double _locPQty = 0;
                        double _locInvLineTotal = 0;
                        ItemUnitOfMeasure _locItemUOM = null;

                        foreach (InventoryTransferOutLine _locInvTransOutLine in _locInvTransOutLines)
                        {
                            #region ProcessCount=0
                            if (_locInvTransOutLine.ProcessCount == 0)
                            {
                                #region MaxQuantity
                                if (_locInvTransOutLine.MxDQty > 0)
                                {
                                    if (_locInvTransOutLine.DQty > 0 && _locInvTransOutLine.DQty <= _locInvTransOutLine.MxDQty)
                                    {
                                        _locPDQty = _locInvTransOutLine.DQty;
                                    }

                                    if (_locInvTransOutLine.Qty > 0 && _locInvTransOutLine.Qty <= _locInvTransOutLine.MxQty)
                                    {
                                        _locPQty = _locInvTransOutLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locInvTransOutLine.Item),
                                                                new BinaryOperator("UOM", _locInvTransOutLine.MxUOM),
                                                                new BinaryOperator("DefaultUOM", _locInvTransOutLine.MxDUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }

                                }
                                #endregion MaxQuantity
                                #region NonMaxQuantity
                                else
                                {
                                    if (_locInvTransOutLine.DQty > 0)
                                    {
                                        _locPDQty = _locInvTransOutLine.DQty;
                                    }

                                    if (_locInvTransOutLine.Qty > 0)
                                    {
                                        _locPQty = _locInvTransOutLine.Qty;
                                    }


                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                               (new GroupOperator(GroupOperatorType.And,
                                                                new BinaryOperator("Item", _locInvTransOutLine.Item),
                                                                new BinaryOperator("UOM", _locInvTransOutLine.UOM),
                                                                new BinaryOperator("DefaultUOM", _locInvTransOutLine.DUOM),
                                                                new BinaryOperator("Active", true)));
                                    if (_locItemUOM != null)
                                    {
                                        if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                        }
                                        else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                        {
                                            _locInvLineTotal = _locPQty + _locPDQty;
                                        }
                                    }
                                    else
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                #endregion NonMaxQuantity
                            }
                            #endregion ProcessCount=0

                            #region ProcessCount>0
                            if (_locInvTransOutLine.ProcessCount > 0)
                            {
                                if (_locInvTransOutLine.PDQty > 0)
                                {
                                    _locPDQty = _locInvTransOutLine.PDQty + _locInvTransOutLine.DQty;
                                }

                                if (_locInvTransOutLine.PQty > 0)
                                {
                                    _locPQty = _locInvTransOutLine.PQty + _locInvTransOutLine.Qty;
                                }

                                if (_locInvTransOutLine.MxDQty > 0 || _locInvTransOutLine.MxQty > 0)
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locInvTransOutLine.Item),
                                                            new BinaryOperator("UOM", _locInvTransOutLine.MxUOM),
                                                            new BinaryOperator("DefaultUOM", _locInvTransOutLine.MxDUOM),
                                                            new BinaryOperator("Active", true)));
                                }
                                else
                                {
                                    _locItemUOM = _currSession.FindObject<ItemUnitOfMeasure>
                                                           (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("Item", _locInvTransOutLine.Item),
                                                            new BinaryOperator("UOM", _locInvTransOutLine.UOM),
                                                            new BinaryOperator("DefaultUOM", _locInvTransOutLine.DUOM),
                                                            new BinaryOperator("Active", true)));
                                }

                                if (_locItemUOM != null)
                                {
                                    if (_locItemUOM.Conversion < _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty * _locItemUOM.DefaultConversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion > _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty / _locItemUOM.Conversion + _locPDQty;
                                    }
                                    else if (_locItemUOM.Conversion == _locItemUOM.DefaultConversion)
                                    {
                                        _locInvLineTotal = _locPQty + _locPDQty;
                                    }
                                }
                                else
                                {
                                    _locInvLineTotal = _locPQty + _locPDQty;
                                }

                            }
                            #endregion ProcessCount>0

                            _locInvTransOutLine.PDQty = _locPDQty;
                            _locInvTransOutLine.PDUOM = _locInvTransOutLine.DUOM;
                            _locInvTransOutLine.PQty = _locPQty;
                            _locInvTransOutLine.PUOM = _locInvTransOutLine.UOM;
                            _locInvTransOutLine.PTQty = _locInvLineTotal;
                            _locInvTransOutLine.Save();
                            _locInvTransOutLine.Session.CommitTransaction();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOut " + ex.ToString());
            }

        }

        private void SetProcessDeliverCount(Session _currSession, InventoryTransferOut _locInventoryTransferOutXPO)
        {
            try
            {
                if (_locInventoryTransferOutXPO != null)
                {
                    XPCollection<InventoryTransferOutLine> _locInventoryTransferOutLines = new XPCollection<InventoryTransferOutLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("InventoryTransferOut", _locInventoryTransferOutXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locInventoryTransferOutLines != null && _locInventoryTransferOutLines.Count > 0)
                    {

                        foreach (InventoryTransferOutLine _locInventoryTransferOutLine in _locInventoryTransferOutLines)
                        {
                            if (_locInventoryTransferOutLine.Status == Status.Progress || _locInventoryTransferOutLine.Status == Status.Posted)
                            {
                                _locInventoryTransferOutLine.ProcessCount = _locInventoryTransferOutLine.ProcessCount + 1;
                                _locInventoryTransferOutLine.Save();
                                _locInventoryTransferOutLine.Session.CommitTransaction();
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOut " + ex.ToString());
            }
        }

        private void SetStatusDeliverInventoryTransferOutLine(Session _currSession, InventoryTransferOut _locInventoryTransferOutXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                if (_locInventoryTransferOutXPO != null)
                {
                    XPCollection<InventoryTransferOutLine> _locInventoryTransferOutLines = new XPCollection<InventoryTransferOutLine>(_currSession,
                                                            new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("InventoryTransferOut", _locInventoryTransferOutXPO),
                                                            new BinaryOperator("Select", true)));

                    if (_locInventoryTransferOutLines != null && _locInventoryTransferOutLines.Count > 0)
                    {

                        foreach (InventoryTransferOutLine _locInventoryTransferOutLine in _locInventoryTransferOutLines)
                        {
                            if (_locInventoryTransferOutLine.Status == Status.Progress || _locInventoryTransferOutLine.Status == Status.Posted)
                            {
                                if (_locInventoryTransferOutLine.RmDQty == 0 && _locInventoryTransferOutLine.RmQty == 0 && _locInventoryTransferOutLine.RmTQty == 0)
                                {
                                    _locInventoryTransferOutLine.Status = Status.Close;
                                    _locInventoryTransferOutLine.ActivationPosting = true;
                                    _locInventoryTransferOutLine.StatusDate = now;
                                }
                                else
                                {
                                    _locInventoryTransferOutLine.Status = Status.Posted;
                                    _locInventoryTransferOutLine.StatusDate = now;
                                }

                                XPCollection<InventoryTransferOutLot> _locInventoryTransferOutLots = new XPCollection<InventoryTransferOutLot>(_currSession,
                                                                                         new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("InventoryTransferOutLine", _locInventoryTransferOutLine),
                                                                                         new BinaryOperator("Select", true)));

                                if (_locInventoryTransferOutLots != null && _locInventoryTransferOutLots.Count() > 0)
                                {
                                    foreach (InventoryTransferOutLot _locInventoryTransferOutLot in _locInventoryTransferOutLots)
                                    {
                                        if (_locInventoryTransferOutLot.Status == Status.Progress || _locInventoryTransferOutLot.Status == Status.Posted)
                                        {
                                            _locInventoryTransferOutLot.Status = Status.Close;
                                            _locInventoryTransferOutLot.ActivationPosting = true;
                                            _locInventoryTransferOutLot.StatusDate = now;
                                            _locInventoryTransferOutLot.Save();
                                            _locInventoryTransferOutLot.Session.CommitTransaction();
                                        }
                                    }
                                }
                                _locInventoryTransferOutLine.Save();
                                _locInventoryTransferOutLine.Session.CommitTransaction();
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOut " + ex.ToString());
            }
        }

        private void SetNormalDeliverQuantity(Session _currSession, InventoryTransferOut _locInventoryTransferOutXPO)
        {
            try
            {
                if (_locInventoryTransferOutXPO != null)
                {
                    XPCollection<InventoryTransferOutLine> _locInventoryTransferOutLines = new XPCollection<InventoryTransferOutLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("InventoryTransferOut", _locInventoryTransferOutXPO),
                                                                                     new BinaryOperator("Select", true)));

                    if (_locInventoryTransferOutLines != null && _locInventoryTransferOutLines.Count > 0)
                    {
                        foreach (InventoryTransferOutLine _locInventoryTransferOutLine in _locInventoryTransferOutLines)
                        {
                            if (_locInventoryTransferOutLine.Status == Status.Progress || _locInventoryTransferOutLine.Status == Status.Posted || _locInventoryTransferOutLine.Status == Status.Close)
                            {
                                if (_locInventoryTransferOutLine.DQty > 0 || _locInventoryTransferOutLine.Qty > 0)
                                {
                                    _locInventoryTransferOutLine.Select = false;
                                    _locInventoryTransferOutLine.DQty = 0;
                                    _locInventoryTransferOutLine.Qty = 0;
                                    _locInventoryTransferOutLine.Save();
                                    _locInventoryTransferOutLine.Session.CommitTransaction();

                                    XPCollection<InventoryTransferOutLot> _locInventoryTransferOutLots = new XPCollection<InventoryTransferOutLot>(_currSession,
                                                                                         new GroupOperator(GroupOperatorType.And,
                                                                                         new BinaryOperator("InventoryTransferOutLine", _locInventoryTransferOutLine),
                                                                                         new BinaryOperator("Select", true)));

                                    if (_locInventoryTransferOutLots != null && _locInventoryTransferOutLots.Count() > 0)
                                    {
                                        foreach (InventoryTransferOutLot _locInventoryTransferOutLot in _locInventoryTransferOutLots)
                                        {
                                            if (_locInventoryTransferOutLot.Status == Status.Progress || _locInventoryTransferOutLot.Status == Status.Posted)
                                            {
                                                _locInventoryTransferOutLot.Select = false;
                                                _locInventoryTransferOutLot.Save();
                                                _locInventoryTransferOutLot.Session.CommitTransaction();
                                            }
                                        }
                                    }
                                }  
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOut " + ex.ToString());
            }
        }

        private void SetFinalStatusDeliverInventoryTransferOut(Session _currSession, InventoryTransferOut _locInventoryTransferOutXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                int _locCount = 0;

                if (_locInventoryTransferOutXPO != null)
                {
                    XPCollection<InventoryTransferOutLine> _locInventoryTransferOutLines = new XPCollection<InventoryTransferOutLine>(_currSession,
                                                                                     new GroupOperator(GroupOperatorType.And,
                                                                                     new BinaryOperator("InventoryTransferOut", _locInventoryTransferOutXPO)));

                    if (_locInventoryTransferOutLines != null && _locInventoryTransferOutLines.Count() > 0)
                    {
                        foreach (InventoryTransferOutLine _locInventoryTransferOutLine in _locInventoryTransferOutLines)
                        {
                            if (_locInventoryTransferOutLine.Status == Status.Close)
                            {
                                _locCount = _locCount + 1;
                            }
                        }

                        if (_locCount == _locInventoryTransferOutLines.Count())
                        {
                            _locInventoryTransferOutXPO.ActivationPosting = true;
                            _locInventoryTransferOutXPO.Status = Status.Close;
                            _locInventoryTransferOutXPO.StatusDate = now;
                            _locInventoryTransferOutXPO.Save();
                            _locInventoryTransferOutXPO.Session.CommitTransaction();
                        }else
                        {
                            _locInventoryTransferOutXPO.Status = Status.Posted;
                            _locInventoryTransferOutXPO.StatusDate = now;
                            _locInventoryTransferOutXPO.Save();
                            _locInventoryTransferOutXPO.Session.CommitTransaction();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOut " + ex.ToString());
            }
        }

        #endregion InventoryNormalDeliver

        #region GoodsIssueJournal

        //PR Besar Harus mengambil dari COGS
        private void SetGoodsDeliverJournal(Session _currSession, InventoryTransferOut _locInventoryTransferOutXPO)
        {
            try
            {
                DateTime now = DateTime.Now;
                double _locTotalUnitAmount = 0; 

                if(_locInventoryTransferOutXPO != null)
                {
                    XPCollection<InventoryTransferOutLine> _locInventoryTransferOutLines = new XPCollection<InventoryTransferOutLine>
                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                           new BinaryOperator("InventoryTransferOut", _locInventoryTransferOutXPO),
                                                                                           new BinaryOperator("Select", true),
                                                                                           new GroupOperator(GroupOperatorType.Or,
                                                                                           new BinaryOperator("Status", Status.Progress),
                                                                                           new BinaryOperator("Status", Status.Posted))));
                    if (_locInventoryTransferOutLines != null && _locInventoryTransferOutLines.Count() > 0)
                    {
                        foreach (InventoryTransferOutLine _locInventoryTransferOutLine in _locInventoryTransferOutLines)
                        {
                            XPQuery<SalesOrderMonitoring> _salesQuotationMonitoringsQuery = new XPQuery<SalesOrderMonitoring>(_currSession);

                            var _salesOrderMonitorings = from sqm in _salesQuotationMonitoringsQuery
                                                         where (sqm.InventoryTransferOut == _locInventoryTransferOutXPO 
                                                         && sqm.InventoryTransferOutLine == _locInventoryTransferOutLine
                                                         )
                                                         group sqm by sqm.SalesOrder into g
                                                         select new { SalesOrder = g.Key };

                            if(_salesOrderMonitorings != null && _salesOrderMonitorings.Count() > 0)
                            {
                                foreach(var _salesOrderMonitoring in _salesOrderMonitorings)
                                {
                                    if(_salesOrderMonitoring.SalesOrder != null)
                                    {
                                        XPCollection<SalesOrderMonitoring> _locSalesOrderMonitorings = new XPCollection<SalesOrderMonitoring>
                                                                                           (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                           new BinaryOperator("InventoryTransferOut", _locInventoryTransferOutXPO),
                                                                                           new BinaryOperator("InventoryTransferOutLine", _locInventoryTransferOutLine),
                                                                                           new BinaryOperator("SalesOrder", _salesOrderMonitoring.SalesOrder)));

                                        if (_locSalesOrderMonitorings != null && _locSalesOrderMonitorings.Count() > 0)
                                        {
                                            foreach (SalesOrderMonitoring _locSalesOrderMonitoring in _locSalesOrderMonitorings)
                                            {
                                                if(_locSalesOrderMonitoring.SalesOrderLine != null)
                                                {
                                                    //Create salesorderline based Item   
                                                    _locTotalUnitAmount = _locSalesOrderMonitoring.TUAmount;

                                                }

                                                #region CreateGoodsIssueJournal

                                                #region JournalMapByItems
                                                if(_locInventoryTransferOutLine.Item != null)
                                                {
                                                    if(_locInventoryTransferOutLine.Item.ItemAccountGroup != null)
                                                    {
                                                        XPCollection<JournalMap> _locJournalMapItems = new XPCollection<JournalMap>
                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                new BinaryOperator("ItemAccountGroup", _locInventoryTransferOutLine.Item.ItemAccountGroup)));

                                                        if (_locJournalMapItems != null && _locJournalMapItems.Count() > 0)
                                                        {
                                                            foreach (JournalMap _locJournalMapItem in _locJournalMapItems)
                                                            {
                                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("JournalMap", _locJournalMapItem)));

                                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                                {
                                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                                    {
                                                                        AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                                    (new GroupOperator(GroupOperatorType.And,
                                                                                                     new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                                     new BinaryOperator("PostingType", PostingType.Sales),
                                                                                                     new BinaryOperator("OrderType", _locSalesOrderMonitoring.SalesType),
                                                                                                     new BinaryOperator("PostingMethod", PostingMethod.Deliver),
                                                                                                     new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                                        if (_locAccountMap != null)
                                                                        {
                                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                                               (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                                new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                                                new BinaryOperator("Active", true)));

                                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                            {
                                                                                double _locTotalAmountDebit = 0;
                                                                                double _locTotalAmountCredit = 0;
                                                                                double _locTotalBalance = 0;

                                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                {
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        _locTotalAmountDebit = _locTotalUnitAmount;
                                                                                    }
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        _locTotalAmountCredit = _locTotalUnitAmount;
                                                                                    }

                                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                    {
                                                                                        PostingDate = now,
                                                                                        PostingType = PostingType.Sales,
                                                                                        OrderType = _locSalesOrderMonitoring.SalesType,
                                                                                        PostingMethod = PostingMethod.Deliver,
                                                                                        PostingMethodType = PostingMethodType.Normal,
                                                                                        Account = _locAccountMapLine.Account,
                                                                                        Debit = _locTotalAmountDebit,
                                                                                        Credit = _locTotalAmountCredit,
                                                                                        SalesOrder = _locSalesOrderMonitoring.SalesOrder,
                                                                                        SalesOrderLine = _locSalesOrderMonitoring.SalesOrderLine,
                                                                                        InventoryTransferOut = _locInventoryTransferOutXPO,
                                                                                        InventoryTransferOutLine = _locInventoryTransferOutLine,
                                                                                        Company = _locInventoryTransferOutXPO.Company,
                                                                                    };

                                                                                    _saveGeneralJournal.Save();
                                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                                    #region COA
                                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                                    {
                                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                                 new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                                        if (_locCOA != null)
                                                                                        {
                                                                                            if (_locCOA.BalanceType == BalanceType.Change)
                                                                                            {
                                                                                                if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                                if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                            }

                                                                                            _locCOA.Balance = _locTotalBalance;
                                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                            _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                            _locCOA.Save();
                                                                                            _locCOA.Session.CommitTransaction();
                                                                                        }
                                                                                    }
                                                                                    #endregion COA
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }   
                                                }
                                                
                                                #endregion JournalMapByItems

                                                #region JournalMapByBusinessPartner
                                                if (_locInventoryTransferOutXPO.BusinessPartner != null)
                                                {
                                                    if (_locInventoryTransferOutXPO.BusinessPartner.BusinessPartnerAccountGroup != null)
                                                    {
                                                        XPCollection<JournalMap> _locJournalMapBusinessPartners = new XPCollection<JournalMap>
                                                                                                                  (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                   new BinaryOperator("BusinessPartnerAccountGroup", _locInventoryTransferOutXPO.BusinessPartner.BusinessPartnerAccountGroup)));

                                                        if (_locJournalMapBusinessPartners != null && _locJournalMapBusinessPartners.Count() > 0)
                                                        {
                                                            foreach (JournalMap _locJournalMapBusinessPartner in _locJournalMapBusinessPartners)
                                                            {
                                                                XPCollection<JournalMapLine> _locJournalMapLines = new XPCollection<JournalMapLine>
                                                                                                                   (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                                    new BinaryOperator("JournalMap", _locJournalMapBusinessPartner)));

                                                                if (_locJournalMapLines != null && _locJournalMapLines.Count() > 0)
                                                                {
                                                                    foreach (JournalMapLine _locJournalMapLine in _locJournalMapLines)
                                                                    {
                                                                        AccountMap _locAccountMap = _currSession.FindObject<AccountMap>
                                                                                                     (new GroupOperator(GroupOperatorType.And,
                                                                                                      new BinaryOperator("Code", _locJournalMapLine.AccountMap.Code),
                                                                                                      new BinaryOperator("PostingType", PostingType.Sales),
                                                                                                      new BinaryOperator("OrderType", _locSalesOrderMonitoring.SalesType),
                                                                                                      new BinaryOperator("PostingMethod", PostingMethod.Deliver),
                                                                                                      new BinaryOperator("PostingMethodType", PostingMethodType.Normal)));

                                                                        if (_locAccountMap != null)
                                                                        {
                                                                            XPCollection<AccountMapLine> _locAccountMapLines = new XPCollection<AccountMapLine>
                                                                                                            (_currSession, new GroupOperator(GroupOperatorType.And,
                                                                                                             new BinaryOperator("AccountMap", _locAccountMap),
                                                                                                             new BinaryOperator("Active", true)));

                                                                            if (_locAccountMapLines != null && _locAccountMapLines.Count() > 0)
                                                                            {
                                                                                double _locTotalAmountDebit = 0;
                                                                                double _locTotalAmountCredit = 0;
                                                                                double _locTotalBalance = 0;

                                                                                foreach (AccountMapLine _locAccountMapLine in _locAccountMapLines)
                                                                                {
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Debit)
                                                                                    {
                                                                                        _locTotalAmountDebit = _locTotalUnitAmount;
                                                                                    }
                                                                                    if (_locAccountMapLine.AccountCharge == AccountCharge.Credit)
                                                                                    {
                                                                                        _locTotalAmountCredit = _locTotalUnitAmount;
                                                                                    }

                                                                                    GeneralJournal _saveGeneralJournal = new GeneralJournal(_currSession)
                                                                                    {
                                                                                        PostingDate = now,
                                                                                        PostingType = PostingType.Sales,
                                                                                        OrderType = _locSalesOrderMonitoring.SalesType,
                                                                                        PostingMethod = PostingMethod.Deliver,
                                                                                        PostingMethodType = PostingMethodType.Normal,
                                                                                        Account = _locAccountMapLine.Account,
                                                                                        Debit = _locTotalAmountDebit,
                                                                                        Credit = _locTotalAmountCredit,
                                                                                        SalesOrder = _locSalesOrderMonitoring.SalesOrder,
                                                                                        SalesOrderLine = _locSalesOrderMonitoring.SalesOrderLine,
                                                                                        InventoryTransferOut = _locInventoryTransferOutXPO,
                                                                                        InventoryTransferOutLine = _locInventoryTransferOutLine,
                                                                                        Company = _locInventoryTransferOutXPO.Company,
                                                                                    };

                                                                                    _saveGeneralJournal.Save();
                                                                                    _saveGeneralJournal.Session.CommitTransaction();

                                                                                    #region COA
                                                                                    if (_locAccountMapLine.Account.Code != null)
                                                                                    {
                                                                                        ChartOfAccount _locCOA = _currSession.FindObject<ChartOfAccount>
                                                                                                                (new GroupOperator(GroupOperatorType.And,
                                                                                                                 new BinaryOperator("Code", _locAccountMapLine.Account.Code)));
                                                                                        if (_locCOA != null)
                                                                                        {
                                                                                            if (_locCOA.BalanceType == BalanceType.Change)
                                                                                            {
                                                                                                if (_locCOA.AccountCharge == AccountCharge.Debit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                                if (_locCOA.AccountCharge == AccountCharge.Credit)
                                                                                                {
                                                                                                    if (_locTotalAmountDebit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance - _locTotalAmountDebit;
                                                                                                    }
                                                                                                    if (_locTotalAmountCredit > 0)
                                                                                                    {
                                                                                                        _locTotalBalance = _locCOA.Balance + _locTotalAmountCredit;
                                                                                                    }
                                                                                                }
                                                                                            }

                                                                                            _locCOA.Balance = _locTotalBalance;
                                                                                            _locCOA.Debit = _locCOA.Debit + _locTotalAmountDebit;
                                                                                            _locCOA.Credit = _locCOA.Credit + _locTotalAmountCredit;
                                                                                            _locCOA.Save();
                                                                                            _locCOA.Session.CommitTransaction();
                                                                                        }
                                                                                    }
                                                                                    #endregion COA
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                #endregion JournalMapByBusinessPartner

                                                #endregion CreateGoodsIssueJournal
                                            }
                                        }
                                    }          
                                }
                            }                          
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError("Business Object = InventoryTransferOut ", ex.ToString());
            }
        }

        #endregion GoodsIssueJournal

        #endregion Posting 

        #region Global Method

        private void SuccessMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 4000;
            options.Message = string.Format("{0}", _locMessage);
            options.Type = InformationType.Success;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Success";
            options.Win.Type = WinMessageType.Flyout;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private void ErrorMessageShow(string _locMessage)
        {
            MessageOptions options = new MessageOptions();
            options.Duration = 2000;
            options.Message = string.Format(_locMessage);
            options.Type = InformationType.Warning;
            options.Web.Position = InformationPosition.Right;
            options.Win.Caption = "Error";
            options.Win.Type = WinMessageType.Alert;
            Application.ShowViewStrategy.ShowMessage(options);
        }

        private int GetStockType(StockType objectName)
        {
            int _result = 0;
            try
            {
                if (objectName == StockType.Good)
                {
                    _result = 1;
                }
                else if (objectName == StockType.Bad)
                {
                    _result = 2;
                }
                else
                {
                    _result = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = InventoryTransferOut " + ex.ToString());
            }
            return _result;
        }

        private int GetActive(bool objectName)
        {
            int _result = 0;
            try
            {
                if (objectName == true)
                {
                    _result = 1;
                }
                else
                {
                    _result = 0;
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" Module = InventoryTransferOut " + ex.ToString());
            }
            return _result;
        }

        private double GetUnitPriceByPurchaseOrder(Session _currSession, PurchaseOrder _locPurchaseOrder, Item _locItem)
        {
            double _result = 0;
            try
            {
                if (_locPurchaseOrder != null)
                {
                    PurchaseOrderLine _locPurchaseOrderLine = _currSession.FindObject<PurchaseOrderLine>
                                                            (new GroupOperator(GroupOperatorType.And,
                                                            new BinaryOperator("PurchaseOrder", _locPurchaseOrder),
                                                            new BinaryOperator("Item", _locItem)));
                    if (_locPurchaseOrderLine != null)
                    {
                        _result = _locPurchaseOrderLine.UAmount;
                    }
                }
            }
            catch (Exception ex)
            {
                Tracing.Tracer.LogError(" BusinessObject = InventoryTransferOut " + ex.ToString());
            }
            return _result;
        }

        #endregion Global Method

    }
}
