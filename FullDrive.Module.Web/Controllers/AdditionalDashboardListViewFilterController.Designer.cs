﻿namespace FullDrive.Module.Web.Controllers
{
    partial class AdditionalDashboardListViewFilterController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CustomDashboardAdditionalDashboard = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // CustomDashboardAdditionalDashboard
            // 
            this.CustomDashboardAdditionalDashboard.Caption = "Edit Dashboard";
            this.CustomDashboardAdditionalDashboard.Category = "Edit";
            this.CustomDashboardAdditionalDashboard.ConfirmationMessage = null;
            this.CustomDashboardAdditionalDashboard.Id = "CustomDashboardAdditionalDashboardId";
            this.CustomDashboardAdditionalDashboard.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.CustomDashboardAdditionalDashboard.ToolTip = null;
            this.CustomDashboardAdditionalDashboard.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CustomDashboardAdditionalDashboard_Execute);
            // 
            // AdditionalDashboardListViewFilterController
            // 
            this.Actions.Add(this.CustomDashboardAdditionalDashboard);
            this.TargetObjectType = typeof(FullDrive.Module.BusinessObjects.AdditionalDashboard);
            this.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.ListView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction CustomDashboardAdditionalDashboard;
    }
}
