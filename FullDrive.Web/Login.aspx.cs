﻿using System;

using DevExpress.ExpressApp.Web;
using DevExpress.ExpressApp.Web.Templates;
using System.Web.UI.WebControls;

public partial class LoginPage : BaseXafPage {
    public override System.Web.UI.Control InnerContentPlaceHolder {
        get {
            return Content;
        }
    }

    protected void Page_Init()
    {
        CustomizeTemplateContent += (s, e) => {
            IHeaderImageControlContainer content = TemplateContent as IHeaderImageControlContainer;
            if (content == null) return;
            content.HeaderImageControl.DefaultThemeImageLocation = "Images";
            content.HeaderImageControl.ImageName = "SKI.png";
            content.HeaderImageControl.Width = Unit.Pixel(140);
            content.HeaderImageControl.Height = Unit.Pixel(90);
        };

        //CustomizeTemplateContent += (s, e) => {
        //    IHeaderImageControlContainer content = TemplateContent as IHeaderImageControlContainer;
        //    if (content == null) return;
        //    content.HeaderImageControl.DefaultThemeImageLocation = "Images";
        //    content.HeaderImageControl.ImageName = "SKC.png";
        //    content.HeaderImageControl.Width = Unit.Pixel(140);
        //    content.HeaderImageControl.Height = Unit.Pixel(100);
        //};

        //CustomizeTemplateContent += (s, e) => {
        //    IHeaderImageControlContainer content = TemplateContent as IHeaderImageControlContainer;
        //    if (content == null) return;
        //    content.HeaderImageControl.DefaultThemeImageLocation = "Images";
        //    content.HeaderImageControl.ImageName = "MEI.png";
        //    content.HeaderImageControl.Width = Unit.Pixel(150);
        //    content.HeaderImageControl.Height = Unit.Pixel(80);
        //};
    }
}
